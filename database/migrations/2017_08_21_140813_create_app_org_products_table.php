<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgProducts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('name_en');
            $table->date('release_date')->index();

            $table->integer('prod_category_id')->index();
            $table->char('game_category',10)->index();
            $table->char('game_producer',10)->index();
            $table->char('game_generation',10)->index();
            $table->char('game_location',10)->index();

            $table->char('language',10)->nullable();

            //$table->string('measures')->nullable();

            $table->decimal('volume',12,2)->nullable();
            $table->decimal('weight',12,2)->nullable();

            $table->string('ean_upc')->nullable();

            $table->char('media',10)->index();

            $table->string('platform');
            $table->integer('platform_id')->index();
            $table->char('region',10);
            $table->integer('region_id')->index();
            $table->string('image_path')->nullable();

            $table->text('comments')->nullable();

            $table->char('is_featured',1)->default('N')->index();
            $table->char('is_enabled',1)->default('Y')->index();

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('modified_by')->unsigned()->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgProducts');
    }
}
