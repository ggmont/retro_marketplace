<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appConversations', function (Blueprint $table) {
            $table->increments('id');
            
            // user
            $table->String('alias_user_id');
            
            // contact
            $table->String('alias_contact_id');
            
            // last message: content, dateTime
            $table->text('last_message')->nullable();
            $table->dateTime('last_time')->nullable();

            $table->boolean('del_messages')->default(false);

            $table->boolean('master_conversation')->default(false);

            $table->boolean('listen_notifications')->default(true);
            $table->boolean('has_blocked')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appConversations');
    }
}
