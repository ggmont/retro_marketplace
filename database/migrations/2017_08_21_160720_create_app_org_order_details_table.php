<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgOrderDetails', function (Blueprint $table) {
            $table->increments('id');

            $table->char('type',2)->index(); // Product, Reversion, Discount

            $table->integer('order_id')->index();
            $table->integer('seller_user_id')->index();
            $table->integer('product_id')->index();

            $table->timestamp('created_on');
            $table->integer('inventory_id')->index();
            $table->integer('quantity');
            $table->decimal('price',12,4);

            $table->decimal('sub_total',12,4);
            $table->decimal('tax',12,4);
            $table->decimal('total',12,4);

            $table->char('status',2)->index()->default('PD'); // Pending, Sent, Returned, Delivered, Lost, Cancelled etc

            $table->char('is_enabled',1)->default('N')->index();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('modified_by')->unsigned()->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgOrderDetails');
    }
}
