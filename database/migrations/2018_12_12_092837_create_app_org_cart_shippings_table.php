<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgCartShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgCartShippings', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('user_id')->unsigned()->nullable()->foreign('user_id')->references('id')->on('sysUsers');

            $table->integer('cart_id')->unsigned()->nullable()->foreign('cart_id')->references('id')->on('appOrgCartUsers');

            $table->integer('shipping_id')->unsigned()->nullable()->foreign('shipping_id')->references('id')->on('sysShippings');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgCartShippings');
    }
}
