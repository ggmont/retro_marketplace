<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeseifTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beseif_transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('action');
            $table->string('id_transaccion');
            $table->string('email_comprador');
            $table->string('id_anuncio');
            $table->string('signature');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beseif_transaction');
    }
}
