<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncidenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidence', function (Blueprint $table) {
            $table->increments('id');

            $table->integer("user_id")->unsigned()->nullable();

            $table->foreign('user_id')->references('id')
                ->on('sysUsers')
                ->onDelete('cascade');

            $table->integer("order_id")->unsigned()->nullable();

            $table->foreign('order_id')->references('id')
                                       ->on('appOrgOrders')
                                       ->onDelete('cascade');

            $table->string('reason');
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidence');
    }
}
