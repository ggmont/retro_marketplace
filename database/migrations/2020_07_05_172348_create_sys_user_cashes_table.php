<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysUserCashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sysUserCashs', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->default(0);
            
            $table->decimal('user_cash', 8, 2)->default(0);
            $table->decimal('user_especial', 8, 2)->default(0);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sysUserCashs');
    }
}
