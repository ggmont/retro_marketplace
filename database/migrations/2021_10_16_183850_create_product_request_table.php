<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_request', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category',255);
            $table->integer("user_id")->unsigned()->nullable();

            $table->foreign('user_id')->references('id')
                                       ->on('sysUsers')
                                       ->onDelete('cascade');

            $table->string('product',255);
            $table->string('platform',255);
            $table->string('region',255); 

            $table->string('quantity',255);
            $table->string('price',255);
            $table->string('comments',255);  

            $table->string('box_condition',100)->nullable(); // Caja
            $table->string('manual_condition',100)->nullable(); // Manual
            $table->string('cover_condition',100)->nullable(); //Interior
            $table->string('game_condition',100)->nullable(); //Consola
            $table->string('extra_condition',100)->nullable(); // Extra
            $table->string('inside_condition',100)->nullable(); // Interior
            $table->string('status',100)->nullable(); //Estado

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_request');
    }
}
