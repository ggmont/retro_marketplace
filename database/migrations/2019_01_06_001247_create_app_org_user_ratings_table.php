<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgUserRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgUserRatings', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('seller_user_id')->index();

            $table->integer('buyer_user_id')->index();

            $table->integer('order_id')->index();

            $table->text('description');

            $table->integer('processig');
            $table->integer('packaging');
            $table->integer('desc_prod');

            $table->boolean('status')->default(false);

            $table->decimal('score',2,2)->default(0.0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgUserRatings');
    }
}
