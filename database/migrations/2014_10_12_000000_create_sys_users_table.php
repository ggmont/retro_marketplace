<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('sysUsers', function (Blueprint $table) {
      $table->increments('id');

      $table->string('first_name');
      $table->string('last_name');
      $table->string('user_name')->unique();
      $table->string('display_name')->nullable();
      $table->string('profile_picture')->nullable();
      $table->string('cover_picture')->nullable();
      $table->string('email')->unique();
      $table->string('password');
      $table->string('phone')->nullable();

      $table->char('is_enabled',1)->default('Y');
      $table->char('request_password',1)->default('N');
      $table->char('is_activated',1)->default('N');

      $table->string('address')->nullable();
      $table->string('zipcode')->nullable();
      $table->string('city')->nullable();

      $table->integer('country_id')->index()->default(46);

 
      
      $table->string('country_code',2)->index()->default('ES');

      $table->decimal('amountEURO',12,2)->default(00.00);
      $table->string('concept')->nullable();
      // company
      $table->string('company_vat')->nullable();
      $table->string('company_name')->nullable();
      $table->char('is_company', 1)->default('N');

      $table->rememberToken();

      $table->timestamp('last_loggin')->nullable();

  		$table->integer('created_by')->unsigned()->default(1);
  		$table->integer('modified_by')->unsigned()->nullable();
  		$table->softDeletes();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('sysUsers');
  }
}
