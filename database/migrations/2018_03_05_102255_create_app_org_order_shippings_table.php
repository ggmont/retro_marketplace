<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgOrderShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgOrderShippings', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('order_id')->index();
            $table->char('company_code',10)->index(); // UPS, FEDEX,
            $table->string('tracking_number')->index();

            $table->text('comments');
            
            $table->char('type',10)->index();

            $table->timestamp('sent_on');
            $table->timestamp('delivered_on');

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('modified_by')->unsigned()->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgOrderShippings');
    }
}
