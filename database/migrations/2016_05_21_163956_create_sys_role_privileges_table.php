<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysRolePrivilegesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('sysRolePrivileges', function (Blueprint $table) {
      $table->increments('id');

      $table->integer('role_id');
      $table->integer('privilege_id');
      $table->char('is_enabled',1)->default('Y');
      $table->integer('created_by')->unsigned()->default(1);
      $table->integer('modified_by')->unsigned()->nullable();
      $table->softDeletes();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('sysRolePrivileges');
  }
}
