<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldOrderShiping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appOrgOrders', function (Blueprint $table) {
            $table->string('company_code')->nullable(); 
            $table->string('tracking_number')->nullable(); 
            $table->string('type_shipping')->nullable(); 
            $table->dateTime('sent_on')->nullable(); 
            $table->dateTime('delivered_on')->nullable(); 
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
