<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgOrderCancelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgOrderCancels', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('seller_user_id')->index();
            $table->integer('buyer_user_id')->index();

            $table->integer('order_id')->index();

            $table->char('solicited_by')->nullable();

            $table->char('answer')->nullable();

            $table->text('cause')->nullable();

            $table->text('reason')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgOrderCancels');
    }
}
