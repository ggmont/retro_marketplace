<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sysCountries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('alpha_2',2)->index();
            $table->string('alpha_3',3);
            $table->string('country_code',3);
            $table->string('iso_3166_2');
            $table->string('region');
            $table->string('sub_region');
            $table->string('region_code');
            $table->string('sub_region_code');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sysCountries');
    }
}
