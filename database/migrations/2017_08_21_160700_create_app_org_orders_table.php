<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgOrders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_identification',50)->index();
            $table->integer('seller_user_id')->unsigned()->index();
            $table->integer('buyer_user_id')->unsigned()->index();

            $table->integer('shipping_id')->unsigned()->index();
            $table->decimal('shipping_price',12,4);
            
            $table->timestamp('created_on');
            $table->timestamp('max_pay_out');

            $table->integer('quantity');
            $table->decimal('sub_total',12,4);
            $table->decimal('tax',12,4);
            $table->decimal('total',12,4);
            
            $table->char('paid_out')->default('N');

            $table->text('instructions');

            $table->char('seller_read',2)->default('N')->nullable();;
            $table->char('buyer_read',2)->default('N')->nullable();;

            $table->char('status',2)->index(); // Created, Pending, Sent, Delivered, Lost, Cancelled

            $table->char('is_enabled',1)->default('N')->index();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('modified_by')->unsigned()->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgOrders');
    }
}
