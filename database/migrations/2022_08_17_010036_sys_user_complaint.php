<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SysUserComplaint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_user_complaint', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id")->unsigned()->nullable();

            $table->foreign('user_id')->references('id')
                ->on('sysUsers')
                ->onDelete('cascade');
            $table->integer("product_id")->unsigned()->nullable();

            $table->foreign('product_id')->references('id')
                ->on('appOrgUserInventories')
                ->onDelete('cascade');

                
            $table->integer('fecha')->unsigned()->nullable();
            $table->string('category')->nullable();
            $table->string('type')->nullable();
            $table->string('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_user_social');
    }
}
