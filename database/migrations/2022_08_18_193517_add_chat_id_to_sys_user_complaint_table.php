<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddChatIdToSysUserComplaintTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sys_user_complaint', function (Blueprint $table) {
            $table->integer("chat_id")->unsigned()->nullable();

            $table->foreign('chat_id')->references('id')
                ->on('sysUsers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sys_user_complaint', function (Blueprint $table) {
            //
        });
    }
}
