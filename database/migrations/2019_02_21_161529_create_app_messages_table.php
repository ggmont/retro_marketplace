<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appMessages', function (Blueprint $table) {
            $table->increments('id');

            // from
            $table->String('alias_from_id');
            // to
            $table->String('alias_to_id');

            $table->String('alias_from_del')->nullable();
            $table->String('alias_to_del')->nullable();
            
            // read
            $table->boolean('read')->default(false);

            // content
            $table->text('content');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appMessages');
    }
}
