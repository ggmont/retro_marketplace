<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sysShippings', function (Blueprint $table) {
            $table->increments('id');

            $table->text('name');

            $table->string('certified', 3);

            $table->char('type',2)->index();
            
            // Max Volume
            $table->integer('large');
            $table->integer('width');
            $table->integer('hight');
            
            $table->decimal('volume',12,2);
            $table->decimal('max_value',12,4);
            $table->integer('max_weight');
            $table->decimal('stamp_price',12,4);
            $table->decimal('price',12,4);
            
            $table->integer('from_id')->default(46);
            $table->integer('to_id')->default(46);

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('modified_by')->unsigned()->nullable();

            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sysShippings');
    }
}
