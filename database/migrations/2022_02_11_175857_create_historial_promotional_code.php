<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistorialPromotionalCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_promotional_code', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id")->unsigned()->nullable();

            $table->foreign('user_id')->references('id')
                                       ->on('sysUsers')
                                       ->onDelete('cascade');
           $table->integer("code_id")->unsigned()->nullable();

           $table->foreign('code_id')->references('id')
           ->on('code_promotional')
           ->onDelete('cascade');

            $table->integer("order_id")->unsigned()->nullable();

            $table->foreign('order_id')->references('id')
            ->on('appOrgOrders')
            ->onDelete('cascade');

            $table->string('total')->nullable();  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_promotional_code');
    }
}
