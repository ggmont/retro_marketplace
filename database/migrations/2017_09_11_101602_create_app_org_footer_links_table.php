<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgFooterLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgFooterLinks', function (Blueprint $table) {
            $table->increments('id');

            $table->string('column',5)->index();
            $table->string('name',100)->unique();
            $table->string('url',255);

            $table->char('new_page',1)->default('Y');
            
            $table->char('is_enabled',1)->default('Y');

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('modified_by')->unsigned()->nullable();

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgFooterLinks');
    }
}
