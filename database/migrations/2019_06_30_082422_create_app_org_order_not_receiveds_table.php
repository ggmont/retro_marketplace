<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgOrderNotReceivedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgOrderNotReceiveds', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('order_id');
            $table->string('buyer_username')->nullable();
            $table->string('seller_username')->nullable();
            $table->string('import',12,4);
            $table->dateTime('dateSend')->nullable();
            $table->string('tracking_number')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgOrderNotReceiveds');
    }
}
