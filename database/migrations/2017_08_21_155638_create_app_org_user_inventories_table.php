<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgUserInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgUserInventories', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned()->index();
            $table->integer('product_id')->unsigned()->index();

            $table->char('box_condition',10)->index(); // Caja
            $table->char('manual_condition',10)->index(); // Manual
            $table->char('cover_condition',10)->index(); //Interior
            $table->char('game_condition',10)->index(); //Consola
            $table->char('extra_condition',10)->index(); // Extra

            //in migration 
            //$table->string('inside_condition')->nullable(); // Cables 

            $table->char('location',10);
            $table->char('media',10);

            $table->integer('quantity')->unsigned()->default(1);
            $table->integer('quantity_sold')->unsigned()->default(0);
            $table->decimal('price',12,4);
            $table->decimal('tax',12,4);
            $table->text('comments')->nullable();

            $table->char('status',1)->index()->default('A'); //Available, Hidden, Sold, Draft
            $table->char('is_enabled',1)->default('N')->index();

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('modified_by')->unsigned()->nullable();

            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgUserInventories');
    }
}
