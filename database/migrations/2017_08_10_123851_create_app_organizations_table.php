<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrganizations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('sys_org_id');

            $table->string('company_name');
            $table->string('address_line_1');
            $table->string('address_line_2');
            $table->integer('country_id')->index();
            $table->string('country_code',2)->index();
            $table->string('country_state');
            $table->string('zip_code');

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('modified_by')->unsigned()->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrganizations');
    }
}
