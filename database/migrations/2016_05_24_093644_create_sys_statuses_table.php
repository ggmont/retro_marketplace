<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysStatusesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('sysStatuses', function (Blueprint $table) {
      $table->increments('id');

      $table->string('status_for');
      $table->string('name');
      $table->string('value');

      $table->index('status_for');

      $table->integer('created_by')->unsigned();
      $table->integer('modified_by')->unsigned()->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('sysStatuses');
  }
}
