<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgCarouselsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgCarousels', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->char('location',10)->index();
            $table->char('in_use',1)->index();

            $table->char('width_type',10);
            $table->char('height_type',10);
            $table->decimal('height',10,4);

            $table->char('is_enabled',1)->default('Y');

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('modified_by')->unsigned()->nullable();

            $table->softDeletes();

            $table->timestamps();

            $table->unique(['name','location']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgCarousels');
    }
}
