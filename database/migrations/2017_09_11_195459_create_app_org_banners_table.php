<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgBanners', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('image_path');
            $table->integer('priority');
            $table->integer('counter');
            $table->char('location',10)->index();

            $table->char('is_enabled',1)->default('Y');

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('modified_by')->unsigned()->nullable();

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgBanners');
    }
}
