<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgCategories', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('parent_id')->default(0)->index();
            $table->string('name');
            $table->integer('hierarchy')->default(0);
            $table->string('category_text')->index();
            $table->string('order_text');
            $table->string('image_path');

            $table->char('is_enabled',1)->default('Y')->index();
            $table->smallinteger('order_by')->default(1);
            $table->smallinteger('children')->default(0);

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('modified_by')->unsigned()->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgCategories');
    }
}
