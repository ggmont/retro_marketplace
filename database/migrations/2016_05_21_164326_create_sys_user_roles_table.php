<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysUserRolesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('sysUserRoles', function (Blueprint $table) {
      $table->increments('id');

      $table->integer('user_id');
      
      $table->integer('role_id');

      $table->char('is_enabled',1)->default('Y');
      $table->integer('created_by')->unsigned()->default(1);
      $table->integer('modified_by')->unsigned()->nullable();
      $table->softDeletes();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('sysUserRoles');
  }
}
