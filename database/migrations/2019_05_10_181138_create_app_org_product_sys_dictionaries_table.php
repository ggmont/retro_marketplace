<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgProductSysDictionariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgProduct_sysDictionary', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('product_id')->unsigned();
            $table->integer('dictionary_id')->unsigned();

            $table->timestamps();
        });

        Schema::table('appOrgProduct_sysDictionary', function (Blueprint $table) {

            $table->foreign('product_id')->references('id')->on('appOrgProducts');
            $table->foreign('dictionary_id')->references('id')->on('sysDictionaries');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgProduct_sysDictionary');
    }
}
