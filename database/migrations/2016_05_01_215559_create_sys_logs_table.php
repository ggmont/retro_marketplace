<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysLogsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('sysLogs', function (Blueprint $table) {
      $table->increments('id');
      $table->string('action',30);

      $table->string('ip_address',15)->nullable();

      $table->string('table_name',255)->nullable();
      $table->integer('object_id')->nullable();

      $table->integer('user_id')->unsigned()->nullable();
      $table->string('additional_info',1000)->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('sysLogs');
  }
}
