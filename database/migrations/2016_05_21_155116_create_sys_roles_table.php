<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysRolesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('sysRoles', function (Blueprint $table) {
      $table->increments('id');

      $table->string('name',50);
      $table->string('description',150);
      $table->char('is_enabled',1)->default('Y');
      $table->integer('created_by')->unsigned()->default(1);
      $table->integer('modified_by')->unsigned()->nullable();
      $table->softDeletes();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('sysRoles');
  }
}
