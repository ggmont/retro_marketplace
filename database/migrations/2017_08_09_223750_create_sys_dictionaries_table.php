<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysDictionariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sysDictionaries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',15)->index();
            $table->string('value_id',10)->index();
            $table->string('value');
            $table->integer('parent_id')->nullable()->index();

            $table->smallInteger('order_by')->nullable();

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('modified_by')->unsigned()->nullable();

            $table->unique(['code','value_id', 'value']);

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sysDictionaries');
    }
}
