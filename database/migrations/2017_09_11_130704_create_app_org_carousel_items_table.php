<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgCarouselItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgCarouselItems', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('carousel_id')->index();
            $table->string('image_path')->nullable();
            $table->string('content_title')->nullable();
            $table->string('content_text')->nullable();
            $table->smallInteger('slide_order')->nullable();
            $table->integer('slide_duration')->nullable(); // millisegs

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('modified_by')->unsigned()->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgCarouselItems');
    }
}
