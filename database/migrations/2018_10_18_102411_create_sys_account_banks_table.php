<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysAccountBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sysAccountBanks', function (Blueprint $table) {
            $table->increments('id');

            $table->string('beneficiary')->nullable();
            $table->string('iban_code')->nullable();
            $table->string('bic_code')->nullable();
            $table->string('bank_address')->nullable();

            $table->integer('user_id');

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sysAccountBanks');
    }
}
