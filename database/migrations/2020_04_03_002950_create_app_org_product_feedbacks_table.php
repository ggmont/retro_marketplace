<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgProductFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgProductFeedbacks', function (Blueprint $table) {
            $table->increments('id');

            $table->char('box_condition',10)->index(); // Caja
            $table->char('manual_condition',10)->index(); // Manual
            $table->char('cover_condition',10)->index(); //Interior
            $table->char('game_condition',10)->index(); //Consola
            $table->char('extra_condition',10)->index(); // Extra
            $table->string('inside_condition')->nullable(); // Inside
            $table->string('ean')->nullable(); // EAN

            $table->integer('inventory_id')->unsigned()->index();
            $table->integer('product_id')->unsigned()->index();
            
            $table->integer('seller_user_id')->unsigned()->index();
            $table->integer('buyer_user_id')->unsigned()->index();
            
            $table->integer('order_id')->unsigned()->index();

            $table->decimal('price',12,4);

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgProductFeedbacks');
    }
}
