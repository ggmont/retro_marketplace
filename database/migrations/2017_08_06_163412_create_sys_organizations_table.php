<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sysOrganizations', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->integer('user_id')->index();
            $table->char('type',1)->default('P'); // (P)rofessional, (C)ompany

            $table->timestamps();
            $table->char('is_enabled',1)->default('Y')->index();
            $table->integer('created_by')->unsigned()->nullable();
      			$table->integer('modified_by')->unsigned()->nullable();
      			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sysOrganizations');
    }
}
