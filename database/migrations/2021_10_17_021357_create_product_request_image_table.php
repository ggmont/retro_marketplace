<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductRequestImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_request_image', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("inventory_id")->unsigned()->nullable();

            $table->foreign('inventory_id')->references('id')
                                       ->on('product_request')
                                       ->onDelete('cascade');

            $table->string('image_path');

            $table->string('created_by')->nullable();
            $table->string('modified_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_request_image');
    }
}
