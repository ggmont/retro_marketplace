<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppOrgPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appOrgPages', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('parent_id')->nullable()->index();
            $table->string('title',100)->unique();
            $table->string('url',100)->unique();
            $table->text('content')->nullable();
            $table->char('show_title',1)->default('Y');
            $table->char('is_enabled',1)->default('Y');

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('modified_by')->unsigned()->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appOrgPages');
    }
}
