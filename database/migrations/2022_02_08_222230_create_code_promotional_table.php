<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCodePromotionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('code_promotional', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id")->unsigned()->nullable();

            $table->foreign('user_id')->references('id')
                                       ->on('sysUsers')
                                       ->onDelete('cascade');
            $table->string('type')->nullable(); 
            $table->string('code')->nullable();
            $table->string('special')->nullable();  
            $table->string('is_enabled')->nullable();

            $table->string('prueba_id')->nullable();

             
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('code_promotional');
    }
}
