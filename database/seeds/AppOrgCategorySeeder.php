<?php

use Illuminate\Database\Seeder;
use App\AppOrgCategory;

class AppOrgCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('appOrgCategories')->delete();

      DB::statement('ALTER TABLE appOrgCategories AUTO_INCREMENT = 1;');

      AppOrgCategory::create(['name' => 'Juegos', 'parent_id' => 0, 'category_text' => 'Juegos','hierarchy' => '0','order_text' => '1','children' => '4', 'order_by' => 1]);
      AppOrgCategory::create(['name' => 'Consolas', 'parent_id' => 0, 'category_text' => 'Consolas','hierarchy' => '0','order_text' => '2','children' => '5' , 'order_by' => 2]);
      AppOrgCategory::create(['name' => 'Periféricos', 'parent_id' => 0, 'category_text' => 'Periféricos','hierarchy' => '0','order_text' => '3','children' => '7' , 'order_by' => 3]);
      AppOrgCategory::create(['name' => 'Accesorios', 'parent_id' => 0, 'category_text' => 'Accesorios','hierarchy' => '0','order_text' => '4','children' => '5' , 'order_by' => 4]);
      AppOrgCategory::create(['name' => 'PAL', 'parent_id' => 1, 'category_text' => 'Juegos -> PAL','hierarchy' => '1','order_text' => '1//5','children' => '0']);
      AppOrgCategory::create(['name' => 'NTFS', 'parent_id' => 1, 'category_text' => 'Juegos -> NTFS','hierarchy' => '1','order_text' => '1//6','children' => '0']);
      AppOrgCategory::create(['name' => 'JAP', 'parent_id' => 1, 'category_text' => 'Juegos -> JAP','hierarchy' => '1','order_text' => '1//7','children' => '0']);
      AppOrgCategory::create(['name' => 'Otros juegos', 'parent_id' => 1, 'category_text' => 'Juegos -> Otros juegos','hierarchy' => '1','order_text' => '1//8','children' => '0']);

      AppOrgCategory::create(['name' => 'Otras consolas', 'parent_id' => 2, 'category_text' => 'Consolas -> Otras consolas','hierarchy' => '1','order_text' => '2//13','children' => '0']);
      AppOrgCategory::create(['name' => 'Mandos', 'parent_id' => 3, 'category_text' => 'Periféricos -> Mandos','hierarchy' => '1','order_text' => '3//14','children' => '0']);
      AppOrgCategory::create(['name' => 'Micrófonos', 'parent_id' => 3, 'category_text' => 'Periféricos -> Micrófonos','hierarchy' => '1','order_text' => '3//15','children' => '0']);
      AppOrgCategory::create(['name' => 'Joystick', 'parent_id' => 3, 'category_text' => 'Periféricos -> Joystick','hierarchy' => '1','order_text' => '3//16','children' => '0']);
      AppOrgCategory::create(['name' => 'Teclados', 'parent_id' => 3, 'category_text' => 'Periféricos -> Teclados','hierarchy' => '1','order_text' => '3//17','children' => '0']);
      AppOrgCategory::create(['name' => 'Cámaras', 'parent_id' => 3, 'category_text' => 'Periféricos -> Cámaras','hierarchy' => '1','order_text' => '3//18','children' => '0']);
      AppOrgCategory::create(['name' => 'Memoria', 'parent_id' => 3, 'category_text' => 'Periféricos -> Memoria','hierarchy' => '1','order_text' => '3//19','children' => '0']);
      AppOrgCategory::create(['name' => 'Otros periféricos', 'parent_id' => 3, 'category_text' => 'Periféricos -> Otros periféricos','hierarchy' => '1','order_text' => '3//20','children' => '0']);
      AppOrgCategory::create(['name' => 'Fundas', 'parent_id' => 4, 'category_text' => 'Accesorios -> Fundas','hierarchy' => '1','order_text' => '4//21','children' => '0']);
      AppOrgCategory::create(['name' => 'Cables', 'parent_id' => 4, 'category_text' => 'Accesorios -> Cables','hierarchy' => '1','order_text' => '4//22','children' => '0']);
      AppOrgCategory::create(['name' => 'Cargadores', 'parent_id' => 4, 'category_text' => 'Accesorios -> Cargadores','hierarchy' => '1','order_text' => '4//23','children' => '0']);
      AppOrgCategory::create(['name' => 'Copias de seguridad', 'parent_id' => 4, 'category_text' => 'Accesorios -> Copias de seguridad','hierarchy' => '1','order_text' => '4//24','children' => '0']);
      AppOrgCategory::create(['name' => 'Otros accesorios', 'parent_id' => 4, 'category_text' => 'Accesorios -> Otros accesorios','hierarchy' => '1','order_text' => '4//25','children' => '0']);
      AppOrgCategory::create(['name' => 'Primera Generación', 'parent_id' => 2, 'category_text' => 'Consolas -> Primera Generación','hierarchy' => '1','order_text' => '2//26','children' => '0']);
      AppOrgCategory::create(['name' => 'Segunda Generación', 'parent_id' => 2, 'category_text' => 'Consolas -> Segunda Generación','hierarchy' => '1','order_text' => '2//27','children' => '0']);
      AppOrgCategory::create(['name' => 'Tercera Generación', 'parent_id' => 2, 'category_text' => 'Consolas -> Tercera Generación','hierarchy' => '1','order_text' => '2//28','children' => '0']);
      AppOrgCategory::create(['name' => 'Cuarta Generación', 'parent_id' => 2, 'category_text' => 'Consolas -> Cuarta Generación','hierarchy' => '1','order_text' => '2//29','children' => '0']);
      AppOrgCategory::create(['name' => 'Quinta Generación', 'parent_id' => 2, 'category_text' => 'Consolas -> Quinta Generación','hierarchy' => '1','order_text' => '2//30','children' => '0']);
      AppOrgCategory::create(['name' => 'Otros', 'parent_id' => 0, 'category_text' => 'Otros','hierarchy' => '0','order_text' => '5','children' => '0','order_by' => 5]);
    }
}
