<?php

use Illuminate\Database\Seeder;

use App\AppMessage;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AppMessage::create([
        	'from_id' => 2,
            'to_id' => 5,
            'content' => 'Hola, cómo estás?' 
        ]);
        AppMessage::create([
        	'from_id' => 5,
            'to_id' => 2,
            'content' => 'Bien, gracias. Y tú?' 
        ]);

        AppMessage::create([
            'from_id' => 2,
            'to_id' => 3,
            'content' => 'Hola, a los tiempos' 
        ]);
        AppMessage::create([
            'from_id' => 3,
            'to_id' => 2,
            'content' => 'Qué tal!' 
        ]);
    }
}
