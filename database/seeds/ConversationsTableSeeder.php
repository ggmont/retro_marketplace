<?php

use Illuminate\Database\Seeder;

use App\AppConversation;

class ConversationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AppConversation::create([
        	'user_id' => 2,
            'contact_id' => 5,
            'last_message' => null,
            'last_time' => null,
            'master_conversation' => true,

            // 'listen_notifications' => ,
            // 'has_blocked' => ,
        ]);
        AppConversation::create([
            'user_id' => 2,
            'contact_id' => 3,
            'last_message' => null,
            'last_time' => null,
            'master_conversation' => true,
        ]);

        AppConversation::create([
        	'user_id' => 5,
            'contact_id' => 2,

            'last_message' => null,
            'last_time' => null
        ]);

        AppConversation::create([
            'user_id' => 3,
            'contact_id' => 2,

            'last_message' => null,
            'last_time' => null
        ]);
    }
}
