<?php

use App\SysDictionary;
use Illuminate\Database\Seeder;

class SysDictionarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Dictionaries
      DB::table('sysDictionaries')->delete();

      DB::statement('ALTER TABLE sysDictionaries AUTO_INCREMENT = 1;');

      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'ANDROID', 'value' => 'Android']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'DCAST', 'value' => 'Dreamcast']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'GCUBE', 'value' => 'GameCube']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'IOS', 'value' => 'iOS devices']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'N64', 'value' => 'Nintendo 64']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'NSWITCH', 'value' => 'Nintendo Switch']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'PC', 'value' => 'PC']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'PS', 'value' => 'PlayStation']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'PS2', 'value' => 'PlayStation 2']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'PS3', 'value' => 'PlayStation 3']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'PS4', 'value' => 'PlayStation 4']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'PS4PRO', 'value' => 'PlayStation 4 Pro']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'WII', 'value' => 'Wii']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'WIIU', 'value' => 'Wii U']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'XBOX', 'value' => 'Xbox']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'XBOX360', 'value' => 'Xbox 360']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'XBOXONE', 'value' => 'Xbox One']);
      SysDictionary::create(['code' => 'GAME_PLATFORM', 'value_id' => 'XBOXONES', 'value' => 'Xbox One S']);

      SysDictionary::create(['code' => 'GAME_REGION', 'value_id' => 'NTSC-J', 'value' => 'NTSC-J']);
      SysDictionary::create(['code' => 'GAME_REGION', 'value_id' => 'NTSC-U', 'value' => 'NTSC-U']);
      SysDictionary::create(['code' => 'GAME_REGION', 'value_id' => 'PAL', 'value' => 'PAL region']);
      SysDictionary::create(['code' => 'GAME_REGION', 'value_id' => 'NTSC-C', 'value' => 'NTSC-C']);
      SysDictionary::create(['code' => 'GAME_REGION', 'value_id' => 'NTSC-M', 'value' => 'NTSC-M']);
      SysDictionary::create(['code' => 'GAME_REGION', 'value_id' => 'FREE', 'value' => 'Free']);

      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'ACTION', 'value' => 'ACTION']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'ADVENTURE', 'value' => 'ADVENTURE']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'ANIMAL', 'value' => 'ANIMAL']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'ART_CREAT', 'value' => 'ART & CREATIVITY']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'BOARDCARD', 'value' => 'BOARD & CARD']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'CASINO', 'value' => 'CASINO GAMES']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'GIRLS', 'value' => 'GIRLS']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'MOBILE', 'value' => 'MOBILE']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'MULTIP', 'value' => 'MULTIPLAYER']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'PUZZ', 'value' => 'PUZZLE']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'RACING', 'value' => 'RACING']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'SHOOTING', 'value' => 'SHOOTING GAMES']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'SIMULAT', 'value' => 'SIMULATION']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'SKILL', 'value' => 'SKILL']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'SPECIALS', 'value' => 'SPECIALS']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'SPORTS', 'value' => 'SPORTS']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'STRATEGY', 'value' => 'STRATEGY']);
      SysDictionary::create(['code' => 'GAME_CATEGORY', 'value_id' => 'VEHICLES', 'value' => 'VEHICLES']);

      SysDictionary::create(['code' => 'GAME_STATE', 'value_id' => 'NEW', 'value' => 'Nuevo', 'order_by' => 0]);
      SysDictionary::create(['code' => 'GAME_STATE', 'value_id' => 'USED-NEW', 'value' => 'Usado como nuevo', 'order_by' => 1]);
      SysDictionary::create(['code' => 'GAME_STATE', 'value_id' => 'USED', 'value' => 'Usado', 'order_by' => 2]);
      SysDictionary::create(['code' => 'GAME_STATE', 'value_id' => 'USED-VERY', 'value' => 'Muy usado', 'order_by' => 3]);
      SysDictionary::create(['code' => 'GAME_STATE', 'value_id' => 'NOT-WORK', 'value' => 'No funciona', 'order_by' => 4]);
      SysDictionary::create(['code' => 'GAME_STATE', 'value_id' => 'NOT-PRES', 'value' => 'No aplica', 'order_by' => 5]);

      SysDictionary::create(['code' => 'ORDER_STATUS', 'value_id' => 'CR', 'value' => 'Created']);
      SysDictionary::create(['code' => 'ORDER_STATUS', 'value_id' => 'PN', 'value' => 'Pending']);
      SysDictionary::create(['code' => 'ORDER_STATUS', 'value_id' => 'CN', 'value' => 'Cancelled']);
      SysDictionary::create(['code' => 'ORDER_STATUS', 'value_id' => 'OH', 'value' => 'On Hold']);
      SysDictionary::create(['code' => 'ORDER_STATUS', 'value_id' => 'DR', 'value' => 'Delivered']);
      SysDictionary::create(['code' => 'ORDER_STATUS', 'value_id' => 'LT', 'value' => 'Lost']);

      SysDictionary::create(['code' => 'ORDER_ISTATUS', 'value_id' => 'RN', 'value' => 'Returned']);
      SysDictionary::create(['code' => 'ORDER_ISTATUS', 'value_id' => 'DR', 'value' => 'Delivered']);
      SysDictionary::create(['code' => 'ORDER_ISTATUS', 'value_id' => 'LT', 'value' => 'Lost']);

      SysDictionary::create(['code' => 'ORDER_ITYPE', 'value_id' => 'PR', 'value' => 'Product']);
      SysDictionary::create(['code' => 'ORDER_ITYPE', 'value_id' => 'RV', 'value' => 'Reversion']);
      SysDictionary::create(['code' => 'ORDER_ITYPE', 'value_id' => 'DS', 'value' => 'Discount']);

      SysDictionary::create(['code' => 'FOOTER_COL', 'value_id' => 'COL1', 'value' => 'Interest']);
      SysDictionary::create(['code' => 'FOOTER_COL', 'value_id' => 'COL2', 'value' => 'Community']);
      SysDictionary::create(['code' => 'FOOTER_COL', 'value_id' => 'COL3', 'value' => 'Help']);
      SysDictionary::create(['code' => 'FOOTER_COL', 'value_id' => 'COL4', 'value' => 'Another stuff']);

      SysDictionary::create(['code' => 'CAROUSEL_LOC', 'value_id' => 'CRS_MAIN', 'value' => 'Main carousel']);
      SysDictionary::create(['code' => 'CAROUSEL_LOC', 'value_id' => 'CRS_PG1', 'value' => 'Carousel for page 1']);
      SysDictionary::create(['code' => 'CAROUSEL_LOC', 'value_id' => 'CRS_PG2', 'value' => 'Carousel for page 2']);
      SysDictionary::create(['code' => 'CAROUSEL_LOC', 'value_id' => 'CRS_PG3', 'value' => 'Carousel for page 3']);

      SysDictionary::create(['code' => 'CAROUSEL_WIDTH', 'value_id' => 'CRS_WBOX', 'value' => 'Box']);
      SysDictionary::create(['code' => 'CAROUSEL_WIDTH', 'value_id' => 'CRS_WFULL', 'value' => 'Full']);

      SysDictionary::create(['code' => 'CAROUSEL_HEIGHT', 'value_id' => 'CRS_HAU', 'value' => 'Automatic']);
      SysDictionary::create(['code' => 'CAROUSEL_HEIGHT', 'value_id' => 'CRS_HFIX', 'value' => 'Fixed']);

      SysDictionary::create(['code' => 'BANNER_LOC', 'value_id' => 'BNR_MAIN_1', 'value' => 'Main Banner 1', 'order_by' => 1]);
      SysDictionary::create(['code' => 'BANNER_LOC', 'value_id' => 'BNR_MAIN_2', 'value' => 'Main Banner 2', 'order_by' => 2]);
      SysDictionary::create(['code' => 'BANNER_LOC', 'value_id' => 'BNR_PG1', 'value' => 'Banner for page 1', 'order_by' => 3]);
      SysDictionary::create(['code' => 'BANNER_LOC', 'value_id' => 'BNR_PG2', 'value' => 'Banner for page 2', 'order_by' => 4]);
      SysDictionary::create(['code' => 'BANNER_LOC', 'value_id' => 'BNR_PG3', 'value' => 'Banner for page 3', 'order_by' => 5]);

      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'ATAR', 'value' => 'Atari']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'SEGA', 'value' => 'Sega']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'SONY', 'value' => 'Sony']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'NINT', 'value' => 'Nintendo']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'MICR', 'value' => 'Microsoft']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'COLE', 'value' => 'Coleco']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'MATT', 'value' => 'Mattel']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'RADI', 'value' => 'Radio Shack']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'GTE ', 'value' => 'GTE Sylvana']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'INTV', 'value' => 'INTV. Corp']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'SEAR', 'value' => 'Sears']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'BAND', 'value' => 'Banday']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'TIGE', 'value' => 'Tiger Electronics']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'CASI', 'value' => 'Casio']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'EPOC', 'value' => 'Epoch']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'FAIR', 'value' => 'Fairchild Semi.']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'TAPW', 'value' => 'Tapwave']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'SUPE', 'value' => 'Supervision']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'PANA', 'value' => 'Panasonic']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'ARCA', 'value' => 'Arcadia Corp.']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'NAMC', 'value' => 'Namco Bandai']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'MAGN', 'value' => 'Magnavox']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'NEC', 'value' => 'NEC']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'PHIL', 'value' => 'Philips']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'APPL', 'value' => 'Apple']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'GAME', 'value' => 'Game Park']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'COMM', 'value' => 'Commodore']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'SMIT', 'value' => 'Smith Engineering']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'SNK', 'value' => 'SNK']);
      SysDictionary::create(['code' => 'GAME_PRODUCER', 'value_id' => 'NOKI', 'value' => 'Nokia']);

      SysDictionary::create(['code' => 'GAME_GENERATION', 'value_id' => 'PRIGEM', 'value' => 'Primera Generación', 'order_by' => 0]);
      SysDictionary::create(['code' => 'GAME_GENERATION', 'value_id' => 'SEGGEN', 'value' => 'Segunda Generación', 'order_by' => 1]);
      SysDictionary::create(['code' => 'GAME_GENERATION', 'value_id' => 'TERGEN', 'value' => 'Tercera  Generación', 'order_by' => 2]);
      SysDictionary::create(['code' => 'GAME_GENERATION', 'value_id' => 'CUAGEN', 'value' => 'Cuarta  Generación', 'order_by' => 3]);
      SysDictionary::create(['code' => 'GAME_GENERATION', 'value_id' => 'QUIGEN', 'value' => 'Quinta  Generación', 'order_by' => 4]);
      SysDictionary::create(['code' => 'GAME_GENERATION', 'value_id' => 'SEXGEN', 'value' => 'Sexta  Generación', 'order_by' => 5]);
      SysDictionary::create(['code' => 'GAME_GENERATION', 'value_id' => 'SEPGEN', 'value' => 'Séptima Generación', 'order_by' => 6]);
      SysDictionary::create(['code' => 'GAME_GENERATION', 'value_id' => 'OCTGEN', 'value' => 'Octava Generación', 'order_by' => 7]);

      SysDictionary::create(['code' => 'GAME_LANGUAGE', 'value_id' => 'ESP', 'value' => 'Castellano','order_by' => 0]);
      SysDictionary::create(['code' => 'GAME_LANGUAGE', 'value_id' => 'JAP', 'value' => 'Japonés','order_by' => 1]);
      SysDictionary::create(['code' => 'GAME_LANGUAGE', 'value_id' => 'MULTI5', 'value' => 'Multi-5','order_by' => 2]);
      SysDictionary::create(['code' => 'GAME_LANGUAGE', 'value_id' => 'ENG', 'value' => 'Inglés','order_by' => 3]);

      SysDictionary::create(['code' => 'GAME_SUPPORT', 'value_id' => 'CART', 'value' => 'Cartucho','order_by' => 0]);
      SysDictionary::create(['code' => 'GAME_SUPPORT', 'value_id' => 'CD', 'value' => 'CD','order_by' => 1]);
      SysDictionary::create(['code' => 'GAME_SUPPORT', 'value_id' => 'BRAY', 'value' => 'Blue-ray','order_by' => 2]);
      SysDictionary::create(['code' => 'GAME_SUPPORT', 'value_id' => 'CASS', 'value' => 'Cassette','order_by' => 3]);
      SysDictionary::create(['code' => 'GAME_SUPPORT', 'value_id' => 'ONL', 'value' => 'Online','order_by' => 4]);
      SysDictionary::create(['code' => 'GAME_SUPPORT', 'value_id' => 'INTG', 'value' => 'Integrado','order_by' => 5]);
      SysDictionary::create(['code' => 'GAME_SUPPORT', 'value_id' => 'OTROS', 'value' => 'Otros','order_by' => 6]);

      SysDictionary::create(['code' => 'GAME_LOCATION', 'value_id' => 'C-EU', 'value' => 'Europa', 'order_by' => 0]);
      SysDictionary::create(['code' => 'GAME_LOCATION', 'value_id' => 'C-AM', 'value' => 'Estados Unidos', 'order_by' => 1]);
      SysDictionary::create(['code' => 'GAME_LOCATION', 'value_id' => 'JP', 'value' => 'Japón', 'order_by' => 2]);
      SysDictionary::create(['code' => 'GAME_LOCATION', 'value_id' => 'KP', 'value' => 'Korea del Norte', 'order_by' => 3]);
      SysDictionary::create(['code' => 'GAME_LOCATION', 'value_id' => 'KR', 'value' => 'Korea del Sur', 'order_by' => 4]);
    }
}
