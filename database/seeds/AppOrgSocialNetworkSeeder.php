<?php

use App\AppOrgSocialNetwork;
use Illuminate\Database\Seeder;

class AppOrgSocialNetworkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('appOrgSocialNetworks')->delete();

      DB::statement('ALTER TABLE appOrgSocialNetworks AUTO_INCREMENT = 1;');

      AppOrgSocialNetwork::create(['name' => 'Twitter',
          'html' => '<a href="https://twitter.com" target="_blank"><i class="fa fa-twitter-square"></i></a>',
          'long_html' => '<a class="twitter-timeline"
           href="https://twitter.com/TwitterDev?ref_src=twsrc%5Etfw"
           data-tweet-limit="1"
           data-height="200"
           data-chrome="noheader, nofooter, noborders, noscrollbar"
           >Tweets by TwitterDev</a>
        <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>']);

      AppOrgSocialNetwork::create(['name' => 'Facebook',
          'html' => '<a href="https://twitter.com" target="_blank"><i class="fa fa-facebook-square"></i></a>',
          'long_html' => '<a class="twitter-timeline"
           href="https://twitter.com/TwitterDev?ref_src=twsrc%5Etfw"
           data-tweet-limit="1"
           data-height="200"
           data-chrome="noheader, nofooter, noborders, noscrollbar"
           >Tweets by TwitterDev</a>
        <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>']);

      AppOrgSocialNetwork::create(['name' => 'Instagram',
          'html' => '<a href="https://twitter.com" target="_blank"><i class="fa fa-instagram"></i></a>',
          'long_html' => '<a class="twitter-timeline"
           href="https://twitter.com/TwitterDev?ref_src=twsrc%5Etfw"
           data-tweet-limit="1"
           data-height="200"
           data-chrome="noheader, nofooter, noborders, noscrollbar"
           >Tweets by TwitterDev</a>
        <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>']);

      AppOrgSocialNetwork::create(['name' => 'Youtube',
          'html' => '<a href="https://twitter.com" target="_blank"><i class="fa fa-youtube-play"></i></a>',
          'long_html' => '<a class="twitter-timeline"
           href="https://twitter.com/TwitterDev?ref_src=twsrc%5Etfw"
           data-tweet-limit="1"
           data-height="200"
           data-chrome="noheader, nofooter, noborders, noscrollbar"
           >Tweets by TwitterDev</a>
        <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>']);
    }
}
