<?php

use Illuminate\Database\Seeder;
use App\AppOrgPage;

class AppOrgPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('appOrgPages')->delete();

      DB::statement('ALTER TABLE appOrgPages AUTO_INCREMENT = 1;');

      AppOrgPage::create(['title' => 'Terminos y condiciones', 'url' => 'terminos-y-condiciones',
        'content' => '<div class="col-xs-12 col-sm-9 col-md-10">
        <div class="section_page section_page_15"><font size="4">1. ¿Quiénes somos?
        </font><p>www.paginaweb.com es un intermediario entre personas que quieren comprar o vender videojuegos y/o otros accesorios relacionados con la indústria del videojuego.<br></p>
        <h3 style="font-size: 18px; line-height: 2;">2. ¿Cómo comprar?</h3>
        <p>Para comprar en www.paginaweb.com debes estar registrado correctamente.<br></p>
        <h3 style="font-size: 18px; line-height: 2;">3. ¿Cómo vender?</h3>
        <p><br></p>
        <h3 style="font-size: 18px; line-height: 2;">4. ¿Cómo pagar?</h3>
        <p><br></p>
        <h3 style="font-size: 18px; line-height: 2;">5. ¿Cómo y cuándo lo recibo?</h3>
        <p><br></p>
        <h3 style="font-size: 18px; line-height: 2;">6. ¿Cuánto cuesta el envío?</h3>
        <p>Los gastos de envío son determinados por cada uno de los vendedores,
        según el método de entrega y el peso del producto. Los gastos estarán
        especificados en pantalla en el momento de realizar la compra.</p>
        <h3 style="font-size: 18px; line-height: 2;">7. ¿Y si me arrepiento de la compra?</h3>
        <p><br></p>
        <h3 style="font-size: 18px; line-height: 2;">8. Normas de Conducta</h3>
        <p><br></p>
        <h3 style="font-size: 18px; line-height: 2;">9. Información General</h3>
        <p><br></p>
        <h3 style="font-size: 18px; line-height: 2;">10. Menores de Edad</h3>
        <p><br></p>
        <h3 style="font-size: 18px; line-height: 2;">11. Protección de datos</h3>
        <p><br></p>
        <h3 style="font-size: 18px; line-height: 2;">12. Ley Aplicable y Jurisdicción</h3><br></div></div><blockquote><sub></sub></blockquote>',
        'show_title' => 'Y',
      ]);

      AppOrgPage::create(['title' => 'Aviso Legal', 'url' => 'aviso-legal',
        'content' => '<p><strong>1. Introducción</strong></p>

          <p>Las presentes condiciones generales de uso de la página web, regulan
          los términos y condiciones de acceso y uso de <font color="#FF0000">www.direcciónweb.com,</font>
          propiedad de <font color="#FF0000">(indicar el nombre de la empresa o profesional propietario
          de el portal)</font>, con domicilio en <font color="#FF0000">(indicar)</font> y con Código de Identificación
           Fiscal número <font color="#FF0000">(indicar),</font> en adelante, «la Empresa», que el usuario del
          Portal deberá de leer y aceptar para usar todos los servicios e
          información que se facilitan desde el portal.&nbsp;El mero acceso y/o
          utilización del portal, de todos o parte de sus contenidos y/o servicios
           significa la plena aceptación de las presentes condiciones generales de
           uso.&nbsp;</p>

          <p><strong>2. Condiciones de uso</strong></p>

          <p>Las presentes condiciones generales de uso del portal regulan el
          acceso y la utilización del portal, incluyendo los contenidos y los
          servicios puestos a disposición de los usuarios en y/o a través del
          portal, bien por el portal, bien por sus usuarios, bien por terceros. No
           obstante, el acceso y la utilización de ciertos contenidos y/o
          servicios puede encontrarse sometido a determinadas condiciones
          específicas.</p>

          <p><strong>3.&nbsp;&nbsp; Modificaciones</strong></p>

          <p>La empresa se reserva la facultad de modificar en cualquier momento
          las condiciones generales de uso del portal. En todo caso, se recomienda
           que consulte periódicamente los presentes&nbsp;términos de uso del portal,
          ya que pueden ser modificados.</p>

          <p><strong>4. Obligaciones del Usuario</strong></p>

          <p>El usuario deberá respetar en todo momento los términos y condiciones
           establecidos en las presentes condiciones generales de uso del portal.
          De forma expresa el usuario manifiesta que utilizará el portal de forma
          diligente y asumiendo cualquier responsabilidad que pudiera derivarse
          del incumplimiento de las normas.</p>

          <p>Así mismo, el usuario no podrá utilizar el portal para transmitir,
          almacenar, divulgar promover o distribuir datos o contenidos que sean
          portadores de virus o cualquier otro código informático, archivos o
          programas diseñados para interrumpir, destruir o perjudicar el
          funcionamiento de cualquier programa o equipo informático o de
          telecomunicaciones.</p>

          <p><strong>5. Responsabilidad del portal</strong></p>

          <p>El usuario conoce y acepta que el portal no otorga ninguna garantía
          de cualquier naturaleza, ya sea expresa o implícita, sobre los datos,
          contenidos, información y servicios que se incorporan y ofrecen desde el
           Portal.</p>

          <p>Exceptuando los casos que la Ley imponga expresamente lo contrario, y
           exclusivamente con la medida y extensión en que lo imponga, el Portal
          no garantiza ni asume responsabilidad alguna respecto a los posibles
          daños y perjuicios causados por el uso y utilización de la información,
          datos y servicios del Portal.</p>

          <p>En todo caso, el Portal excluye cualquier responsabilidad por los
          daños y perjuicios que puedan deberse a la información y/o servicios
          prestados o suministrados por terceros diferentes de la Empresa. Toda
          responsabilidad será del tercero ya sea proveedor o&nbsp;colaborador.</p>

          <p><strong>6. Propiedad intelectual e industrial</strong></p>

          <p>Todos los contenidos, marcas, logos, dibujos, documentación,
          programas informáticos o cualquier otro elemento susceptible de
          protección por la legislación de propiedad intelectual o industrial, que
           sean accesibles en el portal corresponden exclusivamente a la empresa o
           a sus legítimos titulares y quedan expresamente reservados todos los
          derechos sobre los mismos. Queda expresamente prohibida la creación de
          enlaces de hipertexto (links) a cualquier elemento integrante de las
          páginas web del Portal sin la autorización de la empresa, siempre que no
           sean a una página web del Portal que no requiera identificación o
          autenticación para su acceso, o el mismo esté restringido.</p>

          <p>En cualquier caso, el portal se reserva todos los derechos sobre los
          contenidos, información datos y servicios que ostente sobre los mismos.
          El portal no concede ninguna licencia o autorización de uso al usuario
          sobre sus contenidos, datos o servicios, distinta de la que expresamente
           se detalle en las presentes condiciones generales de uso del portal.</p>

          <p><strong>7. Legislación aplicable, jurisdicción competente y notificaciones</strong></p>

          <p>Las presentes condiciones se rigen y se interpretan de acuerdo con
          las Leyes de España. Para cualquier reclamación serán competentes los
          juzgados y tribunales de (indicar la ciudad). Todas las notificaciones,
          requerimientos, peticiones y otras comunicaciones que el Usuario desee
          efectuar a la Empresa titular del Portal deberán realizarse por escrito y
           se entenderá que han sido correctamente realizadas cuando hayan sido
          recibidas en la siguiente dirección <font color="#FF0000">(indicar dirección de correo en la
          que se desean recibir las notificaciones).</font></p>

          <br>',
        'show_title' => 'Y',
      ]);

      AppOrgPage::create(['title' => 'Política de Privacidad', 'url' => 'politica-de-privacidad',
        'content' => '', 'show_title' => 'Y',
      ]);

      AppOrgPage::create(['title' => 'Política de Cookies', 'url' => 'politica-de-cookies',
        'content' => '<p>INTRODUCCIÓN</p>

        <p>Mediante el presente aviso de la web …........ con titular …... le
        informa de su política de cookies, cumpliendo así con lo previsto en el
        artículo 22.2 de la Ley 34/2002, de 11 de julio de Servicios de la
        Sociedad de la Información y de Comercio Electrónico.</p>

        <p>QUÉ ES UNA COOKIE</p>

        <p>Cookie es un fichero que se descarga en su ordenador al acceder a
        determinadas páginas web. Las cookies permiten a una página web, entre
        otras cosas, almacenar y recuperar información sobre los hábitos de
        navegación de un usuario o de su equipo y, dependiendo de la información
         que contengan y de la forma en que utilice su equipo, pueden utilizarse
         para reconocer al usuario. El navegador del usuario memoriza cookies en
         el disco duro solamente durante la sesión actual ocupando un espacio de
         memoria mínimo y no perjudicando su ordenador. Las cookies no contienen
         ninguna clase de información personal específica, y la mayoría de las
        mismas se borran del disco duro al finalizar la sesión de navegador.</p>

        <p>La mayoría de los navegadores aceptan como estándar a las cookies y, con independencia de</p>

        <p>las mismas, permiten o impiden en los ajustes de seguridad las cookies temporales o</p>

        <p>memorizadas.</p>

        <p>Sin su consentimiento mediante la activación de las cookies en su
        navegador, no se enlazará en las cookies los datos memorizados con sus
        datos personales proporcionados en el momento del registro o la compra.</p>

        <p>El Usuario acepta expresamente, por la utilización de este portal, el
         tratamiento de la información recabada en la forma y con los fines
        anteriormente mencionados. Y asimismo reconoce conocer la posibilidad de
         rechazar el tratamiento de tales datos o información rechazando el uso
        de Cookies mediante la selección de la configuración apropiada a tal fin
         en su navegador. Si bien esta opción de bloqueo de Cookies en su
        navegador puede no permitirle el uso pleno de todas las funcionalidades
        de la web.</p>

        <p>Puede usted permitir, bloquear o eliminar las cookies instaladas en
        su equipo mediante la configuración de las opciones del navegador
        instalado en su ordenador.</p>',
        'show_title' => 'Y',
      ]);

    }
}
