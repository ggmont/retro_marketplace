<?php

use Illuminate\Database\Seeder;
use App\AppOrgUserInventory;

class AppOrgUserInventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('appOrgUserInventories')->delete();

  		DB::statement('ALTER TABLE appOrgUserInventories AUTO_INCREMENT = 1;');

     AppOrgUserInventory::create(['user_id' => 2, 'product_id' => 9, 'box_condition' => 'NEW', 'manual_condition' => 'NEW', 'cover_condition' => 'NEW', 'game_condition' => 'NEW', 'extra_condition' => 'NOT-WORK', 'location' => '', 'media' => '' , 'quantity' => 23, 'quantity_sold' => 0, 'price' => 232, 'comments'  => 'asd fasd fasdf  fds', 'status' => 'A', 'is_enabled' => 'Y']);
     AppOrgUserInventory::create(['user_id' => 2, 'product_id' => 7, 'box_condition' => 'USED', 'manual_condition' => 'NOT-PRES', 'cover_condition' => 'USED-NEW', 'game_condition' => 'USED-NEW', 'extra_condition' => 'USED-NEW', 'location' => '', 'media' => '' , 'quantity' => 1, 'quantity_sold' => 0, 'price' => 20, 'comments'  => 'Esta muy roto', 'status' => 'A', 'is_enabled' => 'Y']);
     AppOrgUserInventory::create(['user_id' => 3, 'product_id' => 10, 'box_condition' => 'NEW', 'manual_condition' => 'NEW', 'cover_condition' => 'NEW', 'game_condition' => 'NEW', 'extra_condition' => 'NEW', 'location' => '', 'media' => '' , 'quantity' => 25, 'quantity_sold' => 0, 'price' => 35, 'comments'  => ' adsfadsfas dff', 'status' => 'A', 'is_enabled' => 'Y']);
     AppOrgUserInventory::create(['user_id' => 3, 'product_id' => 7, 'box_condition' => 'USED-NEW', 'manual_condition' => 'NOT-PRES', 'cover_condition' => 'USED', 'game_condition' => 'USED-NEW', 'extra_condition' => 'USED-NEW', 'location' => '', 'media' => '' , 'quantity' => 23, 'quantity_sold' => 0, 'price' => 25, 'comments'  => 'asdf asdfas asdas asdf ', 'status' => 'A', 'is_enabled' => 'Y']);
     AppOrgUserInventory::create(['user_id' => 4, 'product_id' => 7, 'box_condition' => 'USED-VERY', 'manual_condition' => 'NOT-PRES', 'cover_condition' => 'USED-VERY', 'game_condition' => 'USED-VERY', 'extra_condition' => 'USED', 'location' => '', 'media' => '' , 'quantity' => 23, 'quantity_sold' => 0, 'price' => 43, 'comments'  => 'a daf adf asdf asdf a asdf asdfasf asf', 'status' => 'A', 'is_enabled' => 'Y']);
    }
}
