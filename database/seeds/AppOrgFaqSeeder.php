<?php

use Illuminate\Database\Seeder;
use App\AppOrgFaq;

class AppOrgFaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('appOrgFaqs')->delete();

      DB::statement('ALTER TABLE appOrgFaqs AUTO_INCREMENT = 1;');

      AppOrgFaq::create(['question' => '¿Qué es esta página?'
      ,'answer' => 'Esta página es un mercadillo digital donde los usuarios pueden comprar y/o vender videojuegos y consolas.<br>'
      ]);
      AppOrgFaq::create(['question' => '¿Cuánto cuesta vender/comprar?'
      ,'answer' => 'El registro y poner artículos a la venta en esta página es gratis. Sólo pagarás cuando vendas algo. Puedes ver el precio en el apartado "precios"<br><br>Comprar en esta página es GRATIS.<br>'
      ]);
      AppOrgFaq::create(['question' => '¿Cómo puedo poner un artículo en venta?'
      ,'answer' => 'Poner un artículo en venta es muy fácil. Busca tu artículo en nuestra base de datos, especifica su estado y precio y lístalo!<br>Ahora espera que alguien te lo compre!<br>'
      ]);
      
    }
}
