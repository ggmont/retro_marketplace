<?php

use App\AppOrgFooterLink;
use Illuminate\Database\Seeder;

class AppOrgFooterLinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('appOrgFooterLinks')->delete();

      DB::statement('ALTER TABLE appOrgFooterLinks AUTO_INCREMENT = 1;');

      AppOrgFooterLink::create(['column' => 'COL1', 'name' => 'CNN', 'url' => 'https://www.cnn.com']);
      AppOrgFooterLink::create(['column' => 'COL1', 'name' => 'NASA', 'url' => 'https://www.nasa.gov']);
      AppOrgFooterLink::create(['column' => 'COL1', 'name' => 'ESPN', 'url' => 'https://www.espn.com']);
      AppOrgFooterLink::create(['column' => 'COL1', 'name' => 'Prensa', 'url' => 'https://www.prensa.com']);

      AppOrgFooterLink::create(['column' => 'COL2', 'name' => 'Twitter', 'url' => 'https://www.google.com']);
      AppOrgFooterLink::create(['column' => 'COL2', 'name' => 'Facebook', 'url' => 'https://www.google.com']);
      AppOrgFooterLink::create(['column' => 'COL2', 'name' => 'Instagram', 'url' => 'https://www.google.com']);
      AppOrgFooterLink::create(['column' => 'COL2', 'name' => 'Tumblr', 'url' => 'https://www.google.com']);

      AppOrgFooterLink::create(['column' => 'COL3', 'name' => 'Contáctanos', 'url' => 'contact-us', 'new_page' => 'N']);

    }
}
