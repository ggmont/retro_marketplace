<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

use App\SysCompanies;
use App\SysDictionary;
use App\SysOrganization;
use App\SysPrivileges;
use App\SysRolePrivileges;
use App\SysRoles;
use App\AppOrgBanner;
use App\SysUser;
use App\SysAccountBank;
use App\SysUserRoles;
use App\SysFacilities;
use App\SysTrailers;
use App\SysStatuses;
use App\SysShipping;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Model::unguard();

		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		$this->call(SysCountrySeeder::class);
		$this->call(SysDictionarySeeder::class);
		$this->call(UsersTableSeeder::class);
		$this->call(AppOrgProductSeeder::class);
		$this->call(AppOrgUserInventorySeeder::class);
		$this->call(AppOrgCategorySeeder::class);
		$this->call(AppOrgSocialNetworkSeeder::class);
		$this->call(AppOrgFooterLinkSeeder::class);
		$this->call(AppOrgPageSeeder::class);
		$this->call(AppOrgCarouselSeeder::class);
		$this->call(AppOrgFaqSeeder::class);
		//$this->call(ConversationsTableSeeder::class);
		//$this->call(MessagesTableSeeder::class);

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }


}

class UsersTableSeeder extends Seeder {

	public function run() {

		DB::table('sysUsers')->delete();

		DB::statement('ALTER TABLE sysUsers AUTO_INCREMENT = 1;');

		SysUser::create([ 'email' => 'banco@market.com', 'profile_picture' => 'https://www.cobdoglaps.sa.edu.au/wp-content/uploads/2017/11/placeholder-profile-sq.jpg', 'password' => Hash::make('carro24'), 'first_name' => 'Banco', 'last_name' => 'of Application', 'user_name' => 'banco', 'is_enabled' => 'Y', 'is_activated' => 'Y', 'concept' => 'banco-banco']);
		SysUser::create([ 'email' => 'ptykamikaze@gmail.com', 'profile_picture' => 'https://www.cobdoglaps.sa.edu.au/wp-content/uploads/2017/11/placeholder-profile-sq.jpg', 'password' => Hash::make('carro24'), 'first_name' => 'Administrator', 'last_name' => 'of Application', 'user_name' => 'admin', 'is_enabled' => 'Y', 'is_activated' => 'Y', 'concept' => 'admin-123', 'amountEURO' => 1000]);
		SysUser::create([ 'email' => 'info@marketplace.app', 'profile_picture' => 'https://www.cobdoglaps.sa.edu.au/wp-content/uploads/2017/11/placeholder-profile-sq.jpg', 'password' => Hash::make('carro24'), 'first_name' => 'John', 'last_name' => 'Doe', 'user_name' => 'jdoe', 'is_enabled' => 'Y', 'is_activated' => 'Y', 'concept' => 'user-123']);
		SysUser::create([ 'email' => 'info2@marketplace.app', 'profile_picture' => 'https://www.cobdoglaps.sa.edu.au/wp-content/uploads/2017/11/placeholder-profile-sq.jpg', 'password' => Hash::make('carro24'), 'first_name' => 'Jane', 'last_name' => 'Doe', 'user_name' => 'jadoe', 'is_enabled' => 'Y', 'is_activated' => 'Y', 'concept' => 'user-456']);
		SysUser::create([ 'email' => 'info3@marketplace.app', 'profile_picture' => 'https://www.cobdoglaps.sa.edu.au/wp-content/uploads/2017/11/placeholder-profile-sq.jpg', 'password' => Hash::make('carro24'), 'first_name' => 'Michael', 'last_name' => 'Doe', 'user_name' => 'mdoe', 'is_enabled' => 'Y', 'is_activated' => 'Y', 'concept' => 'user-789']);

		DB::table('sysAccountBanks')->delete();

		DB::statement('ALTER TABLE sysAccountBanks AUTO_INCREMENT = 1;');
		SysAccountBank::create([ 'beneficiary' => 'BankPlace', 'iban_code' => 'DE00000000001111', 'bic_code' => 'COBA00011', 'bank_address' => 'CommerzBank', 'user_id' => 1]);
		SysAccountBank::create([ 'beneficiary' => 'Admin Sys', 'iban_code' => 'DE00000000000000', 'bic_code' => 'COBA00000', 'bank_address' => 'Commerz', 'user_id' => 2]);
		SysAccountBank::create([ 'beneficiary' => 'John Doe', 'iban_code' => 'DE00000000000001', 'bic_code' => 'COBA00001', 'bank_address' => 'Commerz2', 'user_id' => 3]);
		SysAccountBank::create([ 'beneficiary' => 'Jane Doe', 'iban_code' => 'DE00000000000002', 'bic_code' => 'COBA00001', 'bank_address' => 'Commerz3', 'user_id' => 4]);
		SysAccountBank::create([ 'beneficiary' => 'Michael Doe', 'iban_code' => 'DE00000000000003', 'bic_code' => 'COBA00001', 'bank_address' => 'Commerz4', 'user_id' => 5]);
		DB::table('sysPrivileges')->delete();

		DB::statement('ALTER TABLE sysPrivileges AUTO_INCREMENT = 1;');

    // Privileges for login
    SysPrivileges::create([ 'name' => 'Access to Web Admin', 'description' => 'Capability to access to the web admin', 'code' => 'WEBADMIN']);
    SysPrivileges::create([ 'name' => 'Access to Web Front end', 'description' => 'Capability to access to the web front end', 'code' => 'WEBFRONT']);

    SysPrivileges::create([ 'name' => 'Messaging', 'description' => 'Capability to send and receive messages', 'code' => 'CONFMESSAGE']);

    // Privileges companies and professional users
		SysPrivileges::create([ 'name' => 'My Store - Configuration', 'description' => 'Capability to view and modify the configuration', 'code' => 'UMYCONF']);
		SysPrivileges::create([ 'name' => 'My Store - Inventory', 'description' => 'Capability to view, add, modify or delete inventory', 'code' => 'UMYINVE']);
		SysPrivileges::create([ 'name' => 'My Store - Orders', 'description' => 'Capability to view, add, modify or delete orders', 'code' => 'UMYORDR']);
		SysPrivileges::create([ 'name' => 'My Store - Invoice', 'description' => 'Capability to view, add, modify or delete invoices', 'code' => 'UMYINVO']);

    // Privileges for buyers

    // Admin privileges
		SysPrivileges::create([ 'name' => 'Users - View List', 'description' => 'Capability to view the list of records', 'code' => 'CONFUSERS_VLT']);
		SysPrivileges::create([ 'name' => 'Users - View Detail', 'description' => 'Capability to view details of the records', 'code' => 'CONFUSERS_VDT']);
		SysPrivileges::create([ 'name' => 'Users - Add', 'description' => 'Capability to add new records', 'code' => 'CONFUSERS_ADD']);
		SysPrivileges::create([ 'name' => 'Users - Modify', 'description' => 'Capability to modify records', 'code' => 'CONFUSERS_MOD']);
		SysPrivileges::create([ 'name' => 'Users - Delete', 'description' => 'Capability to delete records', 'code' => 'CONFUSERS_DEL']);
		SysPrivileges::create([ 'name' => 'Users - Audit', 'description' => 'Capability to view changes done on records', 'code' => 'CONFUSERS_AUD']);

		SysPrivileges::create([ 'name' => 'Roles - View List', 'description' => 'Capability to view the list of records', 'code' => 'CONFROLES_VLT']);
		SysPrivileges::create([ 'name' => 'Roles - View Detail', 'description' => 'Capability to view details of the records', 'code' => 'CONFROLES_VDT']);
		SysPrivileges::create([ 'name' => 'Roles - Add', 'description' => 'Capability to add new records', 'code' => 'CONFROLES_ADD']);
		SysPrivileges::create([ 'name' => 'Roles - Modify', 'description' => 'Capability to modify records', 'code' => 'CONFROLES_MOD']);
		SysPrivileges::create([ 'name' => 'Roles - Delete', 'description' => 'Capability to delete records', 'code' => 'CONFROLES_DEL']);
		SysPrivileges::create([ 'name' => 'Roles - Audit', 'description' => 'Capability to view changes done on records', 'code' => 'CONFROLES_AUD']);

		SysPrivileges::create([ 'name' => 'Privileges - View List', 'description' => 'Capability to view the list of records', 'code' => 'CONFPRIV_VLT']);
		SysPrivileges::create([ 'name' => 'Privileges - View Detail', 'description' => 'Capability to view details of the records', 'code' => 'CONFPRIV_VDT']);
		SysPrivileges::create([ 'name' => 'Privileges - Add', 'description' => 'Capability to add new records', 'code' => 'CONFPRIV_ADD']);
		SysPrivileges::create([ 'name' => 'Privileges - Modify', 'description' => 'Capability to modify records', 'code' => 'CONFPRIV_MOD']);
		SysPrivileges::create([ 'name' => 'Privileges - Delete', 'description' => 'Capability to delete records', 'code' => 'CONFPRIV_DEL']);
		SysPrivileges::create([ 'name' => 'Privileges - Audit', 'description' => 'Capability to view changes done on records', 'code' => 'CONFPRIV_AUD']);

		SysPrivileges::create([ 'name' => 'Lists - View List', 'description' => 'Capability to view the lists', 'code' => 'CONFLIST_VLT']);
		SysPrivileges::create([ 'name' => 'Lists - View Detail', 'description' => 'Capability to view details of the lists', 'code' => 'CONFLIST_VLT']);
		SysPrivileges::create([ 'name' => 'Lists - Add', 'description' => 'Capability to add new records', 'code' => 'CONFLIST_ADD']);
		SysPrivileges::create([ 'name' => 'Lists - Modify', 'description' => 'Capability to modify records', 'code' => 'CONFLIST_MOD']);
		SysPrivileges::create([ 'name' => 'Lists - Delete', 'description' => 'Capability to delete records', 'code' => 'CONFLIST_DEL']);
		SysPrivileges::create([ 'name' => 'Lists - Audit', 'description' => 'Capability to view changes done on records', 'code' => 'CONFLIST_AUD']);

		SysPrivileges::create([ 'name' => 'Stripe - Modify', 'description' => 'Capability to modify stripe info', 'code' => 'CONFSTRI_MOD']);
		SysPrivileges::create([ 'name' => 'Stripe - Audit', 'description' => 'Capability to view changes done on records', 'code' => 'CONFSTRI_AUD']);

    SysPrivileges::create([ 'name' => 'Google Map - Modify', 'description' => 'Capability to modify google map info', 'code' => 'CONFGOMP_MOD']);
		SysPrivileges::create([ 'name' => 'Google Map- Audit', 'description' => 'Capability to view changes done on records', 'code' => 'CONFGOMP_AUD']);

    SysPrivileges::create([ 'name' => 'Market Place Categories - List', 'description' => 'Capability to view the categories', 'code' => 'MPCATG_VLT']);
    SysPrivileges::create([ 'name' => 'Market Place Categories - View Detail', 'description' => 'Capability to view details of the categories', 'code' => 'MPCATG_VDT']);
		SysPrivileges::create([ 'name' => 'Market Place Categories - Add', 'description' => 'Capability to add new records', 'code' => 'MPCATG_ADD']);
		SysPrivileges::create([ 'name' => 'Market Place Categories - Modify', 'description' => 'Capability to modify records', 'code' => 'MPCATG_MOD']);
		SysPrivileges::create([ 'name' => 'Market Place Categories - Delete', 'description' => 'Capability to delete records', 'code' => 'MPCATG_DEL']);
		SysPrivileges::create([ 'name' => 'Market Place Categories - Audit', 'description' => 'Capability to view changes done on records', 'code' => 'MPCATG_AUD']);

		SysPrivileges::create([ 'name' => 'Market Place Products - List', 'description' => 'Capability to view the products', 'code' => 'MPPROD_VLT']);
    SysPrivileges::create([ 'name' => 'Market Place Products - View Detail', 'description' => 'Capability to view details of the products', 'code' => 'MPPROD_VDT']);
		SysPrivileges::create([ 'name' => 'Market Place Products - Add', 'description' => 'Capability to add new records', 'code' => 'MPPROD_ADD']);
		SysPrivileges::create([ 'name' => 'Market Place Products - Modify', 'description' => 'Capability to modify records', 'code' => 'MPPROD_MOD']);
		SysPrivileges::create([ 'name' => 'Market Place Products - Delete', 'description' => 'Capability to delete records', 'code' => 'MPPROD_DEL']);
		SysPrivileges::create([ 'name' => 'Market Place Products - Audit', 'description' => 'Capability to view changes done on records', 'code' => 'MPPROD_AUD']);

    SysPrivileges::create([ 'name' => 'Market Place Inventory - List', 'description' => 'Capability to view the inventory', 'code' => 'MPINVE_VLT']);
    SysPrivileges::create([ 'name' => 'Market Place Inventory - View Detail', 'description' => 'Capability to view details of the inventory', 'code' => 'MPINVE_VDT']);
		SysPrivileges::create([ 'name' => 'Market Place Inventory - Add', 'description' => 'Capability to add new records', 'code' => 'MPINVE_ADD']);
		SysPrivileges::create([ 'name' => 'Market Place Inventory - Modify', 'description' => 'Capability to modify records', 'code' => 'MPINVE_MOD']);
		SysPrivileges::create([ 'name' => 'Market Place Inventory - Delete', 'description' => 'Capability to delete records', 'code' => 'MPINVE_DEL']);
		SysPrivileges::create([ 'name' => 'Market Place Inventory - Audit', 'description' => 'Capability to view changes done on records', 'code' => 'MPINVE_AUD']);

    // Front End - Pages
    SysPrivileges::create([ 'name' => 'Market Place Pages - List', 'description' => 'Capability to view the page', 'code' => 'MPPAGE_VLT']);
    SysPrivileges::create([ 'name' => 'Market Place Pages - View Detail', 'description' => 'Capability to view details of the page', 'code' => 'MPPAGE_VDT']);
		SysPrivileges::create([ 'name' => 'Market Place Pages - Add', 'description' => 'Capability to add new records', 'code' => 'MPPAGE_ADD']);
		SysPrivileges::create([ 'name' => 'Market Place Pages - Modify', 'description' => 'Capability to modify records', 'code' => 'MPPAGE_MOD']);
		SysPrivileges::create([ 'name' => 'Market Place Pages - Delete', 'description' => 'Capability to delete records', 'code' => 'MPPAGE_DEL']);
		SysPrivileges::create([ 'name' => 'Market Place Pages - Audit', 'description' => 'Capability to view changes done on records', 'code' => 'MPPAGE_AUD']);

    // Front End - Footer Links
    SysPrivileges::create([ 'name' => 'Market Place Footer Links - List', 'description' => 'Capability to view the footer link', 'code' => 'MPFLNK_VLT']);
    SysPrivileges::create([ 'name' => 'Market Place Footer Links - View Detail', 'description' => 'Capability to view details of the footer link', 'code' => 'MPFLNK_VDT']);
		SysPrivileges::create([ 'name' => 'Market Place Footer Links - Add', 'description' => 'Capability to add new records', 'code' => 'MPFLNK_ADD']);
		SysPrivileges::create([ 'name' => 'Market Place Footer Links - Modify', 'description' => 'Capability to modify records', 'code' => 'MPFLNK_MOD']);
		SysPrivileges::create([ 'name' => 'Market Place Footer Links - Delete', 'description' => 'Capability to delete records', 'code' => 'MPFLNK_DEL']);
		SysPrivileges::create([ 'name' => 'Market Place Footer Links - Audit', 'description' => 'Capability to view changes done on records', 'code' => 'MPFLNK_AUD']);

    // Front End - FAQs
    SysPrivileges::create([ 'name' => 'Market Place FAQs - List', 'description' => 'Capability to view the faq', 'code' => 'MPFAQS_VLT']);
    SysPrivileges::create([ 'name' => 'Market Place FAQs - View Detail', 'description' => 'Capability to view details of the faq', 'code' => 'MPFAQS_VDT']);
		SysPrivileges::create([ 'name' => 'Market Place FAQs - Add', 'description' => 'Capability to add new records', 'code' => 'MPFAQS_ADD']);
		SysPrivileges::create([ 'name' => 'Market Place FAQs - Modify', 'description' => 'Capability to modify records', 'code' => 'MPFAQS_MOD']);
		SysPrivileges::create([ 'name' => 'Market Place FAQs - Delete', 'description' => 'Capability to delete records', 'code' => 'MPFAQS_DEL']);
		SysPrivileges::create([ 'name' => 'Market Place FAQs - Audit', 'description' => 'Capability to view changes done on records', 'code' => 'MPFAQS_AUD']);

    // Front End - Carousel
    SysPrivileges::create([ 'name' => 'Market Place Carousel - List', 'description' => 'Capability to view the carousel', 'code' => 'MPCARO_VLT']);
    SysPrivileges::create([ 'name' => 'Market Place Carousel - View Detail', 'description' => 'Capability to view details of the carousel', 'code' => 'MPCARO_VDT']);
		SysPrivileges::create([ 'name' => 'Market Place Carousel - Add', 'description' => 'Capability to add new records', 'code' => 'MPCARO_ADD']);
		SysPrivileges::create([ 'name' => 'Market Place Carousel - Modify', 'description' => 'Capability to modify records', 'code' => 'MPCARO_MOD']);
		SysPrivileges::create([ 'name' => 'Market Place Carousel - Delete', 'description' => 'Capability to delete records', 'code' => 'MPCARO_DEL']);
		SysPrivileges::create([ 'name' => 'Market Place Carousel - Audit', 'description' => 'Capability to view changes done on records', 'code' => 'MPCARO_AUD']);

    // Front End - Banner
    SysPrivileges::create([ 'name' => 'Market Place Banner - List', 'description' => 'Capability to view the banner', 'code' => 'MPBANN_VLT']);
    SysPrivileges::create([ 'name' => 'Market Place Banner - View Detail', 'description' => 'Capability to view details of the banner', 'code' => 'MPBANN_VDT']);
		SysPrivileges::create([ 'name' => 'Market Place Banner - Add', 'description' => 'Capability to add new records', 'code' => 'MPBANN_ADD']);
		SysPrivileges::create([ 'name' => 'Market Place Banner - Modify', 'description' => 'Capability to modify records', 'code' => 'MPBANN_MOD']);
		SysPrivileges::create([ 'name' => 'Market Place Banner - Delete', 'description' => 'Capability to delete records', 'code' => 'MPBANN_DEL']);
		SysPrivileges::create([ 'name' => 'Market Place Banner - Audit', 'description' => 'Capability to view changes done on records', 'code' => 'MPBANN_AUD']);

    // Front End - News
    SysPrivileges::create([ 'name' => 'Market Place News - List', 'description' => 'Capability to view the news item', 'code' => 'MPNEWS_VLT']);
    SysPrivileges::create([ 'name' => 'Market Place News - View Detail', 'description' => 'Capability to view details of the news item', 'code' => 'MPNEWS_VDT']);
		SysPrivileges::create([ 'name' => 'Market Place News - Add', 'description' => 'Capability to add new records', 'code' => 'MPNEWS_ADD']);
		SysPrivileges::create([ 'name' => 'Market Place News - Modify', 'description' => 'Capability to modify records', 'code' => 'MPNEWS_MOD']);
		SysPrivileges::create([ 'name' => 'Market Place News - Delete', 'description' => 'Capability to delete records', 'code' => 'MPNEWS_DEL']);
		SysPrivileges::create([ 'name' => 'Market Place News - Audit', 'description' => 'Capability to view changes done on records', 'code' => 'MPNEWS_AUD']);
	
	// Front End - BLogs
    SysPrivileges::create([ 'name' => 'Market Place News - List', 'description' => 'Capability to view the news item', 'code' => 'MPBLOGS_VLT']);
    SysPrivileges::create([ 'name' => 'Market Place News - View Detail', 'description' => 'Capability to view details of the news item', 'code' => 'MPBLOGS_VDT']);
		SysPrivileges::create([ 'name' => 'Market Place News - Add', 'description' => 'Capability to add new records', 'code' => 'MPBLOGS_ADD']);
		SysPrivileges::create([ 'name' => 'Market Place News - Modify', 'description' => 'Capability to modify records', 'code' => 'MPBLOGS_MOD']);
		SysPrivileges::create([ 'name' => 'Market Place News - Delete', 'description' => 'Capability to delete records', 'code' => 'MPBLOGS_DEL']);
		SysPrivileges::create([ 'name' => 'Market Place News - Audit', 'description' => 'Capability to view changes done on records', 'code' => 'MPBLOGS_AUD']);

		SysPrivileges::create([ 'name' => 'Organizations - List', 'description' => 'Capability to view the organizations', 'code' => 'CONFORG_VLT']);

		DB::table('sysRoles')->delete();

		DB::statement('ALTER TABLE sysRoles AUTO_INCREMENT = 1;');

		SysRoles::create([ 'name' => 'Administrator', 'description' => 'Administrator role with all privileges']);
		SysRoles::create([ 'name' => 'Professional Seller', 'description' => 'Role for professional sellers']);
		SysRoles::create([ 'name' => 'Company', 'description' => 'Role for companies']);
		SysRoles::create([ 'name' => 'Customer', 'description' => 'Role for buyers']);

		DB::table('sysRolePrivileges')->delete();

		DB::statement('ALTER TABLE sysRolePrivileges AUTO_INCREMENT = 1;');

    // Add all privileges to admin
	foreach(SysPrivileges::all() as $key => $value) {
		SysRolePrivileges::create([ 'role_id' => '1', 'privilege_id' => $value->id]);
	}
	
    // Add privileges to Professional Role
    SysRolePrivileges::create([ 'role_id' => '2', 'privilege_id' => 3]);
    SysRolePrivileges::create([ 'role_id' => '2', 'privilege_id' => 4]);
    SysRolePrivileges::create([ 'role_id' => '2', 'privilege_id' => 5]);
    SysRolePrivileges::create([ 'role_id' => '2', 'privilege_id' => 6]);

    // Add privileges to Company Role
    SysRolePrivileges::create([ 'role_id' => '3', 'privilege_id' => 3]);
    SysRolePrivileges::create([ 'role_id' => '3', 'privilege_id' => 4]);
    SysRolePrivileges::create([ 'role_id' => '3', 'privilege_id' => 5]);
    SysRolePrivileges::create([ 'role_id' => '3', 'privilege_id' => 6]);
    SysRolePrivileges::create([ 'role_id' => '3', 'privilege_id' => 7]);

    // Add privileges to buyer role
    SysRolePrivileges::create([ 'role_id' => '4', 'privilege_id' => 2]);
    SysRolePrivileges::create([ 'role_id' => '4', 'privilege_id' => 3]);

	        // Front End - BLogs
	$value = SysPrivileges::create([ 'name' => 'Market Place Blogs - List', 'description' => 'Capability to view the Blogs item', 'code' => 'MPBLOGS_VLT']);
	SysRolePrivileges::create([ 'role_id' => '1', 'privilege_id' => $value->id]);
	$value = SysPrivileges::create([ 'name' => 'Market Place Blogs - View Detail', 'description' => 'Capability to view details of the Blogs item', 'code' => 'MPBLOGS_VDT']);
	SysRolePrivileges::create([ 'role_id' => '1', 'privilege_id' => $value->id]);
	$value = SysPrivileges::create([ 'name' => 'Market Place Blogs - Add', 'description' => 'Capability to add new records', 'code' => 'MPBLOGS_ADD']);
	SysRolePrivileges::create([ 'role_id' => '1', 'privilege_id' => $value->id]);
	$value = SysPrivileges::create([ 'name' => 'Market Place Blogs - Modify', 'description' => 'Capability to modify records', 'code' => 'MPBLOGS_MOD']);
	SysRolePrivileges::create([ 'role_id' => '1', 'privilege_id' => $value->id]);
	$value = SysPrivileges::create([ 'name' => 'Market Place Blogs - Delete', 'description' => 'Capability to delete records', 'code' => 'MPBLOGS_DEL']);
	SysRolePrivileges::create([ 'role_id' => '1', 'privilege_id' => $value->id]);
	$value = SysPrivileges::create([ 'name' => 'Market Place Blogs - Audit', 'description' => 'Capability to view changes done on records', 'code' => 'MPBLOGS_AUD']);
	SysRolePrivileges::create([ 'role_id' => '1', 'privilege_id' => $value->id]);
    // Add Roles to Users
		DB::table('sysUserRoles')->delete();

		DB::statement('ALTER TABLE sysUserRoles AUTO_INCREMENT = 1;');

		sysUserRoles::create(['user_id' => '1', 'role_id' => '1']);
		sysUserRoles::create(['user_id' => '2', 'role_id' => '1']);
		sysUserRoles::create(['user_id' => '3', 'role_id' => '2']);
		sysUserRoles::create(['user_id' => '4', 'role_id' => '3']);
		sysUserRoles::create(['user_id' => '5', 'role_id' => '4']);

		DB::table('sysStatuses')->delete();

		DB::statement('ALTER TABLE sysStatuses AUTO_INCREMENT = 1;');

		SysStatuses::create(['name' => 'opened','value' => 'OP', 'status_for' => 'INB_IBL']);
		SysStatuses::create(['name' => 'closed','value' => 'CL', 'status_for' => 'INB_IBL']);
		SysStatuses::create(['name' => 'cancelled','value' => 'CN', 'status_for' => 'INB_IBL']);
		SysStatuses::create(['name' => 'onhold','value' => 'OH', 'status_for' => 'INB_IBL']);
		SysStatuses::create(['name' => 'opened','value' => 'OP', 'status_for' => 'INB_ASN']);
		SysStatuses::create(['name' => 'closed','value' => 'CL', 'status_for' => 'INB_ASN']);
		SysStatuses::create(['name' => 'cancelled','value' => 'CN', 'status_for' => 'INB_ASN']);
		SysStatuses::create(['name' => 'onhold','value' => 'OH', 'status_for' => 'INB_ASN']);
		SysStatuses::create(['name' => 'opened','value' => 'OP', 'status_for' => 'INB_CASES']);
		SysStatuses::create(['name' => 'closed','value' => 'CL', 'status_for' => 'INB_CASES']);
		SysStatuses::create(['name' => 'cancelled','value' => 'CN', 'status_for' => 'INB_CASES']);
		SysStatuses::create(['name' => 'onhold','value' => 'OH', 'status_for' => 'INB_CASES']);
		SysStatuses::create(['name' => 'opened','value' => 'OP', 'status_for' => 'INB_DOCKDOORS']);
		SysStatuses::create(['name' => 'closed','value' => 'CL', 'status_for' => 'INB_DOCKDOORS']);
		SysStatuses::create(['name' => 'cancelled','value' => 'CN', 'status_for' => 'INB_DOCKDOORS']);
		SysStatuses::create(['name' => 'onhold','value' => 'OH', 'status_for' => 'INB_DOCKDOORS']);

    // Organizations

    DB::table('sysOrganizations')->delete();

    DB::statement('ALTER TABLE sysOrganizations AUTO_INCREMENT = 1;');

		SysOrganization::create(['name' => 'Bank store', 'user_id' => 1, 'created_by' => 1]);
		SysOrganization::create(['name' => 'Default store', 'user_id' => 1, 'created_by' => 2]);
    SysOrganization::create([ 'user_id' => 2, 'name' => 'jdoe\'s store', 'created_by' => 2 ]);
    SysOrganization::create([ 'user_id' => 3, 'name' => 'jadoe\'s store', 'created_by' => 2 ]);
    SysOrganization::create([ 'user_id' => 4, 'name' => 'mdoe\'s store', 'created_by' => 2 ]);

    DB::table('appOrgBanners')->delete();

    DB::statement('ALTER TABLE appOrgBanners AUTO_INCREMENT = 1;');

    AppOrgBanner::create(['name' => 'Example 1', 'image_path' => 'banner_image_1.jpg', 'priority' => 1, 'location' => 'BNR_MAIN_1','created_by' => 1]);
    AppOrgBanner::create(['name' => 'Example 2', 'image_path' => 'banner_image_2.jpg', 'priority' => 1, 'location' => 'BNR_MAIN_1','created_by' => 1]);
    AppOrgBanner::create(['name' => 'Example 3', 'image_path' => 'banner_image_3.jpg', 'priority' => 1, 'location' => 'BNR_MAIN_1','created_by' => 1]);
    AppOrgBanner::create(['name' => 'Example 4', 'image_path' => 'banner_image_4.jpg', 'priority' => 1, 'location' => 'BNR_MAIN_2','created_by' => 1]);
    AppOrgBanner::create(['name' => 'Example 5', 'image_path' => 'banner_image_5.jpg', 'priority' => 1, 'location' => 'BNR_MAIN_2','created_by' => 1]);
    AppOrgBanner::create(['name' => 'Example 6', 'image_path' => 'banner_image_6.jpg', 'priority' => 1, 'location' => 'BNR_MAIN_2','created_by' => 1]);

		// Shippings

    DB::table('sysShippings')->delete();

    DB::statement('ALTER TABLE sysShippings AUTO_INCREMENT = 1;');

		SysShipping::create(['type' => 'CA', 'name' => 'Carta Ordinaria', 'certified' => 'No', 																 'large' => 10, 'width' => 12, 'hight' =>5 , 'volume' => (10 * 12 * 5 ) /6000, 'max_value' => 24.99 , 'max_weight' => 50, 'stamp_price' => 0.65, 'price' => 0.95, 'created_by' => 1]);
		SysShipping::create(['type' => 'CA', 'name' => 'Carta Ordinaria', 'certified' => 'No', 																 'large' => 11, 'width' => 13, 'hight' =>6 , 'volume' => (11 * 13 * 6 ) /6000, 'max_value' => 24.99 , 'max_weight' => 100, 'stamp_price' => 1.05, 'price' => 1.55, 'created_by' => 1]);
		SysShipping::create(['type' => 'CA', 'name' => 'Carta Ordinaria', 'certified' => 'No', 																 'large' => 12, 'width' => 14, 'hight' =>7 , 'volume' => (12 * 14 * 7 ) /6000, 'max_value' => 24.99 , 'max_weight' => 500, 'stamp_price' => 2.25, 'price' => 2.75, 'created_by' => 1]);
		SysShipping::create(['type' => 'CA', 'name' => 'Carta Ordinaria', 'certified' => 'No', 																 'large' => 13, 'width' => 15, 'hight' =>8 , 'volume' => (13 * 15 * 8 ) /6000, 'max_value' => 24.99 , 'max_weight' => 500, 'stamp_price' => 2.25, 'price' => 2.75, 'created_by' => 1]);
		SysShipping::create(['type' => 'CA', 'name' => 'Carta Certificada', 'certified' => 'Yes', 														 'large' => 14, 'width' => 16, 'hight' =>9 , 'volume' => (14 * 16 * 9 ) /6000, 'max_value' => 200, 'max_weight' => 50, 'stamp_price' => 3.40, 'price' => 3.90, 'created_by' => 1]);
		SysShipping::create(['type' => 'CA', 'name' => 'Carta Certificada', 'certified' => 'Yes', 														 'large' => 15, 'width' => 17, 'hight' =>10, 'volume' => (15 * 17 * 10) /6000, 'max_value' => 200, 'max_weight' => 100, 'stamp_price' => 3.80, 'price' => 4.30, 'created_by' => 1]);
		SysShipping::create(['type' => 'CA', 'name' => 'Carta Certificada', 'certified' => 'Yes', 														 'large' => 16, 'width' => 18, 'hight' =>11, 'volume' => (16 * 18 * 11) /6000, 'max_value' => 200, 'max_weight' => 500, 'stamp_price' => 5, 'price' => 5.50, 'created_by' => 1]);
		SysShipping::create(['type' => 'CA', 'name' => 'Carta Certificada', 'certified' => 'Yes', 														 'large' => 17, 'width' => 19, 'hight' =>12, 'volume' => (17 * 19 * 12) /6000, 'max_value' => 200, 'max_weight' => 500, 'stamp_price' => 5, 'price' => 5.50, 'created_by' => 1]);
		SysShipping::create(['type' => 'CA', 'name' => 'Carta Certificada (Valor declarado 500€)', 'certified' => 'Yes',       'large' => 18, 'width' => 10, 'hight' =>13, 'volume' => (18 * 10 * 13) /6000, 'max_value' => 500, 'max_weight' => 500, 'stamp_price' => 20, 'price' => 25.50, 'created_by' => 1]);
		SysShipping::create(['type' => 'CA', 'name' => 'Carta Certificada (Valor declarado 1.000€)', 'certified' => 'Yes',     'large' => 19, 'width' => 11, 'hight' =>14, 'volume' => (19 * 11 * 14) /6000, 'max_value' => 1000, 'max_weight' => 500, 'stamp_price' => 40, 'price' => 45.50, 'created_by' => 1]);
		SysShipping::create(['type' => 'CA', 'name' => 'Carta Certificada (Valor declarado 2.000€)', 'certified' => 'Yes', 		 'large' => 20, 'width' => 12, 'hight' =>15, 'volume' => (20 * 12 * 15) /6000, 'max_value' => 2000, 'max_weight' => 500, 'stamp_price' => 80, 'price' => 85.50, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Paquete Azul', 'certified' => 'Yes', 																	 'large' => 21, 'width' => 13, 'hight' =>16, 'volume' => (21 * 13 * 16) /6000, 'max_value' => 100, 'max_weight' => 2000, 'stamp_price' => 12.08, 'price' => 13.08, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Paquete Azul', 'certified' => 'Yes', 																	 'large' => 22, 'width' => 14, 'hight' =>17, 'volume' => (22 * 14 * 17) /6000, 'max_value' => 100, 'max_weight' => 5000, 'stamp_price' => 16.25, 'price' => 17.25, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Paquete Azul', 'certified' => 'Yes', 																	 'large' => 23, 'width' => 15, 'hight' =>18, 'volume' => (23 * 15 * 18) /6000, 'max_value' => 100, 'max_weight' => 10000, 'stamp_price' => 19.34, 'price' => 20.34, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Paquete Azul', 'certified' => 'Yes', 																	 'large' => 24, 'width' => 16, 'hight' =>19, 'volume' => (24 * 16 * 19) /6000, 'max_value' => 100, 'max_weight' => 15000, 'stamp_price' => 28.14, 'price' => 29.14, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Paquete Azul', 'certified' => 'Yes', 																	 'large' => 25, 'width' => 17, 'hight' =>20, 'volume' => (25 * 17 * 20) /6000, 'max_value' => 100, 'max_weight' => 20000, 'stamp_price' => 33.78, 'price' => 34.78, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Paquete Azul (Valor declarado 500€)', 'certified' => 'Yes', 					 'large' => 26, 'width' => 18, 'hight' =>21, 'volume' => (26 * 18 * 21) /6000, 'max_value' => 500, 'max_weight' => 2000, 'stamp_price' => 19.58, 'price' => 20.58, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Paquete Azul (Valor declarado 500€)', 'certified' => 'Yes', 					 'large' => 27, 'width' => 19, 'hight' =>22, 'volume' => (27 * 19 * 22) /6000, 'max_value' => 500, 'max_weight' => 5000, 'stamp_price' => 23.75, 'price' => 24.75, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Paquete Azul (Valor declarado 500€)', 'certified' => 'Yes', 					 'large' => 28, 'width' => 10, 'hight' =>23, 'volume' => (28 * 10 * 23) /6000, 'max_value' => 500, 'max_weight' => 10000, 'stamp_price' => 26.84, 'price' => 27.84, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Paquete Azul (Valor declarado 500€)', 'certified' => 'Yes', 					 'large' => 29, 'width' => 11, 'hight' =>24, 'volume' => (29 * 11 * 24) /6000, 'max_value' => 500, 'max_weight' => 15000, 'stamp_price' => 35.64, 'price' => 36.64, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Paquete Azul (Valor declarado 500€)', 'certified' => 'Yes', 					 'large' => 30, 'width' => 12, 'hight' =>25, 'volume' => (30 * 12 * 25) /6000, 'max_value' => 500, 'max_weight' => 20000, 'stamp_price' => 41.28, 'price' => 42.28, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Paquete Azul (Valor declarado 1000€)', 'certified' => 'Yes', 					 'large' => 31, 'width' => 13, 'hight' =>26, 'volume' => (31 * 13 * 26) /6000, 'max_value' => 1000, 'max_weight' => 2000, 'stamp_price' => 27.08, 'price' => 28.08, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Paquete Azul (Valor declarado 1000€)', 'certified' => 'Yes', 					 'large' => 32, 'width' => 14, 'hight' =>27, 'volume' => (32 * 14 * 27) /6000, 'max_value' => 1000, 'max_weight' => 5000, 'stamp_price' => 31.25, 'price' => 32.25, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Paquete Azul (Valor declarado 1000€)', 'certified' => 'Yes', 					 'large' => 33, 'width' => 15, 'hight' =>28, 'volume' => (33 * 15 * 28) /6000, 'max_value' => 1000, 'max_weight' => 10000, 'stamp_price' => 34.34, 'price' => 35.34, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Paquete Azul (Valor declarado 1000€)', 'certified' => 'Yes', 					 'large' => 34, 'width' => 16, 'hight' =>29, 'volume' => (34 * 16 * 29) /6000, 'max_value' => 1000, 'max_weight' => 15000, 'stamp_price' => 43.14, 'price' => 44.14, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Paquete Azul (Valor declarado 1000€)', 'certified' => 'Yes', 					 'large' => 35, 'width' => 17, 'hight' =>30, 'volume' => (35 * 17 * 30) /6000, 'max_value' => 1000, 'max_weight' => 20000, 'stamp_price' => 48.78, 'price' => 49.78, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Courrier Parcel with Full Insurance (Flat Fee)', 'certified' => 'Yes', 'large' => 36, 'width' => 18, 'hight' =>31, 'volume' => (36 * 18 * 31) /6000, 'max_value' => 50000, 'max_weight' => 20000, 'stamp_price' => 99, 'price' => 100, 'created_by' => 1]);
		SysShipping::create(['type' => 'PA', 'name' => 'Courrier Parcel with Full Insurance (Flat Fee)', 'certified' => 'Yes', 'large' => 37, 'width' => 19, 'hight' =>32, 'volume' => (37 * 19 * 32) /6000, 'max_value' => 100000, 'max_weight' => 20000, 'stamp_price' => 199, 'price' => 200, 'created_by' => 1]);

		DB::table('appOrgCartUsers')->delete();
		DB::statement('ALTER TABLE appOrgCartUsers AUTO_INCREMENT = 1;');
		
		DB::table('appOrgCartDetailUsers')->delete();
		DB::statement('ALTER TABLE appOrgCartDetailUsers AUTO_INCREMENT = 1;');

		DB::table('appOrgCartShippings')->delete();
		DB::statement('ALTER TABLE appOrgCartShippings AUTO_INCREMENT = 1;');
		
  }

}
