<?php

use Illuminate\Database\Seeder;
use App\AppOrgCarousel;
use App\AppOrgCarouselItem;

class AppOrgCarouselSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('appOrgCarousels')->delete();
      DB::table('appOrgCarouselItems')->delete();

      DB::statement('ALTER TABLE appOrgCarousels AUTO_INCREMENT = 1;');
      DB::statement('ALTER TABLE appOrgCarouselItems AUTO_INCREMENT = 1;');

      AppOrgCarousel::create([
        'name' => 'Main carousel', 'location' => 'CRS_MAIN', 'width_type' => 'CRS_WFULL', 'height_type' => 'CRS_HAU',
      ]);

      AppOrgCarouselItem::create([
        'carousel_id' => 1, 'image_path' => 'carousel_image_1_1.jpg', 'content_title' => 'Call of Duty', 'content_text' => 'Call of Duty es una serie de videojuegos de disparos en primera persona (FPS), de estilo bélico, creada por Ben Chichoski, desarrollada principal e inicialmente por Infinity Ward, y distribuida por Activision',
        'slide_order' => 1, 'slide_duration' => 2000
      ]);

      AppOrgCarouselItem::create([
        'carousel_id' => 1, 'image_path' => 'carousel_image_1_2.jpg', 'content_title' => 'Paragon (video game)', 'content_text' => 'Paragon was a third-person multiplayer online battle arena video game. The maps featured in the game were symmetrical, and bases were located at the two opposite ends of a map.',
        'slide_order' => 2, 'slide_duration' => 2000
      ]);

      AppOrgCarouselItem::create([
        'carousel_id' => 1, 'image_path' => 'carousel_image_1_3.jpg', 'content_title' => 'Batman: Arkham Knight', 'content_text' => 'El juego se presenta desde una perspectiva en tercera persona, con un enfoque principal en Batman y sus habilidades como sigilo e infiltración, detective, combate cuerpo a cuerpo y gadgets.',
        'slide_order' => 3, 'slide_duration' => 2000
      ]);
    }
}
