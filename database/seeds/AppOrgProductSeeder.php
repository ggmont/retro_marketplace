<?php

use App\AppOrgProduct;
use App\AppOrgProductImage;
use Illuminate\Database\Seeder;

class AppOrgProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::table('appOrgProducts')->delete();
      DB::table('appOrgProductImages')->delete();

  		DB::statement('ALTER TABLE appOrgProducts AUTO_INCREMENT = 1;');
  		DB::statement('ALTER TABLE appOrgProductImages AUTO_INCREMENT = 1;');

      AppOrgProduct::create(['volume' => 0.6, 'weight' => 2000, 'name' => 'Sega SG 1000', 'release_date' => '1983-01-01', 'prod_category_id' => 23,'platform' => 'ANDROID', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.6, 'weight' => 2000, 'name' => 'Sega SG 3000', 'release_date' => '1983-01-01', 'prod_category_id' => 23,'platform' => 'ANDROID', 'region' => 'NTSC-U']);
      AppOrgProduct::create(['volume' => 0.6, 'weight' => 2000, 'name' => 'Sega Megadrive', 'release_date' => '1983-01-01', 'prod_category_id' => 23,'platform' => 'ANDROID', 'region' => 'NTSC-J']);
      AppOrgProduct::create(['volume' => 0.6, 'weight' => 2000, 'name' => 'Sony Play Station 1', 'release_date' => '1994-01-01', 'prod_category_id' => 26,'platform' => 'ANDROID', 'region' => 'NTSC-J']);
      AppOrgProduct::create(['volume' => 0.6, 'weight' => 2000, 'name' => 'Sony Play Station 2', 'release_date' => '1995-01-01', 'prod_category_id' => 26,'platform' => 'ANDROID', 'region' => 'NTSC-U']);
      AppOrgProduct::create(['volume' => 0.6, 'weight' => 2000, 'name' => 'Sony Play Station 3', 'release_date' => '1995-01-01', 'prod_category_id' => 26,'platform' => 'ANDROID', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.25, 'weight' => 175, 'name' => 'Spyro the Dragon', 'release_date' => '1998-11-23', 'prod_category_id' => 5,'platform' => 'PS', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.25, 'weight' => 175, 'name' => 'Spyro the Dragon - Platinum', 'release_date' => '1996-01-01', 'prod_category_id' => 28,'platform' => 'PS', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.25, 'weight' => 175, 'name' => '007 - El mundo nunca es suficiente', 'release_date' => '2000-11-17', 'prod_category_id' => 5,'platform' => 'PS', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.25, 'weight' => 175, 'name' => '007 - El mañana nunca muere', 'release_date' => '1999-11-25', 'prod_category_id' => 5,'platform' => 'PS', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.25, 'weight' => 175, 'name' => '007 - Racing', 'release_date' => '2000-12-10', 'prod_category_id' => 5,'platform' => 'PS', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.25, 'weight' => 175, 'name' => '100% Star', 'release_date' => '2001-11-23', 'prod_category_id' => 5,'platform' => 'PS', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.25, 'weight' => 175, 'name' => '2Xtreme', 'release_date' => '1997-03-01', 'prod_category_id' => 5,'platform' => 'PS', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.25, 'weight' => 175, 'name' => '3,2,1... Pitufos!Mi primer juego de carreras', 'release_date' => '2001-03-29', 'prod_category_id' => 5,'platform' => 'PS', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.25, 'weight' => 175, 'name' => '360 - Three Sixty', 'release_date' => '1999-07-23', 'prod_category_id' => 5,'platform' => 'PS', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.25, 'weight' => 175, 'name' => '40 Winks - con Ruff y Tumble', 'release_date' => '1999-11-12', 'prod_category_id' => 5,'platform' => 'PS', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.25, 'weight' => 175, 'name' => '007 - Le monde ne suffit pas', 'release_date' => '2000-11-17', 'prod_category_id' => 5,'platform' => 'PS', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.25, 'weight' => 175, 'name' => '007 - Die welt ist nicht genug', 'release_date' => '2000-11-17', 'prod_category_id' => 5,'platform' => 'PS', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.25, 'weight' => 175, 'name' => '007 - The world is not enough', 'release_date' => '2000-11-17', 'prod_category_id' => 5,'platform' => 'PS', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.25, 'weight' => 175, 'name' => '007 - Tomorrow never dies', 'release_date' => '1999-11-25', 'prod_category_id' => 5,'platform' => 'PS', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.25, 'weight' => 175, 'name' => '007 - Demain ne meurt jamais', 'release_date' => '1999-11-25', 'prod_category_id' => 5,'platform' => 'PS', 'region' => 'PAL']);
      AppOrgProduct::create(['volume' => 0.25, 'weight' => 175, 'name' => '007 - Der morgen stirbtnie', 'release_date' => '1999-11-25', 'prod_category_id' => 5,'platform' => 'PS', 'region' => 'PAL']);

      AppOrgProductImage::create(['product_id' => 9, 'image_path' => 'pro_img_9_1.jpg']);
      AppOrgProductImage::create(['product_id' => 11, 'image_path' => 'pro_img_11_5.jpg']);
      AppOrgProductImage::create(['product_id' => 7, 'image_path' => 'pro_img_7_6.jpg']);
      AppOrgProductImage::create(['product_id' => 12, 'image_path' => 'pro_img_12_7.jpg']);
      AppOrgProductImage::create(['product_id' => 13, 'image_path' => 'pro_img_13_8.jpg']);
      AppOrgProductImage::create(['product_id' => 14, 'image_path' => 'pro_img_14_9.jpg']);
      AppOrgProductImage::create(['product_id' => 15, 'image_path' => 'pro_img_15_10.jpg']);
      AppOrgProductImage::create(['product_id' => 16, 'image_path' => 'pro_img_16_11.jpg']);
      AppOrgProductImage::create(['product_id' => 17, 'image_path' => 'pro_img_17_12.jpg']);
      AppOrgProductImage::create(['product_id' => 18, 'image_path' => 'pro_img_18_13.jpg']);
      AppOrgProductImage::create(['product_id' => 19, 'image_path' => 'pro_img_19_14.jpg']);
      AppOrgProductImage::create(['product_id' => 20, 'image_path' => 'pro_img_20_15.jpg']);
      AppOrgProductImage::create(['product_id' => 10, 'image_path' => 'pro_img_10_16.jpg']);
      AppOrgProductImage::create(['product_id' => 21, 'image_path' => 'pro_img_21_17.jpg']);
      AppOrgProductImage::create(['product_id' => 22, 'image_path' => 'pro_img_22_18.jpg']);


    }
}
