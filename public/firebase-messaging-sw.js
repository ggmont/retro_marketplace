importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js');
   
firebase.initializeApp({
  apiKey: "AIzaSyDqCbmiW7VzXWUk71AmAfCDltT5Ne_xGJc",
  authDomain: "retrogamingmarket.firebaseapp.com",
  projectId: "retrogamingmarket",
  storageBucket: "retrogamingmarket.appspot.com",
  messagingSenderId: "628245415654",
  appId: "1:628245415654:web:20bf0e8e77a0cea7dadd27",
  measurementId: "G-G6NFK11DJ5"
});
  
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function({data:{title,body,icon}}) {
    return self.registration.showNotification(title,{body,icon});
});