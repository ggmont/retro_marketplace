var angularApp = angular.module('appMarketPlace',['ngSanitize', 'ui.bootstrap']);

angularApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.common['Accept']         = 'application/json, text/javascript';
    $httpProvider.defaults.headers.common['Content-Type']   = 'application/json; charset=utf-8';
    $httpProvider.defaults.headers.common['X-CSRF-TOKEN']   = $('meta[name="csrf-token"]').attr('content');
}]);
