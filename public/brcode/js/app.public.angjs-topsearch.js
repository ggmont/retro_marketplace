angularApp.controller('topSearchController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {

  var ctrl = this;

  // ctrl.videoShowRequest = false;
  // ctrl.videoShowRequestFrom = false;
  // if (window.location.href.indexOf('messages') > -1) {
  //   $rootScope.$emit('video_request', ctrl.videoRequestData);
  // }
  // else {
  //   $cookies.putObject('video_request', ctrl.videoRequestData, { path: '/' });
  //   window.location.href = BASEURL + 'messages/' + ctrl.videoRequestData.requested_by;
  // }

  // $rootScope.$on('video_request', function (event, chatVideoData) {

  ctrl.validateSearch = function() {
    if (window.location.href.indexOf('search') > -1) {
      $rootScope.$emit('search_request', $('#br-menu-search-input > input').val());
    }
    else {
      window.location.href = BASEURL + '/search?text=' + $('#br-menu-search-input > input').val();
    }
  }

  $('#br-menu-search-input > input').keypress(function(e) {

    if(this.value.length > 0 && e.which == 13) {
      ctrl.validateSearch();
    }

  });

}]);
