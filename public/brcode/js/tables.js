(function( $ ) {

  var tables_tmpls = [];

  $.fn.brTables = function( options ) {

    var settings = $.extend({
      // These are the defaults.
      allowRepeat: true,
      notRepeatNames: [],
      notRepeatMessage: null,
      onAdd: null,
    }, options );

    var tbls = $( this ).each(function(i,e) {
      var table = $( this );
      var tableId = makeid();
      var tmpl = table.find('tr.br-table-template') || '';

      if( tmpl.length > 0 ) {
        tables_tmpls[tableId] = tmpl.clone();
        tables_tmpls[tableId].removeClass('hide');
        tmpl.remove();
        table.attr('br-table-id',tableId);
      }

      if(settings.allowRepeat === false) {
        $.each(settings.notRepeatNames,function(i,v) {
          tables_tmpls[tableId].find('[name="'+v+'"]').addClass('br-table-no-repeat');
          //table.find('[name="'+v+'"]').addClass('br-table-no-repeat');
        });

        table.attr('br-table-no-repeat-msg',settings.notRepeatMessage);
      }

      table.find('button.br-table-add').click(function() {
        var tbl = $( this ).closest('table');
        var id =  tbl.attr('br-table-id');
        var tmpl = tables_tmpls[id].clone() || '';

        if(tmpl.length > 0) {
          if(typeof noty == 'function')
            tmpl.find('select').select2();

          tbl.find('tbody').append(tmpl);

        }

        if(settings.onAdd)
          settings.onAdd.call(tmpl);
      });

    });


  }

  $(document).on('click','table.br-table button.br-table-rem',function() {
    $( this ).closest('tr').remove();
  });

  $(document).on('change','table.br-table select.br-table-no-repeat',function() {
    var v = $( this ).val();
    var n = $( this ).attr('name');
    var t = $( this ).closest('tbody');
    var o = t.find('select[name="'+n+'"] option:selected[value="'+v+'"]');

    if(o.length > 1) {
      var m = t.closest('table').attr('br-table-no-repeat-msg');
      if (typeof noty == 'function') {
        presentNotyMessage(m,'information',[]);
      }
      else {
        alert(m);
      }
      $( this ).val(null);
      if($( this ).hasClass('select2-hidden-accessible')) {
        $( this ).trigger('change.select2');
      }

    }


  });

  function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

}( jQuery ));
