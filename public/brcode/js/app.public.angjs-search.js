angularApp.directive('onErrorSrc', function() {
    return {
        link: function(scope, element, attrs) {
          element.bind('error', function() {
            if (attrs.src != attrs.onErrorSrc) {
              attrs.$set('src', attrs.onErrorSrc);
            }
          });

          element.bind('load', function() {

          });
        }
    }
});

angularApp.controller('searchController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {

  var ctrl = this;

  /*****************************************************************************
  *
  * Search, Pagination and Property Lists
  *
  *****************************************************************************/

  $rootScope.$on('search_request', function (event, text) {
    console.log(text);
  });

  ctrl.searchPage = {
    length: 0,
    numPerPage: 20,
    currentPage: 1,
    maxSize: 5,
    sort: {
      column: 'name',
      dir: 'asc',
    },
    filter: {
      categories: window.categories,
      text: window.searchText,
    }
  };

  $scope.searchText = window.searchText;

  ctrl.trans        = GLOBAL_TRANSLATION;

  ctrl.productList  = [];

  ctrl.loadProducts = function() {
    $http({
        method: 'POST',
        url: BASEURL + '/get-products',
        data: {
          currentPage: ctrl.searchPage.currentPage,
          numPerPage: ctrl.searchPage.numPerPage,
          sortColumn: ctrl.searchPage.sort.column,
          sortDir: ctrl.searchPage.sort.dir,
          filter: $('#br-available').is(":checked") ? true : false,
          categories: ctrl.searchPage.filter.categories,
          text: ctrl.searchPage.filter.text
        }
    })
    .then(function(result) {
        if (result.status != 200) {

          console.log('Unknown error. Please contact your administrator', data);

        } else {
          ctrl.searchPage.length   = result.data.length;
          ctrl.productList         = result.data.records;
          
        }


        if(!$scope.$$phase) {
          $scope.$apply();
        }

    },function(error) {
      console.log('error',error);
    });
  }

  $scope.pageChanged = function() {
    ctrl.loadProducts();
  }

  ctrl.getAveragePrice = function(inventory) {
    var avg = 0, counter = 0;

    inventory.forEach(function(el, id) {
      avg += parseFloat(el.price);
      counter += 1
    });

    return counter > 0 ? avg / counter : 0;

  }

  
  
  ctrl.getAverageQuantity = function(inventory) {
    var data = [];

   

    inventory.forEach(function(el, id) {
      data.push({ id: '{{ $el->id }}', text: '{{ $el->quantity }}' });
    })

    consolelog(data);

  }

  ctrl.loadProducts();
   

  $('.br-search-category').click(function() {
    $(this).toggleClass('br-show-children')
  });

  $('.categoriesdata').change(function() {
    var checks = $('.categoriesdata option:selected');
    
    var cats = checks.map(function() { return checks.parent().val(); } ).get();

    ctrl.searchPage.filter.text = `${$('#br-search-result-input').val()}_platform_sort_${$('#br-search-platform-sort').val()}`;
    ctrl.searchPage.filter.categories = cats;
    ctrl.searchPage.currentPage = 1;
    ctrl.loadProducts();

  });

  $('#br-search-result-input').keypress(function() {
    ctrl.searchPage.currentPage = 1;
    ctrl.searchPage.filter.text = `${this.value}_platform_sort_${$('#br-search-platform-sort').val()}`;
  });

  $('#br-search-platform-sort').change(function() {
    ctrl.searchPage.currentPage = 1;
    ctrl.searchPage.filter.text = `${$('#br-search-result-input').val()}_platform_sort_${this.value}`;
    ctrl.loadProducts();
    return false;
  });


  $('#br-search-form').submit(function() {
    ctrl.loadProducts();
    return false;
  })

  $('.btn-search-main-search').click(function() {
    ctrl.searchPage.currentPage = 1;
    ctrl.searchPage.filter.text = `${$('#br-search-result-input').val()}_platform_sort_${$('#br-search-platform-sort').val()}`;
  });

  $('#br-search-result-sort').change(function() {

    var sort = this.value.split('-');

    ctrl.searchPage.currentPage = 1;
    ctrl.searchPage.sort.column = sort[0];
    ctrl.searchPage.sort.dir    = sort[1];

    ctrl.loadProducts();
  });

  $('#br-search-result-quantity').change(function() {
    ctrl.searchPage.currentPage = 1;
    ctrl.searchPage.numPerPage = this.value;
    ctrl.loadProducts();
  });

  // $('#br-available').change(function() {
  //   ctrl.searchPage.currentPage = 1;
  //   ctrl.searchPage.numPerPage = this.value;
  //   ctrl.loadProducts();
  // });

}]);
