

(function($) {

  $.fn.brComponents = function(options) {

    var settings = $.extend({
      // These are the defaults.
      type: 'input',
      affects: 'self',
    }, options);

    if (settings.type == 'dependency') {

      var objects = settings.affects == 'children' ? $(this).children() : $(this);
      var parentObj = $(this).attr('dependency') || '';

      if (parentObj.length > 0) {

        var val = $('select[id="' + parentObj + '"]').val();
        var opts = [];

        var lo = objects.each(function(i, e) {
          var dep = $(this).attr('dependency');
          if (opts[dep] == undefined) {
            opts[dep] = [$(this).clone()];
          } else {
            opts[dep].push($(this).clone());
          }

          if ($(this).attr('dependency') != val && $(this).attr('value') != '')
            $(this).remove();
        });

        lo.promise().done(function() {

          $('select[id="' + parentObj + '"]').change(function() {
            var child = $('select[dependency="' + parentObj + '"]');
            child.children().not('[value=""]').remove();
            child.change();
            if ($(this).val().length > 0) {
              $.each(opts[$(this).val()], function(i, e) {
                child.append(e.clone());
              })
            }
          });

        });
      } else {
        alert('There is not parent dependency');
      }


    }

    else if(settings.type == '') {

    }
  };

  $.showBigOverlay = function(options) {

    var settings = $.extend({
      message: 'Custom message missing. Add a "message" property to the function',
      onLoad: null,
    }, options);

    var overlay = $('<div/>').css({
			position: 'fixed',
			top: '0',
			left: '0',
			width: '100%',
			height: '100%',
			background: 'rgba(0, 0, 0, 0.5)',
			opacity: 0,
			'z-index': '10',
		}),
      message = $('<div/>').css({
			position: 'absolute',
			top: '50%',
			left: '50%',
			transform: 'translateX(-50%) translateY(-50%)',
			'z-index': '11',
			opacity: 0,
			color: '#fff'
		}),
      methods = {
        show: function() {
          $('body').css('overflow','hidden');

          overlay.appendTo($('body'));
          message.appendTo(overlay);
          message.animate({opacity: 1},250);
          overlay.animate({opacity: '1'},250,function() {
            if(settings.onLoad != null) {
              settings.onLoad();
            }
          });
        },
        hide: function() {
          var instance = window.bigOverlay;
          $('body').css('overflow','auto');
          instance.overlay.animate({opacity:0},250,function(){$(this).remove()});
          window.bigOverlay = null;
        }
      };

		message.append('<h3 class="big-overlay-message">'+settings.message+'</h3>');
		message.append('<div class="big-overlay-spin" style="text-align: center"><i class="fa fa-fw fa-spinner fa-pulse fa-3x"></i></div>');

    if( (typeof options).toString() == 'string' ) {
      if( (typeof methods[options]).toString() != 'undefined' ) {
        methods[options]();
      }
      else {
        alert('BIG Overlay: Invalid method');
      }
    }
    else {
      methods['show']();
      window.bigOverlay = {
        overlay: overlay
      };
    }

  }

}(jQuery));
