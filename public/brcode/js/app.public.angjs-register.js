angularApp.controller('registerController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {

  var ctrl = this;

  ctrl.formError        = false;
  ctrl.formReferred     = false;
  ctrl.formErrorMessage = '';

  ctrl.referred = function() {
    ctrl.formReferred = ctrl.formReferred ? false : true;
  }

  ctrl.mensajes = function(txt, type) {
    notySettings = {
      layout: 'topCenter',
      text: txt,
      theme: 'relax',
      type: type,
      maxVisible: 5,
      timeout: 20000,
    }

    var n = noty(notySettings);
  }

  ctrl.submitForm = function(form) {

    ctrl.formError          = false;
    ctrl.formErrorMessage   = '';
    var msg = "";
    if ( ! $('#br-register-terms').is(':checked')){
      ctrl.formError = true;
      msg += 'Debe aceptar los terminos y condiciones <br><br>';
    }
    if ( ! $('#br-register-years-old').is(':checked')){
      ctrl.formError = true;
      msg += 'Debe ser mayor de edad para poder registrar su cuenta <br>';
    }
    
    // ctrl.formErrorMessage = msg;

    if ( form.$invalid ){ 
      if( msg == ""){
        ctrl.mensajes('Complete todos los campos', 'error');
      }else{
        ctrl.mensajes(msg, 'error');
      }
      return false;
    }


    $http({
        method: 'POST',
        url: BASEURL + '/register-account',
        data: {
          register_email: $scope.brRegisterEmail,
          register_first_name: $scope.brRegisterFirstName,
          register_last_name: $scope.brRegisterLastName,
          register_user_name: $scope.brRegisterUserName,
          register_address: $scope.brRegisterAddress,
          register_zipcode: $scope.brRegisterZipCode,
          register_city: $scope.brRegisterCity,
          register_country: $scope.brRegisterCountry,
          register_password: $scope.brRegisterPassword,
          captcha: $scope.captcha,
          register_referred: $scope.brRegisterReferred,
        }
    })
    .then(function(result) {
        if (result.status != 200) {
          console.log('Unknown error. Please contact your administrator', result);
        }
        else if(result.data.error == 1 ) {
          if( ! jQuery.isEmptyObject(result.data.errors) ) {
            ctrl.formError = true;
            allerrors = "";
            for (var key in result.data.errors) {
              if (result.data.errors.hasOwnProperty(key)) {
                var doo = result.data.errors[key];
                doo.forEach(function(element) {
                  allerrors += `- ${element} <br> <br>`;
                });
              }
            }
            ctrl.mensajes(allerrors, 'error');
            console.log(result);
            // ctrl.formErrorMessage = allerrors;
          }
          else {
            console.log('Unknown error. Please contact your administrator', result);
          }

        }
        else {
          ctrl.formSubmitCompleted = true;
        }


        if(!$scope.$$phase) {
          $scope.$apply();
        }

    },function(error) {
      console.log('error',error);
    });

  }

  $(document).ready(function() {

    $('.br-ajs-select2').each(function() {

      var placeholder = $(this).data('br-placeholder');

      $(this).select2({
        border: "none",
        placeholder: placeholder,
        dropdownParent: $(this).closest('form'),
        width: '100%',
      });
    });

  });

}]);