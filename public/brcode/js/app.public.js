$(document).ready(function() {
  var $xhr = null;
   // variables, listener and functions for Login / Register
   var $showLogin = $('.showLogin')
      ,$inputUser = $('#user_name')
      ,$inputPass = $('#password')
   ,$showRegister = $('#showRegister')
     ,$modalLogin = $('#modalLogin')
    ,$submitLogin = $('#submitLogin');

    var $showlanguage = $('.showlanguage')
    ,$modalLanguage = $('#modalLanguage')

    $showlanguage.click(function() {
      $modalLanguage.find('.lang-form').show();
      $modalLanguage.modal('show');
    });

    var $showComplaint = $('.showComplaint')
    ,$modalComplaint = $('#modalComplaint')

    $showComplaint.click(function() {
      $modalComplaint.find('.complaint-form').show();
      $modalComplaint.modal('show');
    });

    var $showPaypal = $('.showPaypal')
    ,$modalPaypal = $('#modalPaypal')

    $showPaypal.click(function() {
      $modalPaypal.find('.paypal-form').show();
      $modalPaypal.modal('show');
    });

    var $showRgm = $('.showRgm')
    ,$modalRgm = $('#modalRgm')

    $showRgm.click(function() {
      $modalRgm.find('.rgm-form').show();
      $modalRgm.modal('show');
    });

    var $showBeseif = $('.showBeseif')
    ,$modalBeseif = $('#modalBeseif')

    $showBeseif.click(function() {
      $modalBeseif.find('.beseif-form').show();
      $modalBeseif.modal('show');
    });

    var $cancelPay = $('.cancelPay')
    ,$modalCancel = $('#modalCancel')

    $cancelPay.click(function() {
      $modalCancel.find('.cancel-form').show();
      $modalCancel.modal('show');
    });

    var $cancelAccept = $('.cancelAccept')
    ,$modalAcceptCancel = $('#modalAcceptCancel')

    $cancelAccept.click(function() {
      $modalAcceptCancel.find('.accept-form').show();
      $modalAcceptCancel.modal('show');
    });

    var $cancelAutomatic = $('.cancelAutomatic')
    ,$modalCancelAutomatic = $('#modalCancelAutomatic')

    $cancelAutomatic.click(function() {
      $modalCancelAutomatic.find('.the-automatic-form').show();
      $modalCancelAutomatic.modal('show');
    });

    
    var $cancelSoliPay = $('.cancelSoliPay')
    ,$modalSoliCancel = $('#modalSoliCancel')

    $cancelSoliPay.click(function() {
      $modalSoliCancel.find('.the-soli-form').show();
      $modalSoliCancel.modal('show');
    });

    var $reemPay = $('.reemPay')
    ,$reemModal = $('#reemModal')

    $reemPay.click(function() {
      $reemModal.find('.reem-form').show();
      $reemModal.modal('show');
    });

    var $reemPaySoli = $('.reemPaySoli')
    ,$reemModalSoli = $('#reemModalSoli')

    $reemPaySoli.click(function() {
      $reemModalSoli.find('.reem-soli-form').show();
      $reemModalSoli.modal('show');
    });


    var $extraPay = $('.extraPay')
    ,$ModalExtraPay = $('#ModalExtraPay')

    $extraPay.click(function() {
      $ModalExtraPay.find('.extra-pay-form').show();
      $ModalExtraPay.modal('show');
    });

    

    var $NumberFollow = $('.NumberFollow')
    ,$NumberModal = $('#NumberModal')

    $NumberFollow.click(function() {
      $NumberModal.find('.follow-form').show();
      $NumberModal.modal('show');
    });

    var $confirmDelivery = $('.confirmDelivery')
    ,$deliveryModal = $('#deliveryModal')

    $confirmDelivery.click(function() {
      $deliveryModal.find('.delivery-form').show();
      $deliveryModal.modal('show');
    });

    var $showExtraPay = $('.showExtraPay')
    ,$modalExtraPay = $('#modalExtraPay')

    $showExtraPay.click(function() {
      $modalExtraPay.find('.extra-form').show();
      $modalExtraPay.modal('show');
    });

    var $showRating = $('.showRating')
    ,$modalRating = $('#modalRating')

    $showRating.click(function() {
      $modalRating.find('.rating-form').show();
      $modalRating.modal('show');
    });

   $showLogin.click(function() {
     $modalLogin.find('.login-form').show();
     $modalLogin.find('.register-form').hide();
     $modalLogin.modal('show');
   });

   $showRegister.click(function() {
    $modalLogin.find('.login-form').hide();
    $modalLogin.find('.register-form').show();
   });

   var $cancelSoli = $('.cancelSoli')
   ,$modalCancelSoli = $('#modalCancelSoli')

   $cancelSoli.click(function() {
     $modalCancelSoli.find('.soli-form').show();
     $modalCancelSoli.modal('show');
   });

   $inputUser.keypress(function(e) {
     var code = (e.keyCode ? e.keyCode : e.which);
     if(code == 13) {
       $submitLogin.click();
     }
   });

   $inputPass.keypress(function(e) {
     var code = (e.keyCode ? e.keyCode : e.which);
     if(code == 13) {
       $submitLogin.click();
     }
   });

  $submitLogin.click(function() {

    $submitLogin.button('loading');

    var $form = $(this).closest('form'),
        data = $form.serialize();
    $form.find('.error-message').html('').hide();
    $form.find('.login-message').html('').closest('.form-group').hide();
    $form.find('.has-error').removeClass('has-error');

    if($xhr) {
      $xhr = null;
    }

    $xhr = $.ajax({
      url: $form.attr('action'),
      type: $form.attr('method'),
      data: data,
      dataType: 'JSON',
      success: function(data) {
        if(data.result == true) {

          if(typeof data.result_url !== 'undefined') {
            window.location = data.result_url;

          }
          else if (window.location.href.indexOf('account-activated') > -1 ) {
            window.location = BASEURL;

          }
          else {
            window.location = window.location.href.replace('#','');
          }
        }
        else {
          if(typeof data.errors.message == 'string') {
            $form.find('.login-message').html(data.errors.message).closest('.form-group').show();
          }
          else {
            var keys = Object.keys(data.errors);
            for(var i = 0; i < keys.length; i++) {
              $form.find('input[name="'+keys[i]+'"]').next('.error-message').show().html(data.errors[keys[i]][0]).closest('.form-group').addClass('has-error');
            }
          }
        }

        $submitLogin.button('reset');
      },

      error: function(data) {
        $submitLogin.button('reset');
      },

    })

   });

   /*****************************
   *
   *  OFFER / LOGIN
   *
   ******************************/
  $('[data-number]').each(function() {
    var decimal = $(this).data('number');
    $(this).number(true, parseInt(decimal),'.',',');
  });

  $('[data-refused]').each(function() {
    var decimal = $(this).data('number');
    console.log(decimal);
    $(this).number(true, parseInt(decimal),'.',',');

  });

  $('[data-acc-option] a').click(function(e) {
    e.preventDefault();
    var option = $(this).parent().data('acc-option');
    if(option == 'register') {
      $showLogin.click();
      $showRegister.click();
    }
    else if(option == 'login') {
      $showLogin.click();
    }
  });

  $('.textarea-counter').each(function() {
    var textarea = $(this).prev();
    var textCounter = $(this);
    var text = $(this).data('counter-text');
    var textMax = $(this).data('text-max');
    text.replace('_TOTAL_',textMax);

    textCounter.text(text.replace('_TOTAL_',textMax).replace('_QTY_',textarea.val().length));

    textarea.keypress(function(e) {
      if(this.value.length >= parseInt(textMax)) {
        e.preventDefault();
        textCounter.text(text.replace('_TOTAL_',textMax).replace('_QTY_',this.value.length));
      }
      else {
        textCounter.text(text.replace('_TOTAL_',textMax).replace('_QTY_',this.value.length));
      }
    });

    textarea.keyup(function(e) {
      this.value = this.value.substr(0,parseInt(textMax));
      textCounter.text(text.replace('_TOTAL_',textMax).replace('_QTY_',this.value.length));
    });
  });

  uploadButton = $('<button/>')
    .attr('type','button')
    .addClass('btn btn-primary')
    .text('Upload')
    .on('click', function () {
        var $this = $(this),
            data = $this.data();
        $this
            .off('click')
            .text('Abort')
            .on('click', function () {
                $this.remove();
                data.abort();
            });
        data.submit().always(function () {
            $this.remove();
        });
    });

  editButton = $('<button/>')
  .attr('type','button')
  .addClass('btn btn-primary')
  .text('Edit')
  .on('click', function () {
      var $this = $(this),
          data = $this.data();
      $this.text('Save');
      data.imgEl.Jcrop({aspectRatio: 1/1});
  });

   $('.fileupload-input').each(function() {
    $(this).fileupload({
      singleFileUploads: true,
      sequentialUploads: true,
      limitConcurrentUploads: 1,
      dropZone: $('#br-id-images-dropzone'),
      add: function (e, data) {
        $('#progress .progress-bar').css(
            'width',
            0 + '%'
        );
        data.context = $('<div/>').addClass('file-uploader-preview').appendTo('#br-id-images-dropzone');

        $.each(data.files, function (index, file) {
          console.log(file.size);
                           
            if(file.size > 1000000) {
              
              var msg = $('<p/>')
                .text(global_lang['app.uploader_file_size_bigger'].replace('_FILENAME_',file.name));
              msg.appendTo($('#file-uploader-messages'));
              data.context.remove();
              return false;
            }
            else if($.inArray(file.name.split('.').pop().toLowerCase(),['gif','png','jpg','jpeg']) == -1) {
              var msg = $('<p/>')
                .text(global_lang['app.uploader_file_ext_not_supported'].replace('_FILENAME_',file.name));
              msg.appendTo($('#file-uploader-messages'));
              data.context.remove();
              return false;
            }

            var img = $('<img/>').attr('src',URL.createObjectURL(file));
            data.imgEl = img;
            data.inputName = $('#br-id-images-dropzone').find('#fileupload-name').val();
            var node = $('<p/>')
                .append(img);
            node.appendTo(data.context);

        });

        $('#br-id-images-dropzone').addClass('br-dropzone-w-files');
        
        data.submit();
      },
      done: function (e, data) {
        if( data.result != null) {

          if(data.result.error == 0) {
            var img = data.imgEl;
            var parent = img.closest('.file-uploader-preview');
            var removeButton = $('<div/>').html('<i class="fa fa-times-circle-o"></i>').addClass('file-uploader-remove').appendTo(parent);
            var input = $('<input/>').attr('type','hidden').attr('name',data.inputName).attr('value',data.result.name).appendTo(parent);
          }
          else {

          }
        }
      },
      fail: function(e,data) {
        
        console.log(data.imgEl);
      },
      progress: function (e, data) {
          var progress = parseInt(data.loaded / data.total * 100, 10);
          $('#progress .progress-bar').css(
              'width',
              progress + '%'
          );
      },
    });
  });

  $(document).on('click','.file-uploader-remove',function() {
    var parent = $(this).closest('.file-uploader-preview');
    var image = parent.find('input[type=hidden]').val();
    $('#progress .progress-bar').css(
        'width',
        0 + '%'
    );
    $.ajax({
      url: '/account/inventory/remove_file',
      data: {'_token': $('form input[name="_token"]:eq(0)').val(), img: image},
      type: 'POST',
      success: function(data) {
        parent.hide('fade',250,function() {
          $(this).remove()
          if($('#br-id-images-dropzone .file-uploader-preview').length == 0) {
            $('#br-id-images-dropzone').removeClass('br-dropzone-w-files');
          }
        });
      },
      error: function(data) {

      }
    })
  });

  $('#br-id-images-dropzone').on('dragenter dragover', function() {
    $(this).addClass('hover');
  });

  $('#br-id-images-dropzone').on('dragleave dragexit drop', function() {
    $(this).removeClass('hover');
  });

  $('#br-inventory-add #br-publish-item').click(function(e) {
    var requiredItems = $('#br-inventory-add .required');
    var errors = false;
    requiredItems.parent().removeClass('has-error');

    for(var i = 0; i < requiredItems.length; i++) {
      if(requiredItems.eq(i).val() == '') {
        requiredItems.eq(i).parent().addClass('has-error');
        errors = true;
      }
    }

    return true;
  });

  $( "#search" ).keyup(function(e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if(code == 13) {
      location.href = '/search?text=' + $('#search').val();
    }
    var camp = this.value;
    if(camp.length > 3){
      
      $('#result').html('');
      $.ajax({
        url: "/product/search",
        data: { name: this.value }
      })
      .done(function( data ) {
        $('#result').html('');
        if(data[0].length > 0){
          $("#result").show();
          $.each(data[0], function(key, value){
            $('#result').append(`
              <a href="${value.code}" class="list-group-item list-group-item-action" style="padding:.15rem 0.25rem">
                <li class="media">
                <img src="${value.image}" height="45" width="45" class="mr-3" alt="..."/> 
                  <div class="ml-5">
                    ${value.name} <br>
                    ${value.platform}
                  </div>
                </li>
              </a>
              
              `);
            // $('#result').append(`
            //   <a href="${value.code}" class="list-group-item list-group-item-action" style="height:50px !important">
            //     <div class="row" style="margin-top: -7px !important;">
            //       <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 text-center"> 
            //         <img src="${value.image}" height="32" width="32" /> 
            //       </div> 
            //       <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 col-11"> 
            //         ${value.name}<br><small>${value.platform}</small> 
            //       </div> 
            //     </div>
            //   </a>
            //   `);
          });
          $('#result').append('<button onclick="srch()" class="list-group-item list-group-item-action" style="height:50px !important"><div class="row" style="margin-top: -5px !important;"><div class="col-lg-12"> <h5 style="margin-top: 10px;" class="text-center text-black">Show All '+data[1]+' results</h5> </div> </div></button>');
          $('.not_found_product').html('');
        }else{
          $('#result').html('<li class="list-group-item">Products not found</li>');
        }
        
      });
    }else{
      $('#result').html('');
    }
  });
  $("body").click(function() {
    $("#result").hide();
  });
  
});

$('.nk-btn-delete-inventory').click(function() {
  var url = $(this).data('href');
  var pro = $(this).data('pro');
  var tr = $(this).closest('tr');
  var selInv = $(this).closest('tr').find('select');
  var sel = $(this).closest('tr').find('select').prop('value');
  $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') } });
  $.ajax({
    url: url,
    data: {qty: sel, pro: pro},
    type: 'POST',
    success: function(data) {
      if(data.qty > 0){
        selInv.empty();
        for (var i = 1; i <= data.qty; i++) {
          selInv.append(`<option value="${i}">${i}</option>`);
        }
      }else{
        tr.closest('tr').remove();
      }
    },
    error: function(data) {
      console.log('error')
    }
  })
  //console.log(url);
  /*
  $.get(url, function(data, status){
    tr.closest('tr').remove();
  });*/
});
$('.nk-btn-modify-inventory').click(function() {
  var url = $(this).data('href');
  var tr = $(this).closest('tr');
  var sel = $(this).closest('tr').find('select').prop('value');
  console.log(sel);
  //console.log(url);
  location.href = `${url}/${sel}`;
});

$('.search-btn-find').click(function() {
  location.href = '/search?text=' + $('#search').val();
});
function srch(){
  location.href = '/search?text=' + $('#search').val();
}
$('.search-btn-show-form').click(function() {
  
  $('.nk-search-form-validate').toggle();
  $('input[name=s]').focus();
});
if($("input[name='qty_pro']").val() > 0){
  $( "input[name='br_c_quantity']" ).val($("input[name='qty_pro']").val());
}


function upImage(x){
  document.getElementById(x).style.display = 'block';
  //$('#' + x).show();
}

function downImage(x){
  $(x).hide();
}

var hash = window.location.hash;
if(hash == '#sent'){
  $('a[href*="#tabs-1-2"]').click();
}