angularApp.controller('registerCompanyController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {

  var ctrl = this;

  ctrl.formError        = false;
  ctrl.formErrorMessage = '';

  ctrl.submitForm = function(form) {

    ctrl.formError          = false;
    ctrl.formErrorMessage   = '';

    if ( form.$invalid ) return false;

    $http({
        method: 'POST',
        url: BASEURL + '/register-company-account',
        data: {
          register_email: $scope.brRegisterCoEmail,
          register_first_name: $scope.brRegisterCoFirstName,
          register_last_name: $scope.brRegisterCoLastName,
          register_user_name: $scope.brRegisterCoUserName,
          register_address: $scope.brRegisterCoAddress,
          register_zipcode: $scope.brRegisterCoZipCode,
          register_city: $scope.brRegisterCoCity,
          register_country: $scope.brRegisterCoCountry,
          register_password: $scope.brRegisterCoPassword,
          register_name: $scope.brRegisterCoName,
          register_vat: $scope.brRegisterCoVat,
          register_phone: $scope.brRegisterCoPhone,
        }
    })
    .then(function(result) {
        if (result.status != 200) {

          console.log('Unknown error. Please contact your administrator', result);
        }
        else if(result.data.error == 1 ) {

          if( ! jQuery.isEmptyObject(result.data.errors) ) {
            ctrl.formError = true;

            if( result.data.errors.hasOwnProperty('register_email') )
              ctrl.formErrorMessage = GLOBAL_TRANSLATION['El correo introducido ya existe en nuestro sistema'];
            else if ( result.data.errors.hasOwnProperty('register_user_name') )
              ctrl.formErrorMessage = GLOBAL_TRANSLATION['El nombre de usuario introducido ya existe en nuestro sistema'];
            else if ( result.data.errors.hasOwnProperty('register_password') ){
              ctrl.formErrorMessage = GLOBAL_TRANSLATION['La contraseña debe contener minimo 8 caracteres'];
            }else {
              ctrl.formErrorMessage = GLOBAL_TRANSLATION['Error desconocido. Por favor contacte a su administrador'];
            }

          }
          else {
            console.log('Unknown error. Please contact your administrator', result);
          }

        }
        else {
          ctrl.formSubmitCompleted = true;
        }


        if(!$scope.$$phase) {
          $scope.$apply();
        }

    },function(error) {
      console.log('error',error);
    });

  }

  $(document).ready(function() {

    $('.br-ajs-select2').each(function() {

      var placeholder = $(this).data('br-placeholder');

      $(this).select2({
        border: "none",
        placeholder: placeholder,
        width: '100%',
      });
    });

  });

}]);
