(function () {
    "use strict";

    var cookieAlert = document.querySelector(".cookiealert");
    var acceptCookies = document.querySelector(".acceptcookies");
    var denyCookies = document.querySelector(".denycookies");

    if (!cookieAlert) {
       return;
    }

    cookieAlert.offsetHeight; // Force browser to trigger reflow

    // Show the alert if we can't find the "acceptCookies" or "denyCookies" cookies
    if (!getCookie("AC") && !getCookie("DC")) {
        cookieAlert.classList.add("show");
    }

    // When clicking on the agree button, create a 1-year cookie to remember the user's choice and close the banner
    acceptCookies.addEventListener("click", function () {
        setCookie("AC", true, 365);
        cookieAlert.classList.remove("show");
    });

    // When clicking on the deny button, create a cookie to remember that the user denied cookies and close the banner
    denyCookies.addEventListener("click", function () {
        setCookie("DC", true, 365);
        cookieAlert.classList.remove("show");
    });

    // Cookie functions from w3schools
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
})();
