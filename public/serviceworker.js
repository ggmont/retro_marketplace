var staticCacheName = "pwa-v" + new Date().getTime();
var filesToCache = [
  '/offline',
  // '/css/app.css',
  // '/js/app.js',
];

self.addEventListener("install", event => {
  this.skipWaiting();
  event.waitUntil(
    caches.open(staticCacheName)
      .then(cache => {
        return cache.addAll(filesToCache);
      })
  );
});

self.addEventListener('activate', event => {
  event.waitUntil(
    caches.keys().then(cacheNames => {
      return Promise.all(
        cacheNames
          .filter(cacheName => (cacheName.startsWith("pwa-")))
          .filter(cacheName => (cacheName !== staticCacheName))
          .map(cacheName => caches.delete(cacheName))
      );
    })
  );
});

self.addEventListener("fetch", event => {
  event.respondWith(
    // Comment the caching logic
    // caches.open(staticCacheName)
    //   .then(cache => {
    //     return cache.match(event.request)
    //       .then(response => {
    //         const fetchPromise = fetch(event.request)
    //           .then(networkResponse => {
    //             if (networkResponse && networkResponse.status === 200) {
    //               cache.put(event.request, networkResponse.clone());
    //             }
    //             return networkResponse;
    //           })
    //           .catch(() => {
    //             return caches.match('offline');
    //           });
    //
    //         return response || fetchPromise;
    //       });
    // })
    fetch(event.request) // Fetch the request directly from the network
  );
});

// The rest of your Service Worker code
// ...
