require('./bootstrap');

import Vue from 'vue'
import Vuex from 'vuex'
import store from './store'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'


// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
/*
import VueSocketio from 'vue-socket.io-extended'
import socketio from 'socket.io-client'
*/
Vue.use(Vuex)

Vue.component('message-conversation-component', 
	require('./components/MessageConversationComponent.vue').default
);
Vue.component('active-conversation-component', 
	require('./components/ActiveConversationComponent.vue').default
);
 
Vue.component('new-conversation-component', 
	require('./components/NewConversationComponent.vue').default
);
Vue.component('contact-component', 
	require('./components/ContactComponent.vue').default
);
Vue.component('contact-list-component', 
	require('./components/ContactListComponent.vue').default
);
Vue.component('messenger-component', 
    require('./components/MessengerComponent.vue').default
);

const app = new Vue({
	el: '#appMarketPlace',
	created() {
		Echo.channel('notification')
		.listen('MessageNotification' , (e) => {
			alert('DALE!');
		});
	}
});
