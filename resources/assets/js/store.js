import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
          messages: [],
          selectedConversation: null,
          conversations: [],
          con: null,
          user: null,
          mes: Object
      },
  
      mutations: {
          setUser(state, user) {
              state.user = user;
          },
          newMessagesList(state, messages) {
                  state.messages = messages;
          },
  
          selectConversation(state, conversation) {
              state.selectedConversation = conversation;
          },
  
          newConversationsList(state, conversations) {
              state.conversations = conversations;
          },
  
          newConversationsListNew(state, conversations) {
              state.conversations = conversations;
              axios.get(`/account/api/messages?contact_id=${state.conversations[0].alias_contact_id}`)
              .then((response) => {
                  state.selectedConversation = state.conversations[0];
                  state.messages = response.data;
              });
          },
  
          addMessage(state, message) {
              
              const conversation = state.conversations.find( (conversation) => {
                      return conversation.alias_contact_id == message.alias_from_id || 
                              conversation.alias_contact_id == message.alias_to_id;
              });
              
              if (conversation){
                  const author = state.user.user_name === message.alias_from_id ? 'Tú' : conversation.contact_name;
              
                  conversation.last_message = `${author}: ${message.content}`;
                  conversation.last_time = message.created_at;
  
                  if (state.selectedConversation.alias_contact_id == message.alias_from_id
                          || state.selectedConversation.alias_contact_id == message.alias_to_id){
                              state.messages.push(message);
                  }
                          
              }
          },
  
          removeMessage(state, message){
              let index = state.messages.indexOf(message);
              state.messages.splice(index, 1);
          }
  
      },
  
      actions: {
  
          getMessages(context, conversation) {
              axios.get(`/account/api/messages?contact_id=${conversation.alias_contact_id}`)
              .then((response) => {
                      context.commit('selectConversation', conversation);
                      context.commit('newMessagesList', response.data);
              });
          },
          
          getConversations(context) {
              return axios.get('/account/api/conversations')
                  .then(response => 
                      context.commit('newConversationsList', response.data)
              );
          },
  
          getConversationsNew(context) {
              return axios.get('/account/api/conversations')
                  .then(response => 
                      context.commit('newConversationsListNew', response.data)
              );
          },
          
          scrollToBottom(context) {
              const el = document.querySelector('.msg_history');
              $(el).scrollTop( el.scrollHeight );
          },
  
          allSMS(context) {
              const params = {
                  msg: context.state.selectedConversation.alias_contact_id
              };
  
              axios.post('/account/api/messages/all', params)
              .then(response => {
                      if (response.data.success) {
                          state.messages = [];
                          state.selectedConversation = null;
                      }
              });
          },
  
          delMessage(context, message){
              const params = {
                  msg: message.id
              };
              axios.post('/account/api/messages/del', params)
              .then(response => {
                      if (response.data.success) {
                          context.commit('removeMessage', message);
                      }
              });
              
          },
  
          postMessage(context, newMessage) {
  
              const params = {
                      alias: context.state.selectedConversation.alias_contact_id,
                      content: newMessage
              };
              axios.post('/account/api/messages', params)
              .then(response => {
                      if (response.data.success) {
                              newMessage = '';
                              const message = response.data.message;
                              message.written_by_me = true;
                              context.commit('addMessage', message);
                      }
              });
          }
  
      }
      
  });