<?php

return [
    
	// Header
    'agree' 		=> 'Permitir Cookies',
    'text' 			=> '<span class="font-bold text-lg"><b>Te gustan las cookies?</b> &#x1F36A; Usamos cookies para asegurarnos de obtener la mejor experiencia en nuestro sitio web. </span>&nbsp&nbsp<a href="/site/politica-de-cookies" target="_blank"><span class="font-bold text-2xl">Aprender más</span></a><br>',

];
