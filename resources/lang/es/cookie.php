<?php

return [
    
	// Header
    'agree' 		=> 'Permitir Cookies',
    'decline' 		=> 'No Permitir',
    'text' 			=> '<span class="font-bold text-sm"><b>Te gustan las cookies?</b> &#x1F36A; Usamos cookies para agilizar la navegación en nuestro sitio web, almacenando información sobre visitas y visualizaciones. En ningún caso estas cookies se asocian a tu usuario ni se utilizan con fines de seguimiento. </span><br>',

];
