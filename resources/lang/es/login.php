<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'title'                   => 'Registrarse',
    'title_create_new_pwd'    => 'Crear nueva contraseña',
    'title_reset_your_pwd'    => 'Restablecer su contraseña',
    'title_register'          => 'Registre su membresía',
    'title_resend'            => 'Enlace de activación',


    'account_disabled'        => 'Tu cuenta ha sido desactivada.',
    'sign_in_message'         => 'Ingrese sus credenciales para iniciar sesión.',
    'password_reset_message'  => 'Ingrese su correo electrónico para restablecer su contraseña.',
    'user_name'               => 'Nombre de usuario',
    'user_full_name'          => 'Nombre completo',
    'user_first_name'         => 'Primer nombre',
    'user_last_name'          => 'Apellido',
    'user_email'              => 'Correo electrónico',
    'password'						    => 'Contraseña',
    'password_retype'         => 'Vuelva a escribir la contraseña',
    'remember_me'					    => 'Recuérdame',
    'sign_in'                 => 'Registrarse',
    'forgot_password'         => 'Olvidé mi contraseña',
    'register_new_acc'        => 'Registrar una nueva membresía',
    'register'                => 'Registro',
    'agree_to_terms'          => 'Estoy de acuerdo con la <a href="_URL_">terms</a>',
    'already_have_membership' => 'Ya tengo membresia',
    'reset_my_pass'           => 'Envíame un enlace para restablecer la contraseña',
    'email_not_registered'    => 'El correo electrónico no está registrado en nuestra base de datos',
    'confirm_new_password'    => 'Confirmar nueva contraseña',
    'create_new_password_message' => 'Ingrese su nueva contraseña y confirmación',
    'pwd_changed_successfully'=> 'Tu contraseña ha sido cambiada exitosamente',
    'invalid_new_pwd_request' => 'Tu solicitud no es válida! Por favor vuelve a la pantalla de inicio de sesión',
    'back_to_login_screen'    => 'Regresa',
    'email_pwd_reset_unrelated'=> 'El correo electrónico no está relacionado con la solicitud de restablecimiento de contraseña',


    // Activate Account
    'activate_acc_title'      => 'Hola',
    'activate_acc_link'       => 'Activar enlace de cuenta',
    'activate_acc_message'    => 'Hemos recibido su solicitud para crear una nueva cuenta utilizando este correo electrónico. Para activar su cuenta debe hacer clic en el siguiente enlace:',
    'activate_acc_sent'       => 'Le hemos enviado un correo electrónico para completar el proceso de activación de su cuenta.',
    'activate_acc_pending'    => 'Su cuenta no ha sido activada. Por favor revise su bandeja de entrada. A veces los nuevos correos electrónicos son detectados como Spam. Podría estar en tu carpeta de Spam. <br/><br/>Si no puede encontrar el correo electrónico de activación, puedes <a href="_URL_">Enviar un nuevo correo de activacion.</a>.',
        
    'activate_acc_success'    => 'Tu cuenta ha sido activada.',
    'activate_acc_resend'     => 'Ingrese su correo electrónico para recibir un nuevo enlace de activación',
    'activate_acc_resend_btn' => 'Envíame un nuevo enlace de activación',


    // Reset email
    'reset_user_rgm'              => 'Volver a RGM',
    'reset_link'              => 'Restablecer enlace de contraseña',
    'reset_title'             => 'Hola',
    'reset_solution'             => 'Su Nombre de usuario es',
    'reset_message'           => 'Hemos recibido una solicitud de restablecimiento de contraseña utilizando este correo electrónico. Para restablecer su contraseña debe hacer clic en el siguiente enlace:',
    'password_reset_sent'     => 'Las instrucciones para restablecer su contraseña han sido enviadas a su correo electrónico.',
    'reset_user_message'           => 'Hemos recibido una solicitud para saber su nombre de usuario olvidada',

];
