<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'El ":attribute" debe ser aceptado.',
    'active_url'           => 'El ":attribute" no es una URL válida.',
    'after'                => 'El ":attribute" debe ser una fecha después de: fecha.',
    'alpha'                => 'El ":attribute" solo puede contener letras',
    'alpha_dash'           => 'El ":attribute" solo puede contener letras, números y guiones.',
    'alpha_num'            => 'El ":attribute" solo puede contener letras y números.',
    'array'                => 'El ":attribute" debe ser una matriz.',
    'before'               => 'El ":attribute" debe ser una fecha anterior a: fecha.',
    'between'              => [
        'numeric' => 'El ":attribute"  debe estar entre: min y: max.',
        'file'    => 'El ":attribute" debe estar entre: mínimo y: máximo kilobytes.',
        'string'  => 'El ":attribute" debe estar entre: min y: max caracteres.',
        'array'   => 'El ":attribute" debe tener entre: min y: max.',
    ],
    'boolean'              => 'El ":attribute" debe ser verdadero o falso..',
    'confirmed'            => 'El ":attribute" la confirmación no coincide.',
    'date'                 => 'El ":attribute" no es una fecha válida.',
    'date_format'          => 'El ":attribute" no coincide con el formato: formato.',
    'different'            => 'El ":attribute" y: el otro debe ser diferente.',
    'digits'               => 'El ":attribute" debe ser: dígitos dígitos.',
    'digits_between'       => 'El ":attribute" debe estar entre: min y: max dígitos.',
    'distinct'             => 'El ":attribute" tiene un valor duplicado.',
    'email'                => 'El ":attribute" debe ser una dirección de correo electrónico válida.',
    'exists'               => 'El seleccionado":attribute" no es válido.',
    'filled'               => 'El ":attribute" se requiere campo.',
    'image'                => 'El ":attribute" debe ser una imagen',
    'in'                   => 'El seleccionado ":attribute" no es válido.',
    'in_array'             => 'El ":attribute" campo no existe en: otro.',
    'integer'              => 'El ":attribute" debe ser un entero.',
    'ip'                   => 'El ":attribute" debe de ser una dirección Ip válida',
    'json'                 => 'El ":attribute" debe ser una cadena JSON válida.',
    'max'                  => [
        'numeric' => 'El ":attribute" no puede ser mayor que: max.',
        'file'    => ' El ":attribute" no puede ser mayor que: max kilobytes.',
        'string'  => 'El ":attribute" no puede ser mayor que: max caracteres.',
        'array'   => 'El ":attribute" no puede tener más de: max artículos.',
    ],
    'mimes'                => 'El ":attribute" debe ser un archivo de tipo:: valores.',
    'min'                  => [
        'numeric' => 'El ":attribute" debe ser al menos: :min min.',
        'file'    => 'El ":attribute" debe ser de al menos: kilobytes :min.',
        'string'  => 'El":attribute" debe tener al menos: :min caracteres.',
        'array'   => 'El ":attribute" must have at least :min items.',
    ],
    'not_in'               => 'El seleccionado ":attribute" no es válido',
    'numeric'              => 'El ":attribute" tiene que ser un número.',
    'present'              => 'El":attribute" campo debe estar presente',
    'regex'                => 'El ":attribute" formato no es válido',
    'required'             => 'El ":attribute" es un campo obligatorio.',
    'required_if'          => 'El ":attribute" campo es obligatorio cuando: otro es: valor.',
    'required_unless'      => 'El ":attribute" campo es obligatorio a menos que: otro esté en: valores.',
    'required_with'        => 'El ":attribute" campo es obligatorio cuando: valores está presente.',
    'required_with_all'    => 'El ":attribute" campo es obligatorio cuando: valores está presente.',
    'required_without'     => 'El ":attribute" campo es obligatorio cuando: valores no está presente.',
    'required_without_all' => 'El ":attribute" campo es obligatorio cuando ninguno de: valores está presente.',
    'same'                 => 'El ":attribute" y: otros deben coincidir',
    'size'                 => [
        'numeric' => 'El ":attribute" debe ser: tamaño.',
        'file'    => 'El ":attribute" debe ser : de tamaño kilobytes ',
        'string'  => 'El ":attribute" debe ser: caracteres de tamaño.',
        'array'   => 'El ":attribute" debe contener: elementos de tamaño.',
    ],
    'string'               => 'El ":attribute" debe ser una cadena',
    'timezone'             => 'El ":attribute" debe ser una zona válida.',
    'unique'               => 'El campo ":attribute" ya se ha tomado.',
    'url'                  => 'El ":attribute" formato no es válido',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'unique_two_columns'  => 'The combination of ":field1" and ":field2" is not unique',

    'g-recaptcha-response'             => 'Captcha inválido',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
