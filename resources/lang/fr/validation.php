<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Le ":attribute" doit être accepté',
    'active_url'           => 'Le ":attribute" n"est pas une URL valide.',
    'after'                => 'Le ":attribute" doit être une date après fecha.',
    'alpha'                => 'Le ":attribute" ne peut contenir que des lettres',
    'alpha_dash'           => 'Le ":attribute" ne peut contenir que des lettres, des chiffres et des traits d"union.',
    'alpha_num'            => 'Le ":attribute" ne peut contenir que des lettres et des chiffres.',
    'array'                => 'Le ":attribute" doit être un tableau.',
    'before'               => 'Le ":attribute" doit être une date avant: fecha.',
    'between'              => [
        'numeric' => 'Le ":attribute" doit être compris entre min et: max.',
        'file'    => 'Le ":attribute" doit être compris entre : mínimo et: máximo kilobytes.',
        'string'  => 'Le ":attribute" doit être compris entre les min et: max caracteres.',
        'array'   => 'Le ":attribute" doit avoir entre: min et: max.',
    ],
    'boolean'              => 'Le ":attribute" doit être vrai ou faux.',
    'confirmed'            => 'La confirmation ":attribute" ne correspond pas.',
    'date'                 => 'Le ":attribute" n"est pas une date valide.',
    'date_format'          => 'Le ":attribute" ne correspond pas au format : formato.',
    'different'            => 'Le ":attribute" et: l"autre doit être différent.',
    'digits'               => 'Le ":attribute" doit être: dígitos dígitos.',
    'digits_between'       => 'Le ":attribute" doit être compris entre: min et: max dígitos.',
    'distinct'             => 'Le ":attribute" a une valeur en double.',
    'email'                => 'Le ":attribute" doit être une adresse e-mail valide.',
    'exists'                => 'Le ":attribute" sélectionné n"est pas valide.',
    'filled'               => 'Le champ ":attribute" est obligatoire.',
    'image'                => 'Le ":attribute" doit être une image',
    'in'                   => 'Le ":attribute" sélectionné n"est pas valide.',
    'in_array'             => 'Le champ ":attribute" n"existe pas dans: otro.',
    'integer'              => 'Le ":attribute" doit être un entier.',
    'ip'                   => 'Le ":attribute" doit être une adresse IP valide',
    'json'                 => 'Le ":attribute" doit être une chaîne JSON valide.',
    'max'                  => [
        'numeric' => 'Le ":attribute" ne peut être supérieur à: max.',
        'file'    => ' Le ":attribute" ne peut être supérieur à: max kilobytes.',
        'string'  => 'Le ":attribute" ne peut être supérieur à: max caracteres.',
        'array'   => 'Le ":attribute" ne peut pas avoir plus de: max artículos.',
    ],
    'mimes'                => 'Le ":attribute" debe ser un archivo de tipo:: valores.',
    'min'                  => [
        'numeric' => 'Le ":attribute" doit avoir au moins: :min min.',
        'file'    => 'Le ":attribute" doit avoir au moins: kilobytes :min.',
        'string'  => 'Le":attribute" doit avoir au moins: :min caracteres.',
        'array'   => 'Le ":attribute" doit avoir au moins :min items.',
    ],
    'not_in'               => 'La sélection ":attribute" Ce n"est pas valide',
    'numeric'              => 'Le ":attribute" Ce doit être un nombre.',
    'present'              => 'Le":attribute" le champ doit être présent',
    'regex'                => 'Le ":attribute" le format n"est pas valide',
    'required'             => 'Le ":attribute" est un champ obligatoire.',
    'required_if'          => 'Le ":attribute" champ est obligatoire lorsque: autre vaut: valor.',
    'required_unless'      => 'Le ":attribute" champ est obligatoire sauf si: autre est dans: valores.',
    'required_with'        => 'Le ":attribute" champ est obligatoire lorsque : values ​​​​est présent.',
    'required_with_all'    => 'Le ":attribute" champ est obligatoire lorsque : values ​​​​est présent.',
    'required_without'     => 'Le ":attribute" champ est obligatoire lorsque : values ​​​​est présent.',
    'required_without_all' => 'Le ":attribute" champ est obligatoire lorsqu"aucun des: valores está presente.',
    'same'                 => 'Le ":attribute" y: les autres doivent correspondre',
    'size'                 => [
        'numeric' => 'Le ":attribute" doit être: tamaño.',
        'file'    => 'Le ":attribute" doit être : de tamaño kilobytes ',
        'string'  => 'Le ":attribute" doit être: caracteres de tamaño.',
        'array'   => 'Le ":attribute" doit contenir: elementos de tamaño.',
    ],
    'string'               => 'Le ":attribute" doit être une chaîne',
    'timezone'             => 'Le ":attribute" doit être une zone valide.',
    'unique'               => 'Le campagne ":attribute" a déjà été pris.',
    'url'                  => 'Le ":attribute" le format n"est pas valide',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'unique_two_columns'  => 'La combinaison de ":field1" et ":field2" n"est pas unique',

    'captcha'             => 'Captcha invalide',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
