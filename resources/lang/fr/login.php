<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'title'                   => 'Enregistrement',
    'title_create_new_pwd'    => 'Créer un nouveau mot de passe',
    'title_reset_your_pwd'    => 'Restablecer su contraseña',
    'title_register'          => 'Registre su membresía',
    'title_resend'            => 'Enlace de activación',


    'account_disabled'        => 'Votre compte a été désactivé.',
    'sign_in_message'         => 'Entrez vos identifiants pour vous connecter.',
    'password_reset_message'  => 'Entrez votre e-mail pour réinitialiser votre mot de passe.',
    'user_name'               => 'Nom d utilisateur',
    'user_full_name'          => 'Nom complet',
    'user_first_name'         => 'Prénom',
    'user_last_name'          => 'Le nom',
    'user_email'              => 'Courrier électronique',
    'password'                => 'Mot de passe',
    'password_retype'         => 'retaper le mot de passe',
    'remember_me'   	      => 'Souviens-toi de moi',
    'sign_in'                 => 'Enregistrement',
    'forgot_password'         => 'J"ai oublié mon mot de passe',
    'register_new_acc'        => 'Enregistrez une nouvelle adhésion',
    'register'                => 'Disque',
    'agree_to_terms'          => 'Je suis d"accord avec le <a href="_URL_">terms</a>',
    'already_have_membership' => 'J"ai déjà un abonnement',
    'reset_my_pass'           => 'Envoyez-moi un lien pour réinitialiser le mot de passe',
    'email_not_registered'    => 'L"e-mail n"est pas enregistré dans notre base de données',
    'confirm_new_password'    => 'Confirmer le nouveau mot de passe',
    'create_new_password_message' => 'Entrez votre nouveau mot de passe et confirmation',
    'pwd_changed_successfully'=> 'Votre mot de passe a été changé avec succès',
    'invalid_new_pwd_request' => 'Votre demande n"est pas valide ! Veuillez revenir à l"écran de connexion',
    'back_to_login_screen'    => 'Revenu',
    'email_pwd_reset_unrelated'=> 'L"e-mail n"est pas lié à la demande de réinitialisation du mot de passe',


    // Activate Account
    'activate_acc_title'      => 'Salut',
    'activate_acc_link'       => 'Activer le lien du compte',
    'activate_acc_message'    => 'Nous avons reçu votre demande de création d"un nouveau compte à l"aide de cet e-mail. Pour activer votre compte vous devez cliquer sur le lien suivant :',
    'activate_acc_sent'       => 'Nous vous avons envoyé un e-mail pour terminer le processus d"activation de votre compte.',
    'activate_acc_pending'    => 'Votre compte n"a pas été activé. Veuillez vérifier votre boîte de réception. Parfois, de nouveaux e-mails sont détectés comme spam. Il pourrait être dans votre dossier spam. <br/><br/>Si vous ne trouvez pas l"e-mail d"activation, vous pouvez <a href="_URL_">Envoyez un nouvel e-mail d"activation.</a>.',
        
    'activate_acc_success'    => 'Votre compte a été activé.',
    'activate_acc_resend'     => 'Entrez votre email pour recevoir un nouveau lien d"activation',
    'activate_acc_resend_btn' => 'Envoyez-moi un nouveau lien d"activation',


    // Reset email
    'reset_user_rgm'              => 'Retour à RGM',
    'reset_link'              => 'Lien de réinitialisation du mot de passe',
    'reset_title'             => 'Salut',
    'reset_solution'             => 'Votre nom d"utilisateur est',
    'reset_message'           => 'Nous avons reçu une demande de réinitialisation de mot de passe via cet e-mail. Pour réinitialiser votre mot de passe vous devez cliquer sur le lien suivant :',
    'password_reset_sent'     => 'Les instructions pour réinitialiser votre mot de passe ont été envoyées à votre adresse e-mail.',
    'reset_user_message'           => 'Nous avons reçu une demande pour connaître votre nom d"utilisateur oublié',

];
