<?php

return [
    
	// Header
    'agree' 		=> 'Autorise les cookies',
    'text' 			=> '<span class="font-bold text-lg"><b>Aimez-vous les cookies?</b> &#x1F36A; Nous utilisons des cookies pour vous garantir la meilleure expérience sur notre site Web.</span>&nbsp&nbsp<a href="/site/politica-de-cookies" target="_blank"><span class="font-bold text-2xl">Apprendre encore plus</span></a><br>',

];
