<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Organization Language Lines
    |--------------------------------------------------------------------------
    |
    */
    'marketplace'                       => 'RetroGamingMarket',

    // menu
    'my_store'                          => 'My Store',
    'my_messages'                       => 'Messages',
    'my_inventory'                      => 'Inventory',
    'my_orders'                         => 'Orders',
    'my_invoices'                       => 'Invoices',

    // Org info
    'col_company_name'                  => 'Company name',
    'col_address_line_1'                => 'Address line 1',
    'col_address_line_2'                => 'Address line 2',

    'edit_organization'                 => 'Edit organization',
    'organization_info_modified'        => 'Organization\\\'s info modified successfully',

    // Products
    'products'                          => 'Products',
    'product'                           => 'product',
    'col_release_date'                  => 'Release date',
    'col_platform'                      => 'Platform',
    'col_region'                        => 'Region',
    'col_prod_category_id'              => 'Produt category',
    'col_game_category'                 => 'Game category',
    'col_game_location'                 => 'Game location',
    'col_game_language'                 => 'Game language',
    'col_game_generation'               => 'Game generation',
    'images'                            => 'Images',

    // Shipping
    'shipping'                         => 'Shipping Cost',

    // Inventory
    'inventory'                         => 'Inventory',
    'inventory_ean'                     => 'Inventory EAN',
    'inventory_item'                    => 'Inventory item',
    'inventories'                       => 'Inventory items',
    'col_product_name'                  => 'Game',
    'col_quantity'                      => 'Quantity',
    'col_items_sold'                    => 'Items sold',

    // Category
    'category'                          => 'Category',

    // Inventory My Steor
    'col_box_condition'                 => 'Box condition',
    'col_cover_condition'               => 'Cover condition',
    'col_game_condition'                => 'Game condition',
    'col_extra_condition'               => 'Extras condition',
    'col_game_support'                  => 'Media',
    'col_ean_upc'                       => 'EAN / UPC',
    'col_measures'                      => 'Measures',
    'col_volume'                        => 'Volume',
    'col_weight'                        => 'Weight',
    'col_comments'                      => 'Media',
    'col_name_en'                       => 'Alternative - Name',
    'col_price'                         => 'Price',
    'col_status'                        => 'Status',

    // Market Pages
    'col_title'                         => 'Title',
    'col_url'                           => 'Url',
    'col_show_title'                    => 'Show title?',
    'col_parent_id'                     => 'Parent',
    'col_content'                       => 'Content',
    'pages'                             => 'Pages',
    'page'                              => 'Page',

    // Link
    'links'                             => 'Links',
    'link'                              => 'Link',
    'col_column'                        => 'Column',

    // faqs
    'col_question'                      => 'Question',
    'col_answer'                        => 'Answer',
    'faq'                               => 'Faq',

    // carousel
    'col_page_location'                 => 'Location',
    'col_in_use'                        => 'In use',
    'col_images'                        => 'Images',
    'col_car_width'                     => 'Width',
    'col_car_height'                    => 'Height',
    'carousel'                          => 'Carousel',

    // carouse items
    'carousel_images'                   => 'Carousel images',
    'col_content_title'                 => 'Title',
    'col_content_text'                  => 'Text',
    'col_slider_order'                  => 'Order',
    'col_slider_duration'               => 'Duration',

    // banners
    'col_priority'                      => 'Priority',
    'col_counter'                       => 'Counter',
    'banner'                            => 'Banner',

    // News
    'news_item'                         => 'News item',
    'col_source_url'                    => 'Source Url',
    'all'                               => 'All',
    'not_rec'                               => 'Not Received',
    'blogs_item'                               => 'Blog Item',
    'col_description'                               => 'Description',
];
