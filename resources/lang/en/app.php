<?php

return [

    /*
    |--------------------------------------------------------------------------2
    | Application Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the entire application
    |
    */

	// Header
    'main_navigation' 														=> 'MAIN NAVIGATION',
    'CONFIGURACION' => 'PRUEBA DE IDIOMA',

    //Credit
    'addCredit' 														=> 'Transferred credit',
	'removeCredit' 																=> 'Retired credit',
    'outCredit' 																=> 'Insufficient credit',
    'errorPass' 																=> 'Incorrect password',

    //Shipping
    'Nombre de envío'         => 'Name shipping',
    'Certificado'         => 'Certified',
    'Tipo de envío'         => 'Typr shipping',
    'País Origen'         => 'Origin',
    'País Destinatario'         => 'Destination',
    'Largo'         => 'Large',
    'Ancho'         => 'Width',
    'Alto'         => 'Hight',
    'Valor máximo de pedido'         => 'Max. Value	',
    'Peso máximo'         => 'Max. Weight',
    'Franqueo'         => 'Stamp price',
    'Precio'         => 'Price',
    'Cancelar pedido'         => 'Cancel order',
    'Está seguro de cancelar el pedido'         => 'Are you sure to cancel the order?',
    'Detalle del envío'         => 'Shipping detail',
    'Detalle de la venta'         => 'Sale detail',

    'Estado de envío'         => 'Shipping Status',
    'Confirmar entrega'         => 'Confirm delivery',

    'Al confirmar la entrega, usted acepta que recibió el producto satisfactoriamente.'         => 'By confirming the delivery, you accept that you received the product satisfactorily.',
    'Código de compania'         => 'Company code',
    'Número de rastreo'         => 'Tracking number',
    'Comentarios'         => 'Comments',
    'Fecha de envío'         => 'Sent on',
    'Fecha de entrega'         => 'Delivered on',
    
    //'removeCredit' 																=> 'Retired credit',

	// Sidebar
	'configuration' 														=> 'Configuration',
	'general' 																=> 'General',
    'user_management' 														=> 'User management',
    'privileges' 															=> 'Privileges',
    'roles' 																=> 'Roles',
    'users' 																=> 'Users',
    'organization' 															=> 'Organization',
    'statuses' 																=> 'Statuses',
    'search' 																=> 'Search',
    'profile' 																=> 'Profile',
    'sign_out' 																=> 'Sign out',

	// Form titles
    'home' 																	=> 'Home',
    'role' 																	=> 'Role',
    'roles' 																=> 'Roles',
    'privileges' 														=> 'Privileges',
    'privilege' 														=> 'Privilege',
    'users' 																=> 'Users',
    'user' 																	=> 'User',
    'profile_modified' 											=> 'Your profile has been modified',
    'edit_profile' 												  => 'Edit your information',

    'add_new' 															=> 'Add new',
    'edit_existing' 												=> 'Edit',
    'related_objects' 											=> 'Related objects',

	// Select general values
    'yes' 																	=> 'Yes',
    'no' 																	=> 'No',

    'is_enabled' 															=> 'Enabled',
    'is_activated' 													  => 'Is Activated?',
    'enabled' 																=> 'Enabled',
    'disabled' 																=> 'Disabled',
    'status' 																=> 'Status',
    'statuses' 																=> 'Statuses',

	// Form bottom buttons
	'action' 																=> 'Action',
	'edit' 																	=> 'Edit',
	'add' 																	=> 'Add new record',
	'submit' 																=> 'Submit',
	'confirm' 															=> 'Confirm',
  'created_at'                            => 'Created at',
  'modified_at'                           => 'Modified at',
  'deleted_at'                            => 'Deleted at',
	'ok' 																    => 'OK',
    'cancel' 																=> 'Cancel',
    'Pagar orden' 																=> 'Pay order',
    'delete' 																=> 'Delete',
    'delete_are_you_sure' 									=> 'WARNING: The record will be deleted!',
    'image_change' 									=> 'Success: The record was modified!',
    'delete_confirmation' 									=> 'Delete confirmation',

	// Table
	'field_not_allow_repeats'							=> 'This column does not allow repeated selections',

     // General column names
    'addCre' 																=> 'Credit',
    'minusCre' 																=> 'Credit',
    'col_id' 																=> 'Id',
    'col_name' 																=> 'Name',
    'col_record_status' 													=> 'Enabled',
    'col_description' 														=> 'Description',
    'col_privilege_id' 														=> 'Privilege',
    'col_code' 																=> 'Code',
    'col_status' 															=> 'Status',
    'col_status_for' 														=> 'Status for',
    'col_value' 															=> 'Value',
    'col_category' 															=> 'Category',

    // Users - List
    'Saldo' 															    => 'Saldo',
    'Concept' 															=> 'Transfer Reason',
    'col_first_name' 														=> 'First name',
    'col_last_name' 														=> 'Last name',
    'col_email' 															=> 'Email',
    'col_user_name' 														=> 'Username',
    'col_company_id' 														=> 'Company ID',
    'col_password' 															=> 'Password',
    'col_password_confirmation' 											=> 'Confirmation',
    'col_role_id' 															=> 'Role',
    'col_is_enabled' 														=> 'Enabled',

	// Inbound

	// Messages
	'you_have_no_access'													  => 'You have no access to perform the requested operation',
	'created_successfully'													=> 'created successfully',
	'modified_successfully'													=> 'modified successfully',
	'deleted_successfully'													=> 'deleted successfully',

  // Dashboard
    'dashboard'																			=> 'Dashboard',
    'withdrawals'																			=> 'Withdrawals',
	'control_panel'																	=> 'Control Panel',
	'more_info'																			=> 'More info',
  'information'                                   => 'information',
  'view'                                   			  => 'View',

  // Profile
  'image_alt_profile'                             => 'User profile picture',
  'profile_select_photo'                          => 'Select photo',
  'profile_remove_photo'                          => 'Remove photo',
  'no_image'                                      => 'No image',
  'drop_files_here'                               => 'Drop files here',

  // Lists
  'lists'                                         => 'Lists',
  'col_value_id'                                  => 'Value ID',
  'list_value'                                    => 'List value',

  // Organizations
  'col_administrator'                             => 'Administrator',
  'organizations'                                 => 'Organizations',
  'country_id'                                    => 'Country',
  'country_state'                                 => 'State',
  'zip_code'                                      => 'Zip code',

  // Stripe
  'stripe'                                        => 'Stripe',

  // Google Maps
  'google_maps'                                   => 'Google Maps',

  // errors
  'error_unknown'                                 => 'There was an errow processing your request. Please contact your administrator or try again later.',

  // Additional
  'stores'                            => 'Stores',
  'marketplace'                       => 'RetroGamingMarket',
  'category'                          => 'Category',
  'categories'                        => 'Categories',
  'products'                          => 'Products',
  'shipping'                          => 'Shippings',
  'inventory'                         => 'Inventory',
  'inventory ean'                     => 'Inventory EAN',
  'profile'                           => 'Profile',
  'orders'                            => 'Orders',
  'messages'                          => 'Messages',

  'front_end'                         => 'Front end',
  'pages'                             => 'Pages',
  'footer_links'                      => 'Footer links',
  'links'                             => 'Links',
  'faqs'                              => 'Faqs',
  'carousels'                         => 'Carousels',
  'banners'                           => 'Banners',
  'news'                              => 'News',


  'col_category_text'                 => 'Category Text',
  'col_category_text'                 => 'Category Text',
  'col_order_by'                      => 'Order position',
  'col_is_parent'                     => 'Is Parent?',
  'col_parent_id'                     => 'Parent',
  'col_image'                         => 'Image',

  //Shipping Col
  'col_name'                 => 'Name',
  'col_certified'                 => 'Certified',
  'col_max_value'                         => 'Max. Value',
  'col_max_weight'                         => 'Max. Weight',
  'col_volume'                      => 'Volume',
  'col_stamp_price'                     => 'Stamp Price',
  'col_price'                     => 'Price',
  'edit'                     => 'Edit',

  // Contact form
  'contact_form_title'                => 'New contact form message',

  // editor
  'toogle_editor'                     => 'Toogle Editor',
  'full_screen'                       => 'Full Screen',

  // File Upload
  'select_file'                       => 'Select file',
  'remove_file'                       => 'Remove file',

  // PayPal
  'payment_not_approved'              => 'Payment not approved',
  'paypal_error'                      => 'There was an error processing your request. Please try again later',

  'all'                               => 'All',
  'not_rec'                               => 'Not Received',
  'blogs'                               => 'Blogs',
  'col_is_top'                               => 'Is top?',
];
