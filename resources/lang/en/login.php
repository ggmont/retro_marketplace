<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'title'                   => 'Sign in',
    'title_create_new_pwd'    => 'Create new password',
    'title_reset_your_pwd'    => 'Reset your password',
    'title_register'          => 'Register your membership',
    'title_resend'            => 'Activation link',


    'account_disabled'        => 'Your account has been disabled.',
    'sign_in_message'         => 'Enter your credentials to sign in.',
    'password_reset_message'  => 'Enter your email to reset your password.',
    'user_name'               => 'User name',
    'user_full_name'          => 'Full name',
    'user_first_name'         => 'First name',
    'user_last_name'          => 'Last name',
    'user_email'              => 'Email',
    'password'						    => 'Password',
    'password_retype'         => 'Retype password',
    'remember_me'					    => 'Remember me.',
    'sign_in'                 => 'Sign in',
    'forgot_password'         => 'I forgot my password',
    'register_new_acc'        => 'Register a new membership',
    'register'                => 'Register',
    'agree_to_terms'          => 'I agree to the <a href="_URL_">terms</a>',
    'already_have_membership' => 'I already have a membership',
    'reset_my_pass'           => 'Send me a password reset link',
    'email_not_registered'    => 'Email is not registered in our database',
    'confirm_new_password'    => 'Confirm new password',
    'create_new_password_message' => 'Enter your new password and confirmation',
    'pwd_changed_successfully'=> 'Your password has been changed successfully',
    'invalid_new_pwd_request' => 'Your request is invalid! Please go back to the login screen',
    'back_to_login_screen'    => 'Go Back',
    'email_pwd_reset_unrelated'=> 'The email is not related with the password reset\'s request',


    // Activate Account
    'activate_acc_title'      => 'Hello',
    'activate_acc_link'       => 'Activate account link',
    'activate_acc_message'    => 'We have received your request to create a new account using this email. To activate your account you must click on the following link:',
    'activate_acc_sent'       => 'We have sent you an email to complete the activation process of your account.',
    'activate_acc_pending'    => 'Your account has not been activated. Please check your Inbox. Sometimes new emails are detected as Spam. It might be in your Spam folder. <br/><br/> If you cannot find the activation email, you can <a href="_URL_">New activation email was sended</a>.',
    'activate_acc_success'    => 'Your account has been activated.',
    'activate_acc_resend'     => 'Enter your email to receive a new activation link',
    'activate_acc_resend_btn' => 'Send me a new activation link',


    // Reset email
    'reset_link'              => 'Reset password link',
    'reset_title'             => 'Hello',
    'reset_message'           => 'We have received a password reset request using this email. To reset your password you must click on the following link:',
    'password_reset_sent'     => 'The instructions to reset your password has been sent to your email.',

];
