<?php

return [

	// Header
    'agree' 		=> 'Allow coockies',
    'text' 			=> '<b>Do you like cookies?</b> &#x1F36A; We use cookies to ensure you get the best experience on our website. <a href="/site/politica-de-cookies" target="_blank">Learn more</a>',

];
