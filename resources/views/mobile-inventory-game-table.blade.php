<div>

  
    <!-- Button trigger modal -->

    <div class="rating-review-content">
        <button type="button" class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed" data-toggle="modal" data-target="#exampleModal">
            <span class="fa-solid fa-filter"></span>
        </button> 
        <div class="top-products-area clearfix py-3">
            <div class="container">
                 


                <div class="shop-tab-menu">
                    <div class="row">
                        <!--List & Grid View Menu Start-->

                        <div class="col-lg-5 col-md-5 col-xl-6 col-12">
                            <div class="tab layout-options">
                                <ul class="nav">
                                    <li><a class="active" data-toggle="tab" href="#grid-view"><i
                                                class="lni lni-radio-button"></i></a></li>
                                    <li><a data-toggle="tab" href="#list-view"><i class="lni lni-grid-alt"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <!--List & Grid View Menu End-->
                        <!-- View Mode Start-->
                        <!-- View Mode End-->
                    </div>
                    <div class="tab-content">
                        <!--Grid View Start-->
                        <div class="section-heading d-flex align-items-center justify-content-between">

                        </div>
                        <div id="grid-view" class="tab-pane fade show active">
                            <br>




                            <div class="table article-table table-striped">
                                <div class="table-header d-none d-lg-flex">
                                    <div class="row no-gutters flex-nowrap">
                                        <div class="d-none col">#</div>
                                        <div class="col-sellerProductInfo col">
                                            <div class="row no-gutters h-100">
                                                <div class="col-seller col-12 col-lg-auto">Vendedor
                                                </div>
                                                <div class="col-product col-12 col-lg">Información
                                                    de producto + Oferta</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-body">
                                    @foreach ($seller as $s)
                                    
                                        <div id="articleRow1310616173" class="row no-gutters article-row">
                                            <div class="d-none col"></div>
                                            <div class="col-sellerProductInfo col">
                                                <div class="row no-gutters">
                                                    <div class="col-seller col-12 col-lg-auto"><span
                                                            class="seller-info d-flex align-items-center">
                                                            <span class="seller-name d-flex"><span
                                                                    class="icon d-flex has-content-centered mr-1"><span
                                                                        class="flag-icon flag-icon-esp"></span></span><span
                                                                    class="d-flex has-content-centered mr-1 font-extrabold"><a
                                                                        href="{{ route('user-info', $s->user_name) }}">{{ $s->user_name }}</a></span>
                                                                <span
                                                                    class="inline-block px-2 py-1 mr-3 text-xs font-bold text-gray-600 bg-gray-100 rounded-full"
                                                                    data-toggle="popover"
                                                                    data-content="{{ App\AppOrgOrder::where('seller_user_id', $s->user->id)->whereIn('status', ['DD'])->count() }} Ventas | {{ $s->quantity }} Articulos disponibles"
                                                                    data-placement="top"
                                                                    data-trigger="hover">{{ App\AppOrgOrder::where('seller_user_id', $s->user->id)->whereIn('status', ['DD'])->count() }}</span><span
                                                                    class="d-flex text-nowrap ml-lg-auto"></span></span></span>

                                                        @if ($s->user_id != Auth::id())
                                                            <select
                                                                class="d-inline br-btn-product-qty focus:outline-none focus:bg-white focus:border-gray-500">
                                                                @for ($i = 1; $i <= $s->quantity; $i++)
                                                                    {{ $i }}
                                                                    <option value="{{ $i }}">
                                                                        {{ $i }}</option>
                                                                @endfor
                                                            </select>
                                                        @endif &nbsp;&nbsp;
                                                        <button
                                                            style="color:rgb(3, 3, 3);  background:lighten(#292d48,65);"
                                                            class="br-btn-product-add-game"
                                                            data-inventory-id="{{ $s->id + 1000 }}"
                                                            data-inventory-qty="{{ $s->quantity }}"> <a
                                                                class="btn-success btn-sm add2cart-notify" href="#"> <i
                                                                    class="mr-1 lni lni-cart"></i></a> </button>
                                                    </div>
                                                    <div class="col-product col-12 col-lg">
                                                        <div class="row no-gutters">
                                                            <div class="product-attributes col">

                                                                <img width="15px" src="/{{ $s->box }}"
                                                                    data-toggle="popover"
                                                                    data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($s->box_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px" src="/{{ $s->cover }}"
                                                                    data-toggle="popover"
                                                                    data-content="Caratula - {{ __(App\AppOrgUserInventory::getCondicionName($s->cover_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px" src="/{{ $s->manual }}"
                                                                    data-toggle="popover"
                                                                    data-content="Manual - {{ __(App\AppOrgUserInventory::getCondicionName($s->manual_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px" src="/{{ $s->game }}"
                                                                    data-toggle="popover"
                                                                    data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($s->game_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px" src="/{{ $s->extra }}"
                                                                    data-toggle="popover"
                                                                    data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($s->extra_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                @if ($s->comments == '')
                                                                    <i class="lni lni-comments" data-toggle="popover"
                                                                        data-content="@lang('messages.not_working_profile')"
                                                                        data-placement="top" data-trigger="hover"></i>
                                                                @else
                                                                    <i class="lni lni-comments" data-toggle="popover"
                                                                        data-content="{{ $s->comments }}"
                                                                        data-placement="top" data-trigger="hover"></i>
                                                                @endif
                                                                &nbsp;
                                                                @if ($s->images->first())
                                                                    <a href="{{ url('/uploads/inventory-images/' . $s->images->first()->image_path) }}"
                                                                        class="fancybox"
                                                                        data-fancybox="{{ $s->id }}">
                                                                        <i class="lni lni-camera"></i>
                                                                    </a>
                                                                    @foreach ($s->images as $p)
                                                                        @if (!$loop->first)
                                                                            <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                                data-fancybox="{{ $s->id }}">
                                                                                <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                                    width="0px" height="0px"
                                                                                    style="position:absolute;" />
                                                                            </a>
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    <i class="fa fa-times" data-toggle="popover"
                                                                        data-content="@lang('messages.not_working_profile')"
                                                                        data-placement="top" data-trigger="hover"></i>
                                                                @endif
                                                            </div>
                                                            <div
                                                                class="mobile-offer-container d-flex d-md-none justify-content-end col">
                                                                <span
                                                                    class="small item-count-mobile mr-3">{{ $s->quantity }}
                                                                    dispo.</span>
                                                                <div class="d-flex flex-column">
                                                                    <div
                                                                        class="d-flex align-items-center justify-content-end">
                                                                        <span
                                                                            class="font-weight-bold color-primary small text-right text-nowrap">
                                                                            {{ number_format($s->price, 2) }}
                                                                            €</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <!--Grid View End-->
                        <!--List View Start-->
                        <div id="list-view" class="tab-pane fade">
                            <div class="row g-3">
                                @foreach ($seller as $s)
                                    <!-- Single Weekly Product Card-->
                                    <div class="col-12 col-md-6">
                                        <div class="card weekly-product-card">
                                            <div class="card-body d-flex align-items-center">
                                                <div class="product-thumbnail-side"></span><a class="wishlist-btn"
                                                        href="#"></a>
                                                    @if ($s->images->first())
                                                        <a href="{{ url('/uploads/inventory-images/' . $s->images->first()->image_path) }}"
                                                            class="fancybox product-thumbnail d-block"
                                                            data-fancybox="RGM{{ $s->id }}">
                                                            <img src="{{ url('/uploads/inventory-images/' . $s->images->first()->image_path) }}"
                                                                alt="">
                                                        </a>
                                                        @foreach ($s->images as $p)
                                                            @if (!$loop->first)
                                                                <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                    data-fancybox="RGM{{ $s->id }}">
                                                                    <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                        width="0px" height="0px"
                                                                        style="position:absolute;" />
                                                                </a>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <a class="product-thumbnail d-block"><img
                                                                src="{{ asset('img/art-not-found.jpg') }}"
                                                                alt=""></a>
                                                    @endif

                                                </div>
                                                <div class="product-description">
                                                    <div class="product-title d-block"><span
                                                            class="flag-icon flag-icon-esp"></span> <a
                                                            href="{{ route('user-info', $s->user_name) }}">{{ $s->user_name }}</a>
                                                        <span
                                                            class="inline-block px-2 py-1 text-xs font-bold text-gray-600 bg-gray-100 rounded-full"
                                                            data-toggle="popover"
                                                            data-content="{{ App\AppOrgOrder::where('seller_user_id', $s->user->id)->whereIn('status', ['DD'])->count() }} Ventas | {{ $s->quantity }} Articulos disponibles"
                                                            data-placement="top"
                                                            data-trigger="hover">{{ App\AppOrgOrder::where('seller_user_id', $s->user->id)->whereIn('status', ['DD'])->count() }}</span><span
                                                            class="d-flex text-nowrap ml-lg-auto"></span>
                                                    </div>
                                                    <p class="sale-price">{{ number_format($s->price, 2) }}
                                                        €<span></span>
                                                    </p>
                                                    <div class="product-rating">
                                                        <img width="15px" src="/{{ $s->box }}"
                                                            data-toggle="popover"
                                                            data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($s->box_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                        &nbsp;
                                                        <img width="15px" src="/{{ $s->cover }}"
                                                            data-toggle="popover"
                                                            data-content="Caratula - {{ __(App\AppOrgUserInventory::getCondicionName($s->cover_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                        &nbsp;
                                                        <img width="15px" src="/{{ $s->manual }}"
                                                            data-toggle="popover"
                                                            data-content="Manual - {{ __(App\AppOrgUserInventory::getCondicionName($s->manual_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                        &nbsp;
                                                        <img width="15px" src="/{{ $s->game }}"
                                                            data-toggle="popover"
                                                            data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($s->game_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                        &nbsp;
                                                        <img width="15px" src="/{{ $s->extra }}"
                                                            data-toggle="popover"
                                                            data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($s->extra_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                        &nbsp;

                                                    </div>

                                                    <div class="product-rating">

                                                        @if ($s->user_id != Auth::id())
                                                            <select
                                                                class="d-inline br-btn-product-qty focus:outline-none focus:bg-white focus:border-gray-500">
                                                                @for ($i = 1; $i <= $s->quantity; $i++)
                                                                    {{ $i }}
                                                                    <option value="{{ $i }}">
                                                                        {{ $i }}</option>
                                                                @endfor
                                                            </select>
                                                        @endif
                                                        
                                                        -
                                                        Disponible

                                                    </div>

                                                    <a class="btn btn-success btn-sm add2cart-notify" href="#"><i
                                                            class="mr-1 lni lni-cart"></i>Agregar</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>


                        <!--List View End-->
                    </div>
                </div>



            </div>
        </div>

    </div>

    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <center>Filtar por..</center>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="flex flex-wrap mb-6 -mx-3">
                            <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                                <label
                                    class="block mb-2 text-xs font-extrabold tracking-wide text-gray-700 uppercase retro"
                                    for="grid-first-name">
                                    @lang('messages.seller_inventory')
                                </label>
                                <input class="form-control" wire:model="search" type="text"
                                    placeholder="@lang('messages.seller_inventory_search')">
                            </div>
                            <div class="w-full px-3 md:w-1/2">
                                <label
                                    class="block mb-2 text-xs font-extrabold tracking-wide text-gray-700 uppercase retro"
                                    for="grid-state">
                                    @lang('messages.country')
                                </label>
                                <div class="relative">


                                    <select wire:model="search_country" class="form-control">
                                        <option value="" selected> @lang('messages.alls')
                                        </option>
                                        @foreach ($country as $c)
                                            <option value="{{ $c->name }}">
                                                {{ __($c->name) }}</option>
                                        @endforeach

                                    </select>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20">
                                            <path
                                                d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                        </svg>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="row">


                            <div class="col-md-4">
                                <label
                                    class="block mb-2 text-xs font-extrabold tracking-wide text-gray-700 uppercase retro"
                                    for="grid-state">
                                    @lang('messages.box_condition')
                                </label>
                                <div class="relative">

                                    <select wire:model="search_box" class="form-control">
                                        <option value="" selected>@lang('messages.alls')
                                        </option>
                                        @foreach ($condition as $p)
                                            <option value="{{ $p->value_id }}">
                                                {{ __($p->value) }}
                                            </option>
                                        @endforeach

                                    </select>

                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20">
                                            <path
                                                d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                        </svg>
                                    </div>


                                </div>
                            </div>

                            <div class="col-md-4">
                                <label
                                    class="block mb-2 text-xs font-extrabold tracking-wide text-gray-700 uppercase retro"
                                    for="grid-state">
                                    @lang('messages.cover_condition')
                                </label>
                                <div class="relative">


                                    <select wire:model="search_cover" class="form-control">
                                        <option value="" selected>@lang('messages.alls')
                                        </option>

                                        @foreach ($condition as $p)
                                            <option value="{{ $p->value_id }}">
                                                {{ __($p->value) }}
                                            </option>
                                        @endforeach

                                    </select>

                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20">
                                            <path
                                                d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                        </svg>
                                    </div>


                                </div>
                            </div>

                            <div class="col-md-4">
                                <label
                                    class="block mb-2 text-xs font-extrabold tracking-wide text-gray-700 uppercase retro"
                                    for="grid-state">
                                    @lang('messages.manual_condition')
                                </label>
                                <div class="relative">


                                    <select wire:model="search_manual" class="form-control">
                                        <option value="" selected>@lang('messages.alls')
                                        </option>

                                        @foreach ($condition as $p)
                                            <option value="{{ $p->value_id }}">
                                                {{ __($p->value) }}
                                            </option>
                                        @endforeach

                                    </select>

                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20">
                                            <path
                                                d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                        </svg>
                                    </div>


                                </div>
                            </div>

                            <div class="col-md-6">
                                <label
                                    class="block mb-2 text-xs font-extrabold tracking-wide text-gray-700 uppercase retro"
                                    for="grid-state">
                                    @lang('messages.game_condition')
                                </label>
                                <div class="relative">


                                    <select wire:model="search_game" class="form-control">
                                        <option value="" selected>@lang('messages.alls')
                                        </option>
                                        @foreach ($condition as $p)
                                            <option value="{{ $p->value_id }}">
                                                {{ __($p->value) }}
                                            </option>
                                        @endforeach

                                    </select>

                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20">
                                            <path
                                                d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                        </svg>
                                    </div>

                                </div>


                            </div>

                            <div class="col-md-6">
                                <label class="block mb-2 text-xs font-extrabold tracking-wide text-gray-800 uppercase"
                                    for="grid-state">
                                    @lang('messages.extra_condition')
                                </label>
                                <div class="relative">


                                    <select wire:model="search_extra" class="form-control">
                                        <option value="" selected>@lang('messages.alls')
                                        </option>
                                        @foreach ($condition as $p)
                                            <option value="{{ $p->value_id }}">
                                                {{ __($p->value) }}
                                            </option>
                                        @endforeach

                                    </select>

                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20">
                                            <path
                                                d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                        </svg>
                                    </div>

                                </div>
                            </div>




                        </div>

                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

</div>
