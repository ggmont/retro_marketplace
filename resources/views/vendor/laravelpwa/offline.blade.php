@extends('brcode.front.layout.app_offline')

@section('content')

<div id="appCapsule">

    <div class="error-page">
        <img src="{{ asset('img/RGM.png') }}" alt="alt" class="imaged square w200">
        <h1 class="title">Oops!</h1>
        <div class="text mb-5">
            Ha ocurrido un error con su conexión , verifique si esta conectado a alguna red.
        </div>

        <div class="fixed-footer">
            <div class="row">
                <div class="col-12">
                    <a href="/" class="btn btn-danger btn-lg btn-block">Actualizar</a>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection