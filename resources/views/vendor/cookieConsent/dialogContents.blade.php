<div class="text-center cookiealert" role="alert">
    <h5 class="title">{!! Lang::get('cookie.text') !!}</h5>
    <div class="d-flex justify-content-center">
        <button type="button" class="btn btn-primary acceptcookies" aria-label="Accept">
            {{ Lang::get('cookie.agree') }}
        </button>
        &nbsp;
        <button type="button" class="btn btn-danger denycookies" aria-label="Decline">
            {{ Lang::get('cookie.decline') }}
        </button>
    </div>
</div>
