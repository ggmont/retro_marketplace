<!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7cOpen+Sans:400,700" rel="stylesheet" type="text/css">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/dist/css/bootstrap.min.css') }}">

        <!-- FontAwesome -->
        <script defer src="{{ asset('assets/vendor/fontawesome-free/js/all.js') }}"></script>
        <script defer src="{{ asset('assets/vendor/fontawesome-free/js/v4-shims.js') }}"></script>

        <!-- IonIcons -->
        <link rel="stylesheet" href="{{ asset('assets/vendor/ionicons/css/ionicons.min.css') }}">

        <!-- Seiyria Bootstrap Slider -->
        <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-slider/dist/css/bootstrap-slider.min.css') }}">

        <!-- GoodGames -->
        <link rel="stylesheet" href="{{ asset('assets/css/goodgames.css') }}">

        <!-- Custom Styles -->
        <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
        
        <link rel="stylesheet" href="{{ asset('assets/select2/select2.min.css') }}">
        @yield('content-css-include')
        <!-- END: Styles -->

        <!-- jQuery -->
        <script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
    </head>
    <body>
         

    <div class="nk-main">
        

        
        <div class="nk-fullscreen-block">
            <div class="nk-fullscreen-block-top">
                <div class="text-center">
                    <div class="nk-gap-4"></div>
                    <a href="/"><img src="{{ asset('assets/images/art-not-found.jpg') }}" width="150px" style="border-radius: 100px; box-shadow: 0 0 15px #ddd; background: #fff;" alt="GoodGames"></a>
                    <div class="nk-gap-2"></div>
                </div>
            </div>
            <div class="nk-fullscreen-block-middle">
                <div class="container text-center">
                    <div class="row">
                        <div class="col-md-6 offset-md-3 col-lg-4 offset-lg-4">
                            <h1 class="text-main-1" style="font-size: 150px;">503</h1>
        
                            <div class="nk-gap"></div>
                            <h2 class="h4">You chose the wrong path!</h2>
        
                            <div>Or such a page just doesn't exist... <br> Would you like to go back to the homepage?</div>
                            <div class="nk-gap-3"></div>
        
                            <a href="/" class="nk-btn nk-btn-rounded nk-btn-color-white">Return to Homepage</a>
                        </div>
                    </div>
                    <div class="nk-gap-3"></div>
                </div>
            </div>
            <div class="nk-fullscreen-block-bottom">
                <div class="nk-gap-2"></div>
                <ul class="nk-social-links-2 nk-social-links-center">
                    <li><a class="nk-social-rss" href="#"><span class="fa fa-rss"></span></a></li>
                    <li><a class="nk-social-twitch" href="#"><span class="fab fa-twitch"></span></a></li>
                    <li><a class="nk-social-steam" href="#"><span class="fab fa-steam"></span></a></li>
                    <li><a class="nk-social-facebook" href="#"><span class="fab fa-facebook"></span></a></li>
                    <li><a class="nk-social-google-plus" href="#"><span class="fab fa-google-plus"></span></a></li>
                    <li><a class="nk-social-twitter" href="https://twitter.com/nkdevv" target="_blank"><span class="fab fa-twitter"></span></a></li>
                    <li><a class="nk-social-pinterest" href="#"><span class="fab fa-pinterest-p"></span></a></li>
        
                </ul>
                <div class="nk-gap-2"></div>
            </div>
        </div>
        
        
                
            </div>
        
        
            <div class="nk-page-background-fixed" style="background-image: url({{ asset('assets/images/bg-top-6.png') }});"></div>
        
        
    </body>
</html>
