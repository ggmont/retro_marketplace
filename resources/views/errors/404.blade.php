@extends('brcode.front.layout.app_error')
@section('content-css-include')
@endsection

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())

        <body class="bg-white">
            <!-- App Header -->
            <div class="appHeader no-border transparent position-absolute">
                <div class="left">
                    <a href="#" class="headerButton goBack">
                        <ion-icon name="chevron-back-outline"></ion-icon>
                    </a>
                </div>
                <div class="pageTitle">ERROR!</div>
                <div class="right">
                    <a href="#" class="headerButton">
                        404
                    </a>
                </div>
            </div>
            <!-- * App Header -->

            <!-- App Capsule -->
            <div id="appCapsule">

                <div class="error-page">
                    <div class="icon-box text-danger">
                        <ion-icon name="alert-circle"></ion-icon>
                    </div>
                    <h1 class="title">404</h1>
                    <div class="text mb-5">
                        PAGINA NO ENCONTRADA <br>
                        ¿Necesitas ayuda? Consulta nuestro <a href="/site/faqs">
                            FAQ
                        </a> <br>
                        <div class="appFooter">
                            <div class="mt-2">
                                <a href="https://www.facebook.com/Retrogamingmarket-102746414551509"
                                    class="btn btn-icon btn-sm btn-facebook">
                                    <ion-icon name="logo-facebook"></ion-icon>
                                </a>
                                <a href="https://twitter.com/Retrogamingmkt" class="btn btn-icon btn-sm btn-twitter">
                                    <ion-icon name="logo-twitter"></ion-icon>
                                </a>
                                <a href="https://www.instagram.com/retrogamingmarket/"
                                    class="btn btn-icon btn-sm btn-instagram">
                                    <ion-icon name="logo-instagram"></ion-icon>
                                </a>
                            </div>

                        </div>
                    </div>

                    <div class="fixed-footer">
                        <div class="row">
                            <div class="col-12">
                                <a href="/" class="btn btn-secondary btn-lg btn-block">Volver atras</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- * App Capsule -->


        </body>
        <!-- Internet Connection Status-->
        <div class="internet-connection-status" id="internetStatus"></div>
    @else

        <body>
            <section class="notFound">
                <div class="img">
                    <img src="https://assets.codepen.io/5647096/backToTheHomepage.png" alt="Back to the Homepage" />
                    <img src="{{ asset('img/sonic.gif') }}" alt="RGM" />
                </div>
                <div class="text">
                    <h1>404</h1>
                    <h2>PAGINA NO ENCONTRADA</h2>
                    <h3>VOLVER ATRAS?</h3>
                    <a href="/" class="yes">SI</a>
                    <a href="https://www.youtube.com/watch?v=G3AfIvJBcGo">NO</a>
                </div>
            </section>
        </body>
    @endif
@endsection
