@extends('brcode.front.layout.app_home')
@section('content-css-include')
@endsection

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div id="notification-welcome" class="notification-box">
            <div class="notification-dialog android-style">
                <div class="notification-header">
                    <div class="in">
                        <img src="{{ asset('img/RGM.png') }}" alt="image" class="imaged w24 rounded">
                        <strong>Notificación automática</strong>
                        <span>Justo Ahora</span>
                    </div>
                    <a href="#" class="close-button">
                        <ion-icon name="close"></ion-icon>
                    </a>
                </div>
                <div class="notification-content">
                    <div class="in">
                        <h3 class="subtitle">ALERTA
                        </h3>
                        <div class="text">
                            Su sesión caduco
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @include('partials.flash')


        <br>
        <div class="top-products-area clearfix py-5">
            @livewire('home-product')
        </div>
        <!-- Internet Connection Status-->
        <div class="internet-connection-status" id="internetStatus"></div>
    @else
        <div class="wrapper home-5">
            <span class="retro">
                @include('partials.flash')

            </span>



            <!--Header Area End-->
            <!--Slider Area Start-->
            <section class="slider-area ptb-30 white-bg">
                <div class="container">
                    <div class="nk-gap-2"></div>
                    <div class="row">
                        @if ($viewData['locale'] == 'es')
                            <div class="col-lg-9 col-md-9">
                                <div class="slider-wrapper theme-default">
                                    <div class="nk-image-slider" data-autoplay="8000">
                                        @isset($viewData['main_carousel_items'])
                                            @foreach ($viewData['main_carousel_items'] as $cItem)
                                                <div class="nk-image-slider-item">
                                                    <img loading="lazy"
                                                        src="{{ asset('uploads/carousels-images/' . $cItem['image_path']) }}"
                                                        alt="" class="nk-image-slider-img"
                                                        data-thumb="{{ asset('uploads/carousels-images/' . $cItem['image_path']) }}">

                                                    <div class="nk-image-slider-content">
                                                        @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                                            <b>
                                                                <font color="#e5f6fb">
                                                                    <h5 class="text-base font-extrabold retro">
                                                                        {{ $cItem['content_title'] }} </h5>
                                                                </font>
                                                            </b>
                                                            <p class="text-white">{{ $cItem['content_text'] }} </p>
                                                            @if ($cItem['link'] == '')
                                                            @else
                                                                <a href="{{ $cItem['link'] }}" target="_blank"
                                                                    class="nk-btn nk-btn-rounded nk-btn-color-white nk-btn-hover-color-main-1 retro">Saber
                                                                    mas</a>
                                                            @endif
                                                        @else
                                                            <b>
                                                                <font color="#e5f6fb">
                                                                    <h3 class="text-base font-extrabold retro">
                                                                        {{ $cItem['content_title'] }}
                                                                    </h3>
                                                                </font>
                                                            </b>
                                                            <p class="text-white">{{ $cItem['content_text'] }} </p>
                                                            @if ($cItem['link'] == '')
                                                            @else
                                                                <a href="{{ $cItem['link'] }}" target="_blank"
                                                                    class="nk-btn nk-btn-rounded nk-btn-color-white nk-btn-hover-color-main-1 retro">Saber
                                                                    mas</a>
                                                            @endif
                                                        @endif
                                                    </div>

                                                </div>
                                            @endforeach
                                        @endisset
                                    </div>
                                </div>
                            </div>
                        @elseif($viewData['locale'] == 'en')
                            <div class="col-lg-9 col-md-9">
                                <div class="slider-wrapper theme-default">
                                    <div class="nk-image-slider" data-autoplay="8000">
                                        @isset($viewData['main_carousel_items_en'])
                                            @foreach ($viewData['main_carousel_items_en'] as $cItem)
                                                <div class="nk-image-slider-item">
                                                    <img loading="lazy"
                                                        src="{{ asset('uploads/carousels-images/' . $cItem['image_path']) }}"
                                                        alt="" class="nk-image-slider-img"
                                                        data-thumb="{{ asset('uploads/carousels-images/' . $cItem['image_path']) }}">

                                                    <div class="nk-image-slider-content">
                                                        @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                                            <b>
                                                                <font color="#e5f6fb">
                                                                    <h5 class="text-base font-extrabold retro">
                                                                        {{ $cItem['content_title'] }} </h5>
                                                                </font>
                                                            </b>
                                                            <p class="text-white">{{ $cItem['content_text'] }} </p>
                                                            @if ($cItem['link'] == '')
                                                            @else
                                                                <a href="{{ $cItem['link'] }}" target="_blank"
                                                                    class="nk-btn nk-btn-rounded nk-btn-color-white nk-btn-hover-color-main-1 retro">Saber
                                                                    mas</a>
                                                            @endif
                                                        @else
                                                            <b>
                                                                <font color="#e5f6fb">
                                                                    <h3 class="h5 retro">
                                                                        {{ $cItem['content_title'] }}
                                                                    </h3>
                                                                </font>
                                                            </b>
                                                            <p class="text-white">{{ $cItem['content_text'] }} </p>
                                                            @if ($cItem['link'] == '')
                                                            @else
                                                                <a href="{{ $cItem['link'] }}" target="_blank"
                                                                    class="nk-btn nk-btn-rounded nk-btn-color-white nk-btn-hover-color-main-1 retro">Saber
                                                                    mas</a>
                                                            @endif
                                                        @endif
                                                    </div>

                                                </div>
                                            @endforeach
                                        @endisset
                                    </div>
                                </div>
                            </div>
                        @elseif($viewData['locale'] == 'fr')
                            <div class="col-lg-9 col-md-9">
                                <div class="slider-wrapper theme-default">
                                    <div class="nk-image-slider" data-autoplay="8000">
                                        @isset($viewData['main_carousel_items_fr'])
                                            @foreach ($viewData['main_carousel_items_fr'] as $cItem)
                                                <div class="nk-image-slider-item">
                                                    <img loading="lazy"
                                                        src="{{ asset('uploads/carousels-images/' . $cItem['image_path']) }}"
                                                        alt="" class="nk-image-slider-img"
                                                        data-thumb="{{ asset('uploads/carousels-images/' . $cItem['image_path']) }}">

                                                    <div class="nk-image-slider-content">
                                                        @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                                            <b>
                                                                <font color="#e5f6fb">
                                                                    <h5 class="text-base font-extrabold retro">
                                                                        {{ $cItem['content_title'] }} </h5>
                                                                </font>
                                                            </b>
                                                            <p class="text-white">{{ $cItem['content_text'] }} </p>
                                                            @if ($cItem['link'] == '')
                                                            @else
                                                                <a href="{{ $cItem['link'] }}" target="_blank"
                                                                    class="nk-btn nk-btn-rounded nk-btn-color-white nk-btn-hover-color-main-1 retro">Saber
                                                                    mas</a>
                                                            @endif
                                                        @else
                                                            <b>
                                                                <font color="#e5f6fb">
                                                                    <h3 class="h5 retro">
                                                                        {{ $cItem['content_title'] }}
                                                                    </h3>
                                                                </font>
                                                            </b>
                                                            <p class="text-white">{{ $cItem['content_text'] }} </p>
                                                            @if ($cItem['link'] == '')
                                                            @else
                                                                <a href="{{ $cItem['link'] }}" target="_blank"
                                                                    class="nk-btn nk-btn-rounded nk-btn-color-white nk-btn-hover-color-main-1 retro">Saber
                                                                    mas</a>
                                                            @endif
                                                        @endif
                                                    </div>

                                                </div>
                                            @endforeach
                                        @endisset
                                    </div>
                                </div>
                            </div>
                        @endif


                        @if ($viewData['locale'] == 'es')
                            <div class="col-lg-3 col-md-3">
                                <div class="mb-20 single-offer">

                                    <div class="offer-img img-full">
                                        @isset($viewData['main_carousel_items_secundary_one'])
                                            @foreach ($viewData['main_carousel_items_secundary_one'] as $cItem)
                                                <a href="{{ $cItem['link'] }}" target="_blank"><img loading="lazy"
                                                        src="{{ asset('uploads/carousels-images/' . $cItem['image_path']) }}"
                                                        alt=""></a>
                                            @endforeach
                                        @endisset
                                    </div>

                                </div>
                                <div class="offer">

                                    <div class="offer-img2 img-full">
                                        @isset($viewData['main_carousel_items_secundary_two'])
                                            @foreach ($viewData['main_carousel_items_secundary_two'] as $cItem)
                                                <a href="{{ $cItem['link'] }}" target="_blank"><img loading="lazy"
                                                        src="{{ asset('uploads/carousels-images/' . $cItem['image_path']) }}"
                                                        alt=""></a>
                                            @endforeach
                                        @endisset
                                    </div>

                                </div>
                            </div>
                        @elseif($viewData['locale'] == 'en')
                            <div class="col-lg-3 col-md-3">
                                <div class="mb-20 single-offer">

                                    <div class="offer-img img-full">
                                        @isset($viewData['main_carousel_items_secundary_one_en'])
                                            @foreach ($viewData['main_carousel_items_secundary_one_en'] as $cItem)
                                                <a href="{{ $cItem['link'] }}" target="_blank"><img loading="lazy"
                                                        src="{{ asset('uploads/carousels-images/' . $cItem['image_path']) }}"
                                                        alt=""></a>
                                            @endforeach
                                        @endisset
                                    </div>

                                </div>
                                <div class="offer">

                                    <div class="offer-img2 img-full">
                                        @isset($viewData['main_carousel_items_secundary_two_en'])
                                            @foreach ($viewData['main_carousel_items_secundary_two_en'] as $cItem)
                                                <a href="{{ $cItem['link'] }}" target="_blank"><img loading="lazy"
                                                        src="{{ asset('uploads/carousels-images/' . $cItem['image_path']) }}"
                                                        alt=""></a>
                                            @endforeach
                                        @endisset
                                    </div>

                                </div>
                            </div>
                        @elseif($viewData['locale'] == 'fr')
                            <div class="col-lg-3 col-md-3">
                                <div class="mb-20 single-offer">

                                    <div class="offer-img img-full">
                                        @isset($viewData['main_carousel_items_secundary_one_fr'])
                                            @foreach ($viewData['main_carousel_items_secundary_one_fr'] as $cItem)
                                                <a href="{{ $cItem['link'] }}" target="_blank"><img loading="lazy"
                                                        src="{{ asset('uploads/carousels-images/' . $cItem['image_path']) }}"
                                                        alt=""></a>
                                            @endforeach
                                        @endisset
                                    </div>

                                </div>
                                <div class="offer">

                                    <div class="offer-img2 img-full">
                                        @isset($viewData['main_carousel_items_secundary_two_fr'])
                                            @foreach ($viewData['main_carousel_items_secundary_two_fr'] as $cItem)
                                                <a href="{{ $cItem['link'] }}" target="_blank"><img loading="lazy"
                                                        src="{{ asset('uploads/carousels-images/' . $cItem['image_path']) }}"
                                                        alt=""></a>
                                            @endforeach
                                        @endisset
                                    </div>

                                </div>
                            </div>
                        @endif
                        <!--Offer Image Start-->

                        <!--Offer Image End-->
                    </div>
                </div>
            </section>
            <!--Slider Area End-->
            <!--Corporate About Start-->
            <section class="corporate-about white-bg pb-30">
                <div class="container bg-white">
                    <div class="row all-about no-gutters">
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="single-about">
                                <div class="block-wrapper">
                                    <div class="about-content">
                                        <div class="about-content">
                                            <h6 class="text-gray-900 text-sm font-semibold">
                                                <b>@lang('messages.easy_and_safe')</b>
                                            </h6>


                                            <p style="font-size: 12px">@lang('messages.easy_and_safe_message')
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="single-about">
                                <div class="block-wrapper2">
                                    <div class="about-content">
                                        <h6 class="text-gray-900 text-sm font-semibold"><b>@lang('messages.fast')</b></h6>


                                        <p style="font-size: 12px">@lang('messages.fast_message_one') <br>
                                            @lang('messages.fast_message_two')
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="single-about">
                                <div class="block-wrapper3">
                                    <div class="about-content">
                                        <h6 class="text-gray-900 text-sm font-semibold"><b>@lang('messages.best_price')</b>
                                        </h6>

                                        <center>
                                            <p style="font-size: 12px">@lang('messages.best_price_message')</p>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="single-about not-border">
                                <div class="block-wrapper4">
                                    <div class="about-content">
                                        <h6 class="text-gray-900 text-sm font-semibold">
                                            <b>@lang('messages.stock_online')</b>
                                        </h6>
                                        <p>@lang('messages.stock_online_message')
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



            <!--Corporate About End-->
            <!--All Product Area Start-->
            <section class="All Product Area pt-50">
                <div class="container bg-white">
                    <div class="row">
                        <!--Left Side Product Start-->
                        <div class="col-lg-9 col-md-9">
                            <div class="orange-juice-product">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <!--Section Title1 Start-->
                                        <div class="section-title1-border">
                                            <div class="section-title1">
                                                <font color="#ffd3b1">
                                                    <h3><b>@lang('messages.last_in_sale')</b></h3>
                                                </font>
                                            </div>
                                        </div>
                                        <!--Section Title1 End-->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <!--Product Tab Menu Start-->
                                        <div class="product-tab-menu-area">
                                            <div class="product-tab">
                                                <ul class="nav">
                                                    <li><a class="active" data-toggle="tab" href="#top10"><span
                                                                class="text-base font-extrabold text-gray-800 hover:text-red-500 retro">@lang('messages.last_ten')
                                                                10</span></a></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <!--Product Tab Menu End-->
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div id="top10" class="tab-pane fade show active">
                                        <div class="row">
                                            <div class="mb-20 all-product owl-carousel">
                                                @isset($inventory_latest_users)
                                                    @foreach ($inventory_latest_users as $i)
                                                        <div class="col-lg-12 item-col">
                                                            <div class="single-product">
                                                                <div class="product-img">
                                                                    <a href="{{ route('product-show', $i->product->id) }}"
                                                                        target="_blank">
                                                                        @if ($i->images->first())
                                                                            <img class="first-img"
                                                                                src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                                                alt="">
                                                                            @if ($i->images->get(1))
                                                                                <img class="hover-img"
                                                                                    src="{{ url('/uploads/inventory-images/' . $i->images->get(1)->image_path) }}"
                                                                                    alt="">
                                                                            @endif
                                                                        @elseif ($i->product->images->first())
                                                                            <img class="first-img"
                                                                                src="{{ url('/images/' . $i->product->images->first()->image_path) }}"
                                                                                alt="">
                                                                            @if ($i->product->images->get(1))
                                                                                <img class="hover-img"
                                                                                    src="{{ url('/images/' . $i->product->images->get(1)->image_path) }}"
                                                                                    alt="">
                                                                            @endif
                                                                        @else
                                                                            <img loading="lazy" class="first-img"
                                                                                src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                                                alt="">
                                                                        @endif
                                                                    </a>
                                                                    <span class="text-sm font-bold sicker">
                                                                        <a href="{{ route('user-info', $i->user->user_name) }}"
                                                                            target="_blank">
                                                                            <font color="white">
                                                                                {{ $i->user->user_name }} -
                                                                                @include('partials.pais_user_home')
                                                                            </font>
                                                                        </a>
                                                                    </span>
                                                                    <ul class="product-action">
                                                                    </ul>
                                                                </div>
                                                                <div class="product-content">
                                                                    <h2 class="text-gray-900 retro small">
                                                                        <a href="{{ route('product-show', $i->product->id) }}"
                                                                            target="_blank">
                                                                            <b>{{ str_limit($i->product->name, 45) }}</b>
                                                                        </a>
                                                                    </h2>
                                                                    <div class="rating">
                                                                        {{ $i->product->platform . ' - ' . $i->product->region }}
                                                                    </div>
                                                                    <div class="product-price">
                                                                        <span
                                                                            class="font-extrabold text-green-800 new-price"><b>{{ number_format($i->price, 2) . ' €' }}</b></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endisset



                                            </div>
                                        </div>
                                    </div>

                                </div>
                                @if ($viewData['locale'] == 'es')
                                    @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                    @else
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="single-offer">
                                                    <div class="offer-img img-full">
                                                        @isset($viewData['main_banner_1'])
                                                            @foreach ($viewData['main_banner_1'] as $cItem)
                                                                <a href="{{ $cItem->link }}"><img loading="lazy"
                                                                        src="{{ asset('uploads/banners-images/' . $cItem['image_path']) }}"
                                                                        alt=""></a>
                                                            @endforeach
                                                        @endisset

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @elseif($viewData['locale'] == 'en')
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="single-offer">
                                                <div class="offer-img img-full">
                                                    @isset($viewData['main_banner_1_en'])
                                                        @foreach ($viewData['main_banner_1_en'] as $cItem)
                                                            <a href="{{ $cItem->link }}"><img loading="lazy"
                                                                    src="{{ asset('uploads/banners-images/' . $cItem['image_path']) }}"
                                                                    alt=""></a>
                                                        @endforeach
                                                    @endisset

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @elseif($viewData['locale'] == 'fr')
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="single-offer">
                                                <div class="offer-img img-full">
                                                    @isset($viewData['main_banner_1_fr'])
                                                        @foreach ($viewData['main_banner_1_fr'] as $cItem)
                                                            <a href="{{ $cItem->link }}"><img loading="lazy"
                                                                    src="{{ asset('uploads/banners-images/' . $cItem['image_path']) }}"
                                                                    alt=""></a>
                                                        @endforeach
                                                    @endisset

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <br> <br>
                            </div>
                            <!--Orange Juice Product Start-->
                            <div class="orange-juice-product">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <!--Section Title1 Start-->
                                        <div class="section-title1-border">
                                            <div class="section-title1">
                                                <font color="#ffd3b1">
                                                    <h3 class="retro">@lang('messages.retro_in_sale')</h3>
                                                </font>
                                            </div>
                                        </div>
                                        <!--Section Title1 End-->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <!--Product Tab Menu Start-->
                                        <div class="product-tab-menu-area">
                                            <div class="product-tab">
                                                <ul class="nav">
                                                    <li><a class="active" data-toggle="tab" href="#nes"><span
                                                                class="text-base font-extrabold text-gray-800 hover:text-red-500 retro">NES</span></a>
                                                    </li>
                                                    <li><a data-toggle="tab" href="#snes"><span
                                                                class="text-base font-extrabold text-gray-800 hover:text-red-500 retro">SNES</span></a>
                                                    </li>
                                                    <li><a data-toggle="tab" href="#megadrive"><span
                                                                class="text-base font-extrabold text-gray-800 hover:text-red-500 retro">MEGADRIVE</span></a>
                                                    </li>
                                                    <li><a data-toggle="tab" href="#dreamcast"><span
                                                                class="text-base font-extrabold text-gray-800 hover:text-red-500 retro">GAME
                                                                GEAR</span></a>
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>

                                        <!--Product Tab Menu End-->
                                    </div>
                                </div>
                                <!--Product Tab Start-->
                                <div class="tab-content">
                                    <div id="snes" class="tab-pane fade">
                                        <div class="row">
                                            <div class="mb-20 all-product owl-carousel">
                                                @isset($inventory_retro_snes)
                                                    @foreach ($inventory_retro_snes as $i)
                                                        <!--Single Product Start-->

                                                        <div class="col-lg-12 item-col">
                                                            <div class="single-product">
                                                                <div class="product-img">
                                                                    <a href="{{ route('product-show', $i->product->id) }}"
                                                                        target="_blank">
                                                                        @if ($i->images->first())
                                                                            <img class="first-img"
                                                                                src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                                                alt="">
                                                                            @if ($i->images->get(1))
                                                                                <img class="hover-img"
                                                                                    src="{{ url('/uploads/inventory-images/' . $i->images->get(1)->image_path) }}"
                                                                                    alt="">
                                                                            @endif
                                                                        @elseif ($i->product->images->first())
                                                                            <img class="first-img"
                                                                                src="{{ url('/images/' . $i->product->images->first()->image_path) }}"
                                                                                alt="">
                                                                            @if ($i->product->images->get(1))
                                                                                <img class="hover-img"
                                                                                    src="{{ url('/images/' . $i->product->images->get(1)->image_path) }}"
                                                                                    alt="">
                                                                            @endif
                                                                        @else
                                                                            <img loading="lazy" class="first-img"
                                                                                src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                                                alt="">
                                                                        @endif
                                                                    </a>

                                                                    <span class="text-xs font-bold sicker">
                                                                        <a href="{{ route('user-info', $i->user->user_name) }}"
                                                                            target="_blank">
                                                                            <font color="white">
                                                                                {{ $i->user->user_name }} -
                                                                                @include('partials.pais_user_home')
                                                                            </font>
                                                                        </a>
                                                                    </span>

                                                                    <ul class="product-action">
                                                                    </ul>
                                                                </div>
                                                                <div class="product-content">
                                                                    <h2 class="text-gray-900 retro small">
                                                                        <a href="{{ route('product-show', $i->product->id) }}"
                                                                            target="_blank">
                                                                            <b>{{ str_limit($i->product->name, 45) }}</b>
                                                                        </a>
                                                                    </h2>
                                                                    <div class="rating">
                                                                        {{ $i->product->platform . ' - ' . $i->product->region }}
                                                                    </div>
                                                                    <div class="product-price">
                                                                        <span
                                                                            class="font-extrabold text-green-800 new-price"><b>{{ number_format($i->price, 2) . ' €' }}</b></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endisset
                                                <!--Single Product End-->

                                            </div>

                                            <div class="col-lg-12">
                                                <div class="single-offer">
                                                    <div class="offer-img img-full">
                                                        <a href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_platform=Super%20Nintendo"
                                                            target="_blank"
                                                            class="border-b nk-btn-rounded nes-btn is-error text-lg retro-sell  btn-block"><b>@lang('messages.see_all')</b></a><br>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div id="megadrive" class="tab-pane fade">
                                        <div class="row">
                                            <div class="mb-20 all-product owl-carousel">
                                                @isset($inventory_retro_sega_megadrive)
                                                    @foreach ($inventory_retro_sega_megadrive as $i)
                                                        <!--Single Product Start-->
                                                        <div class="col-lg-12 item-col">
                                                            <div class="single-product">
                                                                <div class="product-img">
                                                                    <a href="{{ route('product-show', $i->product->id) }}"
                                                                        target="_blank">
                                                                        @if ($i->images->first())
                                                                            <img class="first-img"
                                                                                src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                                                alt="">
                                                                            @if ($i->images->get(1))
                                                                                <img class="hover-img"
                                                                                    src="{{ url('/uploads/inventory-images/' . $i->images->get(1)->image_path) }}"
                                                                                    alt="">
                                                                            @endif
                                                                        @elseif ($i->product->images->first())
                                                                            <img class="first-img"
                                                                                src="{{ url('/images/' . $i->product->images->first()->image_path) }}"
                                                                                alt="">
                                                                            @if ($i->product->images->get(1))
                                                                                <img class="hover-img"
                                                                                    src="{{ url('/images/' . $i->product->images->get(1)->image_path) }}"
                                                                                    alt="">
                                                                            @endif
                                                                        @else
                                                                            <img loading="lazy" class="first-img"
                                                                                src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                                                alt="">
                                                                        @endif
                                                                    </a>
                                                                    <span class="text-xs font-bold sicker">
                                                                        <a href="{{ route('user-info', $i->user->user_name) }}"
                                                                            target="_blank">
                                                                            <font color="white">
                                                                                {{ $i->user->user_name }} -
                                                                                @include('partials.pais_user_home')
                                                                            </font>
                                                                        </a>
                                                                    </span>
                                                                    <ul class="product-action">
                                                                    </ul>
                                                                </div>
                                                                <div class="product-content">
                                                                    <h2 class="text-gray-900 retro small">
                                                                        <a href="{{ route('product-show', $i->product->id) }}"
                                                                            target="_blank">
                                                                            <b>{{ str_limit($i->product->name, 45) }}</b>
                                                                        </a>
                                                                    </h2>
                                                                    <div class="rating">
                                                                        {{ $i->product->platform . ' - ' . $i->product->region }}
                                                                    </div>
                                                                    <div class="product-price">
                                                                        <span
                                                                            class="font-extrabold text-green-800 new-price"><b>{{ number_format($i->price, 2) . ' €' }}</b></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endisset
                                                <!--Single Product End-->
                                            </div>
                                            <a href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_platform=Sega%20Mega%20Drive"
                                                target="_blank"
                                                class="border-b nk-btn-rounded nes-btn is-error text-lg retro-sell  btn-block">@lang('messages.see_all')</a><br>
                                        </div>
                                    </div>
                                    <div id="nes" class="tab-pane fade show active">
                                        <div class="row">
                                            <div class="mb-20 all-product owl-carousel">
                                                @isset($inventory_retro_nes)
                                                    @foreach ($inventory_retro_nes as $i)
                                                        <div class="col-lg-12 item-col">
                                                            <div class="single-product">
                                                                <div class="product-img">
                                                                    <a href="{{ route('product-show', $i->product->id) }}"
                                                                        target="_blank">
                                                                        @if ($i->images->first())
                                                                            <img class="first-img"
                                                                                src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                                                alt="">
                                                                            @if ($i->images->get(1))
                                                                                <img class="hover-img"
                                                                                    src="{{ url('/uploads/inventory-images/' . $i->images->get(1)->image_path) }}"
                                                                                    alt="">
                                                                            @endif
                                                                        @elseif ($i->product->images->first())
                                                                            <img class="first-img"
                                                                                src="{{ url('/images/' . $i->product->images->first()->image_path) }}"
                                                                                alt="">
                                                                            @if ($i->product->images->get(1))
                                                                                <img class="hover-img"
                                                                                    src="{{ url('/images/' . $i->product->images->get(1)->image_path) }}"
                                                                                    alt="">
                                                                            @endif
                                                                        @else
                                                                            <img loading="lazy" class="first-img"
                                                                                src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                                                alt="">
                                                                        @endif
                                                                    </a>
                                                                    <span class="text-xs font-bold sicker">
                                                                        <a href="{{ route('user-info', $i->user->user_name) }}"
                                                                            target="_blank">
                                                                            <font color="white">
                                                                                {{ $i->user->user_name }} -
                                                                                @include('partials.pais_user_home')
                                                                            </font>
                                                                        </a>
                                                                    </span>
                                                                    <ul class="product-action">

                                                                    </ul>
                                                                </div>
                                                                <div class="product-content">
                                                                    <h2 class="text-gray-900 retro small">
                                                                        <a href="{{ route('product-show', $i->product->id) }}"
                                                                            target="_blank">
                                                                            <b>{{ str_limit($i->product->name, 45) }}</b>
                                                                        </a>
                                                                    </h2>
                                                                    <div class="rating">
                                                                        {{ $i->product->platform . ' - ' . $i->product->region }}
                                                                    </div>
                                                                    <div class="product-price">
                                                                        <span
                                                                            class="font-extrabold text-green-800 new-price"><b>{{ number_format($i->price, 2) . ' €' }}</b></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endisset
                                                <!--Single Product End-->
                                            </div>
                                            <a href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_platform=NES"
                                                target="_blank"
                                                class="border-b nk-btn-rounded nes-btn is-error text-lg retro-sell  btn-block">@lang('messages.see_all')</a><br>
                                        </div>
                                    </div>
                                    <div id="dreamcast" class="tab-pane fade">
                                        <div class="row">
                                            <div class="mb-20 all-product owl-carousel">
                                                @isset($inventory_retro_dreamcast)
                                                    @foreach ($inventory_retro_dreamcast as $i)
                                                        <!--Single Product Start-->

                                                        <div class="col-lg-12 item-col">
                                                            <div class="single-product">
                                                                <div class="product-img">
                                                                    <a href="{{ route('product-show', $i->product->id) }}"
                                                                        target="_blank">
                                                                        @if ($i->images->first())
                                                                            <img class="first-img"
                                                                                src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                                                alt="">
                                                                            @if ($i->images->get(1))
                                                                                <img class="hover-img"
                                                                                    src="{{ url('/uploads/inventory-images/' . $i->images->get(1)->image_path) }}"
                                                                                    alt="">
                                                                            @endif
                                                                        @elseif ($i->product->images->first())
                                                                            <img class="first-img"
                                                                                src="{{ url('/images/' . $i->product->images->first()->image_path) }}"
                                                                                alt="">
                                                                            @if ($i->product->images->get(1))
                                                                                <img class="hover-img"
                                                                                    src="{{ url('/images/' . $i->product->images->get(1)->image_path) }}"
                                                                                    alt="">
                                                                            @endif
                                                                        @else
                                                                            <img loading="lazy" class="first-img"
                                                                                src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                                                alt="">
                                                                        @endif
                                                                    </a>
                                                                    <span class="text-xs font-bold sicker">
                                                                        <a href="{{ route('user-info', $i->user->user_name) }}"
                                                                            target="_blank">
                                                                            <font color="white">
                                                                                {{ $i->user->user_name }} -
                                                                                @include('partials.pais_user_home')
                                                                            </font>
                                                                        </a>
                                                                    </span>
                                                                    <ul class="product-action">

                                                                    </ul>
                                                                </div>
                                                                <div class="product-content">
                                                                    <h2 class="text-gray-900 retro small">
                                                                        <a href="{{ route('product-show', $i->product->id) }}"
                                                                            target="_blank">
                                                                            <b>{{ str_limit($i->product->name, 45) }}</b>
                                                                        </a>
                                                                    </h2>
                                                                    <div class="rating">
                                                                        {{ $i->product->platform . ' - ' . $i->product->region }}
                                                                    </div>
                                                                    <div class="product-price">
                                                                        <span
                                                                            class="font-extrabold text-green-800 new-price"><b>{{ number_format($i->price, 2) . ' €' }}</b></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endisset
                                                <!--Single Product End-->
                                            </div>
                                            <a href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_platform=Game%20Gear"
                                                target="_blank"
                                                class="border-b nk-btn-rounded nes-btn is-error text-lg retro-sell  btn-block">@lang('messages.see_all')</a><br>
                                        </div>
                                        <!--Product Tab End-->



                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="single-offer">



                                    </div>
                                </div>

                            </div>

                            <div class="vegetable-fresh-product pt-50">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <!--Section Title1 Start-->
                                        <div class="section-title1-border">
                                            <div class="section-title1">
                                                <font color="#ffd3b1">
                                                    <h3 class="retro">@lang('messages.new_gen_sale')</h3>
                                                </font>
                                            </div>
                                        </div>
                                        <!--Section Title1 End-->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <!--Product Tab Menu Start-->
                                        <div class="product-tab-menu-area">
                                            <div class="product-tab">
                                                <ul class="nav">
                                                    <li><a class="active" data-toggle="tab" href="#ps4"><span
                                                                class="text-base font-extrabold text-gray-800 hover:text-red-500 retro">PS4</span></a>
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                        <!--Product Tab Menu End-->
                                    </div>
                                </div>
                                <!--Product Tab Start-->
                                <div class="tab-content">
                                    <div id="ps4" class="tab-pane fade show active">
                                        <div class="row">
                                            <div class="mb-20 all-product owl-carousel">
                                                @isset($inventory_new_ps4)
                                                    @foreach ($inventory_new_ps4 as $i)
                                                        <!--Single Product Start-->
                                                        <div class="col-lg-12 item-col">
                                                            <div class="single-product">
                                                                <div class="product-img">
                                                                    <a href="{{ route('product-show', $i->product->id) }}"
                                                                        target="_blank">
                                                                        @if ($i->images->first())
                                                                            <img class="first-img"
                                                                                src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                                                alt="">
                                                                            @if ($i->images->get(1))
                                                                                <img class="hover-img"
                                                                                    src="{{ url('/uploads/inventory-images/' . $i->images->get(1)->image_path) }}"
                                                                                    alt="">
                                                                            @endif
                                                                        @elseif ($i->product->images->first())
                                                                            <img class="first-img"
                                                                                src="{{ url('/images/' . $i->product->images->first()->image_path) }}"
                                                                                alt="">
                                                                            @if ($i->product->images->get(1))
                                                                                <img class="hover-img"
                                                                                    src="{{ url('/images/' . $i->product->images->get(1)->image_path) }}"
                                                                                    alt="">
                                                                            @endif
                                                                        @else
                                                                            <img loading="lazy" class="first-img"
                                                                                src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                                                alt="">
                                                                        @endif
                                                                    </a>

                                                                    <span class="text-xs font-bold sicker">
                                                                        <a href="{{ route('user-info', $i->user->user_name) }}"
                                                                            target="_blank">
                                                                            <font color="white">
                                                                                {{ $i->user->user_name }} -
                                                                                @include('partials.pais_user_home')
                                                                            </font>
                                                                        </a>
                                                                    </span>
                                                                    <ul class="product-action">
                                                                    </ul>
                                                                </div>
                                                                <div class="product-content">
                                                                    <h2 class="text-gray-900 retro small">
                                                                        <a href="{{ route('product-show', $i->product->id) }}"
                                                                            target="_blank">
                                                                            <b>{{ str_limit($i->product->name, 45) }}</b>
                                                                        </a>
                                                                    </h2>
                                                                    <div class="rating">
                                                                        {{ $i->product->platform . ' - ' . $i->product->region }}
                                                                    </div>
                                                                    <div class="product-price">
                                                                        <span
                                                                            class="font-extrabold text-green-800 new-price"><b>{{ number_format($i->price, 2) . ' €' }}</b></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endisset


                                                <!--Single Product End-->
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="single-offer">
                                                    <div class="offer-img img-full">
                                                        <a href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_platform=PlayStation"
                                                            target="_blank"
                                                            class="border-b nk-btn-rounded nes-btn is-error text-lg retro-sell btn-block">@lang('messages.see_all')</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="xbox" class="tab-pane fade">
                                        <div class="row">
                                            <div class="mb-20 all-product mb-85 owl-carousel">
                                                @isset($inventory_new_xbox)
                                                    @foreach ($inventory_new_xbox as $i)
                                                        <!--Single Product Start-->
                                                        <div class="col-lg-12 item-col">
                                                            <div class="single-product">
                                                                <div class="product-img">
                                                                    <a href="{{ route('product-show', $i->product->id) }}"
                                                                        target="_blank">
                                                                        @if ($i->images->first())
                                                                            <img class="first-img"
                                                                                src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                                                alt="">
                                                                            @if ($i->images->get(1))
                                                                                <img class="hover-img"
                                                                                    src="{{ url('/uploads/inventory-images/' . $i->images->get(1)->image_path) }}"
                                                                                    alt="">
                                                                            @endif
                                                                        @elseif ($i->product->images->first())
                                                                            <img class="first-img"
                                                                                src="{{ url('/images/' . $i->product->images->first()->image_path) }}"
                                                                                alt="">
                                                                            @if ($i->product->images->get(1))
                                                                                <img class="hover-img"
                                                                                    src="{{ url('/images/' . $i->product->images->get(1)->image_path) }}"
                                                                                    alt="">
                                                                            @endif
                                                                        @else
                                                                            <img loading="lazy" class="first-img"
                                                                                src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                                                alt="">
                                                                        @endif
                                                                    </a>
                                                                    <span class="text-xs font-bold sicker">
                                                                        <a href="{{ route('user-info', $i->user->user_name) }}"
                                                                            target="_blank">
                                                                            <font color="white">
                                                                                {{ $i->user->user_name }} -
                                                                                @include('partials.pais_user_home')
                                                                            </font>
                                                                        </a>
                                                                    </span>
                                                                    <ul class="product-action">
                                                                    </ul>
                                                                </div>
                                                                <div class="product-content">
                                                                    <h2 class="text-gray-900 retro small">
                                                                        <a href="{{ route('product-show', $i->product->id) }}"
                                                                            target="_blank">
                                                                            <b>{{ str_limit($i->product->name, 45) }}</b>
                                                                        </a>
                                                                    </h2>
                                                                    <div class="rating">
                                                                        {{ $i->product->platform . ' - ' . $i->product->region }}
                                                                    </div>
                                                                    <div class="product-price">
                                                                        <span
                                                                            class="font-extrabold text-green-800 new-price"><b>{{ number_format($i->price, 2) . ' €' }}</b></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endisset
                                                <!--Single Product End-->
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="single-offer">
                                                    <div class="offer-img img-full">
                                                        <a href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_platform=Xbox%20One"
                                                            target="_blank"
                                                            class="border-b nk-btn-rounded nes-btn is-error text-lg retro-sell btn-block">@lang('messages.see_all')</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="switch" class="tab-pane fade">
                                        <div class="row">
                                            <div class="mb-20 all-product owl-carousel">
                                                @isset($inventory_new_switch)
                                                    @foreach ($inventory_new_switch as $i)
                                                        <!--Single Product Start-->
                                                        <div class="col-lg-12 item-col">
                                                            <div class="single-product">
                                                                <div class="product-img">
                                                                    <a href="{{ route('product-show', $i->product->id) }}"
                                                                        target="_blank">
                                                                        @if ($i->images->first())
                                                                            <img class="first-img"
                                                                                src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                                                alt="">
                                                                            @if ($i->images->get(1))
                                                                                <img class="hover-img"
                                                                                    src="{{ url('/uploads/inventory-images/' . $i->images->get(1)->image_path) }}"
                                                                                    alt="">
                                                                            @endif
                                                                        @elseif ($i->product->images->first())
                                                                            <img class="first-img"
                                                                                src="{{ url('/images/' . $i->product->images->first()->image_path) }}"
                                                                                alt="">
                                                                            @if ($i->product->images->get(1))
                                                                                <img class="hover-img"
                                                                                    src="{{ url('/images/' . $i->product->images->get(1)->image_path) }}"
                                                                                    alt="">
                                                                            @endif
                                                                        @else
                                                                            <img loading="lazy" class="first-img"
                                                                                src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                                                alt="">
                                                                        @endif
                                                                    </a>
                                                                    <span class="text-xs font-bold sicker">
                                                                        <a href="{{ route('user-info', $i->user->user_name) }}"
                                                                            target="_blank">
                                                                            <font color="white">
                                                                                {{ $i->user->user_name }} -
                                                                                @include('partials.pais_user_home')
                                                                            </font>
                                                                        </a>
                                                                    </span>
                                                                    <ul class="product-action">
                                                                    </ul>
                                                                </div>
                                                                <div class="product-content">
                                                                    <h2 class="text-gray-900 retro small">
                                                                        <a href="{{ route('product-show', $i->product->id) }}"
                                                                            target="_blank">
                                                                            <b>{{ str_limit($i->product->name, 45) }}</b>
                                                                        </a>
                                                                    </h2>
                                                                    <div class="rating">
                                                                        {{ $i->product->platform . ' - ' . $i->product->region }}
                                                                    </div>
                                                                    <div class="product-price">
                                                                        <span
                                                                            class="font-extrabold text-green-800 new-price"><b>{{ number_format($i->price, 2) . ' €' }}</b></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endisset
                                                <!--Single Product End-->
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="single-offer">
                                                    <div class="offer-img img-full">
                                                        <a href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_platform=Nintendo%20Switch"
                                                            target="_blank"
                                                            class="border-b nk-btn-rounded nes-btn is-error text-lg retro-sell btn-block">@lang('messages.see_all')</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!--Product Tab End-->
                            </div>
                            <!--Orange Juice Product End-->
                            <!--Vegetable Fresh Product Start-->
                            <div class="vegetable-fresh-product pt-50">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <!--Section Title1 Start-->
                                        <div class="section-title1-border">
                                            <div class="section-title1">
                                                <font color="#ffd3b1">
                                                    <h3 class="retro">@lang('messages.azar_in_sale')</h3>
                                                </font>
                                            </div>
                                        </div>
                                        <!--Section Title1 End-->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <!--Product Tab Menu Start-->
                                        <div class="product-tab-menu-area">
                                            <div class="product-tab">
                                                <ul class="nav">
                                                    <li><a class="active" data-toggle="tab" href="#random"><span
                                                                class="text-base font-extrabold text-gray-800 hover:text-red-500 retro">-</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--Product Tab Menu End-->
                                    </div>
                                </div>
                                <?php
                                $duration = [];
                                $t = '';
                                $total_numeros = '';
                                $suma = '';
                                $m = '';
                                $prueba = '';
                                
                                foreach ($inventory_random_product_sale as $item) {
                                    $cantidad = $item->price;
                                    $duration[] = $cantidad;
                                    $total_numeros = count($duration);
                                    $total = max($duration);
                                    $t = min($duration);
                                    $suma = array_sum($duration);
                                    $m = $suma / $total_numeros;
                                    $prueba = $item->created_at;
                                }
                                
                                ?>
                                <!--Product Tab Start-->
                                <div class="tab-content">
                                    <div id="random" class="tab-pane fade show active">
                                        <div class="row">
                                            <div class="mb-20 all-product owl-carousel">
                                                @foreach ($inventory_random_product_sale as $i)
                                                    <div class="col-lg-12 item-col">
                                                        <div class="single-product">
                                                            <div class="product-img">
                                                                <a href="{{ route('product-show', $i->product->id) }}"
                                                                    target="_blank">
                                                                    @if ($i->images->first())
                                                                        <img class="first-img"
                                                                            src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                                            alt="">
                                                                        @if ($i->images->get(1))
                                                                            <img class="hover-img"
                                                                                src="{{ url('/uploads/inventory-images/' . $i->images->get(1)->image_path) }}"
                                                                                alt="">
                                                                        @endif
                                                                    @elseif ($i->product->images->first())
                                                                        <img class="first-img"
                                                                            src="{{ url('/images/' . $i->product->images->first()->image_path) }}"
                                                                            alt="">
                                                                        @if ($i->product->images->get(1))
                                                                            <img class="hover-img"
                                                                                src="{{ url('/images/' . $i->product->images->get(1)->image_path) }}"
                                                                                alt="">
                                                                        @endif
                                                                    @else
                                                                        <img loading="lazy" class="first-img"
                                                                            src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                                            alt="">
                                                                    @endif
                                                                </a>
                                                                <span class="text-xs font-bold sicker">
                                                                    <a href="{{ route('user-info', $i->user->user_name) }}"
                                                                        target="_blank">
                                                                        <font color="white">
                                                                            {{ $i->user->user_name }} -
                                                                            @include('partials.pais_user_home')
                                                                        </font>
                                                                    </a>
                                                                </span>
                                                                <ul class="product-action">
                                                                </ul>
                                                            </div>
                                                            <div class="product-content">
                                                                <h2 class="text-gray-900 retro small">
                                                                    <a href="{{ route('product-show', $i->product->id) }}"
                                                                        target="_blank">
                                                                        <b>{{ str_limit($i->product->name, 45) }}</b>
                                                                    </a>
                                                                </h2>
                                                                <div class="rating">
                                                                    {{ $i->product->platform . ' - ' . $i->product->region }}
                                                                </div>
                                                                <div class="product-price">
                                                                    <span
                                                                        class="font-extrabold text-green-800 new-price"><b>{{ number_format($i->price, 2) . ' €' }}</b></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach



                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!--Product Tab End-->
                                <!--Offer Image End-->
                            </div>
                            <!--Vegetable Fresh Product End-->
                            <!--Food & Bevereages Product Start-->
                            <div class="food-and-beverages pt-50">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <!--Section Title1 Start-->
                                        <div class="section-title1-border">
                                            <div class="section-title1">
                                                <font color="#ffd3b1">
                                                    <h3 class="retro">@lang('messages.last_search')</h3>
                                                </font>
                                            </div>
                                        </div>
                                        <!--Section Title1 End-->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <!--Product Tab Menu Start-->
                                        <div class="product-tab-menu-area">
                                            <div class="product-tab">
                                                <ul class="nav">
                                                    <li><a class="active" data-toggle="tab" href="#mostSold"><span
                                                                class="text-base font-extrabold text-gray-800 hover:text-red-500 retro">-</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--Product Tab Menu End-->
                                    </div>
                                </div>
                                <!--Product Tab Start-->
                                <div class="tab-content">
                                    <div id="mostSold" class="tab-pane fade show active">
                                        <div class="row">
                                            <div class="mb-20 all-product owl-carousel">
                                                <!--Single Product Start-->
                                                @foreach ($viewData['inventory_random_product'] as $i)
                                                    <div class="col-lg-12 item-col">
                                                        <div class="single-product">
                                                            <div class="product-img">
                                                                <a href="{{ route('product-show', $i->id) }}"
                                                                    target="_blank">
                                                                    @if ($i->images->first())
                                                                        <img class="first-img"
                                                                            src="{{ url('/images/' . $i->images->first()->image_path) }}"
                                                                            alt="">
                                                                        @if ($i->images->get(1))
                                                                            <img class="hover-img"
                                                                                src="{{ url('/images/' . $i->images->get(1)->image_path) }}"
                                                                                alt="">
                                                                        @endif
                                                                    @else
                                                                        <img loading="lazy" class="first-img"
                                                                            src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                                            alt="">
                                                                    @endif
                                                                </a>
                                                            </div>
                                                            <div class="product-content">
                                                                <h2 class="text-gray-900 retro small">
                                                                    <a href="{{ route('product-show', $i->id) }}"
                                                                        target="_blank">
                                                                        <b>{{ str_limit($i->name, 45) }}</b>
                                                                    </a>
                                                                </h2>
                                                                <div class="rating">
                                                                    {{ $i->platform . ' - ' . $i->region }}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                                <!--Single Product End-->
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!--Product Tab End-->
                                <!--Offer Image Start-->
                                @if ($viewData['locale'] == 'es')
                                    @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                    @else
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <br>
                                                <div class="single-offer">
                                                    <div class="offer-img img-full">
                                                        @isset($viewData['main_banner_2'])
                                                            @foreach ($viewData['main_banner_2'] as $cItem)
                                                                <a href="{{ $cItem->link }}"><img loading="lazy"
                                                                        src="{{ asset('uploads/banners-images/' . $cItem['image_path']) }}"
                                                                        alt=""></a>
                                                            @endforeach
                                                        @endisset
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @elseif($viewData['locale'] == 'en')
                                    @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                    @else
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <br>
                                                <div class="single-offer">
                                                    <div class="offer-img img-full">
                                                        @isset($viewData['main_banner_2_en'])
                                                            @foreach ($viewData['main_banner_2_en'] as $cItem)
                                                                <a href="{{ $cItem->link }}"><img loading="lazy"
                                                                        src="{{ asset('uploads/banners-images/' . $cItem['image_path']) }}"
                                                                        alt=""></a>
                                                            @endforeach
                                                        @endisset
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @elseif($viewData['locale'] == 'fr')
                                    @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                    @else
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <br>
                                                <div class="single-offer">
                                                    <div class="offer-img img-full">
                                                        @isset($viewData['main_banner_2_fr'])
                                                            @foreach ($viewData['main_banner_2_fr'] as $cItem)
                                                                <a href="{{ $cItem->link }}"><img loading="lazy"
                                                                        src="{{ asset('uploads/banners-images/' . $cItem['image_path']) }}"
                                                                        alt=""></a>
                                                            @endforeach
                                                        @endisset
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif


                                <!--Offer Image End-->
                            </div>
                            <!--Food & Bevereages Product End-->
                            <!--Bestseller Product Start-->

                            <!--Bestseller Product End-->
                        </div>
                        <!--Left Side Product End-->

                        <!--Right Side Product Start-->
                        <div class="col-lg-3 col-md-3">
                            @if ($viewData['locale'] == 'es')
                                @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                @else
                                    <div class="offer-img-area pb-20">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                @isset($sites)
                                                    @foreach ($sites as $cItem)
                                                        <div class="mb-20 single-offer">
                                                            <div class="offer-img img-full">

                                                                <a href="/site/{{ $cItem->category }}">
                                                                    <img src="{{ asset('uploads/banners-images/' . $cItem['image_path']) }}"
                                                                        alt=""></a>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endisset

                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @elseif($viewData['locale'] == 'en')
                                @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                @else
                                    <div class="offer-img-area pb-20">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                @isset($sites_en)
                                                    @foreach ($sites_en as $cItem)
                                                        <div class="mb-20 single-offer">
                                                            <div class="offer-img img-full">

                                                                <a href="/site/{{ $cItem->category }}">
                                                                    <img src="{{ asset('uploads/banners-images/' . $cItem['image_path']) }}"
                                                                        alt=""></a>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endisset

                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @elseif($viewData['locale'] == 'fr')
                                @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                @else
                                    <div class="offer-img-area pb-20">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                @isset($sites_fr)
                                                    @foreach ($sites_fr as $cItem)
                                                        <div class="mb-20 single-offer">
                                                            <div class="offer-img img-full">

                                                                <a href="/site/{{ $cItem->category }}">
                                                                    <img src="{{ asset('uploads/banners-images/' . $cItem['image_path']) }}"
                                                                        alt=""></a>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endisset

                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endif
                            <div class="section-title2">
                                <center>
                                    <h4 class="text-gray-900 font-extrabold"><b>@lang('messages.check_point')</b></h4>
                                </center>

                            </div>
                            <div class="mb-20 single-offer">
                                <div class="offer-img img-full">
                                    @isset($magazine)
                                        @foreach ($magazine as $cItem)
                                            <a href="https://twitter.com/CheckRevista?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor"
                                                target="_blank">
                                                <img src="{{ asset('uploads/banners-images/' . $cItem['image_path']) }}"
                                                    alt=""></a>
                                        @endforeach
                                    @endisset
                                </div>
                            </div>
                            <!--Hot Deal Product Start-->
                            <div class="mb-40 new-arrivals-product">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="new-arrivals-product-title">
                                            <!--Section Title2 Start-->
                                            <div class="section-title2">
                                                <center>
                                                    <h3 class="text-xs retro">
                                                        @lang('messages.top_seller')<br>@lang('messages.top_seller_two')
                                                    </h3>
                                                </center>
                                            </div>
                                            <!--Section Title2 End-->
                                            <!--Hot Deal Single Product Start-->
                                            <div class="hot-del-single-product">
                                                <div class="row slide-active2">

                                                    @foreach ($viewData['usuario'] as $u)
                                                        @if (App\AppOrgOrder::where('seller_user_id', $u->id)->whereIn('status', ['DD'])->count() > 0)
                                                            <!--Single Product Start-->
                                                            <div class="col-lg-12">
                                                                <div class="row no-gutters single-product style-2">
                                                                    @if ($u->profile_picture)
                                                                        <div class="col-4">
                                                                            <div class="product-img">
                                                                                <a href="{{ route('user-info', $u->user_name) }}"
                                                                                    target="_blank">
                                                                                    <img loading="lazy" class="first-img"
                                                                                        src="{{ url($u->profile_picture) }}"
                                                                                        alt="">
                                                                                </a>
                                                                                <ul class="product-action">
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    @else
                                                                        <div class="col-4">
                                                                            <div class="product-img">
                                                                                <a href="{{ route('user-info', $u->user_name) }}"
                                                                                    target="_blank">
                                                                                    <img loading="lazy" class="first-img"
                                                                                        src="{{ asset('img/profile-picture-not-found.png') }}"
                                                                                        alt="">
                                                                                </a>
                                                                                <ul class="product-action">
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                    <div class="col-6">
                                                                        <div class="product-content">
                                                                            @if (App\SysUserRoles::where('user_id', $u->id)->whereIn('role_id', [2])->count() > 0)
                                                                                <h5
                                                                                    class="text-base font-bold text-center profesional retro hover:text-red-500">
                                                                                    <a href="{{ route('user-info', $u->user_name) }}"
                                                                                        target="_blank">{{ str_limit($u->user_name, 25) }}
                                                                                    </a>
                                                                                </h5>
                                                                            @else
                                                                                <h5
                                                                                    class="text-base font-bold text-center text-gray-800 retro hover:text-red-500">
                                                                                    <a href="{{ route('user-info', $u->user_name) }}"
                                                                                        target="_blank">{{ str_limit($u->user_name, 25) }}
                                                                                    </a>
                                                                                </h5>
                                                                            @endif
                                                                            <span
                                                                                class="text-base rating retro-sell hover:text-red-700">
                                                                                <a href="{{ route('user-inventory', $u->user_name) }}"
                                                                                    target="_blank">
                                                                                    {{ App\AppOrgOrder::where('seller_user_id', $u->id)->whereIn('status', ['DD'])->count() }}
                                                                                    Ventas
                                                                                </a>
                                                                            </span>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endforeach


                                                    <!--Single Product End-->
                                                </div>
                                            </div>
                                            <!--Hot Deal Single Product Start-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Hot Deal Product End-->
                            <div class="section-title2">
                                <center>
                                    <h4 class="text-gray-900 font-extrabold"><b>@lang('messages.how_to_use')</b></h4>
                                </center>

                            </div>
                            <div class="mb-20 single-offer">
                                <div class="offer-img img-full">

                                    <a href="#">
                                        <img src="{{ asset('img/compra.png') }}" alt=""></a>
                                </div>
                            </div>
                            <!--Offer Image Start-->



                            <div class="col-lg-12">
                                <div class="mb-30 single-offer">
                                    <div class="offer-img img-full">
                                        <a href="#"><img loading="lazy" src="{{ asset('img/how/tobuy.jpg') }}"
                                                alt=""></a>
                                    </div>
                                </div>
                            </div>


                            <div class="mb-20 single-offer">
                                <div class="offer-img img-full">

                                    <a href="#">
                                        <img src="{{ asset('img/venta.png') }}" alt=""></a>
                                </div>
                            </div>
                            <!--Offer Image End-->
                            <br>
                            <!--New arrivals Product Start-->
                            <div class="mb-20 new-arrivals-product">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="new-arrivals-product-title">
                                            <!--Section Title2 Start-->
                                            <div class="section-title2">
                                                <h3 class="text-xs retro">
                                                    <center>
                                                        @lang('messages.news_home')<br>@lang('messages.news_product_home')
                                                    </center>
                                                </h3>
                                            </div>
                                            <!--Section Title2 End-->
                                            <!--Hot Deal Single Product Start-->
                                            <div class="hot-del-single-product">
                                                <div class="row slide-active2">
                                                    @isset($inventory_latest_product)
                                                        @foreach ($inventory_latest_product as $cItem)
                                                            <!--Single Product Start-->
                                                            <div class="col-lg-12">
                                                                <div class="row no-gutters single-product style-2">
                                                                    <div class="col-4">
                                                                        <div class="product-img">
                                                                            <a href="{{ route('product-show', $cItem->id) }}"
                                                                                target="_blank">

                                                                                @if ($cItem->images->first())
                                                                                    <img class="first-img"
                                                                                        src="{{ url('/images/' . $cItem->images->first()->image_path) }}"
                                                                                        alt="">
                                                                                    @if ($cItem->images->get(1))
                                                                                        <img class="hover-img"
                                                                                            src="{{ url('/images/' . $cItem->images->get(1)->image_path) }}"
                                                                                            alt="">
                                                                                    @endif
                                                                                @else
                                                                                    <img loading="lazy" class="first-img"
                                                                                        src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                                                        alt="">
                                                                                @endif
                                                                            </a>
                                                                            <ul class="product-action">
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="product-content">
                                                                            <h2
                                                                                class="text-xs font-bold text-gray-800 hover:text-red-500">
                                                                                <a href="{{ route('product-show', $cItem->id) }}"
                                                                                    target="_blank">{{ str_limit($cItem->name, 25) }}</a>
                                                                            </h2>
                                                                            <div class="rating">
                                                                                {{ $cItem->platform . ' - ' . $cItem->region }}
                                                                            </div>
                                                                            <div class="product-price">
                                                                                <span class="new-price"></span>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @endisset

                                                    <!--Single Product End-->
                                                </div>
                                            </div>
                                            <!--Hot Deal Single Product Start-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Right Side Product End-->
                    </div>
                    <div class="bg-white nk-gap"></div>
                </div>

                <Section class="container bg-white hot-categories-area pt-100 pb-90">
                    <div class="___class_+?352___">
                        <div class="row">
                            <div class="col-lg-12">

                                <!--Section Title1 Start-->
                                <div class="section-title1-border">
                                    <div class="section-title1">
                                        <h3 class="retro">@lang('messages.category')</h3>
                                    </div>
                                </div>
                                <!--Section Title1 End-->
                            </div>
                        </div>
                        @if ((new \Jenssegers\Agent\Agent())->isMobile())
                        @else
                            <div class="row">
                                <div class="col-md-12">
                                    <!--Product Tab Menu Start-->
                                    <div class="product-tab-menu-area">
                                        <div class="product-tab">
                                            <ul class="nav">
                                                <li><a href="{{ url('/MarketPlaceStore') }}" target="_blank"
                                                        class="retro-sell nes-btn is-error">
                                                        <font color="#ffd3b1">@lang('messages.see_all_two')</font>
                                                    </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!--Product Tab Menu End-->
                                </div>
                            </div>
                        @endif
                        <!--Hot Categories Start-->
                        <div class="row hot-categories">
                            <!--Single Categories Start-->
                            <div class="col-lg-3 col-md-6">
                                <div class="single-categories">
                                    <div class="categories-img img-category">
                                        <a href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Juegos">
                                            <img loading="lazy" src="{{ asset('img/productos/juegos.png') }}">
                                        </a>
                                    </div>
                                    <div class="categories-content">
                                        <h3 class="text-sm font-extrabold"><a
                                                href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Juegos">
                                                <center><b>@lang('messages.games')</b></center>
                                            </a></h3>
                                        <ul class="text-base catgories-list retro">
                                            <li><a
                                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Juegos&search_region=PAL-EU">PAL-EU</a>
                                            </li>
                                            <li><a
                                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Juegos&search_region=PAL-ESP">PAL-ESP</a>
                                            </li>
                                            <li><a
                                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Juegos&search_region=NTSC-U">NTSC-U</a>
                                            </li>
                                            <li><a
                                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Juegos">@lang('messages.other_games')</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <!--Single Categories End-->
                            <!--Single Categories Start-->
                            <div class="col-lg-3 col-md-6">
                                <div class="single-categories">
                                    <div class="categories-img img-category">
                                        <a
                                            href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Consolas">
                                            <img loading="lazy" src="{{ asset('img/productos/consolas.png') }}">
                                        </a>
                                    </div>
                                    <div class="categories-content">
                                        <h3 class="text-sm font-extrabold"><a
                                                href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Consolas">
                                                <center><b>@lang('messages.consoles')</b></center>
                                            </a></h3>
                                        <ul class="text-base catgories-list retro">
                                            <li><a
                                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Consolas&search_platform=Nintendo%20Switch">Nintendo
                                                    Switch</a></li>
                                            <li><a
                                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Consolas&search_platform=PlayStation">PlayStation</a>
                                            </li>
                                            <li><a
                                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Consolas&search_platform=Xbox%20One">Xbox
                                                    One</a></li>
                                            <li><a
                                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Consolas">@lang('messages.other_consoles')</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--Single Categories End-->
                            <!--Single Categories Start-->
                            <div class="col-lg-3 col-md-6">
                                <div class="single-categories">
                                    <div class="categories-img img-category">
                                        <a
                                            href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Perif%C3%A9ricos">
                                            <img loading="lazy" src="{{ asset('img/productos/perifericos.png') }}"
                                                alt="">
                                        </a>
                                    </div>
                                    <div class="categories-content">
                                        <h3 class="text-sm font-extrabold"><a
                                                href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Perif%C3%A9ricos">
                                                <center><b>@lang('messages.peripherals')</b></center>
                                            </a></h3>
                                        <ul class="text-base catgories-list retro">
                                            <li><a
                                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Mandos">@lang('messages.controls')</a>
                                            </li>
                                            <li><a
                                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Micr%C3%B3fonos">@lang('messages.microphones')</a>
                                            </li>
                                            <li><a
                                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Teclados">@lang('messages.peripherals')</a>
                                            </li>
                                            <li><a
                                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Perif%C3%A9ricos">@lang('messages.other_peripherics')</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--Single Categories End-->
                            <!--Single Categories Start-->
                            <div class="col-lg-3 col-md-6">
                                <div class="single-categories">
                                    <div class="categories-img img-category">
                                        <a
                                            href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Accesorios">
                                            <img loading="lazy" src="{{ asset('img/productos/accesorios.png') }}">
                                        </a>
                                    </div>
                                    <div class="categories-content">
                                        <h3 class="text-sm font-extrabold"><a href="#">
                                                <center><b>@lang('messages.accesories')</b></center>
                                            </a></h3>
                                        <ul class="text-base catgories-list retro">
                                            <li><a
                                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Fundas">@lang('messages.funded')</a>
                                            </li>
                                            <li><a
                                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Cables">@lang('messages.cable')</a>
                                            </li>
                                            <li><a
                                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Cables">@lang('messages.chargers')</a>
                                            </li>
                                            <li><a
                                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Accesorios">@lang('messages.other_accesories')</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--Single Categories End-->
                            @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                <div class="col-lg-12">
                                    <a href="{{ url('/MarketPlaceStore') }}" target="_blank"
                                        class="retro-prueba nes-btn is-error small">
                                        <font color="#ffd3b1">@lang('messages.see_all_two')</font>
                                    </a>
                                </div>
                            @endif
                        </div>
                        <!--Hot Categories End-->
                    </div>
                </Section>

            </section>
            <!--All Product Area End-->
            <!--Offer Image Area Start-->

            <!--Brand Area End-->

            <!--Footer Area End-->
            <!--Modal Start-->

            <!--Modal End-->
        </div>
    @endif
@endsection

@section('content-script-include')
    <!-- Flickity -->
    <script src="{{ asset('assets/vendor/flickity/dist/flickity.pkgd.min.js') }}"></script>

    <!-- Hammer.js -->
    <script src="{{ asset('assets/vendor/hammerjs/hammer.min.js') }}"></script>
@endsection

