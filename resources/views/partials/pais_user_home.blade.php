@if ($i->user->country->name == 'Spain')
    <span class="flag-icon flag-icon-esp"></span>
@elseif($i->user->country->name == 'Andorra')
    <span class="flag-icon flag-icon-and"></span>
@elseif($i->user->country->name == 'Åland Islands')
    <span class="flag-icon flag-icon-ala"></span>
@elseif($i->user->country->name == 'Albania')
    <span class="flag-icon flag-icon-alb"></span>
@elseif($i->user->country->name == 'Austria')
    <span class="flag-icon flag-icon-aut"></span>
@elseif($i->user->country->name == 'Belarus')
    <span class="flag-icon flag-icon-blr"></span>
@elseif($i->user->country->name == 'Belgium')
    <span class="flag-icon flag-icon-bel"></span>
@elseif($i->user->country->name == 'Bosnia and Herzegovina')
    <span class="flag-icon flag-icon-bih"></span>
@elseif($i->user->country->name == 'Bulgaria')
    <span class="flag-icon flag-icon-bgr"></span>
@elseif($i->user->country->name == 'Croatia')
    <span class="flag-icon flag-icon-hrv"></span>
@elseif($i->user->country->name == 'Czech Republic')
    <span class="flag-icon flag-icon-cze"></span>
@elseif($i->user->country->name == 'Denmark')
    <span class="flag-icon flag-icon-dnk"></span>
@elseif($i->user->country->name == 'Estonia')
    <span class="flag-icon flag-icon-est"></span>
@elseif($i->user->country->name == 'Faroe Islands')
    <span class="flag-icon flag-icon-fro"></span>
@elseif($i->user->country->name == 'Finland')
    <span class="flag-icon flag-icon-fin"></span>
@elseif($i->user->country->name == 'France')
    <span class="flag-icon flag-icon-fra"></span>
@elseif($i->user->country->name == 'Germany')
    <span class="flag-icon flag-icon-deu"></span>
@elseif($i->user->country->name == 'Gibraltar')
    <span class="flag-icon flag-icon-gib"></span>
@elseif($i->user->country->name == 'Guernsey')
    <span class="flag-icon flag-icon-ggy"></span>
@elseif($i->user->country->name == 'Holy See')
    <span class="flag-icon flag-icon-vat"></span>
@elseif($i->user->country->name == 'Hungary')
    <span class="flag-icon flag-icon-hun"></span>
@elseif($i->user->country->name == 'Iceland')
    <span class="flag-icon flag-icon-isl"></span>
@elseif($i->user->country->name == 'Ireland')
    <span class="flag-icon flag-icon-irl"></span>
@elseif($i->user->country->name == 'Isle of Man')
    <span class="flag-icon flag-icon-imn"></span>
@elseif($i->user->country->name == 'Italy')
    <span class="flag-icon flag-icon-ita"></span>
@elseif($i->user->country->name == 'Jersey')
    <span class="flag-icon flag-icon-jey"></span>
@elseif($i->user->country->name == 'Latvia')
    <span class="flag-icon flag-icon-lva"></span>
@elseif($i->user->country->name == 'Liechtenstein')
    <span class="flag-icon flag-icon-lie"></span>
@elseif($i->user->country->name == 'Lithuania')
    <span class="flag-icon flag-icon-ltu"></span>
@elseif($i->user->country->name == 'Luxembourg')
    <span class="flag-icon flag-icon-lux"></span>
@elseif($i->user->country->name == 'Macedonia (the former Yugoslav Republic of)')
    <span class="flag-icon flag-icon-mkd"></span>
@elseif($i->user->country->name == 'Malta')
    <span class="flag-icon flag-icon-mlt"></span>
@elseif($i->user->country->name == 'Moldova (Republic of)')
    <span class="flag-icon flag-icon-mda"></span>
@elseif($i->user->country->name == 'Monaco')
    <span class="flag-icon flag-icon-mco"></span>
@elseif($i->user->country->name == 'Montenegro')
    <span class="flag-icon flag-icon-mne"></span>
@elseif($i->user->country->name == 'Netherlands')
    <span class="flag-icon flag-icon-nld"></span>
@elseif($i->user->country->name == 'Norway')
    <span class="flag-icon flag-icon-nor"></span>
@elseif($i->user->country->name == 'Poland')
    <span class="flag-icon flag-icon-pol"></span>
@elseif($i->user->country->name == 'Portugal')
    <span class="flag-icon flag-icon-prt"></span>
@elseif($i->user->country->name == 'Romania')
    <span class="flag-icon flag-icon-rou"></span>
@elseif($i->user->country->name == 'Russian Federation')
    <span class="flag-icon flag-icon-rus"></span>
@elseif($i->user->country->name == 'San Marino')
    <span class="flag-icon flag-icon-smr"></span>
@elseif($i->user->country->name == 'Serbia')
    <span class="flag-icon flag-icon-srb"></span>
@elseif($i->user->country->name == 'Slovakia')
    <span class="flag-icon flag-icon-svk"></span>
@elseif($i->user->country->name == 'Slovenia')
    <span class="flag-icon flag-icon-svn"></span>
@elseif($i->user->country->name == 'Svalbard and Jan Mayen')
    <span class="flag-icon flag-icon-sjm"></span>
@elseif($i->user->country->name == 'Sweden')
    <span class="flag-icon flag-icon-swe"></span>
@elseif($i->user->country->name == 'Switzerland')
    <span class="flag-icon flag-icon-swz"></span>
@elseif($i->user->country->name == 'Ukraine')
    <span class="flag-icon flag-icon-ukr"></span>
@elseif($i->user->country->name == 'United Kingdom of Great Britain and NorthernIreland')
    <span class="flag-icon flag-icon-gbr"></span>
@endif
