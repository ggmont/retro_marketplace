@if ((new \Jenssegers\Agent\Agent())->isMobile())

    @if(session('flash_message'))

        <div id="notification-welcome" class="notification-box">
            <div class="notification-dialog android-style">
                <div class="notification-header">
                    <div class="in">
                        <img src="{{ asset('img/RGM.png') }}" alt="image" class="imaged w24 rounded">
                        <strong>Notificación automática</strong>
                        <span>Justo Ahora</span>
                    </div>
                    <a href="#" class="close-button">
                        <ion-icon name="close"></ion-icon>
                    </a>
                </div>
                <div class="notification-content">
                    <div class="in">
                        <h3 class="subtitle">RGM
                        </h3>
                        <div class="text">
                            {{ session('flash_message') }}
                            <br><br>
                            @if ($errors->any())
 
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
     
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@else
    @if (Session::has('flash_message'))
        <div class="row">
            <div class="col-md-12">
                <div
                    class="alert {{ Session::get('flash_class') }} {{ Session::has('flash_important') ? 'alert-important' : '' }}">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong class="text-center">{{ Session::get('flash_message') }}</strong>
                </div>
            </div>
        </div>
    @endif


@endif
