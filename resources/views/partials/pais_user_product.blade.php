@if ($producto->user->country->name == 'Spain')
    <span class="flag-icon flag-icon-esp"></span>
@elseif($producto->user->country->name == 'Andorra')
    <span class="flag-icon flag-icon-and"></span>
@elseif($producto->user->country->name == 'Åland Islands')
    <span class="flag-icon flag-icon-ala"></span>
@elseif($producto->user->country->name == 'Albania')
    <span class="flag-icon flag-icon-alb"></span>
@elseif($producto->user->country->name == 'Austria')
    <span class="flag-icon flag-icon-aut"></span>
@elseif($producto->user->country->name == 'Belarus')
    <span class="flag-icon flag-icon-blr"></span>
@elseif($producto->user->country->name == 'Belgium')
    <span class="flag-icon flag-icon-bel"></span>
@elseif($producto->user->country->name == 'Bosnia and Herzegovina')
    <span class="flag-icon flag-icon-bih"></span>
@elseif($producto->user->country->name == 'Bulgaria')
    <span class="flag-icon flag-icon-bgr"></span>
@elseif($producto->user->country->name == 'Croatia')
    <span class="flag-icon flag-icon-hrv"></span>
@elseif($producto->user->country->name == 'Czech Republic')
    <span class="flag-icon flag-icon-cze"></span>
@elseif($producto->user->country->name == 'Denmark')
    <span class="flag-icon flag-icon-dnk"></span>
@elseif($producto->user->country->name == 'Estonia')
    <span class="flag-icon flag-icon-est"></span>
@elseif($producto->user->country->name == 'Faroe Islands')
    <span class="flag-icon flag-icon-fro"></span>
@elseif($producto->user->country->name == 'Finland')
    <span class="flag-icon flag-icon-fin"></span>
@elseif($producto->user->country->name == 'France')
    <span class="flag-icon flag-icon-fra"></span>
@elseif($producto->user->country->name == 'Germany')
    <span class="flag-icon flag-icon-deu"></span>
@elseif($producto->user->country->name == 'Gibraltar')
    <span class="flag-icon flag-icon-gib"></span>
@elseif($producto->user->country->name == 'Guernsey')
    <span class="flag-icon flag-icon-ggy"></span>
@elseif($producto->user->country->name == 'Holy See')
    <span class="flag-icon flag-icon-vat"></span>
@elseif($producto->user->country->name == 'Hungary')
    <span class="flag-icon flag-icon-hun"></span>
@elseif($producto->user->country->name == 'Iceland')
    <span class="flag-icon flag-icon-isl"></span>
@elseif($producto->user->country->name == 'Ireland')
    <span class="flag-icon flag-icon-irl"></span>
@elseif($producto->user->country->name == 'Isle of Man')
    <span class="flag-icon flag-icon-imn"></span>
@elseif($producto->user->country->name == 'Italy')
    <span class="flag-icon flag-icon-ita"></span>
@elseif($producto->user->country->name == 'Jersey')
    <span class="flag-icon flag-icon-jey"></span>
@elseif($producto->user->country->name == 'Latvia')
    <span class="flag-icon flag-icon-lva"></span>
@elseif($producto->user->country->name == 'Liechtenstein')
    <span class="flag-icon flag-icon-lie"></span>
@elseif($producto->user->country->name == 'Lithuania')
    <span class="flag-icon flag-icon-ltu"></span>
@elseif($producto->user->country->name == 'Luxembourg')
    <span class="flag-icon flag-icon-lux"></span>
@elseif($producto->user->country->name == 'Macedonia (the former Yugoslav Republic of)')
    <span class="flag-icon flag-icon-mkd"></span>
@elseif($producto->user->country->name == 'Malta')
    <span class="flag-icon flag-icon-mlt"></span>
@elseif($producto->user->country->name == 'Moldova (Republic of)')
    <span class="flag-icon flag-icon-mda"></span>
@elseif($producto->user->country->name == 'Monaco')
    <span class="flag-icon flag-icon-mco"></span>
@elseif($producto->user->country->name == 'Montenegro')
    <span class="flag-icon flag-icon-mne"></span>
@elseif($producto->user->country->name == 'Netherlands')
    <span class="flag-icon flag-icon-nld"></span>
@elseif($producto->user->country->name == 'Norway')
    <span class="flag-icon flag-icon-nor"></span>
@elseif($producto->user->country->name == 'Poland')
    <span class="flag-icon flag-icon-pol"></span>
@elseif($producto->user->country->name == 'Portugal')
    <span class="flag-icon flag-icon-prt"></span>
@elseif($producto->user->country->name == 'Romania')
    <span class="flag-icon flag-icon-rou"></span>
@elseif($producto->user->country->name == 'Russian Federation')
    <span class="flag-icon flag-icon-rus"></span>
@elseif($producto->user->country->name == 'San Marino')
    <span class="flag-icon flag-icon-smr"></span>
@elseif($producto->user->country->name == 'Serbia')
    <span class="flag-icon flag-icon-srb"></span>
@elseif($producto->user->country->name == 'Slovakia')
    <span class="flag-icon flag-icon-svk"></span>
@elseif($producto->user->country->name == 'Slovenia')
    <span class="flag-icon flag-icon-svn"></span>
@elseif($producto->user->country->name == 'Svalbard and Jan Mayen')
    <span class="flag-icon flag-icon-sjm"></span>
@elseif($producto->user->country->name == 'Sweden')
    <span class="flag-icon flag-icon-swe"></span>
@elseif($producto->user->country->name == 'Switzerland')
    <span class="flag-icon flag-icon-swz"></span>
@elseif($producto->user->country->name == 'Ukraine')
    <span class="flag-icon flag-icon-ukr"></span>
@elseif($producto->user->country->name == 'United Kingdom of Great Britain and NorthernIreland')
    <span class="flag-icon flag-icon-gbr"></span>
@endif
