                                   @if ($u->country->name == 'Spain')
                                       <img src="{{ asset('img/paises/spain.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="España" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Andorra")
                                       <img src="{{ asset('img/paises/andorra.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Andorra" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Åland Islands")
                                       <img src="{{ asset('img/paises/alanisland.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Åland Islands" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Albania")
                                       <img src="{{ asset('img/paises/albania.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Albania" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Austria")
                                       <img src="{{ asset('img/paises/austria.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Austria" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Belarus")
                                       <img src="{{ asset('img/paises/bielorussia.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Bielorrusia" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Belgium")
                                       <img src="{{ asset('img/paises/belgica.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Belgica" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Bosnia and Herzegovina")
                                       <img src="{{ asset('img/paises/bosnia.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Bosnia and Herzegovina"
                                           data-placement="top" data-trigger="hover">
                                   @elseif($u->country->name == "Bulgaria")
                                       <img src="{{ asset('img/paises/bulgaria.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Bulgaria" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Croatia")
                                       <img src="{{ asset('img/paises/croacia.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Croacia" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Czech Republic")
                                       <img src="{{ asset('img/paises/checa.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Checa" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Denmark")
                                       <img src="{{ asset('img/paises/dinamarca.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Dinamarca" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Estonia")
                                       <img src="{{ asset('img/paises/estonia.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Estonia" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Faroe Islands")
                                       <img src="{{ asset('img/paises/feroe.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Faroe Islands" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Finland")
                                       <img src="{{ asset('img/paises/finlandia.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Finlandia" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "France")
                                       <img src="{{ asset('img/paises/france.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Francia" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Germany")
                                       <img src="{{ asset('img/paises/alemania.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Alemania" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Gibraltar")
                                       <img src="{{ asset('img/paises/gibraltar.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Gilbraltar" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Guernsey")
                                       <img src="{{ asset('img/paises/guernsey.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Guernsey" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Holy See")
                                       <img src="{{ asset('img/paises/holysee.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Holy See" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Hungary")
                                       <img src="{{ asset('img/paises/hungria.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Hungria" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Iceland")
                                       <img src="{{ asset('img/paises/islandia.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Islandia" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Ireland")
                                       <img src="{{ asset('img/paises/irlanda.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Irlanda" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Isle of Man")
                                       <img src="{{ asset('img/paises/isla.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Isla" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Italy")
                                       <img src="{{ asset('img/paises/italy.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Italia" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Jersey")
                                       <img src="{{ asset('img/paises/jersey.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Jersey" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Latvia")
                                       <img src="{{ asset('img/paises/letonia.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Letonia" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Liechtenstein")
                                       <img src="{{ asset('img/paises/liechesten.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Liechtenstein" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Lithuania")
                                       <img src="{{ asset('img/paises/lituania.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Lithuania" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Luxembourg")
                                       <img src="{{ asset('img/paises/luxemburgo.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Luxemburgo" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Macedonia (the former Yugoslav Republic of)")
                                       <img src="{{ asset('img/paises/macedonia.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Macedonia" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Malta")
                                       <img src="{{ asset('img/paises/malta.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Malta" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Moldova (Republic of)")
                                       <img src="{{ asset('img/paises/moldavia.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Moldova" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Monaco")
                                       <img src="{{ asset('img/paises/monaco.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Monaco" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Montenegro")
                                       <img src="{{ asset('img/paises/montenegro.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Monte Negro" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Netherlands")
                                       <img src="{{ asset('img/paises/paisesbajos.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Netherlands" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Norway")
                                       <img src="{{ asset('img/paises/noruega.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Noruega" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Poland")
                                       <img src="{{ asset('img/paises/polonia.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Polonia" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Portugal")
                                       <img src="{{ asset('img/paises/portugal.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Portugal" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Romania")
                                       <img src="{{ asset('img/paises/rumania.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Romania" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Russian Federation")
                                       <img src="{{ asset('img/paises/russianfederation.png') }}" width="17"
                                           height="16" data-toggle="popover" data-content="Russian Federation"
                                           data-placement="top" data-trigger="hover">
                                   @elseif($u->country->name == "San Marino")
                                       <img src="{{ asset('img/paises/sanmarina.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="San Marino" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Serbia")
                                       <img src="{{ asset('img/paises/serbia.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Serbia" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Slovakia")
                                       <img src="{{ asset('img/paises/slovakia.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Slovakia" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Slovenia")
                                       <img src="{{ asset('img/paises/slovenia.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Slovenia" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Svalbard and Jan Mayen")
                                       <img src="{{ asset('img/paises/svalbard.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Svalbard and Jan Mayen"
                                           data-placement="top" data-trigger="hover">
                                   @elseif($u->country->name == "Sweden")
                                       <img src="{{ asset('img/paises/suecia.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Suecia" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Switzerland")
                                       <img src="{{ asset('img/paises/suiza.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Suiza" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "Ukraine")
                                       <img src="{{ asset('img/paises/ucrania.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Ucrania" data-placement="top"
                                           data-trigger="hover">
                                   @elseif($u->country->name == "United Kingdom of Great Britain and Northern
                                       Ireland")
                                       <img src="{{ asset('img/paises/reinounido.png') }}" width="17" height="16"
                                           data-toggle="popover" data-content="Reino Unido" data-placement="top"
                                           data-trigger="hover">
                                   @endif
