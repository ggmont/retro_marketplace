@extends('brcode.front.layout.app')
@section('content-css-include')
    <link rel="stylesheet" href="{{ asset('plugins/fileinput/css/fileinput.min.css') }}">
@endsection

@section('content')

    <div class="container">
        <div class="col-lg-12">
            <ul class="nk-breadcrumbs">
                <br>
                <li>
                    <div class="section-title1">
                        <h3 class="retro" style="font-size:20px;">@lang('messages.my_account')</h3>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="nk-gap-1"></div>

    <div class="container">
        <div class="row vertical-gap">
            <div class="col-lg-12">
                <span class="retro">
                    @include('partials.flash')
                </span>
                <div class="nk-tabs">

                    <ul class="nav nav-tabs" role="tablist">
                        @if ((new \Jenssegers\Agent\Agent())->isMobile())
                            &nbsp;&nbsp;&nbsp;
                        @else
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;
                        @endif
                        <li class="nav-item">
                            <a class="nav-link active" href="#tabs-1-1" role="tab" data-toggle="tab"> <span
                                    class="text-base font-extrabold retro">{{ __('Perfil') }}</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-2" role="tab" data-toggle="tab">
                                <span class="text-base font-extrabold retro">@lang('messages.postal_code_personal')</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-3" role="tab" data-toggle="tab">
                                <span class="text-base font-extrabold retro">{{ __('Saldo disponible') }}</span></a>
                        </li>
                        @if (count(Auth::user()->referidos) > 0)
                            <li class="nav-item">
                                <a class="nav-link" href="#tabs-1-4" role="tab" data-toggle="tab"> <span
                                        class="text-base font-extrabold retro">{{ __('Referidos') }}</span></a>
                            </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-5" role="tab" data-toggle="tab"> <span
                                    class="text-base font-extrabold retro">@lang('messages.public_perfil_user')</span></a>
                        </li>
                    </ul>


                    <div class="nk-gap"></div>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade show active" id="tabs-1-1">
                            <div class="col-sm-12" style="background-color: #FFFFFF">
                                <div class="nk-gap"></div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <form action="{{ route('upload_profile_image', ['id' => auth()->user()->id]) }}"
                                            method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <label for="input-res-1"
                                                class="block mb-2 text-sm font-extrabold tracking-wide text-gray-700 uppercase retro">@lang('messages.profile_image_op')</label>
                                            <div class="file-loading">
                                                <input id="input-20" name="profile_picture" type="file"
                                                    data-browse-on-zone-click="true">
                                            </div>
                                            <button type="submit" class="nk-btn nk-btn-lg nk-btn-rounded nes-btn btn-block">
                                                <font color="black"><span class="text-base font-extrabold retro"><i
                                                            class="fa fa-floppy-o"></i>
                                                        @lang('messages.save')</span></font>
                                            </button>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <form action="{{ route('upload_cover_image', ['id' => auth()->user()->id]) }}"
                                            method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <label for="input-res-1"
                                                class="block mb-2 text-sm font-extrabold tracking-wide text-gray-700 uppercase retro">@lang('messages.cover_image_op')</label>
                                            <div class="file-loading">
                                                <input id="input-19" name="cover_picture" type="file"
                                                    data-browse-on-zone-click="true">
                                            </div>
                                            <button type="submit" class="nk-btn nk-btn-lg nk-btn-rounded nes-btn btn-block">
                                                <font color="black"><span class="text-base font-extrabold retro"><i
                                                            class="fa fa-floppy-o"></i>
                                                        @lang('messages.save')</span></font>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                                <div class="nk-gap"></div>

                                <div class="flex flex-wrap mb-6 -mx-3">
                                    <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">

                                        <label
                                            class="block mb-2 text-sm font-extrabold tracking-wide text-gray-700 uppercase retro"
                                            for="grid-first-name">
                                            {{ __('Nombre de usuario') }} <i class="fa fa-user"></i>
                                        </label>


                                        <input
                                            class="px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border border-red-500 rounded appearance-none focus:outline-none focus:bg-white"
                                            id="inlineFormInputGroup" value="{{ auth()->user()->user_name }}" disabled
                                            style="color:#000" type="text" placeholder="Jane">

                                    </div>
                                    <div class="w-full px-3 md:w-1/2">
                                        <label
                                            class="block mb-2 text-sm font-extrabold tracking-wide text-gray-700 uppercase retro"
                                            for="grid-last-name">
                                            @lang('messages.email_users') <i class="fa fa-envelope"></i>
                                        </label>
                                        <form action="{{ url('/change/email') }}" method="post">
                                            {{ csrf_field() }}

                                            <input
                                                class="appearance-none   bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 @if ($errors->has('name')) is-invalid @endif"
                                                id="inlineFormInputGroup" name="email"
                                                value="{{ auth()->user()->email }}" required type="email">
                                            <button type="submit" class="nk-btn nk-btn-lg nk-btn-rounded nes-btn btn-block">
                                                <font color="black"><span class="text-base font-extrabold retro"><i
                                                            class="fa fa-floppy-o"></i>
                                                        @lang('messages.save')</span></font>
                                            </button>
                                            @if ($errors->has('email'))
                                                <span class="label label-danger">
                                                    @foreach ($errors->get('email') as $error)
                                                        {{ $error }}
                                                    @endforeach
                                                </span>
                                            @endif
                                        </form>
                                    </div>
                                </div>
                                <div class="nk-gap-2"></div>
                                <div class="flex flex-wrap mb-6 -mx-3">
                                    <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                                        <label
                                            class="block mb-2 text-sm font-extrabold tracking-wide text-gray-700 uppercase retro"
                                            for="grid-first-name">
                                            {{ __('Fecha de registro') }} <i class="fa fa-calendar"></i>
                                        </label>
                                        <p class="pb-8 text-base font-semibold retro">
                                            {{ auth()->user()->created_at }}
                                        </p>

                                    </div>
                                    <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                                        <label
                                            class="block mb-2 text-sm font-extrabold tracking-wide text-gray-700 uppercase retro"
                                            for="grid-first-name">
                                            {{ __('Tipo de cuenta') }} <i class="fa fa-id-card"></i>
                                        </label>
                                        <p class="pb-8 text-base font-semibold retro">
                                            @if (auth()->user()->is_company == 'N')
                                                Particular
                                            @else
                                                Empresarial
                                            @endif
                                        </p>

                                    </div>
                                </div>
                                <div class="nk-gap-2"></div>
                                <div class="flex flex-wrap mb-6 -mx-3">
                                    <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                                        <label
                                            class="block mb-2 text-sm font-extrabold tracking-wide text-gray-700 uppercase retro"
                                            for="grid-first-name">
                                            @lang('messages.clasification') <i class="fa fa-line-chart"></i>
                                        </label>
                                        <p class="pb-8 text-base font-semibold retro">
                                            {{ auth()->user()->score[1] }}
                                        </p>

                                    </div>
                                    <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                                        <label
                                            class="block mb-2 text-sm font-extrabold tracking-wide text-gray-700 uppercase retro"
                                            for="grid-first-name">
                                            {{ __('Estado de cuenta:') }} <i class="fa fa-id-card"></i>
                                        </label>
                                        <p class="pb-8 text-xs font-semibold retro">
                                            @if (auth()->user()->is_enabled == 'Y')
                                                <a href="#" class="nes-badge">
                                                    <span class="is-success text-base font-extrabold retro">{{ __('Activo') }}</span>
                                                </a>
                                            @else
                                                <span class="nes-badge is-error text-base font-extrabold retro">{{ __('Inactivo') }}</span>
                                            @endif
                                        </p>

                                    </div>
                                </div>

                                <div class="flex flex-wrap mb-6 -mx-3">
                                    <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                                        <label
                                            class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase retro"
                                            for="grid-first-name">
                                            @lang('messages.stadistic') <i class="fa fa-line-chart"></i>
                                        </label>
                                        <p class="pb-8 text-base font-semibold retro">
                                            Proximamente...
                                        </p>

                                    </div>

                                </div>






                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-2">
                            <div class="col-sm-12" style="background-color: #FFFFFF">
                                <div class="nk-gap"></div>
                                <form action="{{ url('/change/information') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="flex flex-wrap mb-6 -mx-3">
                                        <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">

                                            <label
                                                class="block mb-2 text-base font-extrabold tracking-wide text-gray-700 uppercase retro"
                                                for="grid-first-name">
                                                {{ __('Nombre') }} <i class="fa fa-vcard"></i>
                                            </label>
                                            <input
                                                class="appearance-none  bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white @if ($errors->has('name')) is-invalid @endif"
                                                id="inlineFormInputGroup" value="{{ auth()->user()->first_name }}"
                                                style="color:#000" type="text" name="name">

                                            @if ($errors->has('name'))
                                                <span class="label label-danger">
                                                    @foreach ($errors->get('name') as $error)
                                                        {{ $error }}
                                                    @endforeach
                                                </span>
                                            @endif



                                        </div>
                                        <div class="w-full px-3 md:w-1/2">
                                            <label
                                                class="block mb-2 text-base font-extrabold tracking-wide text-gray-700 uppercase retro"
                                                for="grid-last-name">
                                                {{ __('Apellido') }} <i class="fa fa-vcard"></i>
                                            </label>

                                            <input required type="text" name="lastname"
                                                class="appearance-none  bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white @if ($errors->has('lastname')) is-invalid @endif"
                                                id="inlineFormInputGroup" placeholder="{{ __('Su apellido') }}"
                                                value="{{ auth()->user()->last_name }}">

                                            @if ($errors->has('lastname'))
                                                <span class="label label-danger">
                                                    @foreach ($errors->get('lastname') as $error)
                                                        {{ $error }}
                                                    @endforeach
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="flex flex-wrap mb-6 -mx-3">
                                        <div class="w-full px-3">
                                            <label
                                                class="block mb-2 retro text-base font-extrabold tracking-wide text-gray-700 uppercase"
                                                for="grid-password">
                                                {{ __('Teléfono') }} <i class="fa fa-mobile"></i>
                                            </label>
                                            <input required type="text" name="phone"
                                                class="retro appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 @if ($errors->has('phone')) is-invalid @endif"
                                                id="inlineFormInputGroup" placeholder="{{ __('Teléfono') }}"
                                                value="{{ auth()->user()->phone }}">
                                            @if ($errors->has('phone'))
                                                <span class="label label-danger">
                                                    @foreach ($errors->get('phone') as $error)
                                                        {{ $error }}
                                                    @endforeach
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <br>
                                    <h5 class="block mb-2 text-base font-extrabold tracking-wide text-gray-700 uppercase retro"
                                        for="grid-city">
                                        @lang('messages.direction')
                                    </h5>
                                    <br>
                                    <div class="flex flex-wrap mb-6 -mx-3">
                                        <div class="w-full px-3">
                                            <label
                                                class="block mb-2 text-base font-extrabold tracking-wide text-gray-700 uppercase"
                                                for="grid-password">
                                                {{ __('---') }}
                                            </label>
                                            <input required type="text" name="address"
                                                class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 @if ($errors->has('address')) is-invalid @endif"
                                                id="inlineFormInputGroup" placeholder="{{ __('Su dirección') }}"
                                                value="{{ auth()->user()->address }}">


                                            @if ($errors->has('address'))
                                                <span class="label label-danger">
                                                    @foreach ($errors->get('address') as $error)
                                                        {{ $error }}
                                                    @endforeach
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="flex flex-wrap mb-2 -mx-3">
                                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label
                                                class="block mb-2 text-base font-extrabold tracking-wide text-gray-700 uppercase retro"
                                                for="grid-city">
                                                @lang('messages.postal_code')
                                            </label>
                                            <input required type="text" name="postal"
                                                class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 @if ($errors->has('postal')) is-invalid @endif"
                                                id="inlineFormInputGroup" placeholder="{{ __('Su código postal') }}"
                                                value="{{ auth()->user()->zipcode }}">
                                            @if ($errors->has('postal'))
                                                <span class="label label-danger">
                                                    @foreach ($errors->get('postal') as $error)
                                                        {{ $error }}
                                                    @endforeach
                                                </span>
                                            @endif
                                        </div>
                                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label
                                                class="block mb-2 text-base font-extrabold tracking-wide text-gray-700 uppercase retro"
                                                for="grid-state">
                                                @lang('messages.city_company')
                                            </label>
                                            <div class="relative">
                                                <input required type="text" name="city"
                                                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 @if ($errors->has('city')) is-invalid @endif"
                                                    id="inlineFormInputGroup" placeholder="{{ __('Su ciudad') }}"
                                                    value="{{ auth()->user()->city }}">
                                            </div>
                                            @if ($errors->has('city'))
                                                <span class="label label-danger">
                                                    @foreach ($errors->get('city') as $error)
                                                        {{ $error }}
                                                    @endforeach
                                                </span>
                                            @endif
                                        </div>
                                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label
                                                class="block mb-2 text-base font-extrabold tracking-wide text-gray-700 uppercase retro">
                                                @lang('messages.country')
                                            </label>
                                            <div class="relative">
                                                <select name="country" required=""
                                                    class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500">
                                                    @foreach ($viewData['countries'] as $key)
                                                        <option value="{{ $key->id }}"
                                                            @if (auth()->user()->country_id == $key->id) selected @endif>
                                                            {{ $key->name }}</option>
                                                    @endforeach

                                                </select>
                                                <div
                                                    class="absolute inset-y-0 right-0 flex items-center px-2 text-gray-700 pointer-events-none">
                                                    <svg class="w-4 h-4 fill-current" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 20 20">
                                                        <path
                                                            d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>



                                    </div>
                                    <div class="flex flex-wrap mb-6 -mx-3">
                                        <div class="w-full px-3">
                                            <button type="submit"
                                                class="nk-btn nk-btn-lg nk-btn-rounded nes-btn btn-block">
                                                <font color="black"><span class="text-base font-extrabold retro"><i
                                                            class="fa fa-floppy-o"></i>
                                                        @lang('messages.save')</span></font>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <div class="nk-gap-2"></div>
                                <form action="{{ url('/change/pass') }}" method="post">
                                    {{ csrf_field() }}
                                    <h5 class="block mb-2 text-base font-extrabold tracking-wide text-gray-700 uppercase retro"
                                        for="grid-city">
                                        @lang('messages.password_update')
                                    </h5>
                                    <br>
                                    <div class="flex flex-wrap mb-6 -mx-3">
                                        <div class="w-full px-3">
                                            <label
                                                class="block mb-2 text-base font-extrabold tracking-wide text-gray-700 uppercase retro"
                                                for="grid-password">
                                                @lang('messages.last_password')
                                            </label>
                                            <input required type="password"
                                                class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 @if ($errors->has('lastpass')) is-invalid @endif"
                                                id="inlineFormInputGroup"
                                                placeholder="{{ __('Coloque su ultima contraseña para confirmar su cambio') }}"
                                                name="lastpass">

                                            @if ($errors->has('lastpass'))
                                                <span class="label label-danger">
                                                    @foreach ($errors->get('lastpass') as $error)
                                                        {{ $error }}
                                                    @endforeach
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="flex flex-wrap mb-6 -mx-3">
                                        <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">

                                            <label
                                                class="block mb-2 text-base font-extrabold tracking-wide text-gray-700 uppercase retro"
                                                for="grid-first-name">
                                                @lang('messages.new_password') <i class="fa fa-lock"></i>
                                            </label>
                                            <input required type="password"
                                                class="appearance-none  bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white @if ($errors->has('password')) is-invalid @endif"
                                                id="password" placeholder="@lang('messages.new_password')" name="password">


                                        </div>

                                        <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">

                                            <label
                                                class="block mb-2 text-base font-extrabold tracking-wide text-gray-700 uppercase retro"
                                                for="grid-first-name">
                                                @lang('messages.repeat_password') <i class="fa fa-repeat"></i>
                                            </label>
                                            <input required type="password"
                                                class="appearance-none  bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white @if ($errors->has('password_confirmation')) is-invalid @endif"
                                                id="password_confirmation" placeholder="@lang('messages.repeat_password')"
                                                name="password_confirmation">
                                            <div id="passCheck"></div>
                                            @if ($errors->has('password'))
                                                <span class="label label-danger">
                                                    @foreach ($errors->get('password') as $error)
                                                        {{ $error }}
                                                    @endforeach
                                                </span>
                                            @endif


                                        </div>





                                    </div>
                                    <div class="flex flex-wrap mb-6 -mx-3">
                                        <div class="w-full px-3">
                                            <button type="submit"
                                                class="nk-btn nk-btn-lg nk-btn-rounded nes-btn btn-block">
                                                <font color="black"><span class="text-base font-extrabold retro"><i
                                                            class="fa fa-floppy-o"></i>
                                                        @lang('messages.save')</span></font>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-3">
                            <div class="col-sm-12" style="background-color: #FFFFFF">
                                <div class="nk-gap"></div>
                                @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                    <div class="flex justify-between pb-8 border-b">
                                        <h1 class="text-2xl font-extrabold text-gray-900 retro">
                                            {{ __('Saldo disponible') }} € -
                                            {{ auth()->user()->cash }}</h1>
                                    </div>
                                    <a href="/account/payment" class="nk-btn nk-btn-xs nk-btn-rounded nes-btn">
                                        <font color="black"><i class="fa fa-euro"></i> <span
                                                class="text-xs font-extrabold retro">{{ __('Ingresar dinero') }}</span>
                                        </font>
                                    @else
                                        <div class="flex justify-between pb-8 border-b">
                                            <h1 class="text-2xl font-extrabold text-gray-900 retro">
                                                {{ __('Saldo disponible') }} € -
                                                {{ auth()->user()->cash }}</h1>
                                            <a href="/account/payment" class="nk-btn nk-btn-xs nk-btn-rounded nes-btn">
                                                <font color="black"><i class="fa fa-euro"></i> <span
                                                        class="text-xs font-extrabold retro">{{ __('Ingresar dinero') }}</span>
                                                </font>
                                            </a>
                                        </div>
                                @endif
                                </a>
                                <div class="nk-gap"></div>
                                <form action="{{ route('withdrawals_amount') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="flex flex-wrap mb-6 -mx-3">
                                        <div class="w-full px-3">
                                            <label
                                                class="block mb-2 text-xs font-extrabold tracking-wide text-gray-700 uppercase retro"
                                                for="grid-password">
                                                @lang('messages.remove_cash') €
                                            </label>
                                            <input type="number"
                                                class="block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none retro focus:outline-none focus:bg-white focus:border-gray-500"
                                                step="any" name="credit" min="0" max="{{ Auth::user()->cash }}"
                                                data-number="2" step="0.01" placeholder="0.00" required>
                                            <button type="submit" class="nk-btn nk-btn-xs nk-btn-rounded nes-btn">
                                                <font color="black"><span
                                                        class="text-xs retro font-extrabold">@lang('messages.remove')</span>
                                                </font>
                                            </button>
                                            <span class="text-base font-extrabold retro">Max. <i class="fa fa-euro"></i>
                                                {{ auth()->user()->cash }}</span>
                                        </div>
                                    </div>
                                </form>
                                <div class="nk-gap-2"></div>
                                <h5 class="block mb-2 font-extrabold tracking-wide text-gray-700 uppercase retro">
                                    @lang('messages.bank_account')
                                    <br>
                                </h5>
                                <form action="{{ url('/change/info') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="flex flex-wrap mb-6 -mx-3">
                                        <div class="w-full px-3">
                                            <label
                                                class="block mb-2 text-xs font-extrabold tracking-wide text-gray-700 uppercase retro"
                                                for="grid-password">
                                                {{ __('Nombres & apellidos') }}
                                            </label>
                                            <input type="text" name="beneficiary"
                                                class="retro appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 id="
                                                inlineFormInputGroup" placeholder="{{ __('Your Name') }}"
                                                value="{{ auth()->user()->bank->beneficiary }}">


                                        </div>
                                    </div>
                                    <br>
                                    <div class="flex flex-wrap mb-2 -mx-3">
                                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label
                                                class="block mb-2 text-xs font-extrabold tracking-wide text-gray-700 uppercase retro"
                                                for="grid-city">
                                                IBAN
                                            </label>
                                            <input type="text" name="iban_code"
                                                class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                                id="inlineFormInputGroup" placeholder=""
                                                value="{{ auth()->user()->bank->iban_code }}">
                                        </div>
                                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label
                                                class="block mb-2 text-xs font-extrabold tracking-wide text-gray-700 uppercase retro"
                                                for="grid-state">
                                                BIC
                                            </label>
                                            <div class="relative">
                                                <input type="text" name="bic_code"
                                                    class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                                    id="inlineFormInputGroup" placeholder=""
                                                    value="{{ auth()->user()->bank->bic_code }} ">

                                            </div>

                                        </div>
                                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label
                                                class="block mb-2 text-xs font-extrabold tracking-wide text-gray-700 uppercase retro"
                                                for="grid-state">
                                                @lang('messages.bank_direction')
                                            </label>
                                            <div class="relative">
                                                <input type="text" name="bank_address"
                                                    class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                                    id="inlineFormInputGroup" placeholder="@lang('messages.bank_direction')"
                                                    value="{{ auth()->user()->bank->bank_address }}">


                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex flex-wrap mb-6 -mx-3">
                                        <div class="w-full px-3">
                                            <button type="submit"
                                                class="nk-btn nk-btn-lg nk-btn-rounded nes-btn btn-block">
                                                <font color="black"><span class="text-base font-extrabold retro"><i
                                                            class="fa fa-floppy-o"></i>
                                                        @lang('messages.save')</span></font>
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>



                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-5">
                            <div class="col-sm-12" style="background-color: #FFFFFF">
                                <div class="nk-gap"></div>
                                <form action="{{ route('social_store', ['id' => auth()->user()->id]) }}" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="flex flex-wrap mb-6 -mx-3">
                                        <div class="w-full px-3">
                                            <label
                                                class="block mb-2 font-extrabold tracking-wide text-center text-gray-700 uppercase retro"
                                                for="grid-state">
                                                @lang('messages.term_and_conditions_three')
                                            </label>
                                            <textarea name="terms_and_conditions"
                                                placeholder="@lang('messages.term_and_conditions_message')"
                                                class="nes-textarea" rows="3" maxlength="350" required></textarea>
                                            <div class="textarea-counter" data-text-max="350"
                                                data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></div>



                                        </div>

                                    </div>

                                    <div class="flex flex-wrap mb-6 -mx-3">
                                        <div class="w-full px-3">
                                            <button type="submit"
                                                class="nk-btn nk-btn-lg nk-btn-rounded nes-btn btn-block">
                                                <font color="black"><span class="text-base font-extrabold retro"><i
                                                            class="fa fa-floppy-o"></i>
                                                        @lang('messages.save')</span></font>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <br>
                                @if (count($social_user) == 0)
                                    <form action="{{ route('social_store', ['id' => auth()->user()->id]) }}"
                                        method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <h5
                                            class="block mb-2 font-extrabold tracking-wide text-center text-gray-700 uppercase retro">
                                            @lang('messages.social')
                                        </h5>

                                        <div class="flex flex-wrap mb-2 -mx-3">

                                            <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                                <label
                                                    class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                                    for="grid-city">
                                                    <i class="nes-icon youtube is-medium"></i>
                                                </label>


                                                <input
                                                    class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                                    name="youtube" type="text" placeholder="https://www.youtube.com">

                                            </div>
                                            <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                                <label>
                                                    <img style="width: 45px;" src="{{ asset('img/discord.png') }}">
                                                </label>
                                                <input
                                                    class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                                    name="discord" type="text" placeholder="https://discord.com">
                                            </div>
                                            <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                                <label
                                                    class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                                    for="grid-zip">
                                                    <i class="nes-icon instagram is-medium"></i>
                                                </label>
                                                <input
                                                    class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                                    name="instagram" type="text" placeholder="https://www.instagram.com">
                                            </div>
                                        </div>
                                        <div class="flex flex-wrap mb-6 -mx-3">
                                            <div class="w-full px-3">
                                                <button type="submit"
                                                    class="nk-btn nk-btn-lg nk-btn-rounded nes-btn btn-block">
                                                    <font color="black"><span class="text-base font-extrabold retro"><i
                                                                class="fa fa-floppy-o"></i>
                                                            @lang('messages.save')</span></font>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                @else
                                    @foreach ($social_user as $d)
                                        <form action="{{ route('social_network_update', ['id' => auth()->user()->id]) }}"
                                            method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <h5
                                                class="block mb-2 font-bold tracking-wide text-center text-gray-700 uppercase retro">
                                                @lang('messages.social')
                                            </h5>

                                            <div class="flex flex-wrap mb-2 -mx-3">

                                                <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                                    <label
                                                        class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                                        for="grid-city">
                                                        <i class="nes-icon youtube is-medium"></i>
                                                    </label>


                                                    <input
                                                        class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                                        name="youtube" type="text" value="{{ $d->youtube }}"
                                                        placeholder="https://www.youtube.com">

                                                </div>
                                                <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                                    <label>
                                                        <img style="width: 45px;" src="{{ asset('img/discord.png') }}">
                                                    </label>
                                                    <input
                                                        class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                                        name="discord" type="text" value="{{ $d->discord }}"
                                                        placeholder="https://discord.com">
                                                </div>
                                                <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                                    <label
                                                        class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                                        for="grid-zip">
                                                        <i class="nes-icon instagram is-medium"></i>
                                                    </label>
                                                    <input
                                                        class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                                        name="instagram" value="{{ $d->instagram }}" type="text"
                                                        placeholder="https://www.instagram.com">
                                                </div>
                                            </div>
                                            <div class="flex flex-wrap mb-6 -mx-3">
                                                <div class="w-full px-3">
                                                    <button type="submit"
                                                        class="nk-btn nk-btn-lg nk-btn-rounded nes-btn btn-block">
                                                        <font color="black"><span class="text-base font-extrabold retro"><i
                                                                    class="fa fa-floppy-o"></i>
                                                                @lang('messages.save')</span></font>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    @endforeach
                                @endif


                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-4">
                            <div class="flex my-12 shadow-md">
                                <div class="w-full px-12 py-12 bg-white">
                                    <div class="flex justify-between pb-8 border-b">
                                        <h1 class="text-2xl font-extrabold text-gray-900 retro">
                                            @lang('messages.my_refered')
                                        </h1>

                                    </div>
                                    <div class="nk-gap"></div>
                                    @foreach (Auth::user()->referidos as $referido)
                                        <a href="{{ route('user-info', $referido->user_name) }}" target="_blank">
                                            @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                                <span class="text-lg font-semibold retro">
                                                @else
                                                    <span class="text-lg font-semibold retro">
                                            @endif

                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                            {{ $referido->user_name . ' | ' . $referido->first_name . ' ' . $referido->last_name }}

                                            </span>
                                        </a>
                                        <div class="nk-gap"></div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                    </div>



                </div>
            </div>
            <!-- END: Tabs -->
        </div>
    </div>
    </div>

    <div class="nk-gap-2"></div>



@endsection

@section('content-script-include')
    <script src="{{ asset('plugins/fileinput/js/fileinput.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                overwriteInitial: true,
                @if (isset(auth()->user()->profile_picture))
                    initialPreview: [
                    // IMAGE DATA
                    '<img src="{{ url(auth()->user()->profile_picture) }}" class="kv-preview-data file-preview-image">',
                    ],
                @endif
            });

            $("#input-19").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                maxFileCount: 1,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                overwriteInitial: true,
                @if (isset(auth()->user()->cover_picture))
                    initialPreview: [
                    // IMAGE DATA
                    '<img src="{{ url(auth()->user()->cover_picture) }}" class="kv-preview-data file-preview-image">',
                    ],
                @endif
            });
            $(".fe").datepicker();
        });
    </script>
@endsection

@section('content-script')
    <script>
        $("#password").keyup(function() {
            if ($("#password_confirmation").val() != '') {
                if ($("#password").val().localeCompare($("#password_confirmation").val()) != 0) {
                    $('#passCheck').html('<div  class="alert alert-danger">The passwords does not match.</div >');
                } else {
                    $('#passCheck').html('');
                }
            }
        });
        $("#password_confirmation").keyup(function() {
            if ($("#password").val() != '') {
                if ($("#password").val().localeCompare($("#password_confirmation").val()) != 0) {
                    $('#passCheck').html('<div  class="alert alert-danger">The passwords does not match.</div >');
                } else {
                    $('#passCheck').html('');
                }
            }
        });
        var hash = window.location.hash;
        if (hash == '#available') {
            $('a[href*="#tabs-1-2"]').click();
        }
    </script>

    <script>
        $(document).ready(function() {
            @if ($errors->has('beneficiary'))
                $('a[href*="#tabs-1-3"]').click();
            @elseif($errors->has('iban_code'))
                $('a[href*="#tabs-1-3"]').click();
            @elseif($errors->has('bic_code'))
                $('a[href*="#tabs-1-3"]').click();
            @elseif($errors->has('bank_address'))
                $('a[href*="#tabs-1-3"]').click();
            @endif

            if ('<?= session('
                                                                                                                                                                                                                tab ') ?>' ==
                'fail') {
                $('a[href*="#tabs-1-3"]').click();
            }

            $('.textarea-counter').each(function() {
                var textarea = $(this).prev();
                var textCounter = $(this);
                var text = $(this).data('counter-text');
                var textMax = $(this).data('text-max');
                text.replace('_TOTAL_', textMax);

                textCounter.text(text.replace('_TOTAL_', textMax).replace('_QTY_', textarea.val().length));

                textarea.keypress(function(e) {
                    if (this.value.length >= parseInt(textMax)) {
                        e.preventDefault();
                        textCounter.text(text.replace('_TOTAL_', textMax).replace('_QTY_', this
                            .value.length));
                    } else {
                        textCounter.text(text.replace('_TOTAL_', textMax).replace('_QTY_', this
                            .value.length));
                    }
                });

                textarea.keyup(function(e) {
                    this.value = this.value.substr(0, parseInt(textMax));
                    textCounter.text(text.replace('_TOTAL_', textMax).replace('_QTY_', this.value
                        .length));
                });
            });
        });
    </script>
@endsection
