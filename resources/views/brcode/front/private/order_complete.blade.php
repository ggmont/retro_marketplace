@extends('brcode.front.layout.app')
@section('content-css-include')

@endsection
@section('content')
<!-- Content Header (Page header) -->
<div class="nk-gap-3"></div>
<!-- Main content -->
<section  class="content">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h3>{{ __('Buenas noticias!')  }}</h3>
    </div>
  </div><!-- /.row -->
  @if( ! $viewData['error'] )
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12" >
      <span class="cart-empty">{{ __('Su orden ha sido procesada satisfactoriamente. Para ver el detalle de su compra, ingrese al panel de administración de su cuenta')}}</span>
    </div><!-- /.col-@-10 -->
  </div><!-- /.row -->
  @else
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12" >
      <span class="cart-empty">{{ __('Ocurrió un error durante el proceso de su pago. Inténtelo nuevamente') }}</span>
    </div><!-- /.col-@-10 -->
  </div><!-- /.row -->
  @endif
</div><!-- /.container -->
</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="{{ asset('brcode/js/pty.components.js') }}"></script>
@endsection
@section('content-script')
<script>

$(document).ready(function() {

});
</script>
@endsection
