<div class="modal fade nes-dialog is-rounded" id="NewMessage" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="bg-white modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="ion-android-close"></span>
                </button>

                <h4 class="mb-0 retro"><span class="text-main-1">@lang('messages.advance_filter') </span>
                    <font color="black">@lang('messages.msg'): <b>
                        </b></font>
                </h4>

                <div class="nk-gap-1"></div>
                <form class="nk-form" method="POST" action="{{ route('newMessageChat') }} ">
                    {{ csrf_field() }}
                    <div class="row vertical-gap">
                        <div class="col-md-12">
                            
                            <select name="name" id='selUser' style='width: 200px;'>
                                <option value='0'>-- @lang('messages.search_chat') --</option>
                              </select>
                    
                        </div>

                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <textarea style="width:100%" class="nes-textarea" name="content" cols="30" rows="5"
                                autocomplete="off" required placeholder="@lang('messages.write_msg'):..."></textarea>
                        </div>
                    </div>
                    <div class="nk-gap-2"></div>
                    <div class="row ">
                        <div class="col-md-12">
                            <button class="float-left nk-btn-xs nk-btn-rounded nes-btn is-warning"
                                data-dismiss="modal"><span class="retro">@lang('messages.cancel')</span></button>
                            <button class="float-right nk-btn-xs nk-btn-rounded nes-btn is-error"
                                type="submit"><span class="retro">@lang('messages.sends')</span></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>