<div class="modal modal-primary fade" id="edit{{ $viewData['order_header']->order_identification }}" tabindex="-1"
    role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="bg-white modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            </div>
            <form
                action="{{ url('/account/purchases/view/' . $viewData['order_header']->order_identification . '/score') }}"
                method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-body">
                    <div class="flex flex-wrap mb-2 -mx-3">
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                            <label
                                class="block mb-2 text-xs font-bold tracking-wide text-center text-gray-700 uppercase retro"
                                for="grid-state">
                                @lang('messages.speed_rating')
                            </label>
                            <br>
                            <div class="relative text-xs retro small row">

                                <label class="text-green-900">
                                    <input required type="radio" class="nes-radio" name="optionsProcess"
                                        id="optionsProcess1" value="1"
                                        {{ 1 == $viewData['rating']->processig ? 'checked="checked"' : '' }} />
                                    <span>{{ __('Bueno') }}</span>
                                </label>

                                <label class="text-yellow-900">
                                    <input type="radio" class="nes-radio" name="optionsProcess"
                                        id="optionsProcess2" value="2"
                                        {{ 2 == $viewData['rating']->processig ? 'checked="checked"' : '' }} />
                                    <span>{{ __('Neutral') }}</span>
                                </label>

                                <label class="text-red-900">
                                    <input type="radio" class="nes-radio" name="optionsProcess"
                                        id="optionsProcess3" value="3"
                                        {{ 3 == $viewData['rating']->processig ? 'checked="checked"' : '' }} />
                                    <span>{{ __('Malo') }}</span>
                                </label>

                            </div>
                        </div>
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                            <label
                                class="block mb-2 text-xs font-bold tracking-wide text-center text-gray-700 uppercase retro"
                                for="grid-state">
                                @lang('messages.package_seller') 
                            </label>
                            <br>
                            <div class="relative text-xs retro small row">

                                <label class="text-green-900">
                                    <input required type="radio" class="nes-radio" name="optionsPackage"
                                        class="optionsPackage" id="optionsPackage1" value="1"
                                        {{ 1 == $viewData['rating']->packaging ? 'checked="checked"' : '' }} />
                                    <span>{{ __('Bueno') }}</span>
                                </label>

                                <label class="text-yellow-900">
                                    <input type="radio" class="nes-radio" name="optionsPackage"
                                        class="optionsPackage" id="optionsPackage2" value="2"
                                        {{ 2 == $viewData['rating']->packaging ? 'checked="checked"' : '' }} />
                                    <span>{{ __('Neutral') }}</span>
                                </label>

                                <label class="text-red-900">
                                    <input type="radio" class="nes-radio" name="optionsPackage"
                                        class="optionsPackage" id="optionsPackage3" value="3"
                                        {{ 3 == $viewData['rating']->packaging ? 'checked="checked"' : '' }} />
                                    <span>{{ __('Malo') }}</span>
                                </label>

                            </div>


                        </div>
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                            <label
                                class="block mb-2 text-xs font-bold tracking-wide text-center text-gray-700 uppercase retro"
                                for="grid-state">
                                @lang('messages.product_description')  
                            </label>
                            <br>
                            <div class="relative text-xs retro small row">
                                <label class="text-green-900">
                                    <input required type="radio" class="nes-radio" name="optionsDescription"
                                        class="optionsDescription" id="optionsDescription1" value="1"
                                        {{ 1 == $viewData['rating']->desc_prod ? 'checked="checked"' : '' }} />
                                    <span>{{ __('Bueno') }}</span>
                                </label>

                                <label class="text-yellow-900">
                                    <input type="radio" class="nes-radio" name="optionsDescription"
                                        class="optionsDescription" id="optionsDescription2" value="2"
                                        {{ 2 == $viewData['rating']->desc_prod ? 'checked="checked"' : '' }} />
                                    <span>{{ __('Neutral') }}</span>
                                </label>

                                <label class="text-red-900">
                                    <input type="radio" class="nes-radio" name="optionsDescription"
                                        class="optionsDescription" id="optionsDescription3" value="3"
                                        {{ 3 == $viewData['rating']->desc_prod ? 'checked="checked"' : '' }} />
                                    <span>Malo</span>
                                </label>

                            </div>
                        </div>
                    </div>
                    <label class="block mb-2 text-xs font-bold tracking-wide text-center text-gray-700 uppercase retro"
                        for="grid-state">
                        Comentario
                    </label>

                    <textarea name="description" class="nes-textarea" rows="3" maxlength="350"
                        required>{{ $viewData['rating']->description }}</textarea>
                    <div class="textarea-counter" data-text-max="350"
                        data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></div>
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="submit"
                                class="nk-btn nk-btn-md nk-btn-rounded nes-btn is-primary btn-block">{{ __('Guardar') }}</button>
                        </div>
                    </div>
                </div>


            </form>
        </div>
    </div>
</div>
