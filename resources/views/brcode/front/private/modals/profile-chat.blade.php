<div class="modal fade" id="profile"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog reem-soli-form" role="document">
        <div class="modal-content bg-white">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <h4 class="modal-title">@lang('messages.reem_soli')</h4>
                        <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                            aria-label="Close"></button>
                    </div>

                    <div class="card comment-box">
                        <img src="{{ asset('img/profile-picture-not-found.png') }}" alt="avatar"
                            class="imaged w64 rounded">
                        <h4 class="card-title">{{ $usuario->user_name }}</h4>
                        <div class="card-text">{{ date_format($usuario->created_at, 'd-m-y') }}</div>
                        <div class="text">
                            {{ $usuario->terms_and_conditions ? $usuario->terms_and_conditions : 'Este usuario no tiene de momento terminos y condiciones' }}
                        </div>
                        <div class="wide-block pt-2 pb-2">
    
                            <div class="row">
                                <div class="col">
                                    <div class="exampleBox"> <img src="{{ asset('img/new.png') }}" width="18px"
                                            data-toggle="popover" data-content="@lang('messages.good')" data-placement="top"
                                            data-trigger="hover">
                                        <p class="text-sm font-extrabold text-gray-800 retro">
                                            {{ number_format($viewData['sc1'] * 100, 2) }} %
                                        </p>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="exampleBox"> <img src="{{ asset('img/used.png') }}" width="18px"
                                            data-toggle="popover" data-content="@lang('messages.good')" data-placement="top"
                                            data-trigger="hover">
                                        <p class="text-sn font-extrabold text-gray-800 retro">
                                            {{ number_format($viewData['sc2'] * 100, 2) }} %
                                        </p>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="exampleBox"> <img src="{{ asset('img/no-funciona.png') }}" width="18px"
                                            data-toggle="popover" data-content="@lang('messages.good')" data-placement="top"
                                            data-trigger="hover">
                                        <p class="text-sm font-extrabold text-gray-800 retro">
                                            {{ number_format($viewData['sc3'] * 100, 2) }} %
                                        </p>
                                    </div>
                                </div>
                            </div>
    
                        </div>

                        <div class="wide-block pb-1 pt-2">
                            <span class="font-semibold text-sm">
    
                                <div class="row">
                                    <div class="col-6">
    
                                        <a href="{{ route('user-info', $usuario->user_name) }}"
                                            class="bg-blue-700 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                                            <span class="retro text-white font-extrabold">VER PERFIL</span>
                                        </a>
                                    </div>
                                    <div class="col-6">
    
                                        <a id="showComplaint" data-dismiss="modal"
                                            class="showComplaint bg-red-800 hover:bg-red-700 text-white font-bold py-2 px-4 border border-red-700 rounded">
                                            <span class="retro text-white font-extrabold">REPORTAR</span>
                                        </a>
    
                                    </div>
                                </div>
                            </span>
    
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
