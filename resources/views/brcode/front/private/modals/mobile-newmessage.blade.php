<div class="add-new-contact-modal modal fade px-0" id="conversationplus" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="bg-white modal-content">
            <div class="modal-body">
                <div class="d-flex align-items-center justify-content-between mb-4">
                    <h6 class="modal-title" id="addnewcontactlabel">@lang('messages.msg')</h6>
                    <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                        aria-label="Close"></button>
                </div>
                <form class="nk-form" method="POST" action="{{ route('newMessageChat') }} ">
                    {{ csrf_field() }}
                    <div class="row vertical-gap">
                        <div class="col-md-12">

                            <select name="name" id='selUser' style='width: 200px;'>
                                <option value='0'>-- @lang('messages.search_chat') --</option>
                            </select>

                        </div>

                    </div>
                    <br>
                    <div class="form-group">
                        <label class="form-label" for="message">Mensaje</label>
                        <textarea class="form-control" id="message" name="content" cols="3" rows="5"
                            placeholder="@lang('messages.write_msg')..."></textarea>
                    </div>



                    <button class="btn btn-primary w-100 font-bold" type="submit">Enviar</button>


                </form>
            </div>
        </div>
    </div>
</div>
