@extends('brcode.front.layout.app_config')
@section('content-css-include')
    <style>

    </style>
@endsection

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div class="page-content-wrapper py-3">
            <div class="container">
                <!-- User Information-->
                <div class="listview-title">@lang('messages.information')</div>
                <ul class="listview link-listview">
                    <li>
                        <a href="{{ route('config_personal_data') }}">
                            @lang('messages.personal_data')
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('referrals_index') }}">
                            @lang('messages.refered')
                            <span class="badge badge-danger">{{ count(auth()->user()->referidos) }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('public_profile_data') }}">
                            @lang('messages.information_public')
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    @else
    @endif
@endsection

@section('content-script-include')
    <!-- Flickity -->
    <script src="{{ asset('assets/vendor/flickity/dist/flickity.pkgd.min.js') }}"></script>

    <!-- Hammer.js -->
    <script src="{{ asset('assets/vendor/hammerjs/hammer.min.js') }}"></script>
@endsection
@section('content-script')
    <script>
        $(document).ready(function() {
            $('.comments').popover();
            $('[data-toggle="popover"]').popover();
        });
    </script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#selUser").select2({
                minimumInputLength: 3,
                width: '100%',

                ajax: {
                    url: "{{ route('newUserMessage') }}",
                    type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            _token: CSRF_TOKEN,
                            search: params.term // search term
                        };
                    },
                    processResults: function(response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }

            });

        });
    </script>

    <script>
        $("#men").click(function() {
            $('.single-message').hide();
        });
    </script>

    <script>
        Livewire.on('alert', function() {
            Swal.fire(
                'Listo!',
                '<span class="retro">Tu Mensaje fue eliminado</span>',
                'success'
            )
        })
    </script>
@endsection
