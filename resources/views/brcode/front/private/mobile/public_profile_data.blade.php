@extends('brcode.front.layout.app_config')
@section('content-css-include')
    <style>

    </style>
@endsection

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        @livewire('mobile.config-public')
    @else
    @endif
@endsection

@section('content-script-include')
    <!-- Flickity -->
    <script src="{{ asset('assets/vendor/flickity/dist/flickity.pkgd.min.js') }}"></script>

    <!-- Hammer.js -->
    <script src="{{ asset('assets/vendor/hammerjs/hammer.min.js') }}"></script>
@endsection
@section('content-script')
    @livewireScripts
    <script>
        window.addEventListener('show-form-ultimate', event => {
            document.getElementById("actualizar").click();
        })
    </script>
    <script>
        window.addEventListener('show-form-errorgm', event => {
            document.getElementById("errorgm").click();
        })
    </script>
    <script>
        window.addEventListener('show-form-terms', event => {
            $('#termModal').modal('show');
        })

        window.addEventListener('hide-form-terms', event => {
            $('#termModal').modal('hide');
        })
    </script>
    <script>
        window.addEventListener('show-form-cover_picture', event => {
            $('#photoModal').modal('show');
        })

        window.addEventListener('hide-form-cover_picture', event => {
            $('#photoModal').modal('hide');
        })
    </script>
    <script>
        window.addEventListener('show-form-profile_picture', event => {
            $('#profilesModal').modal('show');
        })

        window.addEventListener('hide-form-profile_picture', event => {
            $('#profilesModal').modal('hide');
        })
    </script>
    <script>
        window.addEventListener('show-form-facebook', event => {
            $('#facebookModal').modal('show');
        })

        window.addEventListener('hide-form-facebook', event => {
            $('#facebookModal').modal('hide');
        })
    </script>
    <script>
        window.addEventListener('show-form-twitch', event => {
            $('#twitchModal').modal('show');
        })

        window.addEventListener('hide-form-twitch', event => {
            $('#twitchModal').modal('hide');
        })
    </script>
    <script>
        window.addEventListener('show-form-new-twitch', event => {
            $('#NewtwitchModal').modal('show');
        })

        window.addEventListener('hide-form-new-twitch', event => {
            $('#NewtwitchModal').modal('hide');
        })
    </script>
    <script>
        window.addEventListener('show-form-new-youtube', event => {
            $('#NewyoutubeModal').modal('show');
        })

        window.addEventListener('hide-form-new-youtube', event => {
            $('#addEventListener').modal('hide');
        })
    </script>
    <script>
        window.addEventListener('show-form-new-tiktok', event => {
            $('#NewtiktokModal').modal('show');
        })

        window.addEventListener('hide-form-new-tiktok', event => {
            $('#addEventListener').modal('hide');
        })
    </script>
    <script>
        window.addEventListener('show-form-new-instagram', event => {
            $('#NewtiktokModal').modal('show');
        })

        window.addEventListener('hide-form-new-instagram', event => {
            $('#addEventListener').modal('hide');
        })
    </script>
    <script>
        window.addEventListener('show-form-youtube', event => {
            $('#youtubeModal').modal('show');
        })

        window.addEventListener('hide-form-youtube', event => {
            $('#youtubeModal').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-tiktok', event => {
            $('#tiktokModal').modal('show');
        })

        window.addEventListener('hide-form-tiktok', event => {
            $('#tiktokModal').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-discord', event => {
            $('#discordModal').modal('show');
        })

        window.addEventListener('hide-form-discord', event => {
            $('#discordModal').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-instagram', event => {
            $('#instagramModal').modal('show');
        })

        window.addEventListener('hide-form-instagram', event => {
            $('#instagramModal').modal('hide');
        })
    </script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#selUser").select2({
                minimumInputLength: 3,
                width: '100%',

                ajax: {
                    url: "{{ route('newUserMessage') }}",
                    type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            _token: CSRF_TOKEN,
                            search: params.term // search term
                        };
                    },
                    processResults: function(response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }

            });

        });
    </script>

    <script>
        $("#men").click(function() {
            $('.single-message').hide();
        });
    </script>

    <script>
        Livewire.on('alert', function() {
            Swal.fire(
                'Listo!',
                '<span class="retro">Tu Mensaje fue eliminado</span>',
                'success'
            )
        })
    </script>
@endsection
