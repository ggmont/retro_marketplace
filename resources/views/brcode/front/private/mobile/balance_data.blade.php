@extends('brcode.front.layout.app_config')
@section('content-css-include')
    <style>

    </style>
@endsection

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
    @include('partials.flash')
        @livewire('mobile.balance-personal')
    @else
    @endif
@endsection

@section('content-script-include')
    <!-- Flickity -->
    <script src="{{ asset('assets/vendor/flickity/dist/flickity.pkgd.min.js') }}"></script>

    <!-- Hammer.js -->
    <script src="{{ asset('assets/vendor/hammerjs/hammer.min.js') }}"></script>
@endsection
@section('content-script')
    @livewireScripts
    <script>
        window.addEventListener('show-form-ultimate', event => {
            document.getElementById("actualizar").click();
        })
    </script>
    <script>
        window.addEventListener('show-form-errorgm', event => {
            document.getElementById("errorgm").click();
        })
    </script>
    <script>
        window.addEventListener('show-form-bank', event => {
            $('#bankModal').modal('show');
        })

        window.addEventListener('hide-form-bank', event => {
            $('#bankModal').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-iban', event => {
            $('#ibanModal').modal('show');
        })

        window.addEventListener('hide-form-iban', event => {
            $('#ibanModal').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-bic_code', event => {
            $('#bicModal').modal('show');
        })

        window.addEventListener('hide-form-bic_code', event => {
            $('#bicModal').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-cash', event => {
            $('#cashModal').modal('show');
        })

        window.addEventListener('hide-form-cash', event => {
            $('#cashModal').modal('hide');
        })
    </script>


    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#selUser").select2({
                minimumInputLength: 3,
                width: '100%',

                ajax: {
                    url: "{{ route('newUserMessage') }}",
                    type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            _token: CSRF_TOKEN,
                            search: params.term // search term
                        };
                    },
                    processResults: function(response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }

            });

        });
    </script>

    <script>
        $("#men").click(function() {
            $('.single-message').hide();
        });
    </script>

    <script>
        Livewire.on('alert', function() {
            Swal.fire(
                'Listo!',
                '<span class="retro">Tu Mensaje fue eliminado</span>',
                'success'
            )
        })
    </script>
@endsection
