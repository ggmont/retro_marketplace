@extends('brcode.front.layout.app_config')
@section('content-css-include')
    <style>

    </style>
@endsection

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div class="page-content-wrapper py-3">
            <div class="container">
                @include('partials.flash')
                <button type="button" id="actualizar" style="display:none" class="btn btn-secondary hidden me-05 mb-1"
                    onclick="notification('notification-6' , 3000)">Auto Close (3s)</button>
                <button type="button" id="errorgm" style="display:none" class="btn btn-secondary hidden me-05 mb-1"
                    onclick="notification('notification-7' , 3000)">Auto Close (3s)</button>
                <!-- User Information-->
                <center><span class="retro-sell font-extrabold">- No Pagados -</span></center>
                
                @if ($unpaid > 0)
                @livewire('mobile.purchase.unpaid-purchase')
                @else
                <div class="page-content-wrapper">
                    <div class="container">
                        <div class="nk-gap-3"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <blockquote class="nk-blockquote">
                                    <div class="nk-blockquote-icon"></div>
                                    <div class="text-center nk-blockquote-content h3 font-extrabold">
                                        <font color="black">
                                            @lang('messages.not_found_registers')
                                        </font>
                                    </div>
                                    <div class="nk-gap"></div>
                                    <div class="nk-blockquote-author"></div>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                @endif



            </div>

        </div>
    @else
    @endif
@endsection

@section('content-script-include')
    <!-- Flickity -->
    <script src="{{ asset('assets/vendor/flickity/dist/flickity.pkgd.min.js') }}"></script>

    <!-- Hammer.js -->
    <script src="{{ asset('assets/vendor/hammerjs/hammer.min.js') }}"></script>
@endsection
@section('content-script')
    @livewireScripts
    <script>
        window.addEventListener('show-form-ultimate', event => {
            document.getElementById("actualizar").click();
        })
    </script>
    <script>
        window.addEventListener('show-form-errorgm', event => {
            document.getElementById("errorgm").click();
        })
    </script>
    <script>
        window.addEventListener('show-form', event => {
            $('#exampleModal').modal('show');
        })

        window.addEventListener('hide-form', event => {
            $('#exampleModal').modal('hide');
        })
    </script>
    <script>
        window.addEventListener('show-form-name', event => {
            $('#ModalName').modal('show');
        })

        window.addEventListener('hide-form-name', event => {
            $('#ModalName').modal('hide');
        })
    </script>
    <script>
        window.addEventListener('show-form-pass', event => {
            $('#ModalPass').modal('show');
        })

        window.addEventListener('hide-form-pass', event => {
            $('#ModalPass').modal('hide');
        })
    </script>
    <script>
        window.addEventListener('show-form-phone', event => {
            $('#ModalPhone').modal('show');
        })

        window.addEventListener('hide-form-phone', event => {
            $('#ModalPhone').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-address', event => {
            $('#ModalAddress').modal('show');
        })

        window.addEventListener('hide-form-address', event => {
            $('#ModalAddress').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-zipcode', event => {
            $('#ModalZipCode').modal('show');
        })

        window.addEventListener('hide-form-zipcode', event => {
            $('#ModalZipCode').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-city', event => {
            $('#ModalCity').modal('show');
        })

        window.addEventListener('hide-form-city', event => {
            $('#ModalCity').modal('hide');
        })
    </script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#selUser").select2({
                minimumInputLength: 3,
                width: '100%',

                ajax: {
                    url: "{{ route('newUserMessage') }}",
                    type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            _token: CSRF_TOKEN,
                            search: params.term // search term
                        };
                    },
                    processResults: function(response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }

            });

        });
    </script>

    <script>
        $("#men").click(function() {
            $('.single-message').hide();
        });
    </script>

    <script>
        Livewire.on('alert', function() {
            Swal.fire(
                'Listo!',
                '<span class="retro">Tu Mensaje fue eliminado</span>',
                'success'
            )
        })
    </script>
@endsection
