@extends('brcode.front.layout.app_config')
@section('content-css-include')
    <style>

    </style>
@endsection

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div class="header-area" id="headerArea">
            <div class="container h-100 d-flex align-items-center justify-content-between">
                <!-- Back Button-->
                <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                <!-- Page Title-->
                <div class="page-heading">
                    <h6 class="mb-0 font-extrabold">Compras</h6>
                </div>
                <!-- Navbar Toggler-->

                @if (Auth::user())
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @else
                    <a href="#" id="showLogin" class="showLogin" class="lni lni-user d-flex flex-wrap"
                        data-nav-toggle="#nk-nav-mobile">
                        <span class="lni lni-user">
                            <span></span><span></span><span></span>
                        </span>
                    </a>
                @endif
            </div>
        </div>

        <div class="page-content-wrapper py-3">
            <div class="container">
                <button type="button" id="actualizar" style="display:none" class="btn btn-secondary hidden me-05 mb-1"
                    onclick="notification('notification-6' , 3000)">Auto Close (3s)</button>
                <button type="button" id="errorgm" style="display:none" class="btn btn-secondary hidden me-05 mb-1"
                    onclick="notification('notification-7' , 3000)">Auto Close (3s)</button>
                <!-- User Information-->

                <ul class="nav nav-tabs capsuled" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-bs-toggle="tab" href="#national" role="tab"
                            aria-selected="true">
                            Nacionales
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#international" role="tab" aria-selected="false">
                            Internacionales
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade show active" id="national" role="tabpanel">
                        <ul class="listview link-listview">
                            <li>
                                <a href="{{ route('reserves_index') }}">
                                    @lang('messages.not_paid')
                                    <span class="badge badge-danger"> {{ $unpaid_national }} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('delivered_index') }}">
                                    @lang('messages.send_sale_order_s')
                                    <span class="badge badge-danger"> {{ $arrived_national }} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('cancel_index') }}">
                                    @lang('messages.the_cancel')
                                    <span class="badge badge-danger"> {{ $cancelled_national }} </span>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-pane fade" id="international" role="tabpanel">
                        <ul class="listview link-listview">
                            <li>
                                <a href="{{ route('reserves_international_index') }}">
                                    @lang('messages.not_paid')
                                    <span class="badge badge-danger"> {{ $unpaid_international }} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('paid_index') }}">
                                    @lang('messages.the_paid')
                                    <span class="badge badge-danger"> {{ $paid_international }} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('send_index') }}">
                                    @lang('messages.send_wait')
                                    <span class="badge badge-danger"> {{ $sent_international }} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('delivered_international_index') }}">
                                    @lang('messages.send_sale_order_s')
                                    <span class="badge badge-danger"> {{ $arrived_international }} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('not_delivered_index') }}">
                                    @lang('messages.undelivered')
                                    <span class="badge badge-danger"> {{ $notArrived_international }} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('cancel_international_index') }}">
                                    @lang('messages.the_cancel')
                                    <span class="badge badge-danger"> {{ $cancelled_international }} </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>



            </div>

        </div>
    @else
    @endif
@endsection

@section('content-script-include')
    <!-- Flickity -->
    <script src="{{ asset('assets/vendor/flickity/dist/flickity.pkgd.min.js') }}"></script>

    <!-- Hammer.js -->
    <script src="{{ asset('assets/vendor/hammerjs/hammer.min.js') }}"></script>
@endsection
@section('content-script')
    @livewireScripts
    <script>
        window.addEventListener('show-form-ultimate', event => {
            document.getElementById("actualizar").click();
        })
    </script>
    <script>
        window.addEventListener('show-form-errorgm', event => {
            document.getElementById("errorgm").click();
        })
    </script>
    <script>
        window.addEventListener('show-form', event => {
            $('#exampleModal').modal('show');
        })

        window.addEventListener('hide-form', event => {
            $('#exampleModal').modal('hide');
        })
    </script>
    <script>
        window.addEventListener('show-form-name', event => {
            $('#ModalName').modal('show');
        })

        window.addEventListener('hide-form-name', event => {
            $('#ModalName').modal('hide');
        })
    </script>
    <script>
        window.addEventListener('show-form-pass', event => {
            $('#ModalPass').modal('show');
        })

        window.addEventListener('hide-form-pass', event => {
            $('#ModalPass').modal('hide');
        })
    </script>
    <script>
        window.addEventListener('show-form-phone', event => {
            $('#ModalPhone').modal('show');
        })

        window.addEventListener('hide-form-phone', event => {
            $('#ModalPhone').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-address', event => {
            $('#ModalAddress').modal('show');
        })

        window.addEventListener('hide-form-address', event => {
            $('#ModalAddress').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-zipcode', event => {
            $('#ModalZipCode').modal('show');
        })

        window.addEventListener('hide-form-zipcode', event => {
            $('#ModalZipCode').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-city', event => {
            $('#ModalCity').modal('show');
        })

        window.addEventListener('hide-form-city', event => {
            $('#ModalCity').modal('hide');
        })
    </script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#selUser").select2({
                minimumInputLength: 3,
                width: '100%',

                ajax: {
                    url: "{{ route('newUserMessage') }}",
                    type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            _token: CSRF_TOKEN,
                            search: params.term // search term
                        };
                    },
                    processResults: function(response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }

            });

        });
    </script>

    <script>
        $("#men").click(function() {
            $('.single-message').hide();
        });
    </script>

    <script>
        Livewire.on('alert', function() {
            Swal.fire(
                'Listo!',
                '<span class="retro">Tu Mensaje fue eliminado</span>',
                'success'
            )
        })
    </script>
@endsection
