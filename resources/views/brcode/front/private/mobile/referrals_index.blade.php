@extends('brcode.front.layout.app_config')
@section('content-css-include')
    <style>

    </style>
@endsection

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div class="page-content-wrapper">
            <div class="container">
                <!-- User Information-->

                @if (Auth::user()->referidos->count() > 0)
                    <div class="listview-title">@lang('messages.refered')</div>
                    @foreach (Auth::user()->referidos as $referido)
                        <ul class="listview image-listview media">
                            <li>
                                <a href="{{ route('user-info', $referido->user_name) }}" class="item">
                                    @if ($referido->profile_picture)
                                        <div class="imageWrapper">
                                            <img src="{{ url($referido->profile_picture) }}" alt="image" class="imaged w64">
                                        </div>
                                    @else
                                        <div class="imageWrapper">
                                            <img src="{{ asset('img/profile-picture-not-found.png') }}" alt="image"
                                                class="imaged w64">
                                        </div>
                                    @endif

                                    <div class="in">
                                        <div>
                                            {{ $referido->user_name . ' | ' . $referido->first_name . ' ' . $referido->last_name }}
                                            <div class="text-muted">
                                                <?php
                                                $duration = [];
                                                
                                                foreach ($referido->inventory as $item) {
                                                    $cantidad = $item->quantity;
                                                    $duration[] = $cantidad;
                                                }
                                                
                                                $total = array_sum($duration);
                                                ?>

                                                @if ($total == 0)
                                                    @lang('messages.not_working_profile')
                                                @else
                                                    {{ $total }} @lang('messages.sell_top')
                                                @endif
                                            </div>
                                        </div>
                                        <span class="text-muted">@lang('messages.the_see')</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    @endforeach
                @else
                    <div class="flex items-center justify-center h-screen">
                        <div class="page-content-wrapper">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <blockquote class="nk-blockquote">
                                            <div class="nk-blockquote-icon"></div>
                                            <div class="text-center nk-blockquote-content h3 font-extrabold">
                                                <font color="black">
                                                    De momento no tienes referidos. Si deseas tener referidos, comparte tu
                                                    enlace de referido con otros usuarios y pídeles que se registren a
                                                    través de él. Si tienes alguna duda, consulta nuestra sección de
                                                    preguntas frecuentes.
                                                </font>
                                                <br>
                                            </div>
                                            <div class="nk-gap-2"></div>
                                            <div class="text-center">
                                                <a class="btn btn-danger" href="/site/faqs">Buscar</a>
                                            </div>
                                            <div class="nk-blockquote-author"></div>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    @else
    @endif
@endsection

@section('content-script-include')
    <!-- Flickity -->
    <script src="{{ asset('assets/vendor/flickity/dist/flickity.pkgd.min.js') }}"></script>

    <!-- Hammer.js -->
    <script src="{{ asset('assets/vendor/hammerjs/hammer.min.js') }}"></script>
@endsection
@section('content-script')
    <script>
        $(document).ready(function() {
            $('.comments').popover();
            $('[data-toggle="popover"]').popover();
        });
    </script>
    <script>
        window.addEventListener('show-form-terms', event => {
            $('#termModal').modal('show');
        })

        window.addEventListener('hide-form-terms', event => {
            $('#termModal').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-youtube', event => {
            $('#youtubeModal').modal('show');
        })

        window.addEventListener('hide-form-youtube', event => {
            $('#youtubeModal').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-discord', event => {
            $('#discordModal').modal('show');
        })

        window.addEventListener('hide-form-discord', event => {
            $('#discordModal').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-instagram', event => {
            $('#instagramModal').modal('show');
        })

        window.addEventListener('hide-form-instagram', event => {
            $('#instagramModal').modal('hide');
        })
    </script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#selUser").select2({
                minimumInputLength: 3,
                width: '100%',

                ajax: {
                    url: "{{ route('newUserMessage') }}",
                    type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            _token: CSRF_TOKEN,
                            search: params.term // search term
                        };
                    },
                    processResults: function(response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }

            });

        });
    </script>

    <script>
        $("#men").click(function() {
            $('.single-message').hide();
        });
    </script>

    <script>
        Livewire.on('alert', function() {
            Swal.fire(
                'Listo!',
                '<span class="retro">Tu Mensaje fue eliminado</span>',
                'success'
            )
        })
    </script>
@endsection
