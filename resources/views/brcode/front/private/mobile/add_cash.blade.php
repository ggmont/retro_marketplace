@extends('brcode.front.layout.app_config')
@section('content-css-include')
    <style>

    </style>
@endsection

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div class="page-content-wrapper py-3">
            <div class="container">
                <div class="listview-title">@lang('messages.information_basic')</div>
                <ul class="listview image-listview">
                    <li>
                        <div class="item">
                            <div class="in">
                                <div>
                                    <header>@lang('messages.cash_available')</header>
                                    {{ auth()->user()->cash }} €
                                </div>
                            </div>
                        </div>
                    </li>
                    @if ($total > 0)
                        <li>
                            <div class="item">
                                <div class="in">
                                    <div>
                                        <header>@lang('messages.pending_pay')</header>
                                        {{ number_format($total, 2, '.', ' ') }} €
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endif
                    @if ($total > 0)
                        @if ($total < Auth::user()->cash)
                            <li>
                                <div class="item">
                                    <div class="in">
                                        <div>
                                            <header>@lang('messages.should_enter')</header>
                                            {{ number_format(Auth::user()->cash - $total, 2, '.', ' ') }} €
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @else
                            <li>
                                <div class="item">
                                    <div class="in">
                                        <div>
                                            <header>@lang('messages.should_enter')</header>
                                            {{ number_format($total - Auth::user()->cash, 2, '.', ' ') }} €
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endif
                    @else
                        <li>
                            <div class="item">
                                <div class="in">
                                    <div>
                                        <header>@lang('messages.should_enter')</header>
                                        0.00 €
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endif
                </ul>
                <div class="listview-title mt-2">@lang('messages.transfer_bank') <i class="fa fa-bank"></i></div>
                <ul class="listview image-listview">
                    <li>
                        <div class="item">
                            <div class="in">
                                <div>
                                    <header><i class="fa fa-clock"></i></header>
                                    @lang('messages.sepa_payment')
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item">
                            <div class="in">
                                <div>
                                    <header>@lang('messages.dest_payment')</header>
                                    Retro Gaming Market
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item">
                            <div class="in">
                                <div>
                                    <header>IBAN</header>
                                    ES8301828749940201553992
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item">
                            <div class="in">
                                <div>
                                    <header>BIC</header>
                                    BBVAESMM
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item">
                            <div class="in">
                                <div>
                                    <header>@lang('messages.payment_razon')</header>
                                    {{ Auth::user()->concept }}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item">
                            <div class="in">
                                <div>
                                    <header>@lang('messages.bank_name')</header>
                                    BBVA
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item">
                            <div class="in">
                                <div>
                                    <header>@lang('messages.important')</header>
                                    @lang('messages.dont_forget_payment') {{ Auth::user()->concept }} @lang('messages.transfer_payment')
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    @else
    @endif
@endsection

@section('content-script-include')
    <!-- Flickity -->
    <script src="{{ asset('assets/vendor/flickity/dist/flickity.pkgd.min.js') }}"></script>

    <!-- Hammer.js -->
    <script src="{{ asset('assets/vendor/hammerjs/hammer.min.js') }}"></script>
@endsection
@section('content-script')
    <script>
        $(document).ready(function() {
            $('.comments').popover();
            $('[data-toggle="popover"]').popover();
        });
    </script>
    <script>
        window.addEventListener('show-form-terms', event => {
            $('#termModal').modal('show');
        })

        window.addEventListener('hide-form-terms', event => {
            $('#termModal').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-youtube', event => {
            $('#youtubeModal').modal('show');
        })

        window.addEventListener('hide-form-youtube', event => {
            $('#youtubeModal').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-discord', event => {
            $('#discordModal').modal('show');
        })

        window.addEventListener('hide-form-discord', event => {
            $('#discordModal').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-instagram', event => {
            $('#instagramModal').modal('show');
        })

        window.addEventListener('hide-form-instagram', event => {
            $('#instagramModal').modal('hide');
        })
    </script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#selUser").select2({
                minimumInputLength: 3,
                width: '100%',

                ajax: {
                    url: "{{ route('newUserMessage') }}",
                    type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            _token: CSRF_TOKEN,
                            search: params.term // search term
                        };
                    },
                    processResults: function(response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }

            });

        });
    </script>

    <script>
        $("#men").click(function() {
            $('.single-message').hide();
        });
    </script>

    <script>
        Livewire.on('alert', function() {
            Swal.fire(
                'Listo!',
                '<span class="retro">Tu Mensaje fue eliminado</span>',
                'success'
            )
        })
    </script>
@endsection
