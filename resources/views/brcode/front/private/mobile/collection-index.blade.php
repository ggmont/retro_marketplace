@extends('brcode.front.layout.app_inventory')
@section('content-css-include')
    @livewireStyles
    <style>

        .tabs {
            overflow: hidden;
        }

        .tabs-content {
            max-height: 0;
            transition: all 0.5s;
        }

        input:checked+.tabs-label .test {
            background-color: #000;
        }

        input:checked+.tabs-label .test svg {
            transform: rotate(180deg);
            stroke: #fff;
        }

        input:checked+.tabs-label::after {
            transform: rotate(90deg);
        }

        input:checked~.tabs-content {
            max-height: 100vh;
        }

        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }
    </style>
@endsection
@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div class="header-area" id="headerArea">
            <div class="container h-100 d-flex align-items-center justify-content-between">
                <!-- Back Button-->
                <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                <!-- Page Title-->
                <div class="page-heading">
                    <h6 class="mb-0 font-extrabold">Mi colección</h6>
                </div>
                <!-- Navbar Toggler-->

                @if (Auth::user())
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @else
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @endif
            </div>
        </div>

        @include('partials.flash')

        <br>
        @if ($collection > 0)
            <livewire:mobile.collection-personal>
            </livewire:mobile.collection-personal>
        @else
        <div class="flex items-center justify-center h-screen">
            <div class="page-content-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <blockquote class="nk-blockquote">
                                <div class="nk-blockquote-icon"></div>
                                <div class="text-center nk-blockquote-content h3 font-extrabold">
                                    <font color="black">
                                        De momento no tienes productos en tu colección, si deseas agregar alguno pulsa aquí
                                    </font>
                                    <br>
                                </div>
                                <div class="nk-gap-2"></div>
                                <div class="text-center">
                                    <a class="btn btn-danger" href="{{ route('search_collection') }}">Buscar</a>
                                </div>
                                <div class="nk-blockquote-author"></div>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        @endif
        <!-- Internet Connection Status-->
        <div class="internet-connection-status" id="internetStatus"></div>
    @else
        <div class="container">
            <div class="row justify-content-lg-center">

                <div class="col-lg-12">
                    <ul class="nk-breadcrumbs">
                        <br>
                        <li>
                            <div class="section-title1 font-extrabold">
                                <h3 class="retro">{{ __('Mi inventario') }}</h3>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="nk-gap-1"></div>

            <span class="retro">@include('partials.flash')</span>

            <div class="col-lg-12">

                <div class="nk-gap"></div>
                <!-- START: Tabs  -->
                <div class="nk-tabs">

                    <ul class="nav nav-tabs" role="tablist">
                        @if ((new \Jenssegers\Agent\Agent())->isMobile())
                            &nbsp;&nbsp;&nbsp;
                        @else
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp; &nbsp;&nbsp;
                        @endif
                        <li class="nav-item">
                            <a class="nav-link active" href="#tabs-1-1" role="tab" data-toggle="tab"><span
                                    class="retro-sell font-extrabold">@lang('messages.games')</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-2" role="tab" data-toggle="tab"><span
                                    class="retro-sell font-extrabold">@lang('messages.consoles')</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-3" role="tab" data-toggle="tab"><span
                                    class="retro-sell font-extrabold">@lang('messages.peripherals')</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-4" role="tab" data-toggle="tab"><span
                                    class="retro-sell font-extrabold">@lang('messages.accesories')</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-5" role="tab" data-toggle="tab"><span
                                    class="retro-sell font-extrabold">@lang('messages.merch')</span></a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade show active" id="tabs-1-1">
                            <div class="nk-gap"></div>
                            <livewire:profile.inventory.user-inventory-game>
                            </livewire:profile.inventory.user-inventory-game>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-2">
                            <div class="nk-gap"></div>
                            <livewire:profile.inventory.user-inventory-console>
                            </livewire:profile.inventory.user-inventory-console>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-3">
                            <div class="nk-gap"></div>
                            <livewire:profile.inventory.user-inventory-periferico>
                            </livewire:profile.inventory.user-inventory-periferico>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-4">
                            <div class="nk-gap"></div>
                            <livewire:profile.inventory.user-inventory-accesorio>
                            </livewire:profile.inventory.user-inventory-accesorio>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-5">
                            <div class="nk-gap"></div>
                            <livewire:profile.inventory.user-inventory-merchadising>
                            </livewire:profile.inventory.user-inventory-merchadising>

                        </div>
                    </div>
                </div>
                <!-- END: Tabs -->
            </div>





        </div>
        <div class="nk-gap"></div>
    @endif
@endsection

@section('content-script-include')
    <script src="{{ asset('brcode/js/pty.components.js') }}"></script>


    <script src="{{ asset('assets/noty/packaged/jquery.noty.packaged.min.js') }}"></script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
@section('content-script')
    @livewireScripts

    <script>
        window.addEventListener('show-form-price', event => {
            $('#ModalPrice').modal('show');
        })

        window.addEventListener('hide-form-price', event => {
            $('#ModalPrice').modal('hide');
        })
    </script>

    <script>
        window.addEventListener('show-form-ultimate', event => {
            document.getElementById("actualizar").click();
        })
    </script>

    <script>
        Livewire.on('alert', function() {
            Swal.fire(
                'Listo!',
                '<span class="retro">Nueva Promocion Agregada</span>',
                'success'
            )
        })
    </script>

    <script>
        Livewire.on('star', function() {


            var $inventoryAdd = $('.br-btn-product-add');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('game', function() {


            var $inventoryAdd = $('.br-btn-product-add');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('console', function() {


            var $inventoryAdd = $('.br-btn-product-add-console');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('periferico', function() {


            var $inventoryAdd = $('.br-btn-product-add-periferico');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('accesorio', function() {


            var $inventoryAdd = $('.br-btn-product-add-accesorio');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('merchandising', function() {


            var $inventoryAdd = $('.br-btn-product-add-merchandising');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('alert', function() {
            Swal.fire(
                'Listo!',
                '<span class="retro">Tu Producto fue eliminado</span>',
                'success'
            )
        })
    </script>

    <script>
        Livewire.on('favorite', function() {
            Swal.fire({
                title: '<span class="retro">Listo!</span>',
                text: 'El Producto ha sido destacado correctamente',
            })
        })
    </script>

    <script>
        Livewire.on('desfavorite', function() {
            Swal.fire({
                title: '<span class="retro">Listo!</span>',
                text: 'El Producto ha sido quitado de destacados correctamente',
            })
        })
    </script>
@endsection
