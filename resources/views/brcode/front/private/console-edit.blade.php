@php($texto = app('request')->input('side') ? 'app_sidebar_game' : 'app')

@extends('brcode.front.layout.' . $texto)

@section('content-css-include')
    <!-- Dropzone css -->
    <link href="/assets/dropzone-master/dist/min/dropzone.min.css" rel="stylesheet" type="text/css" />
    <style>
        .dropzone .dz-preview .dz-error-message {
            top: 175px !important;
        }

        .retro {
            font-family: 'Press Start 2P', cursive;
        }
    </style>
@endsection
@section('content')

    <div class="nk-main">
        <!-- START: Breadcrumbs -->

        <div class="nk-gap-3"></div>
        <div class="top-products-area clearfix py-5">
            <div class="header-area" id="headerArea">
                <div class="container h-100 d-flex align-items-center justify-content-between">
                    <!-- Back Button-->
                    <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                    <!-- Page Title-->
                    <div class="page-heading">
                        <h6 class="mb-0 font-extrabold">Detalles del Producto</h6>
                    </div>
                    <!-- Navbar Toggler-->

                    <div class="normal">
                        @if (Auth::user())
                            <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebarPanel">
                                <span></span><span></span><span></span>
                            </div>
                        @else
                            <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebarPanel">
                                <span></span><span></span><span></span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- END: Breadcrumbs -->
            <div class="product-slides owl-carousel">

                <div class="relative">
                    @if (count($inventory->images) > 0)
                        <a href="{{ url('/uploads/inventory-images/' . $inventory->images->first()->image_path) }}"
                            class="fancybox" data-fancybox="RGM">
                            <img class="single-product-slide"
                                src="{{ url('/uploads/inventory-images/' . $inventory->images->first()->image_path) }}"
                                alt="">
                        </a>
                    @else
                        <img class="single-product-slide" loading="lazy"
                            src="{{ asset('assets/images/art-not-found.jpg') }}" alt="">
                    @endif
                </div>

                @if (count($inventory->images) > 1)
                    @foreach ($inventory->images as $key)
                        @if (!$loop->first)
                            <div class="relative">
                                @if (count($inventory->images) > 0)
                                    <a href="{{ '/uploads/inventory-images/' . $key->image_path }}" class="fancybox"
                                        data-fancybox="RGM">
                                        <img class="single-product-slide"
                                            src="{{ '/uploads/inventory-images/' . $key->image_path }}" alt="dummy-image">
                                    </a>
                                @endif
                            </div>
                        @endif
                    @endforeach
                @endif

            </div>



            <div class="product-description">

                <!-- Product Specification-->
                <div class="p-specification bg-white mb-3 py-3">
                    <div class="container">
                        <div class="p-title-price text-center">
                            <h5 class="mb-1 font-extrabold">{{ $inventory->product->name }}</h5>
                            <br>
                            <h5 class="mb-1 font-extrabold">- Precios Generales -</h5>
                            <br>
                            <?php
                            $duration = [];
                            $duration_not_press = [];
                            $duration_used = [];
                            $duration_new = [];
                            $duration_general = [];
                            $t = '';
                            $total_numeros = '';
                            $total_numeros_not_press = '';
                            $total_numeros_used = '';
                            $total_numeros_new = '';
                            $total_numeros_general = '';
                            $suma = '';
                            $suma_not_press = '';
                            $suma_used = '';
                            $suma_new = '';
                            $suma_general = '';
                            $m = '';
                            $promedio_not_press = '';
                            $promedio_used = '';
                            $promedio_new = '';
                            $promedio_general = '';
                            $suma_total = '';
                            $fecha = '';
                            $precio = '';
                            
                            foreach ($inventory->product->inventory as $item) {
                                $cantidad = $item->price;
                                $duration[] = $cantidad;
                                $total_numeros = count($duration);
                                $total = max($duration);
                                $t = min($duration);
                                $suma = array_sum($duration);
                                $m = $suma / $total_numeros;
                                $suma_total = $item->created_at;
                            }
                            
                            //PROMEDIO GENERAL
                            foreach ($seller_sold_general as $p) {
                                $cantidad_general = $p->price;
                                $duration_general[] = $cantidad_general;
                                $total_numeros_general = count($duration_general);
                                $suma_general = array_sum($duration_general);
                                $promedio_general = $suma_general / $total_numeros_general;
                            }
                            
                            //PROMEDIO PARA LAS CONDICIONES TIPO "NOT PRESS - NO TIENE" POKEMON GOLD ES EL QUE NO TIENE ASTERIX Y OBELIS TAMPOCO TIENEN
                            foreach ($seller_sold_not_pres as $p) {
                                $cantidad_press = $p->price;
                                $duration_not_press[] = $cantidad_press;
                                $total_numeros_not_press = count($duration_not_press);
                                $suma_not_press = array_sum($duration_not_press);
                                $promedio_not_press = $suma_not_press / $total_numeros_not_press;
                            }
                            
                            //PROMEDIO PARA LAS CONDICIONES TIPO "USED" WOODY ES EL QUE ESTA USADO
                            foreach ($seller_sold_used as $p_used) {
                                $cantidad_used = $p_used->price;
                                $duration_used[] = $cantidad_used;
                                $total_numeros_used = count($duration_used);
                                $suma_used = array_sum($duration_used);
                                $promedio_used = $suma_used / $total_numeros_used;
                            }
                            
                            //PROMEDIO PARA LAS CONDICIONES TIPO "NEW" XG2 ES EL QUE TIENE EL ESTADO NUEVO
                            foreach ($seller_sold_new as $p_new) {
                                $cantidad_new = $p_new->price;
                                $duration_new[] = $cantidad_new;
                                $total_numeros_new = count($duration_new);
                                $suma_new = array_sum($duration_new);
                                $promedio_new = $suma_new / $total_numeros_new;
                            }
                            
                            ?>

                            <?php
                            $l = $inventory->price_solo + $inventory->price_used + $inventory->price_new / 3;
                            ?>


                            @if ($promedio_general > 0)
                                <center>
                                    <div class="row-edit">
                                        <div class="col">
                                            <div class="total-result-of-ratings">
                                                <span>

                                                    @if ($promedio_new > 0)
                                                        {{ number_format($promedio_new, 2) }} €
                                                    @elseif($inventory->product->price_new)
                                                        {{ number_format($inventory->product->price_new, 2) }} €
                                                    @else
                                                        -
                                                    @endif

                                                </span>
                                                <span>Nuevo</span>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="total-result-of-ratings-used">
                                                <span>

                                                    @if ($promedio_used > 0)
                                                        {{ number_format($promedio_used, 2) }} €
                                                    @elseif($inventory->product->price_used)
                                                        {{ number_format($inventory->product->price_used, 2) }} €
                                                    @else
                                                        -
                                                    @endif

                                                </span><span>Usado</span>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="total-result-of-ratings-solo"><span>
                                                    @if ($promedio_not_press > 0)
                                                        {{ number_format($promedio_not_press, 2) }} €
                                                    @elseif($inventory->product->price_solo)
                                                        {{ number_format($inventory->product->price_solo, 2) }} €
                                                    @else
                                                        -
                                                    @endif
                                                </span><span>Solo</span>
                                            </div>
                                        </div>
                                    </div>
                                </center>
                            @else
                                <div class="row">
                                    <div class="col">
                                        <div class="total-result-of-ratings">
                                            <span>

                                                @if ($inventory->product->price_new > 0)
                                                    {{ number_format($inventory->product->price_new, 2) }} €
                                                @else
                                                    -
                                                @endif


                                            </span>
                                            <span>Nuevo</span>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="total-result-of-ratings-used"><span>

                                                @if ($inventory->product->price_used > 0)
                                                    {{ number_format($inventory->product->price_used, 2) }} €
                                                @else
                                                    -
                                                @endif

                                            </span><span>Usado</span>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="total-result-of-ratings-solo"><span>

                                                @if ($inventory->product->price_solo > 0)
                                                    {{ number_format($inventory->product->price_solo, 2) }} €
                                                @else
                                                    -
                                                @endif

                                            </span><span>Solo</span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <br>
                        </div>
                        <h6>Detalles</h6>
                        <div class="nk-product-meta">
                            <div class="lists">
                                <ul class="is-disc">
                                    <li>
                                        <span class="font-extrabold">{{ __('Plataforma') }}: </span>
                                        {{ $inventory->product->platform }}
                                    </li>
                                    <li>
                                        <span class="font-extrabold">{{ __('Región') }}:</span>
                                        {{ $inventory->product->region }}
                                    </li>
                                    @if ($inventory->product->media)
                                        <li>
                                            <span class="font-extrabold">{{ __('Media') }}:</span>
                                            {{ $inventory->product->media }}
                                        </li>
                                    @endif
                                    @if ($inventory->product->language == '')
                                    @else
                                        <li>
                                            <span class="font-extrabold">{{ __('Idioma') }}:</span>
                                            {{ $inventory->product->language }}
                                        </li>
                                    @endif
                                    @if ($inventory->product->box_language == '')
                                    @else
                                        <li>
                                            <span class="font-extrabold">{{ __('Idioma de Caja') }}:</span>
                                            {{ $inventory->product->box_language }}
                                        </li>
                                    @endif
                                    @if ($inventory->product->ean_upc == '')
                                    @else
                                        <li>
                                            <span class="font-extrabold">{{ __('Código de Barras') }}:</span>
                                            {{ $inventory->product->ean_upc }}
                                        </li>
                                    @endif
                                    @if ($inventory->comments == '')
                                    @else
                                        <li>
                                            <span class="font-extrabold">{{ __('Comentario') }}:</span>
                                            {{ $inventory->comments }}
                                        </li>
                                    @endif
                                </ul>
                            </div>


                            <div class="nk-gap"></div>


                        </div>

                    </div>
                </div>

            </div>
            <!-- Ratings Submit Form-->
            <!-- Rating & Review Wrapper-->
            <div class="container">
                <div class="section-heading mt-3">
                    <center>
                        <h4 class="mb-1 font-bold">Detalles del inventario</h4>
                    </center>
                </div>
                <!-- Contact Form-->
                <div class="section mt-2 mb-5">
                    <form id="br-inventory-add" class="w-full" action="{{ route('update_inventory', $inventory->id) }}"
                        method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label">Cantidad</label>
                                <input type="number" placeholder="Cantidad de articulos" id="name_field"
                                    name="{{ $viewData['col_prefix'] }}quantity" value="{{ $inventory->quantity }}"
                                    min="1" class="form-control">
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        @if ($inventory->in_collection == 'Y')
                            <input type="hidden" name="{{ $viewData['col_prefix'] }}price" value="">
                        @else
                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label">Precio</label>

                                    <input type="number" autocomplete="off" name="{{ $viewData['col_prefix'] }}price"
                                        class="form-control br-input-number required  {{ $errors->has($viewData['col_prefix'] . 'price') ? 'is-invalid' : '' }} price-check"
                                        step="0.01" required placeholder="Precio" value="{{ $inventory->price }}" />

                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>
                        @endif

                        <div class="section-heading mt-3">
                            <h4 class="mb-1 font-bold">Condiciones</h4>
                        </div>

                        @php( $title_game = [
                            'NEW' => "Nuevo",
                            'USED-NEW' => "Usado como nuevo",
                            'USED' => "Usado",
                            'USED-VERY' => "Muy usado" ,
                            'NOT-WORK' => "No funciona",
                            'NOT-PRES' => "No aplica"
                        ])

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5">Caja</label>
                                <select name="{{ $viewData['col_prefix'] }}box_condition" required
                                    class="form-control form-select" id="default_select">

                                    @isset($viewData['game_states'])
                                        @foreach ($viewData['game_states'] as $k => $r)
                                            {{ __($r['value']) }} QUE
                                            <option data-html="{{ __($r['value']) }}"
                                                {{ (old($viewData['col_prefix'] . 'box_condition') !== null ? old($viewData['col_prefix'] . 'box_condition') : (isset($viewData['model_data']['box_condition']) ? $viewData['model_data']['box_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                value="{{ $r['id'] }}">{{ __($r['value']) }}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5">Juego</label>
                                <select name="{{ $viewData['col_prefix'] }}game_condition" required
                                    class="form-control form-select" id="default_select">
                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                    </option>
                                    @isset($viewData['game_states'])
                                        @foreach ($viewData['game_states'] as $k => $r)
                                            <option data-html="{{ __($r['value']) }}"
                                                {{ (old($viewData['col_prefix'] . 'game_condition') !== null ? old($viewData['col_prefix'] . 'game_condition') : (isset($viewData['model_data']['game_condition']) ? $viewData['model_data']['game_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                value="{{ $r['id'] }}">{{ __($r['value']) }}
                                            </option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5">Manual</label>
                                <select name="{{ $viewData['col_prefix'] }}manual_condition" required
                                    class="form-control form-select" id="default_select">
                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                    </option>
                                    @isset($viewData['game_states'])
                                        @foreach ($viewData['game_states'] as $k => $r)
                                            <option data-html="{{ __($r['value']) }}"
                                                {{ (old($viewData['col_prefix'] . 'manual_condition') !== null ? old($viewData['col_prefix'] . 'manual_condition') : (isset($viewData['model_data']['manual_condition']) ? $viewData['model_data']['manual_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                value="{{ $r['id'] }}">{{ __($r['value']) }}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5"> {{ __('Caratula') }}</label>
                                <select name="{{ $viewData['col_prefix'] }}cover_condition" required
                                    class="form-control form-select" id="default_select">
                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                    </option>
                                    @isset($viewData['game_states'])
                                        @foreach ($viewData['game_states'] as $k => $r)
                                            <option data-html="{{ __($r['value']) }}"
                                                {{ (old($viewData['col_prefix'] . 'cover_condition') !== null ? old($viewData['col_prefix'] . 'cover_condition') : (isset($viewData['model_data']['cover_condition']) ? $viewData['model_data']['cover_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                value="{{ $r['id'] }}">{{ __($r['value']) }}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5"> {{ __('Cables') }}</label>
                                <select name="{{ $viewData['col_prefix'] }}inside_condition" required
                                    class="form-control form-select" id="default_select">
                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                    </option>
                                    @isset($viewData['game_states'])
                                        @foreach ($viewData['game_states'] as $k => $r)
                                            <option data-html="{{ __($r['value']) }}"
                                                {{ (old($viewData['col_prefix'] . 'inside_condition') !== null ? old($viewData['col_prefix'] . 'inside_condition') : (isset($viewData['model_data']['inside_condition']) ? $viewData['model_data']['inside_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                value="{{ $r['id'] }}">{{ __($r['value']) }}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5"> {{ __('Extras') }}</label>
                                <select name="{{ $viewData['col_prefix'] }}extra_condition" required
                                    class="form-control form-select" id="default_select">
                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                    </option>
                                    @isset($viewData['game_states'])
                                        @foreach ($viewData['game_states'] as $k => $r)
                                            <option data-html="{{ __($r['value']) }}"
                                                {{ (old($viewData['col_prefix'] . 'extra_condition') !== null ? old($viewData['col_prefix'] . 'extra_condition') : (isset($viewData['model_data']['extra_condition']) ? $viewData['model_data']['extra_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                value="{{ $r['id'] }}">{{ __($r['value']) }}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>


                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5"> {{ __('Comentarios') }}</label>
                                <textarea id="comments" name="{{ $viewData['col_prefix'] }}comments" rows="4" class="form-control"
                                    placeholder="Comentario"> {{ $inventory->comments }} </textarea>
                                <div class="textarea-counter" data-text-max="300"
                                    data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></div>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label"> {{ __('Imagenes del Producto') }}</label>
                            </div>
                        </div>

                        <div style="display:none" id="fileupload-files" data-name="image_path[]">
                            @foreach ($inventory->images as $p)
                                @php($i = 0)
                                <input type="hidden" id="file_{{ $i }}" name="image_path[]"
                                    value="{{ $p->image_path }}">
                                @php($i++)
                            @endforeach
                        </div>
                        <div id="dropzone">
                            <div class="dz-message text-dark" data-dz-message>
                                <span class="retro">@lang('messages.image_sell_mobile')</span>
                            </div>
                        </div>

                        <br>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label"> {{ __('Codigo de Barras') }} (Opcional)</label>
                            </div>
                        </div>

                        <div style="display:none" id="fileupload-files-ean" data-name="image_path[]">
                            @foreach ($inventory->imagesean as $o)
                                @php($i = 0)
                                <input type="hidden" id="file_{{ $i }}" name="image_path[]"
                                    value="{{ $o->image_path }}">
                                @php($i++)
                            @endforeach
                        </div>
                        <div id="dropzone-ean">
                            <div class="dz-message text-dark" data-dz-message>
                                <span class="retro">@lang('messages.image_sell_mobile')</span>
                            </div>
                        </div>
                        <br>
                        <div id="br-inventory-options">
                            <button id="br-publish-item" type="submit" class="btn text-white btn-warning btn-lg w-100"
                                type="submit">@lang('messages.updates')</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>






@endsection

@section('content-script-include')
    <script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>

    <script src="{{ asset('assets/dropzone-master/dist/min/dropzone.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/quagga/0.12.1/quagga.js"></script>
    <script src="{{ asset('vendor/sweetalert2/sweetalert2.all.min.js') }}"></script>
@endsection

@section('content-script')
    <script>
        var global_lang = [];
        global_lang['app.uploader_file_size_bigger'] = 'file bigger';
        global_lang['app.uploader_file_ext_not_supported'] = 'ext not supported';
    </script>
    <script>
        $(document).ready(function() {
            $('#grid').select2();
            $('#genero').select2();
            $('#location').select2();
            $('#generation').select2();
            $('#language').select2();
            $('#box_language').select2();
            $('#media').select2();
            $('#platform').select2();
            $('#region').select2();
        });
    </script>




    <script>
        var acceptedFileTypes = "image/*"; //dropzone requires this param be a comma separated list
        var fileList = new Array;
        var i = 0;
        var myDropzone = $("#dropzone").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('account/inventory/upload_file/' . $inventory->id) }}",
            addRemoveLinks: true,

            removedfile: function(file) {
                var name = file.name;
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: "{{ url('account/inventory/delete_file/' . $inventory->id) }}",
                    data: {
                        filename: name
                    },
                    success: function(data) {
                        console.log("Se ha borrado correctamente");
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            @if (isset($inventory->images))
                maxFiles: 8 - {{ $prueba }},
            @else
                maxFiles: 8, //change limit as per your requirements
            @endif
            maxfilesexceeded: function(file) {
                Swal.fire({
                    title: '<span class="retro">Error</span>',
                    text: 'Haz Alcanzado el maximo de Imagenes (Max 8)',
                    footer: '<a class="retro" href="">¿Como Vender En RGM?</a>',
                    imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/a6cff967-2930-4785-a8b8-48a6e39101b8/dbrz0sh-36c848a3-b37d-4fd1-8d7e-90a27dcb8130.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2E2Y2ZmOTY3LTI5MzAtNDc4NS1hOGI4LTQ4YTZlMzkxMDFiOFwvZGJyejBzaC0zNmM4NDhhMy1iMzdkLTRmZDEtOGQ3ZS05MGEyN2RjYjgxMzAuZ2lmIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.QwzwRFsxRbUrhacQuJV4fSAij3GxImG_4dkxWDTgkMc',
                    imageWidth: 400,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                })
                this.removeFile(file);
            },
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Arrastra archivos aquí o haz clic para subir.", // Cambiar el mensaje predeterminado
            dictRemoveFile: "Eliminar archivo", // Cambiar el texto para eliminar archivo
            dictCancelUpload: "Cancelar subida", // Cambiar el texto para cancelar subida
            dictMaxFilesExceeded: "Ha alcanzado el límite de archivos permitidos.", // Cambiar el texto para archivos excedidos
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileList[i] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": i
                    };
                    input = $('#fileupload-files').data('name');
                    $('#fileupload-files').append(
                        `<input type="hidden" id="file_${ i }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    i += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileList.length; f++) {
                        if (fileList[f].fileName == file.name) {
                            rmvFile = fileList[f].serverFileName;
                            $("#file_" + fileList[f].fileId).remove();
                        }
                    }
                });

                @foreach ($inventory->images as $p)
                    var mockFile = {
                        name: "{{ $p->image_path }}",
                        size: 12345
                    };

                    myDropzone.emit("addedfile", mockFile);

                    myDropzone.emit("thumbnail", mockFile, "/uploads/inventory-images/{{ $p->image_path }}");
                    myDropzone.emit("complete", mockFile);
                    fileList[i] = {
                        "serverFileName": "{{ $p->image_path }}",
                        "fileName": "{{ $p->image_path }}",
                        "fileId": i
                    };
                    i += 1;
                @endforeach

            }
        });

        var fileListEan = new Array;
        var j = 0;
        var myDropzone = $("#dropzone-ean").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('account/inventory/upload_file_ean/' . $inventory->id) }}",
            addRemoveLinks: true,

            removedfile: function(file) {
                var name = file.name;
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: "{{ url('account/inventory/delete_file_ean/' . $inventory->id) }}",
                    data: {
                        filename: name
                    },
                    success: function(data) {
                        console.log("Se ha borrado correctamente");
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            maxFiles: 4, //change limit as per your requirements
            maxfilesexceeded: function(file) {
                Swal.fire({
                    title: '<span class="retro">Error</span>',
                    text: 'Haz Alcanzado el maximo de Imagenes (Max 4)',
                    footer: '<a class="retro" href="">¿Como Vender En RGM?</a>',
                    imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/a6cff967-2930-4785-a8b8-48a6e39101b8/dbrz0sh-36c848a3-b37d-4fd1-8d7e-90a27dcb8130.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2E2Y2ZmOTY3LTI5MzAtNDc4NS1hOGI4LTQ4YTZlMzkxMDFiOFwvZGJyejBzaC0zNmM4NDhhMy1iMzdkLTRmZDEtOGQ3ZS05MGEyN2RjYjgxMzAuZ2lmIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.QwzwRFsxRbUrhacQuJV4fSAij3GxImG_4dkxWDTgkMc',
                    imageWidth: 400,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                })
                this.removeFile(file);
            },
            acceptedFiles: ".jpg,.jpeg,.png",
            dictInvalidFileType: "No puedes subir archivos de este tipo , solo Imagenes tipo PNG O JPG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Arrastra archivos aquí o haz clic para subir.", // Cambiar el mensaje predeterminado
            dictRemoveFile: "Eliminar archivo", // Cambiar el texto para eliminar archivo
            dictCancelUpload: "Cancelar subida", // Cambiar el texto para cancelar subida
            dictMaxFilesExceeded: "Ha alcanzado el límite de archivos permitidos.", // Cambiar el texto para archivos excedidos
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileListEan[j] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": j
                    };
                    input = $('#fileupload-files-ean').data('name');
                    $('#ffileupload-files-ean').append(
                        `<input type="hidden" id="file_${ j }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    j += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileListEan.length; f++) {
                        if (fileListEan[f].fileName == file.name) {
                            rmvFile = fileListEan[f].serverFileName;
                            $("#file_" + fileListEan[f].fileId).remove();
                        }
                    }
                });

                @foreach ($inventory->imagesean as $o)
                    var mockFile = {
                        name: "{{ $o->image_path }}",
                        size: 12345
                    };

                    myDropzone.emit("addedfile", mockFile);

                    myDropzone.emit("thumbnail", mockFile, "/uploads/ean-images/{{ $o->image_path }}");
                    myDropzone.emit("complete", mockFile);
                    fileListEan[j] = {
                        "serverFileName": "{{ $o->image_path }}",
                        "fileName": "{{ $o->image_path }}",
                        "fileId": j
                    };
                    j += 1;
                @endforeach

            }
        });
    </script>



    <script>
        $('.openean').click(function() {
            $('.imageean').toggle();
        });

        function startScanner() {

            // heigth_image = document.getElementsByName("br_c_comments");
            heigth_image = document.getElementById("br-inventory-details");


            $('.imagesca').toggle();
            Quagga.init({
                inputStream: {
                    name: "Live",
                    type: "LiveStream",
                    target: document.querySelector('#data-data'),
                    locate: "True",
                    constraints: {
                        width: heigth_image.clientWidth,
                        height: 320,
                        facingMode: "environment"
                    },
                },
                decoder: {
                    readers: [
                        "code_128_reader",
                        "ean_reader",
                        "ean_8_reader",
                        "code_39_reader",
                        "code_39_vin_reader",
                        "codabar_reader",
                        "upc_reader",
                        "upc_e_reader",
                        "i2of5_reader"
                    ],
                },

            }, function(err) {
                if (err) {
                    // console.log(err);
                    return
                }

                Quagga.start();

                // Set flag to is running
                _scannerIsRunning = true;
            });

            Quagga.onDetected(function(result) {
                $("#ean").val(result.codeResult.code);
            });
        }

        function stopscan() {
            $('.imagesca').toggle();
            console.log(Quagga);
            Quagga.stop();
        }
    </script>
@endsection
