@extends('brcode.front.layout.app')
@section('content-css-include')
    <style>
        body {
            margin-top: 20px;
        }


        .modal-lg {
            width: 1100px;
        }

        h3 {
            font-size: 16px;
        }

        .text-navy {
            color: #1ab394;
        }

        .cart-product-imitation {
            text-align: center;
            padding-top: 30px;
            height: 80px;
            width: 80px;
            background-color: #f8f8f9;
        }

        .product-imitation.xl {
            padding: 120px 0;
        }

        .product-desc {
            padding: 20px;
            position: relative;
        }

        .ecommerce .tag-list {
            padding: 0;
        }

        .ecommerce .fa-star {
            color: #d1dade;
        }

        .ecommerce .fa-star.active {
            color: #f8ac59;
        }

        .ecommerce .note-editor {
            border: 1px solid #e7eaec;
        }

        table.shoping-cart-table {
            margin-bottom: 0;
        }

        table.shoping-cart-table tr td {
            border: none;
            text-align: right;
        }

        table.shoping-cart-table tr td.desc,
        table.shoping-cart-table tr td:first-child {
            text-align: left;
        }

        table.shoping-cart-table tr td:last-child {
            width: 80px;
        }

        .ibox {
            clear: both;
            margin-bottom: 25px;
            margin-top: 0;
            padding: 0;
        }

        .ibox.collapsed .ibox-content {
            display: none;
        }

        .ibox:after,
        .ibox:before {
            display: table;
        }

        .ibox-title {
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            background-color: #ffffff;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 3px 0 0;
            color: inherit;
            margin-bottom: 0;
            padding: 14px 15px 7px;
            min-height: 48px;
        }

        .ibox-content {
            background-color: #ffffff;
            color: inherit;
            padding: 15px 20px 20px 20px;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 1px 0;
        }

        .ibox-footer {
            color: inherit;
            border-top: 1px solid #e7eaec;
            font-size: 90%;
            background: #ffffff;
            padding: 10px 15px;
        }

        /* width */
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #888;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555;
        }

        .scroller {
            width: 100%;
            max-width: 1120px;
            height: 800px;
            overflow: auto;
            overflow-x: hidden;
        }

        .retro {
            font-family: 'Jost', sans-serif;
            font-weight: 800;
        }
    </style>
@endsection


@php($paypalSet = App\SysSettings::where('type', 'PayPal')->first())

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        @include('partials.flash')
        <div class="header-area" id="headerArea">
            <div class="container h-100 d-flex align-items-center justify-content-between">
                <!-- Back Button-->
                <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                <!-- Page Title-->
                <div class="page-heading">
                    <h6 class="mb-0 font-extrabold">@lang('messages.detail_order')</h6>
                </div>
                <!-- Navbar Toggler-->

                <div class="normal">
                    @if (Auth::user())
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @else
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        @php($showShippingSelect = $viewData['order_header']->seller->country_id != 46 || Auth::user()->country_id != 46)

        <div class="page-content-wrapper">
            <div class="container">
                <button type="button" id="actualizar" style="display:none" class="btn btn-secondary hidden me-05 mb-1"
                    onclick="notification('notification-6' , 3000)">Auto Close (3s)</button>
                <button type="button" id="errorgm" style="display:none" class="btn btn-secondary hidden me-05 mb-1"
                    onclick="notification('notification-7' , 3000)">Auto Close (3s)</button>
                <!-- android style -->

                <button type="button" class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed"
                    data-toggle="modal" data-target="#problemModal">
                    <span class="fa-solid fa-triangle-exclamation"></span>
                </button>

                <div class="listview-title mt-2">@lang('messages.detail_order')</div>
                <ul class="listview image-listview">
                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">{{ __('ID') }}</div>
                                <span
                                    class="text-muted text-sm font-semibold">{{ $viewData['order_header']->order_identification }}</span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">{{ __('Vendedor') }}</div>
                                <span
                                    class="text-muted text-sm font-semibold">{{ $viewData['order_header']->seller->user_name }}</span>
                            </div>
                        </div>
                    </li>

                    @php($fecha = date('d-m-Y'))
                    @php($Date = date('d-m-Y', strtotime($viewData['order_header']->created_on)))
                    @php($prueba = date('d-m-Y', strtotime($fecha . ' + 15 days')))

                    @if ($fecha >= $prueba)
                    @else
                        @if (in_array($viewData['order_header']->status, ['ST']))
                            @if ($viewData['order_header']->max_pay_out < date('Y-m-d H:i:s'))
                                <li>
                                    <div class="item" bis_skin_checked="1">
                                        <div class="in" bis_skin_checked="1">
                                            <div bis_skin_checked="1">
                                                @lang('messages.not_recieved_order')</div>
                                            @if ($viewData['order_header']->shipping_status == 'No')
                                            @else
                                                <form action="{{ route('statusShippingReceived') }}" class="order-mobile"
                                                    method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="id"
                                                        value="{{ $viewData['order_header']->order_identification }}">

                                                    <button type="submit" class="p-2 border-b btn-warning btn-block">
                                                        <span class="text-lg font-bold">@lang('messages.not_recieved_order_two')</span></button>
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                </li>
                            @endif
                        @endif

                    @endif


                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">{{ __('Artículos comprados') }}</div>
                                <span
                                    class="text-muted text-sm font-semibold">{{ $viewData['order_header']->quantity }}</span>
                            </div>
                        </div>
                    </li>



                    @if ($showShippingSelect)
                        <li>
                            <div class="item" bis_skin_checked="1">
                                <div class="in" bis_skin_checked="1">
                                    <div bis_skin_checked="1">@lang('messages.the_method') <br> @lang('messages.de_send')</div>
                                    <span class="text-muted text-sm font-semibold">
                                        @if ($viewData['order_shipping'])
                                            {{ $viewData['order_shipping']->name }} <br>
                                            @lang('messages.certified_order'): {{ $viewData['order_shipping']->certified }}
                                            <br>
                                            <b>Precio: {{ number_format($viewData['order_header']->shipping_price, 2) }}
                                            </b>
                                        @else
                                            -
                                        @endif
                                    </span>
                                </div>
                            </div>
                        </li>
                    @endif


                    @php($totalWeight = 0)
                    @php($totalSize = 0)

                    @if ($showShippingSelect)
                    @else
                        @if (isset($viewData['order_details']))
                            @foreach ($viewData['order_details'] as $detail)
                                @if ($detail->inventory->kg !== null)
                                    @php($totalWeight += $detail->quantity * ($detail->inventory->kg / 1000))
                                @else
                                    @php($totalWeight += $detail->quantity * ($detail->product->weight / 1000))
                                @endif

                                @if ($detail->inventory->size === null)
                                    @if (isset($detail->product->width) && isset($detail->product->large) && isset($detail->product->high))
                                        @php($totalSize += $detail->quantity * ($detail->product->width + $detail->product->large + $detail->product->high))
                                    @endif
                                @elseif ($detail->inventory->size == 0)
                                    @php($totalSize += $detail->quantity * (15 + 15 + 20))
                                @elseif ($detail->inventory->size == 1)
                                    @php($totalSize += $detail->quantity * (20 + 20 + 30))
                                @elseif ($detail->inventory->size == 2)
                                    @php($totalSize += $detail->quantity * (25 + 25 + 45))
                                @elseif ($detail->inventory->size == 3)
                                    @php($totalSize += $detail->quantity * (45 + 45 + 30))
                                @elseif ($detail->inventory->size == 4)
                                    @php($totalSize += $detail->quantity * (50 + 50 + 50))
                                @endif
                            @endforeach
                        @endif
                    @endif
                    @if ($showShippingSelect)
                    @else
                        <li>
                            <div class="item" bis_skin_checked="1">
                                <div class="in" bis_skin_checked="1">
                                    <div bis_skin_checked="1">{{ __('Coste de envío') }}</div>
                                    <span class="text-muted text-sm font-semibold">
                                        @if ($totalWeight >= 0 && $totalWeight <= 1 && $totalSize >= 0 && $totalSize <= 50)
                                            2.90 €
                                        @elseif($totalWeight > 1 && $totalWeight <= 3 && $totalSize <= 50)
                                            3.90 €
                                        @elseif($totalWeight > 3 && $totalWeight <= 5 && $totalSize <= 95)
                                            4.90 €
                                        @elseif($totalWeight > 5 && $totalWeight <= 10 && $totalSize <= 120)
                                            6.90 €
                                        @elseif($totalWeight > 10 && $totalWeight <= 20 && $totalSize <= 150)
                                            10.90 €
                                        @elseif($totalWeight >= 0 && $totalWeight <= 1)
                                            @if ($totalSize >= 0 && $totalSize <= 50)
                                                2.90 €
                                            @elseif($totalSize >= 50 && $totalSize <= 70)
                                                3.90 €
                                            @elseif($totalSize >= 70 && $totalSize <= 95)
                                                4.90 €
                                            @elseif($totalSize >= 95 && $totalSize <= 120)
                                                6.90 €
                                            @elseif($totalSize >= 120 && $totalSize <= 150)
                                                10.90 €
                                            @endif
                                        @elseif($totalWeight > 1 && $totalWeight <= 3)
                                            @if ($totalSize >= 0 && $totalSize <= 50)
                                                3.90 €
                                            @elseif($totalSize >= 50 && $totalSize <= 70)
                                                3.90 €
                                            @elseif($totalSize >= 70 && $totalSize <= 95)
                                                4.90 €
                                            @elseif($totalSize >= 95 && $totalSize <= 120)
                                                6.90 €
                                            @elseif($totalSize >= 120 && $totalSize <= 150)
                                                10.90 €
                                            @endif
                                        @elseif($totalWeight > 3 && $totalWeight <= 5)
                                            @if ($totalSize >= 0 && $totalSize <= 50)
                                                4.90 €
                                            @elseif($totalSize >= 50 && $totalSize <= 70)
                                                4.90 €
                                            @elseif($totalSize >= 70 && $totalSize <= 95)
                                                4.90 €
                                            @elseif($totalSize >= 95 && $totalSize <= 120)
                                                6.90 €
                                            @elseif($totalSize >= 120 && $totalSize <= 150)
                                                10.90 €
                                            @endif
                                        @elseif($totalWeight > 5 && $totalWeight <= 10)
                                            @if ($totalSize >= 0 && $totalSize <= 50)
                                                6.90 €
                                            @elseif($totalSize >= 50 && $totalSize <= 70)
                                                6.90 €
                                            @elseif($totalSize >= 70 && $totalSize <= 95)
                                                6.90 €
                                            @elseif($totalSize >= 95 && $totalSize <= 120)
                                                6.90 €
                                            @elseif($totalSize >= 120 && $totalSize <= 150)
                                                10.90 €
                                            @endif
                                        @elseif($totalWeight > 10 && $totalWeight <= 20)
                                            @if ($totalSize >= 0 && $totalSize <= 50)
                                                10.90 €
                                            @elseif($totalSize >= 50 && $totalSize <= 70)
                                                10.90 €
                                            @elseif($totalSize >= 70 && $totalSize <= 95)
                                                10.90 €
                                            @elseif($totalSize >= 95 && $totalSize <= 120)
                                                10.90 €
                                            @elseif($totalSize >= 120 && $totalSize <= 150)
                                                10.90 €
                                            @endif
                                        @endif
                                    </span>
                                </div>
                            </div>
                        </li>
                    @endif
                    @if ($showShippingSelect)
                    @else
                        <li>
                            <div class="item" bis_skin_checked="1">
                                <div class="in" bis_skin_checked="1">
                                    <div bis_skin_checked="1">{{ __('Peso') }}</div>
                                    <span class="text-muted text-sm font-semibold">
                                         {{ $totalWeight }} g
                                    </span>
                                </div>
                            </div>
                        </li>
                    @endif
                    @php($iva = $viewData['order_header']->total * 0.04)

                    @if ($showShippingSelect)
                    @else
                        <li>
                            <div class="item" bis_skin_checked="1">
                                <div class="in" bis_skin_checked="1">
                                    <div bis_skin_checked="1">{{ __('Gastos de gestión') }}</div>
                                    <span class="text-muted text-sm font-semibold">
                                        {{ number_format($iva, 2) }} €
                                    </span>
                                </div>
                            </div>
                        </li>
                    @endif
                    @if ($showShippingSelect)
                        <li>
                            <div class="item" bis_skin_checked="1">
                                <div class="in" bis_skin_checked="1">
                                    <div bis_skin_checked="1">{{ __('Total') }} ({{ config('brcode.paypal_currency') }})
                                    </div>
                                    <span
                                        class="text-muted text-sm font-semibold">{{ number_format($viewData['order_header']->total, 2) }}</span>
                                </div>
                            </div>
                        </li>
                    @else
                        <li>
                            <div class="item" bis_skin_checked="1">
                                <div class="in" bis_skin_checked="1">
                                    <div bis_skin_checked="1">{{ __('Total') }}
                                        ({{ config('brcode.paypal_currency') }})
                                    </div>
                                    <span class="text-muted text-sm font-semibold">

                                        @if ($totalWeight >= 0 && $totalWeight <= 1 && $totalSize >= 0 && $totalSize <= 50)
                                            {{ number_format($viewData['order_header']->total + $iva + 2.9, 2) }} €
                                        @elseif($totalWeight > 1 && $totalWeight <= 5 && $totalSize <= 95)
                                            {{ number_format($viewData['order_header']->total + $iva + 4.9, 2) }} €
                                        @elseif($totalWeight > 5 && $totalWeight <= 10 && $totalSize <= 120)
                                            {{ number_format($viewData['order_header']->total + $iva + 6.9, 2) }} €
                                        @elseif($totalWeight > 10 && $totalWeight <= 20 && $totalSize <= 150)
                                            {{ number_format($viewData['order_header']->total + $iva + 10.9, 2) }} €
                                        @elseif($totalWeight >= 0 && $totalWeight <= 1)
                                            @if ($totalSize >= 0 && $totalSize <= 50)
                                                {{ number_format($viewData['order_header']->total + $iva + 2.9, 2) }} €
                                            @elseif($totalSize >= 50 && $totalSize <= 95)
                                                {{ number_format($viewData['order_header']->total + $iva + 4.9, 2) }} €
                                            @elseif($totalSize >= 95 && $totalSize <= 120)
                                                {{ number_format($viewData['order_header']->total + $iva + 6.9, 2) }} €
                                            @elseif($totalSize >= 120 && $totalSize <= 150)
                                                {{ number_format($viewData['order_header']->total + $iva + 10.9, 2) }}
                                                €
                                            @endif
                                        @elseif($totalWeight > 1 && $totalWeight <= 5)
                                            @if ($totalSize >= 0 && $totalSize <= 50)
                                                {{ number_format($viewData['order_header']->total + $iva + 4.9, 2) }} €
                                            @elseif($totalSize >= 50 && $totalSize <= 95)
                                                {{ number_format($viewData['order_header']->total + $iva + 4.9, 2) }} €
                                            @elseif($totalSize >= 95 && $totalSize <= 120)
                                                {{ number_format($viewData['order_header']->total + $iva + 6.9, 2) }} €
                                            @elseif($totalSize >= 120 && $totalSize <= 150)
                                                {{ number_format($viewData['order_header']->total + $iva + 10.9, 2) }}
                                                €
                                            @endif
                                        @elseif($totalWeight > 5 && $totalWeight <= 10)
                                            @if ($totalSize >= 0 && $totalSize <= 50)
                                                {{ number_format($viewData['order_header']->total + $iva + 6.9, 2) }} €
                                            @elseif($totalSize >= 50 && $totalSize <= 95)
                                                {{ number_format($viewData['order_header']->total + $iva + 6.9, 2) }} €
                                            @elseif($totalSize >= 95 && $totalSize <= 120)
                                                {{ number_format($viewData['order_header']->total + $iva + 6.9, 2) }} €
                                            @elseif($totalSize >= 120 && $totalSize <= 150)
                                                {{ number_format($viewData['order_header']->total + $iva + 10.9, 2) }}
                                                €
                                            @endif
                                        @elseif($totalWeight > 10 && $totalWeight <= 20)
                                            @if ($totalSize >= 0 && $totalSize <= 50)
                                                {{ number_format($viewData['order_header']->total + $iva + 10.9, 2) }}
                                                €
                                            @elseif($totalSize >= 50 && $totalSize <= 95)
                                                {{ number_format($viewData['order_header']->total + $iva + 10.9, 2) }}
                                                €
                                            @elseif($totalSize >= 95 && $totalSize <= 120)
                                                {{ number_format($viewData['order_header']->total + $iva + 10.9, 2) }}
                                                €
                                            @elseif($totalSize >= 120 && $totalSize <= 150)
                                                {{ number_format($viewData['order_header']->total + $iva + 10.9, 2) }}
                                                €
                                            @endif
                                        @else
                                            {{ number_format($viewData['order_header']->total + $iva + 2.9, 2) }} €
                                        @endif
                                    </span>
                                </div>
                            </div>
                        </li>
                    @endif
                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">{{ __('Estado') }}
                                </div>
                                <span class="text-ultra text-sm font-semibold">
                                    {{ __($viewData['order_header']->statusDes) }} </span>
                            </div>
                        </div>
                    </li>
                </ul>
                @if ($viewData['paid_out'] == 'N' && !in_array($viewData['order_status'], ['CN', 'CW', 'PC']))

                    @if ($showShippingSelect)

                        <div class="listview-title mt-2">@lang('messages.paid_order')</div>
                        <ul class="listview image-listview media mb-2">
                            <?php
                            $valor1 = number_format($viewData['order_header']->total, 2);
                            $valor2 = Auth::user()->cash;
                            $valor_special = Auth::user()->special_cash;
                            $comision = 3.4;
                            $resta = $valor1 - $valor2 - $valor_special;
                            $extra = $resta * $comision;
                            $comision_total = $extra / 100 + 0.35;
                            $total_pay = $resta + $comision_total;
                            ?>
                            @if ($total_pay > 0)
                                <li>
                                    <a href="#" id="showPaypal" class="showPaypal item">
                                        <div class="in">
                                            <div>
                                                @lang('messages.paid_paypal')
                                            </div>
                                            <span class="text-muted">{{ number_format($total_pay, 2) }} €</span>
                                        </div>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    @else
                        <div class="listview-title mt-2">@lang('messages.paid_order')
                            <svg width="20px" height="20px" data-html="true" data-toggle="popover"
                                data-content="
           El sistema te ofrecerá el mejor envío para tu pedido. <br />
           - Entre oficinas - Desde 2,90€ <br />
           - Entrega a domicilio - Desde 5,90€ <br />
           - Puedes escoger servicios extra en el momento de hacer el pago. <br />"
                                data-placement="top" data-trigger="hover" viewBox="0 0 76 76"
                                xmlns="http://www.w3.org/2000/svg">
                                <path id="greyshadow" d="M8 8h68v68h-68z" fill="#BFBFBF" />
                                <path id="blackborder" d="M4 4h68v68h-68z" fill="#000" />
                                <path id="background" d="M4 4h64v64h-64z" fill="#FFC07C" />
                                <path id="borderlefttop" d="M4 0h64M0 4v64" stroke="#DE5917" stroke-width="8" />
                                <path id="rivets" d="M8 8h4v4h-4zM60 60h4v4h-4zM8 60h4v4h-4zM60 8h4v4h-4z"
                                    fill="#000" />
                                <path id="questionshadow"
                                    d="M24 20h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM36 52h8v8h-8z"
                                    fill="#000" />
                                <path id="question"
                                    d="M20 16h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM32 48h8v8h-8z"
                                    fill="#DE5917" />
                            </svg>
                        </div>
                        <ul class="listview image-listview media mb-2">
                            <?php
                            $valor1 = number_format($viewData['order_header']->total, 2);
                            $valor2 = Auth::user()->cash;
                            $valor_special = Auth::user()->special_cash;
                            $comision = 3.4;
                            $resta = $valor1 - $valor2 - $valor_special;
                            $extra = $resta * $comision;
                            $comision_total = $extra / 100 + 0.35;
                            $total_pay = $resta + $comision_total;
                            ?>



                            <li>
                                <a href="#" id="showBeseif" class="showBeseif item">
                                    <div class="in">
                                        <div>
                                            <img src="https://files2.soniccdn.com/files/2021/03/16/beseif-logo.png"
                                                style="display: inline;">
                                        </div>
                                        <span class="text-muted">
                                            @if ($totalWeight >= 0 && $totalWeight <= 1 && $totalSize >= 0 && $totalSize <= 50)
                                                {{ number_format($viewData['order_header']->total + $iva + 2.9, 2) }} €
                                            @elseif($totalWeight > 1 && $totalWeight <= 5 && $totalSize <= 95)
                                                {{ number_format($viewData['order_header']->total + $iva + 4.9, 2) }} €
                                            @elseif($totalWeight > 5 && $totalWeight <= 10 && $totalSize <= 120)
                                                {{ number_format($viewData['order_header']->total + $iva + 6.9, 2) }} €
                                            @elseif($totalWeight > 10 && $totalWeight <= 20 && $totalSize <= 150)
                                                {{ number_format($viewData['order_header']->total + $iva + 10.9, 2) }} €
                                            @elseif($totalWeight >= 0 && $totalWeight <= 1)
                                                @if ($totalSize >= 0 && $totalSize <= 50)
                                                    {{ number_format($viewData['order_header']->total + $iva + 2.9, 2) }} €
                                                @elseif($totalSize >= 50 && $totalSize <= 95)
                                                    {{ number_format($viewData['order_header']->total + $iva + 4.9, 2) }} €
                                                @elseif($totalSize >= 95 && $totalSize <= 120)
                                                    {{ number_format($viewData['order_header']->total + $iva + 6.9, 2) }} €
                                                @elseif($totalSize >= 120 && $totalSize <= 150)
                                                    {{ number_format($viewData['order_header']->total + $iva + 10.9, 2) }}
                                                    €
                                                @endif
                                            @elseif($totalWeight > 1 && $totalWeight <= 5)
                                                @if ($totalSize >= 0 && $totalSize <= 50)
                                                    {{ number_format($viewData['order_header']->total + $iva + 4.9, 2) }} €
                                                @elseif($totalSize >= 50 && $totalSize <= 95)
                                                    {{ number_format($viewData['order_header']->total + $iva + 4.9, 2) }} €
                                                @elseif($totalSize >= 95 && $totalSize <= 120)
                                                    {{ number_format($viewData['order_header']->total + $iva + 6.9, 2) }} €
                                                @elseif($totalSize >= 120 && $totalSize <= 150)
                                                    {{ number_format($viewData['order_header']->total + $iva + 10.9, 2) }}
                                                    €
                                                @endif
                                            @elseif($totalWeight > 5 && $totalWeight <= 10)
                                                @if ($totalSize >= 0 && $totalSize <= 50)
                                                    {{ number_format($viewData['order_header']->total + $iva + 6.9, 2) }} €
                                                @elseif($totalSize >= 50 && $totalSize <= 95)
                                                    {{ number_format($viewData['order_header']->total + $iva + 6.9, 2) }} €
                                                @elseif($totalSize >= 95 && $totalSize <= 120)
                                                    {{ number_format($viewData['order_header']->total + $iva + 6.9, 2) }} €
                                                @elseif($totalSize >= 120 && $totalSize <= 150)
                                                    {{ number_format($viewData['order_header']->total + $iva + 10.9, 2) }}
                                                    €
                                                @endif
                                            @elseif($totalWeight > 10 && $totalWeight <= 20)
                                                @if ($totalSize >= 0 && $totalSize <= 50)
                                                    {{ number_format($viewData['order_header']->total + $iva + 10.9, 2) }}
                                                    €
                                                @elseif($totalSize >= 50 && $totalSize <= 95)
                                                    {{ number_format($viewData['order_header']->total + $iva + 10.9, 2) }}
                                                    €
                                                @elseif($totalSize >= 95 && $totalSize <= 120)
                                                    {{ number_format($viewData['order_header']->total + $iva + 10.9, 2) }}
                                                    €
                                                @elseif($totalSize >= 120 && $totalSize <= 150)
                                                    {{ number_format($viewData['order_header']->total + $iva + 10.9, 2) }}
                                                    €
                                                @endif
                                            @else
                                                {{ number_format($viewData['order_header']->total + $iva + 2.9, 2) }} €
                                            @endif
                                        </span>
                                    </div>
                                </a>
                            </li>

                        </ul>

                    @endif

                @endif



                @if ($viewData['order_header']->sent_on != null)
                    <div class="listview-title mt-2">@lang('messages.details_send')</div>
                    <ul class="listview image-listview">
                        <li>
                            <div class="item" bis_skin_checked="1">
                                <div class="in" bis_skin_checked="1">
                                    <div bis_skin_checked="1">{{ __('Fecha de envío') }}
                                    </div>
                                    <span class="text-muted text-sm font-semibold">
                                        {{ $viewData['order_header']->sent_on }}</span>
                                </div>
                            </div>
                        </li>
                        @if ($viewData['order_header']->status == 'DD')
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">{{ __('Fecha de entrega') }}
                                        </div>
                                        <span class="text-muted text-sm font-semibold">
                                            {{ $viewData['order_header']->delivered_on }}</span>
                                    </div>
                                </div>
                            </li>
                    </ul>
                @endif
                @if ($viewData['order_shipping'])
                    @if ($viewData['order_shipping']->certified == 'Yes')
                        <ul class="listview image-listview">
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">{{ __('Numero') }}
                                        </div>
                                        <span class="text-muted text-sm font-semibold">
                                            {{ $viewData['order_header']->tracking_number }}</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    @endif
                @endif


    @endif


    @if (in_array($viewData['order_header']->status, ['CW', 'PC']))



        @if ($viewData['order_header']->cancelOrder->solicited_by == 'B')
            <div class="listview-title mt-2">@lang('messages.soli_two')</div>
            <ul class="listview link-listview">
                <li>
                    <a href="#" id="cancelSoli" class="cancelSoli item">
                        @lang('messages.the_cancel_soli')
                        <span class="text-muted">@lang('messages.see_reason')</span>
                    </a>
                </li>
            </ul>
        @else
            @if ($viewData['order_header']->cancelOrder->answer == 'N')
            @else
                <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                <ul class="listview image-listview media">
                    <li>
                        <a href="#" id="cancelAccept" class="cancelAccept item">
                            <div class="in">
                                <div>
                                    @lang('messages.the_seller') <b>@lang('messages.want')</b> @lang('messages.cancelorder')
                                </div>
                                <span class="text-muted text-sm font-semibold">
                                    @lang('messages.the_see')
                                </span>
                            </div>
                        </a>
                    </li>
                </ul>
            @endif
        @endif

    @endif



















    @if (in_array($viewData['order_header']->status, ['CR', 'RP']))
        @if ((isset($viewData['order_header']->cancelOrder) ? $viewData['order_header']->cancelOrder->count() : 0) == 0)
            @php($dates = date('Y-m-d', strtotime($viewData['order_header']->created_on)))
            @php($prueba = date('Y-m-d', strtotime($dates . ' + 2 days')))
            @php($prueba2 = date('Y-m-d'))
            @if ($prueba <= $prueba2)
            @else
                <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                <ul class="listview image-listview media">
                    <li>
                        <a href="#" id="cancelPay" class="cancelPay item">
                            <div class="in">
                                <div>
                                    @lang('messages.cancel_the_sale')
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
            @endif
        @else
            @if ($viewData['order_header']->cancelOrder->solicited_by == 'B')
                @php($dates = date('Y-m-d', strtotime($viewData['order_header']->created_on)))
                @php($prueba = date('Y-m-d', strtotime($dates . ' + 2 days')))
                @php($prueba2 = date('Y-m-d'))
                @if ($prueba <= $prueba2)
                    <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                    <ul class="listview image-listview media">
                        <li>
                            <a href="#" id="cancelAutomatic" class="cancelAutomatic item">
                                <div class="in">
                                    <div>
                                        @lang('messages.cancel_automatic')
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                @endif
            @else
                <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                <ul class="listview image-listview media">
                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1"> @lang('messages.cancel_the_sale') </div>
                                <span class="text-muted"> <span class="text-red-900">@lang('messages.i_refuse')</span></span>
                            </div>
                        </div>
                    </li>
                </ul>
            @endif
        @endif
    @endif





    @if ($viewData['order_header']->extra)
        @if (in_array($viewData['order_header']->status, ['CR', 'RP']))
        @elseif(isset($viewData['order_header']->cancelOrder))

        @elseif($viewData['order_header']->status == 'ST')

        @elseif ($viewData['order_header']->extra->paid_out == 'Y' && $viewData['order_header']->extra->status == 'RE')
            @if ($viewData['order_header']->status == 'DD')
            @else
                <div class="listview-title mt-2">@lang('messages.soli_two')</div>
            @endif
        @endif
        @if ($viewData['order_header']->extra->paid_out == 'PE')
            @if ($viewData['order_header']->status == 'CN')
                <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                <ul class="listview image-listview media">
                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">@lang('messages.extra_soli_aprob_two') :<br> <span
                                        class="text-green-900">{{ number_format($viewData['order_header']->extra->total, 2) }}
                                        €</span></div>
                                <span class="text-muted"> <span
                                        class="text-red-900">@lang('messages.the_cancel_two')</span><br>@lang('messages.cash_return')</span>
                            </div>
                            </>
                    </li>
                </ul>
            @else
                @if ($viewData['order_header']->status == 'ST')
                    <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                @else
                @endif

                <ul class="listview image-listview media">
                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">@lang('messages.extra_soli_aprob_two') :<br> <span
                                        class="text-green-900">{{ number_format($viewData['order_header']->extra->total, 2) }}
                                        €</span></div>
                                <span class="text-muted"> <span class="text-red-900">@lang('messages.i_accept')</span><br>
                                    @lang('messages.wait_order')</span>
                            </div>
                            </>
                    </li>
                </ul>
            @endif
        @endif
        @if ($viewData['order_header']->extra->paid_out == 'P' && $viewData['order_header']->extra->status == 'RE')
            @if ($viewData['order_header']->status == 'CN')
                <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                <ul class="listview image-listview">
                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">@lang('messages.extra_soli_aprob_two') : <br>
                                    {{ number_format($viewData['order_header']->extra->total, 2) }} €
                                </div>
                                <span class="text-muted text-sm font-semibold">
                                    @lang('messages.cash_return')
                                </span>
                            </div>
                        </div>
                    </li>
                </ul>
            @else
                <ul class="listview image-listview">
                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">@lang('messages.extra_soli_aprob_two') : <br>
                                    {{ number_format($viewData['order_header']->extra->total, 2) }} €
                                </div>
                                <span class="text-muted text-sm font-semibold">
                                    @lang('messages.extra_soli_aprob_three') :<br>
                                    {{ $viewData['order_header']->seller->user_name }}
                                </span>
                            </div>
                        </div>
                    </li>
                </ul>
            @endif
        @endif
        @if ($viewData['order_header']->extra->paid_out == 'Y' && $viewData['order_header']->extra->status == 'RE')
            @if ($viewData['order_header']->status == 'DD')
                <div class="listview-title mt-2">@lang('messages.soli_two')</div>
            @else
            @endif
            <ul class="listview image-listview media">
                <li>
                    <div class="item" bis_skin_checked="1">
                        <div class="in" bis_skin_checked="1">
                            <div bis_skin_checked="1">@lang('messages.extra_soli_aprob_two') :<br> <span
                                    class="text-green-900">{{ number_format($viewData['order_header']->extra->total, 2) }}
                                    €</span></div>
                            <span class="text-muted"> <span class="text-green-900">@lang('messages.i_accept')</span></span>
                        </div>
                    </div>
                </li>
            </ul>
        @endif
        @if ($viewData['order_header']->extra->paid_out == 'N' && $viewData['order_header']->extra->status == 'RE')
            @if ($viewData['order_header']->status == 'DD')
                <div class="listview-title mt-2">@lang('messages.soli_two')</div>
            @elseif ($viewData['order_header']->status == 'RP')

            @elseif ($viewData['order_header']->status == 'ST')
                <div class="listview-title mt-2">@lang('messages.soli_two')</div>
            @elseif((isset($viewData['order_header']->cancelOrder) ? $viewData['order_header']->cancelOrder->count() : 0) == 0)

            @elseif ($viewData['order_header']->cancelOrder->answer == 'N')
                <div class="listview-title mt-2">@lang('messages.soli_two')</div>
            @elseif ($viewData['order_header']->status == 'CN')
                <div class="listview-title mt-2">@lang('messages.soli_two')</div>
            @else
            @endif
            <ul class="listview image-listview media">
                <li>
                    <div class="item" bis_skin_checked="1">
                        <div class="in" bis_skin_checked="1">
                            <div bis_skin_checked="1">@lang('messages.extra_soli_aprob_two') :<br> <span
                                    class="text-green-900">{{ number_format($viewData['order_header']->extra->total, 2) }}
                                    €</span></div>
                            <span class="text-muted"> <span
                                    class="text-red-900">@lang('messages.i_refuse')</span><br>@lang('messages.cash_return')</span>
                        </div>
                    </div>
                </li>
            </ul>
        @endif
    @else
        @if (in_array($viewData['order_header']->status, ['CN', 'RP', 'ST', 'DD']))
        @else
        @endif
    @endif



    @if ($viewData['order_header']->refund)
        @if ($viewData['order_header']->refund->paid_out == 'P' && $viewData['order_header']->refund->status == 'RF')
            @if ($viewData['order_header']->status == 'PC')
                <ul class="listview image-listview media">
                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">@lang('messages.i_refund') :<br> <span
                                        class="text-green-900">{{ number_format($viewData['order_header']->refund->total, 2) }}
                                        €</span></div>
                                <span class="text-muted"> <span
                                        class="text-red-900">{{ $viewData['order_header']->seller->user_name }}</span>
                                    <br>
                                    @lang('messages.i_waiting')</span>
                            </div>
                        </div>
                    </li>
                </ul>
            @elseif($viewData['order_header']->status == 'CN')
            @else
                <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                <ul class="listview image-listview media">
                    <li>
                        <a href="#" id="reemPaySoli" class="reemPaySoli item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">@lang('messages.i_refund') :<br> <span
                                        class="text-green-900">{{ number_format($viewData['order_header']->refund->total, 2) }}
                                        €</span></div>
                                <span class="text-muted"> <span
                                        class="text-red-900">{{ $viewData['order_header']->seller->user_name }}</span>
                                    <br>
                                    @lang('messages.i_waiting')</span>
                            </div>
                        </a>
                    </li>
                </ul>
            @endif
        @endif
        @if ($viewData['order_header']->refund->paid_out == 'Y' && $viewData['order_header']->refund->status == 'RF')
            <ul class="listview image-listview media">
                <li>
                    <div class="item" bis_skin_checked="1">
                        <div class="in" bis_skin_checked="1">
                            <div bis_skin_checked="1">@lang('messages.i_refund') :<br> <span
                                    class="text-green-900">{{ number_format($viewData['order_header']->refund->total, 2) }}
                                    €</span></div>
                            <span class="text-muted"> <span class="text-green-900">@lang('messages.i_accept')</span> <br>
                                @lang('messages.i_cash_add')
                            </span>
                        </div>
                    </div>
                </li>
            </ul>
        @endif
        @if ($viewData['order_header']->refund->paid_out == 'N' && $viewData['order_header']->refund->status == 'RF')
            <div class="listview-title mt-2">@lang('messages.soli_two')</div>
            <ul class="listview image-listview media">
                <li>
                    <div class="item" bis_skin_checked="1">
                        <div class="in" bis_skin_checked="1">
                            <div bis_skin_checked="1">@lang('messages.i_refund'):<br> <span
                                    class="text-green-900">{{ number_format($viewData['order_header']->refund->total, 2) }}
                                    €</span></div>
                            <span class="text-muted"><span class="text-uppercase text-red-900"> @lang('messages.i_refuse') </span>
                                <br>
                                @lang('messages.cash_return') </span>
                        </div>
                    </div>
                </li>
            </ul>
        @endif
    @endif


    @if ($viewData['order_header']->rating)
        @if ($viewData['order_header']->rating->processig > 0)
            <div class="listview-title mt-2">{{ __('Valoración') }}</div>
            <ul class="listview image-listview">
                <li>
                    <div class="item" bis_skin_checked="1">
                        <div class="in" bis_skin_checked="1">
                            <div bis_skin_checked="1">{{ __('Velocidad de procesamiento') }}
                            </div>
                            <span class="text-muted text-sm font-semibold">
                                {{ $viewData['order_header']->rating->pro }}</span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="item" bis_skin_checked="1">
                        <div class="in" bis_skin_checked="1">
                            <div bis_skin_checked="1">{{ __('Empaquetado') }}
                            </div>
                            <span class="text-muted text-sm font-semibold">
                                {{ $viewData['order_header']->rating->pac }}</span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="item" bis_skin_checked="1">
                        <div class="in" bis_skin_checked="1">
                            <div bis_skin_checked="1">{{ __('Descripción') }}
                            </div>
                            <span class="text-muted text-sm font-semibold">
                                {{ $viewData['order_header']->rating->des }}</span>
                        </div>
                    </div>
                </li>
            </ul>
            <ul class="listview image-listview">
                <li>
                    <div class="item">
                        <div class="in">
                            <div>
                                <header>{{ __('Comentario') }}</header>
                                @if ($viewData['order_header']->rating->description)
                                    {{ $viewData['order_header']->rating->description }}
                                @else
                                    {{ __('No posee') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        @else
            <div class="listview-title mt-2">{{ __('Valoración') }}</div>
            <ul class="listview image-listview">
                <li>
                    <a href="#" id="showRating" class="showRating item">

                        <div class="in">
                            <div>
                                {{ __('Sin Valorar') }}
                            </div>
                            <span class="text-muted">Calificar Pedido</span>
                        </div>
                    </a>
                </li>

            </ul>
        @endif
    @endif

    @if (isset($viewData['order_header']->cancelOrder))
        @if ($viewData['order_header']->cancelOrder->answer == 'Y' || $viewData['order_header']->cancelOrder->answer == 'N')
            <div class="listview-title mt-2">{{ __('Solicitud de Cancelacion') }}</div>
            <ul class="listview image-listview">
                <li>
                    <div class="item" bis_skin_checked="1">
                        <div class="in" bis_skin_checked="1">
                            <div bis_skin_checked="1">{{ __('Solicitado por :') }}</div>
                            <span class="text-muted text-sm font-semibold">
                                {{ $viewData['order_header']->cancelOrder->solicited_by == 'B' ? strtoupper($viewData['order_header']->buyer->user_name) : strtoupper($viewData['order_header']->seller->user_name) }}</span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="item" bis_skin_checked="1">
                        <div class="in" bis_skin_checked="1">
                            <div bis_skin_checked="1">{{ __('Razon :') }}</div>
                            <span
                                class="text-muted text-sm font-semibold">{{ $viewData['order_header']->cancelOrder->cause }}</span>
                        </div>
                    </div>
                </li>
            </ul>
            <ul class="listview image-listview">
                <li>
                    <div class="item">
                        <div class="in">
                            <div>
                                <header>{{ __('Respuesta :') }}</header>
                                <span class="text-muted text-sm font-semibold">
                                    {{ $viewData['order_header']->cancelOrder->reason }}
                                </span>
                            </div>
                            <span
                                class="text-muted">{{ $viewData['order_header']->cancelOrder->answer == 'Y' ? 'Si' : 'No' }}</span>
                        </div>
                    </div>
                </li>
            </ul>
        @endif
    @endif

    @if ($viewData['order_header']->status == 'RC')
    @else
        <div class="listview-title mt-2">{{ __('Detalles del pedido') }}</div>
    @endif



    @if (isset($viewData['order_details']))
        @php($contador = 1)
        <div class="listview-title mt-2" style="display: {{ $contador > 0 ? 'block' : 'none' }};"></div>

        <div class="row g-3" style="display: {{ $contador > 0 ? 'block' : 'none' }};">
            @foreach ($viewData['order_details'] as $detail)
                @if ($detail->inventory->title !== null)
                    <div class="col-12 col-md-6">
                        <div class="card weekly-product-card">
                            <div class="card-body d-flex align-items-center">
                                <div class="product-thumbnail-side">
                                    @if ($detail->inventory->images->first())
                                        <a href="{{ url('/uploads/inventory-images/' . $detail->inventory->images->first()->image_path) }}"
                                            class="fancybox product-thumbnail d-block" data-fancybox="RGM">
                                            <img src="{{ url('/uploads/inventory-images/' . $detail->inventory->images->first()->image_path) }}"
                                                alt="">
                                        </a>
                                    @else
                                        <a class="product-thumbnail d-block"><img
                                                src="{{ asset('img/art-not-found.jpg') }}" alt=""></a>
                                    @endif
                                </div>
                                <div class="product-description"><a class="product-title d-block" href="#">
                                        {{ str_limit($detail->inventory->title, 16) }} <br>
                                        @lang('messages.cants') : {{ $detail->quantity }}
                                    </a>
                                    <p class="sale-price">{{ number_format($detail->price, 2) }}
                                        €<span></span>
                                    </p>

                                    <div class="product-rating">
                                        @if ($detail->inventory->comments == '')
                                            <i class="fa-solid fa-circle-xmark" data-toggle="popover"
                                                data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                data-trigger="hover"></i>
                                        @else
                                            <span data-toggle="popover" data-html="true"
                                                data-content="{{ $detail->inventory->comments }}" data-placement="top"
                                                data-trigger="hover" class="badge-comment badge-info"><i
                                                    class="fa-solid fa-comment"></i>
                                            </span>
                                        @endif


                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-12 col-md-6">
                        <div class="card weekly-product-card">
                            <div class="card-body d-flex align-items-center">
                                <div class="product-thumbnail-side">
                                    @if ($detail->inventory->images->first())
                                        <a href="{{ url('/uploads/inventory-images/' . $detail->inventory->images->first()->image_path) }}"
                                            class="fancybox product-thumbnail d-block" data-fancybox="RGM">
                                            <img src="{{ url('/uploads/inventory-images/' . $detail->inventory->images->first()->image_path) }}"
                                                alt="">
                                        </a>
                                    @elseif (count($detail->product->images) > 0)
                                        <a href="{{ url('/images/' . $detail->product->images[0]->image_path) }}"
                                            class="fancybox product-thumbnail d-block" data-fancybox="RGM">
                                            <img src="{{ url('/images/' . $detail->product->images[0]->image_path) }}"
                                                alt="">
                                        </a>
                                    @else
                                        <a class="product-thumbnail d-block"><img
                                                src="{{ asset('img/art-not-found.jpg') }}" alt=""></a>
                                    @endif
                                </div>
                                <div class="product-description"><a class="product-title d-block"
                                        href="/product/{{ $detail->product->id }}">
                                        {{ str_limit($detail->product->name, 16) }} <br>
                                        {{ $detail->product->platform }} <br>
                                        {{ $detail->product->region }} <br>
                                        @lang('messages.cants') : {{ $detail->quantity }}
                                    </a>
                                    <p class="sale-price">{{ number_format($detail->price, 2) }}
                                        €<span></span>
                                    </p>

                                    @foreach (App\AppOrgCategory::whereIn('id', [1, 2, 3, 4, 172])->get() as $CatItem)
                                        @if ($detail->product->catid == $CatItem->id)
                                            @if ($CatItem->id == 1)
                                                <div class="product-rating">
                                                    <img width="15px" src="/{{ $detail->inventory->box }}"
                                                        data-toggle="popover"
                                                        data-content="@lang('messages.box') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $detail->inventory->cover }}"
                                                        data-toggle="popover"
                                                        data-content="@lang('messages.cover') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->cover_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $detail->inventory->manual }}"
                                                        data-toggle="popover"
                                                        data-content="@lang('messages.the_manual') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->manual_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $detail->inventory->game }}"
                                                        data-toggle="popover"
                                                        data-content="@lang('messages.game_status') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $detail->inventory->extra }}"
                                                        data-toggle="popover"
                                                        data-content="@lang('messages.the_extra') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </div>
                                                <div class="product-rating">

                                                    @if ($detail->inventory->comments == '')
                                                        <i class="fa-solid fa-circle-xmark" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    @else
                                                        <span data-toggle="popover" data-html="true"
                                                            data-content="{{ $detail->inventory->comments }}"
                                                            data-placement="top" data-trigger="hover"
                                                            class="badge-comment badge-info"><i
                                                                class="fa-solid fa-comment"></i>
                                                        </span>
                                                    @endif

                                                    &nbsp;
                                                    <span data-toggle="popover" data-html="true"
                                                        data-content="
                                                    <b>@lang('messages.state_inventory') :</b> <br />
                                                    @lang('messages.box') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }} <br />
                                                    @lang('messages.cover') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->cover_condition)) }} <br />
                                                    @lang('messages.the_manual') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->manual_condition)) }} <br />
                                                    @lang('messages.game_status') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }} <br />
                                                    @lang('messages.the_extra') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }} <br />"
                                                        data-placement="top" data-trigger="hover"
                                                        class="badge-inventory badge-info">ESTADO
                                                    </span>

                                                </div>
                                            @elseif($CatItem->id == 2)
                                                <div class="product-rating">
                                                    <img width="15px" src="/{{ $detail->inventory->box }}"
                                                        data-toggle="popover"
                                                        data-content="@lang('messages.box') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $detail->inventory->cover }}"
                                                        data-toggle="popover"
                                                        data-content="Interior - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->cover_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $detail->inventory->manual }}"
                                                        data-toggle="popover"
                                                        data-content="@lang('messages.the_manual') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->manual_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $detail->inventory->game }}"
                                                        data-toggle="popover"
                                                        data-content="@lang('messages.game_status') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $detail->inventory->extra }}"
                                                        data-toggle="popover"
                                                        data-content="@lang('messages.the_extra') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $detail->inventory->inside }}"
                                                        data-toggle="popover"
                                                        data-content="@lang('messages.inside') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->inside_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </div>
                                                <div class="product-rating">

                                                    @if ($detail->inventory->comments == '')
                                                        <i class="fa-solid fa-circle-xmark" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    @else
                                                        <span data-toggle="popover" data-html="true"
                                                            data-content="{{ $detail->inventory->comments }}"
                                                            data-placement="top" data-trigger="hover"
                                                            class="badge-comment badge-info"><i
                                                                class="fa-solid fa-comment"></i>
                                                        </span>
                                                    @endif

                                                    &nbsp;
                                                    <span data-toggle="popover" data-html="true"
                                                        data-content="
                                                <b>@lang('messages.state_inventory') :</b> <br />

                                                @lang('messages.box') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }} <br />
                                                @lang('messages.cover') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->cover_condition)) }} <br />
                                                @lang('messages.the_manual') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->manual_condition)) }} <br />
                                                @lang('messages.console') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }} <br />
                                                @lang('messages.the_extra') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }} <br />
                                                @lang('messages.inside') - {{ __(App\AppOrgUserInventory::getCondicionName($s->inside_condition)) }} <br />"
                                                        data-placement="top" data-trigger="hover"
                                                        class="badge-inventory badge-info">ESTADO
                                                    </span>

                                                </div>
                                            @else
                                                <div class="product-rating">
                                                    <img width="15px" src="/{{ $detail->inventory->box }}"
                                                        data-toggle="popover"
                                                        data-content="@lang('messages.box') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $detail->inventory->game }}"
                                                        data-toggle="popover"
                                                        data-content="@lang('messages.state_inventory') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $detail->inventory->extra }}"
                                                        data-toggle="popover"
                                                        data-content="@lang('messages.the_extra') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </div>
                                                <div class="product-rating">

                                                    @if ($detail->inventory->comments == '')
                                                        <i class="fa-solid fa-circle-xmark" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    @else
                                                        <span data-toggle="popover" data-html="true"
                                                            data-content="{{ $detail->inventory->comments }}"
                                                            data-placement="top" data-trigger="hover"
                                                            class="badge-comment badge-info"><i
                                                                class="fa-solid fa-comment"></i>
                                                        </span>
                                                    @endif

                                                    &nbsp;
                                                    <span data-toggle="popover" data-html="true"
                                                        data-content="
                                                <b>@lang('messages.state_inventory') :</b> <br />

                                                @lang('messages.box') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }} <br />
                                                @lang('messages.the_general') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }} <br />
                                                @lang('messages.the_extra') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }} <br />"
                                                        data-placement="top" data-trigger="hover"
                                                        class="badge-inventory badge-info">ESTADO
                                                    </span>

                                                </div>
                                            @endif
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    @endif



    @if (in_array($viewData['order_header']->status, ['ST', 'ON']))
        <div class="text-center">
            <form action="{{ route('ReceivedStatusTwo') }}" class="pt-3" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $viewData['order_header']->order_identification }}">
                <button type="submit" class="btn btn-danger w-100 font-bold">@lang('messages.mark_received')</button>
            </form>
        </div>
    @endif





    </div>
    </div>
    </div>

    <div class="modal fade" id="modalBeseif" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelFour"
        aria-hidden="true">
        <div class="modal-dialog beseif-form" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">

                        <iframe
                            src="https://beta.beseif.com/marketplace/pre_checkout?codigo={{ $jsonDatatwo['codigo'] }}&webview=true"
                            style="width: 100%; height: 550px; border-radius: 10px;"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalPaypal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog paypal-form" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title">@lang('messages.paid_paypal')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>
                                <ul class="listview image-listview">
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('ID') }}</div>
                                                <span
                                                    class="text-muted text-sm font-semibold">{{ $viewData['order_header']->order_identification }}</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('Vendedor') }}</div>
                                                <span
                                                    class="text-muted text-sm font-semibold">{{ $viewData['order_header']->seller->user_name }}</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('Artículos comprados') }}</div>
                                                <span
                                                    class="text-muted text-sm font-semibold">{{ $viewData['order_header']->quantity }}</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('Metodo') }} <br> @lang('messages.de_send')
                                                </div>
                                                <span class="text-muted text-sm font-semibold">
                                                    @if ($viewData['order_shipping'])
                                                        {{ $viewData['order_shipping']->name }} <br>

                                                        <b>@lang('messages.price'):
                                                            {{ number_format($viewData['order_header']->shipping_price, 2) }}
                                                        </b>
                                                    @else
                                                        -
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    @if ($viewData['order_header']->status == 'RP')
                                        <?php
                                        $valor1 = number_format($viewData['order_header']->total, 2);
                                        $valor2 = Auth::user()->cash;
                                        $valor_special = Auth::user()->special_cash;
                                        $comision = 3.4;
                                        $resta = $valor1 - $valor2 - $valor_special;
                                        $extra = $resta * $comision;
                                        $comision_total = $extra / 100 + 0.35;
                                        $total_pay = $resta + $comision_total;
                                        ?>
                                        <li>
                                            <div class="item" bis_skin_checked="1">
                                                <div class="in" bis_skin_checked="1">
                                                    <div bis_skin_checked="1">{{ __('Total a pagar en Paypal') }}
                                                        ({{ config('brcode.paypal_currency') }}) <br>
                                                    </div>
                                                    <span class="text-muted text-sm font-semibold">
                                                        @if ($total_pay < 0)
                                                            -
                                                        @else
                                                            {{ number_format($total_pay, 2, '.', '') }}
                                                        @endif

                                                    </span>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                </ul>
                            </form>
                            @if ($viewData['paid_out'] == 'N' && !in_array($viewData['order_status'], ['CN', 'CW', 'PC']))
                                <form
                                    action="{{ url('/process_checkout_new/' . $viewData['order_header']->order_identification) }}"
                                    method="post">
                                    {{ csrf_field() }}
                                    <button type="submit"
                                        class="btn btn-warning font-bold w-100">@lang('messages.the_payer')</button>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalCancelAutomatic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog the-automatic-form" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-2">
                            <h4 class="modal-title">@lang('messages.the_cancel_soli')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block">
                            <form
                                action="{{ url('/account/purchases/automatic/' . $viewData['order_header']->order_identification) }}"
                                method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="optradio" value="Y">

                                <input type="hidden" name="reason" value="Cancelación automatica">

                                <div class="wide-block pb-1 pt-2">
                                    <span class="font-semibold text-sm">

                                        <center>
                                            @lang('messages.seven_days')
                                        </center>
                                        <br>

                                        <div class="row">
                                            <div class="col-12">
                                                <button type="submit"
                                                    class="btn btn-danger font-bold w-100">@lang('messages.cancel')</button>
                                            </div>
                                        </div>
                                    </span>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="problemModal" tabindex="-1" role="dialog" aria-labelledby="problemModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title">¿Tienes algun problema con esta orden?</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <form action="{{ route('problem-order') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="id"
                                value="{{ $viewData['order_header']->order_identification }}">
                            <div class="wide-block pb-1 pt-2">
                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="address5">Describe lo que sucede</label>
                                        <textarea id="address5" rows="2" name="reason" required="" class="form-control"></textarea>
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>

                                </div>
                                <button type="submit" class="btn btn-danger font-bold w-100">ENVIAR</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



    @if ($viewData['order_header']->refund)
        @if ($viewData['order_header']->refund->paid_out == 'P' && $viewData['order_header']->refund->status == 'RF')
            <div class="modal fade" id="reemModalSoli" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog reem-soli-form" role="document">
                    <div class="modal-content bg-white">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="d-flex align-items-center justify-content-between mb-4">
                                    <h4 class="modal-title">@lang('messages.reem_soli')</h4>
                                    <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="wide-block pb-1 pt-2">
                                    <span class="font-semibold text-sm">

                                        <center> El usuario
                                            {{ $viewData['order_header']->seller->user_name }}
                                            @lang('messages.send_extra')
                                            <span class="text-green-900">
                                                {{ number_format($viewData['order_header']->refund->total, 2) }}
                                                €
                                            </span>
                                        </center>
                                        <br>

                                        <div class="row">
                                            <div class="col-6">
                                                <form action="{{ route('responseRefund') }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" value="Y" name="response">
                                                    <input type="hidden"
                                                        value="{{ $viewData['order_header']->order_identification }}"
                                                        name="order">

                                                    <button class="pt-2 pb-2 btn-success btn-block" type="submit"><span
                                                            class="retro text-white font-extrabold">@lang('messages.aceppt')</span></button>
                                                </form>
                                            </div>
                                            <div class="col-6">
                                                <form action="{{ route('responseRefund') }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" value="N" name="response">
                                                    <input type="hidden"
                                                        value="{{ $viewData['order_header']->order_identification }}"
                                                        name="order">
                                                    <button class="pt-2 pb-2 btn-danger btn-block" type="submit"><span
                                                            class="retro text-white font-extrabold">@lang('messages.recha_cash')</span></button>
                                                </form>
                                            </div>
                                        </div>
                                    </span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif






    <div class="modal fade" id="modalRgm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog rgm-form" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title">@lang('messages.the_payer_RGM')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>
                                <ul class="listview image-listview">
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('ID') }}</div>
                                                <span
                                                    class="text-muted text-sm font-semibold">{{ $viewData['order_header']->order_identification }}</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('Vendedor') }}</div>
                                                <span
                                                    class="text-muted text-sm font-semibold">{{ $viewData['order_header']->seller->user_name }}</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('Artículos comprados') }}</div>
                                                <span
                                                    class="text-muted text-sm font-semibold">{{ $viewData['order_header']->quantity }}</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('Metodo') }} <br> De Envío</div>
                                                <span class="text-muted text-sm font-semibold">
                                                    @if ($viewData['order_shipping'])
                                                        {{ $viewData['order_shipping']->name }} <br>

                                                        <b>@lang('messages.price'):
                                                            {{ number_format($viewData['order_header']->shipping_price, 2) }}
                                                        </b>
                                                    @else
                                                        -
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('Total a pagar') }}
                                                    ({{ config('brcode.paypal_currency') }}) <br>
                                                </div>
                                                <span class="text-muted text-sm font-semibold">
                                                    {{ number_format($viewData['order_header']->total, 2) }}
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </form>
                            <form action="{{ url('/cartCompleteCheck') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id_order"
                                    value="{{ $viewData['order_header']->order_identification }}">
                                {{ csrf_field() }}
                                <button type="submit"
                                    class="btn btn-danger font-bold w-100">{{ __('Pagar pedidos abiertos') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (in_array($viewData['order_header']->status, ['CW', 'PC']))
        @if ($viewData['order_header']->cancelOrder->solicited_by == 'B')
            <div class="modal fade" id="modalCancelSoli" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog soli-form" role="document">
                    <div class="modal-content bg-white">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="d-flex align-items-center justify-content-between mb-4">
                                    <h4 class="modal-title">@lang('messages.cancel_soli')</h4>
                                    <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="wide-block pb-1 pt-2">
                                    <center>@lang('messages.cancel_soli_aprob')</center>
                                    <br>
                                    <center><span class="text-lg font-extrabold">@lang('messages.the_reason')</span></center>

                                    <center>{{ $viewData['order_header']->cancelOrder->cause }}</center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="modal fade" id="modalAcceptCancel" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog accept-form" role="document">
                    <div class="modal-content bg-white">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="d-flex align-items-center justify-content-between mb-4">
                                    <h4 class="modal-title">@lang('messages.thecancel_order')</h4>
                                    <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="wide-block pb-1 pt-2">
                                    <center><span class="text-lg font-extrabold">@lang('messages.the_reason')</span></center>

                                    <center>{{ $viewData['order_header']->cancelOrder->cause }}</center>


                                    <form
                                        action="{{ url('/account/purchases/view/' . $viewData['order_header']->order_identification) }}"
                                        method="post">
                                        {{ csrf_field() }}
                                        <div class="d-md-none mb-4">
                                            <div class="row no-gutters align-items-center justify-content-end my-2"
                                                bis_skin_checked="1">
                                                <div class="col-12 pr-md-2 col-md text-center text-md-right"
                                                    bis_skin_checked="1">
                                                    <span class="personalInfo-light">
                                                        @lang('messages.order_accept_cancel')</span>
                                                </div>
                                                <div class="p-title-price text-center">
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-check mb-1">
                                                                <label>
                                                                    <input type="radio" class="nes-radio"
                                                                        name="optradio" value="Y" checked />
                                                                    <span class="retro">@lang('messages.yes')</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-check mb-1">
                                                                <label>
                                                                    <input type="radio" class="nes-radio"
                                                                        name="optradio" value="N" />
                                                                    <span class="retro">@lang('messages.no')</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group boxed">
                                            <div class="input-wrapper">
                                                <label class="form-label" for="address5">@lang('messages.add_comment')</label>
                                                <textarea id="address5" rows="2" name="reason" required="" class="form-control"></textarea>
                                                <i class="clear-input">
                                                    <ion-icon name="close-circle"></ion-icon>
                                                </i>
                                            </div>
                                        </div>

                                        <button type="submit"
                                            class="btn confirmclosed btn-danger font-bold w-100 close-modal">
                                            <span class="text-lg font-bold">{{ __('Guardar') }}</span>
                                        </button>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif


    <div class="modal fade" id="deliveryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog delivery-form" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-2">
                            <h4 class="modal-title">@lang('messages.confirm_delivery')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block">
                            <form action="{{ url('/account/purchases/complete') }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <h5 class="font-bold">
                                    <center>
                                        {{ __('Al confirmar la entrega, usted acepta que recibio el producto satisfactoriamente.') }}
                                    </center>
                                </h5>
                                <input type="hidden" name="id"
                                    value="{{ $viewData['order_header']->order_identification }}">

                                <div class="form-group boxed">
                                    <div class="input-wrapper mb-3">
                                        <label class="form-label" for="name5">@lang('messages.pass_company')</label>
                                        <input type="password" class="form-control" name="pass" id="pass"
                                            placeholder="Escribe tu contraseña" autocomplete="off">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                    <button type="submit"
                                        class="btn btn-danger font-bold w-100">{{ __('Confirmar entrega') }}</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalCancel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog cancel-form" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-2">
                            <h4 class="modal-title">@lang('messages.thecancel_order')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block">
                            <form
                                action="{{ url('/account/purchases/cancel/' . $viewData['order_header']->order_identification) }}"
                                method="post">
                                {{ csrf_field() }}
                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="address5">@lang('messages.thecancel_order')</label>
                                        <textarea name="reason" required rows="2" class="form-control"></textarea>
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-danger font-bold w-100">@lang('messages.sends')</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalExtraPay" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog extra-form" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-2">
                            <h4 class="modal-title">@lang('messages.extrapaid')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block">
                            <form action="{{ route('requestRefund') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id"
                                    value="{{ $viewData['order_header']->order_identification }}">
                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <input type="number" class="form-control" step="0.01" name="cash"
                                            placeholder="0.00">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>
                                <div class="nk-gap"></div>
                                <button type="submit" class="btn confirmclosed btn-danger w-100 close-modal">
                                    <span class="text-lg font-semibold">@lang('messages.soli_send')</span>

                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ($viewData['order_header']->rating)
        <div class="modal fade" id="modalRating" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog extra-form" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-2">
                                <h4 class="modal-title">@lang('messages.ratingorder')</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <form
                                action="{{ url('/account/purchases/view/' . $viewData['order_header']->order_identification . '/score') }}"
                                method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="wide-block">
                                    <div class="d-md-none mb-4">
                                        <div class="row no-gutters align-items-center justify-content-end my-2"
                                            bis_skin_checked="1">
                                            <div class="col-12 pr-md-2 col-md text-center text-md-right"
                                                bis_skin_checked="1">
                                                <span class="personalInfo-light">@lang('messages.speed_rating')</span>
                                            </div>
                                            <div class="p-title-price text-center">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-check mb-1">
                                                            <input required type="radio" class="form-check-input"
                                                                name="optionsProcess" id="optionsProcess1"
                                                                value="1"
                                                                {{ 1 == $viewData['rating']->processig ? 'checked="checked"' : '' }} />
                                                            <label class="form-check-label" for="optionsProcess1">
                                                                {{ __('Bueno') }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-check mb-1">
                                                            <input required type="radio" class="form-check-input"
                                                                name="optionsProcess" id="optionsProcess2"
                                                                value="2"
                                                                {{ 2 == $viewData['rating']->processig ? 'checked="checked"' : '' }} />
                                                            <label class="form-check-label" for="optionsProcess2">
                                                                {{ __('Neutral') }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-check mb-1">
                                                            <input required type="radio" class="form-check-input"
                                                                name="optionsProcess" id="optionsProcess3"
                                                                value="3"
                                                                {{ 3 == $viewData['rating']->processig ? 'checked="checked"' : '' }} />
                                                            <label class="form-check-label" for="optionsProcess3">
                                                                {{ __('Malo') }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-md-none mb-4">
                                        <div class="row no-gutters align-items-center justify-content-end my-2"
                                            bis_skin_checked="1">
                                            <div class="col-12 pr-md-2 col-md text-center text-md-right"
                                                bis_skin_checked="1">
                                                <span class="personalInfo-light">@lang('messages.package_seller')</span>
                                            </div>
                                            <div class="p-title-price text-center">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-check mb-1">
                                                            <input required type="radio" class="form-check-input"
                                                                name="optionsPackage" id="optionsPackage1"
                                                                value="1"
                                                                {{ 1 == $viewData['rating']->packaging ? 'checked="checked"' : '' }} />
                                                            <label class="form-check-label" for="optionsPackage1">
                                                                {{ __('Bueno') }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-check mb-1">
                                                            <input required type="radio" class="form-check-input"
                                                                name="optionsPackage" id="optionsPackage2"
                                                                value="2"
                                                                {{ 2 == $viewData['rating']->packaging ? 'checked="checked"' : '' }} />
                                                            <label class="form-check-label" for="optionsPackage2">
                                                                {{ __('Neutral') }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-check mb-1">
                                                            <input required type="radio" class="form-check-input"
                                                                name="optionsPackage" id="optionsPackage3"
                                                                value="3"
                                                                {{ 3 == $viewData['rating']->packaging ? 'checked="checked"' : '' }} />
                                                            <label class="form-check-label" for="optionsPackage3">
                                                                {{ __('Malo') }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="d-md-none mb-4">
                                        <div class="row no-gutters align-items-center justify-content-end my-2"
                                            bis_skin_checked="1">
                                            <div class="col-12 pr-md-2 col-md text-center text-md-right"
                                                bis_skin_checked="1">
                                                <span class="personalInfo-light">
                                                    @lang('messages.product_description')</span>
                                            </div>
                                            <div class="p-title-price text-center">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-check mb-1">
                                                            <input required type="radio" class="form-check-input"
                                                                name="optionsDescription" id="optionsDescription1"
                                                                value="1"
                                                                {{ 1 == $viewData['rating']->desc_prod ? 'checked="checked"' : '' }} />
                                                            <label class="form-check-label" for="optionsDescription1">
                                                                {{ __('Bueno') }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-check mb-1">
                                                            <input required type="radio" class="form-check-input"
                                                                name="optionsDescription" id="optionsDescription2"
                                                                value="2"
                                                                {{ 2 == $viewData['rating']->desc_prod ? 'checked="checked"' : '' }} />
                                                            <label class="form-check-label" for="optionsDescription2">
                                                                {{ __('Neutral') }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-check mb-1">
                                                            <input required type="radio" class="form-check-input"
                                                                name="optionsDescription" id="optionsDescription3"
                                                                value="3"
                                                                {{ 3 == $viewData['rating']->desc_prod ? 'checked="checked"' : '' }} />
                                                            <label class="form-check-label" for="optionsDescription3">
                                                                {{ __('Malo') }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="address5">@lang('messages.comment_rating')</label>
                                            <textarea id="address5" rows="2" name="description" class="form-control">{{ $viewData['rating']->description }}</textarea>
                                            <i class="clear-input">
                                                <ion-icon name="close-circle"></ion-icon>
                                            </i>
                                        </div>
                                    </div>

                                    <button type="submit"
                                        class="btn confirmclosed btn-danger font-bold w-100 close-modal">
                                        <span class="text-lg font-bold">{{ __('Guardar') }}</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@else
    <!-- Content Header (Page header) -->
    <div class="container">
        <div class="nk-gap-2"></div>
        <div class="nk-gap-2"></div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="nk-decorated-h-3">
                    <div class="section-title2">
                        <h3 class="retro font-extrabold">
                            {{ __('Detalle de compra') . ' - ' . $viewData['order_header']->order_identification }}
                            {{ $viewData['order_header']->status }} </b></h3>
                    </div>
                </h3>
            </div>
        </div><!-- /.row -->

        @if (session('status'))
            <h3 class="text-xl md:text-2x1">
                {{ session('status') }}
            </h3>
        @endif
        <div class="nk-gap"></div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <span class="retro">
                @include('partials.flash')
            </span>
            <div class="row">
                <div class="col-md-9">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h3 class="text-2xl font-extrabold text-gray-900 retro">@lang('messages.detail_order') <span
                                    class="text-main-1"> #</span></h3>
                        </div>
                        <div class="ibox-content">
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('ID') }}</span>
                                <span class="text-sm font-semibold">
                                    {{ $viewData['order_header']->order_identification }}
                            </div>
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('Vendedor') }}</span>
                                <span class="text-sm font-semibold">
                                    {{ $viewData['order_header']->seller->user_name }}
                            </div>
                            @if (in_array($viewData['order_header']->status, ['ST', 'CR', 'PD']))
                                @if ($viewData['order_header']->max_pay_out < date('Y-m-d H:i:s'))
                                    <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                        <span class="text-sm font-semibold uppercase">@lang('messages.not_recieved_order')</span>
                                        @if ($viewData['order_header']->shipping_status == 'No')
                                            <span class="text-sm font-semibold">@lang('messages.not_order')</span>
                                        @else
                                            <form action="{{ route('statusShippingReceived') }}" class="order-deny"
                                                method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="id"
                                                    value="{{ $viewData['order_header']->order_identification }}">

                                                <button type="submit"
                                                    class="p-3 border-b nk-btn-xs nk-btn-rounded nes-btn is-warning btn-block">
                                                    <span class="text-lg font-bold">@lang('messages.not_recieved_order_two')</span></button>
                                            </form>
                                        @endif
                                    </div>
                                @endif
                            @endif
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('Artículos comprados') }}</span>
                                <span class="text-sm font-semibold">
                                    {{ $viewData['order_header']->quantity }}
                            </div>
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('Metodo de envío') }}</span>
                                <span class="text-sm font-semibold">
                                    @if ($viewData['order_shipping'])
                                        {{ $viewData['order_shipping']->name }} <br>
                                        Certificado: {{ $viewData['order_shipping']->certified }}
                                        Peso Max: {{ $viewData['order_shipping']->max_weight }} g<br>
                                        Volume: L {{ $viewData['order_shipping']->large }} x W
                                        {{ $viewData['order_shipping']->weight }} x H
                                        {{ $viewData['order_shipping']->hight }}
                                        (mm)<br>
                                        <b>Precio: {{ number_format($viewData['order_header']->shipping_price, 2) }}
                                        </b>
                                    @else
                                        -
                                    @endif

                            </div>
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-base font-extrabold uppercase">{{ __('Total') }}
                                    ({{ config('brcode.paypal_currency') }}) </span>
                                <span class="text-base font-extrabold text-green-900 retro">
                                    {{ number_format($viewData['order_header']->total, 2) }}
                            </div>
                            @if ($viewData['order_header']->status == 'RP')
                                <?php
                                $valor1 = number_format($viewData['order_header']->total, 2);
                                $valor2 = Auth::user()->cash;
                                $valor_special = Auth::user()->special_cash;
                                $comision = 3.4;
                                $resta = $valor1 - $valor2 - $valor_special;
                                $extra = $resta * $comision;
                                $comision_total = $extra / 100 + 0.35;
                                $total_pay = $resta + $comision_total;
                                ?>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Total a pagar en Paypal') }}
                                        ({{ config('brcode.paypal_currency') }}) <br>
                                        <a href="#" target="_blank">
                                            <span class="text-xs font-bold"> - ¿Que significa esto? -</span>
                                        </a></span>
                                    <span class="text-base retro font-extrabold">

                                        {{ number_format($total_pay, 2, '.', '') }}
                                    </span>
                                </div>
                            @endif
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('Estado') }}</span>
                                <span class="text-base font-extrabold text-red-900 retro">
                                    {{ __($viewData['order_header']->statusDes) }}
                                </span>
                            </div>
                            @if ($viewData['paid_out'] == 'N' && !in_array($viewData['order_status'], ['CN', 'CW', 'PC']))
                                <h5 class="pb-8 text-xs font-semibold text-gray-900 retro">
                                    {{ __('Completar pago') }}
                                    -
                                    <span class="text-main-1">
                                        {{ $viewData['paid_out'] }} {{ $viewData['order_status'] }}</span>
                                </h5>

                                <label class="text-sm font-extrabold uppercase retro">@lang('messages.cash_rgm')</label>
                                <form action="{{ url('/cartCompleteCheck') }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id_order"
                                        value="{{ $viewData['order_header']->order_identification }}">
                                    <button type="submit"
                                        class="nk-btn btn-block font-extrabold nk-btn-md nk-btn-rounded nes-btn is-error retro nk-cancel-btn">{{ __('Pagar pedidos abiertos') }}</button>
                                </form>
                                <div class="nk-gap-2"></div>
                                <label class="text-sm font-extrabold uppercase retro">@lang('messages.also_cash')
                                </label>
                                <form
                                    action="{{ url('/process_checkout_new/' . $viewData['order_header']->order_identification) }}"
                                    method="post">
                                    {{ csrf_field() }}
                                    <button type="submit"
                                        class="retro text-base font-extrabold col-md-12 nes-btn is-warning"><img
                                            loading="lazy" src="{{ asset('img/icon/paypal.png') }}"> -
                                        @lang('messages.purchase_now')</button>
                                </form>
                            @endif

                            @if (
                                $viewData['order_header']->status == 'CR' ||
                                    $viewData['order_header']->status == 'ST' ||
                                    $viewData['order_header']->status == 'DD')
                                @if ($viewData['order_header']->refund)
                                    <br>
                                    @if ($viewData['order_header']->refund->paid_out == 'P' && $viewData['order_header']->refund->status == 'RE')
                                        <h5 class="pb-8 text-base font-semibold text-yellow-900 retro">
                                            @lang('messages.extra_soli_aprob')<b class="text-red-800 text-uppercase">
                                                {{ $viewData['order_header']->seller->user_name }}
                                            </b>
                                        </h5>
                                        <span class="font-semibold text-lg"> @lang('messages.send_soli_extra')
                                        </span>
                                        <span class="font-extrabold text-base text-green-900">
                                            {{ number_format($viewData['order_header']->refund->total, 2) }} €
                                        </span>
                                    @endif
                                    @if ($viewData['order_header']->refund->paid_out == 'Y' && $viewData['order_header']->refund->status == 'RE')
                                        <h5 class="pb-8 text-xs font-semibold retro"><span
                                                class="text-gray-900">SOLICITUD
                                                DE PAGO EXTRA - ACEPTADO POR - </span><b
                                                class="text-red-800 text-uppercase">
                                                {{ $viewData['order_header']->seller->user_name }}
                                            </b> <span class="text-gray-900"> Con un total de </span>
                                            <span class="font-semibold retro text-green-900">
                                                {{ number_format($viewData['order_header']->refund->total, 2) }} €
                                            </span>
                                        </h5>
                                    @endif
                                    @if ($viewData['order_header']->refund->paid_out == 'N' && $viewData['order_header']->refund->status == 'RE')
                                        <h5 class="pb-8 text-base font-semibold text-gray-900 retro">
                                            @lang('messages.soli_extra_sell') <b
                                                class="text-red-800 text-uppercase">{{ $viewData['order_header']->seller->user_name }}
                                            </b>
                                            @lang('messages.refuse_paid_extra')
                                            {{ number_format($viewData['order_header']->refund->total, 2) }} €.
                                        </h5>
                                        <span class="font-bold text-orange-900">
                                            @lang('messages.again_paid')
                                        </span>
                                    @endif
                                    @if ($viewData['order_header']->refund->paid_out == 'P' && $viewData['order_header']->refund->status == 'RF')
                                        <h5 class="text-center text-gray-900 retro text-xs">Solicitud de Reembolso</h5>
                                        <span class="font-semibold text-xs retro">
                                            <center>
                                                El usuario <b
                                                    class="text-uppercase text-red-900">{{ $viewData['order_header']->seller->user_name }}
                                                </b> envia un reembolso de
                                                <span
                                                    class="text-green-900">{{ number_format($viewData['order_header']->refund->total, 2) }}
                                                    € </span>
                                                <br>
                                            </center>
                                        </span>
                                        <div class="row">
                                            <div class="col-6">
                                                <form action="{{ route('responseRefund') }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" value="Y" name="response">
                                                    <input type="hidden"
                                                        value="{{ $viewData['order_header']->order_identification }}"
                                                        name="order">

                                                    <button class="nk-btn nk-btn-lg nes-btn btn-block"
                                                        type="submit"><span
                                                            class="text-gray-900 retro">Aceptar</span></button>
                                                </form>
                                            </div>
                                            <div class="col-6">
                                                <form action="{{ route('responseRefund') }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" value="N" name="response">
                                                    <input type="hidden"
                                                        value="{{ $viewData['order_header']->order_identification }}"
                                                        name="order">
                                                    <button class="nk-btn nk-btn-lg nes-btn btn-block"
                                                        type="submit"><span
                                                            class="text-gray-900 retro">@lang('messages.recha_cash')</span></button>
                                                </form>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($viewData['order_header']->refund->paid_out == 'Y' && $viewData['order_header']->refund->status == 'RF')
                                        <h5 class="pb-8 text-base text-center font-semibold text-gray-900 retro">
                                            @lang('messages.reem_soli')
                                            - <b class="text-red-800 text-uppercase">@lang('messages.aceept_two')
                                            </b>
                                        </h5>
                                        <center> <span class="font-bold retro text-xs"> @lang('messages.accept_reem')
                                            </span>
                                            <span class="font-bold retro text-xs text-green-900">
                                                {{ number_format($viewData['order_header']->refund->total, 2) }} €
                                            </span>
                                            <br>
                                            </span>
                                            <span class="font-bold text-green-900">

                                                @lang('messages.cash_aceppt_victory')
                                            </span>
                                        </center>
                                    @endif
                                    @if ($viewData['order_header']->refund->paid_out == 'N' && $viewData['order_header']->refund->status == 'RF')
                                        <h5 class="pb-8 text-base font-semibold text-gray-900 retro">
                                            @lang('messages.reem_soli')
                                            - <b class="text-red-800 text-uppercase">@lang('messages.recha_cash_two')
                                            </b>
                                        </h5>
                                        <span class="font-bold"> @lang('messages.recha_reem_valor')
                                            <br>
                                        </span>
                                        <span class="font-bold text-green-900">
                                            {{ number_format($viewData['order_header']->refund->total, 2) }} €
                                            @lang('messages.cash_remitent')
                                            {{ $viewData['order_header']->seller->user_name }}
                                        </span>
                                    @endif
                                @else
                                    <br>
                                    <h5 class="pb-8 text-xs font-extrabold text-gray-900">@lang('messages.extra_paid')
                                        -
                                        <span class="text-main-1">
                                            Max. 5€</span>
                                    </h5>
                                    <form action="{{ route('requestRefund') }}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id"
                                            value="{{ $viewData['order_header']->order_identification }}">
                                        <input type="number" name="cash" step="0.01" placeholder="0.00"
                                            class="nes-input">
                                        <div class="nk-gap"></div>
                                        <button type="submit"
                                            class="nk-btn nk-btn-lg nk-btn-rounded nes-btn btn-block">
                                            <font color="black"><span
                                                    class="text-lg font-extrabold">@lang('messages.soli_send')</span>
                                            </font>
                                        </button>
                                    </form>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>


                <div class="col-md-3">
                    @if ($viewData['order_header']->sent_on != null)
                        <div class="ibox">
                            <div class="ibox-title">
                                <h5 class="text-xs text-gray-900 retro">{{ __('Estado de envio') }}</h5>
                            </div>
                            <div class="ibox-content">
                                @if ($viewData['order_shipping'])
                                    @if ($viewData['order_shipping']->certified == 'Yes')
                                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                            <span class="text-sm font-semibold uppercase">{{ __('Numero') }}</span>
                                            <span class="text-sm font-semibold ">
                                                {{ $viewData['order_header']->tracking_number }}
                                            </span>
                                        </div>
                                    @endif
                                @endif
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Fecha de envío') }}</span>
                                    <span class="text-sm font-semibold ">
                                        <div id="orderID">{{ $viewData['order_header']->sent_on }} </div>
                                    </span>
                                </div>
                                @if ($viewData['order_header']->status == 'DD')
                                    <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                        <span
                                            class="text-sm font-semibold uppercase">{{ __('Fecha de entrega') }}</span>
                                        <span class="text-sm font-semibold ">
                                            <div id="orderID">{{ $viewData['order_header']->delivered_on }} </div>
                                        </span>
                                    </div>
                                @endif
                                @if (in_array($viewData['order_header']->status, ['ST', 'ON']))
                                    <form action="{{ url('/account/purchases/complete') }}" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                        <h5 class="pb-8 text-xs font-semibold text-gray-900 border-b retro">
                                            {{ __('Al confirmar la entrega, usted acepta que recibio el producto satisfactoriamente.') }}
                                        </h5><br>
                                        <input type="hidden" name="id"
                                            value="{{ $viewData['order_header']->order_identification }}">
                                        <label class="pb-8 text-xs font-semibold control-label retro"
                                            for="pass">Contraseña</label>
                                        <input type="password" class="nes-input" name="pass" id="pass"
                                            required>
                                        <div class="nk-gap"></div>
                                        <button type="submit" class="nes-btn btn-block"><span
                                                class="text-xs font-semibold retro">{{ __('Confirmar entrega') }}</span></button>
                                    </form>
                                @endif
                            </div>
                        </div>
                    @endif
                    @if (in_array($viewData['order_header']->status, ['CW', 'PC']))
                        <div class="ibox">
                            <div class="ibox-title">
                                <h5 class="text-xs text-gray-900 retro">@lang('messages.soli_two')</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="">

                                    @if ($viewData['order_header']->cancelOrder->solicited_by == 'B')
                                        <span class="font-bold">
                                            @lang('messages.cancel_soli_aprob')
                                        </span>
                                        <br>
                                        <br>
                                        <div class="ibox-title">
                                            <h5 class="text-xs text-gray-900 retro">@lang('messages.the_reason')</h5>
                                        </div>
                                        <p>{{ $viewData['order_header']->cancelOrder->cause }}</p>
                                    @else
                                        <form
                                            action="{{ url('/account/purchases/view/' . $viewData['order_header']->order_identification) }}"
                                            method="post">
                                            {{ csrf_field() }}
                                            <div class="col-lg-12 ">
                                                <label for="">@lang('messages.seller_cancel')</label>
                                                <label for="">@lang('messages.order_accept_cancel')</label>
                                                <br>
                                                <label>
                                                    <input type="radio" class="nes-radio" name="optradio"
                                                        value="Y" checked />
                                                    <span class="retro">Si</span>
                                                </label>

                                                <label>
                                                    <input type="radio" class="nes-radio" name="optradio"
                                                        value="N" />
                                                    <span class="retro">No</span>
                                                </label>
                                                @if ($errors->has('optradio'))
                                                    <br>
                                                    <div class="alert alert-danger" role="alert">
                                                        @foreach ($errors->get('optradio') as $error)
                                                            {{ $error }}
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </div>

                                            <div class="col-lg-12">
                                                <br>
                                                <label class="text-xs text-gray-900 retro">Razon</label>
                                                <textarea name="reason" required cols="30" rows="3"
                                                    class="nes-textarea @if ($errors->has('reason')) is-invalid @endif"></textarea>
                                                <div class="textarea-counter" data-text-max="350"
                                                    data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}">
                                                </div>
                                                @if ($errors->has('reason'))
                                                    <br>
                                                    <div class="alert alert-danger" role="alert">
                                                        @foreach ($errors->get('reason') as $error)
                                                            {{ $error }}
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-lg-12">
                                                <br>
                                                <button type="submit"
                                                    class="nk-btn btn-block nk-btn-md nk-btn-rounded nes-btn is-error nk-cancel-btn">@lang('messages.save')</button>
                                            </div>
                                        </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif

                    @if (in_array($viewData['order_header']->status, ['CR', 'RP']))
                        @if ((isset($viewData['order_header']->cancelOrder) ? $viewData['order_header']->cancelOrder->count() : 0) == 0)
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5 class="text-base font-extrabold text-gray-900 retro">@lang('messages.soli_two')</h5>
                                </div>
                                <div class="ibox-content">
                                    <form
                                        action="{{ url('/account/purchases/view/' . $viewData['order_header']->order_identification) }}"
                                        method="POST" class="delete-confirm">
                                        {{ csrf_field() }}
                                        <h5 class="text-sm font-extrabold text-gray-800 retro">
                                            @lang('messages.are_you_sure_purchase'):</h5>
                                        <label class="text-sm font-bold uppercase">@lang('messages.the_reason')</label>
                                        <textarea name="reason" class="nes-textarea" cols="30" rows="3" required></textarea>
                                        <div class="textarea-counter" data-text-max="350"
                                            data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></div>
                                        <button type="submit"
                                            class="nk-btn font-extrabold btn-block nk-btn-md nk-btn-rounded nes-btn is-error nk-cancel-btn">@lang('messages.sends')</button>
                                    </form>
                                </div>
                            </div>
                        @endif
                    @endif
                    @if ($viewData['order_header']->rating)
                        @if ($viewData['order_header']->rating->processig > 0)
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5 class="text-xs text-gray-900 retro">{{ __('Calificacion') }}</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                        <span class="text-sm font-semibold uppercase">{{ __('Rating') }}</span>
                                        <span class="text-sm font-semibold ">
                                            <div id="orderID"><i
                                                    class="{{ $viewData['order_header']->rating->ratingIcon }} text-{{ $viewData['order_header']->rating->ratingType }}"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="{{ $viewData['order_header']->rating->ratingStatus }}"></i>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                        <span
                                            class="text-sm font-semibold uppercase">{{ __('Velocidad de procesamiento') }}</span>
                                        <span class="text-sm font-semibold ">
                                            <div id="orderID">{{ $viewData['order_header']->rating->pro }} </div>
                                        </span>
                                    </div>
                                    <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                        <span class="text-sm font-semibold uppercase">{{ __('Empaquetado') }}</span>
                                        <span class="text-sm font-semibold ">
                                            <div id="orderID">{{ $viewData['order_header']->rating->pac }} </div>
                                        </span>
                                    </div>
                                    <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                        <span class="text-sm font-semibold uppercase">{{ __('Descripción') }}</span>
                                        <span class="text-sm font-semibold ">
                                            <div id="orderID">{{ $viewData['order_header']->rating->des }} </div>
                                        </span>
                                    </div>
                                    <div class="pb-8 mt-10 mb-10 border-b">
                                        <label class="text-sm font-semibold uppercase retro"><b>{{ __('Comentario') }}
                                                :</b></label>
                                        <br>
                                        <span class="text-sm font-semibold text-center">
                                            {{ $viewData['order_header']->rating->description }}
                                        </span>
                                    </div>
                                </div>
                            @else
                                <div class="ibox">
                                    <div class="ibox-title">
                                        <h5 class="text-xs text-gray-900 retro">{{ __('Calificacion') }}</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                            <span class="text-sm font-semibold uppercase">{{ __('Rating') }}</span>
                                            <span class="text-sm font-semibold ">
                                                <div id="orderID"> {{ __('Sin Calificar') }} </div>
                                            </span>
                                        </div>
                                        @if ($viewData['rating'])
                                            @if ($viewData['rating']->status == 0)
                                                <button class="nk-btn nk-btn-rounded nes-btn is-error btn-block"
                                                    data-toggle="modal"
                                                    data-target="#edit{{ $viewData['order_header']->order_identification }}">
                                                    {{ __('Calificar pedido') }}
                                                </button>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif



                    <br>







                    <div class="ibox">
                        <div class="ibox-title">
                            <h5 class="pb-8 text-xs font-semibold text-gray-900 retro">
                                {{ __('Solicitud de Cancelacion') }}
                            </h5>
                        </div>
                        <div class="ibox-content">
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('Solicitado por :') }}</span>
                                <span class="text-sm font-semibold ">
                                    {{ $viewData['order_header']->cancelOrder->solicited_by == 'B' ? strtoupper($viewData['order_header']->buyer->user_name) : strtoupper($viewData['order_header']->seller->user_name) }}
                                </span>
                            </div>
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('Razon :') }}</span>
                                <span class="text-sm font-semibold ">
                                    {{ $viewData['order_header']->cancelOrder->cause }}
                                </span>
                            </div>
                            <div class="pb-8 mt-10 mb-10 border-b">
                                <label class="text-sm font-semibold uppercase retro"><b>{{ __('Respuesta') }}
                                        :</b></label>
                                <br>
                                <span class="text-sm font-semibold text-center">
                                    {{ $viewData['order_header']->cancelOrder->answer == 'Y' ? 'Si' : 'No' }}
                                    -
                                    {{ $viewData['order_header']->cancelOrder->reason }}
                                </span>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>



    </div>



    </div>

    </body>
    <div class="nk-gap"></div>
    <div class="container">
        <div class="col-md-12">
            <h3 class="nk-decorated-h-3">
                <div class="section-title2">
                    <h3 class="retro">{{ __('Detalles del Pedido') }}</b></h3>
                </div>
            </h3>
        </div>
    </div>
    @if (count($viewData['order_details']) > 2)
        <div class="container px-12 py-12 bg-white scroller">
        @else
            <div class="container px-12 py-12 bg-white">
    @endif
    @if (isset($viewData['order_details']))
        @foreach (App\AppOrgCategory::whereIn('id', [1, 2, 3, 4, 172])->get() as $CatItem)
            @php($contador = 0)

            @foreach ($viewData['order_details'] as $cartItem)
                @if ($cartItem->product->catid == $CatItem->id)
                    @php($contador += 1)
                @endif
            @endforeach
            <div class="nk-gap"></div>
            <h5 class="text-gray-900 text-base font-extrabold mt-15 retro"
                style="display: {{ $contador > 0 ? 'block' : 'none' }};">
                <b>{{ __($CatItem->name) }}</b>

            </h5>
            <div class="wishlist-table table-responsive">
                <table style="display: {{ $contador > 0 ? 'table' : 'none' }}; width:100%;">
                    <thead>
                        @if ($CatItem->id == 1)
                            <tr class="bg-red-700" style="font-size:10px;">
                                <th class="product-cart-img">
                                    <span class="nobr"><i class="fa fa-image"></i></span>
                                </th>
                                <th class="product-name">
                                    <span class="nobr">@lang('messages.products')</span>
                                </th>
                                <th class="product-name">
                                    <span class="nobr">@lang('messages.box')</span>
                                </th>
                                <th class="product-price">
                                    <span class="nobr">@lang('messages.cover')</span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> Manual </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr">@lang('messages.game_product_inventory')</span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> Extra </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> <i class="fa fa-image"></i> </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> <i class="fa fa-comment"></i>
                                    </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> @lang('messages.cants') </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> @lang('messages.price') </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> Total </span>
                                </th>

                            </tr>
                        @elseif($CatItem->id == 2)
                            <tr class="bg-red-700" style="font-size:10px;">
                                <th class="product-cart-img">
                                    <span class="nobr"><i class="fa fa-image"></i></span>
                                </th>
                                <th class="product-name">
                                    <span class="nobr">@lang('messages.products')</span>
                                </th>
                                <th class="product-name">
                                    <span class="nobr">@lang('messages.box')</span>
                                </th>
                                <th class="product-price">
                                    <span class="nobr">@lang('messages.inside')</span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> Manual </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr">@lang('messages.console')</span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> Extra </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> Cables </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> <i class="fa fa-image"></i> </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> <i class="fa fa-comment"></i>
                                    </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr">@lang('messages.cants') </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr">@lang('messages.price') </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> Total </span>
                                </th>
                            </tr>
                        @else
                            <tr class="bg-red-700" style="font-size:10px;">
                                <th class="product-cart-img">
                                    <span class="nobr"><i class="fa fa-image"></i></span>
                                </th>
                                <th class="product-name">
                                    <span class="nobr">@lang('messages.products')</span>
                                </th>
                                <th class="product-name">
                                    <span class="nobr">@lang('messages.box')</span>
                                </th>
                                <th class="product-price">
                                    <span class="nobr">@lang('messages.state_inventory')</span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> Extra </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> <i class="fa fa-image"></i> </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> <i class="fa fa-comment"></i>
                                    </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr">@lang('messages.cants') </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr">@lang('messages.price') </span>
                                </th>
                                <th class="product-stock-stauts">
                                    <span class="nobr"> Total </span>
                                </th>
                            </tr>
                        @endif
                    </thead>
                    <tbody>
                        @foreach ($viewData['order_details'] as $detail)
                            @if ($detail->product->catid == $CatItem->id)
                                @if ($CatItem->id == 1)
                                    <tr>
                                        <td class="product-cart-img">

                                            @if (count($detail->product->images) > 0)
                                                <center>
                                                    <a href="/product/{{ $detail->product->id }}"><img width="70px"
                                                            height="70px"
                                                            src="{{ url('/images/' . $detail->product->images[0]->image_path) }}"></a>
                                                </center>
                                            @else
                                                <center>
                                                    <a href="/product/{{ $detail->product->id }}"><img width="70px"
                                                            height="70px"
                                                            src="{{ url('assets/images/art-not-found.jpg') }}"></a>
                                                </center>
                                            @endif
                                        </td>
                                        <td class="text-left">
                                            <a href="/product/{{ $detail->product->id }}" target="_blank">
                                                <span class="text-sm retro font-bold color-primary">
                                                    <font color="black"><b>{{ $detail->product->name }}
                                                            @if ($detail->product->name_en)
                                                                <br><small>{{ $detail->product->name_en }}</small>
                                                            @endif
                                                        </b>
                                                    </font>
                                                </span>
                                            </a>


                                            <br><span class="h7 text-secondary">{{ $detail->product->platform }}
                                                -
                                                {{ $detail->product->region }}</span>

                                        </td>

                                        <td class="product-name">
                                            <center>
                                                <img width="15px" src="/{{ $detail->inventory->box }}"
                                                    data-toggle="popover"
                                                    data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                    data-placement="top" data-trigger="hover">
                                            </center>

                                        </td>

                                        <td class="product-stock-status">
                                            <center>
                                                <img width="15px" src="/{{ $detail->inventory->cover }}"
                                                    data-toggle="popover"
                                                    data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->cover_condition)) }}"
                                                    data-placement="top" data-trigger="hover">
                                            </center>

                                        </td>

                                        <td class="product-stock-status">
                                            <center>
                                                <img width="15px" src="/{{ $detail->inventory->manual }}"
                                                    data-toggle="popover"
                                                    data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->manual_condition)) }}"
                                                    data-placement="top" data-trigger="hover">
                                            </center>

                                        </td>

                                        <td class="product-price">
                                            <center>
                                                <img width="15px" src="/{{ $detail->inventory->game }}"
                                                    data-toggle="popover"
                                                    data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                    data-placement="top" data-trigger="hover">
                                            </center>
                                        </td>

                                        <td class="product-price">
                                            <center>
                                                <img width="15px" src="/{{ $detail->inventory->extra }}"
                                                    data-toggle="popover"
                                                    data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                    data-placement="top" data-trigger="hover">
                                            </center>

                                        </td>
                                        <td class="text-center">

                                            @if ($detail->inventory->images->first())
                                                <a href="{{ '/uploads/inventory-images/' . $detail->inventory->images[0]->image_path }}"
                                                    data-fancybox="{{ $detail->product->id }}">
                                                    <img width="50px" height="50px"
                                                        src="{{ count($detail->inventory->images) > 0 ? url('/uploads/inventory-images/' . $detail->inventory->images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                </a>


                                                @foreach ($detail->inventory->images as $p)
                                                    @if (!$loop->first)
                                                        <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                            data-fancybox="{{ $detail->product->id }}">
                                                            <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                width="0px" height="0px"
                                                                style="position:absolute;" />
                                                        </a>
                                                    @endif
                                                @endforeach
                                            @else
                                                <center>
                                                    <font color="black">
                                                        <i class="fa fa-times" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    </font>
                                                </center>
                                            @endif
                                        </td>
                                        <td class="product-stock-status">
                                            @if ($detail->inventory->comments == '')
                                                <center>
                                                    <font color="black">
                                                        <i class="fa fa-times" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    </font>
                                                </center>
                                            @else
                                                <center>
                                                    <font color="black">
                                                        <i class="fa fa-comment" data-toggle="popover"
                                                            data-content="{{ $detail->inventory->comments }}"
                                                            data-placement="top" data-trigger="hover"></i>
                                                    </font>
                                                </center>
                                            @endif
                                        </td>

                                        <div class="retro">
                                            <td
                                                class="text-base text-center text-gray-900 retro font-extrabold color-primary text-nowrap">
                                                {{ $detail->quantity }}
                                            </td>
                                        </div>
                                        <td>
                                            <font color="green"> <span
                                                    class="text-base text-right retro font-extrabold color-primary text-nowrap"
                                                    data-value="{{ $detail->price }}">
                                                    {{ number_format($detail->price, 2) }}
                                                    € </span> </font>


                                        </td>
                                        <td>
                                            <font color="green"> <span
                                                    class="text-base text-right retro font-extrabold color-primary text-nowrap"
                                                    data-value="{{ $detail->price }}">
                                                    {{ number_format($detail->total, 2) }}
                                                    € </span> </font>


                                        </td>

                                    </tr>
                                @elseif($CatItem->id == 2)
                                    <tr>
                                        <td class="product-cart-img">

                                            @if (count($detail->product->images) > 0)
                                                <center>
                                                    <a href="/product/{{ $detail->product->id }}"><img width="70px"
                                                            height="70px"
                                                            src="{{ url('/images/' . $detail->product->images[0]->image_path) }}"></a>
                                                </center>
                                            @else
                                                <center>
                                                    <a href="/product/{{ $detail->product->id }}"><img width="70px"
                                                            height="70px"
                                                            src="{{ url('assets/images/art-not-found.jpg') }}"></a>
                                                </center>
                                            @endif
                                        </td>
                                        <td class="text-left">
                                            <a href="/product/{{ $detail->product->id }}" target="_blank">
                                                <span class="text-xs retro font-weight-bold color-primary small">
                                                    <font color="black"><b>{{ $detail->product->name }}
                                                            @if ($detail->product->name_en)
                                                                <br><small>{{ $detail->product->name_en }}</small>
                                                            @endif
                                                        </b>
                                                    </font>
                                                </span>
                                            </a>


                                            <br><span class="h7 text-secondary">{{ $detail->product->platform }}
                                                -
                                                {{ $detail->product->region }}</span>

                                        </td>
                                        <td class="product-name">
                                            <center>
                                                <img width="15px" src="/{{ $detail->inventory->box }}"
                                                    data-toggle="popover"
                                                    data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                    data-placement="top" data-trigger="hover">
                                            </center>

                                        </td>

                                        <td class="product-stock-status">
                                            <center>
                                                <img width="15px" src="/{{ $detail->inventory->cover }}"
                                                    data-toggle="popover"
                                                    data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->cover_condition)) }}"
                                                    data-placement="top" data-trigger="hover">
                                            </center>

                                        </td>

                                        <td class="product-stock-status">
                                            <center>
                                                <img width="15px" src="/{{ $detail->inventory->manual }}"
                                                    data-toggle="popover"
                                                    data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->manual_condition)) }}"
                                                    data-placement="top" data-trigger="hover">
                                            </center>

                                        </td>

                                        <td class="product-price">
                                            <center>
                                                <img width="15px" src="/{{ $detail->inventory->game }}"
                                                    data-toggle="popover"
                                                    data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                    data-placement="top" data-trigger="hover">
                                            </center>
                                        </td>

                                        <td class="product-price">
                                            <center>
                                                <img width="15px" src="/{{ $detail->inventory->extra }}"
                                                    data-toggle="popover"
                                                    data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                    data-placement="top" data-trigger="hover">
                                            </center>

                                        </td>
                                        <td class="product-price">
                                            <center>
                                                <img width="15px" src="/{{ $detail->inventory->inside }}"
                                                    data-toggle="popover"
                                                    data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->inside_condition)) }}"
                                                    data-placement="top" data-trigger="hover">
                                            </center>

                                        </td>
                                        <td class="text-center">
                                            @if ($detail->inventory->images->first())
                                                <a href="{{ '/uploads/inventory-images/' . $detail->inventory->images[0]->image_path }}"
                                                    data-fancybox="{{ $detail->product->id }}">
                                                    <img width="50px" height="50px"
                                                        src="{{ count($detail->inventory->images) > 0 ? url('/uploads/inventory-images/' . $detail->inventory->images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                </a>

                                                @foreach ($detail->inventory->images as $p)
                                                    @if (!$loop->first)
                                                        <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                            data-fancybox="{{ $detail->product->id }}">
                                                            <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                width="0px" height="0px"
                                                                style="position:absolute;" />
                                                        </a>
                                                    @endif
                                                @endforeach
                                            @else
                                                <center>
                                                    <font color="black">
                                                        <i class="fa fa-times" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    </font>
                                                </center>
                                            @endif
                                        </td>
                                        <td class="product-stock-status">
                                            @if ($detail->inventory->comments == '')
                                                <center>
                                                    <font color="black">
                                                        <i class="fa fa-times" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    </font>
                                                </center>
                                            @else
                                                <center>
                                                    <font color="black">
                                                        <i class="fa fa-comment" data-toggle="popover"
                                                            data-content="{{ $detail->inventory->comments }}"
                                                            data-placement="top" data-trigger="hover"></i>
                                                    </font>
                                                </center>
                                            @endif
                                        </td>

                                        <div class="retro">
                                            <td class="text-center retro h7">
                                                <font color="black">{{ $detail->quantity }}</font>
                                            </td>
                                        </div>
                                        <td>
                                            <font color="green"> <span
                                                    class="text-base text-right retro font-extrabold color-primary text-nowrap"
                                                    data-value="{{ $detail->price }}">
                                                    {{ number_format($detail->price, 2) }}
                                                    € </span> </font>


                                        </td>
                                        <td>
                                            <font color="green"> <span
                                                    class="text-base text-right retro font-extrabold color-primary text-nowrap"
                                                    data-value="{{ $detail->price }}">
                                                    {{ number_format($detail->total, 2) }}
                                                    € </span> </font>


                                        </td>

                                    </tr>
                                @else
                                    <tr>
                                        <td class="product-cart-img">

                                            @if (count($detail->product->images) > 0)
                                                <center>
                                                    <a href="/product/{{ $detail->product->id }}"><img width="70px"
                                                            height="70px"
                                                            src="{{ url('/images/' . $detail->product->images[0]->image_path) }}"></a>
                                                </center>
                                            @else
                                                <center>
                                                    <a href="/product/{{ $detail->product->id }}"><img width="70px"
                                                            height="70px"
                                                            src="{{ url('assets/images/art-not-found.jpg') }}"></a>
                                                </center>
                                            @endif
                                        </td>
                                        <td class="text-left">
                                            <a href="/product/{{ $detail->product->id }}" target="_blank">
                                                <span class="text-xs retro font-weight-bold color-primary small">
                                                    <font color="black"><b>{{ $detail->product->name }}
                                                            @if ($detail->product->name_en)
                                                                <br><small>{{ $detail->product->name_en }}</small>
                                                            @endif
                                                        </b>
                                                    </font>
                                                </span>
                                            </a>


                                            <br><span class="h7 text-secondary">{{ $detail->product->platform }}
                                                -
                                                {{ $detail->product->region }}</span>

                                        </td>
                                        <td class="product-name">
                                            <center>
                                                <img width="15px" src="/{{ $detail->inventory->box }}"
                                                    data-toggle="popover"
                                                    data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                    data-placement="top" data-trigger="hover">
                                            </center>

                                        </td>


                                        <td class="product-price">
                                            <center>
                                                <img width="15px" src="/{{ $detail->inventory->game }}"
                                                    data-toggle="popover"
                                                    data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                    data-placement="top" data-trigger="hover">
                                            </center>
                                        </td>

                                        <td class="product-price">
                                            <center>
                                                <img width="15px" src="/{{ $detail->inventory->extra }}"
                                                    data-toggle="popover"
                                                    data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                    data-placement="top" data-trigger="hover">
                                            </center>

                                        </td>
                                        <td class="text-center">
                                            @if ($detail->inventory->images->first())
                                                <a href="{{ '/uploads/inventory-images/' . $detail->inventory->images[0]->image_path }}"
                                                    data-fancybox="{{ $detail->product->id }}">
                                                    <img width="50px" height="50px"
                                                        src="{{ count($detail->inventory->images) > 0 ? url('/uploads/inventory-images/' . $detail->inventory->images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                </a>

                                                @foreach ($detail->inventory->images as $p)
                                                    @if (!$loop->first)
                                                        <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                            data-fancybox="{{ $detail->product->id }}">
                                                            <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                width="0px" height="0px"
                                                                style="position:absolute;" />
                                                        </a>
                                                    @endif
                                                @endforeach
                                            @else
                                                <center>
                                                    <font color="black">
                                                        <i class="fa fa-times" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    </font>
                                                </center>
                                            @endif
                                        </td>
                                        <td class="product-stock-status">
                                            @if ($detail->inventory->comments == '')
                                                <center>
                                                    <font color="black">
                                                        <i class="fa fa-times" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    </font>
                                                </center>
                                            @else
                                                <center>
                                                    <font color="black">
                                                        <i class="fa fa-comment" data-toggle="popover"
                                                            data-content="{{ $detail->inventory->comments }}"
                                                            data-placement="top" data-trigger="hover"></i>
                                                    </font>
                                                </center>
                                            @endif
                                        </td>

                                        <div class="retro">
                                            <td class="text-center retro h7">
                                                <font color="black">{{ $detail->quantity }}</font>
                                            </td>
                                        </div>
                                        <td>
                                            <font color="green"> <span
                                                    class="text-right retro font-weight-bold color-primary small text-nowrap"
                                                    data-value="{{ $detail->price }}">
                                                    {{ number_format($detail->price, 2) }}
                                                    € </span> </font>


                                        </td>
                                        <td>
                                            <font color="green"> <span
                                                    class="text-right retro font-weight-bold color-primary small text-nowrap"
                                                    data-value="{{ $detail->price }}">
                                                    {{ number_format($detail->total, 2) }}
                                                    € </span> </font>


                                        </td>

                                    </tr>
                                @endif
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endforeach
    @endif

    @if ($viewData['order_header']->status == 'ST')
        <div class="text-center">
            <form action="{{ route('ReceivedStatus') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $viewData['order_header']->order_identification }}">
                <button type="submit" class="btn btn-danger w-100 font-bold">MARCAR COMO RECIBIDO</button>
            </form>
        </div>
    @endif

    </div>
    @if ($viewData['rating'])
        @if ($viewData['rating']->status == 0)
            @include('brcode.front.private.modals.rating')
        @endif
    @endif

    @php($paypal = 0)
    @if (isset($viewData['total_paypal']))
        @php($paypal = $viewData['total_paypal'])
    @endif
    @endif
@endsection
@section('content-script-include')
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    <script src="{{ asset('brcode/js/pty.components.js') }}"></script>
    <script src="{{ asset('assets/input-mask/jquery.inputmask.bundle.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
@section('content-script')
    <script src="{{ asset('js/app.js') }}"></script>


    <script>
        function myFunction() {
            location.reload();
        }

        // Escuchar el evento de Broadcasting
        Echo.channel('payment')
            .listen('payment.complete', (data) => {
                // Actualizar la página
                myFunction();
            });
    </script>


    <script>
        $('.delete-confirm').submit(function(e) {
            e.preventDefault();

            Swal.fire({
                title: '<span class="text-base retro">Estas seguro de cancelar el pedido?</span>',
                text: "Tendras que esperar que el vendedor confirme la cancelacion",
                icon: 'warning',
                footer: '<a class="retro" href="/site/guia-de-compra" target="_blank">¿Como Comprar En RGM?</a>',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si,Seguro',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {

                    this.submit();
                }
            })
        })
    </script>
    <script>
        $('.order-deny').submit(function(e) {
            e.preventDefault();
            Swal.fire({
                title: '<span class="text-base retro">AVISO</span>',
                text: "Recuerde que al aceptar el pedido se marcara como 'no recibido' ",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si,Seguro',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {

                    this.submit();
                }
            })
        })
    </script>
    <script>
        $('.order-mobile').submit(function(e) {
            e.preventDefault();
            console.log('send');
            Swal.fire({
                title: '<span class="text-base retro">AVISO</span>',
                text: "Recuerde que al aceptar el pedido se marcara como 'no recibido'",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si,Seguro',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                console.log(result);
                if (result.value) {
                    this.submit();
                }
            })
        })
    </script>
    <script>
        $(document).ready(function() {
            $('.comments').popover();
            $('[data-toggle="popover"]').popover();
        });
    </script>
    <script></script>
    @if (
        $viewData['paid_out'] == 'N' &&
            $viewData['order_status'] != 'CN' &&
            Auth::user()->cash < number_format($viewData['total_paypal'], 2, '.', ''))
    @endif
@endsection
