@extends('brcode.front.layout.app')
@section('content-css-include')
    @livewireStyles
    <style>
        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

 

    </style>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-lg-center">

            <div class="col-lg-12">
                <ul class="nk-breadcrumbs">
                    <br>
                    <li>
                        <div class="section-title1">
                            <h3 class="retro font-extrabold" style="font-size:20px;">@lang('messages.my_purchases')</h3>
                        </div>
                    </li>
                </ul>
              
            </div>
        </div>
        <div class="nk-gap-1"></div>
        <span class="retro">@include('partials.flash')</span>
        <div class="col-lg-12">
            <div class="nk-gap"></div>

            <div class="nk-tabs">

                <ul class="nav nav-tabs" role="tablist">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp; 
                   
                    <li class="nav-item">
                        <a class="nav-link active" href="#tabs-1-1" role="tab" data-toggle="tab"><i
                                class="fa fa-warning"></i> <span class="retro-sell font-extrabold">{!! __('Sin pagar') !!}</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-2" role="tab" data-toggle="tab"> <i
                                class="fa fa-money"></i>
                            <span class="retro-sell font-extrabold"> {!! __('Pagado') !!}</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-3" role="tab" data-toggle="tab"><i
                                class="fa fa-plane"></i>
                            <span class="retro-sell font-extrabold">{!! __('Enviado') !!}</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-4" role="tab" data-toggle="tab"><i
                                class="fa fa-envelope-open-o "></i> <span
                                class="retro-sell font-extrabold">{!! __('Entregado') !!}</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-5" role="tab" data-toggle="tab"><i
                                class="fa fa-times "></i> <span class="retro-sell font-extrabold">{!! __('No entregado') !!}</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-6" role="tab" data-toggle="tab"><i
                                class="fa fa-ban"></i>
                            <span class="retro-sell font-extrabold">{!! __('Cancelado') !!}</span></a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade show active" id="tabs-1-1">
                        <div class="nk-gap"></div>
                        @if ($viewData['unpaid']->count() > 0)
                            <livewire:profile.purchase.unpaid-purchase>
                            </livewire:profile.purchase.unpaid-purchase>
                        @else
                            <div class="container">
                                <div class="nk-gap-3"></div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <blockquote class="nk-blockquote">
                                            <div class="nk-blockquote-icon"></div>
                                            <div class="text-center nk-blockquote-content h4 retro-not-inventory">
                                                <font color="black">
                                                    @lang('messages.not_found_registers')
                                                </font>
                                            </div>
                                            <div class="nk-gap"></div>
                                            <div class="nk-blockquote-author"></div>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        @endif

                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tabs-1-2">
                        <div class="nk-gap"></div>
                        @if ($viewData['paid']->count() > 0)
                            <livewire:profile.purchase.paid-purchase>
                            </livewire:profile.purchase.paid-purchase>
                        @else
                            <div class="container">
                                <div class="nk-gap-3"></div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <blockquote class="nk-blockquote">
                                            <div class="nk-blockquote-icon"></div>
                                            <div class="text-center nk-blockquote-content h4 retro-not-inventory">
                                                <font color="black">
                                                    @lang('messages.not_found_registers')
                                                </font>
                                            </div>
                                            <div class="nk-gap"></div>
                                            <div class="nk-blockquote-author"></div>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        @endif

                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tabs-1-3">
                        <div class="nk-gap"></div>
                        @if ($viewData['sent']->count() > 0)
                            <livewire:profile.purchase.sent-purchase>
                            </livewire:profile.purchase.sent-purchase>

                        @else
                            <div class="container">
                                <div class="nk-gap-3"></div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <blockquote class="nk-blockquote">
                                            <div class="nk-blockquote-icon"></div>
                                            <div class="text-center nk-blockquote-content h4 retro-not-inventory">
                                                <font color="black">
                                                    @lang('messages.not_found_registers')
                                                </font>
                                            </div>
                                            <div class="nk-gap"></div>
                                            <div class="nk-blockquote-author"></div>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        @endif

                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tabs-1-4">
                        <div class="nk-gap"></div>
                        @if ($viewData['arrived']->count() > 0)
                            <livewire:profile.purchase.arrived-purchase>
                            </livewire:profile.purchase.arrived-purchase>

                        @else
                            <div class="container">
                                <div class="nk-gap-3"></div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <blockquote class="nk-blockquote">
                                            <div class="nk-blockquote-icon"></div>
                                            <div class="text-center nk-blockquote-content h4 retro-not-inventory">
                                                <font color="black">
                                                    @lang('messages.not_found_registers')
                                                </font>
                                            </div>
                                            <div class="nk-gap"></div>
                                            <div class="nk-blockquote-author"></div>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tabs-1-5">
                        <div class="nk-gap"></div>
                        @if ($viewData['notArrived']->count() > 0)
                            <livewire:profile.purchase.not-arrived-purchase>
                            </livewire:profile.purchase.not-arrived-purchase>

                        @else
                            <div class="container">
                                <div class="nk-gap-3"></div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <blockquote class="nk-blockquote">
                                            <div class="nk-blockquote-icon"></div>
                                            <div class="text-center nk-blockquote-content h4 retro-not-inventory">
                                                <font color="black">
                                                    @lang('messages.not_found_registers')
                                                </font>
                                            </div>
                                            <div class="nk-gap"></div>
                                            <div class="nk-blockquote-author"></div>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        @endif

                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tabs-1-6">
                        <div class="nk-gap"></div>
                        @if ($viewData['cancelled']->count() > 0)
                            <livewire:profile.purchase.cancelled-purchase>
                            </livewire:profile.purchase.cancelled-purchase>

                        @else
                            <div class="container">
                                <div class="nk-gap-3"></div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <blockquote class="nk-blockquote">
                                            <div class="nk-blockquote-icon"></div>
                                            <div class="text-center nk-blockquote-content h4 retro-not-inventory">
                                                <font color="black">
                                                    @lang('messages.not_found_registers')
                                                </font>
                                            </div>
                                            <div class="nk-gap"></div>
                                            <div class="nk-blockquote-author"></div>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>


    </div>


@endsection
@section('content-script-include')
@endsection
@section('content-script')
    @livewireScripts
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $('.delete-confirm').submit(function(e) {
            e.preventDefault();
            Swal.fire({
                title: '<span class="text-base retro">¿Estas seguro de cancelar la compra?</span>',
                text: "",
                icon: 'warning',
                footer: '<center><a class="retro" href="">¿Tienes alguna duda? Consulta Nuestro FAQ</a></center>',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si,Seguro',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.isConfirmed) {
                    this.submit();
                }
            })
        })
    </script>
    <script>
        Livewire.on('alert', function() {
            $('.delete-confirm').submit(function(e) {
                e.preventDefault();
                Swal.fire({
                    title: '<span class="text-base retro">¿Estas seguro de cancelar la compra?</span>',
                    text: "",
                    icon: 'warning',
                    footer: '<center><a class="text-xs retro" href="">¿Tienes alguna duda? Consulta Nuestro FAQ</a></center>',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si,Seguro',
                    cancelButtonText: 'No'
                }).then((result) => {
                    if (result.isConfirmed) {
                        this.submit();
                    }
                })
            })
        })
    </script>
    <script>
        $('th').click(function() {
            var table = $(this).parents('table').eq(0)
            var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
            this.asc = !this.asc
            if (!this.asc) {
                rows = rows.reverse()
            }
            for (var i = 0; i < rows.length; i++) {
                table.append(rows[i])
            }
        })

        function comparer(index) {
            return function(a, b) {
                var valA = getCellValue(a, index),
                    valB = getCellValue(b, index)
                return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB)
            }
        }

        function getCellValue(row, index) {
            return $(row).children('td').eq(index).text()
        }
    </script>

    <script>
        $(document).ready(function() {
            if ('<?= session('tab') ?>' == '05') {
                $('a[href*="#tabs-1-5"]').click();
            }
        });
    </script>
@endsection
