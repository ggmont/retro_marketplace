@extends('brcode.front.layout.app')
@section('content-css-include')
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <!-- Main content -->

    <div class="container">
        <div class="nk-gap-2"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="section-title1">
                <h3 class="nk-decorated-h-3">{{ __('Mi carrito') }}</h3>
                </div>
            </div>
        </div><!-- /.row -->
        <div class="nk-gap"></div>



        @if ($viewData['order_header']->rating)
                    
                   
        <div id="summary" class="w-1/4 px-8 py-10 bg-white">
            <h5 class="pb-8 text-xs font-semibold border-b retro">{{ __('Estado de calificación') }} </h5>
            @if ($viewData['order_header']->rating->processig > 0)
            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                <span class="text-sm font-semibold uppercase">{{ __('Rating') }}</span>
                <span class="text-sm font-semibold ">
                    <div id="orderID"><i
                        class="{{ $viewData['order_header']->rating->ratingIcon }} text-{{ $viewData['order_header']->rating->ratingType }}"
                        data-toggle="tooltip" data-placement="top"
                        title="{{ $viewData['order_header']->rating->ratingStatus }}"></i> </div>
                </span>
            </div>
            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                <span class="text-sm font-semibold uppercase">{{ __('Velocidad de procesamiento') }}</span>
                <span class="text-sm font-semibold ">
                    <div id="orderID">{{ $viewData['order_header']->rating->pro }} </div>
                </span>
            </div>
            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                <span class="text-sm font-semibold uppercase">{{ __('Empaquetado') }}</span>
                <span class="text-sm font-semibold ">
                    <div id="orderID">{{ $viewData['order_header']->rating->pac }} </div>
                </span>
            </div>
            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                <span class="text-sm font-semibold uppercase">{{ __('Descripción') }}</span>
                <span class="text-sm font-semibold ">
                    <div id="orderID">{{ $viewData['order_header']->rating->des }} </div>
                </span>
            </div>
            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                <span class="text-sm font-semibold uppercase">{{ __('Comment') }}</span>
                <span class="text-sm font-semibold ">
                    <div id="orderID">{{ $viewData['order_header']->rating->description }} </div>
                </span>
            </div>
            @else
            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                <span class="text-sm font-semibold uppercase">{{ __('Rating') }}</span>
                <span class="text-sm font-semibold ">
                    <div id="orderID"> {{ __('Sin Calificar') }} </div>
                </span>
            </div>
            @if ($viewData['rating'])
            @if ($viewData['rating']->status == 0)
                <a class="nk-btn nk-btn-rounded nk-btn-color-main-1 btn-block"
                    href="{{ url('/account/purchases/view/' . $viewData['order_header']->order_identification . '/score') }}">{{ __('Calificar pedido') }}</a>
            @endif
        @endif
        @endif
        @endif
            
        </div>





        @if ($viewData['cart_new']->details->count() > 0)
            @php($total1 = 0)
                @php($art = 0)
                    @foreach ($viewData['products_in_cart'] as $cartItem)
                        @php($total1 += $cartItem->price * $cartItem->qty)
                            @php($art += $cartItem->qty)
                            @endforeach
                            <div class="row" id="cart-order-user-detail">
                                <div class="col-md-6">
                                    <!-- START: Cart Totals -->
                                    <h4 class="nk-title h5">{{ __('Carrito de compras') }} </h4>
                                    <table class="nk-table nk-table-sm">
                                        <tbody>
                                            <tr>
                                                <td style="width:70%">{{ __('Pedidos') }}</td>
                                                <td>
                                                    <div id="orderID">{{ count($us) }} </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>{{ __('Artículos') }}</td>
                                                <td>
                                                    <div id="articlesID">{{ $art }}</div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>{{ __('Valor total de los ítems') }}</td>
                                                <td>
                                                    <div id="subTotalID">{{ number_format(Auth::user()->cart->totalCart, 2) }} €</div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>{{ __('Envío') }}</td>
                                                <td>
                                                    <div id="shippingID">{{ number_format(Auth::user()->cart->totalShipping, 2) }} €</div>
                                                </td>
                                            </tr>
                                            @if (Auth::user()->special_cash > 0)
                                                <tr>
                                                    <td>{{ __('Saldo especial') }}</td>
                                                    <td>
                                                        <div id="shippingID"> - {{ number_format(Auth::user()->special_cash, 2) }} €</div>
                                                    </td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td><b>Total</b></td>
                                                @php($total = Auth::user()->cart->totalCart + Auth::user()->cart->totalShipping -
                                                    (Auth::user()->special_cash > 0 ? Auth::user()->special_cash : 0))
                                                    <td><b>
                                                            <div id="totalID">{{ number_format($total, 2) }} €</div>
                                                        </b></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <!-- END: Cart Totals -->
                                    </div>
                                    @if (Auth::id() > 0)
                                        <div class="col-md-6">
                                            <!-- START: Cart Totals -->
                                            <h4 class="nk-title h5">{{ __('Direccíon de envío') }} </h4>
                                            <table class="nk-table nk-table-sm">
                                                <tbody>
                                                    <tr>
                                                        <td><strong class="text-main-1">{{ __('Para') }}: </strong>
                                                            {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td> <strong class="text-main-1">{{ __('Dirección') }}: </strong>
                                                            @if (!empty(Auth::user()->address))
                                                                {{ Auth::user()->address }}
                                                            @else
                                                                None
                                                            @endif
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td> <strong class="text-main-1">{{ __('Código postal') }}: </strong>
                                                            @if (!empty(Auth::user()->zipcode))
                                                                {{ Auth::user()->zipcode }}
                                                            @else
                                                                None
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong class="text-main-1">{{ __('País') }}: </strong>
                                                            {{ Auth::user()->country->name }} </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <a class="nk-btn nk-btn-xs nk-btn-rounded nk-btn-color-white"
                                                                href="/account/profile#available">{{ __('Cambiar dirección de envío') }}</a>

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>


                                            <!-- END: Cart Totals -->
                                        </div>
                                    @endif
                                </div>
                                <div class="nk-gap-2"></div>
                                <div class="row">

                                    <div class="col-lg-12 col-md-12">
                                        <div class="nk-box-2 bg-dark-2">
                                            <h4>{{ __('Checkout') }}</h4>
                                            @if (App\SysSettings::where('type', 'Compras')->first())
                                                @php($c = App\SysSettings::where('type', 'Compras')->first())
                                                    @if ($c->value == 0)
                                                        <h5 class="text-center">Las compras fueron desabilitadas temporalmente</h5>
                                                    @else
                                                        <form action="{{ url('/cartComplete') }}" method="post">
                                                            @if (Auth::id() > 0)

                                                                <div class="col-md-12">
                                                                    @if (!empty($mes))
                                                                        <div class="alert alert-danger alert-dismissible">
                                                                            <a href="#" class="close" data-dismiss="alert"
                                                                                aria-label="close">&times;</a>
                                                                            <strong>Error!</strong> {{ $mes }}
                                                                        </div>
                                                                    @endif
                                                                    {!! __('Por favor revise el contenido de su carrito de compras. Luego ingrese su contraseña en la casilla de verificación y haga clic en <b> Comprometer para comprar </b>. <br> Entonces podrás elegir tu método de pago.') !!}


                                                                </div>
                                                            @endif

                                                            {{ csrf_field() }}
                                                            <div class="nk-gap"></div>
                                                            <div class="col-lg-6 nk-mchimp nk-form nk-form-style-1 validate">
                                                                <label
                                                                    for="">{{ __('Por favor ingrese su contraseña para confirmar legalmente su pedido.') }}</label>
                                                                <input type="password" name="pass" class="form-control" placeholder="Password"
                                                                    required><br>
                                                            </div>
                                                            <div class="col-md-6"><button type="submit"
                                                                    class="nk-btn nk-btn-xs nk-btn-rounded nk-btn-color-white btn-block">{{ __('Comprometerse a comprar') }}</button>
                                                            </div>
                                                        </form>
                                                    @endif
                                                @else
                                                    <form action="{{ url('/cartComplete') }}" method="post">
                                                        @if (Auth::id() > 0)

                                                            <div class="col-md-12">
                                                                @if (!empty($mes))
                                                                    <div class="alert alert-danger alert-dismissible">
                                                                        <a href="#" class="close" data-dismiss="alert"
                                                                            aria-label="close">&times;</a>
                                                                        <strong>Error!</strong> {{ $mes }}
                                                                    </div>
                                                                @endif
                                                                {!! __('Por favor revise el contenido de su carrito de compras. Luego ingrese su contraseña en la casilla de verificación y haga clic en <b> Comprometer para comprar </b>. <br> Entonces podrás elegir tu método de pago.') !!}


                                                            </div>
                                                        @endif

                                                        {{ csrf_field() }}
                                                        <div class="nk-gap"></div>
                                                        <div class="col-lg-6 nk-mchimp nk-form nk-form-style-1 validate">
                                                            <label
                                                                for="">{{ __('Por favor ingrese su contraseña para confirmar legalmente su pedido.') }}</label>
                                                            <input type="password" name="pass" class="form-control" placeholder="Password"
                                                                required><br>
                                                        </div>
                                                        <div class="col-md-6"><button type="submit"
                                                                class=" nk-btn-rounded nk-btn-color-white btn-block">{{ __('Comprometerse a comprar') }}</button>
                                                        </div>
                                                    </form>
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                    <div class="nk-gap-2"></div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3 class="nk-decorated-h-3">
                                                {!! __('<span class = "text-main-1">Detalles</span> del carrito') !!}</h3>

                                        </div>
                                        @php($order_num = 0)
                                            @if (count($viewData['products_in_cart']) > 0)
                                                @foreach ($viewData['user_order'] as $key)
                                                    @php($qty_det = 0)
                                                        @php($det_total = 0)
                                                            @php($det_vol = 0)
                                                                @php($det_wei = 0)

                                                                    @foreach ($viewData['products_in_cart'] as $cartItem)
                                                                        @if ($cartItem->user->user_name == $key->user_name)
                                                                            @php($qty_det += $cartItem->qty)
                                                                                @php($det_total += $cartItem->qty * $cartItem->price)
                                                                                    @php($det_vol += $cartItem->qty * $cartItem->product->volume)
                                                                                        @php($det_wei += $cartItem->qty * $cartItem->product->weight)
                                                                                        @endif
                                                                                    @endforeach
                                                                                    @php($order_num += 1)
                                                                                        @foreach ($viewData['shipping_values'] as $val)
                                                                                            @if ($val->user_id == $key->id)
                                                                                                @php($shipping_user = $val->shipping_id)
                                                                                                @endif
                                                                                            @endforeach


                                                                                            <div class="col-lg-12">
                                                                                                <div class="nk-gap-2"></div>
                                                                                                <h5 class="nk-decorated-h"><span><span class="text-main-1">{{ __('Detalle de pedido') }}
                                                                                                        </span> #0000{{ $order_num }}</span></h5>

                                                                                            </div>
                                                                                            <div class="col-lg-6">
                                                                                                <!----->
                                                                                                <table class="nk-table nk-table-sm">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td style="width:65%">{{ __('Pedido') }} #</td>
                                                                                                            <td>
                                                                                                                <div id=""> 0000{{ $order_num }} </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>{{ __('Comprador') }}</td>
                                                                                                            <td>
                                                                                                                <div id="">
                                                                                                                    @if (Auth::user())
                                                                                                                        {{ Auth::user()->user_name }} @endif
                                                                                                                </div>
                                                                                                            </td>

                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>{{ __('Vendedor') }}</td>
                                                                                                            <td>
                                                                                                                <div id="">{{ $key->user_name }}</div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>{{ __('Cantidad de artículos') }}</td>
                                                                                                            <td>
                                                                                                                <div id="">{{ $qty_det }}</div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>{{ __('Peso') }}</td>
                                                                                                            <td>
                                                                                                                <div id="">{{ $det_wei }} g </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>{{ __('Volumen') }}</td>
                                                                                                            <td>
                                                                                                                <div id="">{{ $det_vol }}</div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                                <!----->
                                                                                            </div>

                                                                                            <div class="col-lg-6 nk-form nk-form-style-1">
                                                                                                <div class="nk-gap-2"></div>
                                                                                                <label>{{ __('Método de envío') }} </label>
                                                                                                <select class="form-control" data-id="{{ $key->user_name }}" onchange="updateShipping(this)">
                                                                                                    @foreach ($viewData['shipping'] as $key_ship)
                                                                                                        @if ($key_ship->from_id == $key->country_id && $key_ship->to_id == Auth::user()->country_id)
                                                                                                            @if ($det_total <= $key_ship->max_value && $det_wei <= $key_ship->max_weight && $det_vol <= $key_ship->volume)
                                                                                                                <option value="{{ $key_ship->id }}"
                                                                                                                    {{ $key_ship->id == $shipping_user ? 'selected' : '' }}>
                                                                                                                {{ $key_ship->name }} @if ($key_ship->certified == 'Yes') Envio certificado @else Envio
                                                                                                                        no certificado @endif
                                                                                                                    {{ number_format($key_ship->price, 2) }} € -
                                                                                                                    {{ number_format($key_ship->max_value, 2) }} € max value</option>
                                                                                                            @endif
                                                                                                        @endif
                                                                                                        @if ($key_ship->from_id == 0 && $key_ship->to_id == 0)
                                                                                                            <option value="{{ $key_ship->id }}"
                                                                                                                {{ $key_ship->id == $shipping_user ? 'selected' : '' }}>
                                                                                                            {{ $key_ship->name }} @if ($key_ship->certified == 'Yes') Envio certificado @else Envio no
                                                                                                                    certificado @endif
                                                                                                                {{ number_format($key_ship->price, 2) }} € -
                                                                                                                {{ number_format($key_ship->max_value, 2) }} € max value</option>
                                                                                                        @endif
                                                                                                    @endforeach
                                                                                                </select>
                                                                                            </div>
                                                                                            @php($con = 0)
                                                                                                @php($game = 0)
                                                                                                    @foreach (App\AppOrgCategory::all() as $CatItem)
                                                                                                        @php($contador = 0)
                                                                                                            @foreach ($viewData['products_in_cart'] as $cartItem)
                                                                                                                @if ($cartItem->user->user_name == $key->user_name && $cartItem->product_info->catid == $CatItem->id)
                                                                                                                    @php($contador += 1)
                                                                                                                    @endif
                                                                                                                @endforeach


                                                                                                                <div class="col-lg-12" style="display: {{ $contador > 0 ? 'block' : 'none' }}; ">
                                                                                                                    <h5 class="mt-15">
                                                                                                                        {{ $CatItem->name }}
                                                                                                                    </h5>
                                                                                                                    <div class="table-responsive-md">
                                                                                                                        <table class="nk-table nk-table-xs">
                                                                                                                            <thead>

                                                                                                                                <tr class="text-center">
                                                                                                                                    <th rowspan="2" data-priority="1"><i class="fa fa-image"></i></th>
                                                                                                                                    <th rowspan="2" style="width:20%; font-size:10px;" data-priority="2">
                                                                                                                                        {{ __('Artículo') }}</th>

                                                                                                                                    <th colspan="{{ $CatItem->id == 1 ? 5 : ($CatItem->id == 2 ? 6 : 3) }}"
                                                                                                                                        style="font-size:12px;"><b>
                                                                                                                                            {{ __('Condición') }}
                                                                                                                                        </b>
                                                                                                                                    </th>
                                                                                                                                    <th rowspan="2"><i class="fa fa-image"></i></th>
                                                                                                                                    <th rowspan="2"><i class="fa fa-commenting-o"></i></th>
                                                                                                                                    <th rowspan="2" style="font-size:10px;">{{ __('Cant') }}</th>
                                                                                                                                    <th rowspan="2" style="font-size:10px;">{{ __('Precio') }}</th>
                                                                                                                                    <th rowspan="2" style="font-size:10px;">{{ __('Total') }}</th>
                                                                                                                                    <th rowspan="2" style="font-size:10px;" data-priority="3"><i
                                                                                                                                            class="fa fa-trash"></i></th>
                                                                                                                                </tr>

                                                                                                                                @if ($CatItem->id == 1)
                                                                                                                                    <tr class="text-center" style="font-size:10px;">
                                                                                                                                        <th style="font-size:10px;"> {{ __('Caja') }}</th>
                                                                                                                                        <th style="font-size:10px;"> {{ __('Carátula') }}</th>
                                                                                                                                        <th style="font-size:10px;"> {{ __('Manual') }}</td>
                                                                                                                                        <th style="font-size:10px;"> {{ __('Juego') }}</th>
                                                                                                                                        <th style="font-size:10px;"> {{ __('Extras') }}</th>
                                                                                                                                    </tr>
                                                                                                                                @elseif($CatItem->id == 2)
                                                                                                                                    <tr class="text-center" style="font-size:10px;">
                                                                                                                                        <th style="font-size:10px;"> {{ __('Caja') }}</th>
                                                                                                                                        <th style="font-size:10px;"> {{ __('Interior') }}</th>
                                                                                                                                        <th style="font-size:10px;"> {{ __('Manual') }}</td>
                                                                                                                                        <th style="font-size:10px;"> {{ __('Consola') }}</th>
                                                                                                                                        <th style="font-size:10px;"> {{ __('Extras') }}</th>
                                                                                                                                        <th style="font-size:10px;"> {{ __('Cables') }}</th>
                                                                                                                                    </tr>
                                                                                                                                @else
                                                                                                                                    <tr class="text-center" style="font-size:10px;">
                                                                                                                                        <th style="font-size:10px;"> {{ __('Caja') }}</th>
                                                                                                                                        <th style="font-size:10px;"> {{ __('Producto') }}</th>
                                                                                                                                        <th style="font-size:10px;"> {{ __('Extras') }}</th>
                                                                                                                                    </tr>
                                                                                                                                @endif
                                                                                                                            </thead>

                                                                                                                            <tbody>
                                                                                                                                @foreach ($viewData['products_in_cart'] as $cartItem)
                                                                                                                                    @if ($cartItem->user->user_name == $key->user_name && $cartItem->product_info->catid == $CatItem->id)
                                                                                                                                        @if ($CatItem->id == 1)
                                                                                                                                            <tr>
                                                                                                                                                <td class="text-center">

                                                                                                                                                    @if (count($cartItem->product_info->product_images) > 0)
                                                                                                                                                        <a href="/product/{{ $cartItem->product_info->id + 1000 }}"
                                                                                                                                                            class="image_appear">
                                                                                                                                                            <img style="width:350px; height: auto; display:none"
                                                                                                                                                                class="show_me"
                                                                                                                                                                src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                                                                                                                            <img width="27px" height="27px"
                                                                                                                                                                src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}"
                                                                                                                                                                alt="{{ $cartItem->name }}  ">
                                                                                                                                                        </a>
                                                                                                                                                    @else
                                                                                                                                                        <img width="27px" height="27px"
                                                                                                                                                            src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}"
                                                                                                                                                            alt="{{ $cartItem->name }}  ">
                                                                                                                                                    @endif
                                                                                                                                                </td>
                                                                                                                                                <td>
                                                                                                                                                    <a
                                                                                                                                                        href="product/{{ $cartItem->product_info->id + 1000 }}"><span
                                                                                                                                                            class="text-white h7">{{ $cartItem->product_info->name }}
                                                                                                                                                        </span></a><br><span
                                                                                                                                                        class="h7 text-secondary">{{ $cartItem->product_info->platform }}
                                                                                                                                                        - {{ $cartItem->product_info->region }} </span>
                                                                                                                                                </td>
                                                                                                                                                <td class="text-center">
                                                                                                                                                    <img width="15px" src="/{{ $cartItem->box }}"
                                                                                                                                                        data-toggle="popover"
                                                                                                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->box_condition) }}"
                                                                                                                                                        data-placement="top" data-trigger="hover">
                                                                                                                                                </td>
                                                                                                                                                <td class="text-center">
                                                                                                                                                    <img width="15px" src="/{{ $cartItem->cover }}"
                                                                                                                                                        data-toggle="popover"
                                                                                                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->cover_condition) }}"
                                                                                                                                                        data-placement="top" data-trigger="hover">
                                                                                                                                                </td>
                                                                                                                                                <td class="text-center">
                                                                                                                                                    <img width="15px" src="/{{ $cartItem->manual }}"
                                                                                                                                                        data-toggle="popover"
                                                                                                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->manual_condition) }}"
                                                                                                                                                        data-placement="top" data-trigger="hover">
                                                                                                                                                </td>
                                                                                                                                                <td data-filter="game_condition"
                                                                                                                                                    data-value="{{ $cartItem->game_condition }}"
                                                                                                                                                    class="text-center"><img width="15px"
                                                                                                                                                        src="/{{ $cartItem->game }}" data-toggle="popover"
                                                                                                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->game_condition) }}"
                                                                                                                                                        data-placement="top" data-trigger="hover"></td>
                                                                                                                                                <td class="text-center">
                                                                                                                                                    <img width="15px" src="/{{ $cartItem->extra }}"
                                                                                                                                                        data-toggle="popover"
                                                                                                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->extra_condition) }}"
                                                                                                                                                        data-placement="top" data-trigger="hover">
                                                                                                                                                </td>
                                                                                                                                                <td class="text-center">
                                                                                                                                                    @if ($cartItem->images->first())
                                                                                                                                                        <a href="/product/{{ $cartItem->product_info->id + 1000 }}"
                                                                                                                                                            class="image_appear">
                                                                                                                                                            <img style="width:350px; height: auto; display:none"
                                                                                                                                                                class="show_me"
                                                                                                                                                                src="{{ $cartItem->images->first()['image_path'] ? asset('/uploads/inventory-images/' . $cartItem->images->first()['image_path']) : 'assets/images/art-not-found.jpg' }}">
                                                                                                                                                            <img width="27px" height="27px"
                                                                                                                                                                src="{{ $cartItem->images->first()['image_path'] ? asset('/uploads/inventory-images/' . $cartItem->images->first()['image_path']) : 'assets/images/art-not-found.jpg' }}"
                                                                                                                                                                alt="{{ $cartItem->name }}  ">
                                                                                                                                                        </a>
                                                                                                                                                    @else
                                                                                                                                                        <i class="fa fa-times" title="Not found"></i>
                                                                                                                                                    @endif
                                                                                                                                                </td>
                                                                                                                                                <td class="text-center">
                                                                                                                                                    <i class="fa fa-{{ $cartItem->comments ? 'commenting-o' : 'times' }}"
                                                                                                                                                        data-toggle="popover"
                                                                                                                                                        data-content="{{ $cartItem->comments }}"
                                                                                                                                                        data-placement="top" data-trigger="hover"></i>
                                                                                                                                                </td>


                                                                                                                                                <td class="text-center">
                                                                                                                                                    <select name="cart-quantity" class="cart-quantity"
                                                                                                                                                        data-value="{{ $cartItem->qty }}"
                                                                                                                                                        data-inventory-id="{{ $cartItem->id + 1000 }}"
                                                                                                                                                        data-product-id="{{ $cartItem->product_info->id }}">
                                                                                                                                                        @for ($i = 1; $i <= $cartItem->quantity + $cartItem->qty; $i++)
                                                                                                                                                            <option value="{{ $i }}"
                                                                                                                                                                {{ $i == $cartItem->qty ? 'selected="selected"' : '' }}>
                                                                                                                                                                {{ $i }}</option>
                                                                                                                                                        @endfor
                                                                                                                                                    </select> <button
                                                                                                                                                        class="btn btn-default btn-xs cart-update-price"
                                                                                                                                                        style="display: none;"
                                                                                                                                                        type="button">{{ __('Actualizar') }}</button>
                                                                                                                                                </td>
                                                                                                                                                <td class=" h7">
                                                                                                                                                    <span class="cart-unit-price"
                                                                                                                                                        data-value="{{ $cartItem->price }}">{{ number_format($cartItem->price, 2) }}
                                                                                                                                                        €</span>
                                                                                                                                                </td>
                                                                                                                                                <td class=" h7">
                                                                                                                                                    {{ number_format($cartItem->price * $cartItem->qty, 2) }}
                                                                                                                                                    €
                                                                                                                                                </td>
                                                                                                                                                <td class="text-center">
                                                                                                                                                    <button data-inventory-id="{{ $cartItem->id + 1000 }}"
                                                                                                                                                        type="button"
                                                                                                                                                        class="nk-btn nk-btn-xs nk-btn-rounded nk-btn-color-white cart-delete-item-id">
                                                                                                                                                        <i class="fa fa-trash"></i> </button>
                                                                                                                                                </td>

                                                                                                                                            </tr>
                                                                                                                                        @elseif($CatItem->id == 2)
                                                                                                                                            <tr>
                                                                                                                                                <td class="text-center">

                                                                                                                                                    @if (count($cartItem->product_info->product_images) > 0)
                                                                                                                                                        <a href="/product/{{ $cartItem->product_info->id + 1000 }}"
                                                                                                                                                            class="image_appear">
                                                                                                                                                            <img style="width:350px; height: auto; display:none"
                                                                                                                                                                class="show_me"
                                                                                                                                                                src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                                                                                                                            <img width="27px" height="27px"
                                                                                                                                                                src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}"
                                                                                                                                                                alt="{{ $cartItem->name }}  ">
                                                                                                                                                        </a>
                                                                                                                                                    @else
                                                                                                                                                        <img width="27px" height="27px"
                                                                                                                                                            src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}"
                                                                                                                                                            alt="{{ $cartItem->name }}  ">
                                                                                                                                                    @endif
                                                                                                                                                </td>
                                                                                                                                                <td>
                                                                                                                                                    <a
                                                                                                                                                        href="product/{{ $cartItem->product_info->id + 1000 }}"><span
                                                                                                                                                            class="text-white h7">{{ $cartItem->product_info->name }}
                                                                                                                                                        </span></a><br><span
                                                                                                                                                        class="h7 text-secondary">{{ $cartItem->product_info->platform }}
                                                                                                                                                        - {{ $cartItem->product_info->region }} </span>
                                                                                                                                                </td>
                                                                                                                                                <td class="text-center">
                                                                                                                                                    <img width="15px" src="/{{ $cartItem->box }}"
                                                                                                                                                        data-toggle="popover"
                                                                                                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->box_condition) }}"
                                                                                                                                                        data-placement="top" data-trigger="hover">
                                                                                                                                                </td>
                                                                                                                                                <td class="text-center">
                                                                                                                                                    <img width="15px" src="/{{ $cartItem->cover }}"
                                                                                                                                                        data-toggle="popover"
                                                                                                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->cover_condition) }}"
                                                                                                                                                        data-placement="top" data-trigger="hover">
                                                                                                                                                </td>
                                                                                                                                                <td class="text-center">
                                                                                                                                                    <img width="15px" src="/{{ $cartItem->manual }}"
                                                                                                                                                        data-toggle="popover"
                                                                                                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->manual_condition) }}"
                                                                                                                                                        data-placement="top" data-trigger="hover">
                                                                                                                                                </td>

                                                                                                                                                <td data-filter="game_condition"
                                                                                                                                                    data-value="{{ $cartItem->game_condition }}"
                                                                                                                                                    class="text-center"><img width="15px"
                                                                                                                                                        src="/{{ $cartItem->game }}" data-toggle="popover"
                                                                                                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->game_condition) }}"
                                                                                                                                                        data-placement="top" data-trigger="hover"></td>


                                                                                                                                                <td data-filter="extra_condition"
                                                                                                                                                    data-value="{{ $cartItem->extra_condition }}"
                                                                                                                                                    class="text-center"><img width="15px"
                                                                                                                                                        src="/{{ $cartItem->extra }}" data-toggle="popover"
                                                                                                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->extra_condition) }}"
                                                                                                                                                        data-placement="top" data-trigger="hover"></td>
                                                                                                                                                </td>

                                                                                                                                                <td data-filter="inside_condition"
                                                                                                                                                    data-value="{{ $cartItem->inside_condition }}"
                                                                                                                                                    class="text-center"><img width="15px"
                                                                                                                                                        src="/{{ $cartItem->inside }}" data-toggle="popover"
                                                                                                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->inside_condition) }}"
                                                                                                                                                        data-placement="top" data-trigger="hover"></td>
                                                                                                                                                <td class="text-center">
                                                                                                                                                    @if ($cartItem->images->first())
                                                                                                                                                        <a href="/product/{{ $cartItem->product_info->id + 1000 }}"
                                                                                                                                                            class="image_appear">
                                                                                                                                                            <img style="width:350px; height: auto; display:none"
                                                                                                                                                                class="show_me"
                                                                                                                                                                src="{{ $cartItem->images->first()['image_path'] ? asset('/uploads/inventory-images/' . $cartItem->images->first()['image_path']) : 'assets/images/art-not-found.jpg' }}">
                                                                                                                                                            <img width="27px" height="27px"
                                                                                                                                                                src="{{ $cartItem->images->first()['image_path'] ? asset('/uploads/inventory-images/' . $cartItem->images->first()['image_path']) : 'assets/images/art-not-found.jpg' }}"
                                                                                                                                                                alt="{{ $cartItem->name }}  ">
                                                                                                                                                        </a>
                                                                                                                                                    @else
                                                                                                                                                        <i class="fa fa-times" title="Not found"></i>
                                                                                                                                                    @endif
                                                                                                                                                </td>
                                                                                                                                                <td class="text-center">
                                                                                                                                                    <i class="fa fa-{{ $cartItem->comments ? 'commenting-o' : 'times' }}"
                                                                                                                                                        data-toggle="popover"
                                                                                                                                                        data-content="{{ $cartItem->comments }}"
                                                                                                                                                        data-placement="top" data-trigger="hover"></i>
                                                                                                                                                </td>


                                                                                                                                                <td class="text-center">
                                                                                                                                                    <select name="cart-quantity" class="cart-quantity"
                                                                                                                                                        data-value="{{ $cartItem->qty }}"
                                                                                                                                                        data-inventory-id="{{ $cartItem->id + 1000 }}"
                                                                                                                                                        data-product-id="{{ $cartItem->product_info->id }}">
                                                                                                                                                        @for ($i = 1; $i <= $cartItem->quantity + $cartItem->qty; $i++)
                                                                                                                                                            <option value="{{ $i }}"
                                                                                                                                                                {{ $i == $cartItem->qty ? 'selected="selected"' : '' }}>
                                                                                                                                                                {{ $i }}</option>
                                                                                                                                                        @endfor
                                                                                                                                                    </select> <button
                                                                                                                                                        class="btn btn-default btn-xs cart-update-price"
                                                                                                                                                        style="display: none;"
                                                                                                                                                        type="button">{{ __('Actualizar') }}</button>
                                                                                                                                                </td>
                                                                                                                                                <td class=" h7">
                                                                                                                                                    <span class="cart-unit-price"
                                                                                                                                                        data-value="{{ $cartItem->price }}">{{ number_format($cartItem->price, 2) }}
                                                                                                                                                        €</span>
                                                                                                                                                </td>
                                                                                                                                                <td class=" h7">
                                                                                                                                                    {{ number_format($cartItem->price * $cartItem->qty, 2) }}
                                                                                                                                                    €
                                                                                                                                                </td>
                                                                                                                                                <td class="text-center">
                                                                                                                                                    <button data-inventory-id="{{ $cartItem->id + 1000 }}"
                                                                                                                                                        type="button"
                                                                                                                                                        class="nk-btn nk-btn-xs nk-btn-rounded nk-btn-color-white cart-delete-item-id">
                                                                                                                                                        <i class="fa fa-trash"></i> </button>
                                                                                                                                                </td>

                                                                                                                                            </tr>
                                                                                                                                        @else
                                                                                                                                            <tr>
                                                                                                                                                <td class="text-center">

                                                                                                                                                    @if (count($cartItem->product_info->product_images) > 0)
                                                                                                                                                        <a href="/product/{{ $cartItem->product_info->id + 1000 }}"
                                                                                                                                                            class="image_appear">
                                                                                                                                                            <img style="width:350px; height: auto; display:none"
                                                                                                                                                                class="show_me"
                                                                                                                                                                src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                                                                                                                            <img width="27px" height="27px"
                                                                                                                                                                src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}"
                                                                                                                                                                alt="{{ $cartItem->name }}  ">
                                                                                                                                                        </a>
                                                                                                                                                    @else
                                                                                                                                                        <i class="fa fa-times" title="Not found"></i>
                                                                                                                                                    @endif
                                                                                                                                                </td>
                                                                                                                                                <td>
                                                                                                                                                    <a
                                                                                                                                                        href="product/{{ $cartItem->product_info->id + 1000 }}"><span
                                                                                                                                                            class="text-white h7">{{ $cartItem->product_info->name }}
                                                                                                                                                        </span></a><br><span
                                                                                                                                                        class="h7 text-secondary">{{ $cartItem->product_info->platform }}
                                                                                                                                                        - {{ $cartItem->product_info->region }} </span>
                                                                                                                                                </td>
                                                                                                                                                <td data-filter="box_condition"
                                                                                                                                                    data-value="{{ $cartItem->box_condition }}"
                                                                                                                                                    class="text-center"><img width="15px"
                                                                                                                                                        src="/{{ $cartItem->box }}" data-toggle="popover"
                                                                                                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->box_condition) }}"
                                                                                                                                                        data-placement="top" data-trigger="hover"></td>

                                                                                                                                                <td data-filter="game_condition"
                                                                                                                                                    data-value="{{ $cartItem->game_condition }}"
                                                                                                                                                    class="text-center"><img width="15px"
                                                                                                                                                        src="/{{ $cartItem->game }}" data-toggle="popover"
                                                                                                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->game_condition) }}"
                                                                                                                                                        data-placement="top" data-trigger="hover"></td>

                                                                                                                                                <td data-filter="extra_condition"
                                                                                                                                                    data-value="{{ $cartItem->extra_condition }}"
                                                                                                                                                    class="text-center"><img width="15px"
                                                                                                                                                        src="/{{ $cartItem->extra }}" data-toggle="popover"
                                                                                                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->extra_condition) }}"
                                                                                                                                                        data-placement="top" data-trigger="hover"></td>


                                                                                                                                                <td class="text-center">
                                                                                                                                                    @if ($cartItem->images->first())
                                                                                                                                                        <a href="/product/{{ $cartItem->product_info->id + 1000 }}"
                                                                                                                                                            class="image_appear">
                                                                                                                                                            <img style="width:350px; height: auto; display:none"
                                                                                                                                                                class="show_me"
                                                                                                                                                                src="{{ $cartItem->images->first()['image_path'] ? asset('/uploads/inventory-images/' . $cartItem->images->first()['image_path']) : 'assets/images/art-not-found.jpg' }}">
                                                                                                                                                            <img width="27px" height="27px"
                                                                                                                                                                src="{{ $cartItem->images->first()['image_path'] ? asset('/uploads/inventory-images/' . $cartItem->images->first()['image_path']) : 'assets/images/art-not-found.jpg' }}"
                                                                                                                                                                alt="{{ $cartItem->name }}  ">
                                                                                                                                                        </a>
                                                                                                                                                    @else
                                                                                                                                                        <i class="fa fa-times" title="Not found"></i>
                                                                                                                                                    @endif
                                                                                                                                                </td>
                                                                                                                                                <td class="text-center">
                                                                                                                                                    <i class="fa fa-{{ $cartItem->comments ? 'commenting-o' : 'times' }}"
                                                                                                                                                        data-toggle="popover"
                                                                                                                                                        data-content="{{ $cartItem->comments }}"
                                                                                                                                                        data-placement="top" data-trigger="hover"></i>
                                                                                                                                                </td>


                                                                                                                                                <td class="text-center">
                                                                                                                                                    <select name="cart-quantity" class="cart-quantity"
                                                                                                                                                        data-value="{{ $cartItem->qty }}"
                                                                                                                                                        data-inventory-id="{{ $cartItem->id + 1000 }}"
                                                                                                                                                        data-product-id="{{ $cartItem->product_info->id }}">
                                                                                                                                                        @for ($i = 1; $i <= $cartItem->quantity + $cartItem->qty; $i++)
                                                                                                                                                            <option value="{{ $i }}"
                                                                                                                                                                {{ $i == $cartItem->qty ? 'selected="selected"' : '' }}>
                                                                                                                                                                {{ $i }}</option>
                                                                                                                                                        @endfor
                                                                                                                                                    </select> <button
                                                                                                                                                        class="btn btn-default btn-xs cart-update-price"
                                                                                                                                                        style="display: none;"
                                                                                                                                                        type="button">{{ __('Actualizar') }}</button>
                                                                                                                                                </td>
                                                                                                                                                <td class=" h7">
                                                                                                                                                    <span class="cart-unit-price"
                                                                                                                                                        data-value="{{ $cartItem->price }}">{{ number_format($cartItem->price, 2) }}
                                                                                                                                                        €</span>
                                                                                                                                                </td>
                                                                                                                                                <td class=" h7">
                                                                                                                                                    {{ number_format($cartItem->price * $cartItem->qty, 2) }}
                                                                                                                                                    €
                                                                                                                                                </td>
                                                                                                                                                <td class="text-center">
                                                                                                                                                    <button data-inventory-id="{{ $cartItem->id + 1000 }}"
                                                                                                                                                        type="button"
                                                                                                                                                        class="nk-btn nk-btn-xs nk-btn-rounded nk-btn-color-white cart-delete-item-id">
                                                                                                                                                        <i class="fa fa-trash"></i> </button>
                                                                                                                                                </td>

                                                                                                                                            </tr>
                                                                                                                                        @endif
                                                                                                                                    @endif
                                                                                                                                @endforeach
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                    <div class="nk-gap-2"></div>
                                                                                                                </div>
                                                                                                            @endforeach

                                                                                                        @endforeach
                                                                                                    @endif
                                                                                                </div>

                                                                                            @else
                                                                                                <div class="nk-gap-3"></div>
                                                                                                <div class="nk-gap-3"></div>
                                                                                                <h3>{{ __('No hay productos en tu carrito de compra. Agrega productos ingresando a las categorías del menu principal') }}
                                                                                                </h3>
                                                                                            @endif

                                                                                            <div class="nk-gap-2"></div>
                                                                                        </div><!-- /.container -->
                                                                                    @endsection

                                                                                    @section('content-script-include')
                                                                                        <script src="{{ asset('brcode/js/pty.components.js') }}"></script>
                                                                                    @endsection
                                                                                    @section('content-script')
                                                                                        <script>
                                                                                            $(document).ready(function() {
                                                                                                $('.comments').popover();
                                                                                                $('[data-toggle="popover"]').popover();
                                                                                                $('.cart-quantity').change(function() {
                                                                                                    qty = $(this).val();
                                                                                                    id = $(this).data('inventory-id');
                                                                                                    pId = $(this).data('product-id');

                                                                                                    $.showBigOverlay({
                                                                                                        message: '{{ __('Actualizando tu carro de compras') }}',
                                                                                                        onLoad: function() {
                                                                                                            $.ajax({
                                                                                                                url: '{{ url('/cart/add_item') }}',
                                                                                                                data: {
                                                                                                                    _token: $('input[name="_token"]:eq(0)').val(),
                                                                                                                    inventory_id: id,
                                                                                                                    inventory_qty: qty,
                                                                                                                    inventory_typ: 0,
                                                                                                                },
                                                                                                                dataType: 'JSON',
                                                                                                                type: 'POST',
                                                                                                                success: function(data) {
                                                                                                                    if (data.error == 0) {
                                                                                                                        //window.location.href = window.location.href;
                                                                                                                        /*
                                                                                              // message.remove();
                                                                                              // overlay.remove();
                                                                                              $('#br-cart-items').text(data.items);
                                                                                                    $('#articlesID').text(data.items);
                                                                                                    //parent.find('.cart-item-total-price').text(data.item_total + " €");
                                                                                                    
                                                                                                    $('.cart-total-price:eq(0)').text((data.gran_total).toFixed(2)  + " €");
                                                                                                    $('#subTotalID').text((data.item_total).toFixed(2) + " €");
                                                                                                    $('#totalID').text((data.gran_total).toFixed(2) + " €");
                                                                                                    $('#orderID').text(data.user);
                                                                                                    */
                                                                                                                    } else {

                                                                                                                    }
                                                                                                                    $.showBigOverlay('hide');
                                                                                                                },
                                                                                                                error: function(data) {

                                                                                                                }
                                                                                                            })
                                                                                                        }
                                                                                                    });

                                                                                                });

                                                                                                $('.cart-delete-item-id').click(function() {
                                                                                                    var inventoryId = $(this).data('inventory-id');
                                                                                                    var cartItem = $(this).closest('.cart-item');
                                                                                                    $.showBigOverlay({
                                                                                                        message: '{{ __('Actualizando tu carro de compras') }}',
                                                                                                        onLoad: function() {
                                                                                                            $.ajax({
                                                                                                                url: '{{ url('/cart/add_item') }}',
                                                                                                                data: {
                                                                                                                    _token: $('input[name="_token"]:eq(0)').val(),
                                                                                                                    inventory_id: inventoryId,
                                                                                                                    inventory_qty: 0,
                                                                                                                },
                                                                                                                dataType: 'JSON',
                                                                                                                type: 'POST',
                                                                                                                success: function(data) {

                                                                                                                    if (data.error == 0) {
                                                                                                                        window.location.href = window.location.href;
                                                                                                                        /*
                                                                                                                        location.reload();

                                                                                                                        var emptyMessage = $('<span/>').addClass('cart-empty').css({
                                                                                                                          opacity: 0
                                                                                                                        }).text('{{ __('No hay productos en tu carrito de compra. Agrega productos ingresando a las categorías del menu principal') }}');
                                                                                                                        if($('.cart-item').length == 1) {
                                                                                                                          $('#br-search-result-list').slideUp(250, function() {
                                                                                                                            $( this ).remove();
                                                                                                                            $('#cart-container').html('').append(emptyMessage);
                                                                                                                            $('#cart-order-user-detail').html('');
                                                                                                                            emptyMessage.animate({opacity: 1}, 250);
                                                                                                                          });
                                                                                                                        }
                                                                                                                        else {
                                                                                                                          cartItem.slideUp(250, function() {
                                                                                                                            $( this ).remove();
                                                                                                                          });
                                                                                                                          $('.cart-total-price:eq(0)').text(data.gran_total + " €");
                                                                                                                          $('#subTotalID').text(data.item_total.toFixed(2) +" €");
                                                                                                                          $('#totalID').text(data.gran_total.toFixed(2) +" €");
                                                                                                                          $('#orderID').text(data.user);
                                                                                                                        }
                                                                                                                        $('#br-cart-items').text(data.items);
                                                                                                                        $('#articlesID').text(data.items);
                                                                                                                        
                                                                                                                        */
                                                                                                                    } else {
                                                                                                                        $.showBigOverlay('hide');
                                                                                                                    }
                                                                                                                    $.showBigOverlay('hide');
                                                                                                                },
                                                                                                                error: function(data) {
                                                                                                                    $.showBigOverlay('hide');
                                                                                                                }
                                                                                                            })
                                                                                                        }
                                                                                                    });

                                                                                                });

                                                                                            });

                                                                                            function updateShipping(x) {
                                                                                                $.showBigOverlay({
                                                                                                    message: '{{ __('Actualizando tu carro de compras') }}',
                                                                                                    onLoad: function() {
                                                                                                        $.ajax({
                                                                                                            url: '{{ url('/cart/add_shipping') }}',
                                                                                                            data: {
                                                                                                                _token: $('input[name="_token"]:eq(0)').val(),
                                                                                                                d1: $(x).data('id'),
                                                                                                                d2: $(x).val(),
                                                                                                            },
                                                                                                            dataType: 'JSON',
                                                                                                            type: 'POST',
                                                                                                            success: function(data) {
                                                                                                                if (data.error == 0) {
                                                                                                                    window.location.href = window.location.href;
                                                                                                                } else {
                                                                                                                    $.showBigOverlay('hide');
                                                                                                                }
                                                                                                                $.showBigOverlay('hide');
                                                                                                            },
                                                                                                            error: function(data) {

                                                                                                            }
                                                                                                        })
                                                                                                    }
                                                                                                });
                                                                                            }

                                                                                        </script>

                                                                                    @endsection
