@extends('brcode.front.layout.app')
@section('content-css-include')

@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">

</section>

<!-- Main content -->
<section class="content">
  
  <div id="home-products">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="home-products-new">
						<h4 class="section-title">{{ __('Últimos añadidos por administración') }}</h4>
						<div class="section-view-all"><a href="search-products">{{ __('Ver todos') }}</a></div>
						<div class="table-responsive">
							<table class="table table-striped table-with-options">
								<thead>
									<tr>
										<th style="width: 1px;text-align: center;">#</th>
										<th>Name</th>
										<th>Seller</th>
										<th>Price</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="home-products-new">
						<h4 class="section-title">{{ __('Últimos añadidos por usuarios') }}</h4>
						<div class="section-view-all"><a href="search-products">{{ __('Ver todos') }}</a></div>
						<div class="table-responsive">
							<table class="table table-striped table-with-options">
								<thead>
									<tr>
										<th style="width: 1px;text-align: center;">#</th>
										<th>Name</th>
										<th>Seller</th>
										<th>Price</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-picture-o" aria-hidden="true"></i></button>
														<button type="button" class="btn btn-default btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Name</td>
										<td>Seller</td>
										<td>Price
											<div class="table-options" style="opacity: 0">
												<div>
													<div>
						