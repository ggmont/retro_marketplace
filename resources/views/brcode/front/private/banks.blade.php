@extends('brcode.front.layout.app')
@section('content-css-include')

@endsection
@section('content')
<!-- Content Header (Page header) -->

<!-- Main content -->
<section  class="content">
<div class="container">
  <div class="row">
    <div class="col-md-offset-2 col-md-10">
      <div class="account-title">{{ __('Configuración de cuenta bancaria') }}</div>
    </div>
  </div><!-- /.row -->
  <div class="row">
    <div class="col-md-2 col-sm-2 col-xs-12">
      @php($account_menu = 'bank')
      @include('brcode.front.private.account_menu')
    </div><!-- /.col-@-2 -->
    <div class="col-md-10 col-sm-10 col-xs-12">
        <form name="formBank" class="form-horizontal" method="POST" action="/account/banks">
        {{ csrf_field() }}
            <div class="form-group">
                <label for="br-register-co-account" class="col-sm-2 control-label">{{ __('Titular de la cuenta') }}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="beneficiary" placeholder="{{ __('Su nombre') }}" required value="{{ $viewData['bank_data']->beneficiary }}">
                </div>
            </div> 

            <div class="form-group" >
                <label for="br-register-co-iban" class="col-sm-2 control-label">{{ __('IBAN') }}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="iban_code" placeholder="{{ __('Su nombre') }}" required value="{{ $viewData['bank_data']->iban_code }}">
                </div>
            </div>
            
            <div class="form-group">
                <label for="br-register-co-bic" class="col-sm-2 control-label">{{ __('BIC/SWIFT') }}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="bic_code" placeholder="{{ __('Su nombre') }}" required value="{{ $viewData['bank_data']->bic_code }}">
                </div>
            </div>

            <div class="form-group">
                <label for="br-register-co-bank-name" class="col-sm-2 control-label">{{ __('Entidad bancaria') }}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="bank_address"  id="br-register-co-bank-name" placeholder="{{ __('Su nombre') }}" required value="{{ $viewData['bank_data']->bank_address }}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default" id="br-submit-save-co" ><i class="fa fa-floppy-o"></i> {{ __('Guardar') }}</button>
                </div>
            </div>
        </form>
    </div><!-- /.col-@-10 -->
  </div><!-- /.row -->
</div><!-- /.container -->

@endsection

@section('content-script-include')
<script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
<!-- The basic File Upload plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
<!-- The File Upload processing plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>



@endsection
@section('content-script')
<script>
$(document).ready(function() {

});
</script>
@endsection