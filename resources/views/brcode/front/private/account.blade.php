@extends('brcode.front.layout.app')
@section('content-css-include')

@endsection
@section('content')
<!-- Content Header (Page header) -->

<!-- Main content -->
<section  class="content">
<div class="container">
  <div class="row">
    <div class="col-md-offset-2 col-md-10">
      <div class="account-title">{{ __('Tablero de :user_name', ['user_name' => Auth::user()->first_name]) }}</div>
    </div>
  </div><!-- /.row -->
  <div class="row">
    <div class="col-md-2 col-sm-2 col-xs-12">
      @php($account_menu = 'account')
      @include('brcode.front.private.account_menu')
    </div><!-- /.col-@-2 -->
    <div class="col-md-10 col-sm-10 col-xs-12">
      <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-block">
            <h4 class="section-title">{{ __('Credito total: ') }} <b> {{ Auth::user()->cash }} </b></h4>
            <div class="section-block-content">
              @if(!empty($transactionProcess))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Info!</strong> {{$transactionProcess}}.
                </div>
              @endif
              <a class="btn btn-link btn-md" href="/account/payment" style="color: blue;"><i class="fa fa-money"></i> {{ __('INGRESAR DINERO') }}</a>
              <br>
              <a class="btn btn-link btn-md" style="color: blue;" id="withdraw"><i class="fa fa-money"></i> {{ __('RETIRAR DINERO') }}</a>
              <form action="{{ url('/account')}}" method="post">
              {{ csrf_field() }}
              <div id="cred" style="display: none;">
                  {{ __('Cuánto dinero quieres retirar?') }} <input type="number" step="0.01" name="credit" min="0" max="{{ Auth::user()->cash }}" place-holder="Amount" required> ({{ __('Maximo') }}:{{ Auth::user()->cash }} €)
                  <br>
                  <button type="submit" class="btn btn-success btn-xs">{{ __('Revisar Información') }}</button>
                  <a class="btn btn-danger btn-xs" id="withdrawHide"><i class="fa fa-cancel"></i> {{ __('Cancelar') }}</a>
              </div>
              </form>
              
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="section-block">
            <h4 class="section-title">{{ __('Inventario') }}</h4>
            <div class="section-block-content">
              <table class="table table-striped">
                <thead>
                  <tr><th>{{ __('Nombre')}}</th><th> {{ __('Disp.') .' | ' . __('Vend.')  }}</tr>
                </thead>
                <tbody>
                  @foreach($viewData['inventories'] as $inventory)
                  <tr><td><a href="{{ url('account/inventory/modify/' . ($inventory->id + 1000)) }}">{{ $inventory->product->name }} </a></td><td class="text-right">{{$inventory->quantity . ' | ' . $inventory->quantity_sold}}</td></tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="section-block">
            <h4 class="section-title">{{ __('Ventas y compras') }}</h4>
            <div class="section-block-content">
              <p>{{ __trans_choice('Usted ha completado :sales ventas', $viewData['sales'], ['sales' => $viewData['sales']]) }}</p>
              <p>{{ __trans_choice('Usted ha realizado :purchases compras', $viewData['purchases'], ['purchases' => $viewData['purchases']] ) }}</p>
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.col-@-10 -->
  </div><!-- /.row -->
</div><!-- /.container -->
</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
<!-- The basic File Upload plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
<!-- The File Upload processing plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>

@endsection
@section('content-script')
<script>
  $(document).ready(function(){
    $("#withdraw").on( "click", function() {
      $('#cred').show(); 
    });
    $("#withdrawHide").on( "click", function() {
      $('#cred').hide(); 
    });

  });
</script>
@endsection
