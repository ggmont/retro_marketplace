@php($texto = app('request')->input('side') ? 'app_sidebar_game' : 'app')

@extends('brcode.front.layout.' . $texto)

@section('content-css-include')
    <!-- Dropzone css -->
    <link href="/assets/dropzone-master/dist/min/dropzone.min.css" rel="stylesheet" type="text/css" />
    <style>
        .dropzone .dz-preview .dz-error-message {
            top: 175px !important;
        }

        .retro {
            font-family: 'Press Start 2P', cursive;
        }
    </style>
@endsection

@section('content')

    <div class="nk-main">
        <!-- START: Breadcrumbs -->

        <div class="nk-gap-3"></div>
        <div class="top-products-area clearfix py-5">
            <div class="header-area" id="headerArea">
                <div class="container h-100 d-flex align-items-center justify-content-between">
                    <!-- Back Button-->
                    <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                    <!-- Page Title-->
                    <div class="page-heading">
                        <h6 class="mb-0 font-extrabold">Detalles del Producto</h6>
                    </div>
                    <!-- Navbar Toggler-->

                    <div class="normal">
                        @if (Auth::user())
                            <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebarPanel">
                                <span></span><span></span><span></span>
                            </div>
                        @else
                            <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebarPanel">
                                <span></span><span></span><span></span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- END: Breadcrumbs -->
            <div class="product-slides owl-carousel">

                <div class="relative">
                    @if (count($inventory->images) > 0)
                        <a href="{{ url('/uploads/inventory-images/' . $inventory->images->first()->image_path) }}"
                            class="fancybox" data-fancybox="RGM">
                            <img class="single-product-slide"
                                src="{{ url('/uploads/inventory-images/' . $inventory->images->first()->image_path) }}"
                                alt="">
                        </a>
                    @else
                        <img class="single-product-slide" loading="lazy"
                            src="{{ asset('assets/images/art-not-found.jpg') }}" alt="">
                    @endif
                </div>

                @if (count($inventory->images) > 1)
                    @foreach ($inventory->images as $key)
                        @if (!$loop->first)
                            <div class="relative">
                                @if (count($inventory->images) > 0)
                                    <a href="{{ '/uploads/inventory-images/' . $key->image_path }}" class="fancybox"
                                        data-fancybox="RGM">
                                        <img class="single-product-slide"
                                            src="{{ '/uploads/inventory-images/' . $key->image_path }}" alt="dummy-image">
                                    </a>
                                @endif
                            </div>
                        @endif
                    @endforeach
                @endif

            </div>

            <!-- Rating & Review Wrapper-->
            <div class="container">
                <div class="section-heading mt-3">

                </div>
                <!-- Contact Form-->
                <div class="section mt-2 mb-5">
                    <br><br>
                    <center>
                        <h4 class="mb-1 font-bold">Detalles del inventario</h4>
                    </center>
                    <form id="br-inventory-add" class="w-full"
                        action="{{ route('normal_update_inventory', $inventory->id) }}" method="POST"
                        enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label">Título</label>
                                <input type="text" placeholder="Titulo" id="name_field" name="title"
                                    value="{{ $inventory->title }}" min="1" required class="form-control">
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5">Categoría</label>
                                <select name="category" required class="form-control form-select" id="default_select">

                                    @if ($inventory->category == 1)
                                        <option data-html="Juegos" value="{{ $inventory->category }}">
                                            Juegos</option>
                                        <option data-html="Consolas" value="Consolas">Consolas</option>
                                        <option data-html="Periféricos" value="Periféricos">Periféricos</option>
                                        <option data-html="Accesorios" value="Accesorios">Accesorios</option>
                                        <option data-html="Merchadising" value="Merchadising">Merchadising</option>
                                    @elseif($inventory->category == 2)
                                        <option data-html="Consolas" value="{{ $inventory->category }}">
                                            Consolas</option>
                                        <option data-html="Juegos" value="Juegos">Juegos</option>
                                        <option data-html="Periféricos" value="Periféricos">Periféricos</option>
                                        <option data-html="Accesorios" value="Accesorios">Accesorios</option>
                                        <option data-html="Merchadising" value="Merchadising">Merchadising</option>
                                    @elseif($inventory->category == 3)
                                        <option data-html="Periféricos" value="{{ $inventory->category }}">
                                            Periféricos</option>
                                        <option data-html="Juegos" value="Juegos">Juegos</option>
                                        <option data-html="Accesorios" value="Accesorios">Accesorios</option>
                                        <option data-html="Merchadising" value="Merchadising">Merchadising</option>
                                    @elseif($inventory->category == 4)
                                        <option data-html="Accesorios" value="{{ $inventory->category }}">
                                            Accesorios</option>
                                        <option data-html="Juegos" value="Juegos">Juegos</option>
                                        <option data-html="Periféricos" value="Periféricos">Periféricos</option>
                                        <option data-html="Merchadising" value="Merchadising">Merchadising</option>
                                    @elseif($inventory->category == 172)
                                        <option data-html="Merchadising" value="{{ $inventory->category }}">
                                            Merchadising</option>
                                        <option data-html="Juegos" value="Juegos">Juegos</option>
                                        <option data-html="Accesorios" value="Accesorios">Accesorios</option>
                                        <option data-html="Periféricos" value="Periféricos">Periféricos</option>
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label">Cantidad</label>
                                <input type="number" placeholder="Cantidad de articulos" id="quantity_field"
                                    name="quantity" value="{{ $inventory->quantity }}" min="1"
                                    class="form-control">
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label">Precio</label>

                                <input type="number" autocomplete="off" name="price"
                                    class="form-control br-input-number required {{ $errors->has($viewData['col_prefix'] . 'price') ? 'is-invalid' : '' }} price-check"
                                    step="0.01" required placeholder="Precio"
                                    value="{{ number_format($inventory->price, 2) }}" pattern="\d+(\.\d{1,2})?" />

                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5">¿Como es tu producto?</label>
                                <select name="size" required class="form-control form-select" id="default_select">

                                    @if ($inventory->size == 0)
                                        <option data-html="Pequeño (Juegos, cartuchos, mandos...)"
                                            value="{{ $inventory->size }}">Pequeño (Juegos, cartuchos, mandos...)</option>
                                        <option data-html="Mediano - (consolas, lote de juegos) " value="1">Mediano -
                                            (consolas, lote de juegos) </option>
                                        <option data-html="Grande - (consolas en caja, lote grande de juegos)"
                                            value="2">Grande - (consolas en caja, lote grande de juegos)</option>

                                        <option data-html="XXL - (Televisores, colecciones completas)" value="3">XXL
                                            - (Televisores, colecciones completas)
                                        </option>
                                    @elseif($inventory->size == 1)
                                        <option data-html="Mediano - (consolas, lote de juegos) "
                                            value="{{ $inventory->size }}">Mediano - (consolas, lote de juegos) </option>
                                        <option data-html="Pequeño (Juegos, cartuchos, mandos...)" value="0">Pequeño
                                            (Juegos, cartuchos, mandos...)</option>
                                        <option data-html="Grande - (consolas en caja, lote grande de juegos)"
                                            value="2">Grande - (consolas en caja, lote grande de juegos)</option>

                                        <option data-html="XXL - (Televisores, colecciones completas)" value="3">XXL
                                            - (Televisores, colecciones completas)
                                        </option>
                                        <option data-html="Super XXL (Maquinas arcade, expositores, muebles)"
                                            value="4">
                                            Super XXL (Maquinas arcade, expositores, muebles)</option>
                                    @elseif($inventory->size == 2)
                                        <option data-html="Grande - (consolas en caja, lote grande de juegos)"
                                            value="{{ $inventory->size }}">Grande - (consolas en caja, lote grande de
                                            juegos)</option>
                                        <option data-html="Super XXL (Maquinas arcade, expositores, muebles)"
                                            value="4">
                                            Super XXL (Maquinas arcade, expositores, muebles)</option>
                                        <option data-html="XXL - (Televisores, colecciones completas)" value="3">XXL
                                            - (Televisores, colecciones completas)
                                        </option>
                                        <option data-html="Mediano - (consolas, lote de juegos) " value="1">Mediano -
                                            (consolas, lote de juegos) </option>
                                        <option data-html="Pequeño (Juegos, cartuchos, mandos...)" value="0">Pequeño
                                            (Juegos, cartuchos, mandos...)</option>
                                    @elseif($inventory->size == 3)
                                        <option data-html="XXL - (Televisores, colecciones completas)"
                                            value="{{ $inventory->size }}">XXL - (Televisores, colecciones completas)
                                        </option>
                                        <option data-html="Super XXL (Maquinas arcade, expositores, muebles)"
                                            value="4">
                                            Super XXL (Maquinas arcade, expositores, muebles)</option>
                                        <option data-html="Grande - (consolas en caja, lote grande de juegos)"
                                            value="2">Grande - (consolas en caja, lote grande de juegos)</option>
                                        <option data-html="Mediano - (consolas, lote de juegos) " value="1">Mediano -
                                            (consolas, lote de juegos) </option>
                                        <option data-html="Pequeño (Juegos, cartuchos, mandos...)" value="0">Pequeño
                                            (Juegos, cartuchos, mandos...)</option>
                                    @elseif($inventory->size == 4)
                                        <option data-html="Super XXL (Maquinas arcade, expositores, muebles)"
                                            value="{{ $inventory->size }}">Super XXL (Maquinas arcade, expositores,
                                            muebles)
                                        </option>
                                        <option data-html="XXL - (Televisores, colecciones completas)"
                                            value="XXL - (Televisores, colecciones completas)">XXL - (Televisores,
                                            colecciones completas)
                                        </option>
                                        <option data-html="Grande - (consolas en caja, lote grande de juegos)"
                                            value="2">Grande - (consolas en caja, lote grande de juegos)</option>
                                        <option data-html="Mediano - (consolas, lote de juegos) " value="1">Mediano -
                                            (consolas, lote de juegos) </option>
                                        <option data-html="Pequeño (Juegos, cartuchos, mandos...)" value="0">Pequeño
                                            (Juegos, cartuchos, mandos...)</option>
                                    @endif


                                </select>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5">Peso</label>
                                <select name="kg" required class="form-control form-select" id="default_select">
                                    @if ($inventory->kg == 250)
                                        <option data-html="0 a 1 Kg" value="{{ $inventory->kg }}">0 a 1 Kg</option>
                                        <option data-html="1 a 3 Kg" value="1100">1 a 3 Kg</option>
                                        <option data-html="3 a 5 Kg" value="3100">3 a 5 Kg</option>
                                        <option data-html="5 a 10 Kg" value="5100">5 a 10 Kg</option>
                                        <option data-html="10 a 20 Kg" value="10100">10 a 20 Kg</option>
                                    @elseif($inventory->kg == 1100)
                                        <option data-html="1 a 3 Kg" value="{{ $inventory->kg }}">1 a 5 Kg</option>

                                        <option data-html="0 a 1 Kg" value="250">0 a 1 Kg</option>
                                        <option data-html="3 a 5 Kg" value="3100">3 a 5 Kg</option>
                                        <option data-html="5 a 10 Kg" value="5100">5 a 10 Kg</option>
                                        <option data-html="10 a 20 Kg" value="10100">10 a 20 Kg</option> 
                                    @elseif($inventory->kg == 3100)
                                        <option data-html="3 a 5 Kg" value="{{ $inventory->kg }}">3 a 5 Kg</option>

                                        <option data-html="0 a 1 Kg" value="250">0 a 1 Kg</option>
                                        <option data-html="1 a 3 Kg" value="1100">1 a 3 Kg</option>
                                        <option data-html="5 a 10 Kg" value="5100">5 a 10 Kg</option>
                                        <option data-html="10 a 20 Kg" value="10100">10 a 20 Kg</option>
                                    @elseif($inventory->kg == 5100)
                                        <option data-html="5 a 10 Kg" value="{{ $inventory->kg }}">5 a 10 Kg</option>

                                        <option data-html="0 a 1 Kg" value="250">0 a 1 Kg</option>
                                        <option data-html="1 a 3 Kg" value="1100">1 a 3 Kg</option>
                                        <option data-html="3 a 5 Kg" value="3100">3 a 5 Kg</option>
                                        <option data-html="10 a 20 Kg" value="10100">10 a 20 Kg</option>

                                    @elseif($inventory->kg == 10100)
                                        <option data-html="10 a 20 Kg" value="{{ $inventory->kg }}">10 a 20 Kg</option>

                                        <option data-html="0 a 1 Kg" value="250">0 a 1 Kg</option>
                                        <option data-html="1 a 3 Kg" value="1100">1 a 3 Kg</option>
                                        <option data-html="3 a 5 Kg" value="3100">3 a 5 Kg</option>
                                        <option data-html="5 a 10 Kg" value="5100">5 a 10 Kg</option>
                                        
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5"> {{ __('Comentarios') }}</label>
                                <textarea id="comments" name="comments" rows="4" class="form-control" placeholder="Comentario"> {{ $inventory->comments }} </textarea>
                                <div class="textarea-counter" data-text-max="300"
                                    data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></div>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label"> {{ __('Imagenes del Producto') }}</label>
                            </div>
                        </div>

                        <div style="display:none" id="fileupload-files" data-name="image_path[]">
                            @foreach ($inventory->images as $p)
                                @php($i = 0)
                                <input type="hidden" id="file_{{ $i }}" name="image_path[]"
                                    value="{{ $p->image_path }}">
                                @php($i++)
                            @endforeach
                        </div>
                        <div id="dropzone">
                            <div class="dz-message text-dark" data-dz-message>
                                <span class="retro">@lang('messages.image_sell_mobile')</span>
                            </div>
                        </div>

                        <br>
                        <div id="br-inventory-options">
                            <button id="br-publish-item" type="submit" class="btn text-white btn-warning btn-lg w-100"
                                type="submit">@lang('messages.updates')</button>

                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>






@endsection

@section('content-script-include')
    <script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>

    <script src="{{ asset('assets/dropzone-master/dist/min/dropzone.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/quagga/0.12.1/quagga.js"></script>
    <script src="{{ asset('vendor/sweetalert2/sweetalert2.all.min.js') }}"></script>
@endsection

@section('content-script')
    <script>
        var global_lang = [];
        global_lang['app.uploader_file_size_bigger'] = 'file bigger';
        global_lang['app.uploader_file_ext_not_supported'] = 'ext not supported';
    </script>
    <script>
        $(document).ready(function() {
            $('#grid').select2();
            $('#genero').select2();
            $('#location').select2();
            $('#generation').select2();
            $('#language').select2();
            $('#box_language').select2();
            $('#media').select2();
            $('#platform').select2();
            $('#region').select2();
        });
    </script>


    <script>
        // Obtiene el botón de actualización y el campo de cantidad
        const publishBtn = document.getElementById('br-publish-item');
        const quantityField = document.getElementById('quantity_field');

        // Agrega un evento 'input' al campo de cantidad
        quantityField.addEventListener('input', function() {
            if (this.value == 0) { // Si el valor es 0, deshabilita el botón de actualización
                publishBtn.disabled = true;
            } else { // Si el valor no es 0, habilita el botón de actualización
                publishBtn.disabled = false;
            }
        });
    </script>

    <script>
        const priceInput = document.querySelector('input[name="price"]');
        const updateButton = document.querySelector('#br-publish-item');

        priceInput.addEventListener('input', () => {
            const priceValue = parseFloat(priceInput.value);
            if (priceValue === 0 || isNaN(priceValue)) {
                updateButton.setAttribute('disabled', 'disabled');
            } else {
                updateButton.removeAttribute('disabled');
            }
        });
    </script>




    <script>
        var acceptedFileTypes = "image/*"; //dropzone requires this param be a comma separated list
        var fileList = new Array;
        var i = 0;
        var myDropzone = $("#dropzone").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('account/inventory/upload_file/' . $inventory->id) }}",
            addRemoveLinks: true,
            acceptedFiles: ".jpg,.jpeg,.png",
            removedfile: function(file) {
                var name = file.name;
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: "{{ url('account/inventory/delete_file/' . $inventory->id) }}",
                    data: {
                        filename: name
                    },
                    success: function(data) {
                        console.log("Se ha borrado correctamente");
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            @if (isset($inventory->images))
                maxFiles: 8 - {{ $prueba }},
            @else
                maxFiles: 8, //change limit as per your requirements
            @endif
            maxfilesexceeded: function(file) {
                Swal.fire({
                    title: '<span class="retro">Error</span>',
                    text: 'Haz Alcanzado el maximo de Imagenes (Max 8)',
                    footer: '<a class="retro" href="">¿Como Vender En RGM?</a>',
                    imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/a6cff967-2930-4785-a8b8-48a6e39101b8/dbrz0sh-36c848a3-b37d-4fd1-8d7e-90a27dcb8130.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2E2Y2ZmOTY3LTI5MzAtNDc4NS1hOGI4LTQ4YTZlMzkxMDFiOFwvZGJyejBzaC0zNmM4NDhhMy1iMzdkLTRmZDEtOGQ3ZS05MGEyN2RjYjgxMzAuZ2lmIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.QwzwRFsxRbUrhacQuJV4fSAij3GxImG_4dkxWDTgkMc',
                    imageWidth: 400,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                })
                this.removeFile(file);
            },
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Arrastra archivos aquí o haz clic para subir.", // Cambiar el mensaje predeterminado
            dictRemoveFile: "Eliminar archivo", // Cambiar el texto para eliminar archivo
            dictCancelUpload: "Cancelar subida", // Cambiar el texto para cancelar subida
            dictMaxFilesExceeded: "Ha alcanzado el límite de archivos permitidos.", // Cambiar el texto para archivos excedidos
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileList[i] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": i
                    };
                    input = $('#fileupload-files').data('name');
                    $('#fileupload-files').append(
                        `<input type="hidden" id="file_${ i }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    i += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileList.length; f++) {
                        if (fileList[f].fileName == file.name) {
                            rmvFile = fileList[f].serverFileName;
                            $("#file_" + fileList[f].fileId).remove();
                        }
                    }
                });

                @foreach ($inventory->images as $p)
                    var mockFile = {
                        name: "{{ $p->image_path }}",
                        size: 12345
                    };

                    myDropzone.emit("addedfile", mockFile);

                    myDropzone.emit("thumbnail", mockFile, "/uploads/inventory-images/{{ $p->image_path }}");
                    myDropzone.emit("complete", mockFile);
                    fileList[i] = {
                        "serverFileName": "{{ $p->image_path }}",
                        "fileName": "{{ $p->image_path }}",
                        "fileId": i
                    };
                    i += 1;
                @endforeach

            }
        });

        var fileListEan = new Array;
        var j = 0;
        var myDropzone = $("#dropzone-ean").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('account/inventory/upload_file_ean/' . $inventory->id) }}",
            addRemoveLinks: true,

            removedfile: function(file) {
                var name = file.name;
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: "{{ url('account/inventory/delete_file_ean/' . $inventory->id) }}",
                    data: {
                        filename: name
                    },
                    success: function(data) {
                        console.log("Se ha borrado correctamente");
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            maxFiles: 4, //change limit as per your requirements
            maxfilesexceeded: function(file) {
                Swal.fire({
                    title: '<span class="retro">Error</span>',
                    text: 'Haz Alcanzado el maximo de Imagenes (Max 4)',
                    footer: '<a class="retro" href="">¿Como Vender En RGM?</a>',
                    imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/a6cff967-2930-4785-a8b8-48a6e39101b8/dbrz0sh-36c848a3-b37d-4fd1-8d7e-90a27dcb8130.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2E2Y2ZmOTY3LTI5MzAtNDc4NS1hOGI4LTQ4YTZlMzkxMDFiOFwvZGJyejBzaC0zNmM4NDhhMy1iMzdkLTRmZDEtOGQ3ZS05MGEyN2RjYjgxMzAuZ2lmIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.QwzwRFsxRbUrhacQuJV4fSAij3GxImG_4dkxWDTgkMc',
                    imageWidth: 400,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                })
                this.removeFile(file);
            },
            acceptedFiles: ".jpg,.jpeg,.png",
            dictInvalidFileType: "No puedes subir archivos de este tipo , solo Imagenes tipo PNG O JPG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Arrastra archivos aquí o haz clic para subir.", // Cambiar el mensaje predeterminado
            dictRemoveFile: "Eliminar archivo", // Cambiar el texto para eliminar archivo
            dictCancelUpload: "Cancelar subida", // Cambiar el texto para cancelar subida
            dictMaxFilesExceeded: "Ha alcanzado el límite de archivos permitidos.", // Cambiar el texto para archivos excedidos
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileListEan[j] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": j
                    };
                    input = $('#fileupload-files-ean').data('name');
                    $('#ffileupload-files-ean').append(
                        `<input type="hidden" id="file_${ j }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    j += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileListEan.length; f++) {
                        if (fileListEan[f].fileName == file.name) {
                            rmvFile = fileListEan[f].serverFileName;
                            $("#file_" + fileListEan[f].fileId).remove();
                        }
                    }
                });

                @foreach ($inventory->imagesean as $o)
                    var mockFile = {
                        name: "{{ $o->image_path }}",
                        size: 12345
                    };

                    myDropzone.emit("addedfile", mockFile);

                    myDropzone.emit("thumbnail", mockFile, "/uploads/ean-images/{{ $o->image_path }}");
                    myDropzone.emit("complete", mockFile);
                    fileListEan[j] = {
                        "serverFileName": "{{ $o->image_path }}",
                        "fileName": "{{ $o->image_path }}",
                        "fileId": j
                    };
                    j += 1;
                @endforeach

            }
        });
    </script>



    <script>
        $('.openean').click(function() {
            $('.imageean').toggle();
        });

        function startScanner() {

            // heigth_image = document.getElementsByName("br_c_comments");
            heigth_image = document.getElementById("br-inventory-details");


            $('.imagesca').toggle();
            Quagga.init({
                inputStream: {
                    name: "Live",
                    type: "LiveStream",
                    target: document.querySelector('#data-data'),
                    locate: "True",
                    constraints: {
                        width: heigth_image.clientWidth,
                        height: 320,
                        facingMode: "environment"
                    },
                },
                decoder: {
                    readers: [
                        "code_128_reader",
                        "ean_reader",
                        "ean_8_reader",
                        "code_39_reader",
                        "code_39_vin_reader",
                        "codabar_reader",
                        "upc_reader",
                        "upc_e_reader",
                        "i2of5_reader"
                    ],
                },

            }, function(err) {
                if (err) {
                    // console.log(err);
                    return
                }

                Quagga.start();

                // Set flag to is running
                _scannerIsRunning = true;
            });

            Quagga.onDetected(function(result) {
                $("#ean").val(result.codeResult.code);
            });
        }

        function stopscan() {
            $('.imagesca').toggle();
            console.log(Quagga);
            Quagga.stop();
        }
    </script>
@endsection
