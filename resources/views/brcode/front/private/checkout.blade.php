@extends('brcode.front.layout.app')
@section('content-css-include')

@endsection
@section('content')
<!-- Content Header (Page header) -->
@php($total = 0)
@if(count($viewData['products_in_cart']) > 0)
  @foreach($viewData['products_in_cart'] as $cartItem)
    @php( $total += $cartItem->price * $cartItem->qty )
  @endforeach
@endif
<div class="row">
  <div class="offset-lg-1 col-lg-1 offset-md-1 col-md-1">
  </div>
  <div class="offset-lg-1 col-lg-10 offset-md-1 col-md-10">

      <h2>Payment Options</h2>
      <hr>
        <div class="row">
          <div class="col-lg-6">
            <h5>Your current Cardmarket credit:</h5>
            <h2><b>{{ Auth::user()->cash }} €</b></h2>
          </div>
          <div class="col-lg-6">
            <h5>Payable amount:</h5>
            <h2><b>{{ number_format($total, 2, ',', ' ') }} €</b></h2>
          </div>
        </div>
        <h5>Please select one of the following options to credit your Cardmarket account.</h5>
        <h2><b>Normal</b></h2>
        <hr>
      </div>
  </div>


  <div class="offset-lg-1 col-lg-1 offset-md-1 col-md-1">
  </div>

  <div class="panel panel-default col-lg-10 offset-lg-1 col-md-10 offset-md-1">
    <div class="panel-body">
      <div class="row">
        <div class="col-lg-2 text-center">
          <i class="fa fa-bank fa-5x"></i>
        </div>
        <div class="col-lg-10">
          <h2> <b> Bank Transfer </b></h2>
          <h5>Credit to your Cardmarket account manually via bank transfer.</h5>
          <br>
          <h4><i class="fa fa-clock-o" aria-hidden="true"></i> 2-3 Work Days <i class="fa fa-money" aria-hidden="true"></i> Free of charge (0.00 €)</h4>
          <br>
          <div class="col-lg-3">
            <h5>Recipient</h5>
            <h5>IBAN</h5>
            <h5>BIC</h5>
            <h5>Transfer Reason</h5>
            <h5>Address of the Bank</h5>
          </div>
          <div class="col-lg-9">
            <h5>Martketplace</h5>
            <h5>DEXXXXXXXXXXXXXXXXXX</h5>
            <h5>COBAXXXXXX</h5>
            <h5>{{ Auth::user()->concept }}</h5>
            <h5>Bank Name <br>Müllerstr. 180 <br>13353 Berlin  <br>Spain </h5>
                       
          </div>
          <br><br>
          <h5><span style="color:red">Attention:</span> Always indicate {{ Auth::user()->concept }} in the transfer reason (reference) field while wire transferring money to your cardmarket Account! Do not enter any other information in this field since it may slow down the payment process </h5>
          <h5><span style="color:red">Attention:</span>  Make sure that you (or your bank) use(s) an International Payment Order Form in conformity thie the SEPA (Single European Payment Area) agreement. Banks charge extra fees on any transactions done without the correct form.</h5>
          
        </div>
      </div>
    </div>
    </div>
  </div>

</div>

<!-- Main content -->
<section  class="content">
<div class="container">
  <div class="row">
    <div class="col-md-12">
    <!--
      <div class="account-title">{{ __('Formulario de pago')  }}</div>
      -->
    </div>
  </div><!-- /.row -->
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12" id="cart-container">
      @php($total = 0)
      @if(count($viewData['products_in_cart']) > 0)
			<div id="br-search-result-list" class="cart-items">
				<ul class="list-unstyled br-result-as-list">
				@foreach($viewData['products_in_cart'] as $cartItem)
                        @php( $total += $cartItem->price * $cartItem->qty )
				@endforeach
        @if($total > 0)
        <li>
          <div class="cart-total-container clearfix">
            <div>
            </div>
            <div>
              <div class="row">
                <div class="col-md-6 col-xs-12 cart-grand-total">
                  <div class="row cart-total-price-container">
                    <div class="col-md-12 col-xs-12 text-center">{{ __('Usar saldo') }}
                    </div>
                  </div>
                  <div class="row cart-buttons">
                    <div class="col-md-12 text-right">
                      <div id="">
                      <form action="{{ url('/cartCompleteCheck')}}" method="post">
                      {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary btn-block">{{ __('Pagar pedidos abiertos') }}</button>
                      </form>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-6 col-xs-12 cart-grand-total">
                  <div class="row cart-total-price-container">
                    <div class="col-md-12 col-xs-12 text-center">{{ __('Instantaneo') }}
                    </div>
                  </div>
                  <div class="row cart-buttons">
                    <div class="col-md-12 text-right">
                      <div id="paypal-button"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
        @endif
				</ul>
			</div>
      @else
      <span class="cart-empty">{{ __('No hay productos en tu carrito de compra. Agrega productos ingresando a las categorías del menu principal') }}</span>
      @endif
    </div><!-- /.col-@-10 -->
  </div><!-- /.row -->
</div><!-- /.container -->
</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script src="{{ asset('brcode/js/pty.components.js') }}"></script>
@endsection
@section('content-script')
<script>
paypal.Button.render({

    env: 'production', // sandbox | production

    // PayPal Client IDs - replace with your own
    // Create a PayPal app: https://developer.paypal.com/developer/applications/create
    client: {
        sandbox:    "{{ config('brcode.paypal_sb_client_id') }}",
        production: "{{ config('brcode.paypal_sb_client_id_live') }}"
    },

    // Show the buyer a 'Pay Now' button in the checkout flow
    commit: true,

    style: {
      label: 'buynow',
      fundingicons: true, // optional
      branding: true, // optional
      size: 'responsive'
    },

    // payment() is called when the button is clicked
    payment: function(data, actions) {

        // Make a call to the REST api to create the payment
        return actions.payment.create({
            payment: {
                transactions: [
                    {
                        amount: { total: '{{ number_format($total, 2, '.', '') }}', currency: '{{ config('brcode.paypal_currency') }}' }
                    }
                ]
            }
        });
    },

    // onAuthorize() is called when the buyer approves the payment
    onAuthorize: function(data, actions) {

      // console.log(1, data, actions);
      //
      // if (error === 'INSTRUMENT_DECLINED') {
      //     actions.restart();
      //     return;
      // }
      //
      // console.log(1, data, actions);

      // Set up a url on your server to execute the payment
      var EXECUTE_URL = '{{ url('/process_checkout') }}';

      // Set up the data you need to pass to your server
      var data = {
          _token: $('input[name="_token"]:eq(0)').val(),
          paymentID: data.paymentID,
          payerID: data.payerID,

      };

      // Make a call to your server to execute the payment
      return paypal.request.post(EXECUTE_URL, data)
      .then(function (res) {
          //window.alert('Payment Complete!');
          if(res.error == 0) {
            window.location.href = res.redirect_to;
          }
          else {
            if(res.message == 'low_quantity') {
              res.ids.forEach(function(e,i) {
                $('[data-cart-inventory-id="'+e+'"]').find('.cart-item-low-message').show()
                $('[data-cart-inventory-id="'+e+'"]').addClass('cart-item-low');
              });
            }
          }
      });

      // Make a call to the REST api to execute the payment
      // return actions.payment.execute().then(function(response) {
      //   console.log('Payment complete');
      //   //storePaypalResponse(response);
      // });
    },

    onCancel: function(data, actions) {
        return actions.redirect();
    },

    onError: function(err) {
        // Show an error page here, when an error occurs
    }

}, '#paypal-button');
$(document).ready(function() {

  $('.cart-update-price').click(function() {
    var parent = $( this ).closest('.row'),
       $qty = parent.find('.cart-quantity:eq(0)'),
       qty = $qty.val(),
       id  = $qty.data('inventory-id'),
       pId  = $qty.data('product-id');

    // console.log(qty, id, pId);
    //
    // var inventoryId = $( this ).data('inventory-id');
		// var inventoryQty = $( this ).closest('tr').find('select.br-btn-product-qty').val();

		$.showBigOverlay({message: '{{ __('Actualizando tu carro de compras') }}', onLoad: function() {
			$.ajax({
				url: '{{ url('/cart/add_item') }}',
				data: {
					_token: $('input[name="_token"]:eq(0)').val(),
					inventory_id: id,
					inventory_qty: qty,
				},
				dataType: 'JSON',
				type: 'POST',
				success: function(data) {
					if(data.error == 0) {
						// message.remove();
						// overlay.remove();
						$('#br-cart-items').text(data.items);
            parent.find('.cart-item-total-price').text(data.item_total);
            $('.cart-total-price:eq(0)').text(data.gran_total);
					}
					else {

					}
					$.showBigOverlay('hide');
				},
				error: function(data) {

				}
			})
		}});
  });

  $('.cart-delete-item').click(function() {
    var inventoryId = $( this ).data('inventory-id');
    var cartItem = $( this ).closest('.cart-item');
    $.showBigOverlay({message: '{{ __('Actualizando tu carro de compras') }}', onLoad: function() {
			$.ajax({
				url: '{{ url('/cart/add_item') }}',
				data: {
					_token: $('input[name="_token"]:eq(0)').val(),
					inventory_id: inventoryId,
					inventory_qty: 0,
				},
				dataType: 'JSON',
				type: 'POST',
				success: function(data) {
					if(data.error == 0) {
            var emptyMessage = $('<span/>').addClass('cart-empty').css({
              opacity: 0
            }).text('{{ __('No hay productos en tu carrito de compra. Agrega productos ingresando a las categorías del menu principal') }}');
            if($('.cart-item').length == 1) {
              $('#br-search-result-list').slideUp(250, function() {
                $( this ).remove();
                $('#cart-container').html('').append(emptyMessage);
                emptyMessage.animate({opacity: 1}, 250);
              });
            }
            else {
              cartItem.slideUp(250, function() {
                $( this ).remove();
              });
              $('.cart-total-price:eq(0)').text(data.gran_total);
            }
            $('#br-cart-items').text(data.items);

					}
					else {

					}
					$.showBigOverlay('hide');
				},
				error: function(data) {

				}
			})
		}});

  });

  $('.cart-quantity').change(function() {
    if(this.value != $(this).data('value'))
      $( this ).parent().find('.cart-update-price').show();
    else
      $( this ).parent().find('.cart-update-price').hide();
  });

  window.storePaypalResponse = function(response) {
    console.log(response);

    $.showBigOverlay({message: '{{ __('Preparing your order') }}', onLoad: function() {
        $.ajax({
  				url: '{{ url('/process_checkout') }}',
  				data: {
  					_token: $('input[name="_token"]:eq(0)').val(),
            response: response
  				},
  				dataType: 'JSON',
  				type: 'POST',
  				success: function(data) {
            console.log(1);
  					if(data.error == 0) {

  					}
  					else {

  					}
  					$.showBigOverlay('hide');
  				},
  				error: function(data) {
            $.showBigOverlay('hide');
  				}
  			});
      }
    });
  };

});
</script>
@endsection
