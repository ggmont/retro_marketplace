@extends('brcode.front.layout.app')
@section('content-css-include')
    <style>
        body {
            margin-top: 20px;
        }

        h3 {
            font-size: 16px;
        }

        .text-navy {
            color: #1ab394;
        }

        .cart-product-imitation {
            text-align: center;
            padding-top: 30px;
            height: 80px;
            width: 80px;
            background-color: #f8f8f9;
        }

        .product-imitation.xl {
            padding: 120px 0;
        }

        .product-desc {
            padding: 20px;
            position: relative;
        }

        .ecommerce .tag-list {
            padding: 0;
        }

        .ecommerce .fa-star {
            color: #d1dade;
        }

        .ecommerce .fa-star.active {
            color: #f8ac59;
        }

        .ecommerce .note-editor {
            border: 1px solid #e7eaec;
        }

        table.shoping-cart-table {
            margin-bottom: 0;
        }

        table.shoping-cart-table tr td {
            border: none;
            text-align: right;
        }

        table.shoping-cart-table tr td.desc,
        table.shoping-cart-table tr td:first-child {
            text-align: left;
        }

        table.shoping-cart-table tr td:last-child {
            width: 80px;
        }

        .ibox {
            clear: both;
            margin-bottom: 25px;
            margin-top: 0;
            padding: 0;
        }

        .ibox.collapsed .ibox-content {
            display: none;
        }

        .ibox:after,
        .ibox:before {
            display: table;
        }

        .ibox-title {
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            background-color: #ffffff;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 3px 0 0;
            color: inherit;
            margin-bottom: 0;
            padding: 14px 15px 7px;
            min-height: 48px;
        }

        .ibox-content {
            background-color: #ffffff;
            color: inherit;
            padding: 15px 20px 20px 20px;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 1px 0;
        }

        .ibox-footer {
            color: inherit;
            border-top: 1px solid #e7eaec;
            font-size: 90%;
            background: #ffffff;
            padding: 10px 15px;
        }

        /* width */
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #888;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555;
        }

        .scroller {
            width: 100%;
            max-width: 1120px;
            height: 800px;
            overflow: auto;
            overflow-x: hidden;
        }

    </style>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <div class="container">
        <div class="nk-gap-2"></div>
        <div class="nk-gap-2"></div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="nk-decorated-h-3">
                    <div class="section-title2">
                        <h3 class="retro">
                            {{ __('Calificacion de la compra') . ' - ' . $viewData['order_header']->order_identification }}
                            {{ $viewData['order_header']->status }} </b></h3>
                    </div>
                </h3>
            </div>
        </div><!-- /.row -->
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-md-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h3 class="text-2xl font-semibold text-gray-900 retro">Detalles del pedido <span
                                    class="text-main-1"> #</span></h3>
                        </div>
                        <div class="ibox-content">
                            <form
                                action="{{ url('/account/purchases/view/' . $viewData['order_header']->order_identification . '/score') }}"
                                method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="flex flex-wrap mb-2 -mx-3">
                                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label
                                            class="block mb-2 text-xs font-bold tracking-wide text-center text-gray-700 uppercase retro"
                                            for="grid-state">
                                            Velocidad de procesamiento
                                        </label>
                                        <br>
                                        <div class="relative text-xs retro small row">

                                            <label class="text-green-900">
                                                <input required type="radio" class="nes-radio" name="optionsProcess"
                                                    id="optionsProcess1" value="1"
                                                    {{ 1 == $viewData['rating']->processig ? 'checked="checked"' : '' }} />
                                                <span>{{ __('Bueno') }}</span>
                                            </label>

                                            <label class="text-yellow-900">
                                                <input type="radio" class="nes-radio" name="optionsProcess"
                                                    id="optionsProcess2" value="2"
                                                    {{ 2 == $viewData['rating']->processig ? 'checked="checked"' : '' }} />
                                                <span>{{ __('Neutral') }}</span>
                                            </label>

                                            <label class="text-red-900">
                                                <input type="radio" class="nes-radio" name="optionsProcess"
                                                    id="optionsProcess3" value="3"
                                                    {{ 3 == $viewData['rating']->processig ? 'checked="checked"' : '' }} />
                                                <span>{{ __('Malo') }}</span>
                                            </label>

                                        </div>
                                    </div>
                                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label
                                            class="block mb-2 text-xs font-bold tracking-wide text-center text-gray-700 uppercase retro"
                                            for="grid-state">
                                            Empaquetado
                                        </label>
                                        <br>
                                        <div class="relative text-xs retro small row">

                                            <label class="text-green-900">
                                                <input required type="radio" class="nes-radio" name="optionsPackage"
                                                    class="optionsPackage" id="optionsPackage1" value="1"
                                                    {{ 1 == $viewData['rating']->packaging ? 'checked="checked"' : '' }} />
                                                <span>{{ __('Bueno') }}</span>
                                            </label>

                                            <label class="text-yellow-900">
                                                <input type="radio" class="nes-radio" name="optionsPackage"
                                                    class="optionsPackage" id="optionsPackage2" value="2"
                                                    {{ 2 == $viewData['rating']->packaging ? 'checked="checked"' : '' }} />
                                                <span>{{ __('Neutral') }}</span>
                                            </label>

                                            <label class="text-red-900">
                                                <input type="radio" class="nes-radio" name="optionsPackage"
                                                    class="optionsPackage" id="optionsPackage3" value="3"
                                                    {{ 3 == $viewData['rating']->packaging ? 'checked="checked"' : '' }} />
                                                <span>{{ __('Malo') }}</span>
                                            </label>

                                        </div>


                                    </div>
                                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label
                                            class="block mb-2 text-xs font-bold tracking-wide text-center text-gray-700 uppercase retro"
                                            for="grid-state">
                                            Descripcion del Producto
                                        </label>
                                        <br>
                                        <div class="relative text-xs retro small row">
                                            <label class="text-green-900">
                                                <input required type="radio" class="nes-radio"
                                                    name="optionsDescription" class="optionsDescription"
                                                    id="optionsDescription1" value="1"
                                                    {{ 1 == $viewData['rating']->desc_prod ? 'checked="checked"' : '' }} />
                                                <span>{{ __('Bueno') }}</span>
                                            </label>

                                            <label class="text-yellow-900">
                                                <input type="radio" class="nes-radio" name="optionsDescription"
                                                    class="optionsDescription" id="optionsDescription2" value="2"
                                                    {{ 2 == $viewData['rating']->desc_prod ? 'checked="checked"' : '' }} />
                                                <span>{{ __('Neutral') }}</span>
                                            </label>

                                            <label class="text-red-900">
                                                <input type="radio" class="nes-radio" name="optionsDescription"
                                                    class="optionsDescription" id="optionsDescription3" value="3"
                                                    {{ 3 == $viewData['rating']->desc_prod ? 'checked="checked"' : '' }} />
                                                <span>Malo</span>
                                            </label>

                                        </div>
                                    </div>
                                </div>
                                <label
                                    class="block mb-2 text-xs font-bold tracking-wide text-center text-gray-700 uppercase retro"
                                    for="grid-state">
                                    Comentario
                                </label>

                                <textarea name="description" class="nes-textarea" rows="3" maxlength="350"
                                    required>{{ $viewData['rating']->description }}</textarea>
                                <div class="textarea-counter" data-text-max="350"
                                    data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button type="submit"
                                            class="nk-btn nk-btn-md nk-btn-rounded nes-btn is-primary btn-block">{{ __('Guardar') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('content-script-include')

    <script src="{{ asset('brcode/js/pty.components.js') }}"></script>
@endsection
