@extends('brcode.front.layout.app')
@section('content-css-include')
    <style>
        body {
            margin-top: 20px;
        }

        .modal-lg {
            width: 1100px;
        }


        h3 {
            font-size: 16px;
        }

        .text-navy {
            color: #1ab394;
        }

        .cart-product-imitation {
            text-align: center;
            padding-top: 30px;
            height: 80px;
            width: 80px;
            background-color: #f8f8f9;
        }

        .product-imitation.xl {
            padding: 120px 0;
        }

        .product-desc {
            padding: 20px;
            position: relative;
        }

        .ecommerce .tag-list {
            padding: 0;
        }

        .ecommerce .fa-star {
            color: #d1dade;
        }

        .ecommerce .fa-star.active {
            color: #f8ac59;
        }

        .ecommerce .note-editor {
            border: 1px solid #e7eaec;
        }

        table.shoping-cart-table {
            margin-bottom: 0;
        }

        table.shoping-cart-table tr td {
            border: none;
            text-align: right;
        }

        table.shoping-cart-table tr td.desc,
        table.shoping-cart-table tr td:first-child {
            text-align: left;
        }

        table.shoping-cart-table tr td:last-child {
            width: 80px;
        }

        .ibox {
            clear: both;
            margin-bottom: 25px;
            margin-top: 0;
            padding: 0;
        }

        .ibox.collapsed .ibox-content {
            display: none;
        }

        .ibox:after,
        .ibox:before {
            display: table;
        }

        .ibox-title {
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            background-color: #ffffff;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 3px 0 0;
            color: inherit;
            margin-bottom: 0;
            padding: 14px 15px 7px;
            min-height: 48px;
        }

        .ibox-content {
            background-color: #ffffff;
            color: inherit;
            padding: 15px 20px 20px 20px;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 1px 0;
        }

        .ibox-footer {
            color: inherit;
            border-top: 1px solid #e7eaec;
            font-size: 90%;
            background: #ffffff;
            padding: 10px 15px;
        }

        /* width */
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #888;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555;
        }

        .scroller {
            width: 100%;
            max-width: 1120px;
            height: 800px;
            overflow: auto;
            overflow-x: hidden;
        }
    </style>
@endsection
@section('content')

    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        @include('partials.flash')
        <div class="header-area" id="headerArea">
            <div class="container h-100 d-flex align-items-center justify-content-between">
                <!-- Back Button-->
                <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                <!-- Page Title-->
                <div class="page-heading">
                    <h6 class="mb-0 font-extrabold">{{ __('Detalles del Pedido') }}</h6>
                </div>
                <!-- Navbar Toggler-->

                <div class="normal">
                    @if (Auth::user())
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @else
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="page-content-wrapper">
            <div class="container">
                <button type="button" id="actualizar" style="display:none" class="btn btn-secondary hidden me-05 mb-1"
                    onclick="notification('notification-6' , 3000)">Auto Close (3s)</button>
                <button type="button" id="errorgm" style="display:none" class="btn btn-secondary hidden me-05 mb-1"
                    onclick="notification('notification-7' , 3000)">Auto Close (3s)</button>

                <button type="button" class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed"
                    data-toggle="modal" data-target="#problemModal">
                    <span class="fa-solid fa-triangle-exclamation"></span>
                </button>

                @php($showShippingSelect = $viewData['order_header']->seller->country_id != 46 || Auth::user()->country_id != 46)
                <div class="listview-title mt-2">@lang('messages.detail_order')</div>
                <ul class="listview image-listview">
                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">{{ __('ID') }}</div>
                                <span
                                    class="text-muted text-sm font-semibold">{{ $viewData['order_header']->order_identification }}</span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">{{ __('Comprador') }}</div>
                                <span
                                    class="text-muted text-sm font-semibold">{{ $viewData['order_header']->buyer->user_name }}</span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">{{ __('Artículos comprados') }}</div>
                                <span
                                    class="text-muted text-sm font-semibold">{{ $viewData['order_header']->quantity }}</span>
                            </div>
                        </div>
                    </li>
                    @if ($showShippingSelect)
                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">{{ __('Metodo') }} <br> De Envío</div>
                                <span class="text-muted text-sm font-semibold">
                                    @if ($viewData['order_shipping'])
                                        {{ $viewData['order_shipping']->name }} <br>
                                        Certificado: {{ $viewData['order_shipping']->certified }}
                                        <br>
                                        <b>Precio: {{ number_format($viewData['order_header']->shipping_price, 2) }}
                                        </b>
                                    @else
                                        -
                                    @endif
                                </span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">{{ __('Envío hacia') }}</div>
                                <span class="text-muted text-sm font-semibold">
                                    @lang('messages.name'): <b>{{ $viewData['user_shipping']->first_name }}
                                        {{ $viewData['user_shipping']->last_name }}</b> <br>
                                    @lang('messages.direction'): {{ $viewData['user_shipping']->address }} <br>
                                    @lang('messages.postal_code'): {{ $viewData['user_shipping']->zipcode }}<br>
                                    @lang('messages.country'): {{ $viewData['user_shipping']->country->name }}<br>
                                    @lang('messages.city_company'): {{ $viewData['user_shipping']->city }}<br>
                                </span>
                            </div>
                        </div>
                    </li>
                    @endif
                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">{{ __('Total') }} ({{ config('brcode.paypal_currency') }})
                                </div>
                                <span
                                    class="text-muted text-sm font-semibold">{{ number_format($viewData['order_header']->total, 2) }}</span>
                            </div>
                        </div>
                    </li>
                    @if ($viewData['order_header']->seller->user_name == auth()->user()->user_name)
                    @else
                        @if ($viewData['order_header']->status == 'RP')
                            <?php
                            $valor1 = number_format($viewData['total_paypal'], 2, '.', '');
                            $valor2 = Auth::user()->cash;
                            $resta = $valor1 - $valor2;
                            ?>
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">{{ __('Total a pagar en Paypal') }}
                                            ({{ config('brcode.paypal_currency') }}) <br>

                                        </div>
                                        <span class="text-muted text-sm font-semibold">
                                            {{ number_format($viewData['total_paypal'], 2, '.', '') }}
                                            {{ Auth::user()->cash > 0 ? ' - ' . Auth::user()->cash . '' : '' }} =
                                            <br>
                                            {{ $resta }}
                                            {{ Auth::user()->special_cash > 0 ? ' - ' . Auth::user()->special_cash . ' Saldo especial' : '' }}</span>
                                    </div>
                                </div>
                            </li>
                        @endif
                    @endif
                    <li>
                        <div class="item" bis_skin_checked="1">
                            <div class="in" bis_skin_checked="1">
                                <div bis_skin_checked="1">{{ __('Estado') }}
                                </div>
                                <span class="text-ultra text-sm font-semibold">
                                    {{ __($viewData['order_header']->statusDes) }}</span>
                            </div>
                        </div>
                    </li>
                </ul>




                @if (in_array($viewData['order_header']->status, ['CW', 'PC']))
                    <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                    @if ($viewData['order_header']->cancelOrder->solicited_by == 'S')
                        @php($dates = date('Y-m-d', strtotime($viewData['order_header']->created_on)))
                        @php($prueba = date('Y-m-d', strtotime($dates . ' + 2 days')))
                        @php($prueba2 = date('Y-m-d'))
                        @if ($prueba <= $prueba2)
                            <ul class="listview image-listview media">
                                <li>
                                    <a href="#" id="cancelAutomatic" class="cancelAutomatic item">
                                        <div class="in">
                                            <div>
                                                @lang('messages.cancel_automatic')
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        @else
                            <ul class="listview link-listview">
                                <li>
                                    <a href="#" id="cancelSoli" class="cancelSoli item">
                                        @lang('messages.the_cancel_soli')
                                        <span class="text-muted">@lang('messages.see_reason')</span>
                                    </a>
                                </li>
                            </ul>
                        @endif
                    @else
                        <ul class="listview image-listview media">
                            <li>
                                <a href="#" id="cancelPay" class="cancelPay item">
                                    <div class="in">
                                        <div>
                                            @if ($viewData['order_header']->status == 'PC')
                                                @lang('messages.the_purcharser') <b>@lang('messages.the_purcharser_two')</b> @lang('messages.the_purcharser_what') <br>
                                                @lang('messages.the_purcharser_three')
                                            @else
                                                @lang('messages.the_purcharser_four')
                                            @endif
                                        </div>
                                        <span class="text-muted text-sm font-semibold">
                                            @lang('messages.the_see')
                                        </span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    @endif
                @endif

                @if ($viewData['order_header']->status == 'CR' || $viewData['order_header']->status == 'PD')
                @elseif($viewData['order_header']->status == 'DD' || $viewData['order_header']->status == 'ST')
 
                @endif










                @if (
                    $viewData['order_header']->status == 'CR' ||
                        $viewData['order_header']->status == 'DD' ||
                        $viewData['order_header']->status == 'RP')

                    <!---->


                    @if ((isset($viewData['order_header']->cancelOrder) ? $viewData['order_header']->cancelOrder->count() : 0) == 0)
                        @if (in_array($viewData['order_header']->status, ['CR', 'RP']))
                            @php($dates = date('Y-m-d', strtotime($viewData['order_header']->created_on)))
                            @php($prueba = date('Y-m-d', strtotime($dates . ' + 2 days')))
                            @php($prueba2 = date('Y-m-d'))
                            @if ($prueba <= $prueba2)
                                <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                                <ul class="listview image-listview media">
                                    <li>
                                        <a href="#" id="cancelAutomatic" class="cancelAutomatic item">
                                            <div class="in">
                                                <div>
                                                    @lang('messages.cancel_automatic')
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            @else
                                <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                                <ul class="listview image-listview media">
                                    <li>
                                        <a href="#" id="cancelPay" class="cancelPay item">
                                            <div class="in">
                                                <div>
                                                    @lang('messages.cancel_the_sale')
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            @endif
                        @endif
                    @else
                        @if ($viewData['order_header']->cancelOrder->solicited_by == 'S')
                            @php($dates = date('Y-m-d', strtotime($viewData['order_header']->created_on)))
                            @php($prueba = date('Y-m-d', strtotime($dates . ' + 2 days')))
                            @php($prueba2 = date('Y-m-d'))
                            @if ($prueba <= $prueba2)
                                <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                                <ul class="listview image-listview media">
                                    <li>
                                        <a href="#" id="cancelAutomatic" class="cancelAutomatic item">
                                            <div class="in">
                                                <div>
                                                    @lang('messages.cancel_automatic')
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            @endif
                        @else
                            <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                            <ul class="listview image-listview media">
                                <li>
                                    <div class="item" bis_skin_checked="1">
                                        <div class="in" bis_skin_checked="1">
                                            <div bis_skin_checked="1"> @lang('messages.cancel_the_sale') </div>
                                            <span class="text-muted"> <span
                                                    class="text-red-900">@lang('messages.i_refuse')</span></span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        @endif
                    @endif


                    <!---->

                @endif





                @if ($viewData['order_header']->refund)
                    @if ((isset($viewData['order_header']->cancelOrder) ? $viewData['order_header']->cancelOrder->count() : 0) == 0)
                        <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                    @else
                    @endif
                    @if ($viewData['order_header']->refund->paid_out == 'P' && $viewData['order_header']->refund->status == 'RF')
                        @if ($viewData['order_header']->status == 'CN')
                        @else
                            <ul class="listview image-listview media">
                                <li>
                                    <div class="item" bis_skin_checked="1">
                                        <div class="in" bis_skin_checked="1">
                                            <div bis_skin_checked="1"><span class="text-gray-900">@lang('messages.i_refund')
                                                    :</span><br>
                                                <span
                                                    class="text-green-900">{{ number_format($viewData['order_header']->refund->total, 2) }}
                                                    €</span>
                                            </div>
                                            <span class="text-muted">@lang('messages.extra_soli_aprob_three'): <br> <span class="text-red-900">
                                                    {{ $viewData['order_header']->buyer->user_name }}
                                                </span> </span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        @endif
                    @elseif($viewData['order_header']->refund->paid_out == 'Y' && $viewData['order_header']->refund->status == 'RF')
                        <ul class="listview image-listview media">
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">@lang('messages.i_refund') :<br> <span
                                                class="text-green-900">{{ number_format($viewData['order_header']->refund->total, 2) }}
                                                €</span></div>
                                        <span class="text-muted"> <span
                                                class="text-green-900">@lang('messages.i_accept')</span></span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    @elseif($viewData['order_header']->refund->paid_out == 'N' && $viewData['order_header']->refund->status == 'RF')
                        <ul class="listview image-listview media">
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">@lang('messages.i_refund') :<br> <span
                                                class="text-green-900">{{ number_format($viewData['order_header']->refund->total, 2) }}
                                                €</span></div>
                                        <span class="text-muted"><span class="text-uppercase text-red-900">
                                                @lang('messages.i_refuse')
                                            </span> <br>
                                            @lang('messages.cash_return')</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    @endif
                @else
                    @if ($viewData['order_header']->status == 'CN')
                    @elseif($viewData['order_header']->status == 'DD')
                        @if ((isset($viewData['order_header']->cancelOrder) ? $viewData['order_header']->cancelOrder->count() : 0) == 0)
                           
                        @else
                        @endif
 
                    @endif
                @endif

                @if ($viewData['order_header']->extra)
                    @if ($viewData['order_header']->extra->paid_out == 'Y' && $viewData['order_header']->extra->status == 'RE')
                        @if ($viewData['order_header']->status == 'DD')
                        @else
                            <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                        @endif

                        <ul class="listview image-listview media">
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">@lang('messages.extra_soli_aprob_two') :<br> <span
                                                class="text-green-900">{{ number_format($viewData['order_header']->extra->total, 2) }}
                                                €</span></div>
                                        <span class="text-muted"> <span
                                                class="text-red-900">@lang('messages.i_accept')</span></span>
                                    </div>
                                    </>
                            </li>
                        </ul>
                    @elseif($viewData['order_header']->extra->paid_out == 'N' && $viewData['order_header']->extra->status == 'RE')
                        @if ($viewData['order_header']->status == 'DD')
                        @elseif ($viewData['order_header']->status == 'RP')

                        @elseif ($viewData['order_header']->status == 'ST')
                            <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                        @elseif((isset($viewData['order_header']->cancelOrder) ? $viewData['order_header']->cancelOrder->count() : 0) == 0)
                        @else
                            <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                        @endif

                        <ul class="listview image-listview media">
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">@lang('messages.extra_soli_aprob_two') :<br> <span
                                                class="text-green-900">{{ number_format($viewData['order_header']->extra->total, 2) }}
                                                €</span></div>
                                        <span class="text-muted"> <span class="text-red-900">@lang('messages.i_refuse')</span><br>
                                            @lang('messages.extra_refuse')
                                        </span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    @elseif($viewData['order_header']->extra->paid_out == 'PE')
                        @if ($viewData['order_header']->status == 'ST')
                            <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                            <ul class="listview image-listview media">
                                <li>
                                    <div class="item" bis_skin_checked="1">
                                        <div class="in" bis_skin_checked="1">
                                            <div bis_skin_checked="1">@lang('messages.extra_soli_aprob_two') :<br> <span
                                                    class="text-green-900">{{ number_format($viewData['order_header']->extra->total, 2) }}
                                                    €</span></div>
                                            <span class="text-muted"> <span
                                                    class="text-red-900">@lang('messages.i_accept')</span><br>
                                                @lang('messages.wait_order')
                                            </span>
                                        </div>

                                </li>
                            </ul>
                        @elseif($viewData['order_header']->status == 'CN')
                            <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                            <ul class="listview image-listview media">
                                <li>
                                    <div class="item" bis_skin_checked="1">
                                        <div class="in" bis_skin_checked="1">
                                            <div bis_skin_checked="1">@lang('messages.extra_soli_aprob_two') :<br> <span
                                                    class="text-green-900">{{ number_format($viewData['order_header']->extra->total, 2) }}
                                                    €</span></div>
                                            <span class="text-muted"> <span
                                                    class="text-red-900">@lang('messages.the_cancel_two')</span><br>
                                                @lang('messages.cash_return')
                                            </span>
                                        </div>
                                        </>
                                </li>
                            </ul>
                        @else
                            @if ((isset($viewData['order_header']->cancelOrder) ? $viewData['order_header']->cancelOrder->count() : 0) == 0)
                            @else
                            @endif

                            <ul class="listview image-listview media">
                                <li>
                                    <div class="item" bis_skin_checked="1">
                                        <div class="in" bis_skin_checked="1">
                                            <div bis_skin_checked="1">@lang('messages.extra_soli_aprob_two') :<br> <span
                                                    class="text-green-900">{{ number_format($viewData['order_header']->extra->total, 2) }}
                                                    €</span></div>
                                            <span class="text-muted"> <span
                                                    class="text-red-900">@lang('messages.i_accept')</span><br>@lang('messages.wait_order')</span>
                                        </div>
                                        </>
                                </li>
                            </ul>
                        @endif
                    @elseif($viewData['order_header']->extra->paid_out == 'P' && $viewData['order_header']->extra->status == 'RE')
                        @if ($viewData['order_header']->status == 'PC')
                            <ul class="listview image-listview media">
                                <li>
                                    <div class="item" bis_skin_checked="1">
                                        <div class="in" bis_skin_checked="1">
                                            <div bis_skin_checked="1">@lang('messages.extra_soli_aprob_two') :<br> <span
                                                    class="text-green-900">{{ number_format($viewData['order_header']->extra->total, 2) }}
                                                    €</span></div>
                                            <span class="text-muted"> <span
                                                    class="text-red-900">{{ $viewData['order_header']->buyer->user_name }}</span>
                                                <br> @lang('messages.i_waiting')</span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        @elseif($viewData['order_header']->status == 'CN')
                            <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                            <ul class="listview image-listview">
                                <li>
                                    <div class="item" bis_skin_checked="1">
                                        <div class="in" bis_skin_checked="1">
                                            <div bis_skin_checked="1">@lang('messages.extra_soli_aprob_two') : <br>
                                                {{ number_format($viewData['order_header']->extra->total, 2) }} €
                                            </div>
                                            <span class="text-muted text-sm font-semibold">
                                                @lang('messages.refuse_purchase')
                                            </span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        @elseif ((isset($viewData['order_header']->cancelOrder) ? $viewData['order_header']->cancelOrder->count() : 0) == 0)
                            <ul class="listview image-listview media">
                                <li>
                                    <a href="#" id="extraPay" class="extraPay item" bis_skin_checked="1">
                                        <div class="in" bis_skin_checked="1">
                                            <div bis_skin_checked="1">@lang('messages.extra_soli_aprob_two') :<br> <span
                                                    class="text-green-900">{{ number_format($viewData['order_header']->extra->total, 2) }}
                                                    €</span></div>
                                            <span class="text-muted"> <span
                                                    class="text-red-900">{{ $viewData['order_header']->buyer->user_name }}</span>
                                                <br> @lang('messages.i_waiting')</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        @elseif($viewData['order_header']->status == 'CR')
                            @if ((isset($viewData['order_header']->cancelOrder) ? $viewData['order_header']->cancelOrder->count() : 0) == 0)
                                <div class="listview-title mt-2">@lang('messages.soli_two')</div>
                            @else
                            @endif
                            <ul class="listview image-listview media">
                                <li>
                                    <a href="#" id="extraPay" class="extraPay item" bis_skin_checked="1">
                                        <div class="in" bis_skin_checked="1">
                                            <div bis_skin_checked="1">@lang('messages.extra_soli_aprob_two') :<br> <span
                                                    class="text-green-900">{{ number_format($viewData['order_header']->extra->total, 2) }}
                                                    €</span></div>
                                            <span class="text-muted"> <span
                                                    class="text-red-900">{{ $viewData['order_header']->buyer->user_name }}</span>
                                                <br> @lang('messages.i_waiting')</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        @else
                            <ul class="listview image-listview media">
                                <li>
                                    <a href="#" id="extraPay" class="extraPay item" bis_skin_checked="1">
                                        <div class="in" bis_skin_checked="1">
                                            <div bis_skin_checked="1">@lang('messages.extra_soli_aprob_two') :<br> <span
                                                    class="text-green-900">{{ number_format($viewData['order_header']->extra->total, 2) }}
                                                    €</span></div>
                                            <span class="text-muted"> <span
                                                    class="text-red-900">{{ $viewData['order_header']->buyer->user_name }}</span>
                                                <br> @lang('messages.i_waiting')</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        @endif
                    @endif
                @endif



                @if ($viewData['order_header']->rating)
                    @if ($viewData['order_header']->rating->processig > 0)
                        <div class="listview-title mt-2">{{ __('Valoración') }}</div>
                        <ul class="listview image-listview">
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">{{ __('Velocidad de procesamiento') }}
                                        </div>
                                        <span class="text-muted text-sm font-semibold">
                                            {{ $viewData['order_header']->rating->pro }}</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">{{ __('Empaquetado') }}
                                        </div>
                                        <span class="text-muted text-sm font-semibold">
                                            {{ $viewData['order_header']->rating->pac }}</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">{{ __('Descripción') }}
                                        </div>
                                        <span class="text-muted text-sm font-semibold">
                                            {{ $viewData['order_header']->rating->des }}</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <ul class="listview image-listview">
                            <li>
                                <div class="item">

                                    <div class="in">
                                        <div>
                                            <header> {{ __('Comentario') }}</header>
                                            {{ $viewData['order_header']->rating->description }}
                                        </div>

                                    </div>
                                </div>
                            </li>



                        </ul>
                    @else
                        <div class="listview-title mt-2">{{ __('Valoración') }}</div>
                        <ul class="listview image-listview">
                            <li>
                                <div class="item">

                                    <div class="in">
                                        <div>
                                            {{ __('Sin Calificar') }}
                                        </div>
                                        <span class="text-muted">@lang('messages.rating_wait')</span>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    @endif
                @endif








                @if (isset($viewData['order_header']->cancelOrder))
                    @if ($viewData['order_header']->cancelOrder->answer == 'Y' || $viewData['order_header']->cancelOrder->answer == 'N')
                        <div class="listview-title mt-2">{{ __('Solicitud de Cancelacion') }}</div>
                        <ul class="listview image-listview">
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">{{ __('Solicitado por :') }}</div>
                                        <span class="text-muted text-sm font-semibold">
                                            {{ $viewData['order_header']->cancelOrder->solicited_by == 'B' ? strtoupper($viewData['order_header']->buyer->user_name) : strtoupper($viewData['order_header']->seller->user_name) }}</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">{{ __('Razon :') }}</div>
                                        <span
                                            class="text-muted text-sm font-semibold">{{ $viewData['order_header']->cancelOrder->cause }}</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <ul class="listview image-listview">
                            <li>
                                <div class="item">
                                    <div class="in">
                                        <div>
                                            <header>{{ __('Respuesta :') }}</header>
                                            <span class="text-muted text-sm font-semibold">
                                                {{ $viewData['order_header']->cancelOrder->reason }}
                                            </span>
                                        </div>
                                        <span
                                            class="text-muted">{{ $viewData['order_header']->cancelOrder->answer == 'Y' ? 'Si' : 'No' }}</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    @endif
                @endif






                <div class="listview-title mt-2">{{ __('Detalles del Pedido') }}</div>







                @if (isset($viewData['order_details']))
                    @php($contador = 1)
                    <div class="listview-title mt-2" style="display: {{ $contador > 0 ? 'block' : 'none' }};"></div>

                    <div class="row g-3" style="display: {{ $contador > 0 ? 'block' : 'none' }};">
                        @foreach ($viewData['order_details'] as $detail)
                            @if ($detail->inventory->title !== null)
                                <div class="col-12 col-md-6">
                                    <div class="card weekly-product-card">
                                        <div class="card-body d-flex align-items-center">
                                            <div class="product-thumbnail-side">
                                                @if ($detail->inventory->images->first())
                                                    <a href="{{ url('/uploads/inventory-images/' . $detail->inventory->images->first()->image_path) }}"
                                                        class="fancybox product-thumbnail d-block" data-fancybox="RGM">
                                                        <img src="{{ url('/uploads/inventory-images/' . $detail->inventory->images->first()->image_path) }}"
                                                            alt="">
                                                    </a>
                                                @else
                                                    <a class="product-thumbnail d-block"><img
                                                            src="{{ asset('img/art-not-found.jpg') }}"
                                                            alt=""></a>
                                                @endif
                                            </div>
                                            <div class="product-description"><a class="product-title d-block"
                                                    href="#">
                                                    {{ str_limit($detail->inventory->title, 16) }} <br>
                                                    @lang('messages.cants') : {{ $detail->quantity }}
                                                </a>
                                                <p class="sale-price">{{ number_format($detail->price, 2) }}
                                                    €<span></span>
                                                </p>

                                                <div class="product-rating">
                                                    @if ($detail->inventory->comments == '')
                                                        <i class="fa-solid fa-circle-xmark" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    @else
                                                        <span data-toggle="popover" data-html="true"
                                                            data-content="{{ $detail->inventory->comments }}"
                                                            data-placement="top" data-trigger="hover"
                                                            class="badge-comment badge-info"><i
                                                                class="fa-solid fa-comment"></i>
                                                        </span>
                                                    @endif


                                                </div>

                                            </div>


                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="col-12 col-md-6">
                                    <div class="card weekly-product-card">
                                        <div class="card-body d-flex align-items-center">
                                            <div class="product-thumbnail-side">
                                                @if ($detail->inventory->images->first())
                                                    @if (str_starts_with($detail->inventory->images->first()->image_path, 'https'))
                                                        <a href="{{ $detail->inventory->images->first()->image_path }}"
                                                            class="fancybox product-thumbnail d-block"
                                                            data-fancybox="RGM">
                                                            <img src="{{ $detail->inventory->images->first()->image_path }}"
                                                                alt="">
                                                        </a>
                                                    @else
                                                        <a href="{{ url('/uploads/inventory-images/' . $detail->inventory->images->first()->image_path) }}"
                                                            class="fancybox product-thumbnail d-block"
                                                            data-fancybox="RGM">
                                                            <img src="{{ url('/uploads/inventory-images/' . $detail->inventory->images->first()->image_path) }}"
                                                                alt="">
                                                        </a>
                                                    @endif
                                                @elseif (count($detail->product->images) > 0)
                                                    @if (str_starts_with($detail->product->images[0]->image_path, 'https'))
                                                        <a href="{{ $detail->product->images[0]->image_path }}"
                                                            class="fancybox product-thumbnail d-block"
                                                            data-fancybox="RGM">
                                                            <img src="{{ $detail->product->images[0]->image_path }}"
                                                                alt="">
                                                        </a>
                                                    @else
                                                        <a href="{{ url('/images/' . $detail->product->images[0]->image_path) }}"
                                                            class="fancybox product-thumbnail d-block"
                                                            data-fancybox="RGM">
                                                            <img src="{{ url('/images/' . $detail->product->images[0]->image_path) }}"
                                                                alt="">
                                                        </a>
                                                    @endif
                                                @else
                                                    <a class="product-thumbnail d-block"><img
                                                            src="{{ asset('img/art-not-found.jpg') }}"
                                                            alt=""></a>
                                                @endif
                                            </div>
                                            <div class="product-description"><a class="product-title d-block"
                                                    href="/product/{{ $detail->product->id }}">
                                                    {{ str_limit($detail->product->name, 16) }} <br>
                                                    {{ $detail->product->platform }} <br>
                                                    {{ $detail->product->region }} <br>
                                                    @lang('messages.cants') : {{ $detail->quantity }}
                                                </a>
                                                <p class="sale-price">{{ number_format($detail->price, 2) }}
                                                    €<span></span>
                                                </p>

                                                @foreach (App\AppOrgCategory::whereIn('id', [1, 2, 3, 4, 172])->get() as $CatItem)
                                                    @if ($detail->product->catid == $CatItem->id)
                                                        @if ($CatItem->id == 1)
                                                            <div class="product-rating">
                                                                <img width="15px" src="/{{ $detail->inventory->box }}"
                                                                    data-toggle="popover"
                                                                    data-content="@lang('messages.box') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px"
                                                                    src="/{{ $detail->inventory->cover }}"
                                                                    data-toggle="popover"
                                                                    data-content="@lang('messages.cover') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->cover_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px"
                                                                    src="/{{ $detail->inventory->manual }}"
                                                                    data-toggle="popover"
                                                                    data-content="@lang('messages.the_manual') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->manual_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px" src="/{{ $detail->inventory->game }}"
                                                                    data-toggle="popover"
                                                                    data-content="@lang('messages.game_status') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px"
                                                                    src="/{{ $detail->inventory->extra }}"
                                                                    data-toggle="popover"
                                                                    data-content="@lang('messages.the_extra') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                            </div>
                                                            <div class="product-rating">

                                                                @if ($detail->inventory->comments == '')
                                                                    <i class="fa-solid fa-circle-xmark"
                                                                        data-toggle="popover"
                                                                        data-content="@lang('messages.not_working_profile')"
                                                                        data-placement="top" data-trigger="hover"></i>
                                                                @else
                                                                    <span data-toggle="popover" data-html="true"
                                                                        data-content="{{ $detail->inventory->comments }}"
                                                                        data-placement="top" data-trigger="hover"
                                                                        class="badge-comment badge-info"><i
                                                                            class="fa-solid fa-comment"></i>
                                                                    </span>
                                                                @endif

                                                                &nbsp;
                                                                <span data-toggle="popover" data-html="true"
                                                                    data-content="
                                                        <b>@lang('messages.state_inventory') :</b> <br />
                                                        @lang('messages.box') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }} <br />
                                                        @lang('messages.cover') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->cover_condition)) }} <br />
                                                        @lang('messages.the_manual') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->manual_condition)) }} <br />
                                                        @lang('messages.game_status') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }} <br />
                                                        @lang('messages.the_extra') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }} <br />"
                                                                    data-placement="top" data-trigger="hover"
                                                                    class="badge-inventory badge-info">ESTADO
                                                                </span>

                                                            </div>
                                                        @elseif($CatItem->id == 2)
                                                            <div class="product-rating">
                                                                <img width="15px" src="/{{ $detail->inventory->box }}"
                                                                    data-toggle="popover"
                                                                    data-content="@lang('messages.box') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px"
                                                                    src="/{{ $detail->inventory->cover }}"
                                                                    data-toggle="popover"
                                                                    data-content="Interior - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->cover_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px"
                                                                    src="/{{ $detail->inventory->manual }}"
                                                                    data-toggle="popover"
                                                                    data-content="@lang('messages.the_manual') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->manual_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px"
                                                                    src="/{{ $detail->inventory->game }}"
                                                                    data-toggle="popover"
                                                                    data-content="@lang('messages.game_status') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px"
                                                                    src="/{{ $detail->inventory->extra }}"
                                                                    data-toggle="popover"
                                                                    data-content="@lang('messages.the_extra') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px"
                                                                    src="/{{ $detail->inventory->inside }}"
                                                                    data-toggle="popover"
                                                                    data-content="@lang('messages.inside') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->inside_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                            </div>
                                                            <div class="product-rating">

                                                                @if ($detail->inventory->comments == '')
                                                                    <i class="fa-solid fa-circle-xmark"
                                                                        data-toggle="popover"
                                                                        data-content="@lang('messages.not_working_profile')"
                                                                        data-placement="top" data-trigger="hover"></i>
                                                                @else
                                                                    <span data-toggle="popover" data-html="true"
                                                                        data-content="{{ $detail->inventory->comments }}"
                                                                        data-placement="top" data-trigger="hover"
                                                                        class="badge-comment badge-info"><i
                                                                            class="fa-solid fa-comment"></i>
                                                                    </span>
                                                                @endif

                                                                &nbsp;
                                                                <span data-toggle="popover" data-html="true"
                                                                    data-content="
                                                    <b>@lang('messages.state_inventory') :</b> <br />
    
                                                    @lang('messages.box') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }} <br />
                                                    @lang('messages.cover') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->cover_condition)) }} <br />
                                                    @lang('messages.the_manual') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->manual_condition)) }} <br />
                                                    @lang('messages.console') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }} <br />
                                                    @lang('messages.the_extra') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }} <br />
                                                    @lang('messages.inside') - {{ __(App\AppOrgUserInventory::getCondicionName($s->inside_condition)) }} <br />"
                                                                    data-placement="top" data-trigger="hover"
                                                                    class="badge-inventory badge-info">ESTADO
                                                                </span>

                                                            </div>
                                                        @else
                                                            <div class="product-rating">
                                                                <img width="15px" src="/{{ $detail->inventory->box }}"
                                                                    data-toggle="popover"
                                                                    data-content="@lang('messages.box') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px"
                                                                    src="/{{ $detail->inventory->game }}"
                                                                    data-toggle="popover"
                                                                    data-content="@lang('messages.state_inventory') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px"
                                                                    src="/{{ $detail->inventory->extra }}"
                                                                    data-toggle="popover"
                                                                    data-content="@lang('messages.the_extra') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                            </div>
                                                            <div class="product-rating">

                                                                @if ($detail->inventory->comments == '')
                                                                    <i class="fa-solid fa-circle-xmark"
                                                                        data-toggle="popover"
                                                                        data-content="@lang('messages.not_working_profile')"
                                                                        data-placement="top" data-trigger="hover"></i>
                                                                @else
                                                                    <span data-toggle="popover" data-html="true"
                                                                        data-content="{{ $detail->inventory->comments }}"
                                                                        data-placement="top" data-trigger="hover"
                                                                        class="badge-comment badge-info"><i
                                                                            class="fa-solid fa-comment"></i>
                                                                    </span>
                                                                @endif

                                                                &nbsp;
                                                                <span data-toggle="popover" data-html="true"
                                                                    data-content="
                                                    <b>@lang('messages.state_inventory') :</b> <br />
    
                                                    @lang('messages.box') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }} <br />
                                                    @lang('messages.the_general') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }} <br />
                                                    @lang('messages.the_extra') - {{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }} <br />"
                                                                    data-placement="top" data-trigger="hover"
                                                                    class="badge-inventory badge-info">ESTADO
                                                                </span>

                                                            </div>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                @endif




                @if ($viewData['order_header']->status == 'CR')
                    <form action="{{ url('/account/sales/update') }}" class="pt-3" method="post">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" class="form-control" id="id" name="id"
                            value="{{ $viewData['order_header']->order_identification }}">
 



                        <button type="submit" class="btn btn-danger w-100 font-bold">@lang('messages.mark_send')</button>
                    </form>
                @endif



            </div>
        </div>


















        @if (
            $viewData['order_header']->status == 'CR' ||
                $viewData['order_header']->status == 'ST' ||
                $viewData['order_header']->status == 'DD')
            @if ($viewData['order_header']->extra)
                <div class="modal fade" id="ModalExtraPay" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog extra-pay-form" role="document">
                        <div class="modal-content bg-white">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="d-flex align-items-center justify-content-between mb-4">
                                        @if ($viewData['order_header']->extra->paid_out == 'Y' && $viewData['order_header']->extra->status == 'RE')
                                            <h4 class="modal-title">@lang('messages.extra_paid')</h4>
                                        @elseif($viewData['order_header']->extra->paid_out == 'N' && $viewData['order_header']->extra->status == 'RE')
                                            <h4 class="modal-title">@lang('messages.extra_paid')</h4>
                                        @elseif($viewData['order_header']->extra->paid_out == 'P' && $viewData['order_header']->extra->status == 'RE')
                                            <h4 class="modal-title">@lang('messages.extra_paid')</h4>
                                        @endif
                                        <button class="btn btn-close p-1 ms-auto me-0" class="close"
                                            data-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="wide-block pb-1 pt-2">
                                        @if ($viewData['order_header']->extra->paid_out == 'Y' && $viewData['order_header']->extra->status == 'RE')
                                            <span class="font-semibold text-base">
                                                <center> Has aceptado el pago extra por valor de
                                                    <span
                                                        class="text-green-900">{{ number_format($viewData['order_header']->extra->total, 2) }}
                                                        €</span>
                                                </center>
                                            </span>
                                        @elseif($viewData['order_header']->extra->paid_out == 'N' && $viewData['order_header']->extra->status == 'RE')
                                            <span class="font-semibold text-base">
                                                <center> @lang('messages.refuse_extra')
                                                    <span
                                                        class="text-green-900">{{ number_format($viewData['order_header']->extra->total, 2) }}
                                                        €</span>
                                                </center>
                                            </span>
                                        @elseif($viewData['order_header']->extra->paid_out == 'P' && $viewData['order_header']->extra->status == 'RE')
                                            <span class="font-semibold text-base">

                                                <center> El usuario
                                                    {{ $viewData['order_header']->buyer->user_name }}
                                                    @lang('messages.send_extra')
                                                    <span class="text-green-900">
                                                        {{ number_format($viewData['order_header']->extra->total, 2) }}
                                                        €
                                                    </span>
                                                </center>
                                                <br>


                                                <div class="row">
                                                    <div class="col-6">
                                                        <form action="{{ route('responseRefund') }}" method="POST">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" value="Y" name="response">
                                                            <input type="hidden"
                                                                value="{{ $viewData['order_header']->order_identification }}"
                                                                name="order">


                                                            <button class="pt-2 pb-2 btn-success btn-block"
                                                                type="submit"><span
                                                                    class="retro text-white font-extrabold">@lang('messages.aceppt')</span></button>
                                                        </form>
                                                    </div>
                                                    <div class="col-6">
                                                        <form action="{{ route('responseRefund') }}" method="POST">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" value="N" name="response">
                                                            <input type="hidden"
                                                                value="{{ $viewData['order_header']->order_identification }}"
                                                                name="order">
                                                            <button class="pt-2 pb-2 btn-danger btn-block"
                                                                type="submit"><span
                                                                    class="retro text-white font-extrabold">@lang('messages.recha_cash')</span></button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif

        @if (in_array($viewData['order_header']->status, ['CW', 'PC']))
            @if ($viewData['order_header']->cancelOrder->solicited_by == 'S')
                <div class="modal fade" id="modalCancelSoli" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog soli-form" role="document">
                        <div class="modal-content bg-white">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="d-flex align-items-center justify-content-between mb-4">
                                        <h4 class="modal-title">Solicitud de Cancelación</h4>
                                        <button class="btn btn-close p-1 ms-auto me-0" class="close"
                                            data-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="wide-block pb-1 pt-2">
                                        <center>@lang('messages.cancel_soli_aprob')</center>
                                        <br>
                                        <center><span class="text-lg font-extrabold">@lang('messages.the_reason')</span></center>

                                        <center>{{ $viewData['order_header']->cancelOrder->cause }}</center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif

        <div class="modal fade" id="modalCancelAutomatic" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog the-automatic-form" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-2">
                                <h4 class="modal-title">Cancelación automatica</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block">
                                <form
                                    action="{{ url('/account/sales/automatic/' . $viewData['order_header']->order_identification) }}"
                                    method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="optradio" value="Y">

                                    <input type="hidden" name="reason" value="Cancelación automatica">

                                    <div class="wide-block pb-1 pt-2">
                                        <span class="font-semibold text-sm">

                                            <center> Ya pasaron mas de 7 días , puedes cancelar este pedido automaticamente
                                                si lo deseas
                                            </center>
                                            <br>

                                            <div class="row">
                                                <div class="col-12">
                                                    <button type="submit"
                                                        class="btn btn-danger font-bold w-100">Cancelar</button>
                                                </div>
                                            </div>
                                        </span>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="problemModal" tabindex="-1" role="dialog" aria-labelledby="problemModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title">¿Tienes algun problema con esta orden?</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <form action="{{ route('problem-order') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id"
                                    value="{{ $viewData['order_header']->order_identification }}">
                                <div class="wide-block pb-1 pt-2">
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="address5">Describe lo que sucede</label>
                                            <textarea id="address5" rows="2" name="reason" required="" class="form-control"></textarea>
                                            <i class="clear-input">
                                                <ion-icon name="close-circle"></ion-icon>
                                            </i>
                                        </div>

                                    </div>
                                    <button type="submit" class="btn btn-danger font-bold w-100">ENVIAR</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @if (
            $viewData['order_header']->status == 'CR' ||
                $viewData['order_header']->status == 'ST' ||
                $viewData['order_header']->status == 'DD')
            @if ($viewData['order_header']->refund)
                <div class="modal fade" id="reemModalSoli" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog reem-soli-form" role="document">
                        <div class="modal-content bg-white">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="d-flex align-items-center justify-content-between mb-4">
                                        <h4 class="modal-title">@lang('messages.reem_soli')</h4>
                                        <button class="btn btn-close p-1 ms-auto me-0" class="close"
                                            data-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="wide-block pb-1 pt-2">
                                        @if ($viewData['order_header']->refund->paid_out == 'P' && $viewData['order_header']->refund->status == 'RF')
                                            <span class="font-semibold text-base">
                                                <center> Se envio un reembolso con el valor de
                                                    <span
                                                        class="text-green-900">{{ number_format($viewData['order_header']->refund->total, 2) }}
                                                        €</span>
                                                    <br>
                                                    En espera de aprobación por <b
                                                        class="text-uppercase text-red-900">{{ $viewData['order_header']->buyer->user_name }}
                                                    </b>
                                                </center>
                                            </span>
                                        @elseif($viewData['order_header']->refund->paid_out == 'Y' && $viewData['order_header']->refund->status == 'RF')
                                            <span class="font-semibold text-base">
                                                <center>
                                                    @lang('messages.sale_reembols')
                                                    <span class="text-green-900">
                                                        {{ number_format($viewData['order_header']->refund->total, 2) }}
                                                        €
                                                    </span>
                                                    @lang('messages.sale_reembols_aceppt')
                                                    <br>
                                                </center>
                                            </span>
                                        @elseif($viewData['order_header']->refund->paid_out == 'N' && $viewData['order_header']->refund->status == 'RF')
                                            <span class="font-semibold text-base">
                                                <center>
                                                    @lang('messages.sale_reembols')
                                                    <span class="text-green-900">
                                                        {{ number_format($viewData['order_header']->refund->total, 2) }}
                                                        €
                                                    </span>
                                                    @lang('messages.sale_reembols_recha')
                                                    <br>
                                                    @lang('messages.cash_order_reembols')
                                                </center>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif

        <div class="modal fade" id="modalSoliCancel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog soli-form" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-2">
                                <h4 class="modal-title">@lang('messages.thecancel_order')</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block">
                                <form
                                    action="{{ url('/account/sales/cancel/' . $viewData['order_header']->order_identification) }}"
                                    method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="address5">@lang('messages.areyousure_sell')..</label>
                                            <textarea name="reason" required rows="2" class="form-control"></textarea>
                                            <i class="clear-input">
                                                <ion-icon name="close-circle"></ion-icon>
                                            </i>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-danger font-bold w-100">Enviar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if (in_array($viewData['order_header']->status, ['CW', 'PC']))
            <div class="modal fade" id="modalCancel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog cancel-form" role="document">
                    <div class="modal-content bg-white">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="d-flex align-items-center justify-content-between mb-4">
                                    <h4 class="modal-title">Cancelar Pedido</h4>
                                    <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="wide-block pb-1 pt-2">
                                    <center><span class="text-lg font-extrabold">@lang('messages.the_reason')</span></center>

                                    <center>{{ $viewData['order_header']->cancelOrder->cause }}</center>


                                    <form
                                        action="{{ url('/account/sales/view/' . $viewData['order_header']->order_identification) }}"
                                        method="post">
                                        {{ csrf_field() }}
                                        <div class="d-md-none mb-4">
                                            <div class="row no-gutters align-items-center justify-content-end my-2"
                                                bis_skin_checked="1">
                                                <div class="col-12 pr-md-2 col-md text-center text-md-right"
                                                    bis_skin_checked="1">
                                                    <span class="personalInfo-light">
                                                        @lang('messages.order_accept_cancel')</span>
                                                </div>
                                                <div class="p-title-price text-center">
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-check mb-1">
                                                                <label>
                                                                    <input type="radio" class="nes-radio"
                                                                        name="optradio" value="Y" checked />
                                                                    <span class="retro">Si</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-check mb-1">
                                                                <label>
                                                                    <input type="radio" class="nes-radio"
                                                                        name="optradio" value="N" />
                                                                    <span class="retro">No</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group boxed">
                                            <div class="input-wrapper">
                                                <label class="form-label" for="address5">Añadir comentario</label>
                                                <textarea id="address5" rows="2" name="reason" required="" class="form-control"></textarea>
                                                <i class="clear-input">
                                                    <ion-icon name="close-circle"></ion-icon>
                                                </i>
                                            </div>
                                        </div>

                                        <button type="submit"
                                            class="btn confirmclosed btn-danger font-bold w-100 close-modal">
                                            <span class="text-lg font-extrabold">{{ __('Guardar') }}</span>
                                        </button>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="modal fade" id="modalCancel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog cancel-form" role="document">
                    <div class="modal-content bg-white">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="d-flex align-items-center justify-content-between mb-2">
                                    <h4 class="modal-title">Cancelar Pedido</h4>
                                    <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="wide-block">
                                    <form
                                        action="{{ url('/account/sales/cancel/' . $viewData['order_header']->order_identification) }}"
                                        method="post">
                                        {{ csrf_field() }}
                                        <div class="form-group boxed">
                                            <div class="input-wrapper">
                                                <label class="form-label" for="address5">Si no estas seguro de tu
                                                    venta..</label>
                                                <textarea name="reason" required rows="2" class="form-control"></textarea>
                                                <i class="clear-input">
                                                    <ion-icon name="close-circle"></ion-icon>
                                                </i>
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-danger font-bold w-100">Enviar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif







        <div class="modal fade" id="reemModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog reem-form" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-2">
                                <h4 class="modal-title">@lang('messages.reem_soli')</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block">
                                <form action="{{ route('requestRefund') }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id"
                                        value="{{ $viewData['order_header']->order_identification }}">
                                    <div class="mb-2 input-group">
                                        <input type="number" name="cash" autocomplete="off" step="0.01"
                                            placeholder="0.00" class="form-control" required>
                                        <div class="bg-white input-group-prepend">
                                            <div class="bg-white input-group-text">€</div>
                                        </div>
                                    </div>
                                    <span>Max. 100€</span>
                                    <div class="nk-gap"></div>
                                    <button type="submit"
                                        class="btn btn-danger font-bold w-100">@lang('messages.soli_send')</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="container">
            <div class="nk-gap-2"></div>
            <div class="nk-gap-2"></div>
            <div class="row">
                <div class="col-md-12">
                    <h3 class="nk-decorated-h-3">
                        <div class="section-title2">
                            <h3 class="retro">
                                @lang('messages.sale_detail') {{ ' - ' . $viewData['order_header']->order_identification }}
                                {{ $viewData['order_header']->status }} </b></h3>
                        </div>
                    </h3>
                </div>
            </div>
            @if (session('status'))
                <h3 class="text-xl md:text-2x1">
                    {{ session('status') }}
                </h3>
            @endif
            <div class="nk-gap"></div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <span class="retro">
                    @include('partials.flash')
                </span>
                <div class="row">
                    <div class="col-md-9">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h3 class="text-2xl font-extrabold text-gray-900 retro">@lang('messages.detail_order') <span
                                        class="text-main-1"> #</span></h3>
                            </div>
                            <div class="ibox-content">
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('ID') }}</span>
                                    <span class="text-sm font-semibold">
                                        {{ $viewData['order_header']->order_identification }}
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Comprador') }}</span>
                                    <span class="text-sm font-semibold">
                                        {{ $viewData['order_header']->buyer->user_name }}
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Artículos comprados') }}</span>
                                    <span class="text-sm font-semibold">
                                        {{ $viewData['order_header']->quantity }}
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">@lang('messages.send_method_two')</span>
                                    <span class="text-sm font-semibold">
                                        @if ($viewData['order_shipping'])
                                            <b>{{ $viewData['order_shipping']->name }}</b> <br>
                                            @lang('messages.certified_order'): {{ $viewData['order_shipping']->certified }} <br>
                                            @lang('messages.send_sale_order'): {{ $viewData['order_shipping']->max_weight }} g<br>
                                            Volume: L {{ $viewData['order_shipping']->large }} x W
                                            {{ $viewData['order_shipping']->weight }} x H
                                            {{ $viewData['order_shipping']->hight }} (mm)<br>
                                            <b>@lang('messages.price'):
                                                {{ number_format($viewData['order_header']->shipping_price, 2) }}</b>
                                        @else
                                            -
                                        @endif
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Envío hacia') }}</span>
                                    <span class="text-sm font-semibold">
                                        @lang('messages.name'): <b>{{ $viewData['user_shipping']->first_name }}
                                            {{ $viewData['user_shipping']->last_name }}</b> <br>
                                        @lang('messages.direction'): {{ $viewData['user_shipping']->address }} <br>
                                        @lang('messages.postal_code'): {{ $viewData['user_shipping']->zipcode }}<br>
                                        @lang('messages.country'): {{ $viewData['user_shipping']->country->name }}<br>
                                        @lang('messages.city_company'): {{ $viewData['user_shipping']->city }}<br>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">
                                        {{ __('Total') }}
                                        ({{ config('brcode.paypal_currency') }})
                                    </span>
                                    <span class="text-base font-extrabold text-green-900 retro">
                                        {{ number_format($viewData['order_header']->total, 2) }}
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Estado') }}</span>
                                    <span class="text-base font-extrabold text-red-900 retro">
                                        {{ __($viewData['order_header']->statusDes) }}
                                </div>
                                @if (
                                    $viewData['order_header']->status == 'CR' ||
                                        $viewData['order_header']->status == 'ST' ||
                                        $viewData['order_header']->status == 'DD')
                                    <div class="col-md-12">

                                        @if ($viewData['order_header']->refund)
                                            @if ($viewData['order_header']->refund->paid_out == 'P' && $viewData['order_header']->refund->status == 'RF')
                                                <br>
                                                <h5 class="text-center text-gray-900 retro text-xs">@lang('messages.reem_soli')
                                                </h5>
                                                <span class="font-semibold text-xs retro">
                                                    <center> Se envio un reembolso con el valor de
                                                        <span
                                                            class="text-green-900">{{ number_format($viewData['order_header']->refund->total, 2) }}
                                                            €</span>
                                                        <br>
                                                        En espera de aprobación por <b
                                                            class="text-uppercase text-red-900">{{ $viewData['order_header']->buyer->user_name }}
                                                        </b>
                                                    </center>
                                                </span>
                                            @elseif($viewData['order_header']->refund->paid_out == 'Y' && $viewData['order_header']->refund->status == 'RE')
                                                <br>
                                                <h5 class="pb-20 text-center retro">
                                                    <span class="text-gray-900">@lang('messages.accepted_extra')</span>
                                                    <span
                                                        class="text-green-900">{{ number_format($viewData['order_header']->refund->total, 2) }}
                                                        € </span><br>
                                                </h5>
                                            @elseif($viewData['order_header']->refund->paid_out == 'N' && $viewData['order_header']->refund->status == 'RE')
                                                <br>
                                                <h5 class="pb-20 text-center retro">
                                                    <span class="text-gray-900"> @lang('messages.refuse_extra')
                                                    </span>
                                                    <span
                                                        class="text-green-900">{{ number_format($viewData['order_header']->refund->total, 2) }}
                                                        € </span><br>
                                                </h5>
                                            @elseif($viewData['order_header']->refund->paid_out == 'P' && $viewData['order_header']->refund->status == 'RE')
                                                <br>
                                                <h5 class="pb-20 text-center text-gray-900 retro">Solicitud de
                                                    pago extra</h5>
                                                <span class="font-semibold text-lg">
                                                    <center> @lang('messages.the_user')
                                                        {{ $viewData['order_header']->buyer->user_name }}
                                                        @lang('messages.send_extra')
                                                        <span class="text-green-900">
                                                            {{ number_format($viewData['order_header']->refund->total, 2) }}
                                                            €
                                                        </span>
                                                    </center>
                                                    <br>
                                                </span>
                                                <div class="row">
                                                    <div class="col-6">
                                                        <form action="{{ route('responseRefund') }}" method="POST">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" value="Y" name="response">
                                                            <input type="hidden"
                                                                value="{{ $viewData['order_header']->order_identification }}"
                                                                name="order">

                                                            <button class="nk-btn nk-btn-lg nes-btn btn-block"
                                                                type="submit"><span
                                                                    class="text-gray-900 retro text-xs">Aceptar</span></button>
                                                        </form>
                                                    </div>
                                                    <div class="col-6">
                                                        <form action="{{ route('responseRefund') }}" method="POST">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" value="N" name="response">
                                                            <input type="hidden"
                                                                value="{{ $viewData['order_header']->order_identification }}"
                                                                name="order">
                                                            <button class="nk-btn nk-btn-lg nes-btn btn-block"
                                                                type="submit"><span
                                                                    class="text-gray-900 retro text-xs">Rechazar</span></button>
                                                        </form>
                                                    </div>
                                                </div>
                                            @elseif($viewData['order_header']->refund->paid_out == 'Y' && $viewData['order_header']->refund->status == 'RF')
                                                <h5 class="text-center text-gray-900 retro text-xs">@lang('messages.reem_soli')
                                                </h5>
                                                <span class="font-semibold text-xs retro">
                                                    <center>
                                                        @lang('messages.sale_reembols')
                                                        <span class="text-green-900">
                                                            {{ number_format($viewData['order_header']->refund->total, 2) }}
                                                            €
                                                        </span>
                                                        @lang('messages.sale_reembols_aceppt')
                                                        <br>
                                                    </center>
                                                </span>
                                            @elseif($viewData['order_header']->refund->paid_out == 'N' && $viewData['order_header']->refund->status == 'RF')
                                                <br>
                                                <h5 class="text-center text-gray-900 retro text-xs">@lang('messages.reem_soli')
                                                </h5>
                                                <span class="font-semibold text-xs retro">
                                                    <center>
                                                        @lang('messages.sale_reembols')
                                                        <span class="text-green-900">
                                                            {{ number_format($viewData['order_header']->refund->total, 2) }}
                                                            €
                                                        </span>
                                                        @lang('messages.sale_reembols_recha')
                                                        <br>
                                                        @lang('messages.cash_order_reembols')
                                                    </center>
                                                </span>
                                            @endif
                                        @else
                                            <br>
                                            <h5 class="text-lg text-center font-extrabold text-gray-900 retro">
                                                @lang('messages.reem_soli')</h5>
                                            <form action="{{ route('requestRefund') }}" method="post">
                                                {{ csrf_field() }}

                                                <input type="hidden" name="id"
                                                    value="{{ $viewData['order_header']->order_identification }}">

                                                <div class="mb-2 input-group">
                                                    <input type="number" name="cash" autocomplete="off"
                                                        step="0.01" placeholder="0.00" class="form-control" required>
                                                    <div class="bg-white input-group-prepend">
                                                        <div class="bg-white input-group-text">€</div>
                                                    </div>
                                                </div>
                                                <span>Max. 100€</span>
                                                <div class="nk-gap"></div>
                                                <button type="submit"
                                                    class="nk-btn text-lg text-center font-extrabold text-gray-900 nk-btn-lg nk-btn-rounded nes-btn retro btn-block"><span
                                                        class="text-gray-900">@lang('messages.soli_send')</span>
                                                </button>
                                            </form>
                                        @endif
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">


                        @if (in_array($viewData['order_header']->status, ['CW', 'PC']))
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5 class="pb-8 text-base font-extrabold text-gray-900 retro">
                                        {{ __('Solicitudes') }}
                                    </h5>
                                </div>
                                <div class="ibox-content">
                                    @if ($viewData['order_header']->cancelOrder->solicited_by == 'S')
                                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                            <label class="text-sm font-semibold uppercase">@lang('messages.the_traking')</label>
                                            <br>
                                            <label class="text-sm font-bold text-blue-900">
                                                {{ $viewData['order_header']->tracking_number }}
                                        </div>
                                    @else
                                        <form
                                            action="{{ url('/account/sales/view/' . $viewData['order_header']->order_identification) }}"
                                            method="post">
                                            {{ csrf_field() }}
                                            <div class="text-left col-md-12">
                                                @if ($viewData['order_header']->status == 'PC')
                                                    <span class="font-bold">El comprador ha <b>pagado</b> el pedido pero a
                                                        decidido cancelarlo
                                                    </span>
                                                @else
                                                    <span class="font-bold">El comprador ha decidido cancelar el
                                                        pedido
                                                    </span>
                                                @endif
                                                <br>
                                                <br>
                                                <div class="ibox-title">
                                                    <h5 class="text-xs text-gray-900 retro">Motivos</h5>
                                                </div>
                                                <p>{{ $viewData['order_header']->cancelOrder->cause }}</p>
                                                <label class="text-xs retro">@lang('messages.order_accept_cancel')</label>
                                                <br>

                                                <label>
                                                    <input type="radio" class="nes-radio" name="optradio"
                                                        value="Y" checked />
                                                    <span class="retro">Yes</span>
                                                </label>

                                                <label>
                                                    <input type="radio" class="nes-radio" name="optradio"
                                                        value="N" />
                                                    <span class="retro">No</span>
                                                </label>

                                                @if ($errors->has('optradio'))
                                                    <br>
                                                    <div class="alert alert-danger" role="alert">
                                                        @foreach ($errors->get('optradio') as $error)
                                                            {{ $error }}
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-lg-12">
                                                <br>
                                                <label class="text-xs text-gray-900 retro">Razon</label>
                                                <textarea name="reason" required cols="30" rows="3"
                                                    class="nes-textarea @if ($errors->has('reason')) is-invalid @endif"></textarea>
                                                <div class="textarea-counter" data-text-max="350"
                                                    data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}">
                                                </div>
                                                @if ($errors->has('reason'))
                                                    <br>
                                                    <div class="alert alert-danger" role="alert">
                                                        @foreach ($errors->get('reason') as $error)
                                                            {{ $error }}
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-lg-12">
                                                <br>
                                                <button type="submit"
                                                    class="nk-btn nk-btn-lg nk-btn-rounded nes-btn is-error btn-block">@lang('messages.save')</button>
                                            </div>
                                        </form>
                                    @endif
                                </div>
                            </div>
                        @endif

                        @if ($viewData['order_header']->status == 'CR' || $viewData['order_header']->status == 'PD')
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5 class="pb-8 text-sm font-extrabold text-gray-900 retro">@lang('messages.detail_send')
                                    </h5>
                                </div>
                                <div class="ibox-content">
                                    <form action="{{ url('/account/sales/update') }}" method="post">

                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" class="form-control" id="id" name="id"
                                            value="{{ $viewData['order_header']->order_identification }}">

                                        @if ($viewData['order_shipping']->certified == 'Yes')
                                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                                <span class="text-sm font-semibold uppercase">@lang('messages.the_traking')</span>
                                                <span class="text-sm font-semibold">
                                                    <input type="text"
                                                        class="nes-input {{ $errors->has('tracking_number') ? 'is-invalid' : '' }}"
                                                        id="tracking_number" required name="tracking_number"
                                                        value="@if (isset($viewData['order_header']->tracking_number)) {{ old('tracking_number', $viewData['order_header']->tracking_number) }}@else{{ old('tracking_number') }} @endif">
                                            </div>
                                            @if ($errors->has('tracking_number'))
                                                <div class="invalid-feedback">
                                                    @foreach ($errors->get('tracking_number') as $error)
                                                        {{ $error }}
                                                    @endforeach
                                                </div>
                                            @endif
                                        @endif
                                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                            <span
                                                class="text-sm font-semibold uppercase">{{ __('Confirmar el pedido enviado') }}</span>
                                            <span class="text-sm font-semibold">
                                                <button type="submit"
                                                    class="nk-btn nes-btn nk-btn-md nk-btn-rounded nk-btn-color-white btn-block"><i
                                                        class="fa fa-truck"></i> @lang('messages.confirm_order_two')
                                                </button>
                                        </div>
                                </div>
                            </div>
                            </form>
                        @elseif($viewData['order_header']->status == 'DD' || $viewData['order_header']->status == 'ST')
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5 class="pb-8 text-xs font-semibold text-gray-900 retro">
                                        {{ __('Detalle del envio') }}
                                    </h5>
                                </div>
                                <div class="ibox-content">
                                    @if ($viewData['order_header']->company_code)
                                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                            <span
                                                class="text-sm font-semibold uppercase">{{ __('Código de compania') }}</span>
                                            <span class="text-sm font-semibold">
                                                {{ $viewData['order_header']->company_code }}
                                        </div>
                                    @endif
                                    @if ($viewData['order_header']->tracking_number)
                                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                            <span class="text-sm font-semibold uppercase">@lang('messages.the_traking')</span>
                                            <span class="text-sm font-bold text-gray-900">
                                                {{ $viewData['order_header']->tracking_number }}
                                        </div>
                                    @endif
                                    <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                        <span class="text-sm font-semibold uppercase">{{ __('Fecha de envío') }}</span>
                                        <span class="text-sm font-semibold">
                                            {{ $viewData['order_header']->sent_on }}
                                    </div>
                                    @if ($viewData['order_header']->status == 'DD')
                                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                            <span
                                                class="text-sm font-semibold uppercase">{{ __('Fecha de entrega') }}</span>
                                            <span class="text-sm font-semibold">
                                                {{ $viewData['order_header']->delivered_on }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endif

                        @if (
                            $viewData['order_header']->status == 'CR' ||
                                $viewData['order_header']->status == 'DD' ||
                                $viewData['order_header']->status == 'RP')

                            <!---->
                            <div class="row">

                                @if ((isset($viewData['order_header']->cancelOrder) ? $viewData['order_header']->cancelOrder->count() : 0) == 0)
                                    @if (in_array($viewData['order_header']->status, ['CR', 'RP']))
                                        @php($dates = date('d-m-Y', strtotime($viewData['order_header']->created_on)))
                                        @php($prueba = date('d-m-Y', strtotime($dates . ' + 7 days')))
                                        @if ($dates < $prueba)
                                        @else
                                            <div class="col-md-12">
                                                <div class="ibox-title">
                                                    <h5 class="text-sm font-extrabold text-gray-900 retro">
                                                        @lang('messages.soli_cancel_order_sale')
                                                    </h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <form
                                                        action="{{ url('/account/sales/view/' . $viewData['order_header']->order_identification) }}"
                                                        method="POST">
                                                        {{ csrf_field() }}
                                                        <h5 class="text-xs font-bold text-gray-800 retro">
                                                            @lang('messages.are_you_sure_sale'):
                                                        </h5>
                                                        <label
                                                            class="text-sm font-semibold uppercase">@lang('messages.the_reason')</label>
                                                        <textarea name="reason" class="nes-textarea" cols="30" rows="3" required></textarea>
                                                        <div class="textarea-counter" data-text-max="350"
                                                            data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}">
                                                        </div>
                                                        <button type="submit"
                                                            class="nk-btn btn-block font-extrabold nk-btn-md nk-btn-rounded nes-btn is-error nk-cancel-btn">@lang('messages.sends')</button>
                                                    </form>
                                                </div>
                                                <div class="nk-gap-3"></div>
                                            </div>
                                        @endif
                                    @endif
                                @endif

                            </div>
                            <!---->

                        @endif


                        @if ($viewData['order_header']->rating)
                            <div class="ibox">
                                @if ($viewData['order_header']->rating->processig > 0)
                                    <div class="ibox-title">
                                        <h5 class="pb-8 text-xs font-semibold text-gray-900 retro">@lang('messages.the_rating')
                                        </h5>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                            <span class="text-sm font-semibold uppercase">{{ __('Rating') }}</span>
                                            <span class="text-sm font-semibold ">
                                                <div id="orderID"><i
                                                        class="{{ $viewData['order_header']->rating->ratingIcon }} text-{{ $viewData['order_header']->rating->ratingType }}"
                                                        data-toggle="popover"
                                                        data-content="{{ $viewData['order_header']->rating->ratingStatus }}"
                                                        data-placement="top" data-trigger="hover"></i>
                                                </div>
                                            </span>
                                        </div>
                                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                            <span
                                                class="text-sm font-semibold uppercase">{{ __('Velocidad de procesamiento') }}</span>
                                            <span class="text-sm font-semibold ">
                                                <div id="orderID">{{ $viewData['order_header']->rating->pro }} </div>
                                            </span>
                                        </div>
                                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                            <span class="text-sm font-semibold uppercase">{{ __('Empaquetado') }}</span>
                                            <span class="text-sm font-semibold ">
                                                <div id="orderID">{{ $viewData['order_header']->rating->pac }} </div>
                                            </span>
                                        </div>
                                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                            <span class="text-sm font-semibold uppercase">{{ __('Descripción') }}</span>
                                            <span class="text-sm font-semibold ">
                                                <div id="orderID">{{ $viewData['order_header']->rating->des }} </div>
                                            </span>
                                        </div>
                                        <div class="pb-8 mt-10 mb-10 border-b">
                                            <label class="text-sm font-semibold uppercase retro"><b>{{ __('Comentario') }}
                                                    :</b></label>
                                            <br>
                                            <span class="text-sm font-semibold text-center">
                                                {{ $viewData['order_header']->rating->description }}
                                            </span>
                                        </div>
                                    </div>
                                @else
                                    <div class="ibox-title">
                                        <h5 class="pb-8 text-xs font-semibold text-gray-900 retro">
                                            {{ __('Valoración') }}
                                        </h5>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                            <span class="text-sm font-semibold uppercase">{{ __('Rating') }}</span>
                                            <span class="text-sm font-semibold ">
                                                <div id="orderID"> {{ __('Sin Calificar') }} </div>
                                            </span>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endif



                        @if (isset($viewData['order_header']->cancelOrder))
                            @if ($viewData['order_header']->cancelOrder->answer == 'Y' || $viewData['order_header']->cancelOrder->answer == 'N')
                                <div class="ibox">
                                    <div class="ibox-title">
                                        <h5 class="pb-8 text-xs font-semibold text-gray-900 retro">
                                            {{ __('Solicitud de Cancelacion') }}
                                        </h5>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                            <span
                                                class="text-sm font-semibold uppercase">{{ __('Solicitado por :') }}</span>
                                            <span class="text-sm font-semibold ">
                                                {{ $viewData['order_header']->cancelOrder->solicited_by == 'B' ? strtoupper($viewData['order_header']->buyer->user_name) : strtoupper($viewData['order_header']->seller->user_name) }}
                                            </span>
                                        </div>
                                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                            <span class="text-sm font-semibold uppercase">{{ __('Razon :') }}</span>
                                            <span class="text-sm font-semibold ">
                                                {{ $viewData['order_header']->cancelOrder->cause }}
                                            </span>
                                        </div>
                                        <div class="pb-8 mt-10 mb-10 border-b">
                                            <label class="text-sm font-semibold uppercase retro"><b>{{ __('Respuesta') }}
                                                    :</b></label>
                                            <br>
                                            <span class="text-sm font-semibold text-center">
                                                {{ $viewData['order_header']->cancelOrder->answer == 'Y' ? 'Si' : 'No' }}
                                                -
                                                {{ $viewData['order_header']->cancelOrder->reason }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endif


                    </div>
                </div>
            </div>
        </div>


        <body class="bg-gray-100">

            <div class="nk-gap-2"></div>
            @if (isset($viewData['order_details']))
                <div class="container">
                    <div class="col-lg-12">
                        <h3 class="nk-decorated-h-3">
                            <div class="section-title2">
                                <h3 class="retro">@lang('messages.detail_order')
                                    </b>
                                </h3>
                            </div>
                        </h3>
                    </div>
                </div>
                @if (count($viewData['order_details']) > 2)
                    <div class="container px-12 py-12 bg-white scroller">
                    @else
                        <div class="container px-12 py-12 bg-white">
                @endif

                @foreach (App\AppOrgCategory::whereIn('id', [1, 2, 3, 4, 172])->get() as $CatItem)
                    @php($contador = 0)

                    @foreach ($viewData['order_details'] as $cartItem)
                        @if ($cartItem->product->catid == $CatItem->id)
                            @php($contador += 1)
                        @endif
                    @endforeach
                    <div class="nk-gap"></div>
                    <h5 class="text-gray-900 text-base font-extrabold mt-15 retro"
                        style="display: {{ $contador > 0 ? 'block' : 'none' }};">
                        <b>{{ __($CatItem->name) }}</b>

                    </h5>
                    <div class="wishlist-table table-responsive">
                        <table style="display: {{ $contador > 0 ? 'table' : 'none' }}; width:100%;">
                            <thead>
                                @if ($CatItem->id == 1)
                                    <tr class="bg-red-700" style="font-size:10px;">
                                        <th class="product-cart-img">
                                            <span class="nobr"><i class="fa fa-image"></i></span>
                                        </th>
                                        <th class="product-name">
                                            <span class="nobr">@lang('messages.products')</span>
                                        </th>
                                        <th class="product-name">
                                            <span class="nobr">@lang('messages.box')</span>
                                        </th>
                                        <th class="product-price">
                                            <span class="nobr">@lang('messages.cover')</span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> Manual </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr">@lang('messages.game_product_inventory')</span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> Extra </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> <i class="fa fa-image"></i> </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> <i class="fa fa-comment"></i>
                                            </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> @lang('messages.cants') </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> @lang('messages.price') </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> Total </span>
                                        </th>

                                    </tr>
                                @elseif($CatItem->id == 2)
                                    <tr class="bg-red-700" style="font-size:10px;">
                                        <th class="product-cart-img">
                                            <span class="nobr"><i class="fa fa-image"></i></span>
                                        </th>
                                        <th class="product-name">
                                            <span class="nobr">@lang('messages.products')</span>
                                        </th>
                                        <th class="product-name">
                                            <span class="nobr">@lang('messages.box')</span>
                                        </th>
                                        <th class="product-price">
                                            <span class="nobr">@lang('messages.inside')</span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> Manual </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr">@lang('messages.console')</span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> Extra </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> Cables </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> <i class="fa fa-image"></i> </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> <i class="fa fa-comment"></i>
                                            </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr">@lang('messages.cants') </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr">@lang('messages.price') </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> Total </span>
                                        </th>
                                        <th class="product-add-to-cart">Accion</th>
                                    </tr>
                                @else
                                    <tr class="bg-red-700" style="font-size:10px;">
                                        <th class="product-cart-img">
                                            <span class="nobr"><i class="fa fa-image"></i></span>
                                        </th>
                                        <th class="product-name">
                                            <span class="nobr">@lang('messages.products')</span>
                                        </th>
                                        <th class="product-name">
                                            <span class="nobr">@lang('messages.box')</span>
                                        </th>
                                        <th class="product-price">
                                            <span class="nobr">@lang('messages.state_inventory')</span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> Extra </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> <i class="fa fa-image"></i> </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> <i class="fa fa-comment"></i>
                                            </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr">@lang('messages.cants') </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr">@lang('messages.price') </span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr"> Total </span>
                                        </th>
                                        <th class="product-add-to-cart">Accion</th>
                                    </tr>
                                @endif
                            </thead>
                            <tbody>
                                @foreach ($viewData['order_details'] as $detail)
                                    @if ($detail->product->catid == $CatItem->id)
                                        @if ($CatItem->id == 1)
                                            <tr>
                                                <td class="product-cart-img">

                                                    @if (count($detail->product->images) > 0)
                                                        <center>
                                                            <a href="/product/{{ $detail->product->id }}"><img
                                                                    width="70px" height="70px"
                                                                    src="{{ url('/images/' . $detail->product->images[0]->image_path) }}"></a>
                                                        </center>
                                                    @else
                                                        <center>
                                                            <a href="/product/{{ $detail->product->id }}"><img
                                                                    width="70px" height="70px"
                                                                    src="{{ url('assets/images/art-not-found.jpg') }}"></a>
                                                        </center>
                                                    @endif
                                                </td>
                                                <td class="text-left">
                                                    <a href="/product/{{ $detail->product->id }}" target="_blank">
                                                        <span class="text-sm retro font-bold color-primary">
                                                            <font color="black"><b>{{ $detail->product->name }}
                                                                    @if ($detail->product->name_en)
                                                                        <br><small>{{ $detail->product->name_en }}</small>
                                                                    @endif
                                                                </b>
                                                            </font>
                                                        </span>
                                                    </a>


                                                    <br><span class="h7 text-secondary">{{ $detail->product->platform }}
                                                        -
                                                        {{ $detail->product->region }}</span>

                                                </td>


                                                <td class="product-name">
                                                    <center>
                                                        <img width="15px" src="/{{ $detail->inventory->box }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>

                                                </td>

                                                <td class="product-stock-status">
                                                    <center>
                                                        <img width="15px" src="/{{ $detail->inventory->cover }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->cover_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>

                                                </td>

                                                <td class="product-stock-status">
                                                    <center>
                                                        <img width="15px" src="/{{ $detail->inventory->manual }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->manual_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>

                                                </td>

                                                <td class="product-price">
                                                    <center>
                                                        <img width="15px" src="/{{ $detail->inventory->game }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>
                                                </td>

                                                <td class="product-price">
                                                    <center>
                                                        <img width="15px" src="/{{ $detail->inventory->extra }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>

                                                </td>
                                                <td class="text-center">

                                                    @if ($detail->inventory->images->first())
                                                        <a href="{{ '/uploads/inventory-images/' . $detail->inventory->images[0]->image_path }}"
                                                            data-fancybox="{{ $detail->product->id }}">
                                                            <img width="50px" height="50px"
                                                                src="{{ count($detail->inventory->images) > 0 ? url('/uploads/inventory-images/' . $detail->inventory->images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                        </a>

                                                        @foreach ($detail->inventory->images as $p)
                                                            @if (!$loop->first)
                                                                <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                    data-fancybox="{{ $detail->product->id }}">
                                                                    <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                        width="0px" height="0px"
                                                                        style="position:absolute;" />
                                                                </a>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <center>
                                                            <font color="black">
                                                                <i class="fa fa-times" data-toggle="popover"
                                                                    data-content="@lang('messages.not_working_profile')"
                                                                    data-placement="top" data-trigger="hover"></i>
                                                            </font>
                                                        </center>
                                                    @endif
                                                </td>
                                                <td class="product-stock-status">
                                                    @if ($detail->inventory->comments == '')
                                                        <center>
                                                            <font color="black">
                                                                <i class="fa fa-times" data-toggle="popover"
                                                                    data-content="@lang('messages.not_working_profile')"
                                                                    data-placement="top" data-trigger="hover"></i>
                                                            </font>
                                                        </center>
                                                    @else
                                                        <center>
                                                            <font color="black">
                                                                <i class="fa fa-comment" data-toggle="popover"
                                                                    data-content="{{ $detail->inventory->comments }}"
                                                                    data-placement="top" data-trigger="hover"></i>
                                                            </font>
                                                        </center>
                                                    @endif
                                                </td>

                                                <div class="retro">
                                                    <td
                                                        class="text-base text-center retro font-extrabold color-primary text-nowrap">
                                                        <font color="black">{{ $detail->quantity }}</font>
                                                    </td>
                                                </div>
                                                <td>
                                                    <font color="green"> <span
                                                            class="text-base text-right retro font-extrabold color-primary text-nowrap"
                                                            data-value="{{ $detail->price }}">
                                                            {{ number_format($detail->price, 2) }}
                                                            € </span> </font>


                                                </td>
                                                <td>
                                                    <font color="green"> <span
                                                            class="text-base text-right retro font-extrabold color-primary text-nowrap"
                                                            data-value="{{ $detail->price }}">
                                                            {{ number_format($detail->total, 2) }}
                                                            € </span> </font>


                                                </td>

                                            </tr>
                                        @elseif($CatItem->id == 2)
                                            <tr>
                                                <td class="product-cart-img">

                                                    @if (count($detail->product->images) > 0)
                                                        <center>
                                                            <a href="/product/{{ $detail->product->id }}"><img
                                                                    width="70px" height="70px"
                                                                    src="{{ url('/images/' . $detail->product->images[0]->image_path) }}"></a>
                                                        </center>
                                                    @else
                                                        <center>
                                                            <a href="/product/{{ $detail->product->id }}"><img
                                                                    width="70px" height="70px"
                                                                    src="{{ url('assets/images/art-not-found.jpg') }}"></a>
                                                        </center>
                                                    @endif
                                                </td>
                                                <td class="text-left">
                                                    <a href="/product/{{ $detail->product->id }}" target="_blank">
                                                        <span class="text-xs retro font-weight-bold color-primary small">
                                                            <font color="black"><b>{{ $detail->product->name }}
                                                                    @if ($detail->product->name_en)
                                                                        <br><small>{{ $detail->product->name_en }}</small>
                                                                    @endif
                                                                </b>
                                                            </font>
                                                        </span>
                                                    </a>


                                                    <br><span class="h7 text-secondary">{{ $detail->product->platform }}
                                                        -
                                                        {{ $detail->product->region }}</span>

                                                </td>


                                                <td class="product-name">
                                                    <center>
                                                        <img width="15px" src="/{{ $detail->inventory->box }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>

                                                </td>

                                                <td class="product-stock-status">
                                                    <center>
                                                        <img width="15px" src="/{{ $detail->inventory->cover }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->cover_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>

                                                </td>

                                                <td class="product-stock-status">
                                                    <center>
                                                        <img width="15px" src="/{{ $detail->inventory->manual }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->manual_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>

                                                </td>

                                                <td class="product-price">
                                                    <center>
                                                        <img width="15px" src="/{{ $detail->inventory->game }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>
                                                </td>

                                                <td class="product-price">
                                                    <center>
                                                        <img width="15px" src="/{{ $detail->inventory->extra }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>

                                                </td>
                                                <td class="product-price">
                                                    <center>
                                                        <img width="15px" src="/{{ $detail->inventory->inside }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->inside_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>

                                                </td>
                                                <td class="text-center">

                                                    @if ($detail->inventory->images->first())
                                                        <a href="{{ '/uploads/inventory-images/' . $detail->inventory->images[0]->image_path }}"
                                                            data-fancybox="{{ $detail->product->id }}">
                                                            <img width="50px" height="50px"
                                                                src="{{ count($detail->inventory->images) > 0 ? url('/uploads/inventory-images/' . $detail->inventory->images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                        </a>

                                                        @foreach ($detail->inventory->images as $p)
                                                            @if (!$loop->first)
                                                                <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                    data-fancybox="{{ $detail->product->id }}">
                                                                    <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                        width="0px" height="0px"
                                                                        style="position:absolute;" />
                                                                </a>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <center>
                                                            <font color="black">
                                                                <i class="fa fa-times" data-toggle="popover"
                                                                    data-content="@lang('messages.not_working_profile')"
                                                                    data-placement="top" data-trigger="hover"></i>
                                                            </font>
                                                        </center>
                                                    @endif
                                                </td>
                                                <td class="product-stock-status">
                                                    @if ($detail->inventory->comments == '')
                                                        <center>
                                                            <font color="black">
                                                                <i class="fa fa-times" data-toggle="popover"
                                                                    data-content="@lang('messages.not_working_profile')"
                                                                    data-placement="top" data-trigger="hover"></i>
                                                            </font>
                                                        </center>
                                                    @else
                                                        <center>
                                                            <font color="black">
                                                                <i class="fa fa-comment" data-toggle="popover"
                                                                    data-content="{{ $detail->inventory->comments }}"
                                                                    data-placement="top" data-trigger="hover"></i>
                                                            </font>
                                                        </center>
                                                    @endif
                                                </td>

                                                <div class="retro">
                                                    <td class="text-center retro h7">
                                                        <font color="black">{{ $detail->quantity }}</font>
                                                    </td>
                                                </div>
                                                <td>
                                                    <font color="green"> <span
                                                            class="text-right retro font-weight-bold color-primary small text-nowrap"
                                                            data-value="{{ $detail->price }}">
                                                            {{ number_format($detail->price, 2) }}
                                                            € </span> </font>


                                                </td>
                                                <td>
                                                    <font color="green"> <span
                                                            class="text-right retro font-weight-bold color-primary small text-nowrap"
                                                            data-value="{{ $detail->price }}">
                                                            {{ number_format($detail->total, 2) }}
                                                            € </span> </font>


                                                </td>

                                            </tr>
                                        @else
                                            <tr>
                                                <td class="product-cart-img">

                                                    @if (count($detail->product->images) > 0)
                                                        <center>
                                                            <a href="/product/{{ $detail->product->id }}"><img
                                                                    width="70px" height="70px"
                                                                    src="{{ url('/images/' . $detail->product->images[0]->image_path) }}"></a>
                                                        </center>
                                                    @else
                                                        <center>
                                                            <a href="/product/{{ $detail->product->id }}"><img
                                                                    width="70px" height="70px"
                                                                    src="{{ url('assets/images/art-not-found.jpg') }}"></a>
                                                        </center>
                                                    @endif
                                                </td>
                                                <td class="text-left">
                                                    <a href="/product/{{ $detail->product->id }}" target="_blank">
                                                        <span class="text-xs retro font-weight-bold color-primary small">
                                                            <font color="black"><b>{{ $detail->product->name }}
                                                                    @if ($detail->product->name_en)
                                                                        <br><small>{{ $detail->product->name_en }}</small>
                                                                    @endif
                                                                </b>
                                                            </font>
                                                        </span>
                                                    </a>


                                                    <br><span class="h7 text-secondary">{{ $detail->product->platform }}
                                                        -
                                                        {{ $detail->product->region }}</span>

                                                </td>


                                                <td class="product-name">
                                                    <center>
                                                        <img width="15px" src="/{{ $detail->inventory->box }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>

                                                </td>


                                                <td class="product-price">
                                                    <center>
                                                        <img width="15px" src="/{{ $detail->inventory->game }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>
                                                </td>

                                                <td class="product-price">
                                                    <center>
                                                        <img width="15px" src="/{{ $detail->inventory->extra }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>

                                                </td>
                                                <td class="text-center">

                                                    @if ($detail->inventory->images->first())
                                                        <a href="{{ '/uploads/inventory-images/' . $detail->inventory->images[0]->image_path }}"
                                                            data-fancybox="{{ $detail->product->id }}">
                                                            <img width="50px" height="50px"
                                                                src="{{ count($detail->inventory->images) > 0 ? url('/uploads/inventory-images/' . $detail->inventory->images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                        </a>

                                                        @foreach ($detail->inventory->images as $p)
                                                            @if (!$loop->first)
                                                                <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                    data-fancybox="{{ $detail->product->id }}">
                                                                    <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                        width="0px" height="0px"
                                                                        style="position:absolute;" />
                                                                </a>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <center>
                                                            <font color="black">
                                                                <i class="fa fa-times" data-toggle="popover"
                                                                    data-content="@lang('messages.not_working_profile')"
                                                                    data-placement="top" data-trigger="hover"></i>
                                                            </font>
                                                        </center>
                                                    @endif
                                                </td>
                                                <td class="product-stock-status">
                                                    @if ($detail->inventory->comments == '')
                                                        <center>
                                                            <font color="black">
                                                                <i class="fa fa-times" data-toggle="popover"
                                                                    data-content="@lang('messages.not_working_profile')"
                                                                    data-placement="top" data-trigger="hover"></i>
                                                            </font>
                                                        </center>
                                                    @else
                                                        <center>
                                                            <font color="black">
                                                                <i class="fa fa-comment" data-toggle="popover"
                                                                    data-content="{{ $detail->inventory->comments }}"
                                                                    data-placement="top" data-trigger="hover"></i>
                                                            </font>
                                                        </center>
                                                    @endif
                                                </td>

                                                <div class="retro">
                                                    <td class="text-center retro h7">
                                                        <font color="black">{{ $detail->quantity }}</font>
                                                    </td>
                                                </div>
                                                <td>
                                                    <font color="green"> <span
                                                            class="text-right retro font-weight-bold color-primary small text-nowrap"
                                                            data-value="{{ $detail->price }}">
                                                            {{ number_format($detail->price, 2) }}
                                                            € </span> </font>


                                                </td>
                                                <td>
                                                    <font color="green"> <span
                                                            class="text-right retro font-weight-bold color-primary small text-nowrap"
                                                            data-value="{{ $detail->price }}">
                                                            {{ number_format($detail->total, 2) }}
                                                            € </span> </font>


                                                </td>

                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endforeach
            @endif
            </div>
            @php($paypal = 0)
            @if (isset($viewData['total_paypal']))
                @php($paypal = $viewData['total_paypal'])
            @endif


            <div class="nk-gap-3"></div>
            </div>
        </body>
    @endif


@endsection
@section('content-script-include')
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
@endsection
@section('content-script')
    <script>
        $('.delete-confirm').submit(function(e) {
            e.preventDefault();
            Swal.fire({
                title: '<span class="text-base retro">Estas seguro de cancelar el pedido?</span>',
                text: "Tendras que esperar que el vendedor confirme la cancelacion",
                icon: 'warning',
                footer: '<a class="retro" href="/site/guia-de-compra" target="_blank">¿Como Comprar En RGM?</a>',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si,Seguro',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    this.submit();
                }
            })
        })
    </script>
    <script>
        $('.access-deny').submit(function(e) {
            e.preventDefault();
            Swal.fire({
                title: '<span class="text-base retro">AVISO</span>',
                text: "Recuerde que al aceptar el pedido se marcara como 'enviado' ",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si,Seguro',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    this.submit();
                }
            })
        })
    </script>
    <script>
        $('.order-deny').submit(function(e) {
            e.preventDefault();
            Swal.fire({
                title: '<span class="text-base retro">AVISO</span>',
                text: "Recuerde que al aceptar el pedido se marcara como 'no recibido' ",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si,Seguro',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    this.submit();
                }
            })
        })
    </script>
    <script>
        $(document).ready(function() {
            $('.comments').popover();
            $('[data-toggle="popover"]').popover();
        });
    </script>
@endsection
