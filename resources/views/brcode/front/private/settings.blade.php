@extends('brcode.front.layout.app')
@section('content-css-include')

@endsection
@section('content')
<!-- Content Header (Page header) -->

<!-- Main content -->
<section  class="content">
<div class="container">
  <div class="row">
    <div class="col-md-offset-2 col-md-10">
      <div class="account-title">{{ __('Configuración de cuenta') }}</div>
    </div>
  </div><!-- /.row -->
  <div class="row">
    <div class="col-md-2 col-sm-2 col-xs-12">
      @php($account_menu = 'settings')
      @include('brcode.front.private.account_menu')
    </div><!-- /.col-@-2 -->
    <div class="col-md-10 col-sm-10 col-xs-12">
      <form name="formRegistration" class="form-horizontal " novalidate ng-submit="ctrl.submitForm(formRegistration)" ng-class="{ 'br-anim-register-form-completed': ctrl.formSubmitCompleted}" ng-init="ctrl.isCompany='{{ $viewData['user_data']->is_company }}'">
        @if($viewData['user_data']->is_company == 'Y')
        <div class="form-group" ng-show="ctrl.isCompany == 'Y'" ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCoName.$dirty) || (formRegistration.brRegisterCoName.$dirty && formRegistration.brRegisterCoName.$invalid) }">
          <label for="br-register-co-first-name" class="col-sm-2 control-label">{{ __('Compañía') }}</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="brRegisterCoName" ng-model="brRegisterCoName" id="br-register-co-name" placeholder="{{ __('Nombre') }}" required >
          </div>
        </div>
        <div class="form-group" ng-show="ctrl.isCompany == 'Y'" ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCoVat.$dirty) || (formRegistration.brRegisterCoVat.$dirty && formRegistration.brRegisterCoVat.$invalid) }">
          <label for="br-register-co-first-name" class="col-sm-2 control-label"></label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="brRegisterCoVat" ng-model="brRegisterCoVat" id="br-register-co-vat" placeholder="{{ __('VAT Reg No.') }}" required>
          </div>
        </div>
        <div class="form-group" ng-show="ctrl.isCompany == 'Y'" ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCoPhone.$dirty) || (formRegistration.brRegisterCoPhone.$dirty && formRegistration.brRegisterCoPhone.$invalid) }">
          <label for="br-register-co-first-name" class="col-sm-2 control-label"></label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="brRegisterCoPhone" ng-model="brRegisterCoPhone" id="br-register-co-phone" placeholder="{{ __('Teléfono') }}" required>
          </div>
        </div>
        @endif
        <div class="form-group" ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCoFirstName.$dirty) || (formRegistration.brRegisterCoFirstName.$dirty && formRegistration.brRegisterCoFirstName.$invalid) }">
          <label for="br-register-co-first-name" class="col-sm-2 control-label">{{ __('Nombre') }}</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="brRegisterCoFirstName" ng-model="brRegisterCoFirstName" id="br-register-co-first-name" placeholder="{{ __('Su nombre') }}" required ng-init="brRegisterCoFirstName='{{ $viewData['user_data']->first_name }}'">
          </div>
        </div>
        <div class="form-group" ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCoLastName.$dirty) || (formRegistration.brRegisterCoLastName.$dirty && formRegistration.brRegisterCoLastName.$invalid) }">
          <label for="br-register-co-last-name" class="col-sm-2 control-label"></label>
          <div class="col-sm-10">
            <input type="text" class="form-control"  name="brRegisterCoLastName" ng-model="brRegisterCoLastName" id="br-register-co-last-name" placeholder="{{ __('Su apellido') }}" required ng-init="brRegisterCoLastName='{{ $viewData['user_data']->last_name }}'">
          </div>
        </div>
        <div class="form-group" ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCoAddress.$dirty) || (formRegistration.brRegisterCoAddress.$dirty && formRegistration.brRegisterCoAddress.$invalid) }">
          <label for="br-register-co-address" class="col-sm-2 control-label">{{ __('Dirección') }}</label>
          <div class="col-sm-10">
            <input type="text" class="form-control"  name="brRegisterCoAddress" ng-model="brRegisterCoAddress" id="br-register-co-address" placeholder="{{ __('Su dirección') }}" required ng-init="brRegisterCoAddress='{{ $viewData['user_data']->address }}'">
          </div>
        </div>
        <div class="form-group" ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCoZipCode.$dirty) || (formRegistration.brRegisterCoZipCode.$dirty && formRegistration.brRegisterCoZipCode.$invalid) }">
          <label for="br-register-co-zipcode" class="col-sm-2 control-label"></label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="brRegisterCoZipCode" ng-model="brRegisterCoZipCode" id="br-register-co-zipcode" placeholder="{{ __('Su código postal') }}" required ng-init="brRegisterCoZipCode='{{ $viewData['user_data']->zipcode }}'">
          </div>
        </div>
        <div class="form-group" ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCoCity.$dirty) || (formRegistration.brRegisterCoCity.$dirty && formRegistration.brRegisterCoCity.$invalid) }">
          <label for="br-register-co-city" class="col-sm-2 control-label"></label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="brRegisterCoCity" ng-model="brRegisterCoCity" id="br-register-co-city" placeholder="{{ __('Su ciudad') }}" required ng-init="brRegisterCoCity='{{ $viewData['user_data']->zipcode }}'">
          </div>
        </div>
        <div class="form-group" ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCoCountry.$dirty) || (formRegistration.brRegisterCoCountry.$dirty && formRegistration.brRegisterCoCountry.$invalid) }">
          <label for="br-register-co-country" class="col-sm-2 control-label"></label>
          <div class="col-sm-10">
            <select id="br-register-co-country" class="form-control br-ajs-select2" name="brRegisterCoCountry" ng-model="brRegisterCoCountry" data-br-placeholder="{{ __('Su país') }}" data-br-resolve="Y" ng-init="brRegisterCoCountry='{{ $viewData['user_data']->country_id }}'">
              <option style="display:none" value="">select a type</option>
              @foreach($viewData['countries'] as $country)
              <option value="{{ $country->id }}" {{ $country->id == $viewData['user_data']->country_id ? ' selected="selected" ' : ''}}>{{ $country->name }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group" ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCoEmail.$dirty) || (formRegistration.brRegisterCoEmail.$dirty && formRegistration.brRegisterCoEmail.$invalid) }">
          <label for="br-register-co-email" class="col-sm-2 control-label">{{ __('Correo') }}</label>
          <div class="col-sm-10">
            <input type="email" class="form-control" name="brRegisterCoEmail" ng-model="brRegisterCoEmail" id="br-register-co-email" placeholder="{{ __('Introduzca su correo') }}" required ng-init="brRegisterCoEmail='{{ $viewData['user_data']->email }}'">
            <span style="color:red" ng-show="(formRegistration.$submitted && ! formRegistration.brRegisterCoEmail.$dirty) || (formRegistration.brRegisterCoEmail.$dirty && formRegistration.brRegisterCoEmail.$invalid)">
              <span ng-show="formRegistration.brRegisterCoEmail.$error.email">{{ __('El correo introducido es inválido') }}.</span>
            </span>
          </div>
        </div>
        <div class="form-group">
          <label for="br-register-co-username" class="col-sm-2 control-label">{{ __('Usuario') }}</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" readonly value="{{ $viewData['user_data']->user_name }}">
          </div>
        </div>
        <div class="form-group" ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCoPassword.$dirty) || (formRegistration.brRegisterCoPassword.$dirty && formRegistration.brRegisterCoPassword.$invalid) }">
          <label for="br-register-co-password" class="col-sm-2 control-label">{{ __('Contraseña') }}</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" name="brRegisterCoPassword" ng-model="brRegisterCoPassword" id="br-register-co-password" placeholder="{{ __('Cree una contraseña') }}" required>
          </div>
        </div>
        <div class="form-group" ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCoPassword2.$dirty) || (formRegistration.brRegisterCoPassword2.$dirty && formRegistration.brRegisterCoPassword2.$invalid) }">
          <div class="col-sm-offset-2 col-sm-10">
            <input type="password" class="form-control" name="brRegisterCoPassword2" ng-model="brRegisterCoPassword2" id="br-register-co-password-confirmation" placeholder="{{ __('Repita la contraseña') }}" required>
            <span style="color:red" ng-show="( formRegistration.brRegisterCoPassword2.$dirty && formRegistration.brRegisterCoPassword.$valid && formRegistration.brRegisterCoPassword.$viewValue != formRegistration.brRegisterCoPassword2.$viewValue)">
              <span >{{ __('Las contraseñas no son iguales') }}.</span>
            </span>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <span style="color:red" ng-show="ctrl.formError">
              <span ng-bind-html="ctrl.formErrorMessage"></span>
            </span>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">

            <button type="submit" class="btn btn-default" id="br-submit-save-co" ><i class="fa fa-floppy-o"></i> {{ __('Guardar') }}</button>
          </div>
        </div>
      </form>
    </div><!-- /.col-@-10 -->
  </div><!-- /.row -->
</div><!-- /.container -->
</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
<!-- The basic File Upload plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
<!-- The File Upload processing plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>

@endsection
@section('content-script')
<script>
$(document).ready(function() {

});
</script>
@endsection
