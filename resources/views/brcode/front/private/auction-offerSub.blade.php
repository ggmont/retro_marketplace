@extends('brcode.front.layout.app')
@section('content-css-include')
    <!-- Dropzone css -->
    <link href="/assets/dropzone-master/dist/min/dropzone.min.css" rel="stylesheet" type="text/css" />
    <style>
        .dropzone .dz-preview .dz-error-message {
            top: 175px !important;
        }
    </style>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div class="nk-main">
            <!-- START: Breadcrumbs -->

            <div class="header-area" id="headerArea">
                <div class="container h-100 d-flex align-items-center justify-content-between">
                    <!-- Back Button-->
                    <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                    <!-- Page Title-->
                    <div class="page-heading">
                        <h6 class="mb-0 font-extrabold">@lang('messages.detail_product')</h6>
                    </div>
                    <!-- Navbar Toggler-->

                    <div class="normal">
                        @if (Auth::user())
                            <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebarPanel">
                                <span></span><span></span><span></span>
                            </div>
                        @else
                            <a href="#" id="showLogin" class="showLogin" class="lni lni-user d-flex flex-wrap"
                                data-nav-toggle="#nk-nav-mobile">
                                <span class="lni lni-user">
                                    <span></span><span></span><span></span>
                                </span>
                            </a>
                        @endif
                    </div>
                </div>
            </div>

            @include('partials.flash')
            <!-- END: Breadcrumbs -->
            <div class="product-slides owl-carousel">

                @if (count($viewData['product_images']) > 0)
                    <img class="single-product-slide" loading="lazy"
                        src="/images/{{ $viewData['product_images'][0]->image_path }}" alt="">
                    @if (count($viewData['product_images']) > 1)
                        @foreach ($viewData['product_images'] as $key)
                            @if ($viewData['product_images'][0]->id != $key->id)
                                <img class="single-product-slide" loading="lazy" src="/images/{{ $key->image_path }}"
                                    alt="">
                            @endif
                        @endforeach
                    @endif
                @else
                    <img class="single-product-slide" loading="lazy" src="{{ asset('assets/images/art-not-found.jpg') }}"
                        alt="">
                @endif




            </div>



            <div class="product-description pb-3">

                <!-- Product Specification-->
                <div class="p-specification bg-white mb-3 py-3">
                    <div class="container">
                        <div class="p-title-price text-center">
                            <h5 class="mb-1 font-extrabold">{{ $producto->name }}</h5>
                            <br>
                            <h5 class="mb-1 font-extrabold">- Precios Generales -</h5>
                            <br>
                            <?php
                            $duration = [];
                            $duration_not_press = [];
                            $duration_used = [];
                            $duration_new = [];
                            $duration_general = [];
                            $t = '';
                            $total_numeros = '';
                            $total_numeros_not_press = '';
                            $total_numeros_used = '';
                            $total_numeros_new = '';
                            $total_numeros_general = '';
                            $suma = '';
                            $suma_not_press = '';
                            $suma_used = '';
                            $suma_new = '';
                            $suma_general = '';
                            $m = '';
                            $promedio_not_press = '';
                            $promedio_used = '';
                            $promedio_new = '';
                            $promedio_general = '';
                            $suma_total = '';
                            $fecha = '';
                            $precio = '';
                            
                            foreach ($producto->inventory as $item) {
                                $cantidad = $item->price;
                                $duration[] = $cantidad;
                                $total_numeros = count($duration);
                                $total = max($duration);
                                $t = min($duration);
                                $suma = array_sum($duration);
                                $m = $suma / $total_numeros;
                                $suma_total = $item->created_at;
                            }
                            
                            //PROMEDIO GENERAL
                            foreach ($seller_sold_general as $p) {
                                $cantidad_general = $p->price;
                                $duration_general[] = $cantidad_general;
                                $total_numeros_general = count($duration_general);
                                $suma_general = array_sum($duration_general);
                                $promedio_general = $suma_general / $total_numeros_general;
                            }
                            
                            //PROMEDIO PARA LAS CONDICIONES TIPO "NOT PRESS - NO TIENE" POKEMON GOLD ES EL QUE NO TIENE ASTERIX Y OBELIS TAMPOCO TIENEN
                            foreach ($seller_sold_not_pres as $p) {
                                $cantidad_press = $p->price;
                                $duration_not_press[] = $cantidad_press;
                                $total_numeros_not_press = count($duration_not_press);
                                $suma_not_press = array_sum($duration_not_press);
                                $promedio_not_press = $suma_not_press / $total_numeros_not_press;
                            }
                            
                            //PROMEDIO PARA LAS CONDICIONES TIPO "USED" WOODY ES EL QUE ESTA USADO
                            foreach ($seller_sold_used as $p_used) {
                                $cantidad_used = $p_used->price;
                                $duration_used[] = $cantidad_used;
                                $total_numeros_used = count($duration_used);
                                $suma_used = array_sum($duration_used);
                                $promedio_used = $suma_used / $total_numeros_used;
                            }
                            
                            //PROMEDIO PARA LAS CONDICIONES TIPO "NEW" XG2 ES EL QUE TIENE EL ESTADO NUEVO
                            foreach ($seller_sold_new as $p_new) {
                                $cantidad_new = $p_new->price;
                                $duration_new[] = $cantidad_new;
                                $total_numeros_new = count($duration_new);
                                $suma_new = array_sum($duration_new);
                                $promedio_new = $suma_new / $total_numeros_new;
                            }
                            
                            ?>

                            <?php
                            $l = $producto->price_solo + $producto->price_used + $producto->price_new / 3;
                            ?>


                            @if ($promedio_general > 0)
                                <div class="row">
                                    <div class="col">
                                        <div class="total-result-of-ratings">
                                            <span>

                                                @if ($promedio_new > 0)
                                                    {{ number_format($promedio_new, 2) }} €
                                                @elseif($producto->price_new)
                                                    {{ number_format($producto->price_new, 2) }} €
                                                @else
                                                    -
                                                @endif

                                            </span>
                                            <span>Nuevo</span>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="total-result-of-ratings-used">
                                            <span>

                                                @if ($promedio_used > 0)
                                                    {{ number_format($promedio_used, 2) }} €
                                                @elseif($producto->price_used)
                                                    {{ number_format($producto->price_used, 2) }} €
                                                @else
                                                    -
                                                @endif

                                            </span><span>Usado</span>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="total-result-of-ratings-solo"><span>

                                                @if ($promedio_not_press > 0)
                                                    {{ number_format($promedio_not_press, 2) }} €
                                                @elseif($producto->price_solo)
                                                    {{ number_format($producto->price_solo, 2) }} €
                                                @else
                                                    -
                                                @endif

                                            </span><span>Solo</span>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="row">
                                    <div class="col">
                                        <div class="total-result-of-ratings">
                                            <span>


                                                @if ($producto->price_new > 0)
                                                    {{ number_format($producto->price_new, 2) }} €
                                                @else
                                                    -
                                                @endif

                                            </span>
                                            <span>Nuevo</span>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="total-result-of-ratings-used"><span>
                                                @if ($producto->price_used > 0)
                                                    {{ number_format($producto->price_used, 2) }} €
                                                @else
                                                    -
                                                @endif
                                            </span><span>Usado</span>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="total-result-of-ratings-solo"><span>

                                                @if ($producto->price_solo > 0)
                                                    {{ number_format($producto->price_solo, 2) }} €
                                                @else
                                                    -
                                                @endif

                                            </span><span>Solo</span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <br>
                        </div>
                        <h6>Detalles</h6>
                        <div class="nk-product-meta">
                            <div class="lists">
                                <ul class="nes-list is-disc">
                                    @if ($producto->comments == '')
                                    @else
                                        <li>

                                            {{ $producto->comments }}
                                        </li>
                                    @endif
                                    <li>
                                        {{ __('Plataforma') }}:
                                        {{ $producto->platform }}
                                    </li>
                                    <li>
                                        {{ __('Región') }}:
                                        {{ $producto->region }}
                                    </li>
                                    @if ($producto->media)
                                        <li>
                                            {{ __('Media') }}:
                                            {{ $producto->media }}
                                        </li>
                                    @endif
                                    @if ($producto->language == '')
                                    @else
                                        <li>
                                            {{ __('Idioma') }}:
                                            {{ $producto->language }}
                                        </li>
                                    @endif
                                    @if ($producto->box_language == '')
                                    @else
                                        <li>
                                            {{ __('Idioma de Caja') }}:
                                            {{ $producto->box_language }}
                                        </li>
                                    @endif
                                    @if ($producto->ean_upc == '')
                                    @else
                                        <li>
                                            {{ __('Código de Barras') }}:
                                            {{ $producto->ean_upc }}
                                        </li>
                                    @endif
                                </ul>
                            </div>


                            <div class="nk-gap"></div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Ratings Submit Form-->
            <!-- Rating & Review Wrapper-->
            <div class="container">
                <div class="section-heading mt-3">
                    <center>
                        <h4 class="mb-1 font-bold">Detalles del inventario</h4>
                    </center>
                </div>
                <!-- Contact Form-->
                <div class="section mt-2 mb-5">
                    <form class="w-full" method="POST" action="{{ url('/account/inventory/add-auction/') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group boxed">

                            <div class="input-wrapper">
                                <label class="form-label">Cantidad</label>
                                <input type="number" placeholder="Cantidad de articulos" id="name_field"
                                    name="{{ $viewData['col_prefix'] }}quantity"
                                    value="{{ old($viewData['col_prefix'] . 'quantity') !== null ? old($viewData['col_prefix'] . 'quantity') : (isset($viewData['model_data']['quantity']) ? $viewData['model_data']['quantity'] : '1') }}"
                                    min="1" class="form-control">
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <input type="hidden" name="{{ $viewData['col_prefix'] }}max_bid" value="1">

                        <div class="section-heading mt-3">
                            <h4 class="mb-1 font-bold">Condiciones</h4>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5">Caja</label>
                                <select name="{{ $viewData['col_prefix'] }}box_condition" required
                                    class="form-control form-select" id="default_select">
                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                    </option>
                                    @isset($viewData['game_states'])
                                        @foreach ($viewData['game_states'] as $k => $r)
                                            <option data-html="{{ __($r['value']) }}"
                                                {{ (old($viewData['col_prefix'] . 'box_condition') !== null ? old($viewData['col_prefix'] . 'box_condition') : (isset($viewData['model_data']['box_condition']) ? $viewData['model_data']['box_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                value="{{ $r['id'] }}">{{ __($r['value']) }}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5">@lang('messages.console')</label>
                                <select name="{{ $viewData['col_prefix'] }}game_condition" required
                                    class="form-control form-select" id="default_select">
                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                    </option>
                                    @isset($viewData['game_states'])
                                        @foreach ($viewData['game_states'] as $k => $r)
                                            <option data-html="{{ __($r['value']) }}"
                                                {{ (old($viewData['col_prefix'] . 'game_condition') !== null ? old($viewData['col_prefix'] . 'game_condition') : (isset($viewData['model_data']['game_condition']) ? $viewData['model_data']['game_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                value="{{ $r['id'] }}">{{ __($r['value']) }}
                                            </option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>


                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5"> @lang('messages.inside')</label>
                                <select name="{{ $viewData['col_prefix'] }}cover_condition" required
                                    class="form-control form-select" id="default_select">
                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                    </option>
                                    @isset($viewData['game_states'])
                                        @foreach ($viewData['game_states'] as $k => $r)
                                            <option data-html="{{ __($r['value']) }}"
                                                {{ (old($viewData['col_prefix'] . 'cover_condition') !== null ? old($viewData['col_prefix'] . 'cover_condition') : (isset($viewData['model_data']['cover_condition']) ? $viewData['model_data']['cover_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                value="{{ $r['id'] }}">{{ __($r['value']) }}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5">Manual</label>
                                <select name="{{ $viewData['col_prefix'] }}manual_condition" required
                                    class="form-control form-select" id="default_select">
                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                    </option>
                                    @isset($viewData['game_states'])
                                        @foreach ($viewData['game_states'] as $k => $r)
                                            <option data-html="{{ __($r['value']) }}"
                                                {{ (old($viewData['col_prefix'] . 'manual_condition') !== null ? old($viewData['col_prefix'] . 'manual_condition') : (isset($viewData['model_data']['manual_condition']) ? $viewData['model_data']['manual_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                value="{{ $r['id'] }}">{{ __($r['value']) }}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>


                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5">Cables</label>
                                <select name="{{ $viewData['col_prefix'] }}inside_condition" required
                                    class="form-control form-select" id="default_select">
                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                    </option>
                                    @isset($viewData['game_states'])
                                        @foreach ($viewData['game_states'] as $k => $r)
                                            <option data-html="{{ __($r['value']) }}"
                                                {{ (old($viewData['col_prefix'] . 'inside_condition') !== null ? old($viewData['col_prefix'] . 'inside_condition') : (isset($viewData['model_data']['inside_condition']) ? $viewData['model_data']['inside_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                value="{{ $r['id'] }}">{{ __($r['value']) }}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>



                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5"> {{ __('Extras') }}</label>
                                <select name="{{ $viewData['col_prefix'] }}extra_condition" required
                                    class="form-control form-select" id="default_select">
                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                    </option>
                                    @isset($viewData['game_states'])
                                        @foreach ($viewData['game_states'] as $k => $r)
                                            <option data-html="{{ __($r['value']) }}"
                                                {{ (old($viewData['col_prefix'] . 'extra_condition') !== null ? old($viewData['col_prefix'] . 'extra_condition') : (isset($viewData['model_data']['extra_condition']) ? $viewData['model_data']['extra_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                value="{{ $r['id'] }}">{{ __($r['value']) }}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>


                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5"> {{ __('Comentarios') }}</label>
                                <textarea id="comments" name="{{ $viewData['col_prefix'] }}comments" rows="4" class="form-control"
                                    placeholder="Añada un comentario con respecto al producto">{{ old($viewData['col_prefix'] . 'comments') !== null ? old($viewData['col_prefix'] . 'comments') : (isset($viewData['model_data']['comments']) ? $viewData['model_data']['comments'] : '') }}</textarea>
                                <div class="textarea-counter" data-text-max="300"
                                    data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></div>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <br>
                        <div style="display:none" id="fileupload-files"
                            data-name="sub0_{{ $viewData['col_prefix'] }}image_path[]">
                            @if (isset($viewData['model_data']['images']))
                                @php($i = 0)
                                @foreach ($viewData['model_data']['images'] as $imagesnew)
                                    <input type="hidden" id="file_{{ $i }}"
                                        name="sub0_{{ $viewData['col_prefix'] }}image_path[]"
                                        value="{{ $imagesnew['image_path'] }}">

                                    @php($i++)
                                @endforeach
                            @endif
                        </div>
                        <div id="dropzone">
                            <div class="dz-message text-dark" data-dz-message>
                                <span class="retro">@lang('messages.image_sell_mobile')</span>
                            </div>
                        </div>
                        <br>
                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label"> {{ __('Codigo de Barras') }} (Opcional)</label>
                            </div>
                        </div>

                        <div style="display:none" id="fileupload-files-ean"
                            data-name="sub1_{{ $viewData['col_prefix'] }}image_path[]">
                            @if (isset($viewData['inventory_images_ean']))
                                @php($i = 0)
                                @foreach ($viewData['inventory_images_ean'] as $imagesnewean)
                                    <input type="hidden" id="file_{{ $i }}"
                                        name="sub1_{{ $viewData['col_prefix'] }}image_path[]"
                                        value="{{ $imagesnewean['image_path'] }}">
                                    @php($i++)
                                @endforeach
                            @endif
                        </div>
                        <div id="dropzone-ean">
                            <div class="dz-message text-dark" data-dz-message>
                                <span class="retro">Toca para subir los codigos de barras de tu producto</span>
                            </div>
                        </div>
                        <br>
                        @if (isset($viewData['model_data']['images']))
                            <div id="br-inventory-options">
                                <button id="br-publish-item" type="submit"
                                    class="btn text-white btn-warning btn-lg w-100"
                                    type="submit">@lang('messages.updates')</button>
                            </div>
                        @else
                            <div id="br-inventory-options">
                                <button id="br-publish-item" type="submit"
                                    class="btn text-white pt-2 btn-danger btn-lg w-100"
                                    type="submit">{{ __('Subir') }}</button>
                            </div>
                        @endif

                        <br><br><br><br>

                        <div style="display: none;">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="model_id" value="{{ $viewData['model_id'] }}">
                            <input type="hidden" name="type_dv" value="{{ isset($viewData['type_dv']) ? 1 : 0 }}">
                            <input type="hidden" name="qty_pro" value="{{ $viewData['qty_pro'] }}">
                            <input type="hidden" name="{{ $viewData['col_prefix'] }}product_id"
                                value="{{ $viewData['product_id'] }}">
                        </div>

                    </form>
                </div>
            </div>
        </div>

        </div>
    @else
        <div class="container">
            <div class="nk-gap-2"></div>
            <form id="br-inventory-add" class="nk-form nk-form-style" action="{{ url('/account/inventory/add/') }}"
                method="post">

                <div class="nk-main">
                    <!-- START: Breadcrumbs -->
                    <div class="nk-gap-1"></div>
                    <div class="container">
                        <ul class="nk-breadcrumbs">
                            <li>

                                <div class="section-title1">
                                    <h3 class="retro">@lang('messages.information_product')</h3>
                                </div>

                            </li>
                        </ul>
                    </div>
                    <div class="nk-gap-3"></div>

                    <!-- END: Breadcrumbs -->
                    <div class="container bg-white">

                        <div class="row vertical-gap">
                            <div class="col-lg-7">
                                <div class="nk-store-product">
                                    <div class="row vertical-gap">
                                        <div class="col-md-4 item">
                                            @if (count($viewData['product_images']) > 0)
                                                <a href="/images/{{ $viewData['product_images'][0]->image_path }}"
                                                    class="fancybox" data-fancybox="RGM1">
                                                    <img src="/images/{{ $viewData['product_images'][0]->image_path }}"
                                                        width="100%" height="100%" />
                                                </a>
                                                @if (count($viewData['product_images']) > 1)
                                                    <div class="nk-gap-1"></div>
                                                    <div class="row vertical-gap sm-gap">
                                                        @foreach ($viewData['product_images'] as $key)
                                                            @if ($viewData['product_images'][0]->id != $key->id)
                                                                <div class="col-6 col-md-4">
                                                                    <a href="/images/{{ $key->image_path }}"
                                                                        class="fancybox" data-fancybox="RGM1">
                                                                        <img src="/images/{{ $key->image_path }}"
                                                                            width="100%" height="100%" />
                                                                    </a>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                @endif
                                            @else
                                                <a href="{{ url('assets/images/art-not-found.jpg') }}" class="fancybox"
                                                    data-fancybox="RGM1">
                                                    <img src="{{ url('assets/images/art-not-found.jpg') }}"
                                                        width="100%" height="100%" />
                                                </a>
                                            @endif
                                        </div>
                                        <div class="col-md-6">

                                            <h2 class="nk-product-title h3 retro">
                                                <font color="black">{{ $producto->name_en }}</font>
                                            </h2>
                                            <div class="nk-product-meta">
                                                <div class="lists">
                                                    <ul class="nes-list is-disc">
                                                        <li><b>
                                                                <font color="black">{{ __('Fecha de lanzamiento') }}:
                                                                    {{ $producto->release_date }}</font>
                                                            </b></li>
                                                        <li><b>
                                                                <font color="black">{{ __('Plataforma') }}:
                                                                    {{ $producto->platform }}</font>
                                                            </b></li>
                                                        <li><b>
                                                                <font color="black">{{ __('Región') }}:
                                                                    {{ $producto->region }}</font>
                                                            </b></li>
                                                        @if ($producto->volume)
                                                            <li><b>
                                                                    <font color="black">{{ __('Volumen') }}:
                                                                        {{ $producto->volume }}</font>
                                                                </b></li>
                                                        @endif
                                                        @if ($producto->weight)
                                                            <li><b>
                                                                    <font color="black">{{ __('Peso') }}:
                                                                        {{ $producto->weight }}</font>
                                                                </b></li>
                                                        @endif
                                                        @if ($producto->media)
                                                            <li><b>
                                                                    <font color="black">{{ __('Media') }}:
                                                                        {{ $producto->media }}</font>
                                                                </b></li>
                                                        @endif
                                                        @if ($producto->language == '')
                                                        @else
                                                            <li><b>
                                                                    <font color="black">{{ __('Idioma') }}:
                                                                        {{ $producto->language }}</font>
                                                                </b></li>
                                                        @endif
                                                        @if ($producto->box_language == '')
                                                        @else
                                                            <li><b>
                                                                    <font color="black">{{ __('Idioma de Caja') }}:
                                                                        {{ $producto->box_language }}</font>
                                                                </b></li>
                                                        @endif
                                                        @if ($producto->ean_upc == '')
                                                        @else
                                                            <li><b>
                                                                    <font color="black">{{ __('Codigo de Barras') }}:
                                                                        {{ $producto->ean_upc }}</font>
                                                                </b></li>
                                                        @endif
                                                    </ul>
                                                </div>


                                                <div class="nk-gap"></div>
                                                <a class="nes-btn is-primary" href="{{ url()->previous() }}"><span
                                                        class="retro">@lang('messages.go_back')</span></a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="nk-gap-3"></div>

                <div class="container">
                    <section class="content">
                        <div class="row">
                            <div class="col-md-12" style="background-color: #ffffff; border: solid 1px #db2e2e">
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro">
                                    <center>@lang('messages.detail_inventory')</center>
                                </h5>
                                <br>
                                <form id="br-inventory-add" class="w-full"
                                    action="{{ url('/account/inventory/add/') }}" method="post"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="flex flex-wrap mb-2 -mx-3">

                                        <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                                            <label
                                                class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                                {{ __('Cantidad') }}
                                            </label>
                                            <div class="nes-field">

                                                <input type="number"
                                                    placeholder="Ingresa la cantidad de articulos que quieres vender Min-1"
                                                    id="name_field" name="{{ $viewData['col_prefix'] }}quantity"
                                                    value="{{ old($viewData['col_prefix'] . 'quantity') !== null ? old($viewData['col_prefix'] . 'quantity') : (isset($viewData['model_data']['quantity']) ? $viewData['model_data']['quantity'] : '1') }}"
                                                    min="1" class="nes-input">
                                            </div>
                                        </div>
                                        <div class="w-full px-3 md:w-1/2">
                                            <label
                                                class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                                {{ __('Precio') }}
                                            </label>
                                            <div class="nes-field">
                                                <input type="text" autocomplete="off"
                                                    name="{{ $viewData['col_prefix'] }}price"
                                                    class="nes-input br-input-number required  {{ $errors->has($viewData['col_prefix'] . 'price') ? 'is-invalid' : '' }} text-right price-check"
                                                    step="0.01" placeholder="0.00"
                                                    value="{{ old($viewData['col_prefix'] . 'price') !== null ? old($viewData['col_prefix'] . 'price') : (isset($viewData['model_data']['price']) ? $viewData['model_data']['price'] : '') }}" />


                                            </div>
                                        </div>



                                    </div>
                                    <br>
                                    <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro">
                                        @lang('messages.conditions_product')
                                    </h5>
                                    <br>

                                    <div class="flex flex-wrap mb-2 -mx-3">
                                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label
                                                class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                                @lang('messages.box')
                                            </label>
                                            <div class="relative">
                                                <div class="nes-select">

                                                    <select name="{{ $viewData['col_prefix'] }}box_condition" required
                                                        id="default_select" class="retro">
                                                        <option value="" disabled selected hidden>@lang('messages.selects')
                                                        </option>
                                                        @isset($viewData['game_states'])
                                                            @foreach ($viewData['game_states'] as $k => $r)
                                                                <option data-html="{{ __($r['value']) }}"
                                                                    {{ (old($viewData['col_prefix'] . 'box_condition') !== null ? old($viewData['col_prefix'] . 'box_condition') : (isset($viewData['model_data']['box_condition']) ? $viewData['model_data']['box_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                                    value="{{ $r['id'] }}">{{ __($r['value']) }}
                                                                </option>
                                                            @endforeach
                                                        @endisset
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label
                                                class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                                @lang('messages.console')
                                            </label>
                                            <div class="relative">
                                                <div class="nes-select">

                                                    <select name="{{ $viewData['col_prefix'] }}game_condition" required
                                                        id="default_select" class="retro">
                                                        <option value="" disabled selected hidden>@lang('messages.selects')
                                                        </option>
                                                        @isset($viewData['game_states'])
                                                            @foreach ($viewData['game_states'] as $k => $r)
                                                                <option data-html="{{ __($r['value']) }}"
                                                                    {{ (old($viewData['col_prefix'] . 'game_condition') !== null ? old($viewData['col_prefix'] . 'game_condition') : (isset($viewData['model_data']['game_condition']) ? $viewData['model_data']['game_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                                    value="{{ $r['id'] }}">{{ __($r['value']) }}
                                                                </option>
                                                            @endforeach
                                                        @endisset
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label
                                                class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                                @lang('messages.inside')
                                            </label>
                                            <div class="relative">
                                                <div class="nes-select">

                                                    <select name="{{ $viewData['col_prefix'] }}cover_condition" required
                                                        id="default_select" class="retro">
                                                        <option value="" disabled selected hidden>@lang('messages.selects')
                                                        </option>
                                                        @isset($viewData['game_states'])
                                                            @foreach ($viewData['game_states'] as $k => $r)
                                                                <option data-html="{{ __($r['value']) }}"
                                                                    {{ (old($viewData['col_prefix'] . 'cover_condition') !== null ? old($viewData['col_prefix'] . 'cover_condition') : (isset($viewData['model_data']['cover_condition']) ? $viewData['model_data']['cover_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                                    value="{{ $r['id'] }}">{{ __($r['value']) }}
                                                                </option>
                                                            @endforeach
                                                        @endisset
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="flex flex-wrap mb-2 -mx-3">
                                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label
                                                class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                                Manual
                                            </label>
                                            <div class="relative">
                                                <div class="nes-select">

                                                    <select name="{{ $viewData['col_prefix'] }}manual_condition" required
                                                        id="default_select" class="retro">
                                                        <option value="" disabled selected hidden>@lang('messages.selects')
                                                        </option>
                                                        @isset($viewData['game_states'])
                                                            @foreach ($viewData['game_states'] as $k => $r)
                                                                <option data-html="{{ __($r['value']) }}"
                                                                    {{ (old($viewData['col_prefix'] . 'manual_condition') !== null ? old($viewData['col_prefix'] . 'manual_condition') : (isset($viewData['model_data']['manual_condition']) ? $viewData['model_data']['manual_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                                    value="{{ $r['id'] }}">{{ __($r['value']) }}
                                                                </option>
                                                            @endforeach
                                                        @endisset
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label
                                                class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                                Cables
                                            </label>
                                            <div class="relative">
                                                <div class="nes-select">

                                                    <select name="{{ $viewData['col_prefix'] }}inside_condition" required
                                                        id="default_select" class="retro">
                                                        <option value="" disabled selected hidden>@lang('messages.selects')
                                                        </option>
                                                        @isset($viewData['game_states'])
                                                            @foreach ($viewData['game_states'] as $k => $r)
                                                                <option data-html="{{ __($r['value']) }}"
                                                                    {{ (old($viewData['col_prefix'] . 'inside_condition') !== null ? old($viewData['col_prefix'] . 'inside_condition') : (isset($viewData['model_data']['inside_condition']) ? $viewData['model_data']['inside_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                                    value="{{ $r['id'] }}">{{ __($r['value']) }}
                                                                </option>
                                                            @endforeach
                                                        @endisset
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label
                                                class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                                Extras
                                            </label>
                                            <div class="relative">
                                                <div class="nes-select">

                                                    <select name="{{ $viewData['col_prefix'] }}extra_condition" required
                                                        id="default_select" class="retro">
                                                        <option value="" disabled selected hidden>@lang('messages.selects')
                                                        </option>
                                                        @isset($viewData['game_states'])
                                                            @foreach ($viewData['game_states'] as $k => $r)
                                                                <option data-html="{{ __($r['value']) }}"
                                                                    {{ (old($viewData['col_prefix'] . 'extra_condition') !== null ? old($viewData['col_prefix'] . 'extra_condition') : (isset($viewData['model_data']['extra_condition']) ? $viewData['model_data']['extra_condition'] : '')) == $r['id'] ? 'selected="selected"' : '' }}
                                                                    value="{{ $r['id'] }}">{{ __($r['value']) }}
                                                                </option>
                                                            @endforeach
                                                        @endisset
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <br>

                                    <div class="form-group col-sm-12">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            {{ __('Comentarios') }}
                                        </label>
                                        <textarea id="comments" name="{{ $viewData['col_prefix'] }}comments" cols="10" rows="10"
                                            class="nes-textarea" placeholder="Comentario">{{ old($viewData['col_prefix'] . 'comments') !== null ? old($viewData['col_prefix'] . 'comments') : (isset($viewData['model_data']['comments']) ? $viewData['model_data']['comments'] : '') }}</textarea>
                                        <div class="textarea-counter" data-text-max="350"
                                            data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></div>
                                    </div>

                                    <br>
                                    <div style="display:none" id="fileupload-files"
                                        data-name="sub0_{{ $viewData['col_prefix'] }}image_path[]">
                                        @if (isset($viewData['model_data']['images']))
                                            @php($i = 0)
                                            @foreach ($viewData['model_data']['images'] as $imagesnew)
                                                <input type="hidden" id="file_{{ $i }}"
                                                    name="sub0_{{ $viewData['col_prefix'] }}image_path[]"
                                                    value="{{ $imagesnew['image_path'] }}">

                                                @php($i++)
                                            @endforeach
                                        @endif
                                    </div>
                                    <div id="dropzone">
                                        <div class="dz-message text-dark" data-dz-message>
                                            <span class="retro">@lang('messages.image_sell')</span>
                                        </div>
                                    </div>
                                    <div class="nk-gap-2"></div>

                                    <div class="row">
                                        <center>
                                            <div class="col-md-12">
                                                <div id="br-inventory-options">
                                                    <button id="br-publish-item" type="submit"
                                                        class="nes-btn is-error"><span
                                                            class="retro">{{ __('Subir') }}</span></button>
                                                </div>
                                            </div>
                                        </center>
                                    </div>
                                    <div style="display: none;">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="model_id" value="{{ $viewData['model_id'] }}">
                                        <input type="hidden" name="type_dv"
                                            value="{{ isset($viewData['type_dv']) ? 1 : 0 }}">
                                        <input type="hidden" name="qty_pro" value="{{ $viewData['qty_pro'] }}">
                                        <input type="hidden" name="{{ $viewData['col_prefix'] }}product_id"
                                            value="{{ $viewData['product_id'] }}">
                                    </div>
                                    <!----->

                                </form>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="nk-gap-2"></div>
        </div>
    @endif
@endsection

@section('content-script-include')
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>

    <script src="{{ asset('assets/dropzone-master/dist/min/dropzone.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/quagga/0.12.1/quagga.js"></script>
@endsection

@section('content-script')
    <script>
        var global_lang = [];
        global_lang['app.uploader_file_size_bigger'] = 'file bigger';
        global_lang['app.uploader_file_ext_not_supported'] = 'ext not supported';
    </script>
    <script>
        var acceptedFileTypes = "image/*"; // dropzone requires this param be a comma separated list

        var fileList = new Array;
        var i = 0;
        var myDropzone = $("#dropzone").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('/account/inventory/upload_file') }}",
            addRemoveLinks: true,
            @if (isset($viewData['model_data']['images']))
                maxFiles: 8 - {{ $prueba }},
            @else
                maxFiles: 8, //change limit as per your requirements
            @endif
            maxfilesexceeded: function(file) {
                Swal.fire({
                    title: '<span class="retro">Error</span>',
                    text: 'Haz Alcanzado el maximo de Imagenes (Max 8)',
                    footer: '<a class="retro" href="/site/guia-de-venta" target="_blank">¿Como Vender En RGM?</a>',
                    imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/a6cff967-2930-4785-a8b8-48a6e39101b8/dbrz0sh-36c848a3-b37d-4fd1-8d7e-90a27dcb8130.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2E2Y2ZmOTY3LTI5MzAtNDc4NS1hOGI4LTQ4YTZlMzkxMDFiOFwvZGJyejBzaC0zNmM4NDhhMy1iMzdkLTRmZDEtOGQ3ZS05MGEyN2RjYjgxMzAuZ2lmIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.QwzwRFsxRbUrhacQuJV4fSAij3GxImG_4dkxWDTgkMc',
                    imageWidth: 400,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                })
                this.removeFile(file);
            },
            acceptedFiles: ".jpg,.jpeg,.png",
            dictInvalidFileType: "No puedes subir archivos de este tipo , solo Imagenes tipo PNG O JPG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Arrastra archivos aquí o haz clic para subir.", // Cambiar el mensaje predeterminado
            dictRemoveFile: "Eliminar archivo", // Cambiar el texto para eliminar archivo
            dictCancelUpload: "Cancelar subida", // Cambiar el texto para cancelar subida
            dictMaxFilesExceeded: "Ha alcanzado el límite de archivos permitidos.", // Cambiar el texto para archivos excedidos
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileList[i] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": i
                    };
                    input = $('#fileupload-files').data('name');
                    $('#fileupload-files').append(
                        `<input type="hidden" id="file_${ i }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    i += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileList.length; f++) {
                        if (fileList[f].fileName == file.name) {
                            rmvFile = fileList[f].serverFileName;
                            $("#file_" + fileList[f].fileId).remove();
                        }
                    }
                });
                @if (isset($viewData['model_data']['images']))
                    @foreach ($viewData['model_data']['images'] as $imagesnew)
                        var mockFile = {
                            name: "{{ $viewData['product']->name }}_" + i,
                            size: 12345
                        };

                        myDropzone.emit("addedfile", mockFile);

                        myDropzone.emit("thumbnail", mockFile,
                            "/uploads/inventory-images/{{ $imagesnew['image_path'] }}");
                        myDropzone.emit("complete", mockFile);
                        fileList[i] = {
                            "serverFileName": "{{ $imagesnew['image_path'] }}",
                            "fileName": "{{ $viewData['product']->name }}_" + i,
                            "fileId": i
                        };
                        i += 1;
                    @endforeach
                @endif

            }
        });

        j = 0;
        var fileListEan = new Array;
        var myDropzone = $("#dropzone-ean").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('/account/inventory/upload_file_ean') }}",
            addRemoveLinks: true,
            maxFiles: 8, //change limit as per your requirements
            dictMaxFilesExceeded: "Maximum upload limit reached",
            acceptedFiles: "image/*",
            dictInvalidFileType: "upload only JPG/PNG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Arrastra archivos aquí o haz clic para subir.", // Cambiar el mensaje predeterminado
            dictRemoveFile: "Eliminar archivo", // Cambiar el texto para eliminar archivo
            dictCancelUpload: "Cancelar subida", // Cambiar el texto para cancelar subida
            dictMaxFilesExceeded: "Ha alcanzado el límite de archivos permitidos.", // Cambiar el texto para archivos excedidos
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileListEan[j] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": j
                    };
                    input = $('#fileupload-files-ean').data('name');
                    $('#fileupload-files-ean').append(
                        `<input type="hidden" id="file_${ j }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    j += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileListEan.length; f++) {
                        if (fileListEan[f].fileName == file.name) {
                            rmvFile = fileListEan[f].serverFileName;
                            $("#file_" + fileListEan[f].fileId).remove();
                        }
                    }
                });
                //here

                @if (isset($viewData['inventory_images_ean']))
                    @foreach ($viewData['inventory_images_ean'] as $imagesnewean)
                        var mockFile = {
                            name: "{{ $viewData['product']->name }}_" + j,
                            size: 12345
                        };

                        myDropzone.emit("addedfile", mockFile);

                        myDropzone.emit("thumbnail", mockFile,
                            "/uploads/ean-images/{{ $imagesnewean['image_path'] }}");
                        myDropzone.emit("complete", mockFile);
                        fileListEan[j] = {
                            "serverFileName": "{{ $imagesnewean['image_path'] }}",
                            "fileName": "{{ $viewData['product']->name }}_" + i,
                            "fileId": j
                        };
                        j += 1;
                    @endforeach
                @endif

            }
        });
    </script>
    <script>
        $('.openean').click(function() {
            $('.imageean').toggle();
        });

        function startScanner() {
            $('.imagesca').toggle();
            Quagga.init({
                inputStream: {
                    name: "Live",
                    type: "LiveStream",
                    target: document.querySelector('#data-data'),
                    constraints: {
                        width: 320,
                        height: 320,
                        facingMode: "environment"
                    },
                },
                decoder: {
                    readers: [
                        "code_128_reader",
                        "ean_reader",
                        "ean_8_reader",
                        "code_39_reader",
                        "code_39_vin_reader",
                        "codabar_reader",
                        "upc_reader",
                        "upc_e_reader",
                        "i2of5_reader"
                    ],
                },

            }, function(err) {
                if (err) {
                    console.log(err);
                    return
                }

                console.log("Initialization finished. Ready to start");
                Quagga.start();

                // Set flag to is running
                _scannerIsRunning = true;
            });

            Quagga.onDetected(function(result) {
                $("#ean").val(result.codeResult.code);
            });
        }

        function stopscan() {
            $('.imagesca').toggle();
            Quagga.stop();
        }
    </script>
@endsection
