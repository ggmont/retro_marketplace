<ul class="nav nav-pills nav-stacked">
  <li role="presentation" {!! $account_menu == 'profile' ? 'class="active"' : '' !!}><a href="{{ url('account/profile') }}" >{{ __('Perfil') }}</a></li>

  
  <li role="presentation" {!! $account_menu == 'inventory' ? 'class="active"' : '' !!}><a href="{{ url('account/inventory') }}">{{ __('Mi inventario') }}</a></li>
  <li role="presentation" {!! $account_menu == 'sales' ? 'class="active"' : '' !!}><a href="{{ url('account/sales') }}">{{ __('Ventas') }}</a></li>
  <li role="presentation" {!! $account_menu == 'purchases' ? 'class="active"' : '' !!}><a href="{{ url('account/purchases') }}">{{ __('Compras') }}</a></li>
  <!--
  <li role="presentation" {!! $account_menu == 'account' ? 'class="active"' : '' !!}><a href="{{ url('account') }}" >{{ __('Tablero') }}</a></li>  
  <li role="presentation" {!! $account_menu == 'messages' ? 'class="active"' : '' !!}><a href="{{ url('account/messages') }}">{{ __('Mensajes') }}</a></li>
  <li role="presentation" {!! $account_menu == 'events' ? 'class="active"' : '' !!}><a href="{{ url('account/events') }}">{{ __('Eventos') }}</a></li>
  -->
  <li role="presentation" {!! $account_menu == 'settings' ? 'class="active"' : '' !!}><a href="{{ url('account/settings') }}">{{ __('Configuración') }}</a></li>
  <li role="presentation" {!! $account_menu == 'bank' ? 'class="active"' : '' !!}><a href="{{ url('account/banks') }}">{{ __('Cuenta bancaria') }}</a></li>
</ul>
