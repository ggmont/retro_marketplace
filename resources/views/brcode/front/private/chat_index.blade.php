@extends('brcode.front.layout.app_chat')
@section('content-css-include')
    <style>
        .tab {
            overflow: hidden;
        }

        .tab-content {
            max-height: 0;
            transition: all 0.5s;
        }

        input:checked+.tab-label .test {
            background-color: #000;
        }

        input:checked+.tab-label .test svg {
            transform: rotate(180deg);
            stroke: #fff;
        }

        input:checked+.tab-label::after {
            transform: rotate(90deg);
        }

        input:checked~.tab-content {
            max-height: 100vh;
        }

        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Jost', sans-serif;
            font-weight: 800;
        }

        .w-88 {
            width: 99% !important;
        }
    </style>
@endsection
@section('content')
    <div class="modal fade" id="modalComplaint" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog complaint-form" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title">¿Porque quieres denunciar a este usuario?</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <form action="{{ route('complaint_user', $usuario->id) }}" method="POST">
                            {{ csrf_field() }}
                            <input name="user_id" type="hidden" value="{{ auth()->user()->id }}">
                            <div class="wide-block pb-1 pt-2">
                                <div class="input-list">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="Mal comportamiento en el chat"
                                            name="category" id="radioList1">
                                        <label class="form-check-label" for="radioList1">Mal comportamiento en el
                                            chat</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="No contesta los mensajes"
                                            name="category" id="radioList2">
                                        <label class="form-check-label" for="radioList2">No contesta los mensajes</label>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit"
                            class="btn confirmclosed btn-submit btn-primary w-100 text-lg font-extrabold">Enviar</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        @livewireStyles
        <br>
        <div>
            @livewire('mobile.chat-mobile', ['usuario' => $usuario])
        </div>
        @include('brcode.front.private.modals.profile-chat')

        @livewireScripts
    @else
    @endif
@endsection

@section('content-script-include')
    <script src="{{ asset('brcode/js/pty.components.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>


    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="{{ asset('assets/noty/packaged/jquery.noty.packaged.min.js') }}"></script>

    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.4.1/chart.min.js"
        integrity="sha512-5vwN8yor2fFT9pgPS9p9R7AszYaNn0LkQElTXIsZFCL7ucT8zDCAqlQXDdaqgA1mZP47hdvztBMsIoFxq/FyyQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        // Obtener el elemento contenedor de chat
        const chatContainer = document.getElementById('chat-container');

        // Establecer la propiedad scrollTop al máximo
        chatContainer.scrollTop = chatContainer.scrollHeight;
    </script>
@endsection
@section('content-script')
    @livewireScripts

    <script>
        // Función para desplazar la página hacia abajo
        function scrollToBottom() {
            window.scrollTo(0, document.body.scrollHeight);
        }

        // Desplazar la página hacia abajo al cargar la página
        window.addEventListener('load', function() {
            scrollToBottom();

            // Observar cambios en el DOM para desplazar automáticamente en caso de nuevos contenidos
            const observer = new MutationObserver(scrollToBottom);
            const config = {
                childList: true,
                subtree: true
            };
            observer.observe(document.body, config);
        });
    </script>
 

    <script>
        window.addEventListener('show-form-chat', event => {
            $('#chatModal').modal('show');
        })

        window.addEventListener('hide-form-chat', event => {
            $('#chatModal').modal('hide');
        })
    </script>
    @stack('scripts')
@endsection
