@extends('brcode.front.layout.app_config')
@section('content-css-include')
    <style>
        .dataTables_filter input {
            width: 400px !important;
            background-color: white;
            color: black;
        }

        .dataTables_filter input:focus {
            outline: none !important;
            border: 1px solid red;
            box-shadow: 0 0 10px #719ECE;
            background-color: white;
            color: black;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('brcode/css/dataTables.bootstrap4.min.css') }}">
@endsection
@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div class="header-area" id="headerArea">
            <div class="container h-100 d-flex align-items-center justify-content-between">
                <!-- Back Button-->
                <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                <!-- Page Title-->
                <div class="page-heading">
                    <h6 class="mb-0 font-extrabold">{!! __('Mis transacciones') !!}</h6>
                </div>
                <!-- Navbar Toggler-->

                @if (Auth::user())
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @else
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @endif
            </div>
        </div>

        <div id="appCapsule">


            <ul class="nav nav-tabs capsuled" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="tab" href="#national" role="tab" aria-selected="true">
                        Nacionales
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#international" role="tab" aria-selected="false">
                        Internacionales
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="national" role="tabpanel">
                    <iframe
                        src="https://beseif.com/marketplace/pre_mis-transacciones?codigo={{ $jsonDatatwo['codigo'] }}&webview=true"
                        style="width: 100%; height: 700px; border-radius: 10px;"></iframe>
                </div>

                <div class="tab-pane fade" id="international" role="tabpanel">
                    <div class="col-md-12" style="display:none; ">

                        <table class="nk-table" id="nk-table-transactions" style="width:100% !important;">
                            <thead>
                                <tr>
                                    <th>{{ __('Pedido') }} #</th>
                                    <th>{{ __('Fecha') }}</th>
                                    <th>{{ __('Valor') }}</th>
                                    <th>{{ __('Decripción') }}</th>
                                </tr>
                            </thead>

                            <tbody>
                                @isset($viewData['trans'])
                                    @foreach ($viewData['trans'] as $key)
                                        @php($stat = true)
                                        @if ($key->paid_out == 'N' || $key->paid_out == 'P')
                                            @php($stat = false)
                                        @else
                                            @php($stat = true)
                                        @endif
                                        @if ($stat)
                                            <tr>
                                                <td>
                                                    @if (!in_array($key->status, ['EC', 'CE', 'AC', 'SC', 'PV']))
                                                        <a target="_blank"
                                                            href="/account/{{ $key->tipoSeller ? 'sales' : ($key->status == 'FD' ? 'sales' : 'purchases') }}/view/{{ in_array($key->status, ['FD', 'CS', 'RE', 'RF']) ? $key->instructions : $key->order_identification }}">{{ $key->order_identification }}</a>
                                                    @elseif($key->status != 'EC')
                                                        {{ $key->order_identification }}
                                                    @endif

                                                </td>
                                                <td>

                                                    {{ $key->updated_at->format('d/m/Y H:i') }}
                                                </td>

                                                <td>
                                                    @if ($key->stats == 'Compra de Productos')
                                                        <div class="" style="color: #99121d">
                                                            -
                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                        </div>
                                                    @elseif($key->stats == 'Venta de Productos')
                                                        <div class="" style="color: #276749">
                                                            +
                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                        </div>
                                                    @elseif($key->stats == 'Saldo retirado por RetroGamingMarket')
                                                        <div class="" style="color: #99121d">
                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                        </div>
                                                    @elseif($key->stats == 'Saldo retirado')
                                                        <div class="" style="color: #99121d">
                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                        </div>
                                                    @elseif($key->stats == 'Comisión RetroGamingMarket')
                                                        <div class="" style="color: #99121d">
                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                        </div>
                                                    @elseif($key->stats == 'Reembolsos - ventas')
                                                        <div class="" style="color: #99121d">
                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                        </div>
                                                    @else
                                                        <div class="" style="color: #276749">
                                                            +
                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                        </div>
                                                    @endif
                                                </td>

                                                <td>
                                                    {{ __($key->stats) }}
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endisset

                            </tbody>
                        </table>
                    </div>

                    <div
                        style="display: @isset($viewData['d3']) {{ $viewData['d3'] ? 'none' : 'block' }} @endisset ;">
                        <div
                            style="display: @isset($viewData['d2']) {{ $viewData['d2'] ? 'none' : 'block' }} @endisset ;">
                            <div
                                style="display: @isset($viewData['d1']) {{ $viewData['d1'] ? 'none' : 'block' }} @endisset ;">
                                @livewire('mobile.transactions-mobile')
                            </div>
                        </div>
                    </div>


                    @isset($viewData['d1'])
                        @isset($viewData['d2'])
                            <div class="section full">

                                <div class="rating-and-review-wrapper bg-white mb-5">
                                    <button type="button"
                                        class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed"
                                        data-toggle="modal" data-target="#exampleModal">
                                        <span class="fa-solid fa-filter"></span>
                                    </button>

                                    <div class="accordion" id="accordionExample1">
                                        @foreach ($viewData['trans'] as $key)
                                            @php($stat = true)
                                            @if ($key->paid_out == 'N' || $key->paid_out == 'P')
                                                @php($stat = false)
                                            @else
                                                @php($stat = true)
                                            @endif
                                            @if ($stat)
                                                <ul class="listview image-listview media accordion-item">
                                                    <li class="item">
                                                        <h2 class="accordion-header">
                                                            <button class="listview image-listview media accordion-button collapsed"
                                                                type="button" data-bs-toggle="collapse"
                                                                data-bs-target="#accordion{{ $key->id }}">
                                                                <dt class="col-6 text-sm">
                                                                    {{ __($key->stats) }}
                                                                    @if ($key->status == 'YV')
                                                                        {{ $key->seller->user_name }}
                                                                    @elseif($key->status == 'YC')
                                                                        {{ $key->buyer->user_name }}
                                                                    @endif
                                                                </dt>
                                                                <dd class="col-6 text-right text-sm font-weight-bold">
                                                                    @if ($key->stats == 'Compra de Productos')
                                                                        <span class="badge badge-danger">
                                                                            -
                                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                        </span>
                                                                    @elseif($key->stats == 'Venta de Productos')
                                                                        <span class="badge badge-success">
                                                                            +
                                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                        </span>
                                                                    @elseif($key->stats == 'Saldo retirado por RetroGamingMarket')
                                                                        <span class="badge badge-danger">
                                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                        </span>
                                                                    @elseif($key->stats == 'Saldo retirado')
                                                                        <span class="badge badge-danger">
                                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                        </span>
                                                                    @elseif($key->stats == 'Comisión RetroGamingMarket')
                                                                        <span class="badge badge-danger">
                                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                        </span>
                                                                    @elseif($key->stats == 'Reembolsos - ventas')
                                                                        <span class="badge badge-danger">
                                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                        </span>
                                                                    @elseif($key->stats == 'Pedido pendiente de envío')
                                                                        <span class="badge badge-warning">
                                                                            +
                                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                        </span>
                                                                    @else
                                                                        <span class="badge badge-success">
                                                                            +
                                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                        </span>
                                                                    @endif
                                                                </dd>
                                                            </button>
                                                        </h2>
                                                        <div id="accordion{{ $key->id }}" class="accordion-collapse collapse"
                                                            data-bs-parent="#accordionExample{{ $key->id }}">
                                                            <div class="accordion-body">
                                                                <div class="item-body-wrapper">
                                                                    <dl class="row evaluations-descriptionList mb-2">
                                                                        <dt class="col-6 color-primary">Fecha</dt>
                                                                        <dd class="col-6 text-right color-primary font-weight-bold">
                                                                            {{ $key->updated_at->format('d/m/Y H:i') }}</dd>
                                                                        <dt class="col-6 color-primary">Número de transacción</dt>
                                                                        <dd class="col-6 text-right color-primary font-weight-bold">

                                                                            {{ $key->order_identification }}

                                                                        </dd>
                                                                        @if (!in_array($key->status, ['EC', 'CE', 'AC', 'SC', 'PV', 'AS', 'YC', 'YV']))
                                                                            <dt class="col-12 color-primary"> <a
                                                                                    href="/account/{{ $key->tipoSeller ? 'sales' : ($key->status == 'FD' ? 'sales' : 'purchases') }}/view/{{ in_array($key->status, ['FD', 'CS', 'RE', 'RF']) ? $key->instructions : $key->order_identification }}"
                                                                                    class="btn btn-success w-100 font-bold">Ver
                                                                                    transaccion</a>
                                                                            </dt>
                                                                        @else
                                                                        @endif
                                                                    </dl>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            @endif
                                        @endforeach
                                    </div>

                                </div>

                            </div>
                        @endisset
                    @endisset

                    @isset($viewData['d3'])
                        <div class="section full">
                            <div style="display:  {{ $viewData['d3'] ? 'block' : 'none' }} ;">
                                <div class="rating-and-review-wrapper bg-white mb-5">
                                    <button type="button"
                                        class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed"
                                        data-toggle="modal" data-target="#exampleModal">
                                        <span class="fa-solid fa-filter"></span>
                                    </button>

                                    <div class="accordion" id="nk-table-resumen">
                                        @isset($viewData['resumen'])
                                            @foreach ($viewData['resumen'] as $key)
                                                <ul class="listview image-listview media accordion-item">
                                                    <li class="item">
                                                        <h2 class="accordion-header">
                                                            <button
                                                                class="listview image-listview media accordion-button collapsed"
                                                                type="button" data-bs-toggle="collapse"
                                                                data-bs-target="#accordion{{ $key->id }}">
                                                                <dt class="col-6 text-sm">
                                                                    {{ __($key->stats) }}
                                                                    @if ($key->status == 'YV')
                                                                        {{ $key->seller->user_name }}
                                                                    @elseif($key->status == 'YC')
                                                                        {{ $key->buyer->user_name }}
                                                                    @endif
                                                                </dt>
                                                                <dd class="col-6 text-right text-sm font-weight-bold">
                                                                    @if ($key->stats == 'Compra de Productos')
                                                                        <span class="badge badge-danger">
                                                                            -
                                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                        </span>
                                                                    @elseif($key->stats == 'Venta de Productos')
                                                                        <span class="badge badge-success">
                                                                            +
                                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                        </span>
                                                                    @elseif($key->stats == 'Saldo retirado por RetroGamingMarket')
                                                                        <span class="badge badge-danger">
                                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                        </span>
                                                                    @elseif($key->stats == 'Saldo retirado')
                                                                        <span class="badge badge-danger">
                                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                        </span>
                                                                    @elseif($key->stats == 'Comisión RetroGamingMarket')
                                                                        <span class="badge badge-danger">
                                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                        </span>
                                                                    @elseif($key->stats == 'Reembolsos - ventas')
                                                                        <span class="badge badge-danger">
                                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                        </span>
                                                                    @elseif($key->stats == 'Pedido pendiente de envío')
                                                                        <span class="badge badge-warning">
                                                                            +
                                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                        </span>
                                                                    @else
                                                                        <span class="badge badge-success">
                                                                            +
                                                                            {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                            {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                        </span>
                                                                    @endif
                                                                </dd>
                                                            </button>
                                                        </h2>
                                                        <div id="accordion{{ $key->id }}"
                                                            class="accordion-collapse collapse"
                                                            data-bs-parent="#accordionExample{{ $key->id }}">
                                                            <div class="accordion-body">
                                                                <div class="item-body-wrapper">
                                                                    <dl class="row evaluations-descriptionList mb-2">
                                                                        <dt class="col-6 color-primary">Fecha</dt>
                                                                        <dd
                                                                            class="col-6 text-right color-primary font-weight-bold">
                                                                            {{ $key->updated_at->format('d/m/Y H:i') }}</dd>
                                                                        <dt class="col-6 color-primary">Número de transacción</dt>
                                                                        <dd
                                                                            class="col-6 text-right color-primary font-weight-bold">

                                                                            {{ $key->order_identification }}

                                                                        </dd>
                                                                        @if (!in_array($key->status, ['EC', 'CE', 'AC', 'SC', 'PV', 'AS', 'YC', 'YV']))
                                                                            <dt class="col-12 color-primary"> <a
                                                                                    href="/account/{{ $key->tipoSeller ? 'sales' : ($key->status == 'FD' ? 'sales' : 'purchases') }}/view/{{ in_array($key->status, ['FD', 'CS', 'RE', 'RF']) ? $key->instructions : $key->order_identification }}"
                                                                                    class="btn btn-success w-100 font-bold">Ver
                                                                                    transaccion</a>
                                                                            </dt>
                                                                        @else
                                                                        @endif
                                                                    </dl>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            @endforeach
                                        @endisset
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endisset




                    <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content bg-white">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="d-flex align-items-center justify-content-between mb-4">
                                            <h4 class="modal-title" id="addnewcontactlabel">Filtrar por :</h4>
                                            <button class="btn btn-close p-1 ms-auto me-0" class="close"
                                                data-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="wide-block pb-1 pt-2">

                                            <form action="/account/transactions" method="POST">

                                                {{ csrf_field() }}






                                                <div class="form-group boxed">
                                                    <div class="input-wrapper">
                                                        <label class="form-label" for="city5">Fecha</label>

                                                        <select name="d1" style="width: 100%"
                                                            class="form-control select2 nk-tran-option nk-date-option form-select">
                                                            <option value="" selected> @lang('messages.alls')
                                                            </option>
                                                            @foreach ($viewData['dates'] as $key)
                                                                <option value="{!! str_replace(' / ', '-', $key->Dates) !!}"
                                                                    @isset($viewData['d2']) {{ $viewData['d2'] == str_replace(' / ', '-', $key->Dates) ? 'selected' : '' }} @endisset>
                                                                    {{ $key->Dates }}</option>
                                                            @endforeach
                                                        </select>

                                                    </div>
                                                </div>

                                                <div class="form-group boxed">
                                                    <div class="input-wrapper">
                                                        <label class="form-label" for="city5">Tipo</label>

                                                        <select name="d2" style="width: 100%"
                                                            class="form-control select2 nk-tran-option nk-date-option form-select">
                                                            <option value="" selected> @lang('messages.alls')
                                                            </option>
                                                            <option value="CDD"
                                                                @isset($viewData['d2']) {{ $viewData['d2'] == 'CDD' ? 'selected' : '' }} @endisset>
                                                                {{ __('Compra de Productos') }}</option>
                                                            <option value="VCR"
                                                                @isset($viewData['d2']) {{ $viewData['d2'] == 'VCR' ? 'selected' : '' }} @endisset>
                                                                {{ __('Pedido pendiente de envío') }}</option>
                                                            <option value="VDD"
                                                                @isset($viewData['d2']) {{ $viewData['d2'] == 'VDD  ' ? 'selected' : '' }} @endisset>
                                                                {{ __('Venta de Productos') }}</option>
                                                            <option value="CRE"
                                                                @isset($viewData['d2']) {{ $viewData['d2'] == 'CRE' ? 'selected' : '' }} @endisset>
                                                                {{ __('Pagos extra - compras') }}</option>
                                                            <option value="VRE"
                                                                @isset($viewData['d2']) {{ $viewData['d2'] == 'VRE' ? 'selected' : '' }} @endisset>
                                                                {{ __('Pagos extras - ventas') }}</option>
                                                            <option value="CRF"
                                                                @isset($viewData['d2']) {{ $viewData['d2'] == 'CRF' ? 'selected' : '' }} @endisset>
                                                                {{ __('Rembolsos - compras') }}</option>
                                                            <option value="VRF"
                                                                @isset($viewData['d2']) {{ $viewData['d2'] == 'VRF' ? 'selected' : '' }} @endisset>
                                                                {{ __('Reembolsos - ventas') }}</option>
                                                            <option value="CEC"
                                                                @isset($viewData['d2']) {{ $viewData['d2'] == 'CEC' ? 'selected' : '' }} @endisset>
                                                                {{ __('Programa de recomendación') }}</option>
                                                            <option value="CFD"
                                                                @isset($viewData['d2']) {{ $viewData['d2'] == 'CFD' ? 'selected' : '' }} @endisset>
                                                                {{ __('Comisión RetroGamingMarket') }}</option>
                                                            <option value="CCE"
                                                                @isset($viewData['d2']) {{ $viewData['d2'] == 'CCE' ? 'selected' : '' }} @endisset>
                                                                {{ __('Saldo retirado') }}</option>
                                                            <option value="CAC"
                                                                @isset($viewData['d2']) {{ $viewData['d2'] == 'CAC' ? 'selected' : '' }} @endisset>
                                                                {{ __('Saldo agregado por RetroGamingMarket') }}</option>
                                                            <option value="CRC"
                                                                @isset($viewData['d2']) {{ $viewData['d2'] == 'CRC' ? 'selected' : '' }} @endisset>
                                                                {{ __('Saldo retirado por RetroGamingMarket') }}</option>
                                                        </select>

                                                    </div>
                                                </div>

                                                <div class="mt-1 col-md-12 text-start">
                                                    <div class="form-check">
                                                        <input type="checkbox" name="d3" class="form-check-input"
                                                            @isset($viewData['d3']) {{ $viewData['d3'] ? 'checked' : '' }} @endisset>
                                                        <label class="form-check-label" for="br-register-terms">
                                                            {!! __('Resumen') !!}
                                                    </div>

                                                </div>


                                                <div class="col-md">
                                                    <button class="btn btn-primary w-100 font-bold" type="submit">
                                                        @lang('messages.filter_two') </button>
                                                </div>
                                                @foreach (Auth::user()->roles as $key)
                                                    @if ($key->role_id == 3)
                                                        @php($sellerType = 'Y')
                                                    @endif
                                                @endforeach
                                                <br>



                                                <div class="col-md">
                                                    <button class="btn btn-success w-100 font-bold" href="#"
                                                        style="display: @isset($viewData['d3']) {{ $viewData['d3'] ? 'none' : 'block' }} @endisset ;"
                                                        type="button" id="btnExportTransactions"> @lang('messages.export')
                                                    </button>
                                                    @isset($viewData['d3'])
                                                        <button class="btn btn-danger w-100 font-bold" href="#"
                                                            style="display: @isset($viewData['d3']) {{ $viewData['d3'] ? 'block' : 'none' }} @endisset ;"
                                                            type="button" id="btnExportResumen"> @lang('messages.export') </button>
                                                    @endisset
                                                </div>

                                            </form>

                                        </div>
                                    </div>
                                    <div class="modal-footer">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    @else
        <!-- Content Header (Page header) -->
        <div class="container">
            <div class="col-lg-12">
                <ul class="nk-breadcrumbs">
                    <br>
                    <li>
                        <div class="section-title1">
                            <h3 class="retro" style="font-size:20px;">{!! __('Mis transacciones') !!}</h3>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="container">
            <div class="nk-gap-1"></div>
            <span class="text-xs retro">
                @include('partials.flash')
            </span>
            <div class="nk-gap-1"></div>
            <form action="/account/transactions" method="POST">

                {{ csrf_field() }}

                <div class="row nk-mchimp nk-form nk-form-style-1 validate">




                    <div class="col-md">
                        <div class="nes-select">
                            <select name="d1" id="default_select"
                                class="retro form-control nk-tran-option nk-date-option">
                                <option value="">---</option>
                                @foreach ($viewData['dates'] as $key)
                                    <option value="{!! str_replace(' / ', '-', $key->Dates) !!}"
                                        @isset($viewData['d2']) {{ $viewData['d2'] == str_replace(' / ', '-', $key->Dates) ? 'selected' : '' }} @endisset>
                                        {{ $key->Dates }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md">
                        <div class="nes-select">
                            <select name="d2" class="retro form-control nk-tran-option nk-status-option">
                                <option value="">---</option>
                                <option value="CDD"
                                    @isset($viewData['d2']) {{ $viewData['d2'] == 'CDD' ? 'selected' : '' }} @endisset>
                                    {{ __('Compra de Productos') }}</option>
                                <option value="VDD"
                                    @isset($viewData['d2']) {{ $viewData['d2'] == 'VDD  ' ? 'selected' : '' }} @endisset>
                                    {{ __('Venta de Productos') }}</option>
                                <option value="CRE"
                                    @isset($viewData['d2']) {{ $viewData['d2'] == 'CRE' ? 'selected' : '' }} @endisset>
                                    {{ __('Pagos extra - compras') }}</option>
                                <option value="VRE"
                                    @isset($viewData['d2']) {{ $viewData['d2'] == 'VRE' ? 'selected' : '' }} @endisset>
                                    {{ __('Pagos extras - ventas') }}</option>
                                <option value="CRF"
                                    @isset($viewData['d2']) {{ $viewData['d2'] == 'CRF' ? 'selected' : '' }} @endisset>
                                    {{ __('Rembolsos - compras') }}</option>
                                <option value="VRF"
                                    @isset($viewData['d2']) {{ $viewData['d2'] == 'VRF' ? 'selected' : '' }} @endisset>
                                    {{ __('Reembolsos - ventas') }}</option>
                                <option value="CEC"
                                    @isset($viewData['d2']) {{ $viewData['d2'] == 'CEC' ? 'selected' : '' }} @endisset>
                                    {{ __('Programa de recomendación') }}</option>
                                <option value="CFD"
                                    @isset($viewData['d2']) {{ $viewData['d2'] == 'CFD' ? 'selected' : '' }} @endisset>
                                    {{ __('Comisión RetroGamingMarket') }}</option>
                                <option value="CCE"
                                    @isset($viewData['d2']) {{ $viewData['d2'] == 'CCE' ? 'selected' : '' }} @endisset>
                                    {{ __('Saldo retirado') }}</option>
                                <option value="CAC"
                                    @isset($viewData['d2']) {{ $viewData['d2'] == 'CAC' ? 'selected' : '' }} @endisset>
                                    {{ __('Saldo agregado por RetroGamingMarket') }}</option>
                                <option value="CRC"
                                    @isset($viewData['d2']) {{ $viewData['d2'] == 'CRC' ? 'selected' : '' }} @endisset>
                                    {{ __('Saldo retirado por RetroGamingMarket') }}</option>

                            </select>
                        </div>
                    </div>
                    <div class="col-md">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label class="text-gray-800 h5 retro"><input type="checkbox" name="d3"
                                @isset($viewData['d3']) {{ $viewData['d3'] ? 'checked' : '' }} @endisset>
                            <br>
                            <font color="black">{!! __('Resumen') !!}</font>
                        </label>
                    </div>
                    <div class="col-md">
                        <button class="nes-btn nk-btn-xs btn-block nk-btn-rounded nk-btn-color-white" type="submit"><span
                                class="retro font-extrabold text-base">@lang('messages.filter_two')</span></button>
                    </div>
                    @foreach (Auth::user()->roles as $key)
                        @if ($key->role_id == 3)
                            @php($sellerType = 'Y')
                        @endif
                    @endforeach


                    <div class="col-md">
                        <button class="nes-btn nk-btn-xs btn-block nk-btn-rounded nk-btn-color-white" href="#"
                            style="display: @isset($viewData['d3']) {{ $viewData['d3'] ? 'none' : 'block' }} @endisset ;"
                            type="button" id="btnExportTransactions"> <span
                                class="retro font-extrabold text-base">@lang('messages.export')</span></button>
                        @isset($viewData['d3'])
                            <button class="nes-btn nk-btn-xs btn-block nk-btn-rounded nk-btn-color-white" href="#"
                                style="display: @isset($viewData['d3']) {{ $viewData['d3'] ? 'block' : 'none' }} @endisset ;"
                                type="button" id="btnExportResumen"> <span
                                    class="retro font-extrabold text-base">@lang('messages.export')</span></button>
                        @endisset
                    </div>

                    <div class="nk-gap-2"></div>
                </div>
            </form>
            <div class="row">
                <div class="col-md-12" style="display:none; ">

                    <table class="nk-table" id="nk-table-transactions" style="width:100% !important;">
                        <thead>
                            <tr>
                                <th>{{ __('Pedido') }} #</th>
                                <th>{{ __('Fecha') }}</th>
                                <th>{{ __('Valor') }}</th>
                                <th>{{ __('Decripción') }}</th>
                            </tr>
                        </thead>

                        <tbody>
                            @isset($viewData['trans'])
                                @foreach ($viewData['trans'] as $key)
                                    @php($stat = true)
                                    @if ($key->paid_out == 'N' || $key->paid_out == 'P')
                                        @php($stat = false)
                                    @else
                                        @php($stat = true)
                                    @endif
                                    @if ($stat)
                                        <tr>
                                            <td>
                                                @if (!in_array($key->status, ['EC', 'CE', 'AC', 'SC', 'PV']))
                                                    <a target="_blank"
                                                        href="/account/{{ $key->tipoSeller ? 'sales' : ($key->status == 'FD' ? 'sales' : 'purchases') }}/view/{{ in_array($key->status, ['FD', 'CS', 'RE', 'RF']) ? $key->instructions : $key->order_identification }}">{{ $key->order_identification }}</a>
                                                @elseif($key->status != 'EC')
                                                    {{ $key->order_identification }}
                                                @endif

                                            </td>
                                            <td>

                                                {{ $key->updated_at->format('d/m/Y H:i') }}
                                            </td>

                                            <td>
                                                @if ($key->stats == 'Compra de Productos')
                                                    <div class="" style="color: #99121d">
                                                        -
                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                    </div>
                                                @elseif($key->stats == 'Venta de Productos')
                                                    <div class="" style="color: #276749">
                                                        +
                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                    </div>
                                                @elseif($key->stats == 'Saldo retirado por RetroGamingMarket')
                                                    <div class="" style="color: #99121d">
                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                    </div>
                                                @elseif($key->stats == 'Saldo retirado')
                                                    <div class="" style="color: #99121d">
                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                    </div>
                                                @elseif($key->stats == 'Comisión RetroGamingMarket')
                                                    <div class="" style="color: #99121d">
                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                    </div>
                                                @elseif($key->stats == 'Reembolsos - ventas')
                                                    <div class="" style="color: #99121d">
                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                    </div>
                                                @else
                                                    <div class="" style="color: #276749">
                                                        +
                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                    </div>
                                                @endif
                                            </td>

                                            <td>
                                                {{ __($key->stats) }}
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endisset

                        </tbody>
                    </table>
                </div>

                <div class="col-md-12"
                    style="display: @isset($viewData['d3']) {{ $viewData['d3'] ? 'none' : 'block' }} @endisset ;">
                    <div class="col-md-12"
                        style="display: @isset($viewData['d2']) {{ $viewData['d2'] ? 'none' : 'block' }} @endisset ;">
                        <div class="col-md-12"
                            style="display: @isset($viewData['d1']) {{ $viewData['d1'] ? 'none' : 'block' }} @endisset ;">
                            <livewire:transactions-table></livewire:transactions-table>
                        </div>
                    </div>
                </div>

                @isset($viewData['d1'])
                    @isset($viewData['d2'])
                        <div class="wishlist-table table-responsive">
                            <div class="col-md-12"
                                style="
                                                            display:  {{ $viewData['d1'] ? 'block' : 'none' }} ;
                                                            display:  {{ $viewData['d2'] ? 'block' : 'none' }} ;
                                                            display: @isset($viewData['d3']) {{ $viewData['d3'] ? 'none' : 'block' }} @endisset ;">

                                <table class="w-full table-auto min-w-max" id="" style="width:100% !important;">
                                    <thead>
                                        <tr class="text-sm leading-normal text-gray-100 uppercase bg-red-700">
                                            <th>{{ __('Pedidos') }} #</th>
                                            <th>{{ __('Fecha') }}</th>
                                            <th>{{ __('Valor') }}</th>
                                            <th>{{ __('Decripción') }}</th>
                                        </tr>
                                    </thead>

                                    <tbody class="text-sm font-light text-gray-600">
                                        @isset($viewData['trans'])
                                            @foreach ($viewData['trans'] as $key)
                                                @php($stat = true)
                                                @if ($key->paid_out == 'N' || $key->paid_out == 'P')
                                                    @php($stat = false)
                                                @else
                                                    @php($stat = true)
                                                @endif
                                                @if ($stat)
                                                    <tr class="border-b border-gray-500 hover:bg-gray-200">

                                                        <td>
                                                            @if (!in_array($key->status, ['EC', 'CE', 'AC', 'SC', 'PV']))
                                                                <a target="_blank"
                                                                    href="/account/{{ $key->tipoSeller ? 'sales' : ($key->status == 'FD' ? 'sales' : 'purchases') }}/view/{{ in_array($key->status, ['FD', 'CS', 'RE', 'RF']) ? $key->instructions : $key->order_identification }}">{{ $key->order_identification }}</a>
                                                            @elseif($key->status != 'EC')
                                                                {{ $key->order_identification }}
                                                            @endif

                                                        </td>
                                                        <td>
                                                            <span class="font-mono text-base retro">
                                                                {{ $key->updated_at->format('d/m/Y H:i') }}
                                                            </span>
                                                        </td>

                                                        <td>
                                                            <span class="font-mono text-base retro">
                                                                @if ($key->stats == 'Compra de Productos')
                                                                    <div class="" style="color: #99121d">
                                                                        -
                                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                    </div>
                                                                @elseif($key->stats == 'Venta de Productos')
                                                                    <div class="" style="color: #276749">
                                                                        +
                                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                    </div>
                                                                @elseif($key->stats == 'Saldo retirado por RetroGamingMarket')
                                                                    <div class="" style="color: #99121d">
                                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                    </div>
                                                                @elseif($key->stats == 'Saldo retirado')
                                                                    <div class="" style="color: #99121d">
                                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                    </div>
                                                                @elseif($key->stats == 'Comisión RetroGamingMarket')
                                                                    <div class="" style="color: #99121d">
                                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                    </div>
                                                                @elseif($key->stats == 'Reembolsos - ventas')
                                                                    <div class="" style="color: #99121d">
                                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                    </div>
                                                                @else
                                                                    <div class="" style="color: #276749">
                                                                        +
                                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                    </div>
                                                                @endif
                                                            </span>
                                                        </td>

                                                        <td>
                                                            {{ __($key->stats) }}
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endisset

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endisset
                @endisset

                @isset($viewData['d3'])
                    <div class="wishlist-table table-responsive">
                        <div class="col-md-12" style="display:  {{ $viewData['d3'] ? 'block' : 'none' }} ;">

                            <table class="w-full table-auto min-w-max" id="nk-table-resumen" style="width:100% !important;">
                                <thead>
                                    <tr class="text-sm leading-normal text-gray-100 uppercase bg-red-700">
                                        <th>{{ __('Decripción') }}</th>
                                        <th>{{ __('Valor') }}</th>
                                    </tr>
                                </thead>

                                <tbody class="text-sm font-light text-gray-600">
                                    @isset($viewData['resumen'])
                                        @foreach ($viewData['resumen'] as $key)
                                            <tr class="border-b border-gray-500 hover:bg-gray-200">

                                                <td>
                                                    {{ $key->stats }}
                                                </td>
                                                <td>
                                                    <span class="font-mono text-base retro">
                                                        @if ($key->stats == 'Compra de Productos')
                                                            <div class="" style="color: #99121d">
                                                                -
                                                                {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                            </div>
                                                        @elseif($key->stats == 'Venta de Productos')
                                                            <div class="" style="color: #276749">
                                                                +
                                                                {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                            </div>
                                                        @elseif($key->stats == 'Saldo retirado por RetroGamingMarket')
                                                            <div class="" style="color: #99121d">
                                                                {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                            </div>
                                                        @elseif($key->stats == 'Saldo retirado')
                                                            <div class="" style="color: #99121d">
                                                                {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                            </div>
                                                        @elseif($key->stats == 'Comisión RetroGamingMarket')
                                                            <div class="" style="color: #99121d">
                                                                {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                            </div>
                                                        @elseif($key->stats == 'Reembolsos - ventas')
                                                            <div class="" style="color: #99121d">
                                                                {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                            </div>
                                                        @else
                                                            <div class="" style="color: #276749">
                                                                +
                                                                {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                            </div>
                                                        @endif
                                                    </span>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endisset
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endisset
            </div>
        </div>
    @endif

@endsection
@section('content-script-include')
    <script src="{{ asset('brcode/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('brcode/js/dataTables.bootstrap4.min.js') }}"></script>
@endsection
@section('content-script')
    <script>
        $(document).ready(function() {
            $("#btnExportTransactions").click(function(e) {
                var table = document.getElementById('nk-table-transactions');
                var html = table.outerHTML;
                while (html.indexOf('á') != -1) html = html.replace('á', '&aacute;');
                while (html.indexOf('Á') != -1) html = html.replace('Á', '&Aacute;');
                while (html.indexOf('é') != -1) html = html.replace('é', '&eacute;');
                while (html.indexOf('É') != -1) html = html.replace('É', '&Eacute;');
                while (html.indexOf('í') != -1) html = html.replace('í', '&iacute;');
                while (html.indexOf('Í') != -1) html = html.replace('Í', '&Iacute;');
                while (html.indexOf('ó') != -1) html = html.replace('ó', '&oacute;');
                while (html.indexOf('Ó') != -1) html = html.replace('Ó', '&Oacute;');
                while (html.indexOf('ú') != -1) html = html.replace('ú', '&uacute;');
                while (html.indexOf('Ú') != -1) html = html.replace('Ú', '&Uacute;');
                while (html.indexOf('º') != -1) html = html.replace('º', '&ordm;');
                while (html.indexOf('ñ') != -1) html = html.replace('ñ', '&ntilde;');
                while (html.indexOf('Ñ') != -1) html = html.replace('Ñ', '&Ntilde;');
                while (html.indexOf('€') != -1) html = html.replace('€', '&euro;');


                var dt = new Date();
                var day = dt.getDate();
                var month = dt.getMonth() + 1;
                var year = dt.getFullYear();
                var hour = dt.getHours();
                var mins = dt.getMinutes();
                var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
                //creating a temporary HTML link element (they support setting file names)
                var a = document.createElement('a');
                //getting data from our div that contains the HTML table
                var data_type = 'data:application/vnd.ms-excel';
                a.href = data_type + ', ' + encodeURIComponent(
                    html);
                //setting the file name
                a.download = 'Transacciones_RGM_' + '{{ auth()->user()->user_name }}_' + postfix +
                    '.xls';
                //triggering the function
                a.click();
                //just in case, prevent default behaviour
                e.preventDefault();

            });
        });

        $(document).ready(function() {
            $("#btnExportResumen").click(function(e) {
                var table = document.getElementById('nk-table-resumen');
                var html = table.outerHTML;
                while (html.indexOf('á') != -1) html = html.replace('á', '&aacute;');
                while (html.indexOf('Á') != -1) html = html.replace('Á', '&Aacute;');
                while (html.indexOf('é') != -1) html = html.replace('é', '&eacute;');
                while (html.indexOf('É') != -1) html = html.replace('É', '&Eacute;');
                while (html.indexOf('í') != -1) html = html.replace('í', '&iacute;');
                while (html.indexOf('Í') != -1) html = html.replace('Í', '&Iacute;');
                while (html.indexOf('ó') != -1) html = html.replace('ó', '&oacute;');
                while (html.indexOf('Ó') != -1) html = html.replace('Ó', '&Oacute;');
                while (html.indexOf('ú') != -1) html = html.replace('ú', '&uacute;');
                while (html.indexOf('Ú') != -1) html = html.replace('Ú', '&Uacute;');
                while (html.indexOf('º') != -1) html = html.replace('º', '&ordm;');
                while (html.indexOf('ñ') != -1) html = html.replace('ñ', '&ntilde;');
                while (html.indexOf('Ñ') != -1) html = html.replace('Ñ', '&Ntilde;');
                while (html.indexOf('€') != -1) html = html.replace('€', '&euro;');


                var dt = new Date();
                var day = dt.getDate();
                var month = dt.getMonth() + 1;
                var year = dt.getFullYear();
                var hour = dt.getHours();
                var mins = dt.getMinutes();
                var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
                //creating a temporary HTML link element (they support setting file names)
                var a = document.createElement('a');
                //getting data from our div that contains the HTML table
                var data_type = 'data:application/vnd.ms-excel';
                a.href = data_type + ', ' + encodeURIComponent(
                    html);
                //setting the file name
                a.download = 'Resumen_Transacciones_RGM_' + '{{ auth()->user()->user_name }}_' +
                    postfix +
                    '.xls';
                //triggering the function
                a.click();
                //just in case, prevent default behaviour
                e.preventDefault();

            });
        });
    </script>
    <script>
        $('.nk-generate-bill').click(function() {
            if ($('.nk-date-option').val() != '---') {
                window.location.href = "/account/transactions/generate-pdf/" + $('.nk-date-option').val();
            } else {
                alert('Seleccione una fecha');
            }
        });
    </script>
@endsection
