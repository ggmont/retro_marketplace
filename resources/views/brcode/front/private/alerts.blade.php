@extends('brcode.front.layout.app_conversation')
@section('content-css-include')
    <style>
        body {
            margin-top: 20px;

        }

        h3 {
            font-size: 16px;
        }

        .text-navy {
            color: #1ab394;
        }

        .cart-product-imitation {
            text-align: center;
            padding-top: 30px;
            height: 80px;
            width: 80px;
            background-color: #f8f8f9;
        }

        .product-imitation.xl {
            padding: 120px 0;
        }

        .product-desc {
            padding: 20px;
            position: relative;
        }

        .ecommerce .tag-list {
            padding: 0;
        }

        .ecommerce .fa-star {
            color: #d1dade;
        }

        .ecommerce .fa-star.active {
            color: #f8ac59;
        }

        .ecommerce .note-editor {
            border: 1px solid #e7eaec;
        }

        table.shoping-cart-table {
            margin-bottom: 0;
        }

        table.shoping-cart-table tr td {
            border: none;
            text-align: right;
        }

        table.shoping-cart-table tr td.desc,
        table.shoping-cart-table tr td:first-child {
            text-align: left;
        }

        table.shoping-cart-table tr td:last-child {
            width: 80px;
        }

        .ibox {
            clear: both;
            margin-bottom: 25px;
            margin-top: 0;
            padding: 0;
        }

        .ibox.collapsed .ibox-content {
            display: none;
        }

        .ibox:after,
        .ibox:before {
            display: table;
        }

        .ibox-title {
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            background-color: #ffffff;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 3px 0 0;
            color: inherit;
            margin-bottom: 0;
            padding: 14px 15px 7px;
            min-height: 48px;
        }

        .ibox-content {
            background-color: #ffffff;
            color: inherit;
            padding: 15px 20px 20px 20px;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 1px 0;
        }

        .ibox-footer {
            color: inherit;
            border-top: 1px solid #e7eaec;
            font-size: 90%;
            background: #ffffff;
            padding: 10px 15px;
        }

        /* width */
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #888;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555;
        }

        .scroller {
            width: 100%;
            max-width: 1120px;
            height: 1000px;
            overflow: auto;
            overflow-x: hidden;
        }

        .promotion {
            background: radial-gradient(ellipse farthest-corner at right bottom, #FEDB37 0%, #FDB931 8%, #9f7928 30%, #8A6E2F 40%),
                radial-gradient(ellipse farthest-corner at left top, #FFFFFF 0%, #FFFFAC 8%, #D1B464 25%, #5d4a1f 62.5%, #5d4a1f 100%);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }
    </style>
@endsection
@section('content')
    <div class="header-area" id="headerArea">
        <div class="container h-100 d-flex align-items-center justify-content-between">
            <!-- Back Button-->
            <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
            <!-- Page Title-->
            <div class="page-heading">
                <h6 class="mb-0 font-extrabold">Alertas</h6>
            </div>
            <!-- Navbar Toggler-->

            @if (Auth::user())
                <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                    data-bs-target="#sidebarPanel">
                    <span></span><span></span><span></span>
                </div>
            @else
                <a href="#" id="showLogin" class="showLogin" class="lni lni-user d-flex flex-wrap"
                    data-nav-toggle="#nk-nav-mobile">
                    <span class="lni lni-user">
                        <span></span><span></span><span></span>
                    </span>
                </a>
            @endif
        </div>
    </div>
    <div class="page-content-wrapper-noti">
        <div class="container">
            <!-- Notifications Area-->
            <br>
            @if (count($viewData['header_newsletter']) > 0)
                @foreach ($viewData['header_newsletter'] as $key)
                    <div class="notification-area">
                        <div class="list-group-item d-flex"><span class="noti-icon"><i
                                    class="lni lni-warning"></i></span>
                            <div class="noti-info">
                                <h6>{!! $key->title !!}</h6>
                                <p>
                                    {!! $key->content !!}
                                </p>
                                <span>{{ \Carbon\Carbon::parse($key->created_at)->diffForHumans() }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
 
            @else
                <div class="page-content-wrapper">
                    <div class="container">
                        <div class="nk-gap-3"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <blockquote class="nk-blockquote">
                                    <div class="nk-blockquote-icon"></div>
                                    <div class="text-center nk-blockquote-content h3 font-extrabold">
                                        <font color="black">
                                            {{ __('No hay alertas que ver por el momento') }}
                                        </font>
                                    </div>
                                    <div class="nk-gap"></div>
                                    <div class="nk-blockquote-author"></div>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
