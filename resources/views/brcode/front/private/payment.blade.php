@extends('brcode.front.layout.app')
@section('content-css-include')

@endsection
@section('content')

    <div class="container">
        <br>

        <ul class="nk-breadcrumbs">
            <li>

                <div class="section-title1">
                    <h3 class="retro">@lang('messages.your_balance')</h3>
                </div>

            </li>
        </ul>
        <br>
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="nk-box-2" style="background-color: #ffffff; border: solid 1px #db2e2e">
                    <h5><span class="text-xs text-gray-900 retro">@lang('messages.cash_payment'):</span></h5>
                    <h2 class="text-center text-gray-900 text-md"><b>
                            <br>
                            {{ number_format(Auth::user()->cash, 2, '.', ' ') }} €
                        </b></h2>
                </div>
            </div>
            @if ($total > 0)
                <div class="col-lg-4 col-sm-6">
                    <div class="nk-box-2" style="background-color: #ffffff; border: solid 1px #db2e2e">
                        <h5><span class="text-xs text-gray-900 retro">@lang('messages.not_paid_payment'):</span></h5>
                        <h2 class="text-center text-gray-900 text-md"> <a href="/account/purchases" class="text-gray-900 h2"
                                target="_blank">
                                <br>
                                <font color="black"><span class="text-md">{{ number_format($total, 2, '.', ' ') }}
                                        € </span></font>
                            </a></h2>
                    </div>
                </div>
            @endif
            @if ($total > 0)
                <div class="col-lg-4 col-sm-6">
                    <div class="nk-box-2" style="background-color: #ffffff; border: solid 1px #db2e2e">
                        <center>
                            <h5><span class="text-xs text-gray-900 retro">@lang('messages.total_send')</span></h5>
                        </center>
                        <b>
                            <h2 class="text-center">
                                <a href="/account/purchases" class="text-gray-900 text-md" target="_blank">

                                    @if ($total > 0)
                                        <br>
                                        @if ($total < Auth::user()->cash)
                                            {{ number_format(Auth::user()->cash - $total, 2, '.', ' ') }} €
                                        @else
                                            {{ number_format($total - Auth::user()->cash, 2, '.', ' ') }} €
                                        @endif
                                    @else
                                        <br>
                                        $0.00
                                    @endif
                                </a>
                        </b>
                        </h2>
                    </div>
                </div>
            @endif
            @if (Auth::user()->special_cash > 0)
                <div class="col-lg-12 col-sm-12">
                    <div class="nk-box-2" style="background-color: #ffffff; border: solid 1px #db2e2e">
                        <center>
                            <h5><span class="text-xs text-gray-900 retro">@lang('messages.special_payment'):</span></h5>
                        </center>
                        <h2 class="text-center"><b>
                                <br>
                                <font color="black"> {{ number_format(Auth::user()->special_cash, 2, '.', ' ') }} €
                                </font>
                            </b></h2>
                    </div>
                </div>
            @endif
            <br>
            <div class="container">
                <ul class="nk-breadcrumbs">

                    <li>

                        <div class="section-title1">
                            <h3 class="retro">@lang('messages.method_payment')</h3>
                        </div>

                    </li>
                </ul>
            </div>
            <div class="col-lg-6">
                <div class="nk-gap-2"></div>
                <div class="nk-box-2" style="background-color: #ffffff; border: solid 1px #db2e2e">
                    <!----->

                    <div class="row">
                        <div class="text-center col-lg-2">
                            <i class="fa fa-bank fa-5x"></i>
                        </div>
                        @if ((new \Jenssegers\Agent\Agent())->isMobile())
                        ---------------------------------------
                        @endif
                        <div class="text-left col-lg-10">
                            <h4 class="text-gray-900 retro"> <b> @lang('messages.bank_transfer')</b></h4>
                            <br>
                            <p><i class="fa fa-clock-o" aria-hidden="true"></i> @lang('messages.sepa_payment')
                            </p>
                            <p>
                                <strong class="text-xs text-main-1 retro"> @lang('messages.dest_payment')</strong> <span
                                    class="text-gray-900">Retro Gaming Market</span> <br>
                                <strong class="text-xs text-main-1 retro">IBAN</strong> <span
                                    class="text-gray-900">ES8301828749940201553992</span> <br>
                                <strong class="text-xs text-main-1 retro">BIC</strong> <span
                                    class="text-gray-900">BBVAESMM</span> <br>
                                <strong class="text-xs text-main-1 retro">@lang('messages.payment_razon')</strong> <span
                                    class="text-gray-900">{{ Auth::user()->concept }}</span> <br>

                                <strong class="text-xs text-main-1 retro">@lang('messages.bank_name') </strong> <span
                                    class="text-gray-900"> BBVA </span> <br>
                            </p>
                            <p><span class="text-xs text-red-700 retro">@lang('messages.important'):</span>
                                @lang('messages.dont_forget_payment') <strong
                                    class="text-gray-900">{{ Auth::user()->concept }}</strong>@lang('messages.transfer_payment')
                            </p>

                        </div>


                    </div>

                    <!----->
                </div>
            </div>

            <div class="col-lg-6">
                <div class="nk-gap-2"></div>
                <div class="nk-box-2" style="background-color: #ffffff; border: solid 1px #db2e2e">
                    <div class="row">
                        <div class="text-center col-lg-2">
                            <i class="fa fa-paypal fa-5x"></i>
                        </div>
                        @if ((new \Jenssegers\Agent\Agent())->isMobile())
                        ---------------------------------------
                        @endif
                        <div class="text-left col-lg-10">
                            <h4 class="text-gray-900 retro"> <b>@lang('messages.paypal_payment')</b></h4>
                            <p class="retro text-gray">@lang('messages.coming_payment')</p>
                        </div>
                    </div>
                    <!----->
                </div>
            </div>

        </div>

    </div>
    <br>
@endsection

@section('content-script-include')
    <script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>

@endsection
@section('content-script')

@endsection
