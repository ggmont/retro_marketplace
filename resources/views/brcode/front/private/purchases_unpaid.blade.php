@extends('brcode.front.private.purchases')

@section('content-css-include')

<link rel="stylesheet" href="{{ asset('brcode/css/dataTables.bootstrap4.min.css') }}">

@endsection

@section('sale-unpaid', 'active')

@section('tab-content')
	<div role="tabpanel" class="tab-pane fade show active" id="tabs-1-1">
		@if($viewData['unpaid']->count() > 0)
		<div class="table-responsive">
			<table class="nk-table">
				<thead>
					<tr>
						<th style="width:25%">Transaction ID</th>
						<th style="width:25%">Seller</th>
						<th style="width:25%">Purchase Date </th>
						<th style="width:25%">Details </th>
					</tr>
				</thead>
				<tbody>
					@foreach($viewData['unpaid'] as $key)
						<tr>
							<td>{{ $key->order_identification }}</td>
							<td>{{ $key->seller->user_name }}</td>
							<td>{{ date('d-m-Y', strtotime($key->created_on)) }}</td>
							<td><a href="{{ url('/account/purchases/view/'.$key->order_identification) }}" target="_blank" class="nk-btn nk-btn-xs nk-btn-rounded nk-btn-color-white"> <i class="fa fa-info-circle"></i> View</a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		@else
			<div class="nk-gap-2"></div>
			<div class="nk-gap-2"></div>
			<h4 class="text-center">Records not found in this section</h4>
		@endif
	</div>
@endsection
					