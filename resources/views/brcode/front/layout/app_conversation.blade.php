@include('brcode.front.layout.app_header_chat')

<!-- Content Wrapper. Contains page content -->
@if ((new \Jenssegers\Agent\Agent())->isMobile())
<div>
@else
<div style="min-height:500px">
@endif

@yield('content')
</div><!-- /.content-wrapper -->
@include('brcode.front.layout.app_footer')