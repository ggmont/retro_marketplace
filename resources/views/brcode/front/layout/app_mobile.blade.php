<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover, shrink-to-fit=no">
    <meta name="description" content="Suha - Multipurpose Ecommerce Mobile HTML Template">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#100DD1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- The above tags *must* come first in the head, any other head content must come *after* these tags-->
    <!-- Title-->
    <title>Suha - Multipurpose Ecommerce Mobile HTML Template</title>
    <!-- Favicon-->
    <link rel="icon" href="{{ asset('img/RGM.png') }}">
    <!-- Apple Touch Icon-->
    <link rel="apple-touch-icon" href="img/icons/icon-96x96.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/icons/icon-152x152.png">
    <link rel="apple-touch-icon" sizes="167x167" href="img/icons/icon-167x167.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/icons/icon-180x180.png">
    <!-- Stylesheet-->
    <style>
        .sicker {
            position: absolute;
            top: 25px;
            right: 5px;
            width: 137px;
            height: 20px;
            background: #00000094;
            color: #fff;
            line-height: 20px;
            border-radius: 2px;
            text-align: center;
            font-size: 12px;
            z-index: 991;
        }

        /*
    Rollover Image
   */
        .figure {
            position: relative;
            width: 360px;
            /* can be omitted for a regular non-lazy image */
            max-width: 100%;
        }

        .figure img.image-hover {
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            object-fit: contain;
            opacity: 0;
            transition: opacity .2s;
        }

        .figure:hover img.image-hover {
            opacity: 1;
        }

        .modal-header button.close {
            position: absolute;
            right: 25px;
            opacity: 1;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
            width: 30px;
            height: 30px;
            border: 1px solid #808080;
            border-radius: 5px;
            font-size: 16px;
            line-height: 25px;
            -webkit-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            z-index: 99;
            color: #808080;
            padding: 0;
        }

        .Buttons__login-btn {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            position: relative;
            border: none;
            text-align: center;
            line-height: 34px;
            white-space: nowrap;
            border-radius: 21px;
            font-size: 1rem;
            color: #fff;
            width: 100%;
            height: 42px;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            padding: 0;
            cursor: pointer;
        }

        .Buttons__login-btn--google {
            background: #dd4b39;
        }

        .prueba {
            position: relative;
            width: 100%;
            min-height: 100vh;
            z-index: 10;
            overflow-y: auto;
            padding-top: 1rem;
            padding-bottom: 1rem;
            overflow-x: hidden;
        }

        .w-full-ultra {
            width: 115%
        }


        .btn-ultra {
            /* height: 40px; */
            padding: 3px 18px;
            font-size: 13px;
            line-height: 1.2em;
            font-weight: 500;
            box-shadow: none !important;
            display: inline-flex;
            align-items: center;
            justify-content: center;
            transition: none;
            text-decoration: none !important;
            border-radius: 6px;
            border-width: 2px;
        }

        .form-control-ultra {
            display: block;
            width: 120%;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: 0.25rem;
            -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }

    </style>

    <!-- Dropzone css -->
    <link href="/assets/dropzone-master/dist/min/dropzone.min.css" rel="stylesheet" type="text/css" />
    <style>
        .dropzone .dz-preview .dz-error-message {
            top: 175px !important;
        }

        .searchbox .form-control-chat {
            height: 36px;
            border-radius: 6px;
            border: 1px solid #E1E1E1 !important;
            padding: 0 16px 0 36px;
            font-size: 15px;
            box-shadow: none !important;
            color: #141515;
        }

        .searchbox .form-control-chat:focus {
            border-color: #c8c8c8 !important;
        }

        .searchbox .form-control-chat:focus~.input-icon {
            color: #141515;
        }

        .form-control-chat {
            background-clip: padding-box;
            background-image: linear-gradient(transparent, transparent);
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }

        .form-group.basic .form-control-chat,
        .form-group.basic .custom-select {
            background: transparent;
            border: none;
            border-bottom: 1px solid #E1E1E1;
            padding: 0 30px 0 0;
            border-radius: 0;
            height: 40px;
            color: #141515;
            font-size: 15px;
        }

        .form-group.basic .form-control-chat:focus,
        .form-group.basic .custom-select:focus {
            border-bottom-color: #1E74FD;
            box-shadow: inset 0 -1px 0 0 #1E74FD;
        }

        .form-group.basic textarea.form-control-chat {
            height: auto;
            padding: 7px 40px 7px 0;
        }

        .form-group.boxed .form-control-chat.form-select,
        .form-group.basic .form-control-chat.form-select {
            background-image: url("data:image/svg+xml,%0A%3Csvg width='13px' height='8px' viewBox='0 0 13 8' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'%3E%3Cg id='Page-1' stroke='none' stroke-width='1' fill='none' fill-rule='evenodd' stroke-linecap='round' stroke-linejoin='round'%3E%3Cpolyline id='Path' stroke='%23A9ABAD' stroke-width='2' points='1.59326172 1.79663086 6.59326172 6.79663086 11.5932617 1.79663086'%3E%3C/polyline%3E%3C/g%3E%3C/svg%3E") !important;
            background-repeat: no-repeat !important;
            background-position: right center !important;
        }

        .form-group.boxed .form-control-chat.form-select {
            background-position: right 12px center !important;
        }

        .chatFooter .form-group .form-control-chat {
            font-size: 13px;
            border-radius: 300px;
            height: 40px;
        }

        .searchbox .form-control-chat {
            height: 36px;
            border-radius: 6px;
            border: 1px solid #E1E1E1 !important;
            padding: 0 16px 0 36px;
            font-size: 15px;
            box-shadow: none !important;
            color: #141515;
        }

        .searchbox .form-control-chat:focus {
            border-color: #c8c8c8 !important;
        }

        .searchbox .form-control-chat:focus~.input-icon {
            color: #141515;
        }

        #search .searchbox .form-control-chat {
            box-shadow: none !important;
            border: 0 !important;
            border-radius: 0;
            height: 56px;
            padding: 0 56px 0 56px;
            background: transparent;
            font-size: 17px;
            color: #141515;
            width: 100%;
        }

        #search .searchbox .form-control-chat:focus {
            border-color: #bbbbbb;
        }

        #search .searchbox .form-control-chat:focus~.input-icon {
            color: #141515;
        }

        .form-control-chat.is-valid,
        .was-validated .form-control-chat:valid {
            border-color: #34C759;
            box-shadow: 0 !important;
            background-image: none !important;
        }

        .form-control-chat.is-valid:focus,
        .was-validated .form-control-chat:valid:focus {
            border-color: #34C759;
            box-shadow: none !important;
        }

        .form-control-chat.is-invalid,
        .was-validated .form-control-chat:invalid {
            border-color: #EC4433;
            background-image: none !important;
        }

        .form-control-chat.is-invalid:focus,
        .was-validated .form-control-chat:invalid:focus {
            border-color: #EC4433;
            box-shadow: none !important;
        }

        .form-control-chat {
            display: block;
            width: 90%;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: 0.25rem;
            -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }

        img {
            max-width: 100%;
            height: auto;
        }

        .card-product-image-holder .card-product-image {
            object-fit: cover;
            height: 100%;
            width: 100%;
            -webkit-border-radius: 6px;
            border-radius: 6px;
        }

        .card-product-image-holder {
            width: 100%;
            max-width: 400px;
            height: auto;
            overflow: hidden;
            position: relative;
        }

        .element {
            display: inline-flex;
            align-items: center;
        }

        svg.bi bi-camera {
            margin: 10px;
            cursor: pointer;
            font-size: 30px;
        }

        svg:hover {
            opacity: 0.6;
        }

        input {
            display: none;
        }

    </style>
    <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/core.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nes/nes_prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/mobilekit.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/splide.css') }}">

            <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/select2/select2.min.css') }}">

                <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <meta name="google-site-verification" content="uOvlTBBjltn6jz3EfQV11zsAEEHusnIZ9h-Buylgk6s" />
    <!-- Web App Manifest-->
</head>

<body>
    <!-- Preloader-->
    <div class="preloader" id="preloader">
        <div class="spinner-grow text-secondary" role="status">
            <div class="sr-only">Loading...</div>
        </div>
    </div>
    <!-- Header Area-->
    <div class="header-area" id="headerArea">
        <div class="container h-100 d-flex align-items-center justify-content-between">
            <!-- Logo Wrapper-->
            <div class="logo-wrapper"><a href="#"><img src="img/core-img/logo-small.png" alt=""></a></div>
            <!-- Search Form-->
            <div class="top-search-form">
                <form action="" method="">
                    <input class="form-control" type="search" placeholder="Enter your keyword">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <!-- Navbar Toggler-->
            <div class="suha-navbar-toggler d-flex flex-wrap" id="suhaNavbarToggler">
                <span></span><span></span><span></span>
            </div>
        </div>
    </div>
    <!-- Sidenav Black Overlay-->
    <div class="sidenav-black-overlay"></div>
    <!-- Side Nav Wrapper-->
    <div class="suha-sidenav-wrapper" id="sidenavWrapper">
        <!-- Sidenav Profile-->
        <div class="sidenav-profile">
            <div class="user-profile"><img src="img/bg-img/9.jpg" alt=""></div>
            <div class="user-info">
                <h6 class="user-name mb-0">Suha Jannat</h6>
                <p class="available-balance">Balance <span>$<span class="counter">523.98</span></span></p>
            </div>
        </div>
        <!-- Sidenav Nav-->
        <ul class="sidenav-nav pl-0">
            <li><a href="profile.html"><i class="lni lni-user"></i>My Profile</a></li>
            <li><a href="notifications.html"><i class="lni lni-alarm lni-tada-effect"></i>Notifications<span
                        class="ml-3 badge badge-warning">3</span></a></li>
            <li class="suha-dropdown-menu"><a href="#"><i class="lni lni-cart"></i>Shop Pages</a>
                <ul>
                    <li><a href="shop-grid.html">- Shop Grid</a></li>
                    <li><a href="shop-list.html">- Shop List</a></li>
                    <li><a href="single-product.html">- Product Details</a></li>
                    <li><a href="featured-products.html">- Featured Products</a></li>
                    <li><a href="flash-sale.html">- Flash Sale</a></li>
                </ul>
            </li>
            <li><a href="pages.html"><i class="lni lni-empty-file"></i>All Pages</a></li>
            <li class="suha-dropdown-menu"><a href="wishlist-grid.html"><i class="lni lni-heart"></i>My Wishlist</a>
                <ul>
                    <li><a href="wishlist-grid.html">- Wishlist Grid</a></li>
                    <li><a href="wishlist-list.html">- Wishlist List</a></li>
                </ul>
            </li>
            <li><a href="settings.html"><i class="lni lni-cog"></i>Settings</a></li>
            <li><a href="intro.html"><i class="lni lni-power-switch"></i>Sign Out</a></li>
        </ul>
        <!-- Go Back Button-->
        <div class="go-home-btn" id="goHomeBtn"><i class="lni lni-arrow-left"></i></div>
    </div>
    <!-- PWA Install Alert-->
    <div class="toast pwa-install-alert shadow" id="pwaInstallToast" role="alert" aria-live="assertive"
        aria-atomic="true" data-delay="8000" data-autohide="true">
        <div class="toast-body">
            <button class="ml-3 close" type="button" data-dismiss="toast" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <div class="content d-flex align-items-center mb-2"><img src="img/icons/icon-72x72.png" alt="">
                <h6 class="mb-0 text-white">Add to Home Screen</h6>
            </div><span class="mb-0 d-block text-white">Add Suha on your mobile home screen. Click the<strong
                    class="mx-1">"Add to Home Screen"</strong>button & enjoy it like a regular app.</span>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!-- Hero Slides-->
        <div class="hero-slides owl-carousel">
            <!-- Single Hero Slide-->
            <div class="single-hero-slide" style="background-image: url('img/bg-img/1.jpg')">
                <div class="slide-content h-100 d-flex align-items-center">
                    <div class="container">
                        <h4 class="text-white mb-0" data-animation="fadeInUp" data-delay="100ms"
                            data-wow-duration="1000ms">Amazon Echo</h4>
                        <p class="text-white" data-animation="fadeInUp" data-delay="400ms"
                            data-wow-duration="1000ms">3rd Generation, Charcoal</p><a class="btn btn-primary btn-sm"
                            href="#" data-animation="fadeInUp" data-delay="800ms" data-wow-duration="1000ms">Buy Now</a>
                    </div>
                </div>
            </div>
            <!-- Single Hero Slide-->
            <div class="single-hero-slide" style="background-image: url('img/bg-img/2.jpg')">
                <div class="slide-content h-100 d-flex align-items-center">
                    <div class="container">
                        <h4 class="text-white mb-0" data-animation="fadeInUp" data-delay="100ms"
                            data-wow-duration="1000ms">Light Candle</h4>
                        <p class="text-white" data-animation="fadeInUp" data-delay="400ms"
                            data-wow-duration="1000ms">Now only $22</p><a class="btn btn-success btn-sm" href="#"
                            data-animation="fadeInUp" data-delay="500ms" data-wow-duration="1000ms">Buy Now</a>
                    </div>
                </div>
            </div>
            <!-- Single Hero Slide-->
            <div class="single-hero-slide" style="background-image: url('img/bg-img/3.jpg')">
                <div class="slide-content h-100 d-flex align-items-center">
                    <div class="container">
                        <h4 class="text-white mb-0" data-animation="fadeInUp" data-delay="100ms"
                            data-wow-duration="1000ms">Best Furniture</h4>
                        <p class="text-white" data-animation="fadeInUp" data-delay="400ms"
                            data-wow-duration="1000ms">3 years warranty</p><a class="btn btn-danger btn-sm" href="#"
                            data-animation="fadeInUp" data-delay="800ms" data-wow-duration="1000ms">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Product Catagories-->
        <div class="product-catagories-wrapper py-3">
            <div class="container">
                <div class="section-heading">
                    <h6 class="ml-1">Product Category</h6>
                </div>
                <div class="product-catagory-wrap">
                    <div class="row g-3">
                        <!-- Single Catagory Card-->
                        <div class="col-4">
                            <div class="card catagory-card">
                                <div class="card-body"><a href="catagory.html"><i
                                            class="lni lni-heart"></i><span>Women's</span></a></div>
                            </div>
                        </div>
                        <!-- Single Catagory Card-->
                        <div class="col-4">
                            <div class="card catagory-card">
                                <div class="card-body"><a href="catagory.html"><i
                                            class="lni lni-juice"></i><span>Juice</span></a></div>
                            </div>
                        </div>
                        <!-- Single Catagory Card-->
                        <div class="col-4">
                            <div class="card catagory-card">
                                <div class="card-body"><a href="catagory.html"><i
                                            class="lni lni-pizza"></i><span>Foods</span></a></div>
                            </div>
                        </div>
                        <!-- Single Catagory Card-->
                        <div class="col-4">
                            <div class="card catagory-card">
                                <div class="card-body"><a href="catagory.html"><i
                                            class="lni lni-basketball"></i><span>Sports</span></a></div>
                            </div>
                        </div>
                        <!-- Single Catagory Card-->
                        <div class="col-4">
                            <div class="card catagory-card">
                                <div class="card-body"><a href="catagory.html"><i
                                            class="lni lni-tshirt"></i><span>Men's</span></a></div>
                            </div>
                        </div>
                        <!-- Single Catagory Card-->
                        <div class="col-4">
                            <div class="card catagory-card">
                                <div class="card-body"><a href="catagory.html"><i
                                            class="lni lni-island"></i><span>Travel</span></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Flash Sale Slide-->
        <div class="flash-sale-wrapper">
            <div class="container">
                <div class="section-heading d-flex align-items-center justify-content-between">
                    <h6 class="ml-1">Flash Sale</h6><a class="btn btn-primary btn-sm"
                        href="flash-sale.html">View All</a>
                </div>
                <!-- Flash Sale Slide-->
                <div class="flash-sale-slide owl-carousel">
                    <!-- Single Flash Sale Card-->
                    <div class="card flash-sale-card">
                        <div class="card-body"><a href="single-product.html"><img src="img/product/1.png"
                                    alt=""><span class="product-title">Black Table Lamp</span>
                                <p class="sale-price">$7.99<span class="real-price">$15</span></p><span
                                    class="progress-title">33% Sold Out</span>
                                <!-- Progress Bar-->
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 33%"
                                        aria-valuenow="33" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </a></div>
                    </div>
                    <!-- Single Flash Sale Card-->
                    <div class="card flash-sale-card">
                        <div class="card-body"><a href="single-product.html"><img src="img/product/2.png"
                                    alt=""><span class="product-title">Modern Sofa</span>
                                <p class="sale-price">$14<span class="real-price">$21</span></p><span
                                    class="progress-title">57% Sold Out</span>
                                <!-- Progress Bar-->
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 57%"
                                        aria-valuenow="57" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </a></div>
                    </div>
                    <!-- Single Flash Sale Card-->
                    <div class="card flash-sale-card">
                        <div class="card-body"><a href="single-product.html"><img src="img/product/3.png"
                                    alt=""><span class="product-title">Classic Garden Chair</span>
                                <p class="sale-price">$36<span class="real-price">$49</span></p><span
                                    class="progress-title">99% Sold Out</span>
                                <!-- Progress Bar-->
                                <div class="progress">
                                    <div class="progress-bar bg-danger" role="progressbar" style="width: 100%"
                                        aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </a></div>
                    </div>
                    <!-- Single Flash Sale Card-->
                    <div class="card flash-sale-card">
                        <div class="card-body"><a href="single-product.html"><img src="img/product/1.png"
                                    alt=""><span class="product-title">Black Table Lamp</span>
                                <p class="sale-price">$7.99<span class="real-price">$15</span></p><span
                                    class="progress-title">33% Sold Out</span>
                                <!-- Progress Bar-->
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 33%"
                                        aria-valuenow="33" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </a></div>
                    </div>
                    <!-- Single Flash Sale Card-->
                    <div class="card flash-sale-card">
                        <div class="card-body"><a href="single-product.html"><img src="img/product/2.png"
                                    alt=""><span class="product-title">Modern Sofa</span>
                                <p class="sale-price">$14<span class="real-price">$21</span></p><span
                                    class="progress-title">57% Sold Out</span>
                                <!-- Progress Bar-->
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 57%"
                                        aria-valuenow="57" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </a></div>
                    </div>
                    <!-- Single Flash Sale Card-->
                    <div class="card flash-sale-card">
                        <div class="card-body"><a href="single-product.html"><img src="img/product/3.png"
                                    alt=""><span class="product-title">Classic Garden Chair</span>
                                <p class="sale-price">$36<span class="real-price">$49</span></p><span
                                    class="progress-title">99% Sold Out</span>
                                <!-- Progress Bar-->
                                <div class="progress">
                                    <div class="progress-bar bg-danger" role="progressbar" style="width: 100%"
                                        aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </a></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Top Products-->
        @livewire('home-product')
        <!-- Cool Facts Area-->
        <div class="cta-area">
            <div class="container">
                <div class="cta-text p-4 p-lg-5" style="background-image: url(img/bg-img/24.jpg)">
                    <h4>Winter Sale 50% Off</h4>
                    <p>Suha is a multipurpose, creative &amp; <br>modern mobile template.</p><a class="btn btn-danger"
                        href="#">Shop Today</a>
                </div>
            </div>
        </div>
        <!-- Weekly Best Sellers-->
        <div class="weekly-best-seller-area py-3">
            <div class="container">
                <div class="section-heading d-flex align-items-center justify-content-between">
                    <h6 class="pl-1">Weekly Best Sellers</h6><a class="btn btn-success btn-sm"
                        href="shop-list.html">View All</a>
                </div>
                <div class="row g-3">
                    <!-- Single Weekly Product Card-->
                    <div class="col-12 col-md-6">
                        <div class="card weekly-product-card">
                            <div class="card-body d-flex align-items-center">
                                <div class="product-thumbnail-side"><span class="badge badge-success">Sale</span><a
                                        class="wishlist-btn" href="#"><i class="lni lni-heart"></i></a><a
                                        class="product-thumbnail d-block" href="single-product.html"><img
                                            src="img/product/10.png" alt=""></a></div>
                                <div class="product-description"><a class="product-title d-block"
                                        href="single-product.html">Modern Red Sofa</a>
                                    <p class="sale-price"><i class="lni lni-dollar"></i>$64<span>$89</span></p>
                                    <div class="product-rating"><i class="lni lni-star-filled"></i>4.88 (39)</div><a
                                        class="btn btn-success btn-sm add2cart-notify" href="#"><i
                                            class="mr-1 lni lni-cart"></i>Buy Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Weekly Product Card-->
                    <div class="col-12 col-md-6">
                        <div class="card weekly-product-card">
                            <div class="card-body d-flex align-items-center">
                                <div class="product-thumbnail-side"><span class="badge badge-primary">Sale</span><a
                                        class="wishlist-btn" href="#"><i class="lni lni-heart"></i></a><a
                                        class="product-thumbnail d-block" href="single-product.html"><img
                                            src="img/product/7.png" alt=""></a></div>
                                <div class="product-description"><a class="product-title d-block"
                                        href="single-product.html">Office Chair</a>
                                    <p class="sale-price"><i class="lni lni-dollar"></i>$100<span>$160</span></p>
                                    <div class="product-rating"><i class="lni lni-star-filled"></i>4.82 (125)</div><a
                                        class="btn btn-success btn-sm add2cart-notify" href="#"><i
                                            class="mr-1 lni lni-cart"></i>Buy Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Weekly Product Card-->
                    <div class="col-12 col-md-6">
                        <div class="card weekly-product-card">
                            <div class="card-body d-flex align-items-center">
                                <div class="product-thumbnail-side"><span class="badge badge-danger">-10%</span><a
                                        class="wishlist-btn" href="#"><i class="lni lni-heart"></i></a><a
                                        class="product-thumbnail d-block" href="single-product.html"><img
                                            src="img/product/12.png" alt=""></a></div>
                                <div class="product-description"><a class="product-title d-block"
                                        href="single-product.html">Sun Glasses</a>
                                    <p class="sale-price"><i class="lni lni-dollar"></i>$24<span>$32</span></p>
                                    <div class="product-rating"><i class="lni lni-star-filled"></i>4.79 (63)</div><a
                                        class="btn btn-success btn-sm add2cart-notify" href="#"><i
                                            class="mr-1 lni lni-cart"></i>Buy Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Weekly Product Card-->
                    <div class="col-12 col-md-6">
                        <div class="card weekly-product-card">
                            <div class="card-body d-flex align-items-center">
                                <div class="product-thumbnail-side"><span class="badge badge-warning">New</span><a
                                        class="wishlist-btn" href="#"><i class="lni lni-heart"></i></a><a
                                        class="product-thumbnail d-block" href="single-product.html"><img
                                            src="img/product/13.png" alt=""></a></div>
                                <div class="product-description"><a class="product-title d-block"
                                        href="single-product.html">Wall Clock</a>
                                    <p class="sale-price"><i class="lni lni-dollar"></i>$31<span>$47</span></p>
                                    <div class="product-rating"><i class="lni lni-star-filled"></i>4.99 (7)</div><a
                                        class="btn btn-success btn-sm add2cart-notify" href="#"><i
                                            class="mr-1 lni lni-cart"></i>Buy Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Discount Coupon Card-->
        <div class="container">
            <div class="card discount-coupon-card border-0">
                <div class="card-body">
                    <div class="coupon-text-wrap d-flex align-items-center p-3">
                        <h5 class="text-white pr-3 mb-0">Get 20% <br> discount</h5>
                        <p class="text-white pl-3 mb-0">To get discount, enter the<strong
                                class="px-1">GET20</strong>code on the checkout page.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Featured Products Wrapper-->
        <div class="featured-products-wrapper py-3">
            <div class="container">
                <div class="section-heading d-flex align-items-center justify-content-between">
                    <h6 class="pl-1">Featured Products</h6><a class="btn btn-warning btn-sm"
                        href="featured-products.html">View All</a>
                </div>
                <div class="row g-3">
                    <!-- Featured Product Card-->
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="card featured-product-card">
                            <div class="card-body"><span class="badge badge-warning custom-badge"><i
                                        class="lni lni-star"></i></span>
                                <div class="product-thumbnail-side"><a class="wishlist-btn" href="#"><i
                                            class="lni lni-heart"></i></a><a class="product-thumbnail d-block"
                                        href="single-product.html"><img src="img/product/14.png" alt=""></a></div>
                                <div class="product-description"><a class="product-title d-block"
                                        href="single-product.html">Blue Skateboard</a>
                                    <p class="sale-price">$64<span>$89</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Featured Product Card-->
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="card featured-product-card">
                            <div class="card-body"><span class="badge badge-warning custom-badge"><i
                                        class="lni lni-star"></i></span>
                                <div class="product-thumbnail-side"><a class="wishlist-btn" href="#"><i
                                            class="lni lni-heart"></i></a><a class="product-thumbnail d-block"
                                        href="single-product.html"><img src="img/product/15.png" alt=""></a></div>
                                <div class="product-description"><a class="product-title d-block"
                                        href="single-product.html">Travel Bag</a>
                                    <p class="sale-price">$64<span>$89</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Featured Product Card-->
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="card featured-product-card">
                            <div class="card-body"><span class="badge badge-warning custom-badge"><i
                                        class="lni lni-star"></i></span>
                                <div class="product-thumbnail-side"><a class="wishlist-btn" href="#"><i
                                            class="lni lni-heart"></i></a><a class="product-thumbnail d-block"
                                        href="single-product.html"><img src="img/product/16.png" alt=""></a></div>
                                <div class="product-description"><a class="product-title d-block"
                                        href="single-product.html">Cotton T-shirts</a>
                                    <p class="sale-price">$64<span>$89</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Featured Product Card-->
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="card featured-product-card">
                            <div class="card-body"><span class="badge badge-warning custom-badge"><i
                                        class="lni lni-star"></i></span>
                                <div class="product-thumbnail-side"><a class="wishlist-btn" href="#"><i
                                            class="lni lni-heart"></i></a><a class="product-thumbnail d-block"
                                        href="single-product.html"><img src="img/product/6.png" alt=""></a></div>
                                <div class="product-description"><a class="product-title d-block"
                                        href="single-product.html">Roof Lamp </a>
                                    <p class="sale-price">$64<span>$89</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Night Mode View Card-->
        <div class="night-mode-view-card pb-3">
            <div class="container">
                <div class="card settings-card">
                    <div class="card-body">
                        <div class="single-settings d-flex align-items-center justify-content-between">
                            <div class="title"><i class="lni lni-night"></i><span>Night Mode</span></div>
                            <div class="data-content">
                                <div class="toggle-button-cover">
                                    <div class="button r">
                                        <input class="checkbox" id="darkSwitch" type="checkbox">
                                        <div class="knobs"></div>
                                        <div class="layer"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Internet Connection Status-->
    <div class="internet-connection-status" id="internetStatus"></div>
    <!-- Footer Nav-->
    <div class="footer-nav-area" id="footerNav">
        <div class="container h-100 px-0">
            <div class="suha-footer-nav h-100">
                <ul class="h-100 d-flex align-items-center justify-content-between pl-0">
                    <li class="active"><a href="#"><i class="lni lni-home"></i>Home</a></li>
                    <li><a href="message.html"><i class="lni lni-life-ring"></i>Support</a></li>
                    <li><a href="cart.html"><i class="lni lni-shopping-basket"></i>Cart</a></li>
                    <li><a href="pages.html"><i class="lni lni-heart"></i>Pages</a></li>
                    <li><a href="settings.html"><i class="lni lni-cog"></i>Settings</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- All JavaScript Files-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/mobile/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/mobile/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('js/mobile/default/jquery.passwordstrength.js') }}"></script>
    <script src="{{ asset('js/mobile/wow.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jarallax.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jarallax-video.min.js') }}"></script>
    <script src="{{ asset('js/mobile/default/dark-mode-switch.js') }}"></script>
    <script src="{{ asset('js/mobile/default/active.js') }}"></script>
    <script src="{{ asset('js/mobile/pwa.js') }}"></script>
</body>

</html>
