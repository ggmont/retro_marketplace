<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> {{ config('brcode.app_name_public') }}</title>

    <meta name="description" content="Retro Gaming Market - El marketplace especialista en videojuegos">
    <meta name="keywords" content="game, retrogaming, gaming, Retro Gaming, Market, Retro">
    <meta name="author" content="RetroGamingMarket">
    <link rel="apple-touch-icon" href="{{ asset('img/RGM.png') }}">
    <link rel="manifest" href="{{ asset('/manifest.json') }}">
    <link rel="icon" type="image/png" href="{{ asset('img/RGM.png') }}">

    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-9983858812639448"
        crossorigin="anonymous"></script>

    @livewireStyles


    <style>
        .retro {
            font-family: 'Jost', sans-serif;
        }

        .retro-prueba {
            font-family: 'Jost', sans-serif;
        }

        .retro-all {
            font-family: 'Jost', sans-serif;
            font-size: 17px;
            font-weight: 700;
        }

        .retro-sell {
            font-family: 'Jost', sans-serif;
            font-weight: 800;
        }

        .menu-retro {
            font-family: 'Rubik', sans-serif;
        }
    </style>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Version Mobile -->
    <link href="https://cdn.lineicons.com/3.0/lineicons.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css?v=2') }}">
    <link rel="stylesheet" href="{{ asset('css/flag-icon.css') }}">

    <!-- Fancybox CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">

    <style>
        .sicker {
            position: absolute;
            left: 50%;
            top: 10%;
            width: 137px;
            transform: translate(-50%, -50%);
            background: #00000094;
            color: #fff;
            line-height: 20px;
            border-radius: 2px;
            text-align: center;
            font-size: 12px;
            z-index: 991;
        }

        /*
              Rollover Image
             */
        .figure {
            position: relative;
            width: 360px;
            /* can be omitted for a regular non-lazy image */
            max-width: 100%;
        }

        .figure img.image-hover {
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            object-fit: contain;
            opacity: 0;
            transition: opacity .2s;
        }

        .figure:hover img.image-hover {
            opacity: 1;
        }

        .modal-header button.close {
            position: absolute;
            right: 25px;
            opacity: 1;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
            width: 30px;
            height: 30px;
            border: 1px solid #808080;
            border-radius: 5px;
            font-size: 16px;
            line-height: 25px;
            -webkit-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            z-index: 99;
            color: #808080;
            padding: 0;
        }

        .Buttons__login-btn {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            position: relative;
            border: none;
            text-align: center;
            line-height: 34px;
            white-space: nowrap;
            border-radius: 21px;
            font-size: 1rem;
            color: #fff;
            width: 100%;
            height: 42px;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            padding: 0;
            cursor: pointer;
        }

        .Buttons__login-btn--google {
            background: #dd4b39;
        }

        .prueba {
            position: relative;
            width: 100%;
            min-height: 100vh;
            z-index: 10;
            overflow-y: auto;
            padding-top: 1rem;
            padding-bottom: 1rem;
            overflow-x: hidden;
        }

        .w-full-ultra {
            width: 92%
        }


        .btn-ultra {
            /* height: 40px; */
            padding: 3px 18px;
            font-size: 13px;
            line-height: 1.2em;
            font-weight: 500;
            box-shadow: none !important;
            display: inline-flex;
            align-items: center;
            justify-content: center;
            transition: none;
            text-decoration: none !important;
            border-radius: 6px;
            border-width: 2px;
        }

        .form-control-ultra {
            display: block;
            width: 120%;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: 0.25rem;
            -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
    </style>

    <!-- Dropzone css -->
    <link href="/assets/dropzone-master/dist/min/dropzone.min.css" rel="stylesheet" type="text/css" />
    <style>
        .dropzone .dz-preview .dz-error-message {
            top: 175px !important;
        }

        .searchbox .form-control-chat {
            height: 36px;
            border-radius: 6px;
            border: 1px solid #E1E1E1 !important;
            padding: 0 16px 0 36px;
            font-size: 15px;
            box-shadow: none !important;
            color: #141515;
        }

        .searchbox .form-control-chat:focus {
            border-color: #c8c8c8 !important;
        }

        .searchbox .form-control-chat:focus~.input-icon {
            color: #141515;
        }

        .form-control-chat {
            background-clip: padding-box;
            background-image: linear-gradient(transparent, transparent);
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }

        .form-group.basic .form-control-chat,
        .form-group.basic .custom-select {
            background: transparent;
            border: none;
            border-bottom: 1px solid #E1E1E1;
            padding: 0 30px 0 0;
            border-radius: 0;
            height: 40px;
            color: #141515;
            font-size: 15px;
        }

        .form-group.basic .form-control-chat:focus,
        .form-group.basic .custom-select:focus {
            border-bottom-color: #1E74FD;
            box-shadow: inset 0 -1px 0 0 #1E74FD;
        }

        .form-group.basic textarea.form-control-chat {
            height: auto;
            padding: 7px 40px 7px 0;
        }

        .form-group.boxed .form-control-chat.form-select,
        .form-group.basic .form-control-chat.form-select {
            background-image: url("data:image/svg+xml,%0A%3Csvg width='13px' height='8px' viewBox='0 0 13 8' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'%3E%3Cg id='Page-1' stroke='none' stroke-width='1' fill='none' fill-rule='evenodd' stroke-linecap='round' stroke-linejoin='round'%3E%3Cpolyline id='Path' stroke='%23A9ABAD' stroke-width='2' points='1.59326172 1.79663086 6.59326172 6.79663086 11.5932617 1.79663086'%3E%3C/polyline%3E%3C/g%3E%3C/svg%3E") !important;
            background-repeat: no-repeat !important;
            background-position: right center !important;
        }

        .form-group.boxed .form-control-chat.form-select {
            background-position: right 12px center !important;
        }

        .chatFooter .form-group .form-control-chat {
            font-size: 13px;
            border-radius: 300px;
            height: 40px;
        }

        .searchbox .form-control-chat {
            height: 36px;
            border-radius: 6px;
            border: 1px solid #E1E1E1 !important;
            padding: 0 16px 0 36px;
            font-size: 15px;
            box-shadow: none !important;
            color: #141515;
        }

        .searchbox .form-control-chat:focus {
            border-color: #c8c8c8 !important;
        }

        .searchbox .form-control-chat:focus~.input-icon {
            color: #141515;
        }

        #search .searchbox .form-control-chat {
            box-shadow: none !important;
            border: 0 !important;
            border-radius: 0;
            height: 56px;
            padding: 0 56px 0 56px;
            background: transparent;
            font-size: 17px;
            color: #141515;
            width: 100%;
        }

        #search .searchbox .form-control-chat:focus {
            border-color: #bbbbbb;
        }

        #search .searchbox .form-control-chat:focus~.input-icon {
            color: #141515;
        }

        .form-control-chat.is-valid,
        .was-validated .form-control-chat:valid {
            border-color: #34C759;
            box-shadow: 0 !important;
            background-image: none !important;
        }

        .form-control-chat.is-valid:focus,
        .was-validated .form-control-chat:valid:focus {
            border-color: #34C759;
            box-shadow: none !important;
        }

        .form-control-chat.is-invalid,
        .was-validated .form-control-chat:invalid {
            border-color: #EC4433;
            background-image: none !important;
        }

        .form-control-chat.is-invalid:focus,
        .was-validated .form-control-chat:invalid:focus {
            border-color: #EC4433;
            box-shadow: none !important;
        }

        .form-control-chat {
            display: block;
            width: 90%;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: 0.25rem;
            -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }

        img {
            max-width: 100%;
            height: auto;
        }

        .card-product-image-holder .card-product-image {
            object-fit: cover;
            height: 100%;
            width: 100%;
            -webkit-border-radius: 6px;
            border-radius: 6px;
        }

        .card-product-image-holder {
            object-fit: cover;
            width: 100%;
            max-width: 400px;
            height: 250px;
            border-radius: 5px;
            overflow: hidden;
            position: relative;
            margin-bottom: 1px;
        }

        .element {
            display: inline-flex;
            align-items: center;
        }

        svg.bi bi-camera {
            margin: 10px;
            cursor: pointer;
            font-size: 30px;
        }

        svg:hover {
            opacity: 0.6;
        }
    </style>


    @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <link rel="stylesheet" href="{{ asset('css/pc/core.css') }}">
        <link rel="stylesheet" href="{{ asset('css/pc/affan.css') }}">
        <link rel="stylesheet" href="{{ asset('css/pc/mobilekit.css?v=3') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/goodgames.css?v=3') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/default.css?v=2') }}">
        <link rel="stylesheet" href="{{ asset('css/pc/prueba.css?v=2') }}">
    @else
        <link rel="stylesheet" href="{{ asset('css/mobile/core.css?v=2') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/affan.css?v=5') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/mobilekit.css?v=4') }}">
    @endif
 
 
    <link rel="stylesheet" href="{{ asset('assets/cookie/cookiealert.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/splide.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css?v=4') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        @else
        <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css?v=3') }}">
        @endif
    <link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/select2/select2.min.css') }}">
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <meta name="google-site-verification" content="uOvlTBBjltn6jz3EfQV11zsAEEHusnIZ9h-Buylgk6s" />

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-9983858812639448"
        crossorigin="anonymous"></script>
    <style>
        .captcha img {
            width: 250px;
        }

        .ulti {
            max-width: 100%;
            height: 15px;
        }

        .ultri {
            max-width: 11%;
            height: 15px;
        }

        .reload {
            font-family: Lucida Sans Unicode
        }

        .container-detail {
            width: 700px;
        }

        @media (max-width: 681px) {
            .container-detail {
                width: 100%
            }
        }


        .footer.content>img,
        .footer.content>p {
            display: inline-block;
        }


        .profesional {
            background: radial-gradient(ellipse farthest-corner at right bottom, #FEDB37 0%, #FDB931 8%, #9f7928 30%, #8A6E2F 40%),
                radial-gradient(ellipse farthest-corner at left top, #FFFFFF 0%, #FFFFAC 8%, #D1B464 25%, #5d4a1f 62.5%, #5d4a1f 100%);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }

        .the_register {
            max-width: 500px;
            margin: auto;
            text-align: center;
        }

        .flexbox {
            display: flex;
        }

        .flexbox .stretch {
            flex: 1;
        }

        .flexbox .normal {
            flex: 0;
            margin: 0 0 0 1rem;
        }

        .flexbox div input {
            padding: .5em 1em;
            width: 100%;
        }

        .flexbox .listen {
            width: 92%;
        }

        .flexbox div button {
            padding: .5em 1em;
            white-space: nowrap;
        }
    </style>
    <!-- PWA -->



    @laravelPWA
</head>

@if ((new \Jenssegers\Agent\Agent())->isMobile())
    <div id="loader">
        <div class="spinner-border text-danger" role="status"></div>
    </div>

    <div id="notification-8" class="notification-box">
        <div class="notification-dialog android-style">
            <div class="notification-header">
                <div class="in">
                    <img src="{{ asset('img/RGM.png') }}" alt="image" class="imaged w24 rounded">
                    <strong>Notificación automática</strong>
                    <span>Justo Ahora</span>
                </div>
                <a href="#" class="close-button">
                    <ion-icon name="close"></ion-icon>
                </a>
            </div>
            <div class="notification-content">
                <div class="in">
                    <h3 class="subtitle">Prueba!</h3>
                    <div class="text">
                        Probando
                    </div>
                </div>
                <div class="icon-box text-danger">
                    <ion-icon name="close-circle-outline"></ion-icon>
                </div>
            </div>
        </div>
    </div>

    @if (Auth::user())
        <div class="offcanvas offcanvas-start" tabindex="-1" id="sidebarPanel">
            <div class="offcanvas-body">
                <!-- profile box -->
                <div class="profileBox">
                    <div class="image-wrapper">
                        @if (auth()->user()->profile_picture)
                            <img src="{{ url(auth()->user()->profile_picture) }}" alt="image"
                                class="imaged rounded">
                        @else
                            <img src="{{ asset('img/profile-picture-not-found.png') }}" alt="image"
                                class="imaged rounded">
                        @endif
                    </div>
                    <div class="in">
                        <strong>{{ auth()->user()->user_name }}</strong>
                        <div class="text-muted">
                            € {{ auth()->user()->cash }} - € {{ auth()->user()->special_cash }}
                        </div>
                    </div>
                </div>
                <!-- * profile box -->

                <ul class="listview link-listview">
                    @if (App\SysUserRoles::where('user_id', Auth::id())->whereIn('role_id', [1])->count() > 0)
                        <li>
                            <a href="{{ url('/11w5Cj9WDAjnzlg0') }}" class="item">
                                <div class="in">
                                    <div> @lang('messages.panel_control')</div>
                                </div>
                            </a>
                        </li>
                    @endif
                    <li>
                        <a href="/listing/users">
                            @lang('messages.list_user')
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('offer_index') }}">
                            @lang('messages.show_offer')
                        </a>
                    </li>
                    <li>
                        <a href="/cart">
                            @lang('messages.shopping_cart')
                            @if (Auth::user())
                                {{ Auth::user()->lifetimeCart }}

                                <span class="badge badge-danger"
                                    id="br-cart-items">{{ $viewData['items_count'] }}</span>
                            @else
                                -
                            @endif
                        </a>
                    </li>
                    <li>
                        <a href="/alerts">
                            @lang('messages.alerts')
                            <span class="badge badge-danger">{{ count($viewData['header_newsletter']) }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="/notifications">
                            @lang('messages.notification')
                            @if (count($viewData['header_notifications']) > 0)
                                @if ($viewData['c_h_n'] > 0)
                                    <span class="badge badge-danger">{{ $viewData['c_h_n'] }}</span>
                                @endif
                            @endif
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('purchases_data') }}">
                            @lang('messages.the_purchase')



                        </a>
                    </li>
                    <li>
                        <a href="{{ route('sales_data') }}">
                            @lang('messages.the_sale')

                        </a>
                    </li>
                    <li>
                        <a href="{{ route('transaction_index') }}">
                            @lang('messages.the_transaction')

                        </a>
                    </li>
                    <li>
                        <a href="{{ route('collection_index') }}">
                            Mi colección
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/account/inventory') }}">
                            @lang('messages.the_inventory')
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" onclick="salir()" class="item"
                            data-href="{{ url('/account/logout') }}">
                            <div class="in">
                                @lang('messages.get_out')
                            </div>
                        </a>
                    </li>
                </ul>

            </div>
            <!-- sidebar buttons -->
            <div class="sidebar-buttons">
                <a href="{{ route('information_index') }}" class="button">
                    <ion-icon name="help-outline"></ion-icon>
                </a>
                <a href="#" id="showlanguage" class="showlanguage button">
                    @if (App::getLocale() == 'es')
                        <span class="flag-icon flag-icon-esp"></span>
                    @elseif (App::getLocale() == 'en')
                        <span class="flag-icon flag-icon-gbr"></span>
                    @endif
                </a>

            </div>
            <!-- * sidebar buttons -->
        </div>
    @else
        <div class="offcanvas offcanvas-start" tabindex="-1" id="sidebarPanel">
            <div class="offcanvas-body">
                <!-- profile box -->
                <div class="profileBox">
                    <div class="image-wrapper">
                        <img src="{{ asset('img/profile-picture-not-found.png') }}" alt="image"
                            class="imaged rounded">
                    </div>
                    <div class="in">
                        <strong>Bienvenido a RGM!</strong>
                        <div class="text-muted">
                            RGM
                        </div>
                    </div>
                </div>
                <!-- * profile box -->

                <ul class="listview link-listview">
                    <li>
                        <a href="#" id="showLogin" class="item showLogin" data-nav-toggle="#nk-nav-mobile">
                            <div class="in">
                                <div>@lang('messages.sesion_register')</div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="/site/faqs">
                            FAQS
                        </a>
                    </li>
                    <li>
                        <a href="/site/terminos-y-condiciones">
                            @lang('messages.terms')
                        </a>
                    </li>
                    <li>
                        <a href="/site/aviso-legal">
                            @lang('messages.legal')
                        </a>
                    </li>
                    <li>
                        <a href="/site/politica-de-privacidad">
                            @lang('messages.pt_privacy')
                        </a>
                    </li>
                    <li>
                        <a href="/site/politica-de-cookies">
                            @lang('messages.pt_cookie')
                        </a>
                    </li>
                    <li>
                        <a href="/contact-us">
                            Contacto
                        </a>
                    </li>
                    <li>
                        <a href="/site/normas-y-recomendaciones">
                            Normas y recomendaciones
                        </a>
                    </li>
                </ul>

            </div>
            <!-- sidebar buttons -->
            <div class="sidebar-buttons">
                <a href="#" id="showLogin" class="button showLogin" data-nav-toggle="#nk-nav-mobile">
                    <ion-icon name="person-outline"></ion-icon>
                </a>
                <a href="{{ route('information_index') }}" class="button">
                    <ion-icon name="help-outline"></ion-icon>
                </a>
                <a href="#" id="showlanguage" class="showlanguage button">
                    @if (App::getLocale() == 'es')
                        <span class="flag-icon flag-icon-esp"></span>
                    @elseif (App::getLocale() == 'en')
                        <span class="flag-icon flag-icon-gbr"></span>
                    @endif
                </a>

            </div>
            <!-- * sidebar buttons -->
        </div>
    @endif


    <div class="modal fade" id="modalLanguage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog lang-form" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title">Selector de Idioma</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form action="/lang" class="m-3 ml-10 nk-form nk-form-style" id="nk-lang-form"
                                method="POST">
                                {{ csrf_field() }}
                                <select name="locale"
                                    class="form-control form-select p-1 pl-10 text-gray-900 pr-10 nk-lang-change"
                                    onchange="lang_change()" style="width: 100%;">
                                    <option data-img-src="{{ asset('assets/images/es/es.png') }}"
                                        value="/setlocale/es" {{ App::getLocale() == 'es' ? 'selected' : '' }}>
                                        Español
                                    </option>
                                    <option data-img-src="{{ asset('assets/images/en/en.png') }}"
                                        value="/setlocale/en" {{ App::getLocale() == 'en' ? 'selected' : '' }}>
                                        English
                                    </option>
                                    @if (App\SysUserRoles::where('user_id', Auth::id())->whereIn('role_id', [1])->count() > 0)
                                        <option data-img-src="{{ asset('assets/images/fr/fr.png') }}"
                                            value="/setlocale/fr" {{ App::getLocale() == 'fr' ? 'selected' : '' }}>
                                            Francés
                                        </option>
                                        <option data-img-src="{{ asset('assets/images/ita/ita.png') }}"
                                            value="/setlocale/ita" {{ App::getLocale() == 'ita' ? 'selected' : '' }}>
                                            Italia
                                        </option>
                                        <option data-img-src="{{ asset('assets/images/al/al.png') }}"
                                            value="/setlocale/al" {{ App::getLocale() == 'al' ? 'selected' : '' }}>
                                            Alemania
                                        </option>
                                        <option data-img-src="{{ asset('assets/images/prt/prt.png') }}"
                                            value="/setlocale/prt" {{ App::getLocale() == 'prt' ? 'selected' : '' }}>
                                            Portugués
                                        </option>
                                    @endif
                                </select>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">


                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div id="loader">
        <div class="spinner-border text-danger" role="status"></div>
    </div>

    @if (Auth::user())
        <header class="bg-white shadow-lg">
            <div class="flex justify-between items-center py-4 px-4 md:px-0">
                <a href="{{ url('/') }}" class="flex items-center">
                    <img src="{{ asset('img/RGM.png') }}" alt="RGM" class="h-8 mr-2">
                    <h1 class="text-gray-700 font-bold text-xl">RetroGamingMarket</h1>
                </a>
                <div class="flex-1"></div>

 

                <div class="flex items-center">
 

                    @if (App::getLocale() == 'es')
                        <a href="#" class="flag-icon flag-icon-esp ml-3" style="font-size: 1.5rem;"></a>
                    @elseif (App::getLocale() == 'en')
                        <a href="#" class="flag-icon flag-icon-gbr ml-3" style="font-size: 1.5rem;"></a>
                    @endif
                </div>
            </div>
        </header>
    @else
        <header class="bg-white shadow-lg">
            <div class="flex justify-between items-center py-4 px-4 md:px-0">
                <a href="{{ url('/') }}" class="flex items-center">
                    <img src="{{ asset('img/RGM.png') }}" alt="RGM" class="h-8 mr-2">
                    <h1 class="text-gray-700 font-bold text-xl">RetroGamingMarket</h1>
                </a>
                <div class="flex-1"></div>
 
                <div class="flex items-center">
 

                    @if (App::getLocale() == 'es')
                        <a href="#" class="flag-icon flag-icon-esp ml-3" style="font-size: 1.5rem;"></a>
                    @elseif (App::getLocale() == 'en')
                        <a href="#" class="flag-icon flag-icon-gbr ml-3" style="font-size: 1.5rem;"></a>
                    @endif


                </div>
            </div>
        </header>
    @endif











    @if (Auth::user())
        <div class="offcanvas offcanvas-start" tabindex="-1" id="sidebarPanel">
            <div class="offcanvas-body">
                <!-- profile box -->
                <div class="profileBox">
                    <div class="image-wrapper">
                        @if (auth()->user()->profile_picture)
                            <img src="{{ url(auth()->user()->profile_picture) }}"
                                alt="Imagen de perfil de {{ auth()->user()->user_name }}" class="imaged rounded">
                        @else
                            <img src="{{ asset('img/profile-picture-not-found.png') }}"
                                alt="Imagen de perfil de {{ auth()->user()->user_name }}" class="imaged rounded">
                        @endif
                    </div>
                    <div class="in">
                        <strong>{{ auth()->user()->user_name }}</strong>
                        <div class="text-muted">
                            € {{ auth()->user()->cash }} - € {{ auth()->user()->special_cash }}
                        </div>
                    </div>
                </div>
                <!-- * profile box -->

                <ul class="listview link-listview">
                    @if (App\SysUserRoles::where('user_id', Auth::id())->whereIn('role_id', [1])->count() > 0)
                        <li>
                            <a href="{{ url('/11w5Cj9WDAjnzlg0') }}" class="item">
                                <div class="in">
                                    <div> @lang('messages.panel_control')</div>
                                </div>
                            </a>
                        </li>
                    @endif
                    <li>
                        <a href="/listing/users">
                            @lang('messages.list_user')
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('offer_index') }}">
                            @lang('messages.show_offer')
                        </a>
                    </li>
                    <li>
                        <a href="/cart">
                            @lang('messages.shopping_cart')
                            @if (Auth::user())
                                {{ Auth::user()->lifetimeCart }}

                                <span class="badge badge-danger"
                                    id="br-cart-items">{{ $viewData['items_count'] }}</span>
                            @else
                                -
                            @endif
                        </a>
                    </li>
                    <li>
                        <a href="/alerts">
                            @lang('messages.alerts')
                            <span class="badge badge-danger">{{ count($viewData['header_newsletter']) }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="/notifications">
                            @lang('messages.notification')
                            @if (count($viewData['header_notifications']) > 0)
                                @if ($viewData['c_h_n'] > 0)
                                    <span class="badge badge-danger">{{ $viewData['c_h_n'] }}</span>
                                @endif
                            @endif
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('purchases_data') }}">
                            @lang('messages.the_purchase')



                        </a>
                    </li>
                    <li>
                        <a href="{{ route('sales_data') }}">
                            @lang('messages.the_sale')

                        </a>
                    </li>
                    <li>
                        <a href="{{ route('transaction_index') }}">
                            @lang('messages.the_transaction')

                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/account/inventory') }}">
                            @lang('messages.the_inventory')
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" onclick="salir()" class="item"
                            data-href="{{ url('/account/logout') }}">
                            <div class="in">
                                @lang('messages.get_out')
                            </div>
                        </a>
                    </li>
                </ul>

            </div>
            <!-- sidebar buttons -->
            <div class="sidebar-buttons">
                <a href="{{ route('information_index') }}" class="button">
                    <ion-icon name="help-outline"></ion-icon>
                </a>
                <a href="#" id="showlanguage" class="showlanguage button">
                    @if (App::getLocale() == 'es')
                        <span class="flag-icon flag-icon-esp"></span>
                    @elseif (App::getLocale() == 'en')
                        <span class="flag-icon flag-icon-gbr"></span>
                    @endif
                </a>

            </div>
            <!-- * sidebar buttons -->
        </div>
    @else
        <div class="offcanvas offcanvas-start" tabindex="-1" id="sidebarPanel">
            <div class="offcanvas-body">
                <!-- profile box -->
                <div class="profileBox">
                    <div class="image-wrapper">
                        <img src="{{ asset('img/profile-picture-not-found.png') }}" alt="RGM"
                            class="imaged rounded">
                    </div>
                    <div class="in">
                        <strong>@lang('messages.welcome_rgm')</strong>
                        <div class="text-muted">
                            RGM
                        </div>
                    </div>
                </div>
                <!-- * profile box -->

                <ul class="listview link-listview">
                    <li>
                        <a href="#" id="showLogin" class="item showLogin" data-nav-toggle="#nk-nav-mobile">
                            <div class="in">
                                <div>@lang('messages.sesion_register')</div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="/site/faqs">
                            FAQS
                        </a>
                    </li>
                    <li>
                        <a href="/site/terminos-y-condiciones">
                            @lang('messages.terms')
                        </a>
                    </li>
                    <li>
                        <a href="/site/aviso-legal">
                            @lang('messages.legal')
                        </a>
                    </li>
                    <li>
                        <a href="/site/politica-de-privacidad">
                            @lang('messages.pt_privacy')
                        </a>
                    </li>
                    <li>
                        <a href="/site/politica-de-cookies">
                            @lang('messages.pt_cookie')
                        </a>
                    </li>
                </ul>

            </div>
            <!-- sidebar buttons -->
            <div class="sidebar-buttons">
                <a href="#" id="showLogin" class="button showLogin" data-nav-toggle="#nk-nav-mobile">
                    <ion-icon name="person-outline"></ion-icon>
                </a>
                <a href="{{ route('information_index') }}" class="button">
                    <ion-icon name="help-outline"></ion-icon>
                </a>
                <a href="#" id="showlanguage" class="showlanguage button">
                    @if (App::getLocale() == 'es')
                        <span class="flag-icon flag-icon-esp"></span>
                    @elseif (App::getLocale() == 'en')
                        <span class="flag-icon flag-icon-gbr"></span>
                    @endif
                </a>

            </div>
            <!-- * sidebar buttons -->
        </div>
    @endif

    <div class="modal fade" id="modalLanguage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog lang-form" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title">@lang('messages.select_language')</h4>
                            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form action="/lang" class="m-3 ml-10 nk-form nk-form-style" id="nk-lang-form"
                                method="POST">
                                {{ csrf_field() }}
                                <select name="locale"
                                    class="form-control form-select p-1 pl-10 text-gray-900 pr-10 nk-lang-change"
                                    onchange="lang_change()" style="width: 100%;">
                                    <option data-img-src="{{ asset('assets/images/es/es.png') }}"
                                        value="/setlocale/es" {{ App::getLocale() == 'es' ? 'selected' : '' }}>
                                        Español
                                    </option>
                                    <option data-img-src="{{ asset('assets/images/en/en.png') }}"
                                        value="/setlocale/en" {{ App::getLocale() == 'en' ? 'selected' : '' }}>
                                        English
                                    </option>
                                    @if (App\SysUserRoles::where('user_id', Auth::id())->whereIn('role_id', [1])->count() > 0)
                                        <option data-img-src="{{ asset('assets/images/fr/fr.png') }}"
                                            value="/setlocale/fr" {{ App::getLocale() == 'fr' ? 'selected' : '' }}>
                                            Francés
                                        </option>
                                        <option data-img-src="{{ asset('assets/images/ita/ita.png') }}"
                                            value="/setlocale/ita" {{ App::getLocale() == 'ita' ? 'selected' : '' }}>
                                            Italia
                                        </option>
                                        <option data-img-src="{{ asset('assets/images/al/al.png') }}"
                                            value="/setlocale/al" {{ App::getLocale() == 'al' ? 'selected' : '' }}>
                                            Alemania
                                        </option>
                                        <option data-img-src="{{ asset('assets/images/prt/prt.png') }}"
                                            value="/setlocale/prt" {{ App::getLocale() == 'prt' ? 'selected' : '' }}>
                                            Portugués
                                        </option>
                                    @endif
                                </select>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">


                    </div>
                </div>
            </div>
        </div>
    </div>

@endif
