
@section('search-button')
    <li>
        <a href="#" class="search-btn-show-form">
            <span class="fa fa-search"></span>
        </a>
    </li>
@endsection

<div class="col-lg-12 nk-form-style-1 nk-search-form-validate" style="display:none;">
<div class="nk-gap"></div>
    <div class="input-group">
        <input type="text" name="s" id="search" class="form-control" autocomplete="off" placeholder="Busca un producto" >
        <button class="nk-btn nk-btn-color-main-1 search-btn-find" >
            <span class="fa fa-search"></span>
        </button>
    </div>
    <div class="not_found_product">
    </div>
    <ul class="list-group" id="result" style="position: absolute; z-index:5; width:97.5%"></ul>
<div class="nk-gap"></div>
</div>
