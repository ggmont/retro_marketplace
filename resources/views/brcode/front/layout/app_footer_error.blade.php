<div class="nk-gap-3"></div>
<!-- START: Footer -->
<!-- START: Footer -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-60DQ26YMLS"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-60DQ26YMLS');
</script>



</div>




@yield('content-script-include')



<!--All Js JAVENIST Here-->
<!--Jquery 1.12.4 ESTO HACE QUE EL CARRITO NO FUNCIONE-->

<!-- Version Mobile -->
@if ((new \Jenssegers\Agent\Agent())->isMobile())
    <script src="{{ asset('js/mobile/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/mobile/waypoints.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/mobile/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/mobile/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('js/mobile/default/jquery.passwordstrength.js') }}"></script>
    <script src="{{ asset('js/mobile/wow.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jarallax.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jarallax-video.min.js') }}"></script>
    <script src="{{ asset('js/mobile/default/dark-mode-switch.js') }}"></script>
    <script src="{{ asset('js/mobile/default/active.js') }}"></script>
    <script src="{{ asset('js/mobile/base.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://scripts.sirv.com/sirvjs/v3/sirv.js"></script>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    <script></script>
    <script>
        // Trigger welcome notification after 5 seconds
        setTimeout(() => {
            notification('notification-welcome', 10000);
        }, 2000);
    </script>
    <script>
        window.setMobileTable = function(selector) {
            // if (window.innerWidth > 600) return false;
            const tableEl = document.querySelector(selector);
            const thEls = tableEl.querySelectorAll('thead th');
            const tdLabels = Array.from(thEls).map(el => el.innerText);
            tableEl.querySelectorAll('tbody tr').forEach(tr => {
                Array.from(tr.children).forEach(
                    (td, ndx) => td.setAttribute('label', tdLabels[ndx])
                );
            });
        }

        $(function() {
            $('[data-toggle="popover"]').popover()
        })
    </script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>

    <script src="{{ asset('assets/dropzone-master/dist/min/dropzone.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/quagga/0.12.1/quagga.js"></script>
    <script src="{{ asset('vendor/sweetalert2/sweetalert2.all.min.js') }}"></script>
@else
    <!-- GSAP -->
    <script src="{{ asset('assets/vendor/gsap/src/minified/TweenMax.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/gsap/src/minified/plugins/ScrollToPlugin.min.js') }}"></script>

    <!-- Popper -->
    <script src="{{ asset('assets/vendor/popper.js/dist/umd/popper.min.js') }}"></script>

    <!-- Bootstrap -->
    <script src="{{ asset('assets/bootstrap-4.4/js/bootstrap.min.js') }}"></script>


    <script src="{{ asset('assets/js/goodgames.min.js') }}"></script>

    <!-- Seiyria Bootstrap Slider -->
    <script src="{{ asset('assets/vendor/bootstrap-slider/dist/bootstrap-slider.min.js') }}"></script>

    @yield('content-script-include')

    <!-- GoodGames -->
    <script src="{{ asset('assets/js/goodgames-init.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-notify.min.js') }}"></script>


    <!--All Js JAVENIST Here-->
    <!--Jquery 1.12.4 ESTO HACE QUE EL CARRITO NO FUNCIONE-->



    <!--Popper-->
    <script src="{{ asset('js/javenist/popper.min.js') }}"></script>

    <!--Bootstrap-->
    <script src="{{ asset('js/javenist/bootstrap.min.js') }}"></script>

    <!--Imagesloaded-->
    <script src="{{ asset('js/javenist/imagesloaded.pkgd.min.js') }}"></script>

    <!--Isotope-->
    <script src="{{ asset('js/javenist/isotope.pkgd.min.js') }}"></script>
    <!--Ui js-->
    <script src="{{ asset('js/javenist/jquery-ui.min.js') }}"></script>

    <!--Countdown-->
    <script src="{{ asset('js/javenist/jquery.countdown.min.js') }}"></script>
    <!--Counterup-->
    <script src="{{ asset('js/javenist/jquery.counterup.min.js') }}"></script>

    <!--ScrollUp-->
    <script src="{{ asset('js/javenist/jquery.scrollUp.min.js') }}"></script>

    <!--Chosen js-->
    <script src="{{ asset('js/javenist/chosen.jquery.js') }}"></script>

    <!--Meanmenu js-->
    <script src="{{ asset('js/javenist/jquery.meanmenu.min.js') }}"></script>

    <!--Instafeed-->
    <script src="{{ asset('js/javenist/instafeed.min.js') }}"></script>

    <!--EasyZoom-->
    <script src="{{ asset('js/javenist/easyzoom.min.js') }}"></script>


    <!--Nivo Slider-->
    <script src="{{ asset('js/javenist/jquery.nivo.slider.js') }}"></script>

    <!--Waypoints-->
    <script src="{{ asset('js/javenist/waypoints.min.js') }}"></script>

    <!--Carousel-->
    <script src="{{ asset('js/javenist/owl.carousel.min.js') }}"></script>

    <!--Slick-->
    <script src="{{ asset('js/javenist/slick.min.js') }}"></script>

    <!--Wow-->
    <script src="{{ asset('js/javenist/wow.min.js') }}"></script>

    <!--Plugins-->
    <script src="{{ asset('js/javenist/plugins.js') }}"></script>

    <!--Main Js-->
    <script src="{{ asset('js/javenist/main.js') }}"></script>
@endif

<script src="{{ asset('assets/noty/packaged/jquery.noty.packaged.min.js') }}"></script>
<script src="{{ asset('assets/select2/select2.min.js') }}"></script>

<!-- Angular JS -->
<script src="{{ asset('assets/angularjs/angular.min.js') }}"></script>
<script src="{{ asset('assets/angularjs/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('assets/angularjs/ui-bootstrap-tpls-2.5.0.min.js') }}"></script>
<script src="{{ asset('brcode/js/app.public.angjs-module.js') }}"></script>
<script src="{{ asset('brcode/js/app.public.angjs-topsearch.js') }}"></script>
<script src="{{ asset('brcode/js/app.public.angjs-register.js') }}"></script>

<script src="{{ asset('assets/cookie/cookiealert.js') }}"></script>

<script src="{{ asset('assets/imageSelect/js/chosen.jquery.js') }}"></script>
<script src="{{ asset('assets/imageSelect/js/ImageSelect.jquery.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<script>
    $(".nk-lang-change").chosen({
        width: '100%'
    });
</script>
<script type="text/javascript">
    $('#reload').click(function() {
        $.ajax({
            type: 'GET',
            url: '/reload-captcha',
            success: function(data) {
                $(".captcha span").html(data.captcha);
            }
        });
    });
</script>
@if (Auth::user())
    <script>
        $('.nk-cart-toggle').click(function() {
            location.href = "/cart";
        });
    </script>
@endif
<script></script>
<script>
    $(document).ready(function() {


        // $('#registerForm').modal('show');
        // $('.bropdown-back > a').click();
        if ('<?= session('
            message_confirm ') ?>' != '') {
            $.notify({
                message: '<?= session('
                message_confirm ') ?>'
            }, {
                // settings
                type: '<?= session('
                type_msg ') ?>',
                placement: {
                    from: "bottom",
                    align: "right"
                },
            });
        }

    });
</script>

<script>
    $(function() {
        var globalMsg = '<?= session('
        message ') ?>';
        var xhr = null;
        if (globalMsg.length > 0) {
            var globalMsgType = '<?= session('
            messageType ') ?>';
            var buttons = [];

            if (globalMsgType == 'warning') {
                buttons = [{
                    addClass: 'btn btn-primary btn-sm text-center col-md-12',
                    text: 'OK',
                    onClick: function($noty) {
                        $noty.close();
                    }
                }, ]
            }
            presentNotyMessage(globalMsg, globalMsgType, buttons);

        }

    });

    function presentNotyMessage(message, type, buttons, max) {

        buttons = typeof buttons == undefined ? [] : buttons;
        max = typeof max == undefined ? 5 : max;

        notySettings = {
            layout: 'topCenter',
            text: message,
            theme: 'relax',
            type: type,
            maxVisible: max,
            timeout: buttons.length == 0 ? 4000 : false,
        }

        if (buttons.length > 0) {
            notySettings.buttons = buttons;
        }

        var n = noty(notySettings);
    }
</script>
@if (Auth::user())
    <script>
        function salir() {
            var url = event.currentTarget.dataset.href;
            window.location.href = url;
        }
    </script>

    <script>
        // $('#vender').click();
        $(".select2-ajax").select2({
            minimumInputLength: 3,
            width: '100%',
            // tags: [],
            ajax: {
                url: "{{ route('ajaxProduct') }}",
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function(term) {
                    return {
                        term: term
                    };
                },
                processResults: function(data) {
                    console.log(data);
                    return {
                        results: $.map(data, function(item) {

                            return {
                                text: `${item.name} - ${item.platform}  - ${item.region} `,
                                id: item.id,
                            }
                        })
                    };
                }
            }
        });

        $(".select2-ajax").change(function() {
            if ($(this).val()) {
                $('#data-sub').html(
                    `<iframe src="https://www.retrogamingmarket.eu/account/inventory/add/${$(this).val()}?side=1" height="900px" width="100%" frameBorder="0"></iframe>`
                );
            }
        });
    </script>
@endif





@yield('content-script-include-ang')

<script src="{{ asset('brcode/js/app.public.js') }}"></script>

@yield('content-script')

@livewireScripts

</body>

</html>
