<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> {{ config('brcode.app_name_public') }}</title>

    <meta name="description" content="Retro Gaming Market - El marketplace especialista en videojuegos">
    <meta name="keywords" content="game, gaming, Retro Gaming, Market, Retro">
    <meta name="author" content="RetroGamingMarket">

    <link rel="icon" type="image/png" href="{{ asset('img/RGM.png') }}">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7cOpen+Sans:400,700" rel="stylesheet" type="text/css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/bootstrap-4.4/css/bootstrap.min.css') }}">

    <!-- FontAwesome -->
    <script defer src="{{ asset('assets/vendor/fontawesome-free/js/all.js') }}"></script>
    <script defer src="{{ asset('assets/vendor/fontawesome-free/js/v4-shims.js') }}"></script>

    <!-- IonIcons -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/ionicons/css/ionicons.min.css') }}">

    <!-- Seiyria Bootstrap Slider -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-slider/dist/css/bootstrap-slider.min.css') }}">

    <!-- GoodGames -->
    <link rel="stylesheet" href="{{ asset('assets/css/goodgames.css') }}">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/imageSelect/css/chosen.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/imageSelect/css/ImageSelect.css') }}">
    
    <link rel="stylesheet" href="{{ asset('assets/select2/select2.min.css') }}">
    @yield('content-css-include')
    <!-- END: Styles -->
    <link rel="stylesheet" href="{{ asset('assets/cookie/cookiealert.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/flag-icon/css/flag-icon.min.css') }}">

    <!-- jQuery -->
    <script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
    
    <style>
    .footer.content > img,
    .footer.content > p {
        display: inline-block;
    }
    </style>
    
</head>
<body ng-app="appMarketPlace">

<!-- END: Navbar Mobile -->
<div class="nk-main" >
  <!-- Content Wrapper. Contains page content -->
  
  <div style="min-height:500px">
  @yield('content')
  </div><!-- /.content-wrapper -->


</div>

<!-- GSAP -->
<script src="{{ asset('assets/vendor/gsap/src/minified/TweenMax.min.js') }}"></script>
<script src="{{ asset('assets/vendor/gsap/src/minified/plugins/ScrollToPlugin.min.js') }}"></script>

<!-- Popper -->
<script src="{{ asset('assets/vendor/popper.js/dist/umd/popper.min.js') }}"></script>

<!-- Bootstrap -->
<script src="{{ asset('assets/bootstrap-4.4/js/bootstrap.min.js') }}"></script>

<!-- Seiyria Bootstrap Slider -->
<script src="{{ asset('assets/vendor/bootstrap-slider/dist/bootstrap-slider.min.js') }}"></script>

@yield('content-script-include')

<!-- GoodGames -->
<script src="{{ asset('assets/js/goodgames.min.js') }}"></script>
<script src="{{ asset('assets/js/goodgames-init.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-notify.min.js') }}"></script>
<!-- END: Scripts -->

<script src="{{ asset('assets/select2/select2.min.js') }}"></script>

<!-- Angular JS -->
<script src="{{ asset('assets/angularjs/angular.min.js') }}"></script>
<script src="{{ asset('assets/angularjs/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('assets/angularjs/ui-bootstrap-tpls-2.5.0.min.js') }}"></script>
<script src="{{ asset('brcode/js/app.public.angjs-module.js') }}"></script>
<script src="{{ asset('brcode/js/app.public.angjs-topsearch.js') }}"></script>
<script src="{{ asset('brcode/js/app.public.angjs-register.js') }}"></script>

<script src="{{ asset('assets/cookie/cookiealert.js') }}"></script>


<script src="{{ asset('assets/imageSelect/js/chosen.jquery.js') }}"></script>
<script src="{{ asset('assets/imageSelect/js/ImageSelect.jquery.js') }}"></script>

<script src="{{ asset('assets/noty/packaged/jquery.noty.packaged.min.js') }}"></script>
<script>
  $(".nk-lang-change").chosen({ width: '100%' });
</script>
@if(Auth::user())
<script>
  $('.nk-cart-toggle').click(function() {
    location.href = "/cart";
  });
</script>
@endif
<script>
</script>
<script>
  $(document).ready(function(){
    // $('.bropdown-back > a').click();
    if('<?= session('message_confirm') ?>' != ''){
      $.notify({
        message: '<?= session('message_confirm') ?>'
      },{
        // settings
        type: '<?= session('type_msg') ?>',
        placement: {
          from: "bottom",
          align: "right"
        },
      });
    }
    
  });
</script>

<script>
$(function() {
		var globalMsg = '<?= session('message') ?>';
		var xhr = null;
		if(globalMsg.length > 0) {
			var globalMsgType = '<?= session('messageType') ?>';
			var buttons = [];

			if(globalMsgType == 'warning') {
				buttons =[
					{addClass: 'btn btn-primary btn-sm text-center col-md-12', text: 'OK', onClick: function($noty) {
							$noty.close();
						}
					},
				]
			}
			presentNotyMessage(globalMsg, globalMsgType, buttons);

		}

	});

  function presentNotyMessage(message,type, buttons,max) {

  buttons = typeof buttons == undefined ? [] : buttons;
  max = typeof max == undefined ? 5 : max;

    notySettings = {
      layout: 'topCenter',
      text: message,
      theme: 'relax',
      type: type,
      maxVisible: max,
      timeout: buttons.length == 0 ? 4000 : false,
    }

    if(buttons.length > 0) {
      notySettings.buttons = buttons;
    }

    var n = noty(notySettings);
  }
</script>
@if(Auth::user())
<script>
  function salir(){
    if($('#br-cart-items').text() > 0){
        $("#reload_logout").load(window.location.href + " #reload_logout");
        $('#ModalSingOut').modal('show');
    }else{
        location.href = '{{ url('/account/logout') }}';
    }
  }
</script>

<script>
  // $('#vender').click();
  $(".select2-ajax").select2({
    minimumInputLength: 3,
    width: '100%',
    // tags: [],
    ajax: {
        url: "{{route('ajaxProduct')}}",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (term) {
          return {
            term: term
          };
        },
        processResults: function (data) {
          return {
            results: $.map(data, function (item) {
              return {
                text: `${item.name} - ${item.platform}`,
                id: item.id,
              }
            })
          };
        }
    }
  });
  $('#data-sub').html(
    `<iframe src="http://127.0.0.1:8000/account/inventory/side/add/1009" height="900px" width="100%" frameBorder="0"></iframe>`
  );
</script>

@endif
  
  
<script>
  var BASEURL = '{{ url('/') }}';
  var GLOBAL_TRANSLATION = {!! $viewData['translations'] !!};
  var lang_rgm = '{{Config::get('app.locale')}}';
</script>
<script>
  function lang_change(){
    location.href = $('.nk-lang-change').val() ;
  }
</script>

@yield('content-script-include-ang')

<script src="{{ asset('brcode/js/app.public.js') }}"></script>

@yield('content-script')


</body>
</html>
