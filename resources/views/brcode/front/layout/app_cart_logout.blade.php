@include('brcode.front.layout.app_header')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
@yield('content')
</div><!-- /.content-wrapper -->

@include('brcode.front.layout.app_sidebar')

@include('brcode.front.layout.app_footer')
