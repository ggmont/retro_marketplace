<div class="nk-gap-3"></div>
<!-- START: Footer -->
<!-- START: Footer -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-60DQ26YMLS"></script>
<script>
    $(document).ready(function() {
        setInterval('window.location.reload()', 5600000);
    });
</script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-60DQ26YMLS');
</script>




</div>

<!-- END: Page Background -->
@if ((new \Jenssegers\Agent\Agent())->isMobile())
    <!-- START: Login Modal -->
    @include('cookieConsent::index')
    <div class="modal fade modalbox" id="modalLogin" role="dialog">
        <div class="modal-dialog modal-md login-form" role="document">

            <div class="modal-content">
                <div class="modal-header">
                    <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
                </div>
                <div class="modal-body">

                    <div class="login-wrapper d-flex align-items-center justify-content-center text-center">
                        <div class="col-12 col-sm-9 col-md-7 col-lg-6 col-xl-5"><img class="big-logo" width="78px"
                                height="111px" src="{{ asset('img/RGM.png') }}" alt=""><br><br><br><br>
                            <div class="container">
                                <div class="row justify-content-center">

                                    <!-- Register Form-->

                                    <form action="{{ url('/account/login') }}" method="post" class="form-horizontal">
                                        @csrf
                                        <div class="form-group boxed">
                                            <div class="input-wrapper">
                                                <input type="text" id="user_name" name="user_name" class="form-control" placeholder="Usuario">
                                                <i class="clear-input">
                                                    <ion-icon name="close-circle"></ion-icon>
                                                </i>
                                            </div>
                                        </div>
                                    
                                        <div class="form-group boxed">
                                            <div class="input-wrapper">
                                                <input type="password" value="" id="password" name="password" class="form-control" placeholder="Contraseña">
                                                <i class="clear-input">
                                                    <ion-icon name="close-circle"></ion-icon>
                                                </i>
                                            </div>
                                        </div>
                                    
                                        <div class="mt-4 text-start">
                                            <div class="form-check">
                                                <input type="checkbox" name="remember_me" class="form-check-input">
                                                <label class="form-check-label font-bold" for="customCheckb1">{{ __('Recordarme') }}</label>
                                            </div>
                                        </div>
                                    
                                        <br>
                                    
                                        <button id="submitLogin" class="btn btn-danger font-extrabold btn-lg w-100" type="submit">Iniciar Sesión</button>
                                    </form>

                                    <!-- Login Meta-->
                                    <div class="login-meta-data text-left"><a class="forgot-password d-block mt-3 mb-1"
                                            href="{{ url('/show_password_reset') }}">@lang('messages.forget_password')</a>
                                        <a class="forgot-password d-block mt-3 mb-1"
                                            href="{{ url('/show_user_reset') }}">@lang('messages.forget_user')</a>
                                        <a class="forgot-password d-block mt-3 mb-1"
                                            href="{{ url('/show_resend_activation') }}">@lang('messages.forget_email')</a>

                                        <p class="mb-0 mt-2">@lang('messages.forget_account')<a class="ml-1"
                                                href="{{ url('register-user') }}">
                                                <font color="#fe0000">@lang('messages.registers')</font>
                                            </a>
                                        </p>
                                        <p class="mb-0 mt-2">@lang('messages.doyouwant_enterprise')<a class="ml-1"
                                                href="{{ url('register-company') }}">
                                                <font color="#fe0000">@lang('messages.registers')</font>
                                            </a>
                                        </p>
                                    </div>
                                    <!-- View As Guest-->
                                    <div class="view-as-guest mt-3"><a class="btn"
                                            href="#">@lang('messages.also_sing')</a></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>




    <!-- END: Login Modal -->
@else
    @include('cookieConsent::index')
    @if (!Auth::id())
        <!-- START: Login Modal -->
        <div class="modal fade" id="modalLogin" role="dialog">
            <div class="modal-dialog modal-md login-form" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-details">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6">

                                    <h4 class="retro-sell">
                                        <font color="#ffd3b1"> {{ __('Iniciar') }} {{ __('Sesion') }}</font>
                                    </h4>
                                    <form action="{{ url('/account/login') }}" method="post" class="form-horizontal">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="nk-gap"></div>
                                        <h6 class="retro-prueba font-bold">
                                            <font color="#e5f6fb">{{ __('Usuario') }}</font>
                                        </h6>
                                        <input type="email" value="" id="user_name" name="user_name"
                                            class=" form-control">
                                        <span class="help-block error-message" style="display: none;"></span>

                                        <div class="nk-gap"></div>
                                        <h6 class="retro-prueba font-bold">
                                            <font color="#e5f6fb">{{ __('Contraseña') }}</font>
                                        </h6>
                                        <input type="password" value="" id="password" name="password"
                                            class="required form-control">
                                        <span class="help-block error-message" style="display: none;"></span>

                                        <div class="form-group" style="display: none;">
                                            <div class="nk-gap-2"></div>
                                            <div class="col-lg-12">
                                                <p class="bg-white login-message"></p>
                                            </div>
                                        </div>

                                        <div class="nk-gap"></div>
                                        <div style="background-color:#212529; padding: 1rem 0;">
                                            <label class="font-bold">
                                                <input type="checkbox" name="remember_me"
                                                    class="nes-checkbox is-dark" />
                                                <span>{{ __('Recordarme') }}</span>
                                            </label>
                                        </div>



                                        <div class="nk-gap"></div>
                                        <button type="button" id="submitLogin"
                                            class="nk-btn font-extrabold nk-btn-rounded nk-btn-color-white nk-btn-block">{{ __('Iniciar') }}
                                            {{ __('Sesión') }}</button>
                                        <small>
                                            <a href="{{ route('passReset') }}">
                                                <font color="#ffd3b1">@lang('messages.password_forgott')</font>
                                            </a><br>
                                            <a href="{{ route('userReset') }}">
                                                <font color="#ffd3b1">@lang('messages.user_forgott')
                                                </font>
                                            </a> <br>
                                            <a href="{{ route('ShowresendActivation') }}">
                                                <font color="#ffd3b1">
                                                    @lang('messages.email_forgott')
                                                </font>
                                            </a>
                                        </small>

                                        <div class="nk-gap"></div>
                                        <span class="retro-sell">@lang('messages.or_sing_up_register')</span>
                                        <ul class="text-center">
                                            <li><a class="nk-social-google-plus" href="/google/redirect"><i
                                                        class="nes-icon gmail is-medium"></i></a></li>
                                        </ul>
                                    </form>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="nk-gap-2 d-md-none"></div>

                                    <h4 class="mb-0"> <span class="retro-sell">
                                            <font color="#f81100">@lang('messages.register')</font>
                                        </span> </h4>


                                    <div class="nk-gap-2"></div>
                                    <small>
                                        <a href="#" id="showRegister"><span
                                                class="retro-sell hover:text-red-500">
                                                <font color="#e5f6fb">@lang('messages.not_member')
                                                    &nbsp; <button
                                                        class="nes-btn is-error retro-sell">@lang('messages.register')</button>
                                                </font>
                                            </span></a>
                                    </small>
                                    <div class="nk-gap-2"></div>
                                    <small>
                                        <a href="{{ url('register-company') }}"><span
                                                class="retro-sell hover:text-red-500">
                                                <font color="#e5f6fb">
                                                    @lang('messages.register_enterprise')
                                                    &nbsp; &nbsp; &nbsp; &nbsp; <button
                                                        class="nes-btn is-error retro-sell">@lang('messages.register')</button>
                                                </font>
                                            </span></a>
                                    </small>
                                    <div class="nk-gap-2"></div>
                                    <div class="nk-gap-2"></div>
                                    <div class="nk-gap-2"></div>
                                    <div class="nk-gap-2"></div>
                                    <span class="retro-sell">@lang('messages.or_sing_up_register_two')</span>
                                    <div class="nk-gap"></div>

                                    <ul class="text-center">
                                        <li><a class="nk-social-google-plus" href="/google/redirect"><i
                                                    class="nes-icon gmail is-medium"></i></a></li>
                                    </ul>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal-dialog modal-lg register-form" id="registerForm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" ng-controller="registerController as ctrl">


                        <h4 class="mb-0">
                            <font color="#f81100"> <span class="retro-sell">@lang('messages.register')</span>
                            </font>
                        </h4>

                        <div class="nk-gap-1"></div>

                        <div class="br-register-completed"
                            ng-class="{ 'br-anim-register-completed' : ctrl.formSubmitCompleted }">
                            <font color="#e5f6fb">
                                <h4 class="text-lg retro-sell">@lang('messages.account_register_success')</h4>
                            </font>
                        </div>

                        <form name="formRegistration" class="form-horizontal " novalidate
                            ng-submit="ctrl.submitForm(formRegistration)"
                            ng-class="{ 'br-anim-register-form-completed': ctrl.formSubmitCompleted}">
                            <div class="row">
                                <div class="col-12">
                                    <h6 class="font-bold">
                                        <font color="white">@lang('messages.personal_data')</font>
                                    </h6>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterFirstName.$dirty) || (formRegistration.brRegisterFirstName.$dirty && formRegistration.brRegisterFirstName.$invalid) }">
                                        <input type="text" class="form-control" name="brRegisterFirstName"
                                            ng-model="brRegisterFirstName" id="br-register-first-name"
                                            placeholder="{{ __('Su nombre') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterLastName.$dirty) || (formRegistration.brRegisterLastName.$dirty && formRegistration.brRegisterLastName.$invalid) }">
                                        <input type="text" class="form-control" name="brRegisterLastName"
                                            ng-model="brRegisterLastName" id="br-register-last-name"
                                            placeholder="{{ __('Su apellido') }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <h6 class="font-bold">
                                        <font color="white">{{ __('Dirección') }}</font>
                                    </h6>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterAddress.$dirty) || (formRegistration.brRegisterAddress.$dirty && formRegistration.brRegisterAddress.$invalid) }">
                                        <input type="text" class="form-control" name="brRegisterAddress"
                                            ng-model="brRegisterAddress" id="br-register-address"
                                            placeholder="{{ __('Su dirección') }}" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterZipCode.$dirty) || (formRegistration.brRegisterZipCode.$dirty && formRegistration.brRegisterZipCode.$invalid) }">
                                        <input type="text" class="form-control" name="brRegisterZipCode"
                                            ng-model="brRegisterZipCode" id="br-register-zipcode"
                                            placeholder="{{ __('Su código postal') }}" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCity.$dirty) || (formRegistration.brRegisterCity.$dirty && formRegistration.brRegisterCity.$invalid) }">
                                        <input type="text" class="form-control" name="brRegisterCity"
                                            ng-model="brRegisterCity" id="br-register-city"
                                            placeholder="{{ __('Su ciudad') }}" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCountry.$dirty) || (formRegistration.brRegisterCountry.$dirty && formRegistration.brRegisterCountry.$invalid) }">
                                        <select id="br-register-country" class="form-control br-ajs-select2"
                                            name="brRegisterCountry" ng-model="brRegisterCountry"
                                            data-br-placeholder="{{ __('Su país') }}" data-br-resolve="Y">
                                            <option style="display:none" value="">select a type</option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <h6 class="font-bold">
                                        <font color="white">@lang('messages.data_account_register')</font>
                                    </h6>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterEmail.$dirty) || (formRegistration.brRegisterEmail.$dirty && formRegistration.brRegisterEmail.$invalid) }">
                                        <input type="email" class="form-control" name="brRegisterEmail"
                                            ng-model="brRegisterEmail" id="br-register-email"
                                            placeholder="{{ __('Introduzca su correo') }}" required>
                                        <!-- <span style="color:red" ng-show="(formRegistration.$submitted && ! formRegistration.brRegisterEmail.$dirty) || (formRegistration.brRegisterEmail.$dirty && formRegistration.brRegisterEmail.$invalid)"> -->
                                        <span ng-show="formRegistration.brRegisterEmail.$error.email"
                                            class="text-danger">{{ __('El correo introducido es inválido') }}.</span>
                                        <!-- </span> -->
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterUserName.$dirty) || (formRegistration.brRegisterUserName.$dirty && formRegistration.brRegisterUserName.$invalid) }">
                                        <input type="text" class="form-control" name="brRegisterUserName"
                                            ng-model="brRegisterUserName" id="br-register-username"
                                            placeholder="{{ __('Su nuevo nombre de usuario') }}" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterPassword.$dirty) || (formRegistration.brRegisterPassword.$dirty && formRegistration.brRegisterPassword.$invalid) }">
                                        <input type="password" class="form-control" name="brRegisterPassword"
                                            ng-model="brRegisterPassword" id="br-register-password"
                                            placeholder="{{ __('Cree una contraseña') }}" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterPassword2.$dirty) || (formRegistration.brRegisterPassword2.$dirty && formRegistration.brRegisterPassword2.$invalid) }">
                                        <input type="password" class="form-control" name="brRegisterPassword2"
                                            ng-model="brRegisterPassword2" id="br-register-password-confirmation"
                                            placeholder="{{ __('Repita la contraseña') }}" required>
                                        <span style="color:red"
                                            ng-show="( formRegistration.brRegisterPassword2.$dirty && formRegistration.brRegisterPassword.$valid && formRegistration.brRegisterPassword.$viewValue != formRegistration.brRegisterPassword2.$viewValue)">
                                            <span>{{ __('Las contraseñas no son iguales') }}.</span>
                                        </span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="captcha">
                                            {!! NoCaptcha::renderJs() !!}
                                            {!! NoCaptcha::display() !!}
                                            @if ($errors->has('g-recaptcha-response'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                </div>



                                <div class="mt-5 col-12"> </div>

                                <div class="col-md-6">
                                    <div style="background-color:#212529; padding: 1rem 0;">
                                        <label>
                                            <input type="checkbox" name="brRegisterRef" ng-model="brRegisterRef"
                                                id="brRegisterRef" ng-click="ctrl.referred()"
                                                class="nes-checkbox is-dark" />
                                            <span class="h6 font-bold">@lang('messages.recomended')</span>
                                        </label>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="brRegisterReferred"
                                        ng-model="brRegisterReferred" id="br-register-referred"
                                        ng-show="ctrl.formReferred "
                                        placeholder="{{ __('Recomendado por (Nombre de usuario)') }}">
                                </div>

                                <div class="mt-5 col-12"></div>

                                <div class="mt-5 mb-5 col-md-6">
                                    <div style="background-color:#212529; padding: 1rem 0;">
                                        <label>
                                            <input type="checkbox" name="brRegisterTerms" ng-model="brRegisterTerms"
                                                id="br-register-terms" required
                                                class="nes-checkbox is-dark { 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterTerms.$dirty) || (formRegistration.brRegisterTerms.$dirty && formRegistration.brRegisterTerms.$invalid) }" />
                                            <span>@lang('messages.term_and_conditions_one') <a href="/site/terminos-y-condiciones"
                                                    target="_blank">
                                                    @lang('messages.term_and_conditions_two') </a> & <a href="/site/politica-de-privacidad"
                                                    target="_blank"> Política de
                                                    privacidad</a></span>
                                        </label>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div style="background-color:#212529; padding: 1rem 0;">
                                        <label>
                                            <input type="checkbox" name="brRegisterYearsOld"
                                                ng-model="brRegisterYearsOld" id="br-register-years-old" required
                                                class="nes-checkbox is-dark { 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterYearsOld.$dirty) || (formRegistration.brRegisterYearsOld.$dirty && formRegistration.brRegisterYearsOld.$invalid) }" />
                                            <span>{{ __('Tengo mas de 18 años') }}</span>
                                        </label>
                                    </div>


                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-10">
                                    <a href="{{ url('register-company') }}"><span class="retro-sell">
                                            <font color="white">{{ __('Desea registrar una cuenta empresarial') }}?
                                            </font>
                                        </span></a>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-lg-10">
                                    <span style="color:red" ng-show="ctrl.formError">
                                        <span ng-bind-html="ctrl.formErrorMessage"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6 ">
                                    <button type="submit"
                                        class="float-right retro-sell nk-btn nk-btn-rounded nk-btn-color-white nk-btn-block"><i
                                            class="fa fa-user-plus"></i> {{ __('Registrarse') }}</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
        <!-- END: Login Modal -->
    @endif
@endif



@yield('content-script-include')



<!--All Js JAVENIST Here-->
<!--Jquery 1.12.4 ESTO HACE QUE EL CARRITO NO FUNCIONE-->

<!-- Version Mobile -->
@if ((new \Jenssegers\Agent\Agent())->isMobile())
    <script src="{{ asset('js/mobile/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/mobile/waypoints.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/mobile/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/mobile/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('js/mobile/default/jquery.passwordstrength.js') }}"></script>
    <script src="{{ asset('js/mobile/wow.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jarallax.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jarallax-video.min.js') }}"></script>
    <script src="{{ asset('js/mobile/default/dark-mode-switch.js') }}"></script>
    <script src="{{ asset('js/mobile/default/active.js') }}"></script>
    <script src="{{ asset('js/mobile/base.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://scripts.sirv.com/sirvjs/v3/sirv.js"></script>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    <script></script>
    <script>
        // Trigger welcome notification after 5 seconds
        setTimeout(() => {
            notification('notification-welcome', 10000);
        }, 2000);
    </script>
    <script>
        // Trigger welcome notification after 5 seconds
        setTimeout(() => {
            notification('cookies-box', 10000);
        }, 2000);
    </script>
    <script>
        window.setMobileTable = function(selector) {
            // if (window.innerWidth > 600) return false;
            const tableEl = document.querySelector(selector);
            const thEls = tableEl.querySelectorAll('thead th');
            const tdLabels = Array.from(thEls).map(el => el.innerText);
            tableEl.querySelectorAll('tbody tr').forEach(tr => {
                Array.from(tr.children).forEach(
                    (td, ndx) => td.setAttribute('label', tdLabels[ndx])
                );
            });
        }

        $(function() {
            $('[data-toggle="popover"]').popover()
        })
    </script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>

    <script src="{{ asset('assets/dropzone-master/dist/min/dropzone.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/quagga/0.12.1/quagga.js"></script>
    <script src="{{ asset('vendor/sweetalert2/sweetalert2.all.min.js') }}"></script>
@else
    <!-- GSAP -->
    <script src="{{ asset('assets/vendor/gsap/src/minified/TweenMax.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/gsap/src/minified/plugins/ScrollToPlugin.min.js') }}"></script>

    <!-- Popper -->
    <script src="{{ asset('assets/vendor/popper.js/dist/umd/popper.min.js') }}"></script>

    <!-- Bootstrap -->
    <script src="{{ asset('assets/bootstrap-4.4/js/bootstrap.min.js') }}"></script>


    <script src="{{ asset('assets/js/goodgames.min.js') }}"></script>

    <!-- Seiyria Bootstrap Slider -->
    <script src="{{ asset('assets/vendor/bootstrap-slider/dist/bootstrap-slider.min.js') }}"></script>

    @yield('content-script-include')

    <!-- GoodGames -->
    <script src="{{ asset('assets/js/goodgames-init.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-notify.min.js') }}"></script>


    <!--All Js JAVENIST Here-->
    <!--Jquery 1.12.4 ESTO HACE QUE EL CARRITO NO FUNCIONE-->



    <!--Popper-->
    <script src="{{ asset('js/javenist/popper.min.js') }}"></script>

    <!--Bootstrap-->
    <script src="{{ asset('js/javenist/bootstrap.min.js') }}"></script>

    <!--Imagesloaded-->
    <script src="{{ asset('js/javenist/imagesloaded.pkgd.min.js') }}"></script>

    <!--Isotope-->
    <script src="{{ asset('js/javenist/isotope.pkgd.min.js') }}"></script>
    <!--Ui js-->
    <script src="{{ asset('js/javenist/jquery-ui.min.js') }}"></script>

    <!--Countdown-->
    <script src="{{ asset('js/javenist/jquery.countdown.min.js') }}"></script>
    <!--Counterup-->
    <script src="{{ asset('js/javenist/jquery.counterup.min.js') }}"></script>

    <!--ScrollUp-->
    <script src="{{ asset('js/javenist/jquery.scrollUp.min.js') }}"></script>

    <!--Chosen js-->
    <script src="{{ asset('js/javenist/chosen.jquery.js') }}"></script>

    <!--Meanmenu js-->
    <script src="{{ asset('js/javenist/jquery.meanmenu.min.js') }}"></script>

    <!--Instafeed-->
    <script src="{{ asset('js/javenist/instafeed.min.js') }}"></script>

    <!--EasyZoom-->
    <script src="{{ asset('js/javenist/easyzoom.min.js') }}"></script>


    <!--Nivo Slider-->
    <script src="{{ asset('js/javenist/jquery.nivo.slider.js') }}"></script>

    <!--Waypoints-->
    <script src="{{ asset('js/javenist/waypoints.min.js') }}"></script>

    <!--Carousel-->
    <script src="{{ asset('js/javenist/owl.carousel.min.js') }}"></script>

    <!--Slick-->
    <script src="{{ asset('js/javenist/slick.min.js') }}"></script>

    <!--Wow-->
    <script src="{{ asset('js/javenist/wow.min.js') }}"></script>

    <!--Plugins-->
    <script src="{{ asset('js/javenist/plugins.js') }}"></script>

    <!--Main Js-->
    <script src="{{ asset('js/javenist/main.js') }}"></script>
@endif

<script src="{{ asset('assets/noty/packaged/jquery.noty.packaged.min.js') }}"></script>
<script src="{{ asset('assets/select2/select2.min.js') }}"></script>

<!-- Angular JS -->
<script src="{{ asset('assets/angularjs/angular.min.js') }}"></script>
<script src="{{ asset('assets/angularjs/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('assets/angularjs/ui-bootstrap-tpls-2.5.0.min.js') }}"></script>
<script src="{{ asset('brcode/js/app.public.angjs-module.js') }}"></script>
<script src="{{ asset('brcode/js/app.public.angjs-topsearch.js') }}"></script>
<script src="{{ asset('brcode/js/app.public.angjs-register.js') }}"></script>

<script src="{{ asset('assets/cookie/cookiealert.js') }}"></script>

<script src="{{ asset('assets/imageSelect/js/chosen.jquery.js') }}"></script>
<script src="{{ asset('assets/imageSelect/js/ImageSelect.jquery.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<script>
    $(".nk-lang-change").chosen({
        width: '100%'
    });
</script>
<script type="text/javascript">
    $('#reload').click(function() {
        $.ajax({
            type: 'GET',
            url: '/reload-captcha',
            success: function(data) {
                $(".captcha span").html(data.captcha);
            }
        });
    });
</script>
@if (Auth::user())
    <script>
        $('.nk-cart-toggle').click(function() {
            location.href = "/cart";
        });
    </script>
@endif
<script></script>
<script>
    $(document).ready(function() {


        // $('#registerForm').modal('show');
        // $('.bropdown-back > a').click();
        if ('<?= session('
            message_confirm ') ?>' != '') {
            $.notify({
                message: '<?= session('
                message_confirm ') ?>'
            }, {
                // settings
                type: '<?= session('
                type_msg ') ?>',
                placement: {
                    from: "bottom",
                    align: "right"
                },
            });
        }

    });
</script>

<script>
    $(function() {
        var globalMsg = '<?= session('
        message ') ?>';
        var xhr = null;
        if (globalMsg.length > 0) {
            var globalMsgType = '<?= session('
            messageType ') ?>';
            var buttons = [];

            if (globalMsgType == 'warning') {
                buttons = [{
                    addClass: 'btn btn-primary btn-sm text-center col-md-12',
                    text: 'OK',
                    onClick: function($noty) {
                        $noty.close();
                    }
                }, ]
            }
            presentNotyMessage(globalMsg, globalMsgType, buttons);

        }

    });

    function presentNotyMessage(message, type, buttons, max) {

        buttons = typeof buttons == undefined ? [] : buttons;
        max = typeof max == undefined ? 5 : max;

        notySettings = {
            layout: 'topCenter',
            text: message,
            theme: 'relax',
            type: type,
            maxVisible: max,
            timeout: buttons.length == 0 ? 4000 : false,
        }

        if (buttons.length > 0) {
            notySettings.buttons = buttons;
        }

        var n = noty(notySettings);
    }
</script>
@if (Auth::user())
    <script>
        function salir() {
            var url = event.currentTarget.dataset.href;
            window.location.href = url;
        }
    </script>

    <script>
        // $('#vender').click();
        $(".select2-ajax").select2({
            minimumInputLength: 3,
            width: '100%',
            // tags: [],
            ajax: {
                url: "{{ route('ajaxProduct') }}",
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function(term) {
                    return {
                        term: term
                    };
                },
                processResults: function(data) {
                    console.log(data);
                    return {
                        results: $.map(data, function(item) {

                            return {
                                text: `${item.name} - ${item.platform}  - ${item.region} `,
                                id: item.id,
                            }
                        })
                    };
                }
            }
        });

        $(".select2-ajax").change(function() {
            if ($(this).val()) {
                $('#data-sub').html(
                    `<iframe src="https://www.retrogamingmarket.eu/account/inventory/add/${$(this).val()}?side=1" height="900px" width="100%" frameBorder="0"></iframe>`
                );
            }
        });
    </script>
@endif



<script>
    function lang_change() {
        location.href = $('.nk-lang-change').val();
    }
</script>



@yield('content-script-include-ang')

<script src="{{ asset('brcode/js/app.public.js') }}"></script>

@yield('content-script')

@livewireScripts

</body>

</html>
