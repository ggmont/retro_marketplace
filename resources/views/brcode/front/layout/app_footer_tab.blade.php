<div class="nk-gap-3"></div>
<!-- START: Footer -->
<!-- START: Footer -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-60DQ26YMLS"></script>
<script>
    $(document).ready(function() {
        setInterval('window.location.reload()', 5600000);
    });
</script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-60DQ26YMLS');
</script>
@if ((new \Jenssegers\Agent\Agent())->isMobile())
    <div class="footer-nav-area" id="footerNav">
        <div class="container h-100 px-0">
            <div class="appBottomMenu">
                <div @if (request()->is('/')) class="item active"  @else class="item" @endif>
                    <a href="/">
                        <div class="col">
                            <ion-icon name="home-outline"></ion-icon>
                            <strong>@lang('messages.start')</strong>
                        </div>
                    </a>
                </div>
                @if (Auth::user())
                    @livewire('menu-home')
                @else
                    <a href="#" id="showLogin" data-nav-toggle="#nk-nav-mobile" class="showLogin item">
                        <div class="col">
                            <ion-icon name="chatbubbles-outline"></ion-icon>
                            <strong>@lang('messages.chat')</strong>
                        </div>
                    </a>
                    <a href="#" id="showLogin" data-nav-toggle="#nk-nav-mobile" class="showLogin item">
                        <div class="col">
                            <ion-icon name="add-circle-outline"></ion-icon>
                            <strong>Vender</strong>
                        </div>
                    </a>

                    <a href="#" id="showLogin" data-nav-toggle="#nk-nav-mobile" class="showLogin item">
                        <div class="col">
                            <ion-icon name="cash-outline"></ion-icon>
                            <strong>@lang('messages.the_price')</strong>
                        </div>
                    </a>
                    <a href="#" id="showLogin" data-nav-toggle="#nk-nav-mobile" class="showLogin item">
                        <div class="col">
                            <ion-icon name="person-outline"></ion-icon>
                            <strong>@lang('messages.the_configuration')</strong>
                        </div>
                    </a>
                @endif
            </div>
        </div>
    </div>
@else
    <footer class="bg-gray-800 py-8">
        <div class="container mx-auto">
            <div class="flex flex-wrap justify-center">
                <div class="w-full md:w-1/3 px-4 mb-4 md:mb-0">
                    <h2 class="text-gray-300 font-bold text-lg mb-2">Información</h2>
                    <p class="text-gray-400 text-sm">-</p>
                </div>
                <div class="w-full md:w-1/3 px-4 mb-4 md:mb-0">
                    <h2 class="text-gray-300 font-bold text-lg mb-2">Contacto</h2>
                    <p class="text-gray-400 text-sm">Correo: contacto@retrogamingmarket.com</p>
                    <p class="text-gray-400 text-sm">Teléfono: -</p>
                </div>
                <div class="w-full md:w-1/3 px-4 mb-4 md:mb-0">
                    <h2 class="text-gray-300 font-bold text-lg mb-2">Redes Sociales</h2>
                    <div class="flex">
                        <a href="#" class="text-gray-400 hover:text-white mr-4">
                            <i class="fab fa-facebook fa-2x"></i>
                        </a>
                        <a href="#" class="text-gray-400 hover:text-white mr-4">
                            <i class="fab fa-twitter fa-2x"></i>
                        </a>
                        <a href="#" class="text-gray-400 hover:text-white">
                            <i class="fab fa-instagram fa-2x"></i>
                        </a>
                    </div>
                </div>
            </div>
            <hr class="border-gray-700 my-8">
            <div class="flex justify-center">
                <p class="text-gray-400 text-sm">&copy; 2023 RetroGamingMarket. Todos los derechos reservados.</p>
            </div>
        </div>
    </footer>

@endif



</div>

<!-- END: Page Background -->
@if ((new \Jenssegers\Agent\Agent())->isMobile())
    <!-- START: Login Modal -->
    @include('cookieConsent::index')
    <div class="modal fade modalbox" id="modalLogin" role="dialog">
        <div class="modal-dialog modal-md login-form" role="document">
    
            <div class="modal-content">
                <div class="modal-header">
                    <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
                </div>
                <div class="modal-body">
    
                    <div class="d-flex align-items-center justify-content-center text-center">
                        <div class="col-12 col-sm-9 col-md-7 col-lg-6 col-xl-5">
                            <img class="big-logo mt-0" width="250px" src="{{ asset('img/RGMBANNER.png') }}" alt="RGM">
                            <br>
                            <div class="container mt-0">
                                <div class="row justify-content-center">
                                    <!-- Register Form-->
                                    @livewire('login')
    
                                    <div class="flex justify-between mt-3">
                                        <a class="forgot-password" href="/site/normas-y-recomendaciones">
                                            <font color="#000000">Normas y recomendaciones</font>
                                        </a>
                                    
                                        <a class="forgot-password" href="{{ url('register-user') }}">
                                            <font color="#fe0000">@lang('messages.registers')</font>
                                        </a>
                                    </div>
 
    
                                    <div class="mt-4 text-center">
                                        <div class="p-2">
                                            <h4 class="text-lg font-semibold mb-3">Síguenos en nuestras redes sociales</h4>
                                            <div class="flex justify-center space-x-4">
                                                <a href="https://www.facebook.com/people/Retrogamingmarket/100063863901900/"
                                                    class="btn btn-icon btn-sm btn-facebook">
                                                    <ion-icon name="logo-facebook"></ion-icon>
                                                </a>
                                                <a href="https://twitter.com/Retrogamingmkt"
                                                    class="btn btn-icon btn-sm btn-twitter">
                                                    <ion-icon name="logo-twitter"></ion-icon>
                                                </a>
                                                <a href="https://www.instagram.com/retrogamingmarket/"
                                                    class="btn btn-icon btn-sm btn-instagram">
                                                    <ion-icon name="logo-instagram"></ion-icon>
                                                </a>
                                                <a href="https://www.twitch.tv/retrogamingmarket"
                                                    class="btn btn-icon btn-sm btn-twitch">
                                                    <ion-icon name="logo-twitch"></ion-icon>
                                                </a>
                                                <a href="https://www.tiktok.com/@retrogamingmarket"
                                                    class="btn btn-icon btn-sm btn-tiktok">
                                                    <ion-icon name="logo-tiktok"></ion-icon>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
    
                                    <div class="text-center">
                                        <div class="p-2">
                                            <h4 class="text-lg font-semibold mb-3">Colabora</h4>
                                            <div class="flex justify-center space-x-4">
                                                <a href="https://www.patreon.com/retrogamingmarket"
                                                    class="btn btn-icon btn-sm btn-pateon">
                                                    <i class="fa-brands fa-patreon" style="color: #f7f7f7;"></i>
                                                </a>
                                                <a href="https://www.paypal.com/donate/?hosted_button_id=4BEVQXF9EJMTC"
                                                    class="btn btn-icon btn-sm btn-paypal">
                                                    <ion-icon name="logo-paypal"></ion-icon>
                                                </a>
                                                <a href="https://www.twitch.tv/retrogamingmarket" class="btn btn-icon btn-sm btn-blog">
                                                    <img src="{{ asset('img/icon/prime.png') }}" alt="Icono" style="width: 16px; height: 16px;">
                                                </a>                                                
                                            </div>
                                        </div>
                                    </div>
    
                                </div>
                            </div>
                        </div>
                    </div>
    
                </div>
            </div>
        </div>
    </div>
    




    <!-- END: Login Modal -->
@else
    @include('cookieConsent::index')
    @if (!Auth::id())
        <!-- START: Login Modal -->
        <div class="modal fade" id="modalLogin" role="dialog">
            <div class="modal-dialog modal-md login-form" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-details">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6">

                                    <h4 class="retro-sell">
                                        <font color="#ffd3b1"> {{ __('Iniciar') }} {{ __('Sesion') }}</font>
                                    </h4>
                                    <form action="{{ url('/account/login') }}" method="post"
                                        class="form-horizontal">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="nk-gap"></div>
                                        <h6 class="retro-prueba font-bold">
                                            <font color="#e5f6fb">{{ __('Usuario') }}</font>
                                        </h6>
                                        <input type="email" value="" id="user_name" name="user_name"
                                            class=" form-control">
                                        <span class="help-block error-message" style="display: none;"></span>

                                        <div class="nk-gap"></div>
                                        <h6 class="retro-prueba font-bold">
                                            <font color="#e5f6fb">{{ __('Contraseña') }}</font>
                                        </h6>
                                        <input type="password" value="" id="password" name="password"
                                            class="required form-control">
                                        <span class="help-block error-message" style="display: none;"></span>

                                        <div class="form-group" style="display: none;">
                                            <div class="nk-gap-2"></div>
                                            <div class="col-lg-12">
                                                <p class="bg-white login-message"></p>
                                            </div>
                                        </div>

                                        <div class="nk-gap"></div>
                                        <div style="background-color:#212529; padding: 1rem 0;">
                                            <label class="font-bold">
                                                <input type="checkbox" name="remember_me"
                                                    class="nes-checkbox is-dark" />
                                                <span>{{ __('Recordarme') }}</span>
                                            </label>
                                        </div>



                                        <div class="nk-gap"></div>
                                        <button type="button" id="submitLogin"
                                            class="nk-btn font-extrabold nk-btn-rounded nk-btn-color-white nk-btn-block">{{ __('Iniciar') }}
                                            {{ __('Sesión') }}</button>
                                        <small>
                                            <a href="{{ route('passReset') }}">
                                                <font color="#ffd3b1">@lang('messages.password_forgott')</font>
                                            </a><br>
                                            <a href="{{ route('userReset') }}">
                                                <font color="#ffd3b1">@lang('messages.user_forgott')
                                                </font>
                                            </a> <br>
                                            <a href="{{ route('ShowresendActivation') }}">
                                                <font color="#ffd3b1">
                                                    @lang('messages.email_forgott')
                                                </font>
                                            </a>
                                        </small>

                                        <div class="nk-gap"></div>
                                        <span class="retro-sell">@lang('messages.or_sing_up_register')</span>
                                        <ul class="text-center">
                                            <li><a class="nk-social-google-plus" href="/google/redirect"><i
                                                        class="nes-icon gmail is-medium"></i></a></li>
                                        </ul>
                                    </form>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="nk-gap-2 d-md-none"></div>

                                    <h4 class="mb-0"> <span class="retro-sell">
                                            <font color="#f81100">@lang('messages.register')</font>
                                        </span> </h4>


                                    <div class="nk-gap-2"></div>
                                    <small>
                                        <a href="#" id="showRegister"><span
                                                class="retro-sell hover:text-red-500">
                                                <font color="#e5f6fb">@lang('messages.not_member')
                                                    &nbsp; <button
                                                        class="nes-btn is-error retro-sell">@lang('messages.register')</button>
                                                </font>
                                            </span></a>
                                    </small>
                                    <div class="nk-gap-2"></div>
                                    <small>
                                        <a href="{{ url('register-company') }}"><span
                                                class="retro-sell hover:text-red-500">
                                                <font color="#e5f6fb">
                                                    @lang('messages.register_enterprise')
                                                    &nbsp; &nbsp; &nbsp; &nbsp; <button
                                                        class="nes-btn is-error retro-sell">@lang('messages.register')</button>
                                                </font>
                                            </span></a>
                                    </small>
                                    <div class="nk-gap-2"></div>
                                    <div class="nk-gap-2"></div>
                                    <div class="nk-gap-2"></div>
                                    <div class="nk-gap-2"></div>
                                    <span class="retro-sell">@lang('messages.or_sing_up_register_two')</span>
                                    <div class="nk-gap"></div>

                                    <ul class="text-center">
                                        <li><a class="nk-social-google-plus" href="/google/redirect"><i
                                                    class="nes-icon gmail is-medium"></i></a></li>
                                    </ul>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal-dialog modal-lg register-form" id="registerForm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" ng-controller="registerController as ctrl">


                        <h4 class="mb-0">
                            <font color="#f81100"> <span class="retro-sell">@lang('messages.register')</span>
                            </font>
                        </h4>

                        <div class="nk-gap-1"></div>

                        <div class="br-register-completed"
                            ng-class="{ 'br-anim-register-completed' : ctrl.formSubmitCompleted }">
                            <font color="#e5f6fb">
                                <h4 class="text-lg retro-sell">@lang('messages.account_register_success')</h4>
                            </font>
                        </div>

                        <form name="formRegistration" class="form-horizontal " novalidate
                            ng-submit="ctrl.submitForm(formRegistration)"
                            ng-class="{ 'br-anim-register-form-completed': ctrl.formSubmitCompleted}">
                            <div class="row">
                                <div class="col-12">
                                    <h6 class="font-bold">
                                        <font color="white">@lang('messages.personal_data')</font>
                                    </h6>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterFirstName.$dirty) || (formRegistration.brRegisterFirstName.$dirty && formRegistration.brRegisterFirstName.$invalid) }">
                                        <input type="text" class="form-control" name="brRegisterFirstName"
                                            ng-model="brRegisterFirstName" id="br-register-first-name"
                                            placeholder="{{ __('Su nombre') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterLastName.$dirty) || (formRegistration.brRegisterLastName.$dirty && formRegistration.brRegisterLastName.$invalid) }">
                                        <input type="text" class="form-control" name="brRegisterLastName"
                                            ng-model="brRegisterLastName" id="br-register-last-name"
                                            placeholder="{{ __('Su apellido') }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <h6 class="font-bold">
                                        <font color="white">{{ __('Dirección') }}</font>
                                    </h6>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterAddress.$dirty) || (formRegistration.brRegisterAddress.$dirty && formRegistration.brRegisterAddress.$invalid) }">
                                        <input type="text" class="form-control" name="brRegisterAddress"
                                            ng-model="brRegisterAddress" id="br-register-address"
                                            placeholder="{{ __('Su dirección') }}" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterZipCode.$dirty) || (formRegistration.brRegisterZipCode.$dirty && formRegistration.brRegisterZipCode.$invalid) }">
                                        <input type="text" class="form-control" name="brRegisterZipCode"
                                            ng-model="brRegisterZipCode" id="br-register-zipcode"
                                            placeholder="{{ __('Su código postal') }}" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCity.$dirty) || (formRegistration.brRegisterCity.$dirty && formRegistration.brRegisterCity.$invalid) }">
                                        <input type="text" class="form-control" name="brRegisterCity"
                                            ng-model="brRegisterCity" id="br-register-city"
                                            placeholder="{{ __('Su ciudad') }}" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCountry.$dirty) || (formRegistration.brRegisterCountry.$dirty && formRegistration.brRegisterCountry.$invalid) }">
                                        <select id="br-register-country" class="form-control br-ajs-select2"
                                            name="brRegisterCountry" ng-model="brRegisterCountry"
                                            data-br-placeholder="{{ __('Su país') }}" data-br-resolve="Y">
                                            <option style="display:none" value="">select a type</option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <h6 class="font-bold">
                                        <font color="white">@lang('messages.data_account_register')</font>
                                    </h6>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterEmail.$dirty) || (formRegistration.brRegisterEmail.$dirty && formRegistration.brRegisterEmail.$invalid) }">
                                        <input type="email" class="form-control" name="brRegisterEmail"
                                            ng-model="brRegisterEmail" id="br-register-email"
                                            placeholder="{{ __('Introduzca su correo') }}" required>
                                        <!-- <span style="color:red" ng-show="(formRegistration.$submitted && ! formRegistration.brRegisterEmail.$dirty) || (formRegistration.brRegisterEmail.$dirty && formRegistration.brRegisterEmail.$invalid)"> -->
                                        <span ng-show="formRegistration.brRegisterEmail.$error.email"
                                            class="text-danger">{{ __('El correo introducido es inválido') }}.</span>
                                        <!-- </span> -->
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterUserName.$dirty) || (formRegistration.brRegisterUserName.$dirty && formRegistration.brRegisterUserName.$invalid) }">
                                        <input type="text" class="form-control" name="brRegisterUserName"
                                            ng-model="brRegisterUserName" id="br-register-username"
                                            placeholder="{{ __('Su nuevo nombre de usuario') }}" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterPassword.$dirty) || (formRegistration.brRegisterPassword.$dirty && formRegistration.brRegisterPassword.$invalid) }">
                                        <input type="password" class="form-control" name="brRegisterPassword"
                                            ng-model="brRegisterPassword" id="br-register-password"
                                            placeholder="{{ __('Cree una contraseña') }}" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterPassword2.$dirty) || (formRegistration.brRegisterPassword2.$dirty && formRegistration.brRegisterPassword2.$invalid) }">
                                        <input type="password" class="form-control" name="brRegisterPassword2"
                                            ng-model="brRegisterPassword2" id="br-register-password-confirmation"
                                            placeholder="{{ __('Repita la contraseña') }}" required>
                                        <span style="color:red"
                                            ng-show="( formRegistration.brRegisterPassword2.$dirty && formRegistration.brRegisterPassword.$valid && formRegistration.brRegisterPassword.$viewValue != formRegistration.brRegisterPassword2.$viewValue)">
                                            <span>{{ __('Las contraseñas no son iguales') }}.</span>
                                        </span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="captcha">
                                            {!! NoCaptcha::renderJs() !!}
                                            {!! NoCaptcha::display() !!}
                                            @if ($errors->has('g-recaptcha-response'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                </div>



                                <div class="mt-5 col-12"> </div>

                                <div class="col-md-6">
                                    <div style="background-color:#212529; padding: 1rem 0;">
                                        <label>
                                            <input type="checkbox" name="brRegisterRef" ng-model="brRegisterRef"
                                                id="brRegisterRef" ng-click="ctrl.referred()"
                                                class="nes-checkbox is-dark" />
                                            <span class="h6 font-bold">@lang('messages.recomended')</span>
                                        </label>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="brRegisterReferred"
                                        ng-model="brRegisterReferred" id="br-register-referred"
                                        ng-show="ctrl.formReferred "
                                        placeholder="{{ __('Recomendado por (Nombre de usuario)') }}">
                                </div>

                                <div class="mt-5 col-12"></div>

                                <div class="mt-5 mb-5 col-md-6">
                                    <div style="background-color:#212529; padding: 1rem 0;">
                                        <label>
                                            <input type="checkbox" name="brRegisterTerms" ng-model="brRegisterTerms"
                                                id="br-register-terms" required
                                                class="nes-checkbox is-dark { 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterTerms.$dirty) || (formRegistration.brRegisterTerms.$dirty && formRegistration.brRegisterTerms.$invalid) }" />
                                            <span>@lang('messages.term_and_conditions_one') <a href="/site/terminos-y-condiciones"
                                                    target="_blank">
                                                    @lang('messages.term_and_conditions_two') </a> & <a href="/site/politica-de-privacidad"
                                                    target="_blank"> Política de
                                                    privacidad</a></span>
                                        </label>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div style="background-color:#212529; padding: 1rem 0;">
                                        <label>
                                            <input type="checkbox" name="brRegisterYearsOld"
                                                ng-model="brRegisterYearsOld" id="br-register-years-old" required
                                                class="nes-checkbox is-dark { 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterYearsOld.$dirty) || (formRegistration.brRegisterYearsOld.$dirty && formRegistration.brRegisterYearsOld.$invalid) }" />
                                            <span>{{ __('Tengo mas de 18 años') }}</span>
                                        </label>
                                    </div>


                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-10">
                                    <a href="{{ url('register-company') }}"><span class="retro-sell">
                                            <font color="white">{{ __('Desea registrar una cuenta empresarial') }}?
                                            </font>
                                        </span></a>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-lg-10">
                                    <span style="color:red" ng-show="ctrl.formError">
                                        <span ng-bind-html="ctrl.formErrorMessage"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6 ">
                                    <button type="submit"
                                        class="float-right retro-sell nk-btn nk-btn-rounded nk-btn-color-white nk-btn-block"><i
                                            class="fa fa-user-plus"></i> {{ __('Registrarse') }}</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
        <!-- END: Login Modal -->
    @endif
@endif



@yield('content-script-include')



<!--All Js JAVENIST Here-->
<!--Jquery 1.12.4 ESTO HACE QUE EL CARRITO NO FUNCIONE-->

<!-- Version Mobile -->

<script src="{{ asset('js/mobile/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/mobile/waypoints.min.js') }}"></script>
<script src="{{ asset('js/mobile/jquery.easing.min.js') }}"></script>
<script src="{{ asset('js/mobile/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/mobile/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/mobile/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('js/mobile/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('js/mobile/default/jquery.passwordstrength.js') }}"></script>
<script src="{{ asset('js/mobile/wow.min.js') }}"></script>
<script src="{{ asset('js/mobile/jarallax.min.js') }}"></script>
<script src="{{ asset('js/mobile/jarallax-video.min.js') }}"></script>
<script src="{{ asset('js/mobile/default/dark-mode-switch.js') }}"></script>
<script src="{{ asset('js/mobile/default/active.js') }}"></script>
<script src="{{ asset('js/mobile/base.js') }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://scripts.sirv.com/sirvjs/v3/sirv.js"></script>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<script>
    window.addEventListener('load', function() {
        // Muestra la notificación
        notification('notification-1');

        // Oculta el botón
        var btnNotification = document.querySelector('.btn-notification');
        btnNotification.style.display = 'none';
    });
</script>
<!-- Agrega el script para el funcionamiento del botón de mostrar/ocultar contraseña -->
<script>
    // Obtén el botón de mostrar/ocultar contraseña
    var passwordToggle = document.querySelector('.password-toggle');
    // Obtén el campo de contraseña
    var passwordInput = document.getElementById('password');

    // Agrega un evento de clic al botón de mostrar/ocultar contraseña
    passwordToggle.addEventListener('click', function() {
        // Cambia el tipo de input de contraseña a texto o viceversa
        var type = passwordInput.getAttribute('type') === 'password' ? 'text' : 'password';
        passwordInput.setAttribute('type', type);
        // Cambia el icono del botón de mostrar/ocultar contraseña
        var icon = passwordToggle.querySelector('i');
        icon.classList.toggle('fa-eye');
        icon.classList.toggle('fa-eye-slash');
    });
</script>

<script>
    // Obtén el botón de mostrar/ocultar contraseña para el registro
    var passwordToggleRegister = document.querySelector('.password-toggle-register');
    // Obtén el campo de contraseña para el registro
    var passwordInputRegister = document.getElementById('register_password');

    // Agrega un evento de clic al botón de mostrar/ocultar contraseña para el registro
    passwordToggleRegister.addEventListener('click', function() {
        // Cambia el tipo de input de contraseña a texto o viceversa para el registro
        var type = passwordInputRegister.getAttribute('type') === 'password' ? 'text' : 'password';
        passwordInputRegister.setAttribute('type', type);
        // Cambia el icono del botón de mostrar/ocultar contraseña para el registro
        var icon = passwordToggleRegister.querySelector('i');
        icon.classList.toggle('fa-eye');
        icon.classList.toggle('fa-eye-slash');
    });

    // Agrega un evento de clic al icono de borrar el contenido del campo de contraseña
    var clearInput = document.querySelector('.clear-input ion-icon');
    clearInput.addEventListener('click', function() {
        passwordInputRegister.value = '';
    });
</script>

<script>
    window.addEventListener('show-session', event => {
        document.getElementById("session").click();
    })
</script>

<script>
    // Trigger welcome notification after 5 seconds
    setTimeout(() => {
        notification('notification-welcome', 10000);
    }, 2000);
</script>
<script>
    // Trigger welcome notification after 5 seconds
    setTimeout(() => {
        notification('cookies-box', 10000);
    }, 2000);
</script>
<script>
    window.setMobileTable = function(selector) {
        // if (window.innerWidth > 600) return false;
        const tableEl = document.querySelector(selector);
        const thEls = tableEl.querySelectorAll('thead th');
        const tdLabels = Array.from(thEls).map(el => el.innerText);
        tableEl.querySelectorAll('tbody tr').forEach(tr => {
            Array.from(tr.children).forEach(
                (td, ndx) => td.setAttribute('label', tdLabels[ndx])
            );
        });
    }

    $(function() {
        $('[data-toggle="popover"]').popover()
    })
</script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>

<script src="{{ asset('assets/dropzone-master/dist/min/dropzone.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/quagga/0.12.1/quagga.js"></script>
<script src="{{ asset('vendor/sweetalert2/sweetalert2.all.min.js') }}"></script>


<script src="{{ asset('assets/noty/packaged/jquery.noty.packaged.min.js') }}"></script>
<script src="{{ asset('assets/select2/select2.min.js') }}"></script>

<!-- Angular JS -->
<script src="{{ asset('assets/angularjs/angular.min.js') }}"></script>
<script src="{{ asset('assets/angularjs/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('assets/angularjs/ui-bootstrap-tpls-2.5.0.min.js') }}"></script>
<script src="{{ asset('brcode/js/app.public.angjs-module.js') }}"></script>
<script src="{{ asset('brcode/js/app.public.angjs-topsearch.js') }}"></script>
<script src="{{ asset('brcode/js/app.public.angjs-register.js') }}"></script>

<script src="{{ asset('assets/cookie/cookiealert.js') }}"></script>

<script src="{{ asset('assets/imageSelect/js/chosen.jquery.js') }}"></script>
<script src="{{ asset('assets/imageSelect/js/ImageSelect.jquery.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<script>
    $(".nk-lang-change").chosen({
        width: '100%'
    });
</script>
<script type="text/javascript">
    $('#reload').click(function() {
        $.ajax({
            type: 'GET',
            url: '/reload-captcha',
            success: function(data) {
                $(".captcha span").html(data.captcha);
            }
        });
    });
</script>
@if (Auth::user())
    <script>
        $('.nk-cart-toggle').click(function() {
            location.href = "/cart";
        });
    </script>
@endif
<script></script>
<script>
    $(document).ready(function() {


        // $('#registerForm').modal('show');
        // $('.bropdown-back > a').click();
        if ('<?= session('
            message_confirm ') ?>' != '') {
            $.notify({
                message: '<?= session('
                message_confirm ') ?>'
            }, {
                // settings
                type: '<?= session('
                type_msg ') ?>',
                placement: {
                    from: "bottom",
                    align: "right"
                },
            });
        }

    });
</script>

<script>
    $(function() {
        var globalMsg = '<?= session('
        message ') ?>';
        var xhr = null;
        if (globalMsg.length > 0) {
            var globalMsgType = '<?= session('
            messageType ') ?>';
            var buttons = [];

            if (globalMsgType == 'warning') {
                buttons = [{
                    addClass: 'btn btn-primary btn-sm text-center col-md-12',
                    text: 'OK',
                    onClick: function($noty) {
                        $noty.close();
                    }
                }, ]
            }
            presentNotyMessage(globalMsg, globalMsgType, buttons);

        }

    });

    function presentNotyMessage(message, type, buttons, max) {

        buttons = typeof buttons == undefined ? [] : buttons;
        max = typeof max == undefined ? 5 : max;

        notySettings = {
            layout: 'topCenter',
            text: message,
            theme: 'relax',
            type: type,
            maxVisible: max,
            timeout: buttons.length == 0 ? 4000 : false,
        }

        if (buttons.length > 0) {
            notySettings.buttons = buttons;
        }

        var n = noty(notySettings);
    }
</script>
@if (Auth::user())
    <script>
        function salir() {
            var url = event.currentTarget.dataset.href;
            window.location.href = url;
        }
    </script>

    <script>
        // $('#vender').click();
        $(".select2-ajax").select2({
            minimumInputLength: 3,
            width: '100%',
            // tags: [],
            ajax: {
                url: "{{ route('ajaxProduct') }}",
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function(term) {
                    return {
                        term: term
                    };
                },
                processResults: function(data) {
                    console.log(data);
                    return {
                        results: $.map(data, function(item) {

                            return {
                                text: `${item.name} - ${item.platform}  - ${item.region} `,
                                id: item.id,
                            }
                        })
                    };
                }
            }
        });

        $(".select2-ajax").change(function() {
            if ($(this).val()) {
                $('#data-sub').html(
                    `<iframe src="https://www.retrogamingmarket.eu/account/inventory/add/${$(this).val()}?side=1" height="900px" width="100%" frameBorder="0"></iframe>`
                );
            }
        });
    </script>
@endif



<script>
    function lang_change() {
        location.href = $('.nk-lang-change').val();
    }
</script>



@yield('content-script-include-ang')

<script src="{{ asset('brcode/js/app.public.js') }}"></script>

@yield('content-script')

</body>

</html>
