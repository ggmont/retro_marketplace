<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> {{ config('brcode.app_name_public') }}</title>

    <meta name="description" content="Retro Gaming Market - El marketplace especialista en videojuegos">
    <meta name="keywords" content="game, retrogaming, gaming, Retro Gaming, Market, Retro">
    <meta name="author" content="RetroGamingMarket">
    <link rel="apple-touch-icon" href="{{ asset('img/RGM.png') }}">
    <link rel="manifest" href="{{ asset('/manifest.json') }}">
    <link rel="icon" type="image/png" href="{{ asset('img/RGM.png') }}">
    @livewireStyles


    <style>
        .retro {
            font-family: 'Jost', sans-serif;
        }

        .retro-prueba {
            font-family: 'Jost', sans-serif;
        }

        .retro-all {
            font-family: 'Jost', sans-serif;
            font-size: 17px;
            font-weight: 700;
        }

        .retro-sell {
            font-family: 'Jost', sans-serif;
            font-weight: 800;
        }

        .menu-retro {
            font-family: 'Rubik', sans-serif;
        }
    </style>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Version Mobile -->
    <link href="https://cdn.lineicons.com/3.0/lineicons.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="{{ asset('css/flag-icon.css') }}">
    <!-- Fancybox CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <style>
            .sicker {
                position: absolute;
                left: 50%;
                top: 10%;
                width: 137px;
                transform: translate(-50%, -50%);
                background: #00000094;
                color: #fff;
                line-height: 20px;
                border-radius: 2px;
                text-align: center;
                font-size: 12px;
                z-index: 991;
            }

            /*
      Rollover Image
     */
            .figure {
                position: relative;
                width: 360px;
                /* can be omitted for a regular non-lazy image */
                max-width: 100%;
            }

            .figure img.image-hover {
                position: absolute;
                top: 0;
                right: 0;
                left: 0;
                bottom: 0;
                object-fit: contain;
                opacity: 0;
                transition: opacity .2s;
            }

            .figure:hover img.image-hover {
                opacity: 1;
            }

            .modal-header button.close {
                position: absolute;
                right: 25px;
                opacity: 1;
                -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
                width: 30px;
                height: 30px;
                border: 1px solid #808080;
                border-radius: 5px;
                font-size: 16px;
                line-height: 25px;
                -webkit-transition: all 0.3s ease-in-out;
                transition: all 0.3s ease-in-out;
                z-index: 99;
                color: #808080;
                padding: 0;
            }

            .Buttons__login-btn {
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                position: relative;
                border: none;
                text-align: center;
                line-height: 34px;
                white-space: nowrap;
                border-radius: 21px;
                font-size: 1rem;
                color: #fff;
                width: 100%;
                height: 42px;
                display: -webkit-flex;
                display: -ms-flexbox;
                display: flex;
                -webkit-align-items: center;
                -ms-flex-align: center;
                align-items: center;
                -webkit-justify-content: center;
                -ms-flex-pack: center;
                justify-content: center;
                padding: 0;
                cursor: pointer;
            }

            .Buttons__login-btn--google {
                background: #dd4b39;
            }

            .prueba {
                position: relative;
                width: 100%;
                min-height: 100vh;
                z-index: 10;
                overflow-y: auto;
                padding-top: 1rem;
                padding-bottom: 1rem;
                overflow-x: hidden;
            }

            .w-full-ultra {
                width: 115%
            }


            .btn-ultra {
                /* height: 40px; */
                padding: 3px 18px;
                font-size: 13px;
                line-height: 1.2em;
                font-weight: 500;
                box-shadow: none !important;
                display: inline-flex;
                align-items: center;
                justify-content: center;
                transition: none;
                text-decoration: none !important;
                border-radius: 6px;
                border-width: 2px;
            }

            .btn-edit {
                /* height: 40px; */
                padding: 3px 5px;
                font-size: 10px;
                line-height: 1.2em;
                font-weight: 500;
                box-shadow: none !important;
                display: inline-flex;
                align-items: center;
                justify-content: center;
                transition: none;
                text-decoration: none !important;
                border-radius: 6px;
                border-width: 2px;
            }

            .form-control-ultra {
                display: block;
                width: 120%;
                padding: 0.375rem 0.75rem;
                font-size: 1rem;
                font-weight: 400;
                line-height: 1.5;
                color: #212529;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                border-radius: 0.25rem;
                -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            }
        </style>

        <!-- Dropzone css -->
        <link href="/assets/dropzone-master/dist/min/dropzone.min.css" rel="stylesheet" type="text/css" />
        <style>
            .dropzone .dz-preview .dz-error-message {
                top: 175px !important;
            }

            .searchbox .form-control-chat {
                height: 36px;
                border-radius: 6px;
                border: 1px solid #E1E1E1 !important;
                padding: 0 16px 0 36px;
                font-size: 15px;
                box-shadow: none !important;
                color: #141515;
            }

            .searchbox .form-control-chat:focus {
                border-color: #c8c8c8 !important;
            }

            .searchbox .form-control-chat:focus~.input-icon {
                color: #141515;
            }

            .form-control-chat {
                background-clip: padding-box;
                background-image: linear-gradient(transparent, transparent);
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
            }

            .form-group.basic .form-control-chat,
            .form-group.basic .custom-select {
                background: transparent;
                border: none;
                border-bottom: 1px solid #E1E1E1;
                padding: 0 30px 0 0;
                border-radius: 0;
                height: 40px;
                color: #141515;
                font-size: 15px;
            }

            .form-group.basic .form-control-chat:focus,
            .form-group.basic .custom-select:focus {
                border-bottom-color: #1E74FD;
                box-shadow: inset 0 -1px 0 0 #1E74FD;
            }

            .form-group.basic textarea.form-control-chat {
                height: auto;
                padding: 7px 40px 7px 0;
            }

            .form-group.boxed .form-control-chat.form-select,
            .form-group.basic .form-control-chat.form-select {
                background-image: url("data:image/svg+xml,%0A%3Csvg width='13px' height='8px' viewBox='0 0 13 8' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'%3E%3Cg id='Page-1' stroke='none' stroke-width='1' fill='none' fill-rule='evenodd' stroke-linecap='round' stroke-linejoin='round'%3E%3Cpolyline id='Path' stroke='%23A9ABAD' stroke-width='2' points='1.59326172 1.79663086 6.59326172 6.79663086 11.5932617 1.79663086'%3E%3C/polyline%3E%3C/g%3E%3C/svg%3E") !important;
                background-repeat: no-repeat !important;
                background-position: right center !important;
            }

            .form-group.boxed .form-control-chat.form-select {
                background-position: right 12px center !important;
            }

            .chatFooter .form-group .form-control-chat {
                font-size: 13px;
                border-radius: 300px;
                height: 40px;
            }

            .searchbox .form-control-chat {
                height: 36px;
                border-radius: 6px;
                border: 1px solid #E1E1E1 !important;
                padding: 0 16px 0 36px;
                font-size: 15px;
                box-shadow: none !important;
                color: #141515;
            }

            .searchbox .form-control-chat:focus {
                border-color: #c8c8c8 !important;
            }

            .searchbox .form-control-chat:focus~.input-icon {
                color: #141515;
            }

            #search .searchbox .form-control-chat {
                box-shadow: none !important;
                border: 0 !important;
                border-radius: 0;
                height: 56px;
                padding: 0 56px 0 56px;
                background: transparent;
                font-size: 17px;
                color: #141515;
                width: 100%;
            }

            #search .searchbox .form-control-chat:focus {
                border-color: #bbbbbb;
            }

            #search .searchbox .form-control-chat:focus~.input-icon {
                color: #141515;
            }

            .form-control-chat.is-valid,
            .was-validated .form-control-chat:valid {
                border-color: #34C759;
                box-shadow: 0 !important;
                background-image: none !important;
            }

            .form-control-chat.is-valid:focus,
            .was-validated .form-control-chat:valid:focus {
                border-color: #34C759;
                box-shadow: none !important;
            }

            .form-control-chat.is-invalid,
            .was-validated .form-control-chat:invalid {
                border-color: #EC4433;
                background-image: none !important;
            }

            .form-control-chat.is-invalid:focus,
            .was-validated .form-control-chat:invalid:focus {
                border-color: #EC4433;
                box-shadow: none !important;
            }

            .form-control-chat {
                display: block;
                width: 90%;
                padding: 0.375rem 0.75rem;
                font-size: 1rem;
                font-weight: 400;
                line-height: 1.5;
                color: #212529;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                border-radius: 0.25rem;
                -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            }

            img {
                max-width: 100%;
                height: auto;
            }

            .card-product-image-holder .card-product-image {
                object-fit: cover;
                height: 100%;
                width: 100%;
                -webkit-border-radius: 6px;
                border-radius: 6px;
            }

            .card-product-image-holder {
                object-fit: cover;
                width: 100%;
                max-width: 400px;
                height: 250px;
                border-radius: 5px;
                overflow: hidden;
                position: relative;
                margin-bottom: 1px;
            }

            .element {
                display: inline-flex;
                align-items: center;
            }

            svg.bi bi-camera {
                margin: 10px;
                cursor: pointer;
                font-size: 30px;
            }

            svg:hover {
                opacity: 0.6;
            }
        </style>
        <link rel="stylesheet" href="{{ asset('css/mobile/core.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/affan.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/mobilekit.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/splide.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/animate.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
            integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
            crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/nes/nes_prueba.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/select2/select2.min.css') }}">
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <meta name="google-site-verification" content="uOvlTBBjltn6jz3EfQV11zsAEEHusnIZ9h-Buylgk6s" />
        <link rel="stylesheet" href="{{ asset('assets/cookie/cookiealert.css') }}">
        <!-- Custom Styles -->
        <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">

        <link rel="stylesheet" href="{{ asset('assets/imageSelect/css/chosen.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/imageSelect/css/ImageSelect.css') }}">

        <link rel="stylesheet" href="{{ asset('assets/select2/select2.min.css') }}">
        @yield('content-css-include')
    @else
        <link rel="stylesheet" href="{{ asset('css/menu.css') }}">

        <!-- NES CSS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/nes/nes_prueba.css') }}">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="{{ asset('assets/css/desk.min.css') }}">

        <!-- FontAwesome -->
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}">

        <!-- IonIcons -->
        <link rel="stylesheet" href="{{ asset('assets/vendor/ionicons/css/ionicons.min.css') }}">

        <!-- Seiyria Bootstrap Slider -->
        <link rel="stylesheet"
            href="{{ asset('assets/vendor/bootstrap-slider/dist/css/bootstrap-slider.min.css') }}">

        <!-- GoodGames -->
        <link rel="stylesheet" href="{{ asset('assets/css/goodgames.css') }}">

        <!-- Custom Styles -->
        <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">

        <link rel="stylesheet" href="{{ asset('assets/imageSelect/css/chosen.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/imageSelect/css/ImageSelect.css') }}">

        <link rel="stylesheet" href="{{ asset('assets/select2/select2.min.css') }}">
        @yield('content-css-include')
        <!-- END: Styles -->
        <link rel="stylesheet" href="{{ asset('assets/cookie/cookiealert.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/flag-icon/css/flag-icon.min.css') }}">
        <!-- Prueba -->
        <!-- Animate CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
        <!-- UI CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.min.css') }}">
        <!-- Chosen CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/chosen.css') }}">
        <!-- Meanmenu CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/meanmenu.min.css') }}">
        <!-- Normalize CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}">
        <!-- Nivo Slider CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/nivo-slider.css') }}">
        <!-- Owl Carousel CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
        <!-- EasyZoom CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/easyzoom.css') }}">
        <!-- Slick CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/slick.css') }}">
        <!-- Default CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/default.css') }}">
        <!-- Style CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <meta name="google-site-verification" content="uOvlTBBjltn6jz3EfQV11zsAEEHusnIZ9h-Buylgk6s" />
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
        <!-- jQuery -->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('headroom/headroom.min.js') }}"></script>
        <script src="{{ asset('js/menu.js') }}"></script>
    @endif
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-9983858812639448"
        crossorigin="anonymous"></script>
    <style>
        .captcha img {
            width: 250px;
        }

        .ulti {
            max-width: 100%;
            height: 15px;
        }

        .ultri {
            max-width: 11%;
            height: 15px;
        }

        .reload {
            font-family: Lucida Sans Unicode
        }


        .footer.content>img,
        .footer.content>p {
            display: inline-block;
        }


        .profesional {
            background: radial-gradient(ellipse farthest-corner at right bottom, #FEDB37 0%, #FDB931 8%, #9f7928 30%, #8A6E2F 40%),
                radial-gradient(ellipse farthest-corner at left top, #FFFFFF 0%, #FFFFAC 8%, #D1B464 25%, #5d4a1f 62.5%, #5d4a1f 100%);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }

        .the_register {
            max-width: 500px;
            margin: auto;
            text-align: center;
        }

        .flexbox {
            display: flex;
        }

        .flexbox .stretch {
            flex: 1;
        }

        .flexbox .normal {
            flex: 0;
            margin: 0 0 0 1rem;
        }

        .flexbox div input {
            padding: .5em 1em;
            width: 100%;
        }

        .flexbox .listen {
            width: 92%;
        }

        .flexbox div button {
            padding: .5em 1em;
            white-space: nowrap;
        }

        .edition {
            padding-left: 25px;
            background: url("/img/pencil.png") no-repeat center #ffffff;
            background-size: 14px;
            padding-top: 10px;
        }

        .form-control-prueba {
            position: absolute;
            width: 30px;
            height: 30px;
            right: 20px;
            border: 0;
            border-radius: 50%;
            padding: 0;
            line-height: 30px;
            text-indent: -100000000000000020rem;
            background-color: #0d5afd;
        }
    </style>
    <!-- PWA -->
    @laravelPWA
</head>

@if ((new \Jenssegers\Agent\Agent())->isMobile())
    <div id="loader">
        <div class="spinner-border text-danger" role="status"></div>
    </div>
    <div id="notification-6" class="notification-box">
        <div class="notification-dialog android-style">
            <div class="notification-header">
                <div class="in">
                    <img src="{{ asset('img/RGM.png') }}" alt="image" class="imaged w24 rounded">
                    <strong>Notificación automática</strong>
                    <span>Justo Ahora</span>
                </div>
                <a href="#" class="close-button">
                    <ion-icon name="close"></ion-icon>
                </a>
            </div>
            <div class="notification-content">
                <div class="in">
                    <h3 class="subtitle">Listo!</h3>
                    <div class="text">
                        Cambio realizado con éxito
                    </div>
                </div>
                <div class="icon-box text-success">
                    <ion-icon name="checkmark-circle-outline"></ion-icon>
                </div>
            </div>
        </div>
    </div>

    <div id="notification-7" class="notification-box">
        <div class="notification-dialog android-style">
            <div class="notification-header">
                <div class="in">
                    <img src="{{ asset('img/RGM.png') }}" alt="image" class="imaged w24 rounded">
                    <strong>Notificación automática</strong>
                    <span>Justo Ahora</span>
                </div>
                <a href="#" class="close-button">
                    <ion-icon name="close"></ion-icon>
                </a>
            </div>
            <div class="notification-content">
                <div class="in">
                    <h3 class="subtitle">Error!</h3>
                    <div class="text">
                        La red social no coincide con el campo
                    </div>
                </div>
                <div class="icon-box text-danger">
                    <ion-icon name="close-circle-outline"></ion-icon>
                </div>
            </div>
        </div>
    </div>

    @if (Auth::user())
        <div class="offcanvas offcanvas-start" tabindex="-1" id="sidebarPanel">
            <div class="offcanvas-body">
                <!-- profile box -->
                <div class="profileBox">
                    <div class="image-wrapper">
                        @if (auth()->user()->profile_picture)
                            <img src="{{ url(auth()->user()->profile_picture) }}" alt="image"
                                class="imaged rounded">
                        @else
                            <img src="{{ asset('img/profile-picture-not-found.png') }}" alt="image"
                                class="imaged rounded">
                        @endif
                    </div>
                    <div class="in">
                        <strong>{{ auth()->user()->user_name }}</strong>
                        <div class="text-muted">
                            € {{ auth()->user()->cash }} - € {{ auth()->user()->special_cash }}
                        </div>
                    </div>
                </div>
                <!-- * profile box -->

                <ul class="listview link-listview">
                    @if (App\SysUserRoles::where('user_id', Auth::id())->whereIn('role_id', [1])->count() > 0)
                        <li>
                            <a href="{{ url('/11w5Cj9WDAjnzlg0') }}" class="item">
                                <div class="in">
                                    <div> @lang('messages.panel_control')</div>
                                </div>
                            </a>
                        </li>
                    @endif
                    <li>
                        <a href="/listing/users">
                            @lang('messages.list_user')
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('offer_index') }}">
                            @lang('messages.show_offer')
                        </a>
                    </li>
                    <li>
                        <a href="/cart">
                            @lang('messages.shopping_cart')
                            @if(Auth::user())
                            {{ Auth::user()->lifetimeCart }}

                            <span class="badge badge-danger" id="br-cart-items">{{ $viewData['items_count'] }}</span>

                            @else
                                -
                            @endif
                        </a>
                    </li>
                    <li>
                        <a href="/alerts">
                            @lang('messages.alerts')
                            <span class="badge badge-danger">{{ count($viewData['header_newsletter']) }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="/notifications">
                            @lang('messages.notification')
                            @if (count($viewData['header_notifications']) > 0)
                                @if ($viewData['c_h_n'] > 0)
                                    <span class="badge badge-danger">{{ $viewData['c_h_n'] }}</span>
                                @endif
                            @endif
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('purchases_data') }}">
                            @lang('messages.the_purchase')



                        </a>
                    </li>
                    <li>
                        <a href="{{ route('sales_data') }}">
                            @lang('messages.the_sale')

                        </a>
                    </li>
                    <li>
                        <a href="{{ route('transaction_index') }}">
                            @lang('messages.the_transaction')

                        </a>
                    </li>
                    <li>
                        <a href="{{ route('collection_index') }}">
                            Mi colección
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/account/inventory') }}">
                            @lang('messages.the_inventory')
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" onclick="salir()" class="item" data-href="{{ url('/account/logout') }}">
                            <div class="in">
                                @lang('messages.get_out')
                            </div>
                        </a>
                    </li>
                </ul>

            </div>
            <!-- sidebar buttons -->
            <div class="sidebar-buttons">
                <a href="{{ route('information_index') }}" class="button">
                    <ion-icon name="help-outline"></ion-icon>
                </a>
                <a href="#" id="showlanguage" class="showlanguage button">
                    @if (App::getLocale() == 'es')
                        <span class="flag-icon flag-icon-esp"></span>
                    @elseif (App::getLocale() == 'en')
                        <span class="flag-icon flag-icon-gbr"></span>
                    @endif
                </a>

            </div>
            <!-- * sidebar buttons -->
        </div>
    @else
        <div class="offcanvas offcanvas-start" tabindex="-1" id="sidebarPanel">
            <div class="offcanvas-body">
                <!-- profile box -->
                <div class="profileBox">
                    <div class="image-wrapper">
                        <img src="{{ asset('img/profile-picture-not-found.png') }}" alt="image"
                            class="imaged rounded">
                    </div>
                    <div class="in">
                        <strong>Bienvenido a RGM!</strong>
                        <div class="text-muted">
                            RGM
                        </div>
                    </div>
                </div>
                <!-- * profile box -->

                <ul class="listview link-listview">
                    <li>
                        <a href="#" id="showLogin" class="item showLogin" data-nav-toggle="#nk-nav-mobile">
                            <div class="in">
                                <div>Iniciar Sesión / Registrarse</div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="/site/faqs">
                            FAQS
                        </a>
                    </li>
                    <li>
                        <a href="/site/terminos-y-condiciones">
                            Terminos y Condiciones
                        </a>
                    </li>
                    <li>
                        <a href="/site/terminos-y-condiciones">
                            Aviso Legal
                        </a>
                    </li>
                    <li>
                        <a href="/site/politica-de-privacidad">
                            Politica de Privacidad
                        </a>
                    </li>
                    <li>
                        <a href="/site/politica-de-cookies">
                            Politica de Cookies
                        </a>
                    </li>
                </ul>

            </div>
            <!-- sidebar buttons -->
            <div class="sidebar-buttons">
                <a href="#" id="showLogin" class="button showLogin" data-nav-toggle="#nk-nav-mobile">
                    <ion-icon name="person-outline"></ion-icon>
                </a>
                <a href="{{ route('information_index') }}" class="button">
                    <ion-icon name="help-outline"></ion-icon>
                </a>
                <a href="#" id="showlanguage" class="showlanguage button">
                    <span class="flag-icon flag-icon-esp"></span>
                </a>

            </div>
            <!-- * sidebar buttons -->
        </div>
    @endif

    <div class="modal fade" id="modalLanguage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog lang-form" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title">Selector de Idioma</h4>
                            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form action="/lang" class="m-3 ml-10 nk-form nk-form-style" id="nk-lang-form"
                                method="POST">
                                {{ csrf_field() }}
                                <select name="locale"
                                    class="form-control form-select p-1 pl-10 text-gray-900 pr-10 nk-lang-change"
                                    onchange="lang_change()" style="width: 100%;">
                                    <option data-img-src="{{ asset('assets/images/es/es.png') }}"
                                        value="/setlocale/es" {{ App::getLocale() == 'es' ? 'selected' : '' }}>
                                        Español
                                    </option>
                                    <option data-img-src="{{ asset('assets/images/en/en.png') }}"
                                        value="/setlocale/en" {{ App::getLocale() == 'en' ? 'selected' : '' }}>
                                        English
                                    </option>
                                    @if (App\SysUserRoles::where('user_id', Auth::id())->whereIn('role_id', [1])->count() > 0)
                                        <option data-img-src="{{ asset('assets/images/fr/fr.png') }}"
                                            value="/setlocale/fr" {{ App::getLocale() == 'fr' ? 'selected' : '' }}>
                                            Francés
                                        </option>
                                        <option data-img-src="{{ asset('assets/images/ita/ita.png') }}"
                                            value="/setlocale/ita" {{ App::getLocale() == 'ita' ? 'selected' : '' }}>
                                            Italia
                                        </option>
                                        <option data-img-src="{{ asset('assets/images/al/al.png') }}"
                                            value="/setlocale/al" {{ App::getLocale() == 'al' ? 'selected' : '' }}>
                                            Alemania
                                        </option>
                                        <option data-img-src="{{ asset('assets/images/prt/prt.png') }}"
                                            value="/setlocale/prt" {{ App::getLocale() == 'prt' ? 'selected' : '' }}>
                                            Portugués
                                        </option>
                                    @endif
                                </select>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">


                    </div>
                </div>
            </div>
        </div>
    </div>
@else

    <body ng-app="appMarketPlace">

        <header class="nk-header nk-header-opaque">



            <div id="nk-nav-newProduct" class="nk-navbar nk-navbar-side nk-navbar-left-side hidden-lg-up">
                <div class="nano">
                    <div class="nano-content">
                        <br>
                        <h4 class="p-10 retro"><b>
                                <font color="white">{{ __('Vender un producto') }}</font>
                            </b>
                            <a href="#" data-nav-toggle="#nk-nav-newProduct" id="vender"
                                class="float-right nk-btn nk-btn-sm nk-btn-rounded nk-btn-color-main-1">
                                <i class="fa fa-times"></i>
                            </a>
                        </h4>

                        <div class="nk-navbar-mobile-content">
                            <ul class="p-10 nk-nav">
                                <li>
                                    <label class="font-semibold retro">
                                        <font color="#eee0e2">{{ __('Producto') }}</font>
                                    </label><br>
                                    <select name="product" id="product" class="select2-ajax"></select>
                                </li>
                                <li id="data-sub"> </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


        </header>
        <div class="wrapper home-7">
            <header>
                <div class="header-container">
                    <!--Header Top Area Start-->
                    <div class="header-top-area">
                        <div class="container">

                            <div class="row">
                                <!--Header Top Left Area Start-->
                                <div class="col-lg-5 col-md-4 col-12">

                                    <div class="header-top-menu">
                                        <form action="/lang" class="m-3 ml-10 nk-form nk-form-style"
                                            id="nk-lang-form" method="POST">
                                            {{ csrf_field() }}
                                            <select name="locale"
                                                class="p-1 pl-10 text-gray-900 pr-10 nk-lang-change"
                                                onchange="lang_change()" style="width: 40%;">
                                                <option data-img-src="{{ asset('assets/images/es/es.png') }}"
                                                    value="/setlocale/es"
                                                    {{ App::getLocale() == 'es' ? 'selected' : '' }}> Español
                                                </option>
                                                <option data-img-src="{{ asset('assets/images/en/en.png') }}"
                                                    value="/setlocale/en"
                                                    {{ App::getLocale() == 'en' ? 'selected' : '' }}> English
                                                </option>
                                                @if (App\SysUserRoles::where('user_id', Auth::id())->whereIn('role_id', [1])->count() > 0)
                                                    <option data-img-src="{{ asset('assets/images/fr/fr.png') }}"
                                                        value="/setlocale/fr"
                                                        {{ App::getLocale() == 'fr' ? 'selected' : '' }}> Francés
                                                    </option>
                                                    <option data-img-src="{{ asset('assets/images/ita/ita.png') }}"
                                                        value="/setlocale/ita"
                                                        {{ App::getLocale() == 'ita' ? 'selected' : '' }}> Italia
                                                    </option>
                                                    <option data-img-src="{{ asset('assets/images/al/al.png') }}"
                                                        value="/setlocale/al"
                                                        {{ App::getLocale() == 'al' ? 'selected' : '' }}> Alemania
                                                    </option>
                                                    <option data-img-src="{{ asset('assets/images/prt/prt.png') }}"
                                                        value="/setlocale/prt"
                                                        {{ App::getLocale() == 'prt' ? 'selected' : '' }}> Portugués
                                                    </option>
                                                @endif
                                            </select>
                                        </form>
                                        @if (App\SysUserRoles::where('user_id', Auth::id())->whereIn('role_id', [1])->count() > 0)
                                            @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                                <ul>
                                                    @yield('search-button')

                                                    @if (Auth::user())

                                                        @if (count($viewData['header_pay_pending']) > 0)
                                                            <li>
                                                                <a href="/account/purchases" class="___class_+?24___">
                                                                    <span class="fa fa-tag"></span>
                                                                    <span
                                                                        class="nk-badge">{{ count($viewData['header_pay_pending']) }}</span>
                                                                </a>
                                                            </li>
                                                        @endif
                                                        @if (count($viewData['header_send_pending']) > 0)
                                                            <li>
                                                                <a href="/account/sales#sent"
                                                                    class="___class_+?27___">
                                                                    <span class="fa fa-paper-plane-o"></span>
                                                                    <span
                                                                        class="nk-badge">{{ count($viewData['header_send_pending']) }}</span>
                                                                </a>
                                                            </li>
                                                        @endif
                                                        @if (count($viewData['header_notifications']) > 0)
                                                            <li>
                                                                <span class="nk-notifications-toggle">
                                                                    <span class="fa fa-bell"></span>
                                                                    @if ($viewData['c_h_n'] > 0)
                                                                        <span
                                                                            class="nk-badge">{{ $viewData['c_h_n'] }}</span>
                                                                    @endif
                                                                </span>
                                                                <div class="list-group nk-notifications-dropdown">

                                                                    @foreach ($viewData['header_notifications'] as $key)
                                                                        @php($stat = true)
                                                                        @if ($key->paid_out == 'N' || $key->paid_out == 'P')
                                                                            @php($stat = false)
                                                                        @else
                                                                            @php($stat = true)
                                                                        @endif

                                                                        @if ($key->tipoSeller)
                                                                            <a href="/account/order/{{ $key->order_identification }}"
                                                                                class="list-group-item bg-dark hover:border-teal-100{{ $key->seller_read == 'N' ? -1 : '' }}">

                                                                                <span class="text-base text-white">

                                                                                    @if ($key->status == 'DD')
                                                                                        {{ __('El pedido fue entregado') }}
                                                                                    @endif

                                                                                    @if ($key->status == 'RP')
                                                                                        {{ __('Un pedido requiere su atención') }}
                                                                                    @endif

                                                                                    @if ($key->status == 'CR')
                                                                                        {{ __('Tiene un pedido pendiente por enviar') }}
                                                                                    @endif

                                                                                    @if ($key->status == 'CW')
                                                                                        {{ __('Tiene una solicitud de cancelación de pedido') }}
                                                                                    @endif

                                                                                    @if ($key->status == 'PC')
                                                                                        {{ __('Tiene una solicitud de cancelación de un pedido pagado') }}
                                                                                    @endif

                                                                                </span> <br>
                                                                                <p class="nk-notifications-box">
                                                                                    <span
                                                                                        class="fa fa-clock-o text-main-1"></span>
                                                                                    <span
                                                                                        class="text-white">{{ \Carbon\Carbon::parse($key->updated_at)->diffForHumans() }}
                                                                                    </span>
                                                                                    <span
                                                                                        class="fa fa-shopping-bag"></span>
                                                                                    <span
                                                                                        class="text-white font-weight-bold">{{ count($key->details) }}
                                                                                    </span>
                                                                                </p>


                                                                            </a>
                                                                        @else
                                                                            <a href="/account/order/{{ $key->order_identification }}"
                                                                                class="list-group-item bg-dark hover:border-teal-100{{ $key->buyer_read == 'N' ? -1 : '' }}">

                                                                                <span class="text-base text-white">
                                                                                    @if ($key->status == 'RP')
                                                                                        {{ __('Tiene un pedido pendiente por pagar') }}
                                                                                    @endif
                                                                                    @if ($key->status == 'ST')
                                                                                        {{ __('Tu pedido ha sido enviado. No olvides confirmar la llegada.') }}
                                                                                    @endif
                                                                                </span> <br>
                                                                                <p class="nk-notifications-box">
                                                                                    <span
                                                                                        class="fa fa-clock-o text-main-1"></span>
                                                                                    <span
                                                                                        class="text-white">{{ \Carbon\Carbon::parse($key->updated_at)->diffForHumans() }}
                                                                                    </span>
                                                                                    <span
                                                                                        class="fa fa-shopping-cart"></span>
                                                                                    <span
                                                                                        class="text-white font-weight-bold">{{ count($key->details) }}
                                                                                    </span>
                                                                                </p>
                                                                            </a>
                                                                        @endif
                                                                    @endforeach
                                                                </div>

                                                            </li>
                                                        @endif


                                                    @endif

                                                    @if (Auth::user())
                                                        <li>
                                                            <a href="/account/messages">
                                                                <span class="fa fa-envelope"></span>
                                                                @isset($viewData['header_messages'])
                                                                    @if (count($viewData['header_messages']) > 0)
                                                                        <span
                                                                            class="nk-badge">{{ count($viewData['header_messages']) }}</span>
                                                                    @endif
                                                                @endisset
                                                            </a>
                                                        </li>
                                                    @endif
                                                    @if (!Auth::user())
                                                        <li>
                                                            <a href="#" id="showLogin" class="showLogin">

                                                                <span>@lang('messages.sesion_register')</span>

                                                            </a>
                                                        </li>
                                                    @endif
                                                    <li>
                                                        <span class="nk-cart-toggle">
                                                            @if (Auth::user())
                                                                {{ Auth::user()->lifetimeCart }}
                                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                                <span class="nk-badge"
                                                                    id="br-cart-items">{{ $viewData['items_count'] }}</span>
                                                            @else
                                                                <i class="fa fa-shopping-cart showLogin"
                                                                    aria-hidden="true"></i>
                                                            @endif
                                                        </span>
                                                    </li>
                                                </ul>
                                            @endif
                                        @endif
                                    </div>

                                </div>
                                <!--Header Top Left Area End-->
                                <!--Header Top Right Area Start-->
                                <div class="text-right col-lg-7 col-md-7 d-lg-block d-md-block d-none">
                                    <div class="header-top-menu">
                                        <ul>
                                            @yield('search-button')

                                            @if (Auth::user())

                                                @if (count($viewData['header_pay_pending']) > 0)
                                                    <li>
                                                        <a href="/account/purchases" class="___class_+?24___">
                                                            <span class="fa fa-tag"></span>
                                                            <span
                                                                class="nk-badge">{{ count($viewData['header_pay_pending']) }}</span>
                                                        </a>
                                                    </li>
                                                @endif
                                                @if (count($viewData['header_send_pending']) > 0)
                                                    <li>
                                                        <a href="/account/sales#sent" class="___class_+?27___">
                                                            <span class="fa fa-paper-plane-o"></span>
                                                            <span
                                                                class="nk-badge">{{ count($viewData['header_send_pending']) }}</span>
                                                        </a>
                                                    </li>
                                                @endif
                                                @if (count($viewData['header_notifications']) > 0)
                                                    <li>
                                                        <span class="nk-notifications-toggle">
                                                            <span class="fa fa-bell"></span>
                                                            @if ($viewData['c_h_n'] > 0)
                                                                <span
                                                                    class="nk-badge">{{ $viewData['c_h_n'] }}</span>
                                                            @endif
                                                        </span>
                                                        <div class="list-group nk-notifications-dropdown">

                                                            @foreach ($viewData['header_notifications'] as $key)
                                                                @php($stat = true)
                                                                @if ($key->paid_out == 'N' || $key->paid_out == 'P')
                                                                    @php($stat = false)
                                                                @else
                                                                    @php($stat = true)
                                                                @endif

                                                                @if ($key->tipoSeller)
                                                                    <a href="/account/order/{{ $key->order_identification }}"
                                                                        class="list-group-item bg-dark hover:border-teal-100{{ $key->seller_read == 'N' ? -1 : '' }}">

                                                                        <span class="text-base text-white">

                                                                            @if ($key->status == 'DD')
                                                                                {{ __('El pedido fue entregado') }}
                                                                            @endif

                                                                            @if ($key->status == 'RP')
                                                                                {{ __('Un pedido requiere su atención') }}
                                                                            @endif

                                                                            @if ($key->status == 'CR')
                                                                                {{ __('Tiene un pedido pendiente por enviar') }}
                                                                            @endif

                                                                            @if ($key->status == 'CW')
                                                                                {{ __('Tiene una solicitud de cancelación de pedido') }}
                                                                            @endif

                                                                            @if ($key->status == 'PC')
                                                                                {{ __('Tiene una solicitud de cancelación de un pedido pagado') }}
                                                                            @endif

                                                                        </span> <br>
                                                                        <p class="nk-notifications-box">
                                                                            <span
                                                                                class="fa fa-clock-o text-main-1"></span>
                                                                            <span
                                                                                class="text-white">{{ \Carbon\Carbon::parse($key->updated_at)->diffForHumans() }}
                                                                            </span>
                                                                            <span class="fa fa-shopping-bag"></span>
                                                                            <span
                                                                                class="text-white font-weight-bold">{{ count($key->details) }}
                                                                            </span>
                                                                        </p>


                                                                    </a>
                                                                @else
                                                                    <a href="/account/order/{{ $key->order_identification }}"
                                                                        class="list-group-item bg-dark hover:border-teal-100{{ $key->buyer_read == 'N' ? -1 : '' }}">

                                                                        <span class="text-base text-white">
                                                                            @if ($key->status == 'RP')
                                                                                {{ __('Tiene un pedido pendiente por pagar') }}
                                                                            @endif
                                                                            @if ($key->status == 'ST')
                                                                                {{ __('Tu pedido ha sido enviado. No olvides confirmar la llegada.') }}
                                                                            @endif
                                                                        </span> <br>
                                                                        <p class="nk-notifications-box">
                                                                            <span
                                                                                class="fa fa-clock-o text-main-1"></span>
                                                                            <span
                                                                                class="text-white">{{ \Carbon\Carbon::parse($key->updated_at)->diffForHumans() }}
                                                                            </span>
                                                                            <span class="fa fa-shopping-cart"></span>
                                                                            <span
                                                                                class="text-white font-weight-bold">{{ count($key->details) }}
                                                                            </span>
                                                                        </p>
                                                                    </a>
                                                                @endif
                                                            @endforeach
                                                        </div>

                                                    </li>
                                                @endif
                                                @if (count($viewData['product_request']) > 0)
                                                    <li>
                                                        <span class="nk-notifications-toggle">
                                                            <span class="fa fa-check-square"></span>
                                                            @if ($viewData['c_r_n'] > 0)
                                                                <span
                                                                    class="nk-badge">{{ $viewData['c_r_n'] }}</span>
                                                            @endif
                                                        </span>
                                                        <div class="list-group nk-notifications-dropdown">

                                                            @foreach ($viewData['product_request'] as $key)
                                                                <a href="{{ route('product-show', $key->product_id) }}"
                                                                    class="list-group-item bg-dark hover:border-teal-100{{ $key->user_read == 'N' ? -1 : '' }}">

                                                                    <span class="text-base text-white">


                                                                        {{ __('Tu Peticion del Producto Fue aprobada y tu inventario a sido actualizado') }}




                                                                    </span> <br>
                                                                    <p class="nk-notifications-box">
                                                                        <span class="fa fa-clock-o text-main-1"></span>
                                                                        <span
                                                                            class="text-white">{{ \Carbon\Carbon::parse($key->updated_at)->diffForHumans() }}
                                                                            -
                                                                        </span>
                                                                        <span class="fa fa-time"></span> <span
                                                                            class="text-white font-weight-bold">
                                                                        </span>
                                                                    </p>


                                                                </a>
                                                            @endforeach
                                                        </div>

                                                    </li>
                                                @endif
                                            @endif

                                            @if (Auth::user())
                                                <li>
                                                    <a href="/account/messages">
                                                        <span class="fa fa-envelope"></span>
                                                        @isset($viewData['header_messages'])
                                                            @if (count($viewData['header_messages']) > 0)
                                                                <span
                                                                    class="nk-badge">{{ count($viewData['header_messages']) }}</span>
                                                            @endif
                                                        @endisset
                                                    </a>
                                                </li>
                                            @endif
                                            @if (!Auth::user())
                                                <li>
                                                    <a href="#" id="showLogin" class="showLogin">

                                                        <span>@lang('messages.sesion_register')</span>

                                                    </a>
                                                </li>
                                            @endif
                                            <li>
                                                <span class="nk-cart-toggle">
                                                    @if (Auth::user())
                                                        {{ Auth::user()->lifetimeCart }}
                                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span
                                                            class="nk-badge"
                                                            id="br-cart-items">{{ $viewData['items_count'] }}</span>
                                                    @else
                                                        <i class="fa fa-shopping-cart showLogin"
                                                            aria-hidden="true"></i>
                                                    @endif
                                                </span>

                                            </li>
                                            @if (count($viewData['header_newsletter']) > 0)
                                                <li>
                                                    <span class="nk-news-toggle">
                                                        <i class="fa fa-list-ul"></i>
                                                        <span
                                                            class="nk-badge">{{ count($viewData['header_newsletter']) }}</span>
                                                    </span>

                                                    <div class="list-group nk-news-dropdown">

                                                        @foreach ($viewData['header_newsletter'] as $key)
                                                            <a href="#"
                                                                class="list-group-item bg-dark hover:border-teal-100">
                                                                <span
                                                                    class="text-white retro h6">{!! $key->title !!}</span>
                                                                <br>
                                                                <p class="text-justify text-white"
                                                                    style="margin: 0px 0px !important;">
                                                                    {!! $key->content !!}
                                                                </p>
                                                                <p class="text-white font-italic nk-notifications-box">
                                                                    <span
                                                                        class="fa fa-clock-o"></span>{{ \Carbon\Carbon::parse($key->created_at)->diffForHumans() }}
                                                                </p>
                                                            </a>
                                                        @endforeach
                                                    </div>
                                                </li>
                                            @endif




                                        </ul>
                                    </div>
                                </div>
                                <!--Header Top Right Area End-->
                            </div>
                        </div>
                    </div>
                    <!--Header Top Area End-->
                    <!--Header Middel Area Start-->
                    <div class="header-middel-area">
                        <div class="container">
                            <div class="row">
                                <!--Logo Start-->
                                <div class="col-lg-3 col-md-3 col-12">
                                    <div class="logo">
                                        <a href="/"><img src="{{ asset('img/logo.png') }}"
                                                alt=""></a>
                                    </div>
                                </div>
                                <!--Logo End-->
                                <!--Search Box Start-->
                                <div class="col-lg-6 col-md-5 col-12">
                                    <div class="search-box-area">
                                        <div class="mx-10">
                                            @livewire('product-search-bar')
                                        </div>
                                    </div>
                                </div>
                                <!--Search Box End-->
                                <!--Mini Cart Start-->
                                <div class="col-lg-3 col-md-4 col-12">
                                    <div class="mini-cart-area">
                                        <nav class="nk-navbar nk-navbar-top">
                                            <div class="container">
                                                <div class="nk-nav-table">

                                                    @if (Auth::id() > 0)
                                                        <ul class="nk-nav nk-nav-right d-none d-lg-table-cell"
                                                            data-nav-mobile="#nk-nav-mobile">

                                                            <li class="nk-drop-item">
                                                                <a href="#">
                                                                    <i class="fa fa-user"></i>
                                                                    {{ auth()->user()->user_name }} <i
                                                                        class="fa fa-chevron-down"></i><br>
                                                                    <small><i class="fa fa-euro"></i>
                                                                        {{ auth()->user()->cash }}</small> <span
                                                                        class="ml-10 mr-10"></span>
                                                                    <small><i class="fa fa-euro"></i>
                                                                        {{ auth()->user()->special_cash }}</small>
                                                                </a>
                                                                <ul class="dropdown">
                                                                    @if (App\SysUserRoles::where('user_id', Auth::id())->whereIn('role_id', [1])->count() > 0)
                                                                        <li>
                                                                            <a href="{{ url('/11w5Cj9WDAjnzlg0') }}">
                                                                                <i class="fa fa-gamepad"></i>
                                                                                @lang('messages.panel_control')
                                                                            </a>
                                                                        </li>
                                                                    @endif
                                                                    @if (App\SysUserRoles::where('user_id', Auth::id())->whereIn('role_id', [5, 2])->count() > 0)
                                                                        <li>
                                                                            <a href="{{ url('/SpecialZone') }}">
                                                                                <i class="fa fa-gamepad"></i>
                                                                                @lang('messages.panel_control')
                                                                            </a>
                                                                        </li>
                                                                    @endif
                                                                    <li>
                                                                        <a href="{{ url('/account/profile') }}">
                                                                            <i class="fa fa-cogs"></i>
                                                                            @lang('messages.configuration')
                                                                        </a>
                                                                    </li>

                                                                    <li>
                                                                        <a
                                                                            href="{{ url('/user/' . auth()->user()->user_name) }}">
                                                                            <i class="fa fa-user-circle "></i>
                                                                            @lang('messages.public_perfil')
                                                                        </a>
                                                                    </li>
                                                                    <!--
                                                            <li>
                                                                <a href="#" data-nav-toggle="#nk-nav-newProduct"
                                                                    id="vender">
                                                                    <i class="fa fa-shopping-bag"></i>
                                                                    {{ __('Vender') }}
                                                                </a>
                                                            </li>
                                                            -->
                                                                    <li>
                                                                        <a href="{{ url('/account/transactions') }}">
                                                                            <i class="fa fa-list"
                                                                                aria-hidden="true"></i>
                                                                            @lang('messages.transactions')
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="{{ url('/account/purchases') }}">
                                                                            <i class="fa fa-shopping-cart "></i>
                                                                            @lang('messages.purchases')
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="{{ url('/account/sales') }}">
                                                                            <i class="fa fa-truck "></i>
                                                                            @lang('messages.sales')
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="{{ url('/account/inventory') }}">
                                                                            <i class="fa fa-cubes "></i>
                                                                            @lang('messages.inventory')
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" onclick="salir()" class="item" data-href="{{ url('/account/logout') }}">
                                                                            <div class="in">
                                                                                @lang('messages.get_out')
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    @endif
                                                    @if (Auth::user())
                                                        <ul class="nk-nav nk-nav-right nk-nav-icons">
                                                            <li class="single-icon d-lg-none">
                                                                <a href="#" class="no-link-effect"
                                                                    data-nav-toggle="#nk-nav-mobile">
                                                                    <span class="fa fa-user">
                                                                        <span class="nk-t-1"></span>
                                                                        <span class="nk-t-2"></span>
                                                                        <span class="nk-t-3"></span>
                                                                    </span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    @else
                                                        <ul class="nk-nav nk-nav-right nk-nav-icons">
                                                            <li class="single-icon d-lg-none">
                                                                <a href="#" id="showLogin" class="showLogin"
                                                                    class="no-link-effect"
                                                                    data-nav-toggle="#nk-nav-mobile">
                                                                    <span class="fa fa-user">
                                                                        <span class="nk-t-1"></span>
                                                                        <span class="nk-t-2"></span>
                                                                        <span class="nk-t-3"></span>
                                                                    </span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    @endif
                                                </div>
                                            </div>
                                        </nav>
                                    </div>
                                </div>
                                <!--Mini Cart End-->
                            </div>
                        </div>
                    </div>
                    <!--Header Middel Area End-->
                    <!--Header bottom Area Start-->
                    <header id="header" class="header-bottom-area header-sticky prueba">
                        <nav class="menu">

                            <div class="text-lg enlaces menu-retro" id="enlaces">
                                <a href="/">@lang('messages.start')</a>
                                <a
                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Juegos">@lang('messages.games')</a>
                                <a
                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Consolas">@lang('messages.consoles')</a>
                                <a
                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Perif%C3%A9ricos">@lang('messages.peripherals')</a>
                                <a
                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Accesorios">@lang('messages.accesories')</a>
                                <a
                                    href="https://www.retrogamingmarket.eu/MarketPlaceStore?search_category=Merchandising">@lang('messages.merch')</a>
                                <a href="https://www.retrogamingmarket.eu/rgmblog">Blog</a>
                            </div>
                            <div class="log">
                                <a href="#" class="btn-menu" id="btn-menu"><i
                                        class="icono fa fa-bars"></i></a>
                            </div>
                        </nav>
                    </header>
                    <!--Header bottom Area End-->
                    <!--Mobile Menu Area Start-->

                    <!--Mobile Menu Area End-->
                </div>
            </header>
        </div>

        @if (Auth::user())
            <div class="modal fade bs-example-modal-lg" tabindex="-1" id="" role="dialog"
                aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title">{{ __('Salir') }}</h3>

                        </div>

                        <div class="modal-body" id="reload_logout" style="padding:10px">
                            <br>
                            There are items in your shopping cart. Are you sure you want to logout?
                            <br><br>

                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">Img</th>
                                        <th class="text-left">Product</th>
                                        <th class="text-left">Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (Auth::user()->cart->cartItems)
                                        @foreach (Auth::user()->cart->cartItems as $key)
                                            <tr>
                                                <td class="text-center">
                                                    <a href="product/{{ $key->product_info->id + 1000 }}">
                                                        <img class="img-thumbnail"
                                                            src="{{ count($key->product_info->product_images) > 0 ? url('/uploads/product-images/' . $key->product_info->product_images[0]->image_path) : url('/uploads/no-image.jpg') }}" />
                                                    </a>
                                                </td>
                                                <td class="text-left">
                                                    <a href="product/{{ $key->product_info->id + 1000 }}">
                                                        {{ $key->product_info->name }}
                                                    </a>
                                                </td>
                                                <td class="text-left">{{ number_format($key->price, 2) }} €
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div><br>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="ModalSingOut" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <font color="#e5f6fb">
                                {{ __('Hay artículos en su carrito de compras. ¿Estás seguro de que quieres cerrar sesión? Si la cierras se te quitaran estos productos del carrito') }}
                            </font>
                            <br><br>
                            <div class="text-white row">
                                <div class="col-md">{{ __('Imagen') }}</div>
                                <div class="col-md-6">{{ __('Producto') }}</div>
                                <div class="col-md">{{ __('Precio') }}</div>
                            </div>
                            <div class="nk-gap"></div>
                            @if (Auth::user()->cart->cartItems)
                                @foreach (Auth::user()->cart->cartItems as $key)
                                    <div class="row">
                                        <div class="col-md">
                                            <a href="{{ route('product-show', $key->product_info->id) }}">
                                                <img class="" width=" 50px"
                                                    src="{{ count($key->product_info->product_images) > 0 ? url('/images/' . $key->product_info->product_images[0]->image_path) : url('/uploads/no-image.jpg') }}" />
                                            </a>
                                        </div>
                                        <div class="col-md-6"><a
                                                href="{{ route('product-show', $key->product_info->id) }}">
                                                <font color="#e5f6fb">{{ $key->product_info->name }}</font>
                                            </a>
                                        </div>
                                        <div class="col-md"><span
                                                class="text-xs text-green-500 retro">{{ number_format($key->price, 2) }}
                                                €</span>
                                        </div>
                                    </div>
                                    <div class="nk-gap"></div>
                                @endforeach
                            @endif
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="nes-btn is-warning retro"
                                data-dismiss="modal">{{ __('Cancelar') }}</button>
                            <a href="{{ url('/account/logout') }}"
                                class="nes-btn is-error retro">{{ __('Salir') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (Auth::user())
            <div id="nk-nav-mobile"
                class="nk-navbar nk-navbar-side nk-navbar-right-side nk-navbar-overlay-content d-lg-none">
                <div class="nano">
                    <div class="nano-content">
                        <a href="/" class="nk-nav-logo">
                            <img src="{{ asset('img/logo.png') }}" alt="" width="120">
                        </a>
                        <div class="nk-navbar-mobile-content">
                            <ul class="nk-nav">
                                <!-- Here will be inserted menu from [data-mobile-menu="#nk-nav-mobile"] -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="nk-main">
            @if ((new \Jenssegers\Agent\Agent())->isMobile())
                <br>
                <div class="col-md-12">

                    <div
                        class="flex flex-row items-center p-5 bg-yellow-400 border-b-2 border-yellow-500 rounded alert">
                        <div
                            class="flex items-center justify-center flex-shrink-0 w-10 h-10 bg-yellow-100 border-2 border-yellow-500 rounded-full alert-icon">
                            <span class="text-yellow-500">
                                <svg fill="currentColor" viewBox="0 0 20 20" class="w-6 h-6">
                                    <path fill-rule="evenodd"
                                        d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                                        clip-rule="evenodd"></path>
                                </svg>
                            </span>
                        </div>
                        <div class="ml-4 alert-content">
                            <div class="text-lg font-semibold text-center text-yellow-800 retro alert-title">
                                @lang('messages.warnings')
                            </div>
                            <div class="text-sm text-gray-700 alert-description">
                                @lang('messages.warnings_two')
                            </div>
                            <center>
                                <a href="https://play.google.com/store/apps/details?id=io.ionic.retro.gaming.market"><img
                                        class="w-40" src="{{ asset('img/play.png') }}" alt=""></a>
                            </center>
                        </div>
                    </div>



                </div>
            @endif
@endif
