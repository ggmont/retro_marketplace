@extends('brcode.front.layout.app')
@section('content-css-include')
    <style>
        body {
            margin-top: 20px;

        }

        h3 {
            font-size: 16px;
        }

        .text-navy {
            color: #1ab394;
        }

        .cart-product-imitation {
            text-align: center;
            padding-top: 30px;
            height: 80px;
            width: 80px;
            background-color: #f8f8f9;
        }

        .product-imitation.xl {
            padding: 120px 0;
        }

        .product-desc {
            padding: 20px;
            position: relative;
        }

        .ecommerce .tag-list {
            padding: 0;
        }

        .ecommerce .fa-star {
            color: #d1dade;
        }

        .ecommerce .fa-star.active {
            color: #f8ac59;
        }

        .ecommerce .note-editor {
            border: 1px solid #e7eaec;
        }

        table.shoping-cart-table {
            margin-bottom: 0;
        }

        table.shoping-cart-table tr td {
            border: none;
            text-align: right;
        }

        table.shoping-cart-table tr td.desc,
        table.shoping-cart-table tr td:first-child {
            text-align: left;
        }

        table.shoping-cart-table tr td:last-child {
            width: 80px;
        }

        .ibox {
            clear: both;
            margin-bottom: 25px;
            margin-top: 0;
            padding: 0;
        }

        .ibox.collapsed .ibox-content {
            display: none;
        }

        .ibox:after,
        .ibox:before {
            display: table;
        }

        .ibox-title {
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            background-color: #ffffff;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 3px 0 0;
            color: inherit;
            margin-bottom: 0;
            padding: 14px 15px 7px;
            min-height: 48px;
        }

        .ibox-content {
            background-color: #ffffff;
            color: inherit;
            padding: 15px 20px 20px 20px;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 1px 0;
        }

        .ibox-footer {
            color: inherit;
            border-top: 1px solid #e7eaec;
            font-size: 90%;
            background: #ffffff;
            padding: 10px 15px;
        }

        /* width */
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #888;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555;
        }

        .scroller {
            width: 100%;
            max-width: 1120px;
            height: 1000px;
            overflow: auto;
            overflow-x: hidden;
        }

    </style>
@endsection
@section('content')
    <div class="container">
        <div class="nk-gap-2"></div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="nk-decorated-h-3">
                    <div class="section-title2">
                        <h3 class="retro">{{ __('Mi carrito') }}</b></h3>
                    </div>
                </h3>
            </div>
        </div><!-- /.row -->
        <div class="nk-gap"></div>
        <span class="retro">
            @include('partials.flash')
        </span>
        @if ($viewData['cart_new']->details->count() > 0)
            @php($total1 = 0)
            @php($art = 0)
            @foreach ($viewData['products_in_cart'] as $cartItem)
                @php($total1 += $cartItem->price * $cartItem->qty)
                @php($art += $cartItem->qty)
            @endforeach
            <div class="row" id="cart-order-user-detail">
                <div class="col-md-6">
                    <!-- START: Cart Totals -->
                    <div class="ibox-title">
                        <h5 class="text-xs text-gray-900 retro">@lang('messages.cart_general')</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                            <span class="text-sm font-semibold uppercase">{{ __('Pedidos') }}</span>
                            <span class="text-sm font-semibold ">
                                <div id="orderID">{{ count($us) }} </div>
                            </span>
                        </div>
                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                            <span class="text-sm font-semibold uppercase">{{ __('Artículos') }}</span>
                            <span class="text-sm font-semibold ">
                                <div id="articlesID">{{ $art }}</div>
                            </span>
                        </div>
                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                            <span class="text-sm font-semibold uppercase">{{ __('Precio total ítems') }}</span>
                            <span class="text-sm font-semibold ">
                                <div id="subTotalID">{{ number_format(Auth::user()->cart->totalCart, 2) }} €</div>
                            </span>
                        </div>
                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                            <span class="text-sm font-semibold uppercase">{{ __('Envío') }}</span>
                            <span class="text-sm font-semibold ">
                                <div id="shippingID">{{ number_format(Auth::user()->cart->totalShipping, 2) }} €
                                </div>
                            </span>
                        </div>
                        @if (Auth::user()->special_cash > 0)
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('Saldo especial') }}</span>
                                <span class="text-sm font-semibold ">
                                    <div id="shippingID"> - {{ number_format(Auth::user()->special_cash, 2) }} €
                                    </div>
                                </span>
                            </div>
                        @endif
                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                            <span class="text-sm font-semibold uppercase retro"><b>{{ __('Total') }}</b></span>
                            @php($total = Auth::user()->cart->totalCart + Auth::user()->cart->totalShipping - (Auth::user()->special_cash > 0 ? Auth::user()->special_cash : 0))
                            <span class="text-xs font-bold text-gray-900 retro">
                                <div id="totalID">{{ number_format($total, 2) }} €</div>
                            </span>


                        </div>
                        <span class="text-muted small">

                        </span>
                        <div class="m-t-sm">
                            <div class="btn-group">
                            </div>
                        </div>
                    </div>
                    <!-- END: Cart Totals -->
                </div>
                @if (Auth::id() > 0)
                    <div class="col-md-6">
                        <!-- START: Cart Totals -->
                        <div class="ibox-title">
                            <h5 class="text-xs text-gray-900 retro">@lang('messages.cart_direction')</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('Para') }}</span>
                                <span class="text-sm font-semibold">{{ Auth::user()->first_name }}
                                    {{ Auth::user()->last_name }}</span>
                            </div>
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('Dirección') }}</span>
                                <span class="text-sm font-semibold">
                                    @if (!empty(Auth::user()->address))
                                        {{ Auth::user()->address }}
                                    @else
                                        @lang('messages.emptys')
                                    @endif
                                </span>
                            </div>
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('Código postal') }}</span>
                                <span class="text-sm font-semibold">
                                    @if (!empty(Auth::user()->zipcode))
                                        {{ Auth::user()->zipcode }}
                                    @else
                                        @lang('messages.emptys')
                                    @endif
                                </span>
                            </div>
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('País') }}</span>
                                <span class="text-sm font-semibold ">{{ Auth::user()->country->name }}</span>
                            </div>

                            <a class="border-b retro nk-btn-xs nk-btn-rounded nes-btn is-error btn-block "
                                href="/account/profile#available"><span class="text-sm font-semibold retro">
                                    @lang('messages.change_direction')</span></a>
                        </div>


                        <!-- END: Cart Totals -->
                    </div>
                @endif
            </div>
            <div class="nk-gap-2"></div>
            <div class="row">

                <div class="col-lg-12 col-md-12">
                    <div class="bg-white nk-box-2">
                        <h1 class="text-2xl font-semibold text-gray-900 retro">@lang('messages.verified')</h1>
                        @if (App\SysSettings::where('type', 'Compras')->first())
                            @php($c = App\SysSettings::where('type', 'Compras')->first())
                            @if ($c->value == 0)
                                <h5 class="text-center">@lang('messages.purchase_deshabilited')</h5>
                            @else
                                <form action="{{ url('/cartComplete') }}" method="post">
                                    @if (Auth::id() > 0)

                                        <div class="col-md-12">
                                            @if (!empty($mes))
                                                <div class="alert alert-danger alert-dismissible">
                                                    <a href="#" class="close" data-dismiss="alert"
                                                        aria-label="close">&times;</a>
                                                    <strong>Error!</strong> {{ $mes }}
                                                </div>
                                            @endif
                                            <p class="text-lg text-gray-700">
                                                @lang('messages.please_read_cart') <b>@lang('messages.cart_comprometer').</b>
                                                <br>
                                                <b> <font color="red">@lang('messages.cart_warning')</font> </b> / @lang('messages.cart_duda') <a href="/site/faqs" class="profesional" target="_blank">FAQ</a>
                                            </p>

                                        </div>
                                    @endif

                                    {{ csrf_field() }}
                                    <div class="nk-gap"></div>
                                    <div class="nk-gap">
                                    </div>
                                    <div class="col-lg-6 nk-mchimp nk-form nk-form-style-1 validate">
                                        <label
                                            class="text-lg text-gray-700">{{ __('Por favor ingrese su contraseña para confirmar legalmente su pedido.') }}</label>
                                        <input type="password" name="pass" class="nes-input"
                                            placeholder="@lang('messages.yours_password')" required><br>

                                    </div>
                                    <br>
                                    <div class="col-md-12"> <button type="submit"
                                            class="border-b retro nk-btn-xs nk-btn-rounded nes-btn is-warning btn-block">
                                            <span class="text-sm font-semibold retro">
                                                {{ __('Comprometerse a comprar') }}
                                            </span>
                                        </button>
                                    </div>
                                </form>
                            @endif
                        @else
                            <form action="{{ url('/cartComplete') }}" method="post">
                                @if (Auth::id() > 0)

                                    <div class="col-md-12">
                                        @if (!empty($mes))
                                            <div class="alert alert-danger alert-dismissible">
                                                <a href="#" class="close" data-dismiss="alert"
                                                    aria-label="close">&times;</a>
                                                <strong>Error!</strong> {{ $mes }}
                                            </div>
                                        @endif
                                        {!! __('Por favor revise el contenido de su carrito de compras. Luego ingrese su contraseña en la casilla de verificación y haga clic en <b> Comprometer para comprar </b>. <br> Entonces podrás elegir tu método de pago.') !!}


                                    </div>
                                @endif

                                {{ csrf_field() }}
                                <div class="nk-gap"></div>
                                <div class="col-lg-6 nk-mchimp nk-form nk-form-style-1 validate">
                                    <label
                                        for="">{{ __('Por favor ingrese su contraseña para confirmar legalmente su pedido.') }}</label>
                                    <input type="password" name="pass" class="form-control" placeholder="Password"
                                        required><br>
                                </div>
                                <div class="col-md-6"><button type="submit"
                                        class="nk-btn nk-btn-xs nk-btn-rounded nk-btn-color-white btn-block">{{ __('Comprometerse a comprar') }}</button>
                                </div>
                            </form>
                        @endif
                    </div>

                </div>
            </div>
            <div class="nk-gap-2"></div>

            <div class="col-md-12">
                <h3 class="nk-decorated-h-3">
                    <div class="section-title2">
                        <h3 class="retro">@lang('messages.cart_details')</b></h3>
                    </div>
                </h3>
            </div>
            @if (count($viewData['products_in_cart']) > 1)
            <div class="bg-white row scroller">
                @else
                <div class="bg-white row">
            @endif
                @php($order_num = 0)
                @if (count($viewData['products_in_cart']) > 0)
                    @foreach ($viewData['user_order'] as $key)
                        @php($qty_det = 0)
                        @php($det_total = 0)
                        @php($det_vol = 0)
                        @php($det_wei = 0)

                        @foreach ($viewData['products_in_cart'] as $cartItem)
                            @if ($cartItem->user->user_name == $key->user_name)
                                @php($qty_det += $cartItem->qty)
                                @php($det_total += $cartItem->qty * $cartItem->price)
                                @php($det_vol += $cartItem->qty * $cartItem->product->volume)
                                @php($det_wei += $cartItem->qty * $cartItem->product->weight)
                            @endif
                        @endforeach
                        @php($order_num += 1)
                        @foreach ($viewData['shipping_values'] as $val)
                            @if ($val->user_id == $key->id)
                                @php($shipping_user = $val->shipping_id)
                            @endif
                        @endforeach

                        <div class="col-lg-6">
                            <div class="ibox-title">
                                <h5 class="text-xs text-gray-900 retro"> {{ __('Detalle de pedido') }} <span
                                        class="text-main-1">
                                        #0000{{ $order_num }}</span></h5>
                            </div>
                            <div class="ibox-content">
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Comprador') }}</span>
                                    <span class="text-sm font-semibold ">
                                        @if (Auth::user())
                                            {{ Auth::user()->user_name }}
                                        @endif
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Vendedor') }}</span>
                                    <span class="text-sm font-semibold">
                                        {{ $key->user_name }}
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span
                                        class="text-sm font-semibold uppercase">{{ __('Cantidad de artículos') }}</span>
                                    <span class="text-sm font-semibold ">
                                        {{ $qty_det }}
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Peso') }}</span>
                                    <span class="text-sm font-semibold ">
                                        {{ $det_wei }} g
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Peso') }}</span>
                                    <span class="text-sm font-semibold ">
                                        {{ $det_wei }} g
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Volumen') }}</span>
                                    <span class="text-sm font-semibold ">
                                        {{ $det_vol }}
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase retro"><b>{{ __('Total') }}</b></span>
                                    <span class="text-xs font-bold text-gray-900 retro">
                                        {{ number_format($det_total, 2) }} €
                                    </span>
                                </div>
                                <div class="m-t-sm">
                                    <div class="btn-group">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-6 nk-form nk-form-style-1">
                            <div class="ibox-title">
                                <h5 class="text-xs text-gray-900 retro">@lang('messages.send_method')</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="nes-select">
                                    <select data-id="{{ $key->user_name }}" onchange="updateShipping(this)">
                                        @foreach ($viewData['shipping'] as $key_ship)
                                            @if ($key_ship->from_id == $key->country_id && $key_ship->to_id == Auth::user()->country_id)
                                                @if ($det_total <= $key_ship->max_value && $det_wei <= $key_ship->max_weight && $det_vol <= $key_ship->volume)
                                                    <option value="{{ $key_ship->id }}"
                                                        {{ $key_ship->id == $shipping_user ? 'selected' : '' }}>
                                                        {{ $key_ship->name }} @if ($key_ship->certified == 'Yes') Envio certificado
                                                        @else Envio
                                                            no certificado @endif
                                                        {{ number_format($key_ship->price, 2) }} € -
                                                        {{ number_format($key_ship->max_value, 2) }} € max value</option>
                                                @endif
                                            @endif
                                            @if ($key_ship->from_id == 0 && $key_ship->to_id == 0)
                                                <option value="{{ $key_ship->id }}"
                                                    {{ $key_ship->id == $shipping_user ? 'selected' : '' }}>
                                                {{ $key_ship->name }} @if ($key_ship->certified == 'Yes') Envio certificado @else
                                                        Envio no
                                                        certificado @endif
                                                    {{ number_format($key_ship->price, 2) }} € -
                                                    {{ number_format($key_ship->max_value, 2) }} € max value</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        @php($con = 0)
                        @php($game = 0)
                        @foreach (App\AppOrgCategory::all() as $CatItem)
                            @php($contador = 0)
                            @foreach ($viewData['products_in_cart'] as $cartItem)
                                @if ($cartItem->user->user_name == $key->user_name && $cartItem->product_info->catid == $CatItem->id)
                                    @php($contador += 1)
                                @endif
                            @endforeach


                            <div class="col-lg-12" style="display: {{ $contador > 0 ? 'block' : 'none' }}; ">
                                <h5 class="text-gray-900 mt-15 retro">
                                    {{ __($CatItem->name) }}
                                </h5>
                                <div class="wishlist-table table-responsive">
                                    <table>
                                        <thead>
                                            @if ($CatItem->id == 1)
                                                <tr class="bg-red-700" style="font-size:10px;">
                                                    <th class="product-cart-img">
                                                        <span class="nobr"><i class="fa fa-image"></i></span>
                                                    </th>
                                                    <th class="product-name">
                                                        <span class="nobr">@lang('messages.products')</span>
                                                    </th>
                                                    <th class="product-name">
                                                        <span class="nobr">@lang('messages.box')</span>
                                                    </th>
                                                    <th class="product-price">
                                                        <span class="nobr">@lang('messages.cover')</span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Manual </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr">@lang('messages.game_product_inventory')</span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Extra </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> <i class="fa fa-image"></i>
                                                        </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> <i class="fa fa-comment"></i>
                                                        </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> @lang('messages.cants') </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> @lang('messages.price') </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Total </span>
                                                    </th>
                                                    <th class="product-add-to-cart">@lang('messages.actions')</th>
                                                </tr>
                                            @elseif($CatItem->id == 2)
                                                <tr class="bg-red-700" style="font-size:10px;">
                                                    <th class="product-cart-img">
                                                        <span class="nobr"><i class="fa fa-image"></i></span>
                                                    </th>
                                                    <th class="product-name">
                                                        <span class="nobr">@lang('messages.products')</span>
                                                    </th>
                                                    <th class="product-name">
                                                        <span class="nobr">@lang('messages.box')</span>
                                                    </th>
                                                    <th class="product-price">
                                                        <span class="nobr">@lang('messages.inside')</span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Manual </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr">@lang('messages.console')</span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Extra </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Cables </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> <i class="fa fa-image"></i>
                                                        </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> <i class="fa fa-comment"></i>
                                                        </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr">@lang('messages.cants') </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr">@lang('messages.price') </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Total </span>
                                                    </th>
                                                    <th class="product-add-to-cart">@lang('messages.actions')</th>
                                                </tr>

                                            @else
                                                <tr class="bg-red-700" style="font-size:10px;">
                                                    <th class="product-cart-img">
                                                        <span class="nobr"><i class="fa fa-image"></i></span>
                                                    </th>
                                                    <th class="product-name">
                                                        <span class="nobr">@lang('messages.products')</span>
                                                    </th>
                                                    <th class="product-name">
                                                        <span class="nobr">@lang('messages.box')</span>
                                                    </th>
                                                    <th class="product-price">
                                                        <span class="nobr">@lang('messages.state_inventory')</span>
                                                    </th>

                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Extra </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> <i class="fa fa-image"></i>
                                                        </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> <i class="fa fa-comment"></i>
                                                        </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> @lang('messages.cants') </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr">  @lang('messages.price') </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Total </span>
                                                    </th>
                                                    <th class="product-add-to-cart">@lang('messages.actions')</th>
                                                </tr>
                                            @endif
                                        </thead>

                                        <tbody>
                                            @foreach ($viewData['products_in_cart'] as $cartItem)
                                                @if ($cartItem->user->user_name == $key->user_name && $cartItem->product_info->catid == $CatItem->id)
                                                    @if ($CatItem->id == 1)
                                                        <tr>
                                                            <td class="text-center">
                                                                @if (count($cartItem->product_info->product_images) > 0)
                                                                    <a href="/product/{{ $cartItem->product_info->id }}">
                                                                        <img width="70px" height="70px"
                                                                            src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                                    </a>
                                                                @else
                                                                    <img width="70px" height="70px"
                                                                        src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                                @endif
                                                            </td>
                                                            <td class="text-left">
                                                                <a href="/product/{{ $cartItem->product_info->id }}"
                                                                    target="_blank">
                                                                    <span
                                                                        class="text-xs retro font-weight-bold color-primary small">
                                                                        <font color="black">
                                                                            <b>{{ $cartItem->product_info->name }}
                                                                                @if ($cartItem->product_info->name_en)
                                                                                    <br><small>{{ $cartItem->product_info->name_en }}</small>
                                                                                @endif
                                                                            </b>
                                                                        </font>
                                                                    </span>
                                                                </a>



                                                                <br><span
                                                                    class="h7 text-secondary">{{ $cartItem->product_info->platform }}
                                                                    -
                                                                    {{ $cartItem->product_info->region }}</span>

                                                            </td>

                                                            <td class="product-name">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->box }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->box_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>
                                                            <td class="product-stock-status">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->cover }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->cover_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>
                                                            <td class="product-stock-status">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->manual }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->manual_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>
                                                            <td class="product-price">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->game }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->game_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>
                                                            </td>
                                                            <td class="product-price">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->extra }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->extra_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>

                                                            @if ($cartItem->images->first())
                                                                <td class="text-center product-cart-img">

                                                                    <a href="{{ url('/uploads/inventory-images/' . $cartItem->images->first()->image_path) }}"
                                                                        class="fancybox"
                                                                        data-fancybox="{{ $cartItem->id }}">

                                                                        <img width="50px" height="50px"
                                                                            class="transform motion-safe:hover:scale-110"
                                                                            src="{{ url('/uploads/inventory-images/' . $cartItem->images->first()->image_path) }}"
                                                                            alt="">
                                                                    </a>
                                                                    
                                                                    @foreach ($cartItem->images as $p)

                                                                        @if (!$loop->first)
                                                                            <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                                data-fancybox="{{ $cartItem->id }}">

                                                                                <img width="0px" height="0px"
                                                                                    style="position:absolute;"
                                                                                    src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                                    alt="">
                                                                            </a>
                                                                        @endif
                                                                        
                                                                    @endforeach


                                                                </td>
                                                            @else
                                                                <td class="product-stock-status">
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-times" data-toggle="popover"
                                                                                data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>
                                                                </td>
                                                            @endif

                                                            <td class="product-stock-status">
                                                                @if ($cartItem->comments == '')
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-times" data-toggle="popover"
                                                                                data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>
                                                                @else
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-comment" data-toggle="popover"
                                                                                data-content="{{ $cartItem->comments }}"
                                                                                data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>

                                                                @endif
                                                            </td>


                                                            <td class="product-price">
                                                                <font color="black">
                                                                    <div class="relative">
                                                                        <span
                                                                            class="text-right cart-unit-price retro font-weight-bold color-primary small text-nowrap"
                                                                            data-value="{{ $cartItem->qty }}">
                                                                            {{ $cartItem->qty }}</span>

                                                                    </div>
                                                                </font>
                                                            </td>
                                                            <td>
                                                                <font color="green">
                                                                    <span
                                                                        class="text-right cart-unit-price retro font-weight-bold color-primary small text-nowrap"
                                                                        data-value="{{ $cartItem->price }}">
                                                                        {{ number_format($cartItem->price, 2) }}
                                                                        €
                                                                    </span>
                                                                </font>
                                                            </td>
                                                            <td>
                                                                <font color="green"> <span
                                                                        class="text-right retro font-weight-bold color-primary small text-nowrap">
                                                                        {{ number_format($cartItem->price * $cartItem->qty, 2) }}
                                                                        € </span> </font>
                                                            </td>
                                                            <td class="text-center">
                                                                <button data-inventory-id="{{ $cartItem->id + 1000 }}"
                                                                    type="button"
                                                                    class="nk-btn nk-btn-xs nes-btn is-error nk-btn-rounded nk-btn-color-white cart-delete-item-id">
                                                                    <i class="fa fa-trash"></i> </button>
                                                            </td>

                                                        </tr>
                                                    @elseif($CatItem->id == 2)
                                                        <tr>
                                                            <td class="text-center">
                                                                @if (count($cartItem->product_info->product_images) > 0)
                                                                    <a href="/product/{{ $cartItem->product_info->id }}"
                                                                        class="image_appear">
                                                                        <img width="70px" height="70px"
                                                                            src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                                    </a>
                                                                @else
                                                                    <img width="70px" height="70px"
                                                                        src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                                @endif
                                                            </td>
                                                            <td class="text-left">
                                                                <a href="/product/{{ $cartItem->product_info->id }}"
                                                                    target="_blank">
                                                                    <span
                                                                        class="text-xs retro font-weight-bold color-primary small">
                                                                        <font color="black">
                                                                            <b>{{ $cartItem->product_info->name }}
                                                                                @if ($cartItem->product_info->name_en)
                                                                                    <br><small>{{ $cartItem->product_info->name_en }}</small>
                                                                                @endif
                                                                            </b>
                                                                        </font>
                                                                    </span>
                                                                </a>



                                                                <br><span
                                                                    class="h7 text-secondary">{{ $cartItem->product_info->platform }}
                                                                    -
                                                                    {{ $cartItem->product_info->region }}</span>

                                                            </td>
                                                            <td class="product-name">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->box }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->box_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>
                                                            <td class="product-stock-status">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->cover }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->cover_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>
                                                            <td class="product-stock-status">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->manual }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->manual_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>

                                                            <td class="product-price">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->game }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->game_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>
                                                            </td>


                                                            <td class="product-price">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->extra }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->extra_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>

                                                            <td class="product-price">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->inside }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->inside_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>
                                                            @if ($cartItem->images->first())
                                                                <td class="text-center product-cart-img">

                                                                    <a href="{{ url('/uploads/inventory-images/' . $cartItem->images->first()->image_path) }}"
                                                                        class="fancybox"
                                                                        data-fancybox="{{ $cartItem->id }}">

                                                                        <img width="50px" height="50px"
                                                                            class="transform motion-safe:hover:scale-110"
                                                                            src="{{ url('/uploads/inventory-images/' . $cartItem->images->first()->image_path) }}"
                                                                            alt="">
                                                                    </a>

                                                                    @foreach ($cartItem->images as $p)

                                                                        @if (!$loop->first)
                                                                            <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                                data-fancybox="{{ $cartItem->id }}">

                                                                                <img width="0px" height="0px"
                                                                                    style="position:absolute;"
                                                                                    src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                                    alt="">
                                                                            </a>
                                                                        @endif

                                                                    @endforeach


                                                                </td>
                                                            @else
                                                                <td class="product-stock-status">
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-times" data-toggle="popover"
                                                                                data-content="No Posee" data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                            <td class="product-stock-status">
                                                                @if ($cartItem->comments == '')
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-times" data-toggle="popover"
                                                                                data-content="No Posee" data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>
                                                                @else
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-comment" data-toggle="popover"
                                                                                data-content="{{ $cartItem->comments }}"
                                                                                data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>

                                                                @endif
                                                            </td>
                                                            <td class="product-price">
                                                                <div class="relative">
                                                                    <span
                                                                        class="text-right text-gray-900 cart-unit-price retro font-weight-bold color-primary small text-nowrap"
                                                                        data-value="{{ $cartItem->qty }}">
                                                                        {{ $cartItem->qty }}</span>

                                                                </div>
                                                            </td>
                                                            <td>
                                                                <font color="green"> <span
                                                                        class="text-right cart-unit-price retro font-weight-bold color-primary small text-nowrap"
                                                                        data-value="{{ $cartItem->price }}">
                                                                        {{ number_format($cartItem->price, 2) }}
                                                                        € </span> </font>


                                                            </td>
                                                            <td>
                                                                <font color="green"> <span
                                                                        class="text-right retro font-weight-bold color-primary small text-nowrap">
                                                                        {{ number_format($cartItem->price * $cartItem->qty, 2) }}
                                                                        € </span> </font>
                                                            </td>
                                                            <td class="text-center">
                                                                <button data-inventory-id="{{ $cartItem->id + 1000 }}"
                                                                    type="button"
                                                                    class="nk-btn nk-btn-xs nes-btn is-error nk-btn-rounded nk-btn-color-white cart-delete-item-id">
                                                                    <i class="fa fa-trash"></i> </button>
                                                            </td>

                                                        </tr>
                                                    @else
                                                        <tr>
                                                            <td class="text-center">
                                                                @if (count($cartItem->product_info->product_images) > 0)
                                                                    <a href="/product/{{ $cartItem->product_info->id }}"
                                                                        class="image_appear">
                                                                        <img width="70px" height="70px"
                                                                            src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                                    </a>
                                                                @else
                                                                    <img width="70px" height="70px"
                                                                        src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                                @endif
                                                            </td>
                                                            <td class="text-left">
                                                                <a href="/product/{{ $cartItem->product_info->id }}"
                                                                    target="_blank">
                                                                    <span
                                                                        class="text-xs retro font-weight-bold color-primary small">
                                                                        <font color="black">
                                                                            <b>{{ $cartItem->product_info->name }}
                                                                                @if ($cartItem->product_info->name_en)
                                                                                    <br><small>{{ $cartItem->product_info->name_en }}</small>
                                                                                @endif
                                                                            </b>
                                                                        </font>
                                                                    </span>
                                                                </a>



                                                                <br><span
                                                                    class="h7 text-secondary">{{ $cartItem->product_info->platform }}
                                                                    -
                                                                    {{ $cartItem->product_info->region }}</span>

                                                            </td>
                                                            <td class="product-name">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->box }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->box_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>

                                                            <td class="product-price">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->game }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->game_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>
                                                            </td>

                                                            <td class="product-price">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->extra }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->extra_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>


                                                            @if ($cartItem->images->first())
                                                                <td class="text-center product-cart-img">

                                                                    <a href="{{ url('/uploads/inventory-images/' . $cartItem->images->first()->image_path) }}"
                                                                        class="fancybox"
                                                                        data-fancybox="{{ $cartItem->id }}">

                                                                        <img width="50px" height="50px"
                                                                            class="transform motion-safe:hover:scale-110"
                                                                            src="{{ url('/uploads/inventory-images/' . $cartItem->images->first()->image_path) }}"
                                                                            alt="">
                                                                    </a>

                                                                    @foreach ($cartItem->images as $p)

                                                                        @if (!$loop->first)
                                                                            <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                                data-fancybox="{{ $cartItem->id }}">

                                                                                <img width="0px" height="0px"
                                                                                    style="position:absolute;"
                                                                                    src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                                    alt="">
                                                                            </a>
                                                                        @endif

                                                                    @endforeach


                                                                </td>
                                                            @else
                                                                <td class="product-stock-status">
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-times" data-toggle="popover"
                                                                                data-content="No Posee" data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                            <td class="product-stock-status">
                                                                @if ($cartItem->comments == '')
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-times" data-toggle="popover"
                                                                                data-content="No Posee" data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>
                                                                @else
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-comment" data-toggle="popover"
                                                                                data-content="{{ $cartItem->comments }}"
                                                                                data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>

                                                                @endif
                                                            </td>


                                                            <td class="product-price">
                                                                <div class="relative">
                                                                    <span
                                                                        class="text-right text-gray-900 cart-unit-price retro font-weight-bold color-primary small text-nowrap"
                                                                        data-value="{{ $cartItem->qty }}">
                                                                        {{ $cartItem->qty }}</span>

                                                                </div>
                                                            </td>
                                                            <td>
                                                                <font color="green"> <span
                                                                        class="text-right cart-unit-price retro font-weight-bold color-primary small text-nowrap"
                                                                        data-value="{{ $cartItem->price }}">
                                                                        {{ number_format($cartItem->price, 2) }}
                                                                        € </span> </font>


                                                            </td>
                                                            <td>
                                                                <font color="green"> <span
                                                                        class="text-right retro font-weight-bold color-primary small text-nowrap">
                                                                        {{ number_format($cartItem->price * $cartItem->qty, 2) }}
                                                                        € </span> </font>
                                                            </td>
                                                            <td class="text-center">
                                                                <button data-inventory-id="{{ $cartItem->id + 1000 }}"
                                                                    type="button"
                                                                    class="nk-btn nk-btn-xs nes-btn is-error nk-btn-rounded nk-btn-color-white cart-delete-item-id">
                                                                    <i class="fa fa-trash"></i> </button>
                                                            </td>

                                                        </tr>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="nk-gap-2"></div>
                            </div>
                        @endforeach

                    @endforeach
                @endif
            </div>
    </div>

@else
    <div class="container">
        <div class="nk-gap-3"></div>
        <div class="row">
            <div class="col-lg-12">
                <blockquote class="nk-blockquote">
                    <div class="nk-blockquote-icon"></div>
                    <div class="text-center nk-blockquote-content h4 retro">
                        <font color="black">
                            {{ __('No hay productos en tu carrito de compra. Agrega productos ingresando a las categorías del menu principal') }}
                        </font>
                    </div>
                    <div class="nk-gap"></div>
                    <div class="nk-blockquote-author"></div>
                </blockquote>
            </div>
        </div>
    </div>
    @endif

    <div class="nk-gap-2"></div>
    </div><!-- /.container -->
@endsection

@section('content-script-include')
    <script src="{{ asset('brcode/js/pty.components.js') }}"></script>
@endsection
@section('content-script')
    <script>
        $(document).ready(function() {
            $('.comments').popover();
            $('[data-toggle="popover"]').popover();
            $('.cart-quantity').change(function() {
                qty = $(this).val();
                id = $(this).data('inventory-id');
                pId = $(this).data('product-id');

                $.showBigOverlay({
                    message: '{{ __('Actualizando tu carro de compras') }}',
                    onLoad: function() {
                        $.ajax({
                            url: '{{ url('/cart/add_item') }}',
                            data: {
                                _token: $('input[name="_token"]:eq(0)').val(),
                                inventory_id: id,
                                inventory_qty: qty,
                                inventory_typ: 0,
                            },
                            dataType: 'JSON',
                            type: 'POST',
                            success: function(data) {
                                if (data.error == 0) {
                                    //window.location.href = window.location.href;
                                    /*
                                                                                                  // message.remove();
                                                                                                  // overlay.remove();
                                                                                                  $('#br-cart-items').text(data.items);
                                                                                                        $('#articlesID').text(data.items);
                                                                                                        //parent.find('.cart-item-total-price').text(data.item_total + " €");
                                                                                                        
                                                                                                        $('.cart-total-price:eq(0)').text((data.gran_total).toFixed(2)  + " €");
                                                                                                        $('#subTotalID').text((data.item_total).toFixed(2) + " €");
                                                                                                        $('#totalID').text((data.gran_total).toFixed(2) + " €");
                                                                                                        $('#orderID').text(data.user);
                                                                                                        */
                                } else {

                                }
                                $.showBigOverlay('hide');
                            },
                            error: function(data) {

                            }
                        })
                    }
                });

            });

            $('.cart-delete-item-id').click(function() {
                var inventoryId = $(this).data('inventory-id');
                var cartItem = $(this).closest('.cart-item');
                $.showBigOverlay({
                    message: '{{ __('Actualizando tu carro de compras') }}',
                    onLoad: function() {
                        $.ajax({
                            url: '{{ url('/cart/add_item') }}',
                            data: {
                                _token: $('input[name="_token"]:eq(0)').val(),
                                inventory_id: inventoryId,
                                inventory_qty: 0,
                            },
                            dataType: 'JSON',
                            type: 'POST',
                            success: function(data) {

                                if (data.error == 0) {
                                    window.location.href = window.location.href;
                                    /*
                                    location.reload();

                                    var emptyMessage = $('<span/>').addClass('cart-empty').css({
                                      opacity: 0
                                    }).text('{{ __('No hay productos en tu carrito de compra. Agrega productos ingresando a las categorías del menu principal') }}');
                                    if($('.cart-item').length == 1) {
                                      $('#br-search-result-list').slideUp(250, function() {
                                        $( this ).remove();
                                        $('#cart-container').html('').append(emptyMessage);
                                        $('#cart-order-user-detail').html('');
                                        emptyMessage.animate({opacity: 1}, 250);
                                      });
                                    }
                                    else {
                                      cartItem.slideUp(250, function() {
                                        $( this ).remove();
                                      });
                                      $('.cart-total-price:eq(0)').text(data.gran_total + " €");
                                      $('#subTotalID').text(data.item_total.toFixed(2) +" €");
                                      $('#totalID').text(data.gran_total.toFixed(2) +" €");
                                      $('#orderID').text(data.user);
                                    }
                                    $('#br-cart-items').text(data.items);
                                    $('#articlesID').text(data.items);
                                    
                                    */
                                } else {
                                    $.showBigOverlay('hide');
                                }
                                $.showBigOverlay('hide');
                            },
                            error: function(data) {
                                $.showBigOverlay('hide');
                            }
                        })
                    }
                });

            });

        });

        function updateShipping(x) {
            $.showBigOverlay({
                message: '{{ __('Actualizando tu carro de compras') }}',
                onLoad: function() {
                    $.ajax({
                        url: '{{ url('/cart/add_shipping') }}',
                        data: {
                            _token: $('input[name="_token"]:eq(0)').val(),
                            d1: $(x).data('id'),
                            d2: $(x).val(),
                        },
                        dataType: 'JSON',
                        type: 'POST',
                        success: function(data) {
                            if (data.error == 0) {
                                window.location.href = window.location.href;
                            } else {
                                $.showBigOverlay('hide');
                            }
                            $.showBigOverlay('hide');
                        },
                        error: function(data) {

                        }
                    })
                }
            });
        }
    </script>

@endsection
