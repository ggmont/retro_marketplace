<div>
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div class="page-content-wrapper py-3">
            <div class="container">

                <div class="form-group searchbox-product">
                    <input type="text" class="form-control" placeholder="@lang('messages.user_search')" wire:model="search">
                </div>
               
                <div  id="EvaluationsAccordion" class="accordion rounded-0 d-md-none mb-4">
                   
                    @foreach ($user as $u)
                            <div class="item">
                                <div class="item-head">
                                    <a href="#EvaluationsAccordion-item-{{ $u->id }}"
                                        data-parent="#EvaluationsAccordion" data-toggle="collapse"
                                        aria-controls="EvaluationsAccordion-item-{{ $u->id }}"
                                        aria-expanded="false" class="collapsed">
                                        <h3 class="h4 d-flex align-items-center">
                                            <div class="mr-2">
                                                <span class="flag-icon flag-icon-esp"></span>
                                            </div>
                                            <div>{{ ucfirst($u->user_name) }}</div>
                                            <div class="ml-auto pr-5"><img src="{{ asset('img/new.png') }}"
                                                    width="20px" data-toggle="popover"
                                                    data-content="@lang('messages.good')" data-placement="top"
                                                    data-trigger="hover"></div>
                                        </h3>
                                    </a>
                                </div>
                                <div id="EvaluationsAccordion-item-{{ $u->id }}" role="tabpanel"
                                    class="item-body collapse" style="">
                                    <div class="item-body-wrapper">
                                        <dl class="row evaluations-descriptionList mb-2">
                                            <dt class="col-6 color-primary">Fecha</dt>
                                            <dd class="col-6 text-right color-primary font-weight-bold">
                                                {{ date_format($u->created_at, 'Y/m/d') }}</dd>
                                            <dt class="col-6 color-primary">Productos en venta</dt>
                                            <dd class="col-6 text-right color-primary font-weight-bold">
                                                @if (App\AppOrgOrder::where('seller_user_id', $u->id)->whereIn('status', ['DD'])->count() == 0)
                                                    No info
                                                @else
                                                    {{ App\AppOrgOrder::where('seller_user_id', $u->id)->whereIn('status', ['DD'])->count() }}
                                                @endif
                                            </dd>
                                            <dt class="col-6 color-primary">Productos Vendidos</dt>
                                            <dd class="col-6 text-right color-primary font-weight-bold">
                                                <a class="color-primary"
                                                    href="{{ route('user-inventory', $u->user_name) }}">
                                                    <?php
                                                    $duration = [];
                                                    
                                                    foreach ($u->inventory as $item) {
                                                        $cantidad = $item->quantity;
                                                        $duration[] = $cantidad;
                                                    }
                                                    
                                                    $total = array_sum($duration);
                                                    ?>
                                                    @if ($total == 0)
                                                        @lang('messages.not_articles')
                                                    @else
                                                        {{ $total }}
                                                    @endif
                                                </a>
                                            </dd>
                                            <dt class="col-6 color-primary">Evaluación de usuario</dt>
                                            <dd class="col-6 text-right color-primary font-weight-bold">
                                                Buen Vendedor
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                    @endforeach

                    <div class="flex flex-col items-center">
                        @if ($user->count())
                            <div
                                class="flex items-center justify-between px-4 py-3 border-t border-gray-200 bg-red sm:px-6">
                                {{ $user->links('pagination') }}
                            </div>
                        @else
                            <div
                                class="flex items-center justify-between px-4 py-3 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                                @lang('messages.not_found_producto') "{{ $search }}" @lang('messages.per_page_two')
                                {{ $page }}
                                @lang('messages.see_page') {{ $perPage }}
                            </div>
                        @endif
                    </div>
                 
                </div>
               
            </div>

        </div>
    @else
        <div class="mt-20 wishlist-table-area mb-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="my-6 bg-white rounded shadow-md">
                            <div class="mt-20 col-12">
                                <h4 class="text-gray-900 retro"> {{ __('Filtrar por') . ':' }} </h4>
                            </div>
                            <div class="row">
                                <div class="col-md-11">
                                    <input wire:model="search"
                                        class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text" placeholder="@lang('messages.purcha_search')">


                                </div>
                                @if ($search !== '')
                                    <div class="col-md-1">
                                        <button wire:click="clear" class="nes-btn is-error"><span
                                                class="text-right retro font-weight-bold color-primary small text-nowrap"><i
                                                    class="nes-icon close is-small"></i> </span></button>
                                    </div>
                                @endif
                            </div>
                            <div class="col-lg-12">
                                <div
                                    class="flex items-center justify-between px-4 py-3 bg-white border-t border-gray-200 sm:px-6">

                                    <div class="nes-select">
                                        <select wire:model="perPage" class="text-gray-500 retro">
                                            <option value="5">5 @lang('messages.per_page')</option>
                                            <option value="10">10 @lang('messages.per_page')</option>
                                            <option value="15">15 @lang('messages.per_page')</option>
                                            <option value="25">25 @lang('messages.per_page')</option>
                                            <option value="50">50 @lang('messages.per_page')</option>
                                            <option value="10">100 @lang('messages.per_page')</option>
                                        </select>
                                    </div>


                                </div>
                            </div>
                            <div class="wishlist-table table-responsive">
                                <table class="w-full table-auto min-w-max">
                                    <thead>
                                        <tr class="text-sm leading-normal text-gray-100 uppercase bg-red-700">
                                            <th class="px-6 py-3 text-center">@lang('messages.user')</th>
                                            <th class="px-6 py-3 text-center">@lang('messages.member_date')</th>
                                            <th class="px-6 py-3 text-center">@lang('messages.sale_user')</th>
                                            <th class="px-6 py-3 text-center">@lang('messages.articles_inventory')</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-sm font-light text-gray-600">
                                        @foreach ($user as $u)
                                            <tr class="border-b border-gray-500 hover:bg-gray-200">
                                                <td class="px-6 py-3 text-left whitespace-nowrap">

                                                    <div class="flex items-center justify-between md:space-x-8">
                                                        @include('partials.pais_user')
                                                        &nbsp;
                                                        <?php foreach ($u->roles as $prueba) {
                                                            $prueba = $prueba->rol->name;
                                                        } ?>
                                                        <b><span class="font-mono text-lg font-medium"><a
                                                                    href="{{ route('user-info', $u->user_name) }}"
                                                                    target="_blank"
                                                                    style="color: #db0e0e">{{ ucfirst($u->user_name) }}</a></span></b>
                                                        @if ($prueba == 'Professional Seller')
                                                            <img src="{{ asset('img/roles/ProfesionalSeller.png') }}"
                                                                width="25" data-toggle="popover"
                                                                data-content="Vendedor Profesional" data-placement="top"
                                                                data-trigger="hover">
                                                        @else
                                                        @endif
                                                        &nbsp;
                                                        <img class="float-right" width="15px"
                                                            src="{{ asset($u->is_enabled == 'Y' ? '/img/new.png' : '/img/no-funciona.png') }}"
                                                            data-toggle="popover"
                                                            data-content="{{ $u->is_enabled == 'Y' ? 'Activo' : 'Inactivo' }}"
                                                            data-placement="top" data-trigger="hover" width="15px">

                                                    </div>


                                                </td>
                                                <td class="px-6 py-3 text-center">
                                                    <span class="font-mono text-base retro" style="color: #d44e4e">

                                                        {{ date_format($u->created_at, 'Y/m/d') }}

                                                    </span>
                                                </td>
                                                <td class="px-6 py-3 text-center">
                                                    <span class="font-mono text-base retro" style="color: #070707">
                                                        @if (App\AppOrgOrder::where('seller_user_id', $u->id)->whereIn('status', ['DD'])->count() == 0)
                                                            No info
                                                        @else
                                                            {{ App\AppOrgOrder::where('seller_user_id', $u->id)->whereIn('status', ['DD'])->count() }}
                                                        @endif
                                                    </span>
                                                </td>
                                                <td class="px-6 py-3 text-center">
                                                    <a href="{{ route('user-inventory', $u->user_name) }}"
                                                        target="_blank">
                                                        <?php
                                                        $duration = [];
                                                        
                                                        foreach ($u->inventory as $item) {
                                                            $cantidad = $item->quantity;
                                                            $duration[] = $cantidad;
                                                        }
                                                        
                                                        $total = array_sum($duration);
                                                        ?>
                                                        @if ($total == 0)
                                                            <span class="font-mono text-base retro"
                                                                style="color: #050505">@lang('messages.not_articles')</span>
                                                        @else
                                                            <span class="font-mono text-base retro"
                                                                style="color: #d82a2a">{{ $total }}</span>
                                                        @endif
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="flex flex-col items-center">
                                    @if ($user->count())
                                        <div
                                            class="flex items-center justify-between px-4 py-3 border-t border-gray-200 bg-red sm:px-6">
                                            {{ $user->links('pagination') }}
                                        </div>
                                    @else
                                        <div
                                            class="flex items-center justify-between px-4 py-3 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                                            @lang('messages.not_found_producto') "{{ $search }}" @lang('messages.per_page_two')
                                            {{ $page }}
                                            @lang('messages.see_page') {{ $perPage }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            document.addEventListener("DOMContentLoaded", () => {
                $(function() {
                    $('[data-toggle="popover"]').popover()
                })
            });
        </script>
    @endif

    
 
</div>
