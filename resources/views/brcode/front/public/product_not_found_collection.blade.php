@extends('brcode.front.layout.app_conversation')
@section('content-css-include')
    <link rel="stylesheet" href="{{ asset('plugins/fileinput/css/fileinput.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/jquery-ui.css') }}">
    <link href="/assets/dropzone-master/dist/min/dropzone.min.css" rel="stylesheet" type="text/css" />
    <style>
        .dropzone .dz-preview .dz-error-message {
            top: 175px !important;
        }
    </style>
    <style>
        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .select2-dropdown {
            background-color: white;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-sizing: border-box;
            display: block;
            position: absolute;
            left: -100000px;
            width: 100%;
            z-index: 1051;
        }
    </style>
@endsection

@section('content')
    <div class="header-area" id="headerArea">
        <div class="container h-100 d-flex align-items-center justify-content-between">

            <button type="button" id="errorgm" style="display:none" class="btn btn-secondary hidden me-05 mb-1"
                onclick="notification('notification-7' , 3000)">Auto Close (3s)</button>

            @include('partials.flash')

            <!-- Back Button-->
            <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
            <!-- Page Title-->
            <div class="page-heading">
                <h6 class="mb-0 font-extrabold">Producto No encontrado - Colección</h6>
            </div>
            <!-- Navbar Toggler-->

            <div class="normal">
                @if (Auth::user())
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @else
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="nk-main">
        <!-- START: Breadcrumbs -->
        <div class="nk-gap-1"></div>
        <div class="container">
            <div class="nk-gap-1"></div>
            <div class="row">
                <div class="col-md-12">


                    <br><br><br>
                    <span class="retro">@include('partials.flash')</span>
                    <h4>
                        <center>@lang('messages.select_sell')</center>
                    </h4>
                    <br>
                    <select class="form-control form-select" required id="type">
                        <option value="inicio" disabled selected hidden>@lang('messages.category_two')...</option>
                        <option value="Juegos">@lang('messages.games')</option>
                        <option value="Consolas">@lang('messages.consoles')</option>
                        <option value="Perifericos">@lang('messages.peripherals')</option>
                        <option value="Accesorios">@lang('messages.accesories')</option>
                        <option value="Merchandising">Merchandising</option>
                    </select>

                    <br>
                    <div id="target">
                        <div id="inicio">
                            <h4>¿Por qué pasa esto?</h4>
                            <p>
                                Existen miles y miles de juegos y productos diferentes y ,aunque lo intentamos,
                                no los tenemos todos.
                                <br>
                                Con tu ayuda, estaremos más cerca de tener una base de datos más fiable.
                            </p>
                            <h4>¿No encuentras tu producto?</h4>
                            <p>
                                Rellena el formulario y nosotros nos encargamos de subirlo
                                <br> Recuerda subir tus productos uno por uno con fotos por delante
                                y por detrás.
                            </p>
                        </div>
                        <div id="Juegos">
                            <form class="container" action="{{ route('send_product_request_collection') }}" method="POST"
                                enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                                <input name="category" type="hidden" value="Juegos">
                                <br>
                                <h4>
                                    <center>@lang('messages.detail_not_found')</center>
                                </h4>
                                <br>
                                <center>
                                    <small>¿Tienes dudas? Recuerda - <svg width="20px" height="20px" data-html="true"
                                            data-toggle="popover"
                                            data-content="
                                                                                                                                               - Si la caja está en diferentes idiomas - PAL-EU <br />
                                                                                                                                               - Si la caja está solo en Español - PAL-ESP <br />
                                                                                                                                               - Si la caja está en Alemán - PAL-DE <br />
                                                                                                                                               - Si la caja está en Italiano - PAL-ITA <br />
                                                                                                                                               - Si la caja está en Francés - PAL-FRA <br />
                                                                                                                                               - Si el juego está en inglés y tiene etiqueta con una M en el frontal - es NTSC-U <br />
                                                                                                                                               - Si el juego tiene una etiqueta en el frontal marcada con USK es PAL-UK<br />
                                                                                                                                               - Si la portada e información del juego está en japonés es NTSC-J"
                                            data-placement="top" data-trigger="hover" viewBox="0 0 76 76"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path id="greyshadow" d="M8 8h68v68h-68z" fill="#BFBFBF" />
                                            <path id="blackborder" d="M4 4h68v68h-68z" fill="#000" />
                                            <path id="background" d="M4 4h64v64h-64z" fill="#FFC07C" />
                                            <path id="borderlefttop" d="M4 0h64M0 4v64" stroke="#DE5917" stroke-width="8" />
                                            <path id="rivets" d="M8 8h4v4h-4zM60 60h4v4h-4zM8 60h4v4h-4zM60 8h4v4h-4z"
                                                fill="#000" />
                                            <path id="questionshadow"
                                                d="M24 20h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM36 52h8v8h-8z"
                                                fill="#000" />
                                            <path id="question"
                                                d="M20 16h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM32 48h8v8h-8z"
                                                fill="#DE5917" />
                                        </svg>
                                    </small>
                                </center>

                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label"> @lang('messages.name')</label>
                                        <input type="text" class="form-control" name="product" type="text"
                                            placeholder="Mario Bros" required="" autocomplete="off">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">@lang('messages.platform')</label>
                                        <select style="width: 100%" required="" class="form-control form-select"
                                            id="platform" name="platform">
                                            <option value="">@lang('messages.selects')</option>
                                            @foreach ($platform as $p)
                                                <option value="{{ $p->value }}">{{ $p->value }}</option>
                                            @endforeach
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">Region</label>
                                        <select style="width: 100%" class="form-control form-select" id="region"
                                            name="region" required="">
                                            <option value="">@lang('messages.selects')</option>
                                            @foreach ($region as $r)
                                                <option value="{{ $r->value }}">{{ $r->value }}</option>
                                            @endforeach
                                        </select>
                                    </section>
                                </section>

                                <br>
                                <h4>
                                    <center>@lang('messages.detail_product_sell')</center>
                                </h4>
                                <br>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label" for="name5">{{ __('Cantidad') }}</label>
                                        <input type="number" name="quantity" class="form-control" min="1"
                                            value="1" placeholder="Enter your name" required=""
                                            autocomplete="off">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </section>
                                </section>
                                <br>
                                <h5>
                                    @lang('messages.conditions_product')
                                </h5>
                                <br>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">@lang('messages.box')</label>
                                        <select style="width: 100%" class="form-control form-select" name="box_condition"
                                            required="">
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">@lang('messages.games')</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            name="game_condition" required="">
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">Manual</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            name="manual_condition" required="">
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label"> @lang('messages.cover')</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            name="cover_condition" required="">
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">Extras</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            name="extra_condition" required="">
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <br>
                                <h5 class="text-sm font-medium pb-2">Agrega imagenes de tu producto.
                                    <font color="red">Máximo 8 - Obligatorio</font>
                                </h5>

                                <section style="display:none" id="fileupload-files" required data-name="image_path[]">
                                </section>
                                <section id="dropzone">
                                    <section class="dz-message text-dark" data-dz-message>
                                        <span class="retro">@lang('messages.image_sell_mobile')</span>
                                    </section>
                                </section>

                                <br>
                                <h5 class="text-sm font-medium pb-2">Agregar Codigo de Barras de tu producto (Opcional)
                                </h5>

                                <section style="display:none" id="fileupload-files-ean" required data-name="image_ean[]">

                                </section>
                                <section id="dropzone-ean">
                                    <section class="dz-message text-dark" data-dz-message>
                                        <span class="retro">@lang('messages.image_sell_mobile')</span>
                                    </section>
                                </section>

                                <br>

                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                        <label class="form-label" for="city5"> {{ __('Comentarios') }}</label>
                                        <textarea id="comments" name="comments" rows="4" class="form-control" placeholder="Comentario"></textarea>
                                        <section class="textarea-counter" data-text-max="300"
                                            data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></section>
                                    </section>
                                </section>

                                <section class="text-center form-group col-md-12">
                                    <br>
                                    <input type="submit" class="btn btn-submit btn-danger w-100 text-lg font-extrabold"
                                        value="@lang('messages.soli_send')">
                                </section>
                                <br><br><br><br>
                            </form>
                        </div>
                        <div id="Consolas">
                            <form class="container" action="{{ route('send_product_request_collection') }}"
                                method="POST" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                                <input name="category" type="hidden" value="Consolas">
                                <br>
                                <h4>
                                    <center>@lang('messages.detail_not_found')</center>
                                </h4>
                                <br>
                                <center>
                                    <small>¿Tienes dudas? Recuerda - <svg width="20px" height="20px" data-html="true"
                                            data-toggle="popover"
                                            data-content="
                                                                                                                                               - Si la caja está en diferentes idiomas - PAL-EU <br />
                                                                                                                                               - Si la caja está solo en Español - PAL-ESP <br />
                                                                                                                                               - Si la caja está en Alemán - PAL-DE <br />
                                                                                                                                               - Si la caja está en Italiano - PAL-ITA <br />
                                                                                                                                               - Si la caja está en Francés - PAL-FRA <br />
                                                                                                                                               - Si el juego está en inglés y tiene etiqueta con una M en el frontal - es NTSC-U <br />
                                                                                                                                               - Si el juego tiene una etiqueta en el frontal marcada con USK es PAL-UK<br />
                                                                                                                                               - Si la portada e información del juego está en japonés es NTSC-J"
                                            data-placement="top" data-trigger="hover" viewBox="0 0 76 76"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path id="greyshadow" d="M8 8h68v68h-68z" fill="#BFBFBF" />
                                            <path id="blackborder" d="M4 4h68v68h-68z" fill="#000" />
                                            <path id="background" d="M4 4h64v64h-64z" fill="#FFC07C" />
                                            <path id="borderlefttop" d="M4 0h64M0 4v64" stroke="#DE5917"
                                                stroke-width="8" />
                                            <path id="rivets" d="M8 8h4v4h-4zM60 60h4v4h-4zM8 60h4v4h-4zM60 8h4v4h-4z"
                                                fill="#000" />
                                            <path id="questionshadow"
                                                d="M24 20h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM36 52h8v8h-8z"
                                                fill="#000" />
                                            <path id="question"
                                                d="M20 16h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM32 48h8v8h-8z"
                                                fill="#DE5917" />
                                        </svg>
                                    </small>
                                </center>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label"> @lang('messages.name')</label>
                                        <input type="text" class="form-control" name="product" type="text"
                                            placeholder="Nintendo" required="" autocomplete="off">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">@lang('messages.platform')</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            id="consola_platform" required name="platform">
                                            <option value="">@lang('messages.selects')</option>
                                            @foreach ($platform as $p)
                                                <option value="{{ $p->value }}">{{ $p->value }}</option>
                                            @endforeach
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">Region</label>
                                        <select style="width: 100%" class="form-control form-select" required
                                            id="region_platform" name="region">
                                            <option value="">@lang('messages.selects')</option>
                                            @foreach ($region as $r)
                                                <option value="{{ $r->value }}">{{ $r->value }}</option>
                                            @endforeach
                                        </select>
                                    </section>
                                </section>
                                <br>
                                <h4>
                                    <center>@lang('messages.detail_product_sell')</center>
                                </h4>
                                <br>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label" for="name5">{{ __('Cantidad') }}</label>
                                        <input type="number" name="quantity" class="form-control" min="1"
                                            value="1" required placeholder="Enter your name" autocomplete="off">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </section>
                                </section>
                                <br>
                                <h5>
                                    @lang('messages.conditions_product')
                                </h5>
                                <br>

                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">@lang('messages.box')</label>
                                        <select style="width: 100%" class="form-control form-select" name="box_condition"
                                            required>
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">Consola</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            name="game_condition" required>
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">@lang('messages.inside')</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            name="cover_condition" required>
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">Manual</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            name="manual_condition" required>
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">Cables</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            name="inside_condition" required>
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">Extras</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            name="extra_condition" required>
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <br>
                                <h5 class="text-sm font-medium pb-2">Agrega imagenes de tu producto.
                                    <font color="red">Máximo 8 - Obligatorio</font>
                                </h5>

                                <section style="display:none" id="fileupload-files-console" data-name="image_path[]">
                                </section>
                                <section id="dropzone-console">
                                    <section class="dz-message text-dark" data-dz-message>
                                        <span class="retro">@lang('messages.image_sell_mobile')</span>
                                    </section>
                                </section>

                                <br>
                                <h5 class="text-sm font-medium pb-2">Agregar Codigo de Barras de tu producto (Opcional)
                                </h5>

                                <section style="display:none" id="fileupload-files-ean-console" data-name="image_ean[]">
                                </section>
                                <section id="dropzone-ean-console">
                                    <section class="dz-message text-dark" data-dz-message>
                                        <span class="retro">@lang('messages.image_sell_mobile')</span>
                                    </section>
                                </section>

                                <br>

                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                        <label class="form-label" for="city2"> {{ __('Comentarios') }}</label>
                                        <textarea id="comments" name="comments" rows="4" class="form-control" placeholder="Comentario"></textarea>
                                        <section class="textarea-counter" data-text-max="300"
                                            data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></section>
                                    </section>
                                </section>

                                <section class="text-center form-group col-md-12">
                                    <br>
                                    <input type="submit" class="btn btn-submit btn-danger w-100 text-lg font-extrabold"
                                        value="@lang('messages.soli_send')">
                                </section>
                                <br><br><br><br>
                            </form>
                        </div>
                        <div id="Perifericos">
                            <form class="container" action="{{ route('send_product_request_collection') }}"
                                method="POST" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                                <input name="category" type="hidden" value="Perifericos">
                                <br>
                                <h4>
                                    <center>@lang('messages.detail_not_found')</center>
                                </h4>
                                <br>
                                <center>
                                    <small>¿Tienes dudas? Recuerda - <svg width="20px" height="20px" data-html="true"
                                            data-toggle="popover"
                                            data-content="
                                                                                                                                               - Si la caja está en diferentes idiomas - PAL-EU <br />
                                                                                                                                               - Si la caja está solo en Español - PAL-ESP <br />
                                                                                                                                               - Si la caja está en Alemán - PAL-DE <br />
                                                                                                                                               - Si la caja está en Italiano - PAL-ITA <br />
                                                                                                                                               - Si la caja está en Francés - PAL-FRA <br />
                                                                                                                                               - Si el juego está en inglés y tiene etiqueta con una M en el frontal - es NTSC-U <br />
                                                                                                                                               - Si el juego tiene una etiqueta en el frontal marcada con USK es PAL-UK<br />
                                                                                                                                               - Si la portada e información del juego está en japonés es NTSC-J"
                                            data-placement="top" data-trigger="hover" viewBox="0 0 76 76"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path id="greyshadow" d="M8 8h68v68h-68z" fill="#BFBFBF" />
                                            <path id="blackborder" d="M4 4h68v68h-68z" fill="#000" />
                                            <path id="background" d="M4 4h64v64h-64z" fill="#FFC07C" />
                                            <path id="borderlefttop" d="M4 0h64M0 4v64" stroke="#DE5917"
                                                stroke-width="8" />
                                            <path id="rivets" d="M8 8h4v4h-4zM60 60h4v4h-4zM8 60h4v4h-4zM60 8h4v4h-4z"
                                                fill="#000" />
                                            <path id="questionshadow"
                                                d="M24 20h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM36 52h8v8h-8z"
                                                fill="#000" />
                                            <path id="question"
                                                d="M20 16h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM32 48h8v8h-8z"
                                                fill="#DE5917" />
                                        </svg>
                                    </small>
                                </center>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label"> @lang('messages.name')</label>
                                        <input type="text" class="form-control" name="product" type="text"
                                            placeholder="Gamepad" required="" autocomplete="off">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">@lang('messages.platform')</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            id="platform_periferico" name="platform">
                                            <option value="">@lang('messages.selects')</option>
                                            @foreach ($platform as $p)
                                                <option value="{{ $p->value }}">{{ $p->value }}</option>
                                            @endforeach
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">Region</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            id="region_periferico" name="region">
                                            <option value="">@lang('messages.selects')</option>
                                            @foreach ($region as $r)
                                                <option value="{{ $r->value }}">{{ $r->value }}</option>
                                            @endforeach
                                        </select>
                                    </section>
                                </section>
                                <br>
                                <h4>
                                    <center>@lang('messages.detail_product_sell')</center>
                                </h4>
                                <br>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label" for="name5">{{ __('Cantidad') }}</label>
                                        <input type="number" name="quantity" class="form-control" min="1"
                                            value="1" placeholder="Enter your name" autocomplete="off">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </section>
                                </section>
                                <br>
                                <h5>
                                    @lang('messages.conditions_product')
                                </h5>
                                <br>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">@lang('messages.box')</label>
                                        <select style="width: 100%" class="form-control form-select" name="box_condition"
                                            required>
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">@lang('messages.products')</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            name="game_condition" required>
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">Extras</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            name="extra_condition" required>
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <br>
                                <h5 class="text-sm font-medium pb-2">Agrega imagenes de tu producto.
                                    <font color="red">Máximo 8 - Obligatorio</font>
                                </h5>

                                <section style="display:none" id="fileupload-files-peripheral" data-name="image_path[]">
                                </section>
                                <section id="dropzone-peripheral">
                                    <section class="dz-message text-dark" data-dz-message>
                                        <span class="retro">@lang('messages.image_sell_mobile')</span>
                                    </section>
                                </section>
                                <br>

                                <h5 class="text-sm font-medium pb-2">Agregar Codigo de Barras de tu producto (Opcional)
                                </h5>

                                <section style="display:none" id="fileupload-files-ean-peripheral"
                                    data-name="image_ean[]">
                                </section>
                                <section id="dropzone-ean-peripheral">
                                    <section class="dz-message text-dark" data-dz-message>
                                        <span class="retro">@lang('messages.image_sell_mobile')</span>
                                    </section>
                                </section>

                                <br>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                        <label class="form-label" for="city4"> {{ __('Comentarios') }}</label>
                                        <textarea id="comments" name="comments" rows="4" class="form-control" placeholder="Comentario"></textarea>
                                        <section class="textarea-counter" data-text-max="300"
                                            data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></section>
                                    </section>
                                </section>

                                <section class="text-center form-group col-md-12">
                                    <br>
                                    <input type="submit" class="btn btn-submit btn-danger w-100 text-lg font-extrabold"
                                        value="@lang('messages.soli_send')">
                                </section>
                                <br><br><br><br>
                            </form>
                        </div>
                        <div id="Accesorios">
                            <form class="container" action="{{ route('send_product_request_collection') }}"
                                method="POST" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                                <input name="category" type="hidden" value="Accesorios">
                                <br>
                                <h4>
                                    <center>@lang('messages.detail_not_found')</center>
                                </h4>
                                <br>
                                <center>
                                    <small>¿Tienes dudas? Recuerda - <svg width="20px" height="20px" data-html="true"
                                            data-toggle="popover"
                                            data-content="
                                                                                                                                               - Si la caja está en diferentes idiomas - PAL-EU <br />
                                                                                                                                               - Si la caja está solo en Español - PAL-ESP <br />
                                                                                                                                               - Si la caja está en Alemán - PAL-DE <br />
                                                                                                                                               - Si la caja está en Italiano - PAL-ITA <br />
                                                                                                                                               - Si la caja está en Francés - PAL-FRA <br />
                                                                                                                                               - Si el juego está en inglés y tiene etiqueta con una M en el frontal - es NTSC-U <br />
                                                                                                                                               - Si el juego tiene una etiqueta en el frontal marcada con USK es PAL-UK<br />
                                                                                                                                               - Si la portada e información del juego está en japonés es NTSC-J"
                                            data-placement="top" data-trigger="hover" viewBox="0 0 76 76"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path id="greyshadow" d="M8 8h68v68h-68z" fill="#BFBFBF" />
                                            <path id="blackborder" d="M4 4h68v68h-68z" fill="#000" />
                                            <path id="background" d="M4 4h64v64h-64z" fill="#FFC07C" />
                                            <path id="borderlefttop" d="M4 0h64M0 4v64" stroke="#DE5917"
                                                stroke-width="8" />
                                            <path id="rivets" d="M8 8h4v4h-4zM60 60h4v4h-4zM8 60h4v4h-4zM60 8h4v4h-4z"
                                                fill="#000" />
                                            <path id="questionshadow"
                                                d="M24 20h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM36 52h8v8h-8z"
                                                fill="#000" />
                                            <path id="question"
                                                d="M20 16h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM32 48h8v8h-8z"
                                                fill="#DE5917" />
                                        </svg>
                                    </small>
                                </center>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label"> @lang('messages.name')</label>
                                        <input type="text" class="form-control" name="product" type="text"
                                            placeholder="Accesorio" required="" autocomplete="off">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">@lang('messages.platform')</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            id="platform_accesorio" required name="platform">
                                            <option value="">@lang('messages.selects')</option>
                                            @foreach ($platform as $p)
                                                <option value="{{ $p->value }}">{{ $p->value }}</option>
                                            @endforeach
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">Region</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            id="region_accesorio" required name="region">
                                            <option value="">@lang('messages.selects')</option>
                                            @foreach ($region as $r)
                                                <option value="{{ $r->value }}">{{ $r->value }}</option>
                                            @endforeach
                                        </select>
                                    </section>
                                </section>
                                <br>
                                <h4>
                                    <center>@lang('messages.detail_product_sell')</center>
                                </h4>
                                <br>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label" for="name5">{{ __('Cantidad') }}</label>
                                        <input type="number" name="quantity" class="form-control" min="1"
                                            value="1" required placeholder="Enter your name" autocomplete="off">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </section>
                                </section>
                                <br>
                                <h5>
                                    @lang('messages.conditions_product')
                                </h5>
                                <br>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">@lang('messages.box')</label>
                                        <select style="width: 100%" class="form-control form-select" name="box_condition"
                                            required>
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">@lang('messages.products')</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            name="game_condition" required>
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">Extras</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            name="extra_condition" required>
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <br>
                                <h5 class="text-sm font-medium pb-2">Agrega imagenes de tu producto.
                                    <font color="red">Máximo 8 - Obligatorio</font>
                                </h5>

                                <section style="display:none" id="fileupload-files-accesories" data-name="image_path[]">
                                </section>
                                <section id="dropzone-accesories">
                                    <section class="dz-message text-dark" data-dz-message>
                                        <span class="retro">@lang('messages.image_sell_mobile')</span>
                                    </section>
                                </section>

                                <br>
                                <h5 class="text-sm font-medium pb-2">Agregar Codigo de Barras de tu producto (Opcional)
                                </h5>

                                <section style="display:none" id="fileupload-files-ean-accesories"
                                    data-name="image_ean[]">
                                </section>
                                <section id="dropzone-ean-accesories">
                                    <section class="dz-message text-dark" data-dz-message>
                                        <span class="retro">@lang('messages.image_sell_mobile')</span>
                                    </section>
                                </section>

                                <br>

                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                        <label class="form-label" for="city6"> {{ __('Comentarios') }}</label>
                                        <textarea id="comments" name="comments" rows="4" class="form-control" placeholder="Comentario"></textarea>
                                        <section class="textarea-counter" data-text-max="300"
                                            data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></section>
                                    </section>
                                </section>

                                <section class="text-center form-group col-md-12">
                                    <br>
                                    <input type="submit" class="btn btn-submit btn-danger w-100 text-lg font-extrabold"
                                        value="@lang('messages.soli_send')">
                                </section>
                                <br><br><br><br>
                            </form>
                        </div>
                        <div id="Merchandising">
                            <form class="container" action="{{ route('send_product_request_collection') }}"
                                method="POST" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                                <input name="category" type="hidden" value="Merchandising">
                                <br>
                                <h4>
                                    <center>@lang('messages.detail_not_found')</center>
                                </h4>
                                <br>
                                <center>
                                    <small>¿Tienes dudas? Recuerda - <svg width="20px" height="20px" data-html="true"
                                            data-toggle="popover"
                                            data-content="
                                                                                                                                               - Si la caja está en diferentes idiomas - PAL-EU <br />
                                                                                                                                               - Si la caja está solo en Español - PAL-ESP <br />
                                                                                                                                               - Si la caja está en Alemán - PAL-DE <br />
                                                                                                                                               - Si la caja está en Italiano - PAL-ITA <br />
                                                                                                                                               - Si la caja está en Francés - PAL-FRA <br />
                                                                                                                                               - Si el juego está en inglés y tiene etiqueta con una M en el frontal - es NTSC-U <br />
                                                                                                                                               - Si el juego tiene una etiqueta en el frontal marcada con USK es PAL-UK<br />
                                                                                                                                               - Si la portada e información del juego está en japonés es NTSC-J"
                                            data-placement="top" data-trigger="hover" viewBox="0 0 76 76"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path id="greyshadow" d="M8 8h68v68h-68z" fill="#BFBFBF" />
                                            <path id="blackborder" d="M4 4h68v68h-68z" fill="#000" />
                                            <path id="background" d="M4 4h64v64h-64z" fill="#FFC07C" />
                                            <path id="borderlefttop" d="M4 0h64M0 4v64" stroke="#DE5917"
                                                stroke-width="8" />
                                            <path id="rivets" d="M8 8h4v4h-4zM60 60h4v4h-4zM8 60h4v4h-4zM60 8h4v4h-4z"
                                                fill="#000" />
                                            <path id="questionshadow"
                                                d="M24 20h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM36 52h8v8h-8z"
                                                fill="#000" />
                                            <path id="question"
                                                d="M20 16h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM32 48h8v8h-8z"
                                                fill="#DE5917" />
                                        </svg>
                                    </small>
                                </center>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label"> @lang('messages.name')</label>
                                        <input type="text" class="form-control" name="product" type="text"
                                            placeholder="Club Nintendo" required="" autocomplete="off">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">@lang('messages.platform')</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            id="platform_merchandising" name="platform">
                                            <option value="">@lang('messages.selects')</option>
                                            @foreach ($platform as $p)
                                                <option value="{{ $p->value }}">{{ $p->value }}</option>
                                            @endforeach
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">Region</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            id="region_merchandising" name="region" required>
                                            <option value="">@lang('messages.selects')</option>
                                            @foreach ($region as $r)
                                                <option value="{{ $r->value }}">{{ $r->value }}</option>
                                            @endforeach
                                        </select>
                                    </section>
                                </section>
                                <br>
                                <h4>
                                    <center>@lang('messages.detail_product_sell')</center>
                                </h4>
                                <br>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label" for="name5">{{ __('Cantidad') }}</label>
                                        <input type="number" name="quantity" class="form-control" min="1"
                                            value="1" required placeholder="Enter your name" autocomplete="off">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </section>
                                </section>
                                <br>
                                <h5>
                                    @lang('messages.conditions_product')
                                </h5>
                                <br>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">@lang('messages.box')</label>
                                        <select style="width: 100%" class="form-control form-select" name="box_condition"
                                            required>
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">@lang('messages.products')</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            name="game_condition" required>
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <label class="form-label">Extras</label>
                                        <select style="width: 100%" class="form-control form-select"
                                            name="extra_condition" required>
                                            <option value="" disabled selected hidden>@lang('messages.selects')
                                            </option>

                                            <option value="NEW">@lang('messages.new_inventory')</option>
                                            <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                            <option value="USED">@lang('messages.used_inventory')</option>
                                            <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                            <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                            <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                        </select>
                                    </section>
                                </section>
                                <br>
                                <h5 class="text-sm font-medium pb-2">Agrega imagenes de tu producto.
                                    <font color="red">Máximo 8 - Obligatorio</font>
                                </h5>

                                <section style="display:none" id="fileupload-files-merchandising"
                                    data-name="image_path[]">
                                </section>
                                <section id="dropzone-merchandising">
                                    <section class="dz-message text-dark" data-dz-message>
                                        <span class="retro">@lang('messages.image_sell_mobile')</span>
                                    </section>
                                </section>

                                <br>
                                <h5 class="text-sm font-medium pb-2">Agregar Codigo de Barras de tu producto (Opcional)
                                </h5>

                                <section style="display:none" id="fileupload-files-ean-merchandising"
                                    data-name="image_ean[]">
                                </section>
                                <section id="dropzone-ean-merchandising">
                                    <section class="dz-message text-dark" data-dz-message>
                                        <span class="retro">@lang('messages.image_sell_mobile')</span>
                                    </section>
                                </section>

                                <br>

                                <section class="form-group boxed">
                                    <section class="input-wrapper">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                        <label class="form-label" for="city6"> {{ __('Comentarios') }}</label>
                                        <textarea id="comments" name="comments" rows="4" class="form-control" placeholder="Comentario"></textarea>
                                        <section class="textarea-counter" data-text-max="300"
                                            data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></section>
                                    </section>
                                </section>

                                <section class="text-center form-group col-md-12">
                                    <br>
                                    <input type="submit" class="btn btn-submit btn-danger w-100 text-lg font-extrabold"
                                        value="@lang('messages.soli_send')">
                                </section>
                                <br><br><br><br>
                            </form>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>
@endsection

@section('content-script-include')
    <script>
        $(document).ready(function() {
            //$("#gate").val('Gateway 2');
            //hide all in target div
            $("div", "div#target").hide();
            $("div#" + 'inicio').show();
            $("select#type").change(function() {
                // hide previously shown in target div
                $("div", "div#target").hide();
                //$("select#type").val('Juegos');
                // read id from your select
                var value = $(this).val();

                console.log(value);

                // show rlrment with selected id
                $("div#" + value).show();
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.comments').popover();
            $('[data-toggle="popover"]').popover();
        });
    </script>

    <script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>

    <script src="{{ asset('assets/dropzone-master/dist/min/dropzone.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/quagga/0.12.1/quagga.js"></script>
    <script src="{{ asset('vendor/sweetalert2/sweetalert2.all.min.js') }}"></script>
    <script>
        var global_lang = [];
        global_lang['app.uploader_file_size_bigger'] = 'file bigger';
        global_lang['app.uploader_file_ext_not_supported'] = 'ext not supported';
    </script>

    <script>
        window.addEventListener('show-form-ultimate', event => {
            document.getElementById("errorgm").click();
        })
    </script>
    <!-- Juego -->
    <script>
        var acceptedFileTypes = "image/*"; 
        var fileList = new Array;
        var i = 0;
        var myDropzone = $("#dropzone").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('/account/inventory/upload_file_request') }}",
            addRemoveLinks: true,
            maxFiles: 8,
            maxfilesexceeded: function(file) {
                Swal.fire({
                    title: '<span class="retro">Error</span>',
                    text: 'Haz Alcanzado el maximo de Imagenes (Max 8)',
                    footer: '<a class="retro" href="/site/guia-de-venta" target="_blank">¿Como Vender En RGM?</a>',
                })
                this.removeFile(file);
            },
            acceptedFiles: ".jpg,.jpeg,.png",
            dictInvalidFileType: "No puedes subir archivos de este tipo , solo Imagenes tipo PNG O JPG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Drop files here to upload",
            init: function() {
                myDropzone = this;
                $(this.element).addClass("dropzone");
                this.on("success", function(file, serverFileName) {
                    fileList[i] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": i
                    };
                    input = $('#fileupload-files').data('name');
                    $('#fileupload-files').append(
                        `<input type="hidden" id="file_${ i }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    i += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileList.length; f++) {
                        if (fileList[f].fileName == file.name) {
                            rmvFile = fileList[f].serverFileName;
                            $("#file_" + fileList[f].fileId).remove();
                        }
                    }
                });
            }
        });


        j = 0;
        var fileListEan = new Array;
        var myDropzone = $("#dropzone-ean").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('/account/inventory/upload_file_ean') }}",
            addRemoveLinks: true,
            maxFiles: 8, //change limit as per your requirements
            dictMaxFilesExceeded: "Maximum upload limit reached",
            acceptedFiles: "image/*",
            dictInvalidFileType: "upload only JPG/PNG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Drop files here to upload",
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileListEan[j] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": j
                    };
                    input = $('#fileupload-files-ean').data('name');
                    $('#fileupload-files-ean').append(
                        `<input type="hidden" id="file_${ j }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    j += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileListEan.length; f++) {
                        if (fileListEan[f].fileName == file.name) {
                            rmvFile = fileListEan[f].serverFileName;
                            $("#file_" + fileListEan[f].fileId).remove();
                        }
                    }
                });
                //here

            }
        });
    </script>

    <!-- Consolas -->
    <script>
        var acceptedFileTypes = "image/*";  //dropzone requires this param be a comma separated list
        var fileListConsole = new Array;
        var a = 0;
        var myDropzone = $("#dropzone-console").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('/account/inventory/upload_file_request') }}",
            addRemoveLinks: true,
            maxFiles: 8, //change limit as per your requirements
            maxfilesexceeded: function(file) {
                Swal.fire({
                    title: '<span class="retro">Error</span>',
                    text: 'Haz Alcanzado el maximo de Imagenes (Max 8)',
                    footer: '<a class="retro" href="/site/guia-de-venta" target="_blank">¿Como Vender En RGM?</a>',
                    imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/a6cff967-2930-4785-a8b8-48a6e39101b8/dbrz0sh-36c848a3-b37d-4fd1-8d7e-90a27dcb8130.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2E2Y2ZmOTY3LTI5MzAtNDc4NS1hOGI4LTQ4YTZlMzkxMDFiOFwvZGJyejBzaC0zNmM4NDhhMy1iMzdkLTRmZDEtOGQ3ZS05MGEyN2RjYjgxMzAuZ2lmIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.QwzwRFsxRbUrhacQuJV4fSAij3GxImG_4dkxWDTgkMc',
                    imageWidth: 400,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                })
                this.removeFile(file);
            },
            acceptedFiles: ".jpg,.jpeg,.png",
            dictInvalidFileType: "No puedes subir archivos de este tipo , solo Imagenes tipo PNG O JPG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Drop files here to upload",
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileListConsole[a] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": a
                    };
                    input = $('#fileupload-files-console').data('name');
                    $('#fileupload-files-console').append(
                        `<input type="hidden" id="file_${ a }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    i += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileListConsole.length; f++) {
                        if (fileListConsole[f].fileName == file.name) {
                            rmvFile = fileListConsole[f].serverFileName;
                            $("#file_" + fileListConsole[f].fileId).remove();
                        }
                    }
                });
            }
        });

        b = 0;
        var fileListConsoleEan = new Array;
        var myDropzone = $("#dropzone-ean-console").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('/account/inventory/upload_file_ean') }}",
            addRemoveLinks: true,
            maxFiles: 8, //change limit as per your requirements
            dictMaxFilesExceeded: "Maximum upload limit reached",
            acceptedFiles: "image/*",
            dictInvalidFileType: "upload only JPG/PNG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Drop files here to upload",
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileListConsoleEan[b] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": b
                    };
                    input = $('#fileupload-files-ean-console').data('name');
                    $('#fileupload-files-ean-console').append(
                        `<input type="hidden" id="file_${ b }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    j += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileListConsoleEan.length; f++) {
                        if (fileListConsoleEan[f].fileName == file.name) {
                            rmvFile = fileListConsoleEan[f].serverFileName;
                            $("#file_" + fileListConsoleEan[f].fileId).remove();
                        }
                    }
                });
                //here

            }
        });
    </script>

    <!-- Periféricos -->
    <script>
        var acceptedFileTypes = "image/*"; //dropzone requires this param be a comma separated list
        var fileListPeripheral = new Array;
        var c = 0;
        var myDropzone = $("#dropzone-peripheral").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('/account/inventory/upload_file_request') }}",
            addRemoveLinks: true,
            maxFiles: 8, //change limit as per your requirements
            maxfilesexceeded: function(file) {
                Swal.fire({
                    title: '<span class="retro">Error</span>',
                    text: 'Haz Alcanzado el maximo de Imagenes (Max 8)',
                    footer: '<a class="retro" href="/site/guia-de-venta" target="_blank">¿Como Vender En RGM?</a>',
                    imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/a6cff967-2930-4785-a8b8-48a6e39101b8/dbrz0sh-36c848a3-b37d-4fd1-8d7e-90a27dcb8130.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2E2Y2ZmOTY3LTI5MzAtNDc4NS1hOGI4LTQ4YTZlMzkxMDFiOFwvZGJyejBzaC0zNmM4NDhhMy1iMzdkLTRmZDEtOGQ3ZS05MGEyN2RjYjgxMzAuZ2lmIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.QwzwRFsxRbUrhacQuJV4fSAij3GxImG_4dkxWDTgkMc',
                    imageWidth: 400,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                })
                this.removeFile(file);
            },
            acceptedFiles: ".jpg,.jpeg,.png",
            dictInvalidFileType: "No puedes subir archivos de este tipo , solo Imagenes tipo PNG O JPG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Drop files here to upload",
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileListPeripheral[c] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": c
                    };
                    input = $('#fileupload-files-peripheral').data('name');
                    $('#fileupload-files-peripheral').append(
                        `<input type="hidden" id="file_${ c }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    c += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileListConsole.length; f++) {
                        if (fileListConsole[f].fileName == file.name) {
                            rmvFile = fileListConsole[f].serverFileName;
                            $("#file_" + fileListConsole[f].fileId).remove();
                        }
                    }
                });
            }
        });

        d = 0;
        var fileListPeripheralEan = new Array;
        var myDropzone = $("#dropzone-ean-peripheral").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('/account/inventory/upload_file_ean') }}",
            addRemoveLinks: true,
            maxFiles: 8, //change limit as per your requirements
            dictMaxFilesExceeded: "Maximum upload limit reached",
            acceptedFiles: "image/*",
            dictInvalidFileType: "upload only JPG/PNG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Drop files here to upload",
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileListEan[d] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": d
                    };
                    input = $('#fileupload-files-ean-peripheral').data('name');
                    $('#fileupload-files-ean-peripheral').append(
                        `<input type="hidden" id="file_${ d }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    d += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileListPeripheralEan.length; f++) {
                        if (fileListPeripheralEan[f].fileName == file.name) {
                            rmvFile = fileListPeripheralEan[f].serverFileName;
                            $("#file_" + fileListPeripheralEan[f].fileId).remove();
                        }
                    }
                });
                //here

            }
        });
    </script>


    <!-- Accesorios -->
    <script>
        var acceptedFileTypes = "image/*"; //dropzone requires this param be a comma separated list
        var fileListAccesories = new Array;
        var h = 0;
        var myDropzone = $("#dropzone-accesories").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('/account/inventory/upload_file_request') }}",
            addRemoveLinks: true,
            maxFiles: 8, //change limit as per your requirements
            maxfilesexceeded: function(file) {
                Swal.fire({
                    title: '<span class="retro">Error</span>',
                    text: 'Haz Alcanzado el maximo de Imagenes (Max 8)',
                    footer: '<a class="retro" href="/site/guia-de-venta" target="_blank">¿Como Vender En RGM?</a>',
                    imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/a6cff967-2930-4785-a8b8-48a6e39101b8/dbrz0sh-36c848a3-b37d-4fd1-8d7e-90a27dcb8130.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2E2Y2ZmOTY3LTI5MzAtNDc4NS1hOGI4LTQ4YTZlMzkxMDFiOFwvZGJyejBzaC0zNmM4NDhhMy1iMzdkLTRmZDEtOGQ3ZS05MGEyN2RjYjgxMzAuZ2lmIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.QwzwRFsxRbUrhacQuJV4fSAij3GxImG_4dkxWDTgkMc',
                    imageWidth: 400,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                })
                this.removeFile(file);
            },
            acceptedFiles: ".jpg,.jpeg,.png",
            dictInvalidFileType: "No puedes subir archivos de este tipo , solo Imagenes tipo PNG O JPG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Drop files here to upload",
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileListAccesories[h] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": h
                    };
                    input = $('#fileupload-files-accesories').data('name');
                    $('#fileupload-files-accesories').append(
                        `<input type="hidden" id="file_${ h }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    h += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileListAccesories.length; f++) {
                        if (fileListAccesories[f].fileName == file.name) {
                            rmvFile = fileListAccesories[f].serverFileName;
                            $("#file_" + fileListAccesories[f].fileId).remove();
                        }
                    }
                });
            }
        });

        p = 0;
        var fileListAccesoriesEan = new Array;
        var myDropzone = $("#dropzone-ean-accesories").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('/account/inventory/upload_file_ean') }}",
            addRemoveLinks: true,
            maxFiles: 8, //change limit as per your requirements
            dictMaxFilesExceeded: "Maximum upload limit reached",
            acceptedFiles: acceptedFileTypes,
            dictInvalidFileType: "upload only JPG/PNG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Drop files here to upload",
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileListEan[p] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": p
                    };
                    input = $('#fileupload-files-ean-accesories').data('name');
                    $('#fileupload-files-ean-accesories').append(
                        `<input type="hidden" id="file_${ p }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    p += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileListAccesoriesEan.length; f++) {
                        if (fileListAccesoriesEan[f].fileName == file.name) {
                            rmvFile = fileListAccesoriesEan[f].serverFileName;
                            $("#file_" + fileListAccesoriesEan[f].fileId).remove();
                        }
                    }
                });
                //here

            }
        });
    </script>

    <!-- Merchandising -->
    <script>
        var acceptedFileTypes = "image/*"; //dropzone requires this param be a comma separated list
        var fileListMerchandising = new Array;
        var m = 0;
        var myDropzone = $("#dropzone-merchandising").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('/account/inventory/upload_file_request') }}",
            addRemoveLinks: true,
            maxFiles: 8, //change limit as per your requirements
            maxfilesexceeded: function(file) {
                Swal.fire({
                    title: '<span class="retro">Error</span>',
                    text: 'Haz Alcanzado el maximo de Imagenes (Max 8)',
                    footer: '<a class="retro" href="/site/guia-de-venta" target="_blank">¿Como Vender En RGM?</a>',
                    imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/a6cff967-2930-4785-a8b8-48a6e39101b8/dbrz0sh-36c848a3-b37d-4fd1-8d7e-90a27dcb8130.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2E2Y2ZmOTY3LTI5MzAtNDc4NS1hOGI4LTQ4YTZlMzkxMDFiOFwvZGJyejBzaC0zNmM4NDhhMy1iMzdkLTRmZDEtOGQ3ZS05MGEyN2RjYjgxMzAuZ2lmIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.QwzwRFsxRbUrhacQuJV4fSAij3GxImG_4dkxWDTgkMc',
                    imageWidth: 400,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                })
                this.removeFile(file);
            },
            acceptedFiles: ".jpg,.jpeg,.png",
            dictInvalidFileType: "No puedes subir archivos de este tipo , solo Imagenes tipo PNG O JPG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Drop files here to upload",
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileListMerchandising[m] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": m
                    };
                    input = $('#fileupload-files-merchandising').data('name');
                    $('#fileupload-files-merchandising').append(
                        `<input type="hidden" id="file_${ m }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    m += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileListMerchandising.length; f++) {
                        if (fileListMerchandising[f].fileName == file.name) {
                            rmvFile = fileListMerchandising[f].serverFileName;
                            $("#file_" + fileListMerchandising[f].fileId).remove();
                        }
                    }
                });
            }
        });

        n = 0;
        var fileLisMerchandisingEan = new Array;
        var myDropzone = $("#dropzone-ean-merchandising").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('/account/inventory/upload_file_ean') }}",
            addRemoveLinks: true,
            maxFiles: 8, //change limit as per your requirements
            dictMaxFilesExceeded: "Maximum upload limit reached",
            acceptedFiles: acceptedFileTypes,
            dictInvalidFileType: "upload only JPG/PNG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Drop files here to upload",
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileLisMerchandisingEan[n] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": n
                    };
                    input = $('#fileupload-files-ean-merchandising').data('name');
                    $('#fileupload-files-ean-merchandising').append(
                        `<input type="hidden" id="file_${ n }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    n += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileLisMerchandisingEan.length; f++) {
                        if (fileLisMerchandisingEan[f].fileName == file.name) {
                            rmvFile = fileLisMerchandisingEan[f].serverFileName;
                            $("#file_" + fileLisMerchandisingEan[f].fileId).remove();
                        }
                    }
                });
                //here

            }
        });
    </script>

    <script>
        $(document).ready(function() {
            $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                maxFileCount: 8
            });
            $("#input-barcode20").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                maxFileCount: 8
            });
            $("#input-19").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                maxFileCount: 8
            });
            $("#input-barcode19").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                maxFileCount: 8
            });
            $("#input-18").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                maxFileCount: 8
            });
            $("#input-barcode18").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                maxFileCount: 8
            });
            $("#input-17").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                maxFileCount: 8
            });
            $("#input-barcode17").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                maxFileCount: 8
            });
            $("#input-16").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                maxFileCount: 8
            });
            $("#input-barcode16").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                maxFileCount: 8
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#region_merchandising').select2();
            $('#platform_merchandising').select2();
            $('#region_accesorio').select2();
            $('#platform_accesorio').select2();
            $('#platform_periferico').select2();
            $('#region_periferico').select2();
            $('#region_platform').select2();
            $('#consola_platform').select2();
            $('#platform').select2();
            $('#region').select2();
        });
    </script>
@endsection
