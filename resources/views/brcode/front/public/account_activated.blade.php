@extends('brcode.front.layout.app')
@section('content-css-include')
<!-- File Uploader -->
<link rel="stylesheet" href="{{ asset('assets/jQuery-FileUpload/css/jquery.fileupload.css') }}">
@endsection
@section('content')
<!-- Content Header (Page header) -->

<!-- Main content -->
<section  class="content">
	<div class="container">
	  <div class="row">
	    <div class="col-md-12">
	      <div class="account-title">{{ __('Cuenta activada!')  }}</div>
	    </div>
	  </div><!-- /.row -->
	  <div class="row">
	    
	  </div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.content -->
@endsection

@section('content-script-include')
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
<!-- The basic File Upload plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
<!-- The File Upload processing plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>
@endsection
@section('content-script')
@endsection
