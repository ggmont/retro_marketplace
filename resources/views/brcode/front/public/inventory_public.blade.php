@extends('brcode.front.layout.app')
@section('content-css-include')
@livewireStyles
<style>
           .tabs {
            overflow: hidden;
            }
            .tabs-content {
            max-height: 0;
            transition: all 0.5s;
            }
            input:checked + .tabs-label .test {
                background-color: #000;
            }
            input:checked + .tabs-label .test svg {
                transform: rotate(180deg);
                stroke: #fff;
            }
            input:checked + .tabs-label::after {
            transform: rotate(90deg);
            }
            input:checked ~ .tabs-content {
            max-height: 100vh;
            }
    .demo {
        margin: 30px auto;
        max-width: 960px;
    }

    .demo>li {
        float: left;
    }

    .demo>li img {
        width: 220px;
        margin: 10px;
        cursor: pointer;
    }

    .item {
        transition: .5s ease-in-out;
    }

    .item:hover {
        filter: brightness(80%);
    }

    .retro-guide {
        font-family: 'Jost', sans-serif;
            font-size: 17px;
            font-weight: 700;
    }

</style>

@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <div class="nk-gap-2"></div>
    <!-- Main content -->
    <div class="container">
        <div class="nk-main">
            <!-- START: Breadcrumbs -->
            <div class="nk-gap-1"></div>
            <div class="container">
                <ul class="nk-breadcrumbs">

                    <li>

                        <div class="section-title1">
                            <h3 class="retro">@lang('messages.inventory_of') {{ ucwords($user) }}</h3>
                        </div>

                    </li>
                </ul>
            </div>
    


        </div>




        @php( $sellers = [])
        @php( $title_game = [
            'NEW' => "Nuevo",
            'USED-NEW' => "Usado como nuevo",
            'USED' => "Usado",
            'USED-VERY' => "Muy usado" ,
            'NOT-WORK' => "No funciona",
            'NOT-PRES' => "No aplica"
        ])

        @php( $country = [
            'Spain' => "España",
            'Andorra' => "Andorra",
             'Åland Islands' => "Åland Islands",
             'Albania' => "Albania",
             'Austria' => "Austria",
             'Belarus' => "Belarus",
             'Belgium' => "Belgium",
             'Bosnia and Herzegovina' => "Bosnia and Herzegovina",
             'Bulgaria' => "Bulgaria",
             'Croatia' => "Croatia",
             'Czech Republic' => "Czech Republic",
             'Denmark' => "Denmark",
             'Estonia' => "Estonia",
             'Faroe Islands' => "Faroe Islands",
              'Finland' => "Finland",
              'France' => "France",
              'Germany' => "Germany",
              'Gibraltar' => "Gibraltar",
              'Guernsey' => "Guernsey",
              'Holy See' => "Holy See",
              'Hungary' => "Hungary",
              'Iceland' => "Iceland",
              'Ireland' => "Ireland",
              'Isle of Man' => "Isle of Man",
              'Italy' => "Italy",
              'Jersey' => "Jersey",
              'Latvia' => "Latvia",
              'Liechtenstein' => "Liechtenstein",
              'Lithuania' => "Lithuania",
              'Luxembourg' => "Luxembourg",
              'Macedonia (the former Yugoslav Republic of)' => "Macedonia (the former Yugoslav Republic of)",
              'Malta' => "Malta",
              'Moldova (Republic of)' => "Moldova (Republic of)",
              'Monaco' => "Monaco",
              'Montenegro' => "Montenegro",
              'Netherlands' => "Netherlands",
              'Norway' => "Norway",
              'Poland' => "Poland",
              'Portugal' => "Portugal",
              'Romania' => "Romania",
              'Russian Federation' => "Russian Federation",
              'San Marino' => "San Marino",
              'Serbia' => "Serbia",
              'Slovakia' => "Slovakia",
              'Slovenia' => "Slovenia",
              'Svalbard and Jan Mayen' => "Svalbard and Jan Mayen",
              'Sweden' => "Sweden",
              'Switzerland' => "Switzerland",
              'Ukraine' => "Ukraine",
              'United Kingdom of Great Britain and Northern Ireland' => "United Kingdom of Great Britain and Northern Ireland"
        ])

        @php( $game_conditions = [])
        @php( $box_conditions = [])
        @php( $countrys = [])
        @php( $manual_conditions = [])
        @php( $extra_conditions = [])
        @php( $cover_conditions = [])
        @php( $inside_conditions = [])


        <div class="col-lg-12">
            <div class="nk-gap"></div>

            <div class="nk-tabs">

                <ul class="nav nav-tabs" role="tablist">
                    @if ((new \Jenssegers\Agent\Agent())->isMobile())
                    &nbsp;&nbsp;&nbsp;
                    @else                    
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp; &nbsp;&nbsp;
                    @endif
                    <li class="nav-item">
                        <a class="nav-link active" href="#tabs-1-1" role="tab" data-toggle="tab"><span
                                class="text-base retro-sell">@lang('messages.games')</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-2" role="tab" data-toggle="tab"><span
                                class="text-base retro-sell">@lang('messages.consoles')</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-3" role="tab" data-toggle="tab"><span
                                class="text-base retro-sell">@lang('messages.peripherals')</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-4" role="tab" data-toggle="tab"><span
                                class="text-base retro-sell">@lang('messages.accesories')</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-5" role="tab" data-toggle="tab"><span
                                class="text-base retro-sell">@lang('messages.merch')</span></a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade show active" id="tabs-1-1">
                        <div class="nk-gap"></div>
                        @livewire('public-table.inventory.inventory-public-user',['user' => $user])
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tabs-1-2">
                        <div class="nk-gap"></div>
                        @livewire('public-table.inventory.inventory-public-consola-user',['user' => $user])
                        <div class="nk-gap"></div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tabs-1-3">
                        <div class="nk-gap"></div>
                        @livewire('public-table.inventory.inventory-public-periferico-user',['user' => $user])
                        <div class="nk-gap"></div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tabs-1-4">
                        <div class="nk-gap"></div>
                        @livewire('public-table.inventory.inventory-public-accesorio-user',['user' => $user])
                        <div class="nk-gap"></div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tabs-1-5">
                        <div class="nk-gap"></div>
                        @livewire('public-table.inventory.inventory-public-merchadising-user',['user' => $user])
                        <div class="nk-gap"></div>
                    </div>
                </div>
            </div>
            <!-- END: Tabs -->
        </div>




    </div><!-- /.container -->
@endsection

@section('content-script-include')
    <script src="{{ asset('brcode/js/pty.components.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    @livewireScripts

    <script>
        Livewire.on('pop', function() {


            var $inventoryAdd = $('.br-btn-product-add');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('console', function() {


            var $inventoryAdd = $('.br-btn-product-add-console');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('periferico', function() {


            var $inventoryAdd = $('.br-btn-product-add-periferico');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('accesorio', function() {


            var $inventoryAdd = $('.br-btn-product-add-accesorio');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('merchandising', function() {


            var $inventoryAdd = $('.br-btn-product-add-merchandising');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

<script>
    Livewire.on('alert', function() {
        Swal.fire(
            'Listo!',
            '<span class="retro">Tu Producto fue eliminado</span>',
            'success'
        )
    })
</script>
@endsection

