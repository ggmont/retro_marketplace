@extends('brcode.front.layout.app_conversation')
@section('content-css-include')
    @livewireStyles
@endsection

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div class="header-area" id="headerArea">
            <div class="container h-100 d-flex align-items-center justify-content-between">
                <!-- Back Button-->
                <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                <!-- Page Title-->
                <div class="page-heading">
                    <h6 class="mb-0 font-extrabold">Subir/Vender Producto</h6>
                </div>
                <!-- Navbar Toggler-->

                @if (Auth::user())
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @else
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @endif
            </div>
        </div>
        <div id="appCapsule">
            <div class="section mt-2">
                <h4 class="text-center">¿Cómo quieres publicar tu anuncio?</h4>
                <br>

                <h4>Precio Fijo</h4>
                <p>
                    ¿Tienes claro cuánto vale?
                    <br>
                    Rellena el formulario y establece un precio fijo a tu producto. Sin límite de tiempo.
                </p>
                <div class="text-center">
                    <a href="{{ route('fixed_price_select') }}" class="btn btn-danger w-100 font-bold">PRECIO FIJO</a>
                </div>
                <br>

                <h4>Quiero escuchar ofertas</h4>
                <p>
                    ¿No sabes cuánto vale? <br>
                    Utiliza este método para recibir ofertas durante 24h. El precio empieza por 1€ y podrás aceptar o
                    rechazar ofertas sin necesidad de negociar por chat. Nunca recibirás ofertas más bajas que la actual.<br>
                    Puedes borrar tu artículo en cualquier momento.
                </p>
                <div class="text-center">
                    <a href="{{ route('auction_select') }}" class="btn btn-danger w-100 font-bold">QUIERO ESCUCHAR OFERTAS</a>
                </div>
            </div>

        </div>
    @else
    @endif
@endsection

@section('content-script-include')
    @livewireScripts
@endsection
@section('content-script')
@endsection
