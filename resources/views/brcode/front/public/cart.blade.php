@extends('brcode.front.layout.app_conversation')
@section('content-css-include')
    <style>
        body {
            margin-top: 20px;

        }

        h3 {
            font-size: 16px;
        }

        .text-navy {
            color: #1ab394;
        }

        .cart-product-imitation {
            text-align: center;
            padding-top: 30px;
            height: 80px;
            width: 80px;
            background-color: #f8f8f9;
        }

        .product-imitation.xl {
            padding: 120px 0;
        }

        .product-desc {
            padding: 20px;
            position: relative;
        }

        .ecommerce .tag-list {
            padding: 0;
        }

        .ecommerce .fa-star {
            color: #d1dade;
        }

        .ecommerce .fa-star.active {
            color: #f8ac59;
        }

        .ecommerce .note-editor {
            border: 1px solid #e7eaec;
        }

        table.shoping-cart-table {
            margin-bottom: 0;
        }

        table.shoping-cart-table tr td {
            border: none;
            text-align: right;
        }

        table.shoping-cart-table tr td.desc,
        table.shoping-cart-table tr td:first-child {
            text-align: left;
        }

        table.shoping-cart-table tr td:last-child {
            width: 80px;
        }

        .ibox {
            clear: both;
            margin-bottom: 25px;
            margin-top: 0;
            padding: 0;
        }

        .ibox.collapsed .ibox-content {
            display: none;
        }

        .ibox:after,
        .ibox:before {
            display: table;
        }

        .ibox-title {
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            background-color: #ffffff;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 3px 0 0;
            color: inherit;
            margin-bottom: 0;
            padding: 14px 15px 7px;
            min-height: 48px;
        }

        .ibox-content {
            background-color: #ffffff;
            color: inherit;
            padding: 15px 20px 20px 20px;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 1px 0;
        }

        .ibox-footer {
            color: inherit;
            border-top: 1px solid #e7eaec;
            font-size: 90%;
            background: #ffffff;
            padding: 10px 15px;
        }

        /* width */
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #888;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555;
        }

        .scroller {
            width: 100%;
            max-width: 1120px;
            height: 1000px;
            overflow: auto;
            overflow-x: hidden;
        }

        .promotion {
            background: radial-gradient(ellipse farthest-corner at right bottom, #FEDB37 0%, #FDB931 8%, #9f7928 30%, #8A6E2F 40%),
                radial-gradient(ellipse farthest-corner at left top, #FFFFFF 0%, #FFFFAC 8%, #D1B464 25%, #5d4a1f 62.5%, #5d4a1f 100%);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }
    </style>
@endsection
@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div class="header-area" id="headerArea">
            <div class="container h-100 d-flex align-items-center justify-content-between">
                <!-- Back Button-->
                <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                <!-- Page Title-->
                <div class="page-heading">
                    <h6 class="mb-0 font-extrabold">@lang('messages.my_cart')</h6>
                </div>
                <!-- Navbar Toggler-->

                @if (Auth::user())
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @else
                    <a href="#" id="showLogin" class="showLogin" class="lni lni-user d-flex flex-wrap"
                        data-nav-toggle="#nk-nav-mobile">
                        <span class="lni lni-user">
                            <span></span><span></span><span></span>
                        </span>
                    </a>
                @endif
            </div>
        </div>


        <span class="retro">
            @include('partials.flash')
        </span>
        @if ($viewData['cart_new']->details->count() > 0)
            @php($total1 = 0)
            @php($art = 0)
            @foreach ($viewData['products_in_cart'] as $cartItem)
                @php($total1 += $cartItem->price * $cartItem->qty)
                @php($art += $cartItem->qty)
            @endforeach
            <div class="row" id="cart-order-user-detail">
                <div class="col-md-12">
                    @if ($month == 0)
                    @endif
                </div>
                <div class="page-content-wrapper">
                    <div class="container">
                        <br><br>
                        <div class="listview-title mt-2">Detalles Generales</div>
                        <ul class="listview image-listview">
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">{{ __('Pedidos') }}</div>
                                        <span class="text-muted text-sm font-semibold">{{ count($us) }}</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">{{ __('Artículos') }}</div>
                                        <span class="text-muted text-sm font-semibold">{{ $art }}</span>
                                    </div>
                                </div>
                            </li>

                            @if ($historial_promotion_count > 0)
                                @foreach ($historial_promotion as $h)
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('Precio total ítems') }}</div>
                                                @php($special_promotion = (Auth::user()->cart->totalCart * $h->promotion->special) / 100)
                                                @php($final_special = Auth::user()->cart->totalCart - $special_promotion)
                                                <span
                                                    class="text-muted text-sm font-semibold">{{ number_format($final_special, 2) }}
                                                    €</span>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            @else
                                <li>
                                    <div class="item" bis_skin_checked="1">
                                        <div class="in" bis_skin_checked="1">
                                            <div bis_skin_checked="1">{{ __('Precio total ítems') }}</div>
                                            <span
                                                class="text-muted text-sm font-semibold">{{ number_format(Auth::user()->cart->totalCart, 2) }}
                                                € </span>
                                        </div>
                                    </div>
                                </li>
                            @endif



                        </ul>

                        <div class="listview-title mt-2">Dirección de envío</div>
                        <ul class="listview image-listview">
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">{{ __('Para') }}</div>
                                        <span class="text-muted text-sm font-semibold">

                                            @if (!empty(Auth::user()->first_name) && !empty(Auth::user()->last_name))
                                                {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
                                            @else
                                                -
                                            @endif


                                        </span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">{{ __('Dirección') }}</div>
                                        <span class="text-muted text-sm font-semibold">
                                            @if (!empty(Auth::user()->address))
                                                {{ Auth::user()->address }}
                                            @else
                                                -
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">{{ __('Código postal') }}</div>
                                        <span class="text-muted text-sm font-semibold">
                                            @if (!empty(Auth::user()->zipcode))
                                                {{ Auth::user()->zipcode }}
                                            @else
                                                -
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">{{ __('País') }}</div>
                                        <span class="text-muted text-sm font-semibold">
                                            @if (!empty(Auth::user()->country->name))
                                                {{ Auth::user()->country->name }}
                                            @else
                                                -
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="item" bis_skin_checked="1">
                                    <div class="in" bis_skin_checked="1">
                                        <div bis_skin_checked="1">{{ __('Ciudad') }}</div>
                                        <span class="text-muted text-sm font-semibold">
                                            @if (!empty(Auth::user()->city))
                                                {{ Auth::user()->city }}
                                            @else
                                                -
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </li>

                        </ul>

                        <section class="text-center form-group col-md-12">
                            <br>
                            <a href="{{ route('config_personal_data') }}"
                                class="btn btn-submit btn-danger w-100 text-lg font-extrabold">
                                Actualizar dirección de envío
                            </a>
                        </section>


                        <form action="{{ url('/cartComplete') }}" method="post">
                            {{ csrf_field() }}
                            @if ($historial_promotion)
                                @foreach ($historial_promotion as $h)
                                    <input name="promotion_code" type="hidden" value="{{ $h->promotion->code }}">
                                @endforeach
                            @endif

                            <div class="card coupon-card mb-3">
                                <div class="card-body">
                                    <div class="apply-coupon">
                                        <h6 class="mb-0">Verificar</h6>
                                        <p class="mb-1">Por favor revise el contenido de su carrito de compras.
                                            Luego ingrese su contraseña en la casilla de verificación y toque
                                            en Comprometer para comprar.<br><br>
                                        </p>
                                        <br>
                                        <div class="coupon-form">

                                            <input class="form-control" type="password" name="pass"
                                                placeholder="Tu contraseña">
                                            <button class="btn btn-warning" type="submit"><span
                                                    class="text-white">Reservar</span></button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        @php($order_num = 0)
                        @if (count($viewData['products_in_cart']) > 0)
                            @foreach ($viewData['user_order'] as $key)
                                @php($qty_det = 0)
                                @php($det_total = 0)
                                @php($iva = 0)
                                @php($det_vol = 0)
                                @php($det_wei = 0)
                                @php($totalWeight = 0)
                                @php($totalSize = 0)
                                @php($total = 0)
                                @php($shipping_cost = 0)


                                @foreach ($viewData['products_in_cart'] as $cartItem)
                                    @if ($cartItem->user->user_name == $key->user_name)
                                        @php($qty_det += $cartItem->qty)
                                    @endif
                                @endforeach


                                @foreach ($viewData['products_in_cart'] as $cartItem)
                                    @if ($cartItem->user->user_name == $key->user_name)
                                        @php($det_total += $cartItem->qty * $cartItem->price)
                                        @php($iva = $det_total * 0.04)

                                        @if ($cartItem->kg !== null)
                                            @php($det_wei += $cartItem->qty * $cartItem->kg)
                                            @php($totalWeight += $cartItem->qty * ($cartItem->kg / 1000))
                                        @else
                                            @php($det_wei += $cartItem->qty * $cartItem->product->weight)
                                            @php($totalWeight += $cartItem->qty * ($cartItem->product->weight / 1000))
                                        @endif

                                        @if ($cartItem->size === null)
                                            @if (isset($cartItem->product->width) && isset($cartItem->product->large) && isset($cartItem->product->high))
                                                @php($totalSize += $cartItem->qty * ($cartItem->product->width + $cartItem->product->large + $cartItem->product->high))
                                            @endif
                                        @elseif ($cartItem->size == 0)
                                            @php($totalSize += $cartItem->qty * (15 + 15 + 20))
                                        @elseif ($cartItem->size == 1)
                                            @php($totalSize += $cartItem->qty * (20 + 20 + 30))
                                        @elseif ($cartItem->size == 2)
                                            @php($totalSize += $cartItem->qty * (25 + 25 + 45))
                                        @elseif ($cartItem->size == 3)
                                            @php($totalSize += $cartItem->qty * (45 + 45 + 30))
                                        @elseif ($cartItem->size == 4)
                                            @php($totalSize += $cartItem->qty * (50 + 50 + 50))
                                        @endif
                                    @endif
                                @endforeach








                                @php($order_num += 1)

                                @foreach ($viewData['shipping_values'] as $val)
                                    @if ($val->user_id == $key->id)
                                        @php($shipping_user = $val->shipping_id)
                                    @endif
                                @endforeach

                                @php($showShippingSelect = $key->country_id != 46 || Auth::user()->country_id != 46)

                                <div class="listview-title mt-2">Detalles del Pedido #0000{{ $order_num }}</div>
                                <ul class="listview image-listview">
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('Comprador') }}</div>
                                                <span class="text-muted text-sm font-semibold">
                                                    @if (Auth::user())
                                                        {{ Auth::user()->user_name }}
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('Vendedor') }}</div>
                                                <span class="text-muted text-sm font-semibold">
                                                    {{ $key->user_name }}
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('Cantidad de artículos') }}</div>
                                                <span class="text-muted text-sm font-semibold">
                                                    {{ $qty_det }}
                                                </span>
                                            </div>
                                        </div>
                                    </li>

                                    @if ($showShippingSelect)
                                    @else
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('Peso') }}</div>
                                                <span class="text-muted text-sm font-semibold">
                                                    {{ $det_wei }} g
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('Dimension') }}</div>
                                                <span class="text-muted text-sm font-semibold">
                                                    {{ $totalSize }}
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    @endif

                                    @if ($showShippingSelect)
                                    @else
                                        <li>
                                            <div class="item" bis_skin_checked="1">
                                                <div class="in" bis_skin_checked="1">
                                                    <div bis_skin_checked="1">{{ __('Coste de envío') }}</div>
                                                    <span class="text-muted text-sm font-semibold">
                                                        @if ($totalWeight >= 0 && $totalWeight <= 1 && $totalSize >= 0 && $totalSize <= 50)
                                                            2.90 €
                                                        @elseif($totalWeight > 1 && $totalWeight <= 3 && $totalSize <= 50)
                                                            3.90 €
                                                        @elseif($totalWeight > 3 && $totalWeight <= 5 && $totalSize <= 95)
                                                            4.90 €
                                                        @elseif($totalWeight > 5 && $totalWeight <= 10 && $totalSize <= 120)
                                                            6.90 €
                                                        @elseif($totalWeight > 10 && $totalWeight <= 20 && $totalSize <= 150)
                                                            10.90 €
                                                        @elseif($totalWeight >= 0 && $totalWeight <= 1)
                                                            @if ($totalSize >= 0 && $totalSize <= 50)
                                                                2.90 €
                                                            @elseif($totalSize >= 50 && $totalSize <= 70)
                                                                3.90 €
                                                            @elseif($totalSize >= 70 && $totalSize <= 95)
                                                                4.90 €
                                                            @elseif($totalSize >= 95 && $totalSize <= 120)
                                                                6.90 €
                                                            @elseif($totalSize >= 120 && $totalSize <= 150)
                                                                10.90 €
                                                            @endif
                                                        @elseif($totalWeight > 1 && $totalWeight <= 3)
                                                            @if ($totalSize >= 0 && $totalSize <= 50)
                                                                3.90 €
                                                            @elseif($totalSize >= 50 && $totalSize <= 70)
                                                                3.90 €
                                                            @elseif($totalSize >= 70 && $totalSize <= 95)
                                                                4.90 €
                                                            @elseif($totalSize >= 95 && $totalSize <= 120)
                                                                6.90 €
                                                            @elseif($totalSize >= 120 && $totalSize <= 150)
                                                                10.90 €
                                                            @endif
                                                        @elseif($totalWeight > 3 && $totalWeight <= 5)
                                                            @if ($totalSize >= 0 && $totalSize <= 50)
                                                                4.90 €
                                                            @elseif($totalSize >= 50 && $totalSize <= 70)
                                                                4.90 €
                                                            @elseif($totalSize >= 70 && $totalSize <= 95)
                                                                4.90 €
                                                            @elseif($totalSize >= 95 && $totalSize <= 120)
                                                                6.90 €
                                                            @elseif($totalSize >= 120 && $totalSize <= 150)
                                                                10.90 €
                                                            @endif
                                                        @elseif($totalWeight > 5 && $totalWeight <= 10)
                                                            @if ($totalSize >= 0 && $totalSize <= 50)
                                                                6.90 €
                                                            @elseif($totalSize >= 50 && $totalSize <= 70)
                                                                6.90 €
                                                            @elseif($totalSize >= 70 && $totalSize <= 95)
                                                                6.90 €
                                                            @elseif($totalSize >= 95 && $totalSize <= 120)
                                                                6.90 €
                                                            @elseif($totalSize >= 120 && $totalSize <= 150)
                                                                10.90 €
                                                            @endif
                                                        @elseif($totalWeight > 10 && $totalWeight <= 20)
                                                            @if ($totalSize >= 0 && $totalSize <= 50)
                                                                10.90 €
                                                            @elseif($totalSize >= 50 && $totalSize <= 70)
                                                                10.90 €
                                                            @elseif($totalSize >= 70 && $totalSize <= 95)
                                                                10.90 €
                                                            @elseif($totalSize >= 95 && $totalSize <= 120)
                                                                10.90 €
                                                            @elseif($totalSize >= 120 && $totalSize <= 150)
                                                                10.90 €
                                                            @endif
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                    @if ($showShippingSelect)
                                    @else
                                        <li>
                                            <div class="item" bis_skin_checked="1">
                                                <div class="in" bis_skin_checked="1">
                                                    <div bis_skin_checked="1">{{ __('Gastos de gestión') }}</div>
                                                    <span class="text-muted text-sm font-semibold">
                                                        {{ number_format($iva, 2) }} €
                                                    </span>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                    @if ($showShippingSelect)
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('Total') }}</div>
                                                <span class="text-muted text-sm font-semibold">
                                                    {{ number_format($det_total, 2) }} €
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    @else
                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in" bis_skin_checked="1">
                                                <div bis_skin_checked="1">{{ __('Total') }}</div>
                                                <span class="text-muted text-sm font-semibold">
                                                    @if ($totalWeight >= 0 && $totalWeight <= 1 && $totalSize >= 0 && $totalSize <= 50)
                                                        {{ number_format($det_total + $iva + 2.9, 2) }} €
                                                    @elseif($totalWeight > 1 && $totalWeight <= 5 && $totalSize <= 95)
                                                        {{ number_format($det_total + $iva + 4.9, 2) }} €
                                                    @elseif($totalWeight > 5 && $totalWeight <= 10 && $totalSize <= 120)
                                                        {{ number_format($det_total + $iva + 6.9, 2) }} €
                                                    @elseif($totalWeight > 10 && $totalWeight <= 20 && $totalSize <= 150)
                                                        {{ number_format($det_total + $iva + 10.9, 2) }} €
                                                    @elseif($totalWeight >= 0 && $totalWeight <= 1)
                                                        @if ($totalSize >= 0 && $totalSize <= 50)
                                                            {{ number_format($det_total + $iva + 2.9, 2) }} €
                                                        @elseif($totalSize >= 50 && $totalSize <= 95)
                                                            {{ number_format($det_total + $iva + 4.9, 2) }} €
                                                        @elseif($totalSize >= 95 && $totalSize <= 120)
                                                            {{ number_format($det_total + $iva + 6.9, 2) }} €
                                                        @elseif($totalSize >= 120 && $totalSize <= 150)
                                                            {{ number_format($det_total + $iva + 10.9, 2) }} €
                                                        @endif
                                                    @elseif($totalWeight > 1 && $totalWeight <= 5)
                                                        @if ($totalSize >= 0 && $totalSize <= 50)
                                                            {{ number_format($det_total + $iva + 4.9, 2) }} €
                                                        @elseif($totalSize >= 50 && $totalSize <= 95)
                                                            {{ number_format($det_total + $iva + 4.9, 2) }} €
                                                        @elseif($totalSize >= 95 && $totalSize <= 120)
                                                            {{ number_format($det_total + $iva + 6.9, 2) }} €
                                                        @elseif($totalSize >= 120 && $totalSize <= 150)
                                                            {{ number_format($det_total + $iva + 10.9, 2) }} €
                                                        @endif
                                                    @elseif($totalWeight > 5 && $totalWeight <= 10)
                                                        @if ($totalSize >= 0 && $totalSize <= 50)
                                                            {{ number_format($det_total + $iva + 6.9, 2) }} €
                                                        @elseif($totalSize >= 50 && $totalSize <= 95)
                                                            {{ number_format($det_total + $iva + 6.9, 2) }} €
                                                        @elseif($totalSize >= 95 && $totalSize <= 120)
                                                            {{ number_format($det_total + $iva + 6.9, 2) }} €
                                                        @elseif($totalSize >= 120 && $totalSize <= 150)
                                                            {{ number_format($det_total + $iva + 10.9, 2) }} €
                                                        @endif
                                                    @elseif($totalWeight > 10 && $totalWeight <= 20)
                                                        @if ($totalSize >= 0 && $totalSize <= 50)
                                                            {{ number_format($det_total + $iva + 10.9, 2) }} €
                                                        @elseif($totalSize >= 50 && $totalSize <= 95)
                                                            {{ number_format($det_total + $iva + 10.9, 2) }} €
                                                        @elseif($totalSize >= 95 && $totalSize <= 120)
                                                            {{ number_format($det_total + $iva + 10.9, 2) }} €
                                                        @elseif($totalSize >= 120 && $totalSize <= 150)
                                                            {{ number_format($det_total + $iva + 10.9, 2) }} €
                                                        @endif
                                                    @else
                                                        {{ number_format($det_total + $iva + 2.9, 2) }} €
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="item" bis_skin_checked="1">
                                            <div class="in">
                                                <div>
                                                    <img src="https://files2.soniccdn.com/files/2021/03/16/beseif-logo.png"
                                                        style="display: inline;">
                                                </div>
                                                <span class="text-muted">
                                                    Envío asegurado <br>
                                                    Compra asegurada
                                                </span>
                                            </div>
                                        </div>
                                    </li>

                                    @endif
                                </ul>
                                @if ($showShippingSelect)
                                    <div class="listview-title mt-2">@lang('messages.send_method')</div>
                                    <div class="ibox-content">


                                        <select class="form-control form-select" data-id="{{ $key->user_name }}">
                                            @foreach ($viewData['shipping'] as $key_ship)
                                                @if ($key_ship->id == $shipping_user)
                                                    <option value="{{ $key_ship->id }}" selected>
                                                        {{ $key_ship->name }} Envío certificado
                                                        {{ number_format($key_ship->price, 2) }} € 
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                        

                                        @php($selectedShipping = session('selected_shipping'))


                                    </div>
                                @endif

                                @php($con = 0)
                                @php($game = 0)

                                @php($contador = 0)
                                @foreach ($viewData['products_in_cart'] as $cartItem)
                                    @if ($cartItem->user->user_name == $key->user_name)
                                        @php($contador += 1)
                                    @endif
                                @endforeach


                                <div class="col-lg-12" style="display: {{ $contador > 0 ? 'block' : 'none' }}; ">



                                    <div class="rating-review-content">
                                        <div class="container">
                                            <div class="shop-tab-menu">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="table article-table table-striped">
                                                            <div class="table-header d-none d-lg-flex">
                                                                <div class="row no-gutters flex-nowrap">
                                                                    <div class="d-none col">#</div>
                                                                    <div class="col-sellerProductInfo col">
                                                                        <div class="row no-gutters h-100">
                                                                            <div class="col-seller col-12 col-lg-auto">
                                                                                Vendedor
                                                                            </div>
                                                                            <div class="col-product col-12 col-lg">
                                                                                Información
                                                                                de producto + Oferta</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="table-body">
                                                                @foreach ($viewData['products_in_cart'] as $cartItem)
                                                                    @if ($cartItem->user->user_name == $key->user_name)
                                                                        <div class="article-row">
                                                                            <div class="d-none col"></div>
                                                                            <div class="col-sellerProductInfo col">
                                                                                <div class="row no-gutters">

                                                                                    <table
                                                                                        class="w-full table-auto min-w-max">
                                                                                        <tbody
                                                                                            class="text-sm font-light text-gray-600">
                                                                                            <tr class="border-b">
                                                                                                <td>
                                                                                                    <div
                                                                                                        class="row no-gutters">
                                                                                                        <div
                                                                                                            class="col-seller col-12 col-lg-auto">
                                                                                                            <span
                                                                                                                class="seller-cart text-left">
                                                                                                                <span
                                                                                                                    class="seller-name">
                                                                                                                    <span
                                                                                                                        class="has-content-centered font-extrabold"><a
                                                                                                                            href="#">
                                                                                                                            @if ($cartItem->title !== null)
                                                                                                                                {{ $cartItem->title }}
                                                                                                                            @else
                                                                                                                                {{ $cartItem->product_info->name }}
                                                                                                                            @endif
                                                                                                                        </a>

                                                                                                                    </span>

                                                                                                                    <br>
                                                                                                                    @if ($cartItem->title !== null)
                                                                                                                    @else
                                                                                                                        <span
                                                                                                                            class="h7   text-secondary">

                                                                                                                            {{ $cartItem->product_info->platform }}
                                                                                                                            -
                                                                                                                            {{ $cartItem->product_info->region }}

                                                                                                                        </span>
                                                                                                                    @endif






                                                                                                                    <span
                                                                                                                        class="d-flex text-nowrap ml-lg-auto"></span>

                                                                                                                </span>
                                                                                                            </span>

                                                                                                            &nbsp;&nbsp;



                                                                                                            <select
                                                                                                                name="cart-quantity"
                                                                                                                class="cart-quantity"
                                                                                                                data-user-id="{{ $cartItem->user->id }}"
                                                                                                                data-value="{{ $cartItem->qty }}"
                                                                                                                data-inventory-id="{{ $cartItem->id + 1000 }}">
                                                                                                                @for ($i = 1; $i <= $cartItem->qty; $i++)
                                                                                                                    <option
                                                                                                                        value="{{ $i }}"
                                                                                                                        {{ $i == $cartItem->qty ? 'selected="selected"' : '' }}>
                                                                                                                        {{ $i }}
                                                                                                                    </option>
                                                                                                                @endfor
                                                                                                            </select>
                                                                                                            <button
                                                                                                                class="btn btn-default btn-xs cart-update-price"
                                                                                                                style="display: none;"
                                                                                                                type="button">{{ __('Actualizar') }}</button>

                                                                                                            &nbsp;&nbsp;




                                                                                                            <a data-user-id="{{ $cartItem->user->id }}"
                                                                                                                data-delete-id="0"
                                                                                                                data-inventory-id="{{ $cartItem->id + 1000 }}"
                                                                                                                class="remove-product cart-delete-item-id btn-danger btn-sm"
                                                                                                                href="#">
                                                                                                                <i
                                                                                                                    class="mr-1 ml-1 lni lni-trash-can"></i></a>


                                                                                                        </div>
                                                                                                        <div
                                                                                                            class="col-product col-12 col-lg">
                                                                                                            <div
                                                                                                                class="row no-gutters">
                                                                                                                <div
                                                                                                                    class="product-attributes col">

                                                                                                                    @if ($cartItem->title !== null)
                                                                                                                    @else
                                                                                                                        @foreach (App\AppOrgCategory::all() as $CatItem)
                                                                                                                            @if ($cartItem->user->user_name == $key->user_name && $cartItem->product_info->catid == $CatItem->id)
                                                                                                                                @if ($CatItem->id == 1)
                                                                                                                                    <img width="15px"
                                                                                                                                        src="/{{ $cartItem->box }}"
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-content="Caja - {{ App\AppOrgUserInventory::getCondicionName($cartItem->box_condition) }}"
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover">
                                                                                                                                    &nbsp;
                                                                                                                                    <img width="15px"
                                                                                                                                        src="/{{ $cartItem->cover }}"
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-content="Caratula - {{ App\AppOrgUserInventory::getCondicionName($cartItem->cover_condition) }}"
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover">
                                                                                                                                    &nbsp;
                                                                                                                                    <img width="15px"
                                                                                                                                        src="/{{ $cartItem->manual }}"
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-content="Manual - {{ App\AppOrgUserInventory::getCondicionName($cartItem->manual_condition) }}"
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover">
                                                                                                                                    &nbsp;
                                                                                                                                    <img width="15px"
                                                                                                                                        src="/{{ $cartItem->game }}"
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-content="Juego - {{ App\AppOrgUserInventory::getCondicionName($cartItem->game_condition) }}"
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover">
                                                                                                                                    &nbsp;
                                                                                                                                    <img width="15px"
                                                                                                                                        src="/{{ $cartItem->extra }}"
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-content="Extra - {{ App\AppOrgUserInventory::getCondicionName($cartItem->extra_condition) }}"
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover">
                                                                                                                                @elseif($CatItem->id == 2)
                                                                                                                                    <img width="15px"
                                                                                                                                        src="/{{ $cartItem->box }}"
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-content="Caja - {{ App\AppOrgUserInventory::getCondicionName($cartItem->box_condition) }}"
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover">
                                                                                                                                    &nbsp;
                                                                                                                                    <img width="15px"
                                                                                                                                        src="/{{ $cartItem->cover }}"
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-content="Caratula - {{ App\AppOrgUserInventory::getCondicionName($cartItem->cover_condition) }}"
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover">
                                                                                                                                    &nbsp;
                                                                                                                                    <img width="15px"
                                                                                                                                        src="/{{ $cartItem->manual }}"
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-content="Manual - {{ App\AppOrgUserInventory::getCondicionName($cartItem->manual_condition) }}"
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover">
                                                                                                                                    &nbsp;
                                                                                                                                    <img width="15px"
                                                                                                                                        src="/{{ $cartItem->game }}"
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-content="Estado - {{ App\AppOrgUserInventory::getCondicionName($cartItem->game_condition) }}"
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover">
                                                                                                                                    &nbsp;
                                                                                                                                    <img width="15px"
                                                                                                                                        src="/{{ $cartItem->extra }}"
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-content="Extra - {{ App\AppOrgUserInventory::getCondicionName($cartItem->extra_condition) }}"
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover">
                                                                                                                                    &nbsp;
                                                                                                                                    <img width="15px"
                                                                                                                                        src="/{{ $cartItem->inside }}"
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-content="Interior - {{ App\AppOrgUserInventory::getCondicionName($cartItem->inside_condition) }}"
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover">
                                                                                                                                @else
                                                                                                                                    <img width="15px"
                                                                                                                                        src="/{{ $cartItem->box }}"
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-content="Caja - {{ App\AppOrgUserInventory::getCondicionName($cartItem->box_condition) }}"
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover">
                                                                                                                                    &nbsp;
                                                                                                                                    <img width="15px"
                                                                                                                                        src="/{{ $cartItem->game }}"
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-content="Juego - {{ App\AppOrgUserInventory::getCondicionName($cartItem->game_condition) }}"
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover">
                                                                                                                                    &nbsp;
                                                                                                                                    <img width="15px"
                                                                                                                                        src="/{{ $cartItem->extra }}"
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-content="Extra - {{ App\AppOrgUserInventory::getCondicionName($cartItem->extra_condition) }}"
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover">
                                                                                                                                @endif
                                                                                                                            @endif
                                                                                                                        @endforeach
                                                                                                                    @endif

                                                                                                                    &nbsp;
                                                                                                                    @if ($cartItem->comments == '')
                                                                                                                        <span
                                                                                                                            data-toggle="popover"
                                                                                                                            data-html="true"
                                                                                                                            data-content="@lang('messages.not_working_profile')"
                                                                                                                            data-placement="top"
                                                                                                                            data-trigger="hover"
                                                                                                                            class="pl-1 pr-2 badge-inventories badge-info">

                                                                                                                            <i
                                                                                                                                class="fa-solid pl-1 fa-circle-xmark text-base"></i>

                                                                                                                        </span>
                                                                                                                    @else
                                                                                                                        <span
                                                                                                                            data-toggle="popover"
                                                                                                                            data-html="true"
                                                                                                                            data-content="{{ $cartItem->comments }}"
                                                                                                                            data-placement="top"
                                                                                                                            data-trigger="hover"
                                                                                                                            class="pl-1 pr-2 badge-inventories badge-info">

                                                                                                                            <i
                                                                                                                                class="lni pl-1 lni-comments text-base"></i>

                                                                                                                        </span>
                                                                                                                    @endif
                                                                                                                    &nbsp;
                                                                                                                    @if ($cartItem->images->first())
                                                                                                                        @if (str_starts_with($cartItem->images->first()->image_path, 'https'))
                                                                                                                            <a href="{{ url($cartItem->images->first()->image_path) }}"
                                                                                                                                class="fancybox"
                                                                                                                                data-fancybox="{{ $cartItem->id }}">
                                                                                                                                <span
                                                                                                                                    class="pl-1 pr-2 badge-inventories badge-info">
                                                                                                                                    <i
                                                                                                                                        class="lni pl-1 lni-camera text-base"></i>
                                                                                                                                </span>
                                                                                                                            </a>
                                                                                                                        @else
                                                                                                                            <a href="{{ url('/uploads/inventory-images/' . $cartItem->images->first()->image_path) }}"
                                                                                                                                class="fancybox"
                                                                                                                                data-fancybox="{{ $cartItem->id }}">
                                                                                                                                <span
                                                                                                                                    class="pl-1 pr-2 badge-inventories badge-info">
                                                                                                                                    <i
                                                                                                                                        class="lni pl-1 lni-camera text-base"></i>
                                                                                                                                </span>
                                                                                                                            </a>
                                                                                                                        @endif


                                                                                                                        @foreach ($cartItem->images as $p)
                                                                                                                            @if (!$loop->first)
                                                                                                                                @if (str_starts_with($p->image_path, 'https'))
                                                                                                                                    <a href="{{ $p->image_path }}"
                                                                                                                                        data-fancybox="{{ $cartItem->id }}">
                                                                                                                                        <img src="{{ $p->image_path }}"
                                                                                                                                            width="0px"
                                                                                                                                            height="0px"
                                                                                                                                            style="position:absolute;" />
                                                                                                                                    </a>
                                                                                                                                @else
                                                                                                                                    <a href="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                                                                                        data-fancybox="{{ $cartItem->id }}">
                                                                                                                                        <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                                                                                            width="0px"
                                                                                                                                            height="0px"
                                                                                                                                            style="position:absolute;" />
                                                                                                                                    </a>
                                                                                                                                @endif
                                                                                                                            @endif
                                                                                                                        @endforeach
                                                                                                                    @else
                                                                                                                        <span
                                                                                                                            data-toggle="popover"
                                                                                                                            data-html="true"
                                                                                                                            data-content="@lang('messages.not_working_profile')"
                                                                                                                            data-placement="top"
                                                                                                                            data-trigger="hover"
                                                                                                                            class="pl-1 pr-2 badge-inventories badge-info">

                                                                                                                            <i
                                                                                                                                class="fa-solid pl-1 fa-circle-xmark text-base"></i>

                                                                                                                        </span>
                                                                                                                    @endif
                                                                                                                    &nbsp;

                                                                                                                    @if ($cartItem->title !== null)
                                                                                                                    @else
                                                                                                                        @foreach (App\AppOrgCategory::all() as $CatItem)
                                                                                                                            @if ($cartItem->user->user_name == $key->user_name && $cartItem->product_info->catid == $CatItem->id)
                                                                                                                                @if ($CatItem->id == 1)
                                                                                                                                    <span
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-html="true"
                                                                                                                                        data-content="
                                                                                                    <b>Estado :</b> <br />
                                                                                                Caja - {{ __(App\AppOrgUserInventory::getCondicionName($cartItem->box_condition)) }} <br />
                                                                                                Carátula - {{ __(App\AppOrgUserInventory::getCondicionName($cartItem->cover_condition)) }} <br />
                                                                                                Manual - {{ __(App\AppOrgUserInventory::getCondicionName($cartItem->manual_condition)) }} <br />
                                                                                                Juego - {{ __(App\AppOrgUserInventory::getCondicionName($cartItem->game_condition)) }} <br />
                                                                                                Extra - {{ __(App\AppOrgUserInventory::getCondicionName($cartItem->extra_condition)) }} <br />
                                                                                                "
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover"
                                                                                                                                        class="pr-2 pl-2 badge-inventories badge-info">Info</span>
                                                                                                                                @elseif($CatItem->id == 2)
                                                                                                                                    <span
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-html="true"
                                                                                                                                        data-content="
                                                                                                    <b>Estado :</b> <br />
                                                                                                Caja - {{ __(App\AppOrgUserInventory::getCondicionName($cartItem->box_condition)) }} <br />
                                                                                                Carátula - {{ __(App\AppOrgUserInventory::getCondicionName($cartItem->cover_condition)) }} <br />
                                                                                                Manual - {{ __(App\AppOrgUserInventory::getCondicionName($cartItem->manual_condition)) }} <br />
                                                                                                Estado - {{ __(App\AppOrgUserInventory::getCondicionName($cartItem->game_condition)) }} <br />
                                                                                                Interior - {{ __(App\AppOrgUserInventory::getCondicionName($producto->inside_condition)) }} br />
                                                                                                Extra - {{ __(App\AppOrgUserInventory::getCondicionName($cartItem->extra_condition)) }} <br />
                                                                                                "
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover"
                                                                                                                                        class="pr-2 pl-2 badge-inventories badge-info">Info</span>
                                                                                                                                @else
                                                                                                                                    <span
                                                                                                                                        data-toggle="popover"
                                                                                                                                        data-html="true"
                                                                                                                                        data-content="
                                                                                                    <b>Estado :</b> <br />
                                                                                                Caja - {{ __(App\AppOrgUserInventory::getCondicionName($cartItem->box_condition)) }} <br />
                                                                                                Juego - {{ __(App\AppOrgUserInventory::getCondicionName($cartItem->game_condition)) }} <br />
                                                                                                Extra - {{ __(App\AppOrgUserInventory::getCondicionName($cartItem->extra_condition)) }} <br />
                                                                                                "
                                                                                                                                        data-placement="top"
                                                                                                                                        data-trigger="hover"
                                                                                                                                        class="pr-2 pl-2 badge-inventories badge-info">Info</span>
                                                                                                                                @endif
                                                                                                                            @endif
                                                                                                                        @endforeach
                                                                                                                    @endif
                                                                                                                </div>
                                                                                                                <div
                                                                                                                    class="mobile-offer-container d-flex d-md-none justify-content-end col">

                                                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                </div>
                                                                                                                <div
                                                                                                                    class="mobile-offer-container d-flex d-md-none justify-content-end col">
                                                                                                                    <div
                                                                                                                        class="d-flex flex-column">
                                                                                                                        <div
                                                                                                                            class="d-flex align-items-center justify-content-end">
                                                                                                                            <span
                                                                                                                                class="font-weight-bold color-primary small text-right text-nowrap">
                                                                                                                                {{ number_format($cartItem->price * $cartItem->qty, 2) }}
                                                                                                                                €</span>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>

                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>




            @include('brcode.front.public.modals.check_promotion')
        @else
            <div class="page-content-wrapper">
                <div class="container">
                    <div class="nk-gap-3"></div>
                    <div class="row">
                        <div class="col-lg-12">
                            <blockquote class="nk-blockquote">
                                <div class="nk-blockquote-icon"></div>
                                <div class="text-center nk-blockquote-content h3 font-extrabold">
                                    <font color="black">
                                        @lang('messages.my_cart_empty')
                                    </font>
                                </div>
                                <div class="nk-gap"></div>
                                <div class="nk-blockquote-author"></div>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="nk-gap-2"></div>
        </div><!-- /.container -->
    @else
        <div class="container">
            <div class="nk-gap-2"></div>
            <div class="row">
                <div class="col-md-12">
                    <h3 class="nk-decorated-h-3">
                        <div class="section-title2">
                            <h3 class="retro">{{ __('Mi carrito') }}</b></h3>
                        </div>
                    </h3>
                </div>
            </div><!-- /.row -->
            <div class="nk-gap"></div>
            <span class="retro">
                @include('partials.flash')
            </span>
            @if ($viewData['cart_new']->details->count() > 0)
                @php($total1 = 0)
                @php($art = 0)
                @foreach ($viewData['products_in_cart'] as $cartItem)
                    @php($total1 += $cartItem->price * $cartItem->qty)
                    @php($art += $cartItem->qty)
                @endforeach
                <div class="row" id="cart-order-user-detail">
                    <div class="col-md-12">
                        @if ($month == 0)
                            <div
                                class="flex flex-row items-center p-5 bg-yellow-400 border-b-2 border-yellow-500 rounded alert">
                                <div
                                    class="flex items-center justify-center flex-shrink-0 w-10 h-10 bg-yellow-100 border-2 border-yellow-500 rounded-full alert-icon">
                                    <span class="text-yellow-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="w-6 h-6">
                                            <path fill-rule="evenodd"
                                                d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                                                clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="ml-4 alert-content">
                                    <div class="text-xl font-semibold text-center text-yellow-800 retro alert-title">
                                        @lang('messages.warnings')
                                    </div>
                                    <div class="text-lg text-gray-700 alert-description">
                                        Esta es tu primera compra del mes , ten en cuenta que primero se tendra que
                                        confirmar la
                                        entrega para recibir tu 20% de tu saldo especial
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <!-- START: Cart Totals -->
                        <div class="ibox-title">
                            <h5 class="text-xs text-gray-900 retro">@lang('messages.cart_general')</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('Pedidos') }}</span>
                                <span class="text-sm font-semibold ">
                                    <div id="orderID">{{ count($us) }} </div>
                                </span>
                            </div>
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('Artículos') }}</span>
                                <span class="text-sm font-semibold ">
                                    <div id="articlesID">{{ $art }}</div>
                                </span>
                            </div>
                            @if ($historial_promotion_count > 0)
                                @foreach ($historial_promotion as $h)
                                    <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                        <span
                                            class="text-sm font-semibold uppercase"><b>{{ __('Precio total ítems') }}</b></span>
                                        @php($special_promotion = (Auth::user()->cart->totalCart * $h->promotion->special) / 100)
                                        @php($final_special = Auth::user()->cart->totalCart - $special_promotion)
                                        <span class="text-xs font-bold text-gray-900 retro">
                                            <div id="totalID"> {{ number_format($final_special, 2) }} €
                                            </div>
                                        </span>
                                    </div>
                                @endforeach
                            @else
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Precio total ítems') }}</span>
                                    <span class="text-sm font-semibold ">
                                        <div id="subTotalID">{{ number_format(Auth::user()->cart->totalCart, 2) }} €
                                        </div>
                                    </span>
                                </div>
                            @endif
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('Envío') }}</span>
                                <span class="text-sm font-semibold ">
                                    <div id="shippingID">{{ number_format(Auth::user()->cart->totalShipping, 2) }} €
                                    </div>
                                </span>
                            </div>
                            @if (Auth::user()->special_cash > 0)
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Saldo especial') }}</span>
                                    <span class="text-sm font-semibold ">
                                        <div id="shippingID"> - {{ number_format(Auth::user()->special_cash, 2) }} €
                                        </div>
                                    </span>
                                </div>
                            @endif
                            <form class="flex justify-between pb-8 mt-10 mb-5 border-b"
                                action="{{ route('check_promotional_code') }}" method="POST"
                                enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <span class="text-sm font-semibold uppercase">{{ __('Codigo Promocional') }}</span>
                                @if ($historial_promotion_count > 0)
                                    @foreach ($historial_promotion as $h)
                                        <span class="text-sm font-semibold promotion" data-toggle="popover"
                                            data-content="{{ $h->promotion->description }}" data-placement="top"
                                            data-trigger="hover">
                                            <div id="shippingID"> {{ $h->promotion->code }}
                                            </div>
                                        </span>
                                    @endforeach
                                @else
                                    <span class="text-sm font-semibold ">
                                        <input type="text" name="code" id="name_field"
                                            placeholder="Recuerda presionar enter" class="nes-input">
                                        <button style="display:none" type="submit"
                                            class="border-b retro nk-btn-xs nk-btn-rounded nes-btn is-warning btn-block">
                                            <span class="text-sm font-semibold retro">
                                                {{ __('Comprometerse a comprar') }}
                                            </span>
                                        </button>
                                    </span>
                                @endif
                            </form>
                            @if ($historial_promotion_count > 0)
                                @foreach ($historial_promotion as $h)
                                    <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                        <span
                                            class="text-sm font-semibold uppercase retro"><b>{{ __('Total') }}</b></span>
                                        @php($total = $final_special + Auth::user()->cart->totalShipping - (Auth::user()->special_cash > 0 ? Auth::user()->special_cash : 0))
                                        <span class="text-xs font-bold text-gray-900 retro">
                                            <div id="totalID">{{ number_format($total, 2) }} €
                                            </div>
                                        </span>
                                    </div>
                                @endforeach
                            @else
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase retro"><b>{{ __('Total') }}</b></span>
                                    @php($total = Auth::user()->cart->totalCart + Auth::user()->cart->totalShipping - (Auth::user()->special_cash > 0 ? Auth::user()->special_cash : 0))
                                    <span class="text-xs font-bold text-gray-900 retro">
                                        <div id="totalID">{{ number_format($total, 2) }} €</div>
                                    </span>
                                </div>
                            @endif

                            <span class="text-muted small">

                            </span>
                            <div class="m-t-sm">
                                <div class="btn-group">
                                </div>
                            </div>
                        </div>
                        <!-- END: Cart Totals -->
                    </div>
                    @if (Auth::id() > 0)
                        <div class="col-md-6">
                            <!-- START: Cart Totals -->
                            <div class="ibox-title">
                                <h5 class="text-xs text-gray-900 retro">@lang('messages.cart_direction')</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Para') }}</span>
                                    <span class="text-sm font-semibold">{{ Auth::user()->first_name }}
                                        {{ Auth::user()->last_name }}</span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Dirección') }}</span>
                                    <span class="text-sm font-semibold">
                                        @if (!empty(Auth::user()->address))
                                            {{ Auth::user()->address }}
                                        @else
                                            @lang('messages.emptys')
                                        @endif
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Código postal') }}</span>
                                    <span class="text-sm font-semibold">
                                        @if (!empty(Auth::user()->zipcode))
                                            {{ Auth::user()->zipcode }}
                                        @else
                                            @lang('messages.emptys')
                                        @endif
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('País') }}</span>
                                    <span class="text-sm font-semibold ">{{ Auth::user()->country->name }}</span>
                                </div>

                                <a class="border-b retro nk-btn-xs nk-btn-rounded nes-btn is-error btn-block "
                                    href="/account/profile#available"><span class="text-sm font-semibold retro">
                                        @lang('messages.change_direction')</span></a>
                            </div>


                            <!-- END: Cart Totals -->
                        </div>
                    @endif
                </div>
                <div class="nk-gap-2"></div>
                <div class="row">

                    <div class="col-lg-12 col-md-12">
                        <div class="bg-white nk-box-2">
                            <h1 class="text-2xl font-semibold text-gray-900 retro">@lang('messages.verified')</h1>
                            @if (App\SysSettings::where('type', 'Compras')->first())
                                @php($c = App\SysSettings::where('type', 'Compras')->first())
                                @if ($c->value == 0)
                                    <h5 class="text-center">@lang('messages.purchase_deshabilited')</h5>
                                @else
                                    <form action="{{ url('/cartComplete') }}" method="post">
                                        @if (Auth::id() > 0)
                                            <div class="col-md-12">
                                                @if (!empty($mes))
                                                    <div class="alert alert-danger alert-dismissible">
                                                        <a href="#" class="close" data-dismiss="alert"
                                                            aria-label="close">&times;</a>
                                                        <strong>Error!</strong> {{ $mes }}
                                                    </div>
                                                @endif
                                                <p class="text-lg text-gray-700">
                                                    @lang('messages.please_read_cart')
                                                    <b>@lang('messages.cart_comprometer').</b>
                                                    <br>
                                                    <b>
                                                        <font color="red">@lang('messages.cart_warning')</font>
                                                    </b> / @lang('messages.cart_duda') <a href="/site/faqs" class="profesional"
                                                        target="_blank">FAQ</a>
                                                </p>

                                            </div>
                                        @endif

                                        {{ csrf_field() }}
                                        <div class="nk-gap"></div>
                                        @if ($historial_promotion)
                                            @foreach ($historial_promotion as $h)
                                                <input name="promotion_code" type="hidden"
                                                    value="{{ $h->promotion->code }}">
                                            @endforeach
                                        @endif
                                        <div class="nk-gap">
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 nk-mchimp nk-form nk-form-style-1 validate">
                                                <label
                                                    class="text-lg text-gray-700">{{ __('Por favor ingrese su contraseña para confirmar legalmente su pedido.') }}</label>
                                                <input type="password" name="pass" class="nes-input"
                                                    placeholder="@lang('messages.yours_password')" required><br>

                                            </div>

                                        </div>
                                        <br>
                                        <div class="col-md-12"> <button type="submit"
                                                class="border-b retro nk-btn-xs nk-btn-rounded nes-btn is-warning btn-block">
                                                <span class="text-sm font-semibold retro">
                                                    {{ __('Comprometerse a comprar') }}
                                                </span>
                                            </button>
                                        </div>
                                    </form>
                                @endif
                            @else
                                <form action="{{ url('/cartComplete') }}" method="post">
                                    @if (Auth::id() > 0)
                                        <div class="col-md-12">
                                            @if (!empty($mes))
                                                <div class="alert alert-danger alert-dismissible">
                                                    <a href="#" class="close" data-dismiss="alert"
                                                        aria-label="close">&times;</a>
                                                    <strong>Error!</strong> {{ $mes }}
                                                </div>
                                            @endif
                                            {!! __(
                                                'Por favor revise el contenido de su carrito de compras. Luego ingrese su contraseña en la casilla de verificación y haga clic en <b> Comprometer para comprar </b>. <br> Entonces podrás elegir tu método de pago.',
                                            ) !!}


                                        </div>
                                    @endif

                                    {{ csrf_field() }}
                                    <div class="nk-gap"></div>
                                    <div class="col-lg-6 nk-mchimp nk-form nk-form-style-1 validate">
                                        <label
                                            for="">{{ __('Por favor ingrese su contraseña para confirmar legalmente su pedido.') }}</label>
                                        <input type="password" name="pass" class="form-control"
                                            placeholder="Password" required><br>
                                    </div>
                                    <div class="col-md-6"><button type="submit"
                                            class="nk-btn nk-btn-xs nk-btn-rounded nk-btn-color-white btn-block">{{ __('Comprometerse a comprar') }}</button>
                                    </div>
                                </form>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="nk-gap-2"></div>

                <div class="col-md-12">
                    <h3 class="nk-decorated-h-3">
                        <div class="section-title2">
                            <h3 class="retro">@lang('messages.cart_details')</b></h3>
                        </div>
                    </h3>
                </div>
                @if (count($viewData['products_in_cart']) > 1)
                    <div class="bg-white row scroller">
                    @else
                        <div class="bg-white row">
                @endif
                @php($order_num = 0)
                @if (count($viewData['products_in_cart']) > 0)
                    @foreach ($viewData['user_order'] as $key)
                        @php($qty_det = 0)
                        @php($det_total = 0)
                        @php($det_vol = 0)
                        @php($det_wei = 0)

                        @foreach ($viewData['products_in_cart'] as $cartItem)
                            @if ($cartItem->user->user_name == $key->user_name)
                                @php($qty_det += $cartItem->qty)
                                @php($det_total += $cartItem->qty * $cartItem->price)
                                @php($det_vol += $cartItem->qty * $cartItem->product->volume)
                                @php($det_wei += $cartItem->qty * $cartItem->product->weight)
                            @endif
                        @endforeach
                        @php($order_num += 1)
                        @foreach ($viewData['shipping_values'] as $val)
                            @if ($val->user_id == $key->id)
                                @php($shipping_user = $val->shipping_id)
                            @endif
                        @endforeach

                        <div class="col-lg-6">
                            <div class="ibox-title">
                                <h5 class="text-xs text-gray-900 retro"> {{ __('Detalle de pedido') }} <span
                                        class="text-main-1">
                                        #0000{{ $order_num }}</span></h5>
                            </div>
                            <div class="ibox-content">
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Comprador') }}</span>
                                    <span class="text-sm font-semibold ">
                                        @if (Auth::user())
                                            {{ Auth::user()->user_name }}
                                        @endif
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Vendedor') }}</span>
                                    <span class="text-sm font-semibold">
                                        {{ $key->user_name }}
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span
                                        class="text-sm font-semibold uppercase">{{ __('Cantidad de artículos') }}</span>
                                    <span class="text-sm font-semibold ">
                                        {{ $qty_det }}
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Peso') }}</span>
                                    <span class="text-sm font-semibold ">
                                        {{ $det_wei }} g
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Peso') }}</span>
                                    <span class="text-sm font-semibold ">
                                        {{ $det_wei }} g
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Volumen') }}</span>
                                    <span class="text-sm font-semibold ">
                                        {{ $det_vol }}
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase retro"><b>{{ __('Total') }}</b></span>
                                    <span class="text-xs font-bold text-gray-900 retro">
                                        {{ number_format($det_total, 2) }} €
                                    </span>
                                </div>
                                <div class="m-t-sm">
                                    <div class="btn-group">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-6 nk-form nk-form-style-1">
                            <div class="ibox-title">
                                <h5 class="text-xs text-gray-900 retro">@lang('messages.send_method')</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="nes-select">
                                    <select data-id="{{ $key->user_name }}" onchange="updateShipping(this)">
                                        @foreach ($viewData['shipping'] as $key_ship)
                                            @if ($key_ship->from_id == $key->country_id && $key_ship->to_id == Auth::user()->country_id)
                                                @if ($det_total <= $key_ship->max_value && $det_wei <= $key_ship->max_weight && $det_vol <= $key_ship->volume)
                                                    <option value="{{ $key_ship->id }}"
                                                        {{ $key_ship->id == $shipping_user ? 'selected' : '' }}>
                                                        {{ $key_ship->name }} @if ($key_ship->certified == 'Yes')
                                                            Envio certificado
                                                        @else
                                                            Envio
                                                            no certificado
                                                        @endif
                                                        {{ number_format($key_ship->price, 2) }} € -
                                                        {{ number_format($key_ship->max_value, 2) }} € max value
                                                    </option>
                                                @endif
                                            @endif
                                            @if ($key_ship->from_id == 0 && $key_ship->to_id == 0)
                                                <option value="{{ $key_ship->id }}"
                                                    {{ $key_ship->id == $shipping_user ? 'selected' : '' }}>
                                                    {{ $key_ship->name }} @if ($key_ship->certified == 'Yes')
                                                        Envio certificado
                                                    @else
                                                        Envio no
                                                        certificado
                                                    @endif
                                                    {{ number_format($key_ship->price, 2) }} € -
                                                    {{ number_format($key_ship->max_value, 2) }} € max value</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        @php($con = 0)
                        @php($game = 0)
                        @foreach (App\AppOrgCategory::all() as $CatItem)
                            @php($contador = 0)
                            @foreach ($viewData['products_in_cart'] as $cartItem)
                                @if ($cartItem->user->user_name == $key->user_name && $cartItem->product_info->catid == $CatItem->id)
                                    @php($contador += 1)
                                @endif
                            @endforeach


                            <div class="col-lg-12" style="display: {{ $contador > 0 ? 'block' : 'none' }}; ">
                                <h5 class="text-gray-900 mt-15 retro">
                                    {{ __($CatItem->name) }}
                                </h5>
                                <div class="wishlist-table table-responsive">
                                    <table>
                                        <thead>
                                            @if ($CatItem->id == 1)
                                                <tr class="bg-red-700" style="font-size:10px;">
                                                    <th class="product-cart-img">
                                                        <span class="nobr"><i class="fa fa-image"></i></span>
                                                    </th>
                                                    <th class="product-name">
                                                        <span class="nobr">@lang('messages.products')</span>
                                                    </th>
                                                    <th class="product-name">
                                                        <span class="nobr">@lang('messages.box')</span>
                                                    </th>
                                                    <th class="product-price">
                                                        <span class="nobr">@lang('messages.cover')</span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Manual </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr">@lang('messages.game_product_inventory')</span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Extra </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> <i class="fa fa-image"></i>
                                                        </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> <i class="fa fa-comment"></i>
                                                        </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> @lang('messages.cants') </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> @lang('messages.price') </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Total </span>
                                                    </th>
                                                    <th class="product-add-to-cart">@lang('messages.actions')</th>
                                                </tr>
                                            @elseif($CatItem->id == 2)
                                                <tr class="bg-red-700" style="font-size:10px;">
                                                    <th class="product-cart-img">
                                                        <span class="nobr"><i class="fa fa-image"></i></span>
                                                    </th>
                                                    <th class="product-name">
                                                        <span class="nobr">@lang('messages.products')</span>
                                                    </th>
                                                    <th class="product-name">
                                                        <span class="nobr">@lang('messages.box')</span>
                                                    </th>
                                                    <th class="product-price">
                                                        <span class="nobr">@lang('messages.inside')</span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Manual </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr">@lang('messages.console')</span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Extra </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Cables </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> <i class="fa fa-image"></i>
                                                        </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> <i class="fa fa-comment"></i>
                                                        </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr">@lang('messages.cants') </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr">@lang('messages.price') </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Total </span>
                                                    </th>
                                                    <th class="product-add-to-cart">@lang('messages.actions')</th>
                                                </tr>
                                            @else
                                                <tr class="bg-red-700" style="font-size:10px;">
                                                    <th class="product-cart-img">
                                                        <span class="nobr"><i class="fa fa-image"></i></span>
                                                    </th>
                                                    <th class="product-name">
                                                        <span class="nobr">@lang('messages.products')</span>
                                                    </th>
                                                    <th class="product-name">
                                                        <span class="nobr">@lang('messages.box')</span>
                                                    </th>
                                                    <th class="product-price">
                                                        <span class="nobr">@lang('messages.state_inventory')</span>
                                                    </th>

                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Extra </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> <i class="fa fa-image"></i>
                                                        </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> <i class="fa fa-comment"></i>
                                                        </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> @lang('messages.cants') </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> @lang('messages.price') </span>
                                                    </th>
                                                    <th class="product-stock-stauts">
                                                        <span class="nobr"> Total </span>
                                                    </th>
                                                    <th class="product-add-to-cart">@lang('messages.actions')</th>
                                                </tr>
                                            @endif
                                        </thead>

                                        <tbody>
                                            @foreach ($viewData['products_in_cart'] as $cartItem)
                                                @if ($cartItem->user->user_name == $key->user_name && $cartItem->product_info->catid == $CatItem->id)
                                                    @if ($CatItem->id == 1)
                                                        <tr>
                                                            <td class="text-center">
                                                                @if (count($cartItem->product_info->product_images) > 0)
                                                                    <a href="/product/{{ $cartItem->product_info->id }}">
                                                                        <img width="70px" height="70px"
                                                                            src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                                    </a>
                                                                @else
                                                                    <img width="70px" height="70px"
                                                                        src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                                @endif
                                                            </td>
                                                            <td class="text-left">
                                                                <a href="/product/{{ $cartItem->product_info->id }}"
                                                                    target="_blank">
                                                                    <span
                                                                        class="text-xs retro font-weight-bold color-primary small">
                                                                        <font color="black">
                                                                            <b>{{ $cartItem->product_info->name }}
                                                                                @if ($cartItem->product_info->name_en)
                                                                                    <br><small>{{ $cartItem->product_info->name_en }}</small>
                                                                                @endif
                                                                            </b>
                                                                        </font>
                                                                    </span>
                                                                </a>



                                                                <br><span
                                                                    class="h7 text-secondary">{{ $cartItem->product_info->platform }}
                                                                    -
                                                                    {{ $cartItem->product_info->region }}</span>

                                                            </td>

                                                            <td class="product-name">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->box }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->box_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>
                                                            <td class="product-stock-status">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->cover }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->cover_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>
                                                            <td class="product-stock-status">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->manual }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->manual_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>
                                                            <td class="product-price">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->game }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->game_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>
                                                            </td>
                                                            <td class="product-price">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->extra }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->extra_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>

                                                            @if ($cartItem->images->first())
                                                                <td class="text-center product-cart-img">

                                                                    <a href="{{ url('/uploads/inventory-images/' . $cartItem->images->first()->image_path) }}"
                                                                        class="fancybox"
                                                                        data-fancybox="{{ $cartItem->id }}">

                                                                        <img width="50px" height="50px"
                                                                            class="transform motion-safe:hover:scale-110"
                                                                            src="{{ url('/uploads/inventory-images/' . $cartItem->images->first()->image_path) }}"
                                                                            alt="">
                                                                    </a>

                                                                    @foreach ($cartItem->images as $p)
                                                                        @if (!$loop->first)
                                                                            <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                                data-fancybox="{{ $cartItem->id }}">

                                                                                <img width="0px" height="0px"
                                                                                    style="position:absolute;"
                                                                                    src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                                    alt="">
                                                                            </a>
                                                                        @endif
                                                                    @endforeach


                                                                </td>
                                                            @else
                                                                <td class="product-stock-status">
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-times" data-toggle="popover"
                                                                                data-content="@lang('messages.not_working_profile')"
                                                                                data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>
                                                                </td>
                                                            @endif

                                                            <td class="product-stock-status">
                                                                @if ($cartItem->comments == '')
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-times" data-toggle="popover"
                                                                                data-content="@lang('messages.not_working_profile')"
                                                                                data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>
                                                                @else
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-comment" data-toggle="popover"
                                                                                data-content="{{ $cartItem->comments }}"
                                                                                data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>
                                                                @endif
                                                            </td>


                                                            <td class="product-price">
                                                                <font color="black">
                                                                    <div class="relative">
                                                                        <span
                                                                            class="text-right cart-unit-price retro font-weight-bold color-primary small text-nowrap"
                                                                            data-value="{{ $cartItem->qty }}">
                                                                            {{ $cartItem->qty }}</span>

                                                                    </div>
                                                                </font>
                                                            </td>
                                                            <td>
                                                                <font color="green">
                                                                    <span
                                                                        class="text-right cart-unit-price retro font-weight-bold color-primary small text-nowrap"
                                                                        data-value="{{ $cartItem->price }}">
                                                                        {{ number_format($cartItem->price, 2) }}
                                                                        €
                                                                    </span>
                                                                </font>
                                                            </td>
                                                            <td>
                                                                <font color="green"> <span
                                                                        class="text-right retro font-weight-bold color-primary small text-nowrap">
                                                                        {{ number_format($cartItem->price * $cartItem->qty, 2) }}
                                                                        € </span> </font>
                                                            </td>
                                                            <td class="text-center">
                                                                <button data-inventory-id="{{ $cartItem->id + 1000 }}"
                                                                    type="button"
                                                                    class="nk-btn nk-btn-xs nes-btn is-error nk-btn-rounded nk-btn-color-white cart-delete-item-id">
                                                                    <i class="fa fa-trash"></i> </button>
                                                            </td>

                                                        </tr>
                                                    @elseif($CatItem->id == 2)
                                                        <tr>
                                                            <td class="text-center">
                                                                @if (count($cartItem->product_info->product_images) > 0)
                                                                    <a href="/product/{{ $cartItem->product_info->id }}"
                                                                        class="image_appear">
                                                                        <img width="70px" height="70px"
                                                                            src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                                    </a>
                                                                @else
                                                                    <img width="70px" height="70px"
                                                                        src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                                @endif
                                                            </td>
                                                            <td class="text-left">
                                                                <a href="/product/{{ $cartItem->product_info->id }}"
                                                                    target="_blank">
                                                                    <span
                                                                        class="text-xs retro font-weight-bold color-primary small">
                                                                        <font color="black">
                                                                            <b>{{ $cartItem->product_info->name }}
                                                                                @if ($cartItem->product_info->name_en)
                                                                                    <br><small>{{ $cartItem->product_info->name_en }}</small>
                                                                                @endif
                                                                            </b>
                                                                        </font>
                                                                    </span>
                                                                </a>



                                                                <br><span
                                                                    class="h7 text-secondary">{{ $cartItem->product_info->platform }}
                                                                    -
                                                                    {{ $cartItem->product_info->region }}</span>

                                                            </td>
                                                            <td class="product-name">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->box }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->box_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>
                                                            <td class="product-stock-status">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->cover }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->cover_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>
                                                            <td class="product-stock-status">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->manual }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->manual_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>

                                                            <td class="product-price">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->game }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->game_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>
                                                            </td>


                                                            <td class="product-price">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->extra }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->extra_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>

                                                            <td class="product-price">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->inside }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->inside_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>
                                                            @if ($cartItem->images->first())
                                                                <td class="text-center product-cart-img">

                                                                    <a href="{{ url('/uploads/inventory-images/' . $cartItem->images->first()->image_path) }}"
                                                                        class="fancybox"
                                                                        data-fancybox="{{ $cartItem->id }}">

                                                                        <img width="50px" height="50px"
                                                                            class="transform motion-safe:hover:scale-110"
                                                                            src="{{ url('/uploads/inventory-images/' . $cartItem->images->first()->image_path) }}"
                                                                            alt="">
                                                                    </a>

                                                                    @foreach ($cartItem->images as $p)
                                                                        @if (!$loop->first)
                                                                            <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                                data-fancybox="{{ $cartItem->id }}">

                                                                                <img width="0px" height="0px"
                                                                                    style="position:absolute;"
                                                                                    src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                                    alt="">
                                                                            </a>
                                                                        @endif
                                                                    @endforeach


                                                                </td>
                                                            @else
                                                                <td class="product-stock-status">
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-times" data-toggle="popover"
                                                                                data-content="No Posee"
                                                                                data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                            <td class="product-stock-status">
                                                                @if ($cartItem->comments == '')
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-times" data-toggle="popover"
                                                                                data-content="No Posee"
                                                                                data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>
                                                                @else
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-comment" data-toggle="popover"
                                                                                data-content="{{ $cartItem->comments }}"
                                                                                data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>
                                                                @endif
                                                            </td>
                                                            <td class="product-price">
                                                                <div class="relative">
                                                                    <span
                                                                        class="text-right text-gray-900 cart-unit-price retro font-weight-bold color-primary small text-nowrap"
                                                                        data-value="{{ $cartItem->qty }}">
                                                                        {{ $cartItem->qty }}</span>

                                                                </div>
                                                            </td>
                                                            <td>
                                                                <font color="green"> <span
                                                                        class="text-right cart-unit-price retro font-weight-bold color-primary small text-nowrap"
                                                                        data-value="{{ $cartItem->price }}">
                                                                        {{ number_format($cartItem->price, 2) }}
                                                                        € </span> </font>


                                                            </td>
                                                            <td>
                                                                <font color="green"> <span
                                                                        class="text-right retro font-weight-bold color-primary small text-nowrap">
                                                                        {{ number_format($cartItem->price * $cartItem->qty, 2) }}
                                                                        € </span> </font>
                                                            </td>
                                                            <td class="text-center">
                                                                <button data-inventory-id="{{ $cartItem->id + 1000 }}"
                                                                    type="button"
                                                                    class="nk-btn nk-btn-xs nes-btn is-error nk-btn-rounded nk-btn-color-white cart-delete-item-id">
                                                                    <i class="fa fa-trash"></i> </button>
                                                            </td>

                                                        </tr>
                                                    @else
                                                        <tr>
                                                            <td class="text-center">
                                                                @if (count($cartItem->product_info->product_images) > 0)
                                                                    <a href="/product/{{ $cartItem->product_info->id }}"
                                                                        class="image_appear">
                                                                        <img width="70px" height="70px"
                                                                            src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                                    </a>
                                                                @else
                                                                    <img width="70px" height="70px"
                                                                        src="{{ count($cartItem->product_info->product_images) > 0 ? url('/images/' . $cartItem->product_info->product_images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                                @endif
                                                            </td>
                                                            <td class="text-left">
                                                                <a href="/product/{{ $cartItem->product_info->id }}"
                                                                    target="_blank">
                                                                    <span
                                                                        class="text-xs retro font-weight-bold color-primary small">
                                                                        <font color="black">
                                                                            <b>{{ $cartItem->product_info->name }}
                                                                                @if ($cartItem->product_info->name_en)
                                                                                    <br><small>{{ $cartItem->product_info->name_en }}</small>
                                                                                @endif
                                                                            </b>
                                                                        </font>
                                                                    </span>
                                                                </a>



                                                                <br><span
                                                                    class="h7 text-secondary">{{ $cartItem->product_info->platform }}
                                                                    -
                                                                    {{ $cartItem->product_info->region }}</span>

                                                            </td>
                                                            <td class="product-name">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->box }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->box_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>

                                                            <td class="product-price">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->game }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->game_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>
                                                            </td>

                                                            <td class="product-price">
                                                                <center>
                                                                    <img width="15px" src="/{{ $cartItem->extra }}"
                                                                        data-toggle="popover"
                                                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($cartItem->extra_condition) }}"
                                                                        data-placement="top" data-trigger="hover">
                                                                </center>

                                                            </td>


                                                            @if ($cartItem->images->first())
                                                                <td class="text-center product-cart-img">

                                                                    <a href="{{ url('/uploads/inventory-images/' . $cartItem->images->first()->image_path) }}"
                                                                        class="fancybox"
                                                                        data-fancybox="{{ $cartItem->id }}">

                                                                        <img width="50px" height="50px"
                                                                            class="transform motion-safe:hover:scale-110"
                                                                            src="{{ url('/uploads/inventory-images/' . $cartItem->images->first()->image_path) }}"
                                                                            alt="">
                                                                    </a>

                                                                    @foreach ($cartItem->images as $p)
                                                                        @if (!$loop->first)
                                                                            <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                                data-fancybox="{{ $cartItem->id }}">

                                                                                <img width="0px" height="0px"
                                                                                    style="position:absolute;"
                                                                                    src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                                    alt="">
                                                                            </a>
                                                                        @endif
                                                                    @endforeach


                                                                </td>
                                                            @else
                                                                <td class="product-stock-status">
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-times" data-toggle="popover"
                                                                                data-content="No Posee"
                                                                                data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                            <td class="product-stock-status">
                                                                @if ($cartItem->comments == '')
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-times" data-toggle="popover"
                                                                                data-content="No Posee"
                                                                                data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>
                                                                @else
                                                                    <center>
                                                                        <font color="black">
                                                                            <i class="fa fa-comment" data-toggle="popover"
                                                                                data-content="{{ $cartItem->comments }}"
                                                                                data-placement="top"
                                                                                data-trigger="hover"></i>
                                                                        </font>
                                                                    </center>
                                                                @endif
                                                            </td>


                                                            <td class="product-price">
                                                                <div class="relative">
                                                                    <span
                                                                        class="text-right text-gray-900 cart-unit-price retro font-weight-bold color-primary small text-nowrap"
                                                                        data-value="{{ $cartItem->qty }}">
                                                                        {{ $cartItem->qty }}</span>

                                                                </div>
                                                            </td>
                                                            <td>
                                                                <font color="green"> <span
                                                                        class="text-right cart-unit-price retro font-weight-bold color-primary small text-nowrap"
                                                                        data-value="{{ $cartItem->price }}">
                                                                        {{ number_format($cartItem->price, 2) }}
                                                                        € </span> </font>


                                                            </td>
                                                            <td>
                                                                <font color="green"> <span
                                                                        class="text-right retro font-weight-bold color-primary small text-nowrap">
                                                                        {{ number_format($cartItem->price * $cartItem->qty, 2) }}
                                                                        € </span> </font>
                                                            </td>
                                                            <td class="text-center">
                                                                <button data-inventory-id="{{ $cartItem->id + 1000 }}"
                                                                    type="button"
                                                                    class="nk-btn nk-btn-xs nes-btn is-error nk-btn-rounded nk-btn-color-white cart-delete-item-id">
                                                                    <i class="fa fa-trash"></i> </button>
                                                            </td>

                                                        </tr>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="nk-gap-2"></div>
                            </div>
                        @endforeach
                    @endforeach
                @endif
        </div>
        </div>

        @include('brcode.front.public.modals.check_promotion')
    @else
        <div class="container">
            <div class="nk-gap-3"></div>
            <div class="row">
                <div class="col-lg-12">
                    <blockquote class="nk-blockquote">
                        <div class="nk-blockquote-icon"></div>
                        <div class="text-center nk-blockquote-content h3 font-extrabold">
                            <font color="black">
                                {{ __('No hay productos en tu carrito de compra. Agrega productos ingresando a las categorías del menu principal') }}
                            </font>
                        </div>
                        <div class="nk-gap"></div>
                        <div class="nk-blockquote-author"></div>
                    </blockquote>
                </div>
            </div>
        </div>
    @endif

    <div class="nk-gap-2"></div>
    </div><!-- /.container -->


    @endif

@endsection

@section('content-script-include')
    <script src="{{ asset('brcode/js/pty.components.js') }}"></script>
@endsection
@section('content-script')
    <script>
        $(document).ready(function() {
            $('.comments').popover();
            $('[data-toggle="popover"]').popover();
            $('.cart-quantity').change(function() {
                qty = $(this).val();
                id = $(this).data('inventory-id');
                pId = $(this).data('product-id');
                var userId = $(this).data('user-id');
                $.showBigOverlay({
                    message: '{{ __('Actualizando tu carro de compras') }}',
                    onLoad: function() {
                        $.ajax({
                            url: '{{ url('/cart/add_item') }}',
                            data: {
                                _token: $('input[name="_token"]:eq(0)').val(),
                                user_id: userId,
                                inventory_id: id,
                                inventory_qty: qty,
                                inventory_typ: 0,
                            },
                            dataType: 'JSON',
                            type: 'POST',
                            success: function(data) {
                                if (data.error == 0) {
                                    window.location.href = window.location.href;
                                } else {

                                }
                                $.showBigOverlay('hide');
                            },
                            error: function(data) {

                            }
                        })
                    }
                });

            });

            $('.cart-delete-item-id').click(function() {
                var userId = $(this).data('user-id');
                var deleteId = $(this).data('delete-id');
                var inventoryId = $(this).data('inventory-id');
                var cartItem = $(this).closest('.cart-item');
                $.showBigOverlay({
                    message: '{{ __('Actualizando tu carro de compras') }}',
                    onLoad: function() {
                        $.ajax({
                            url: '{{ url('/cart/delete_item') }}',
                            data: {
                                _token: $('input[name="_token"]:eq(0)').val(),
                                inventory_id: inventoryId,
                                inventory_qty: 0,
                                user_id: userId,
                                delete_id: deleteId,
                            },
                            dataType: 'JSON',
                            type: 'POST',
                            success: function(data) {
                                if (data.error == 0) {
                                    window.location.href = window.location.href;
                                } else {
                                    $.showBigOverlay('hide');
                                }
                                $.showBigOverlay('hide');
                            },
                            error: function(data) {
                                $.showBigOverlay('hide');
                            }
                        })
                    }
                });

            });

        });

        function updateShipping(x) {
            var selectedShipping = $(x).val();
            var username = $(x).data('id');

            $.ajax({
                url: '{{ url('/cart/add_shipping') }}',
                data: {
                    _token: $('input[name="_token"]:eq(0)').val(),
                    d1: username,
                    d2: selectedShipping,
                },
                dataType: 'JSON',
                type: 'POST',
                success: function(data) {
                    if (data.error == 0) {
                        // Redireccionar a la misma página sin recargar
                        
                    } else {
                        console.log('Hubo un error al actualizar el envío.');
                    }
                },
                error: function(data) {
                    console.log('Hubo un error en la solicitud.');
                }
            });
        }
 
    </script>
@endsection
