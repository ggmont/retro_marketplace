@extends('brcode.front.layout.app_home')
@section('content-css-include')
    <style>
        .btn-notification {
            display: none;
            /* oculta el botón por defecto */
        }
    </style>
    @livewireStyles
@endsection

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <button type="button" class="btn btn-secondary me-05 mb-1 btn-notification"
            onclick="notification('notification-1')">Default</button>

        <button type="button" id="session" style="display:none" class="btn btn-secondary hidden me-05 mb-1"
            onclick="notification('notification-9' , 10000)">Auto Close (3s)</button>


        @if (!Auth::check())
            <div id="notification-1" class="notification-box">
                <div class="notification-dialog android-style">
                    <div class="notification-header">
                        <div class="in">
                            <img src="{{ asset('img/RGM.png') }}"
                                alt="Notificación especial - Retrogaming Market: Alerta importante"
                                class="imaged w24 rounded">
                            <strong>RGM</strong>
                            <span>justo ahora</span>
                        </div>
                        <a href="#" class="close-button">
                            <ion-icon name="close"></ion-icon>
                        </a>
                    </div>
                    <div class="notification-content">
                        <div class="in">
                            <h3 class="subtitle">¡Compra, vende y colecciona en Retro Gaming Market!</h3>
                            <div class="text">
                                ¿Tienes dudas? Revisa aquí nuestras<a
                                href="https://www.retrogamingmarket.eu/Information"> guías y recomendaciones
                            </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="notification-9" class="notification-box">
                <div class="notification-dialog android-style">
                    <div class="notification-header">
                        <div class="in">
                            <img src="{{ asset('img/RGM.png') }}" alt="image" class="imaged w24 rounded">
                            <strong>Notificación automática</strong>
                            <span>Justo Ahora</span>
                        </div>
                        <a href="#" class="close-button">
                            <ion-icon name="close"></ion-icon>
                        </a>
                    </div>
                    <div class="notification-content">
                        <div class="in">
                            <h3 class="subtitle">¿Quieres ver más productos?</h3>
                            <div class="text">
                                Regístrate <a href="{{ url('register-user') }}">aquí</a> y disfruta de nuestra aplicacion
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif





        @if (Auth::user())
            @if (count($viewData['header_notifications']) > 0)
                @if ($viewData['c_h_n'] > 0)
                    <div id="notification-welcome" class="notification-box">
                        <div class="notification-dialog android-style">
                            <div class="notification-header">
                                <div class="in">
                                    <img src="{{ asset('img/RGM.png') }}"
                                        alt="Notificación de bienvenida - Retrogaming Market: Alerta importante"
                                        class="imaged w24 rounded">
                                    <strong>@lang('messages.alert_notification')</strong>
                                    <span>@lang('messages.right_now')</span>
                                </div>
                                <a href="#" class="close-button">
                                    <ion-icon name="close"></ion-icon>
                                </a>
                            </div>
                            <div class="notification-content">
                                <div class="in">
                                    <h3 class="subtitle">@lang('messages.alert_important')
                                    </h3>
                                    <div class="text">
                                        @lang('messages.dont_see_notification')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
        @endif

        @include('partials.flash')


        <br>
        <div class="top-products-area clearfix py-5">
            @livewire('home-product')
        </div>
        <!-- Internet Connection Status-->
        <div class="internet-connection-status" id="internetStatus"></div>
    @else
        @include('partials.flash')


        <br>
        <div class="top-products-area clearfix py-5">
            @livewire('pc.home-product-p-c')
        </div>
        <!-- Internet Connection Status-->
        <div class="internet-connection-status" id="internetStatus"></div>
    @endif
@endsection

@section('content-script-include')
    @livewireScripts

    <script>
        Livewire.on('prueba', function() {
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
        })


        Livewire.on('clearFilters', function () {
            $('#search_country').select2('destroy').val('').select2({
                dropdownParent: $('#exampleModal')
            });
            $('#search_platform').select2('destroy').val('').select2({
                dropdownParent: $('#exampleModal')
            });
            $('#search_category').select2('destroy').val('').select2({
                dropdownParent: $('#exampleModal')
            });
            $('#search_region').select2('destroy').val('').select2({
                dropdownParent: $('#exampleModal')
            });
        });
    </script>

    <!-- Flickity -->
    <script src="{{ asset('assets/vendor/flickity/dist/flickity.pkgd.min.js') }}"></script>

    <!-- Hammer.js -->
    <script src="{{ asset('assets/vendor/hammerjs/hammer.min.js') }}"></script>
@endsection
