@extends('brcode.front.layout.app')
@section('content-css-include')
    <style>
        .br-captcha img {
            width: 220px;
        }
    </style>
@endsection
@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
    @include('partials.flash')
        <div class="header-area" id="headerArea">
            <div class="container h-100 d-flex align-items-center justify-content-between">
                <!-- Back Button-->
                <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                <!-- Page Title-->
                <div class="page-heading">
                    <h6 class="mb-0 font-extrabold">Registrar Compañia</h6>
                </div>
                <!-- Navbar Toggler-->

                @if (Auth::user())
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @else
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @endif
            </div>
        </div>
        <div class="login-form">
            <div class="page-content-wrapper py-3">
                <div class="container">
                    <form action="{{ url('/register-company-account') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="listview-title mt-2">Datos Basicos</div>
                        <div class="section mt-2 mb-3">

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Nombre</label>
                                <div class="input-wrapper">
                                    <input type="text" class="form-control" name="register_name" placeholder="RGM" reqauired>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <label class="form-label" for="name5"> VAT Reg No.</label>
                                <div class="input-wrapper">
                                    <input type="text" class="form-control" name="register_vat" placeholder="Reg No." required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Teléfono</label>
                                <div class="input-wrapper">
                                    <input type="text" class="form-control" name="register_phone" placeholder="0000" required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>



                        </div>
                        <div class="listview-title mt-2">Datos del Nombre Real del usuario</div>
                        <div class="section mt-2 mb-3">

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Nombre</label>
                                <div class="input-wrapper">
                                    <input type="text" class="form-control" name="register_first_name" placeholder="Nombre" required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Apellido</label>
                                <div class="input-wrapper">
                                    <input type="text" class="form-control" name="register_last_name" placeholder="Apellido" required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class="listview-title mt-2">Datos de Dirección y Usuario</div>
                        <div class="section mt-2 mb-3">

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Dirección</label>
                                <div class="input-wrapper">
                                    <input type="text" class="form-control" name="register_address" placeholder="Dirección" required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                                
                            </div>

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Código postal</label>
                                <div class="input-wrapper">
                                    <input type="text" class="form-control" name="register_zipcode" placeholder="Código postal" required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Ciudad</label>
                                <div class="input-wrapper">
                                    <input type="text" class="form-control" name="register_city" placeholder="Ciudad" required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Ciudad</label>
                                <div class="input-wrapper">
                                    <select id="br-register-co-country" class="form-control select2-multiple"
                                    name="register_country" ng-model="brRegisterCoCountry" required
                                    data-br-placeholder="{{ __('Su país') }}" data-br-resolve="Y">
                                    <option style="display:none" value=""> @lang('messages.country_select')</option>
                                    @foreach ($viewData['countries'] as $country)
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Correo Electronico</label>
                                <div class="input-wrapper">
                                    <input type="email" class="form-control" name="register_email" placeholder="rgm@gmail.com" required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Usuario ( Máx. 10 carácteres)</label>
                                <div class="input-wrapper">
                                    <input type="text" class="form-control" name="register_user_name" maxlength="10" placeholder="usuario"
                                        required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>


                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Contraseña - Max 8.</label>
                                <div class="input-wrapper">
                                    <input type="password" class="form-control" name="register_password" placeholder="*******" required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            
                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Captcha</label>
                                <div class="captcha">
                                    {!! NoCaptcha::renderJs() !!}
                                    {!! NoCaptcha::display() !!}
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class=" mt-1 text-start">
                                <div class="form-check">
                                    <input type="checkbox" name="brRegisterCoYearsOld" class="form-check-input" id="br-register-years-old">
                                    <label class="form-check-label" for="br-register-years-old">Tengo mas de 18
                                        años</label>
                                </div>

                            </div>

                            <div class="mt-1 text-start">
                                <div class="form-check">
                                    <input type="checkbox" name="brRegisterCoTerms" class="form-check-input" id="br-register-terms">
                                    <label class="form-check-label" for="br-register-terms"> He leído y acepto los <a
                                            href="#">términos y condiciones & Política de privacidad</a></label>
                                </div>

                            </div>
                            <br>
                            <button id="submitLogin" class="btn mb-3 btn-danger font-extrabold btn-lg w-100"
                            type="submit">Registrarse</button>





                        </div>
                    </form>
                </div>
            </div>
        </div>
    @else
        <!-- Content Header (Page header) -->
        <div class="nk-gap-2"></div>
        <!-- Main content -->
        <div class="container">
            <ul class="nk-breadcrumbs">
                <li>

                    <div class="section-title1">
                        <h3 class="retro">@lang('messages.or_sing_up_register_company')</h3>
                    </div>

                </li>
            </ul>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12" style="background-color: #ffffff; border: solid 1px #db2e2e">
                        <span class="retro">
                            @include('partials.flash')
                            @if (count($errors) > 0)
                                <div class="alert alert-danger alert-important">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </span>
                        <form action="{{ url('/register-company-account') }}" method="POST" class="form-horizontal">
                            {{ csrf_field() }}
                            <br>
                            <div class="col-md-12" ng-controller="registerCompanyController as ctrl">
                                <div class="br-register-completed"
                                    ng-class="{ 'br-anim-register-completed' : ctrl.formSubmitCompleted }">
                                    <span>{!! __(
                                        'Cuenta registrada satisfactoriamente.<br/>Ahora es necesario que revises tu correo para completar la activación de tu cuenta.',
                                    ) !!}</span>
                                </div>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro"
                                    for="grid-city">
                                    @lang('messages.company_basic_data')
                                </h5>
                                <br>
                                <div class="flex flex-wrap mb-2 -mx-3">
                                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="br-register-co-first-name">
                                            @lang('messages.name')
                                        </label>
                                        <input
                                            class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                            name="register_name" type="text" placeholder="RGM" required>
                                    </div>
                                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="br-register-co-first-name">
                                            VAT Reg No.
                                        </label>
                                        <div class="relative">
                                            <input
                                                class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                                name="register_vat" type="text" placeholder="Reg No." required>
                                        </div>
                                    </div>
                                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="br-register-co-first-name">
                                            @lang('messages.phone')
                                        </label>
                                        <input
                                            class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                            name="register_phone" placeholder="{{ __('Teléfono') }}" required
                                            type="text">
                                    </div>
                                </div>
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro"
                                    for="grid-city">
                                    @lang('messages.company_basic_name')
                                </h5>
                                <br>
                                <div class="flex flex-wrap mb-6 -mx-3">
                                    <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="grid-first-name">
                                            @lang('messages.name')
                                        </label>
                                        <input
                                            class="block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border border-red-500 rounded appearance-none focus:outline-none focus:bg-white"
                                            name="register_first_name" placeholder=" @lang('messages.name')" required
                                            type="text">
                                    </div>
                                    <div class="w-full px-3 md:w-1/2">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="grid-last-name">
                                            @lang('messages.last_names')
                                        </label>
                                        <input
                                            class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                            name="register_last_name" placeholder="@lang('messages.last_names')" required
                                            type="text">
                                    </div>
                                </div>
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro"
                                    for="grid-city">
                                    @lang('messages.company_basic_direction')
                                </h5>
                                <br>
                                <div class="flex flex-wrap mb-2 -mx-3">
                                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="br-register-co-first-name">
                                            @lang('messages.direction')
                                        </label>
                                        <input
                                            class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                            name="register_address" placeholder="@lang('messages.direction')" type="text"
                                            required>
                                    </div>
                                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="br-register-co-first-name">
                                            @lang('messages.postal_code')
                                        </label>
                                        <div class="relative">
                                            <input
                                                class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                                name="register_zipcode" placeholder="@lang('messages.postal_code')" required
                                                type="number">
                                        </div>
                                    </div>
                                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="br-register-co-first-name">
                                            @lang('messages.city_company')
                                        </label>
                                        <input
                                            class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                            name="register_city" placeholder="@lang('messages.city_company')" required
                                            type="text">
                                    </div>

                                </div>
                                <div class="form-group"
                                    ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCoCountry.$dirty) || (formRegistration.brRegisterCoCountry.$dirty && formRegistration.brRegisterCoCountry.$invalid) }">
                                    <label for="br-register-co-country"
                                        class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">Pais</label>
                                    <div class="col-md-12">
                                        <select id="br-register-co-country" class="form-control br-ajs-select2"
                                            name="register_country" ng-model="brRegisterCoCountry"
                                            data-br-placeholder="{{ __('Su país') }}" data-br-resolve="Y">
                                            <option style="display:none" value=""> @lang('messages.country_select')</option>
                                            @foreach ($viewData['countries'] as $country)
                                                <option value="{{ $country->id }}">{{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div
                                    class="flex flex-wrap -mx-3 mb-6 {{ $errors->has('register_email') ? 'has-error' : '' }}">
                                    <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="grid-first-name">
                                            @lang('messages.email_users')
                                        </label>
                                        <input
                                            class="block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border border-red-500 rounded appearance-none focus:outline-none focus:bg-white"
                                            name="register_email" ng-model="brRegisterCoEmail" id="br-register-co-email"
                                            placeholder="{{ __('Introduzca su correo') }}" required type="email">
                                        <span style="color:red"
                                            ng-show="(formRegistration.$submitted && ! formRegistration.brRegisterCoEmail.$dirty) || (formRegistration.brRegisterCoEmail.$dirty && formRegistration.brRegisterCoEmail.$invalid)">
                                            <span
                                                ng-show="formRegistration.brRegisterCoEmail.$error.email">{{ __('El correo introducido es inválido') }}.</span>
                                        </span>
                                    </div>
                                    <div class="w-full px-3 md:w-1/2">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="grid-last-name">
                                            @lang('messages.user')
                                        </label>
                                        <input
                                            class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                            name="register_user_name" ng-model="brRegisterCoUserName"
                                            id="br-register-co-username"
                                            placeholder="{{ __('Su nuevo nombre de usuario') }}" required type="text">
                                    </div>
                                </div>
                                <div class="flex flex-wrap mb-6 -mx-3">
                                    <div class="w-full px-3">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="grid-password">
                                            @lang('messages.pass_company') - <span class="text-red-700">@lang('messages.pass_company_warning')</span>
                                        </label>
                                        <input
                                            class="block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                            id="grid-password" type="password" name="register_password"
                                            ng-model="brRegisterCoPassword" id="br-register-co-password"
                                            placeholder="{{ __('Cree una contraseña') }}">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="captcha">
                                        {!! NoCaptcha::renderJs() !!}
                                        {!! NoCaptcha::display() !!}
                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <br>

                                    <input id="captcha" ng-model="captcha" type="text"
                                        class="block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                        placeholder="{{ __('Escriba los carácteres de la imagen anterior') }}"
                                        name="captcha">
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCoTerms.$dirty) || (formRegistration.brRegisterCoTerms.$dirty && formRegistration.brRegisterCoTerms.$invalid) }">
                                        <label>
                                            <input type="checkbox" class="nes-checkbox" name="brRegisterCoTerms"
                                                ng-model="brRegisterCoTerms" id="br-register-co-terms" required />
                                            <span class="text-xs retro">@lang('messages.term_and_conditions_one')<a
                                                    href="/site/terminos-y-condiciones" target="_blank"> <i
                                                        class="fa fa-link" aria-hidden="true"></i>
                                                    @lang('messages.term_and_conditions_two') </a> y <a href="/site/politica-de-privacidad"
                                                    target="_blank"><i class="fa fa-link" aria-hidden="true"></i>
                                                    @lang('messages.private') </a></span>
                                        </label>

                                    </div>

                                    <div class="col-md-6"
                                        ng-class="{ 'has-error' : (formRegistration.$submitted && ! formRegistration.brRegisterCoYearsOld.$dirty) || (formRegistration.brRegisterCoYearsOld.$dirty && formRegistration.brRegisterCoYearsOld.$invalid) }">
                                        <label>
                                            <input type="checkbox" class="nes-checkbox" name="brRegisterCoYearsOld"
                                                ng-model="brRegisterCoYearsOld" id="br-register-co-years-old" required />
                                            <span class="text-xs retro"> {{ __('Tengo mas de 18 años') }}</span>
                                        </label>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">

                                        <button type="submit"
                                            class="nes-btn retro nk-btn-lg nk-btn-rounded nk-btn-color-white btn-block"
                                            id="br-submit-register-co"><i class="fa fa-user-plus"></i>
                                            {{ __('Registrarse') }}</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div><!-- /.col-md-* -->
                    <!----->
                </div>
            </div>
        </div><!-- /.row -->
        </div><!-- /.container -->
        <div class="nk-gap-2"></div>
    @endif

@endsection


@section('content-script-include-ang')
    <script src="{{ asset('brcode/js/app.public.angjs-register-company.js') }}"></script>
@endsection
@section('content-script')
<script>
    $(document).ready(function() {
        // Select2 Multiple
        $('.select2-multiple').select2({
        });

    });

</script>
@endsection
