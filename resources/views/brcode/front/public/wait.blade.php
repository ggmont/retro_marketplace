@extends('brcode.front.layout.app')
@section('content-css-include')
    <style>
        .tab {
            overflow: hidden;
        }

        .detail-price {
            font-size: 2rem;
            font-weight: 700;
            display: inline-block;
        }

        .nk-product-meta ul li::before {
            content: "•";
            margin-right: 0.5rem;
            color: black;
        }

        .detail-title {
            font-size: 2rem;
            padding: 0 0 16px;
            border-bottom: 1px solid #ECEFF1;
            line-height: 1.2em;
            margin-top: 8px;
            overflow: hidden;
            text-overflow: ellipsis;
        }


        .tab-content {
            max-height: 0;
            transition: all 0.5s;
        }

        input:checked+.tab-label .test {
            background-color: #000;
        }

        input:checked+.tab-label .test svg {
            transform: rotate(180deg);
            stroke: #fff;
        }

        input:checked+.tab-label::after {
            transform: rotate(90deg);
        }

        input:checked~.tab-content {
            max-height: 100vh;
        }

        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Jost', sans-serif;
            font-weight: 800;
        }

        .w-88 {
            width: 99% !important;
        }
    </style>
@endsection
@section('content')
<br><br>
<div class="flex flex-col items-center justify-center">
    <img src="{{ asset('img/wait.png') }}" alt="alt" class="imaged square w-64 mb-8">
    <h1 class="text-4xl font-bold mb-4">¡Estamos trabajando en ello!</h1>
    <div class="text-xl text-center mb-8">
       Estamos mejorando nuestro sitio web para ofrecerte la mejor experiencia posible. Vuelve pronto.
    </div>
    <a href="https://www.retrogamingmarket.eu/landing" class="btn btn-danger   rounded-md text-lg font-bold">Más información</a>
</div>

@endsection
@section('content-script-include')

    <script src="{{ asset('brcode/js/pty.components.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>


    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="{{ asset('assets/noty/packaged/jquery.noty.packaged.min.js') }}"></script>

    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.4.1/chart.min.js"
        integrity="sha512-5vwN8yor2fFT9pgPS9p9R7AszYaNn0LkQElTXIsZFCL7ucT8zDCAqlQXDdaqgA1mZP47hdvztBMsIoFxq/FyyQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection
@section('content-script')
    @livewireScripts

    @stack('scripts')
@endsection
