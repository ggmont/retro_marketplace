@extends('brcode.front.layout.app')
@section('content-css-include')
    <style>
        .tab {
            overflow: hidden;
        }

        .tab-content {
            max-height: 0;
            transition: all 0.5s;
        }

        input:checked+.tab-label .test {
            background-color: #000;
        }

        input:checked+.tab-label .test svg {
            transform: rotate(180deg);
            stroke: #fff;
        }

        input:checked+.tab-label::after {
            transform: rotate(90deg);
        }

        input:checked~.tab-content {
            max-height: 100vh;
        }

        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Jost', sans-serif;
            font-weight: 800;
        }

        .w-88 {
            width: 99% !important;
        }
    </style>
@endsection
@section('content')
    @if (Auth::user())
        <div class="modal fade" id="modalComplaint" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog complaint-form" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title">¿Porque quieres denunciar este producto?</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <form action="{{ route('complaint_product', $producto->id) }}" method="POST">
                                {{ csrf_field() }}
                                <input name="user_id" type="hidden" value="{{ auth()->user()->id }}">
                                <div class="wide-block pb-1 pt-2">
                                    <div class="input-list">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" value="Imagen Equivocada"
                                                name="category" id="radioList1">
                                            <label class="form-check-label" for="radioList1">Imagen Equivocada</label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" value="Datos incorrectos"
                                                name="category" id="radioList2">
                                            <label class="form-check-label" for="radioList2">Datos incorrectos</label>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit"
                                class="btn confirmclosed btn-submit btn-primary w-100 text-lg font-extrabold">Enviar</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div class="top-products-area clearfix py-3" style="margin-bottom: 47.2188px; padding-bottom: 5rem;">
            <!-- START: Breadcrumbs -->

            <div id="notification-6" class="notification-box">
                <div class="notification-dialog android-style">
                    <div class="notification-header">
                        <div class="in">
                            <img src="{{ asset('img/RGM.png') }}"
                                alt="Notificación automática de RetroGamingMarket: Hecho! - Añadido al carrito correctamente"
                                class="imaged w24 rounded">
                            <strong>Notificación automática</strong>
                            <span>Justo Ahora</span>
                        </div>
                        <a href="#" class="close-button">
                            <ion-icon name="close"></ion-icon>
                        </a>
                    </div>
                    <div class="notification-content">
                        <div class="in">
                            <h3 class="subtitle">Hecho!</h3>
                            <div class="text">
                                Añadido al carrito correctamente
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="notification-7" class="notification-box">
                <div class="notification-dialog android-style">
                    <div class="notification-header">
                        <div class="in">
                            <img src="{{ asset('img/RGM.png') }}"
                                alt="Notificación automática de RetroGamingMarket: Hecho! - Tu producto fue eliminado"
                                class="imaged w24 rounded">
                            <strong>Notificación automática</strong>
                            <span>Justo Ahora</span>
                        </div>
                        <a href="#" class="close-button">
                            <ion-icon name="close"></ion-icon>
                        </a>
                    </div>
                    <div class="notification-content">
                        <div class="in">
                            <h3 class="subtitle">Hecho!</h3>
                            <div class="text">
                                Tu producto fue eliminado
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="header-area" id="headerArea">
                <div class="container h-100 d-flex align-items-center justify-content-between">
                    <!-- Back Button-->
                    <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                    <!-- Page Title-->
                    <div class="page-heading">
                        <h6 class="mb-0 font-extrabold">@lang('messages.detail_product')</h6>
                    </div>
                    <!-- Navbar Toggler-->

                    <div class="normal">
                        @if (Auth::user())
                            <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebarPanel">
                                <span></span><span></span><span></span>
                            </div>
                        @else
                            <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebarPanel">
                                <span></span><span></span><span></span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <!-- END: Breadcrumbs -->
            <div class="product-slides owl-carousel">

                <div class="relative">
                    @if (count($viewData['product_images']) > 0)
                        <a href="/images/{{ $viewData['product_images'][0]->image_path }}" class="fancybox"
                            data-fancybox="RGM">
                            <img class="single-product-slide" src="/images/{{ $viewData['product_images'][0]->image_path }}"
                                alt="{{ $producto->name }} - RetroGamingMarket">
                        </a>
                        @if (Auth::user())
                            <button id="showComplaint"
                                style="     
                            font-size: 20px;  
                          border: 1px solid rgb(200, 200, 200);
                        box-shadow: rgba(0, 0, 0, 0.1) 0px 5px 5px 2px;
                        background: rgba(14, 0, 0, 0.6);"
                                class="showComplaint absolute bottom-30 right-0  text-white p-2 rounded hover:bg-blue-800 m-2">
                                <ion-icon name="alert-circle-outline"></ion-icon>
                            </button>
                        @endif
                    @else
                        <img class="single-product-slide" loading="lazy"
                            src="{{ asset('assets/images/art-not-found.jpg') }}"
                            alt="Producto no encontrado - RetroGamingMarket">
                    @endif
                </div>

                @if (count($viewData['product_images']) > 1)
                    @foreach ($viewData['product_images'] as $key)
                        @if (!$loop->first)
                            <div class="relative">
                                @if ($viewData['product_images'][0]->id != $key->id)
                                    <a href="/images/{{ $key->image_path }}" class="fancybox" data-fancybox="RGM">
                                        <img class="single-product-slide" src="/images/{{ $key->image_path }}"
                                            alt="{{ $producto->name }} - RetroGamingMarket">
                                    </a>
                                    @if (Auth::user())
                                        <button id="showComplaint"
                                            style="     
                                        font-size: 20px;  
                                      border: 1px solid rgb(200, 200, 200);
                                    box-shadow: rgba(0, 0, 0, 0.1) 0px 5px 5px 2px;
                                    background: rgba(14, 0, 0, 0.6);"
                                            class="showComplaint absolute bottom-25 right-0  text-white p-2 rounded hover:bg-blue-800 m-2">
                                            <ion-icon name="alert-circle-outline"></ion-icon>
                                        </button>
                                    @endif
                                @endif
                            </div>
                        @endif
                    @endforeach
                @endif

            </div>

            <div class="product-description pb-3">
                <div class="container">
                    <div class="p-title-price text-center">
                        <h2 class="mt-6 mb-2 font-extrabold">{{ $producto->name }}</h2>
                        <h3>{{ $producto->platform }} - {{ $producto->region }}</h2>

                    </div>
                </div>
            </div>





            <div class="product-description">

                <!-- Product Specification-->
                <div class="p-specification bg-white mb-2 py-2">
                    <div class="container">
                        <div class="p-title-price text-center">

                            <button type="button" id="cartrgm" style="display:none"
                                class="btn btn-secondary hidden me-05 mb-1"
                                onclick="notification('notification-6' , 3000)">Auto Close (3s)</button>

                            <button type="button" id="deletergm" style="display:none"
                                class="btn btn-secondary hidden me-05 mb-1"
                                onclick="notification('notification-7' , 3000)">Auto Close (3s)</button>

                            <h5 class="mb-1 font-extrabold">- @lang('messages.general_price') -</h5>
                            <br>
                            <?php
                            $duration = [];
                            $duration_not_press = [];
                            $duration_used = [];
                            $duration_new = [];
                            $duration_general = [];
                            $t = '';
                            $total_numeros = '';
                            $total_numeros_not_press = '';
                            $total_numeros_used = '';
                            $total_numeros_new = '';
                            $total_numeros_general = '';
                            $suma = '';
                            $suma_not_press = '';
                            $suma_used = '';
                            $suma_new = '';
                            $suma_general = '';
                            $m = '';
                            $promedio_not_press = '';
                            $promedio_used = '';
                            $promedio_new = '';
                            $promedio_general = '';
                            $prueba = '';
                            $fecha = '';
                            $precio = '';
                            
                            foreach ($producto->inventory as $item) {
                                $cantidad = $item->price;
                                $duration[] = $cantidad;
                                $total_numeros = count($duration);
                                $total = max($duration);
                                $t = min($duration);
                                $suma = array_sum($duration);
                                $m = $suma / $total_numeros;
                                $prueba = $item->created_at;
                            }
                            
                            //PROMEDIO GENERAL
                            foreach ($seller_sold_general as $p) {
                                $cantidad_general = $p->price;
                                $duration_general[] = $cantidad_general;
                                $total_numeros_general = count($duration_general);
                                $suma_general = array_sum($duration_general);
                                $promedio_general = $suma_general / $total_numeros_general;
                            }
                            
                            //PROMEDIO PARA LAS CONDICIONES TIPO "NOT PRESS - NO TIENE" POKEMON GOLD ES EL QUE NO TIENE ASTERIX Y OBELIS TAMPOCO TIENEN
                            foreach ($seller_sold_not_pres as $p) {
                                $cantidad_press = $p->price;
                                $duration_not_press[] = $cantidad_press;
                                $total_numeros_not_press = count($duration_not_press);
                                $suma_not_press = array_sum($duration_not_press);
                                $promedio_not_press = $suma_not_press / $total_numeros_not_press;
                            }
                            
                            //PROMEDIO PARA LAS CONDICIONES TIPO "USED" WOODY ES EL QUE ESTA USADO
                            foreach ($seller_sold_used as $p_used) {
                                $cantidad_used = $p_used->price;
                                $duration_used[] = $cantidad_used;
                                $total_numeros_used = count($duration_used);
                                $suma_used = array_sum($duration_used);
                                $promedio_used = $suma_used / $total_numeros_used;
                            }
                            
                            //PROMEDIO PARA LAS CONDICIONES TIPO "NEW" XG2 ES EL QUE TIENE EL ESTADO NUEVO
                            foreach ($seller_sold_new as $p_new) {
                                $cantidad_new = $p_new->price;
                                $duration_new[] = $cantidad_new;
                                $total_numeros_new = count($duration_new);
                                $suma_new = array_sum($duration_new);
                                $promedio_new = $suma_new / $total_numeros_new;
                            }
                            
                            ?>

                            <?php
                            $l = $producto->price_solo + $producto->price_used + $producto->price_new / 3;
                            ?>



                            @if ($promedio_general > 0)
                                <div class="row">
                                    <div class="col flex items-center justify-center">
                                        <div class="total-result-of-ratings text-center">
                                            <span>@lang('messages.new_inventory')</span> <!-- Título arriba -->
                                            <br>
                                            <span class="inline-block">
                                                @if ($promedio_new > 0)
                                                    {{ number_format($promedio_new, 2) }} €
                                                @elseif($producto->price_new)
                                                    {{ number_format($producto->price_new, 2) }} €
                                                @else
                                                    No info
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col flex items-center justify-center">
                                        <div class="total-result-of-ratings-used text-center">
                                            <span>@lang('messages.used_inventory')</span> <!-- Título arriba -->
                                            <br>
                                            <span class="inline-block">
                                                @if ($promedio_used > 0)
                                                    {{ number_format($promedio_used, 2) }} €
                                                @elseif($producto->price_used)
                                                    {{ number_format($producto->price_used, 2) }} €
                                                @else
                                                    No info
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col flex items-center justify-center">
                                        <div class="total-result-of-ratings-solo text-center">
                                            <span>@lang('messages.only_inventory')</span> <!-- Título arriba -->
                                            <br>
                                            <span class="inline-block">
                                                @if ($promedio_not_press > 0)
                                                    {{ number_format($promedio_not_press, 2) }} €
                                                @elseif($producto->price_solo)
                                                    {{ number_format($producto->price_solo, 2) }} €
                                                @else
                                                    No info
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="row">
                                    <div class="col flex items-center justify-center">
                                        <div class="total-result-of-ratings text-center">
                                            <span>@lang('messages.new_inventory')</span> <!-- Título arriba -->
                                            <br>
                                            <span class="inline-block">

                                                @if ($producto->price_new > 0)
                                                    {{ number_format($producto->price_new, 2) }} €
                                                @else
                                                    No info
                                                @endif

                                            </span>
                                        </div>
                                    </div>
                                    <div class="col flex items-center justify-center">
                                        <div class="total-result-of-ratings-used text-center">
                                            <span>@lang('messages.used_inventory')</span> <!-- Título arriba -->
                                            <br>
                                            <span class="inline-block">
                                                @if ($producto->price_used > 0)
                                                    {{ number_format($producto->price_used, 2) }} €
                                                @else
                                                    No info
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col flex items-center justify-center">
                                        <div class="total-result-of-ratings-solo text-center">
                                            <span>@lang('messages.only_inventory')</span> <!-- Título arriba -->
                                            <br>
                                            <span class="inline-block">
                                                @if ($producto->price_solo > 0)
                                                    {{ number_format($producto->price_solo, 2) }} €
                                                @else
                                                    No info
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>



                <div class="flex justify-center">
                    <a class="btn btn-danger mt-3" href="/offerproduct/{{ $producto->id + 1000 }}">
                        <span class="retro font-extrabold">@lang('messages.sell_similar_product')</span>
                    </a>
                </div>



            </div>



            <div class="rating-and-review-wrapper bg-white py-3 mb-3">
                <!-- Button trigger modal -->

                @if (count($viewData['inventory']) > 0)
                    @if ($producto->catid == 1)
                        @livewire('mobile.mobile-inventory-game-table', ['producto' => $producto])
                    @elseif ($producto->catid == 2)
                        @livewire('mobile.mobile-inventory-console', ['producto' => $producto])
                    @else
                        @livewire('mobile.mobile-inventory-accesories', ['producto' => $producto])
                    @endif
                @else
                    <div class="row">
                        <div class="col-lg-12">
                            <blockquote class="nk-blockquote">
                                <div class="nk-blockquote-icon"></div>
                                <div class="text-center nk-blockquote-content h4">
                                    <font color="black">
                                        {{ __('No hay inventario disponible para este producto') }}
                                    </font>
                                </div>
                                <div class="nk-gap"></div>
                                <div class="nk-blockquote-author"></div>
                            </blockquote>
                        </div>
                    </div>
                @endif
            </div>


        </div>
    @else
        <div class="nk-main">
            <!-- START: Breadcrumbs -->
            <div class="nk-gap-1"></div>
            <div class="container">
                <ul class="nk-breadcrumbs">
                    <li>

                        <div class="section-title1">
                            <h3 class="retro">@lang('messages.information_product')</h3>
                        </div>

                    </li>
                </ul>
            </div>
            <div class="nk-gap-3"></div>

            <!-- END: Breadcrumbs -->
            <div class="container bg-white">
                <div class="row vertical-gap">
                    <div class="col-lg-7">
                        <div class="nk-store-product">
                            <div class="row vertical-gap">
                                <div class="col-md-4 item">
                                    @if (count($viewData['product_images']) > 0)
                                        <a href="/images/{{ $viewData['product_images'][0]->image_path }}"
                                            class="fancybox" data-fancybox="RGM1">
                                            <img src="/images/{{ $viewData['product_images'][0]->image_path }}"
                                                width="100%" height="100%" />
                                        </a>
                                        @if (count($viewData['product_images']) > 1)
                                            <div class="nk-gap-1"></div>
                                            <div class="row vertical-gap sm-gap">
                                                @foreach ($viewData['product_images'] as $key)
                                                    @if ($viewData['product_images'][0]->id != $key->id)
                                                        <div class="col-6 col-md-4">
                                                            <a href="/images/{{ $key->image_path }}" class="fancybox"
                                                                data-fancybox="RGM1">
                                                                <img src="/images/{{ $key->image_path }}" width="100%"
                                                                    height="100%" />
                                                            </a>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        @endif
                                    @else
                                        <a href="{{ url('assets/images/art-not-found.jpg') }}" class="fancybox"
                                            data-fancybox="RGM1">
                                            <img src="{{ url('assets/images/art-not-found.jpg') }}" width="100%"
                                                height="100%" />
                                        </a>
                                    @endif
                                </div>
                                <div class="col-md-6">

                                    <h2 class="nk-product-title h3 retro">
                                        <font color="black">{{ $producto->name_en }}</font>
                                    </h2>
                                    <div class="nk-product-meta">
                                        <div class="lists">
                                            <ul class="nes-list is-disc">
                                                @if ($producto->comments == '')
                                                @else
                                                    <li><b>
                                                            <font color="black">
                                                                {{ $producto->comments }}</font>
                                                        </b></li>
                                                @endif
                                                <li><b>
                                                        <font color="black">{{ __('Fecha de lanzamiento') }}:
                                                            {{ $producto->release_date }}</font>
                                                    </b></li>
                                                <li><b>
                                                        <font color="black">{{ __('Plataforma') }}:
                                                            {{ $producto->platform }}</font>
                                                    </b></li>
                                                <li><b>
                                                        <font color="black">{{ __('Región') }}:
                                                            {{ $producto->region }}</font>
                                                    </b></li>
                                                @if ($producto->volume)
                                                    <li><b>
                                                            <font color="black">{{ __('Volumen') }}:
                                                                {{ $producto->volume }}</font>
                                                        </b></li>
                                                @endif
                                                @if ($producto->weight)
                                                    <li><b>
                                                            <font color="black">{{ __('Peso') }}:
                                                                {{ $producto->weight }}</font>
                                                        </b></li>
                                                @endif
                                                @if ($producto->media)
                                                    <li><b>
                                                            <font color="black">{{ __('Media') }}:
                                                                {{ $producto->media }}</font>
                                                        </b></li>
                                                @endif
                                                @if ($producto->language == '')
                                                @else
                                                    <li><b>
                                                            <font color="black">{{ __('Idioma') }}:
                                                                {{ $producto->language }}</font>
                                                        </b></li>
                                                @endif
                                                @if ($producto->box_language == '')
                                                @else
                                                    <li><b>
                                                            <font color="black">{{ __('Idioma de Caja') }}:
                                                                {{ $producto->box_language }}</font>
                                                        </b></li>
                                                @endif
                                                @if ($producto->ean_upc == '')
                                                @else
                                                    <li><b>
                                                            <font color="black">{{ __('Codigo de Barras') }}:
                                                                {{ $producto->ean_upc }}</font>
                                                        </b></li>
                                                @endif
                                            </ul>
                                        </div>


                                        <div class="nk-gap"></div>
                                        <a class="nes-btn is-error" href="/offerproduct/{{ $producto->id + 1000 }}"><span
                                                class="retro">@lang('messages.sell_similar')</span></a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <aside class="nk-sidebar nk-sidebar-right nk-sidebar-sticky">
                        @if (Auth::user())
                            <?php
                            $l = $producto->price_solo + $producto->price_used + $producto->price_new / 3;
                            ?>
                            @if (count($chart_sold) > 0)
                                <center><small class="retro"> - @lang('messages.selects') -</small></center>
                                @livewire('product-price', ['producto' => $producto])
                            @elseif($l > 0)
                                <div class="nes-container with-title is-centered">
                                    <p class="title retro text-gray-900">GENERAL</p>
                                    <p class="text-center text-lg retro">{{ number_format($l, 2) }} €</p>
                                </div>
                                <div class="col-md-12">
                                    @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                        <div class="mt-10">
                                        @else
                                            <div class="d-flex justify-content-between mt-10">
                                    @endif
                                    <button class="nk-btn nk-btn-outline nk-btn-color-danger retro">Solo <br> <span
                                            class="muyusado">
                                            {{ number_format($producto->price_solo, 2) }} €
                                        </span></button>
                                    <button class="nk-btn nk-btn-outline nk-btn-color-warning retro">Usado <br> <span
                                            class="usado">
                                            {{ number_format($producto->price_used, 2) }}
                                        </span></button>

                                    <button class="nk-btn nk-btn-outline nk-btn-color-success retro">Nuevo <br> <span
                                            class="nuevo">
                                            {{ number_format($producto->price_new, 2) }}
                                        </span></button>

                                </div>
                </div>
    @endif
    @endif
    <div class="nk-widget">
        <div class="nk-widget-content">
            @if (Auth::user())
                <br>
                <div class="social-share">
                    @if ((new \Jenssegers\Agent\Agent())->isMobile())
                    @else
                        <h4 class="text-md retro">
                            <font color="black">
                                &nbsp;&nbsp;&nbsp;@lang('messages.share_this_product')&nbsp;&nbsp;&nbsp;&nbsp;
                            </font>
                        </h4>
                    @endif
                    <center>
                        <ul class="socil-icon2">
                            <li><a href="#" id="facebook-btn" target="_blank"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#" id="twitter-btn" target="_blank"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#" id="whatsapp-btn" target="_blank"><i class="fa fa-whatsapp"></i></a>
                            </li>
                            <li><a href="#" id="linkedin-btn" target="_blank"><i
                                        class="fa fa-linkedin-square"></i></a>
                            </li>
                            <li><a href="#" id="gmail-btn" target="_blank"><i class="fa fa-envelope-o"></i></a>
                            </li>
                        </ul>
                    </center>
                </div>
            @endif

        </div>
        <div class="nk-gap-3"></div>
    </div>
    </aside>
    </div>
    </div>

    </div>





    <div class="container">

        <!-- Content Header (Page header) -->
        @if (count($viewData['inventory']) > 0)
            @if ($producto->catid == 1)
                @livewire('product.inventory-game-table', ['producto' => $producto])
            @elseif($producto->catid == 2)
                @livewire('product.inventory-console-table', ['producto' => $producto])
            @elseif($producto->catid == 3)
                @livewire('product.inventory-periferico-table', ['producto' => $producto])
            @elseif($producto->catid == 4)
                @livewire('product.inventory-accesorio-table', ['producto' => $producto])
            @elseif($producto->catid == 172)
                @livewire('product.inventory-merchadising-table', ['producto' => $producto])
            @endif
        @else
            <div class="container">
                <div class="nk-gap-3"></div>
                <div class="row">
                    <div class="col-lg-12">
                        <blockquote class="nk-blockquote">
                            <div class="nk-blockquote-icon"></div>
                            <div class="text-center nk-blockquote-content h4 retro">
                                <font color="black">
                                    {{ __('No hay inventario disponible para este producto') }}
                                </font>
                            </div>
                            <div class="nk-gap"></div>
                            <div class="nk-blockquote-author"></div>
                        </blockquote>
                    </div>
                </div>
            </div>
        @endif








        @endif

    @endsection

    @section('content-script-include')
        <script src="{{ asset('brcode/js/pty.components.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>


        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

        <script src="{{ asset('assets/noty/packaged/jquery.noty.packaged.min.js') }}"></script>

        <!-- date-range-picker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.4.1/chart.min.js"
            integrity="sha512-5vwN8yor2fFT9pgPS9p9R7AszYaNn0LkQElTXIsZFCL7ucT8zDCAqlQXDdaqgA1mZP47hdvztBMsIoFxq/FyyQ=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    @endsection
    @section('content-script')
        @livewireScripts


        <script>
            Livewire.on('clearFilters', function() {
                $('#search_country').select2('destroy').val('').select2({
                    dropdownParent: $('#exampleModal')
                });
                $('#search_box').select2('destroy').val('').select2({
                    dropdownParent: $('#exampleModal')
                });
                $('#cover_condition').select2('destroy').val('').select2({
                    dropdownParent: $('#exampleModal')
                });
                $('#search_manual').select2('destroy').val('').select2({
                    dropdownParent: $('#exampleModal')
                });
                $('#search_game').select2('destroy').val('').select2({
                    dropdownParent: $('#exampleModal')
                });
                $('#search_extra').select2('destroy').val('').select2({
                    dropdownParent: $('#exampleModal')
                });
            });
        </script>

        <script>
            Livewire.on('clearFiltersConsole', function() {
                $('#search_country').select2('destroy').val('').select2({
                    dropdownParent: $('#filterModal')
                });
                $('#search_box').select2('destroy').val('').select2({
                    dropdownParent: $('#filterModal')
                });
                $('#cover_condition').select2('destroy').val('').select2({
                    dropdownParent: $('#filterModal')
                });
                $('#search_manual').select2('destroy').val('').select2({
                    dropdownParent: $('#filterModal')
                });
                $('#search_game').select2('destroy').val('').select2({
                    dropdownParent: $('#filterModal')
                });
                $('#search_inside').select2('destroy').val('').select2({
                    dropdownParent: $('#filterModal')
                });
                $('#search_extra').select2('destroy').val('').select2({
                    dropdownParent: $('#filterModal')
                });
            });
        </script>


        <script>
            Livewire.on('clearFiltersAccesories', function() {
                $('#search_country').select2('destroy').val('').select2({
                    dropdownParent: $('#accesorieModal')
                });
                $('#search_box').select2('destroy').val('').select2({
                    dropdownParent: $('#accesorieModal')
                });
                $('#search_game').select2('destroy').val('').select2({
                    dropdownParent: $('#accesorieModal')
                });
                $('#search_extra').select2('destroy').val('').select2({
                    dropdownParent: $('#accesorieModal')
                });
                $('#search_region').select2('destroy').val('').select2({
                    dropdownParent: $('#accesorieModal')
                });
            });
        </script>

        <script>
            Livewire.on('game', function() {


                var $inventoryAdd = $('.br-btn-product-add-game');
                $(function() {
                    $('[data-toggle="popover"]').popover()
                })
                $('.nk-btn-modify-inventory').click(function() {
                        var url = $(this).data('href');
                        var tr = $(this).closest('tr');
                        var sel = $(this).closest('tr').find('select').prop('value');
                        console.log(sel);
                        //console.log(url);
                        location.href = `${url}/${sel}`;
                    }),
                    $inventoryAdd.click(function() {
                        var userId = $(this).data('user-id');
                        var inventoryId = $(this).data('inventory-id');
                        var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty').val();
                        var stock = $(this).closest('tr').find('div.qty-new');
                        var tr = $(this).closest('tr');
                        var select = $(this).closest('tr').find('select.br-btn-product-qty');

                        if (inventoryQty === '0') {
                            // Mostrar la alerta SweetAlert si la cantidad es 0
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Ya no está disponible este producto para su venta.',
                            });
                            return; // Salir de la función para evitar la solicitud AJAX
                        }

                        $.showBigOverlay({
                            message: '{{ __('Agregando producto a tu carro de compras') }}',
                            onLoad: function() {
                                $.ajax({
                                    url: '{{ url('/cart/add_item') }}',
                                    data: {
                                        _token: $('meta[name="csrf-token"]').attr('content'),
                                        inventory_id: inventoryId,
                                        inventory_qty: inventoryQty,
                                        user_id: userId,
                                    },
                                    dataType: 'JSON',
                                    type: 'POST',
                                    success: function(data) {
                                        if (data.productCountForVendor >= 4) {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops...',
                                                text: 'No puedes agregar más de 4 productos diferentes por usuario y carrito.',
                                                footer: '<a href="#">¿Por qué existe esta limitación?</a>'
                                            });
                                            $.showBigOverlay('hide');
                                        } else {
                                            if (data.error == 0) {
                                                $('#br-cart-items').text(data.items);
                                                stock.html(data.Qty);
                                                if (data.Qty == 0) {
                                                    tr.remove();
                                                } else {
                                                    select.html('');
                                                    for (var i = 1; i < data.Qty + 1; i++) {
                                                        select.append('<option value=' + i +
                                                            '>' + i + '</option>');
                                                    }
                                                }
                                            }
                                            $.showBigOverlay('hide');
                                            document.getElementById("cartrgm").click();
                                        }
                                    },
                                    error: function(data) {
                                        $.showBigOverlay('hide');
                                    }
                                });
                            }
                        });
                    });

            })
        </script>

        <script>
            Livewire.on('console', function() {


                var $inventoryAdd = $('.br-btn-product-add-console');
                $(function() {
                    $('[data-toggle="popover"]').popover()
                })
                $('.nk-btn-modify-inventory').click(function() {
                        var url = $(this).data('href');
                        var tr = $(this).closest('tr');
                        var sel = $(this).closest('tr').find('select').prop('value');
                        console.log(sel);
                        //console.log(url);
                        location.href = `${url}/${sel}`;
                    }),
                    $inventoryAdd.click(function() {
                        var inventoryId = $(this).data('inventory-id');
                        var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                            .val();
                        var stock = $(this).closest('tr').find('div.qty-new');
                        var tr = $(this).closest('tr');
                        var select = $(this).closest('tr').find('select.br-btn-product-qty');

                        if (inventoryQty === '0') {
                            // Mostrar la alerta SweetAlert si la cantidad es 0
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Ya no está disponible este producto para su venta.',
                            });
                            return; // Salir de la función para evitar la solicitud AJAX
                        }

                        $.showBigOverlay({
                            message: '{{ __('Agregando producto a tu carro de compras') }}',
                            onLoad: function() {
                                $.ajax({
                                    url: '{{ url('/cart/add_item') }}',
                                    data: {
                                        _token: $('meta[name="csrf-token"]').attr('content'),
                                        inventory_id: inventoryId,
                                        inventory_qty: inventoryQty,
                                        user_id: userId,
                                    },
                                    dataType: 'JSON',
                                    type: 'POST',
                                    success: function(data) {
                                        if (data.productCountForVendor >= 4) {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops...',
                                                text: 'No puedes agregar más de 4 productos diferentes por usuario y carrito.',
                                                footer: '<a href="#">¿Por qué existe esta limitación?</a>'
                                            });
                                            $.showBigOverlay('hide');
                                        } else {
                                            if (data.error == 0) {
                                                $('#br-cart-items').text(data.items);
                                                stock.html(data.Qty);
                                                if (data.Qty == 0) {
                                                    tr.remove();
                                                } else {
                                                    select.html('');
                                                    for (var i = 1; i < data.Qty + 1; i++) {
                                                        select.append('<option value=' + i +
                                                            '>' + i + '</option>');
                                                    }
                                                }
                                            }
                                            $.showBigOverlay('hide');
                                            document.getElementById("cartrgm").click();
                                        }
                                    },
                                    error: function(data) {
                                        $.showBigOverlay('hide');
                                    }
                                });
                            }
                        });
                    });
            })
        </script>

        <script>
            Livewire.on('periferico', function() {


                var $inventoryAdd = $('.br-btn-product-add-periferico');
                $(function() {
                    $('[data-toggle="popover"]').popover()
                })
                $('.nk-btn-modify-inventory').click(function() {
                        var url = $(this).data('href');
                        var tr = $(this).closest('tr');
                        var sel = $(this).closest('tr').find('select').prop('value');
                        console.log(sel);
                        //console.log(url);
                        location.href = `${url}/${sel}`;
                    }),
                    $inventoryAdd.click(function() {
                        var inventoryId = $(this).data('inventory-id');
                        var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                            .val();
                        var stock = $(this).closest('tr').find('div.qty-new');
                        var tr = $(this).closest('tr');
                        var select = $(this).closest('tr').find('select.br-btn-product-qty');


                        if (inventoryQty === '0') {
                            // Mostrar la alerta SweetAlert si la cantidad es 0
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Ya no está disponible este producto para su venta.',
                            });
                            return; // Salir de la función para evitar la solicitud AJAX
                        }

                        $.showBigOverlay({
                            message: '{{ __('Agregando producto a tu carro de compras') }}',
                            onLoad: function() {
                                $.ajax({
                                    url: '{{ url('/cart/add_item') }}',
                                    data: {
                                        _token: $('meta[name="csrf-token"]').attr('content'),
                                        inventory_id: inventoryId,
                                        inventory_qty: inventoryQty,
                                        user_id: userId,
                                    },
                                    dataType: 'JSON',
                                    type: 'POST',
                                    success: function(data) {
                                        if (data.productCountForVendor >= 4) {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops...',
                                                text: 'No puedes agregar más de 4 productos diferentes por usuario y carrito.',
                                                footer: '<a href="#">¿Por qué existe esta limitación?</a>'
                                            });
                                            $.showBigOverlay('hide');
                                        } else {
                                            if (data.error == 0) {
                                                $('#br-cart-items').text(data.items);
                                                stock.html(data.Qty);
                                                if (data.Qty == 0) {
                                                    tr.remove();
                                                } else {
                                                    select.html('');
                                                    for (var i = 1; i < data.Qty + 1; i++) {
                                                        select.append('<option value=' + i +
                                                            '>' + i + '</option>');
                                                    }
                                                }
                                            }
                                            $.showBigOverlay('hide');
                                            document.getElementById("cartrgm").click();
                                        }
                                    },
                                    error: function(data) {
                                        $.showBigOverlay('hide');
                                    }
                                });
                            }
                        });
                    });
            })
        </script>

        <script>
            Livewire.on('accesorio', function() {


                var $inventoryAdd = $('.br-btn-product-add-accesorio');
                $(function() {
                    $('[data-toggle="popover"]').popover()
                })
                $('.nk-btn-modify-inventory').click(function() {
                        var url = $(this).data('href');
                        var tr = $(this).closest('tr');
                        var sel = $(this).closest('tr').find('select').prop('value');
                        console.log(sel);
                        //console.log(url);
                        location.href = `${url}/${sel}`;
                    }),
                    $inventoryAdd.click(function() {
                        var inventoryId = $(this).data('inventory-id');
                        var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                            .val();
                        var stock = $(this).closest('tr').find('div.qty-new');
                        var tr = $(this).closest('tr');
                        var select = $(this).closest('tr').find('select.br-btn-product-qty');

                        if (inventoryQty === '0') {
                            // Mostrar la alerta SweetAlert si la cantidad es 0
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Ya no está disponible este producto para su venta.',
                            });
                            return; // Salir de la función para evitar la solicitud AJAX
                        }

                        $.showBigOverlay({
                            message: '{{ __('Agregando producto a tu carro de compras') }}',
                            onLoad: function() {
                                $.ajax({
                                    url: '{{ url('/cart/add_item') }}',
                                    data: {
                                        _token: $('meta[name="csrf-token"]').attr('content'),
                                        inventory_id: inventoryId,
                                        inventory_qty: inventoryQty,
                                        user_id: userId,
                                    },
                                    dataType: 'JSON',
                                    type: 'POST',
                                    success: function(data) {
                                        if (data.productCountForVendor >= 4) {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops...',
                                                text: 'No puedes agregar más de 4 productos diferentes por usuario y carrito.',
                                                footer: '<a href="#">¿Por qué existe esta limitación?</a>'
                                            });
                                            $.showBigOverlay('hide');
                                        } else {
                                            if (data.error == 0) {
                                                $('#br-cart-items').text(data.items);
                                                stock.html(data.Qty);
                                                if (data.Qty == 0) {
                                                    tr.remove();
                                                } else {
                                                    select.html('');
                                                    for (var i = 1; i < data.Qty + 1; i++) {
                                                        select.append('<option value=' + i +
                                                            '>' + i + '</option>');
                                                    }
                                                }
                                            }
                                            $.showBigOverlay('hide');
                                            document.getElementById("cartrgm").click();
                                        }
                                    },
                                    error: function(data) {
                                        $.showBigOverlay('hide');
                                    }
                                });
                            }
                        });
                    });
            })
        </script>

        <script>
            Livewire.on('merchandising', function() {


                var $inventoryAdd = $('.br-btn-product-add-merchandising');
                $(function() {
                    $('[data-toggle="popover"]').popover()
                })
                $('.nk-btn-modify-inventory').click(function() {
                        var url = $(this).data('href');
                        var tr = $(this).closest('tr');
                        var sel = $(this).closest('tr').find('select').prop('value');
                        console.log(sel);
                        //console.log(url);
                        location.href = `${url}/${sel}`;
                    }),
                    $inventoryAdd.click(function() {
                        var inventoryId = $(this).data('inventory-id');
                        var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                            .val();
                        var stock = $(this).closest('tr').find('div.qty-new');
                        var tr = $(this).closest('tr');
                        var select = $(this).closest('tr').find('select.br-btn-product-qty');

                        if (inventoryQty === '0') {
                            // Mostrar la alerta SweetAlert si la cantidad es 0
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Ya no está disponible este producto para su venta.',
                            });
                            return; // Salir de la función para evitar la solicitud AJAX
                        }

                        $.showBigOverlay({
                            message: '{{ __('Agregando producto a tu carro de compras') }}',
                            onLoad: function() {
                                $.ajax({
                                    url: '{{ url('/cart/add_item') }}',
                                    data: {
                                        _token: $('meta[name="csrf-token"]').attr('content'),
                                        inventory_id: inventoryId,
                                        inventory_qty: inventoryQty,
                                        user_id: userId,
                                    },
                                    dataType: 'JSON',
                                    type: 'POST',
                                    success: function(data) {
                                        if (data.productCountForVendor >= 4) {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops...',
                                                text: 'No puedes agregar más de 4 productos diferentes por usuario y carrito.',
                                                footer: '<a href="#">¿Por qué existe esta limitación?</a>'
                                            });
                                            $.showBigOverlay('hide');
                                        } else {
                                            if (data.error == 0) {
                                                $('#br-cart-items').text(data.items);
                                                stock.html(data.Qty);
                                                if (data.Qty == 0) {
                                                    tr.remove();
                                                } else {
                                                    select.html('');
                                                    for (var i = 1; i < data.Qty + 1; i++) {
                                                        select.append('<option value=' + i +
                                                            '>' + i + '</option>');
                                                    }
                                                }
                                            }
                                            $.showBigOverlay('hide');
                                            document.getElementById("cartrgm").click();
                                        }
                                    },
                                    error: function(data) {
                                        $.showBigOverlay('hide');
                                    }
                                });
                            }
                        });
                    });
            })
        </script>

        <script>
            Livewire.on('alert', function() {
                document.getElementById("deletergm").click();
            })
        </script>




        <script>
            function setup() {
                return {
                    activeTab: 0,
                    tabs: [
                        "Nuevo",
                        "Solo",
                        "Usado",
                    ]
                };
            };
        </script>
        @stack('scripts')
    @endsection
