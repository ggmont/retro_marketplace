<div class="modal fade" id="check_promotion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <center>
                    <h5 class="modal-title retro" id="exampleModalLabel">
                        ¿Tienes un Codigo Promocional? Introducelo
                        para recibir descuentos especiales!
                    </h5>
                </center>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <br>
                <div class="form-group">
                    <label class="control-label" for="descripcion">Falla:</label>
                    <input id="descripcion" class="form-control" type="text" name="descripcion"
                        placeholder="Observacion mas comun de la maquina" required="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="nes-btn is-warning retro close-btn"
                    data-dismiss="modal">@lang('messages.closes')</button>
                <button type="button" class="nes-btn retro is-error close-modal"
                    data-dismiss="modal">@lang('messages.yes_closes')</button>
            </div>
        </div>
    </div>
</div>
