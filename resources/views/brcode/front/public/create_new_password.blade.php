@extends('brcode.front.layout.app')
@section('content-css-include')

@endsection

@section('content')
  <div class="container">
    <div class="nk-gap-4"></div>
    <div class="row">
      <div class="col-lg-6 offset-lg-3">
        <div class="nk-box-2 bg-dark-2">
            <h4><a href="/">{!! config('brcode.app_name_html') !!}</a></h4>
            <div class="nk-gap"></div>
            @if($viewData['token'])
              <p class="login-box-msg">{{ Lang::get('login.create_new_password_message') }}</p>
              <form action="{{ url('/create_new_password') }}" method="post" class="nk-form nk-form-style-1">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="_resetToken" value="{{ $viewData['token'] }}" />
                <div class="form-group has-feedback  ">
                  <input type="user_name" name="email" class="form-control" placeholder="{{ Lang::get('login.user_email') }}">
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  <span class="help-block">{{ $errors->first('email') }}</span>
                </div>
                <div class="form-group has-feedback">
                  <input type="password" name="password" class="form-control" placeholder="{{ Lang::get('login.password') }}">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  <span class="help-block">{{ $errors->first('password') }}</span>
                </div>
                <div class="form-group has-feedback">
                  <input type="password" name="password_confirmation" class="form-control" placeholder="{{ Lang::get('login.password_retype') }}">
                  <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                  <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                </div>
                <div class="form-group has-feedback">
                  @if(session('login_message') )
                    @if(  count(session('login_message') ) > 0 )
                    <p class="login-message bg-{{ session('login_message')['type'] }}">{!! session('login_message')['message'] !!}</p>
                    @endif
                  @endif
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <button type="submit" class="nk-btn nk-btn-rounded nk-btn-color-white ml-20">{{ Lang::get('login.confirm_new_password') }}</button>
                  </div>
                  <!-- /.col -->
                </div>
              </form>
            @else
              <p class="bg-danger text-center text-white">{{ Lang::get('login.invalid_new_pwd_request') }}</p>
              <a href="/" class="">{{ Lang::get('login.back_to_login_screen') }}</a><br>
            @endif
        </div>
      </div>
    </div>
  </div>
  
  @endsection

  @section('content-script-include')
  @endsection
  @section('content-script')
  
  <script>
    $(function() {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      });
    });
  </script>
  @endsection

