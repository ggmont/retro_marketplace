@extends('brcode.front.layout.app_conversation')
@section('content-css-include')
    @livewireStyles
@endsection

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div class="header-area" id="headerArea">
            <div class="container h-100 d-flex align-items-center justify-content-between">
                <!-- Back Button-->
                <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                <!-- Page Title-->
                <div class="page-heading">
                    <h6 class="mb-0 font-extrabold">Subir/Vender Producto</h6>
                </div>
                <!-- Navbar Toggler-->

                @if (Auth::user())
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @else
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @endif
            </div>
        </div>
        <div id="appCapsule">
            <div class="section mt-2">
                <h4 class="text-center">¿Cómo quieres publicar tu anuncio?</h4>
                <br>

                <h4>Oferta Rápida</h4>
                <p>
                    La forma más sencilla de subir tu producto. Pon el título, sube fotos del producto y el peso aproximado.
                </p>
                <div class="text-center">
                    <a href="{{ route('auction_normal_upload') }}" class="btn btn-danger w-100 font-bold">OFERTA RÁPIDA</a>
                </div>
                <br>
                
                <h4>Oferta Coleccionista</h4>
                <p>
                    Aumenta la probabilidad de venta subiendo tu producto en modo coleccionista.<br>
                    En este modo los productos aparecen en los filtros de búsqueda, listas de precios y otras herramientas de coleccionista.
                </p>                
                <div class="text-center">
                    <a href="{{ route('auction_upload_product') }}"
                        class="btn btn-danger w-100 font-bold">COLECCIONISTA</a>
                </div>
            </div>
        </div>
    @else
    @endif
@endsection

@section('content-script-include')
    @livewireScripts
@endsection
@section('content-script')
@endsection
