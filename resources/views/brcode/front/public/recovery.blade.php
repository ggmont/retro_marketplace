@extends('brcode.front.layout.app')
@section('content-css-include')
@endsection

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        @include('partials.flash')
        <div class="header-area" id="headerArea">
            <div class="container h-100 d-flex align-items-center justify-content-between">
                <!-- Back Button-->
                <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                <!-- Page Title-->
                <div class="page-heading">
                    <h6 class="mb-0 font-extrabold">@lang('messages.recovery_password')</h6>
                </div>
                <!-- Navbar Toggler-->

                <div class="normal">
                    @if (Auth::user())
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @else
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div id="appCapsule">
            <div class="section mt-48 text-center">
                <h4>@lang('messages.recovery_password')</h4>
                <div class="font-normal">@lang('messages.recovery_password_email')</div>
                <div class="wide-block pb-1 pt-2">
                    <div class="form-group boxed">
                        <form method="post" action="{{ route('passResetzero') }}">
                            {{ csrf_field() }}
                            <div class="input-wrapper">
                                <input type="email" value="" name="email" class="form-control"
                                    placeholder="@lang('messages.recovery_ingress_email')">
                            </div>
                            <button type="submit" class="btn btn-success mt-3 btn-lg w-100"
                                type="submit">@lang('messages.the_recovery')</button>

                        </form>
                    </div>
                    <br>
                    <!-- Redes Sociales -->
                    <span class="text-center retro">@lang('messages.follow_social')</span>
                    <div class="mt-2">
                        <a href="https://www.facebook.com/people/Retrogamingmarket/100063863901900/"
                            class="btn btn-icon btn-sm btn-facebook">
                            <ion-icon name="logo-facebook"></ion-icon>
                        </a>
                        <a href="https://twitter.com/Retrogamingmkt" class="btn btn-icon btn-sm btn-twitter">
                            <ion-icon name="logo-twitter"></ion-icon>
                        </a>
                        <a href="https://www.instagram.com/retrogamingmarket/" class="btn btn-icon btn-sm btn-instagram">
                            <ion-icon name="logo-instagram"></ion-icon>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="nk-fullscreen-block">
            <div class="nk-fullscreen-block-middle">
                <div class="container text-center">
                    <div class="row">
                        <div class="col-md-6 offset-md-3 col-lg-4 offset-lg-4">
                            <span class="text-gray-900 retro">@lang('messages.recovery_password')</span>
                            <div class="text-center text-red-700 retro">@lang('messages.recovery_password_email')</div>
                            <div class="nk-gap-2"></div>
                            <span class="text-xs retro">
                                @include('partials.flash')
                            </span>

                            <!-- START: MailChimp Signup Form -->
                            <form method="post" action="{{ route('passResetzero') }}">
                                {{ csrf_field() }}
                                <div class="nes-field is-inline">
                                    <input type="email" value="" name="email" class="nes-input"
                                        placeholder="@lang('messages.recovery_ingress_email')">
                                    <button class="nes-btn inline_field" type="submit">
                                        <span class="text-gray-900 retro">@lang('messages.the_recovery')</span>
                                    </button>
                                </div>

                            </form>
                            <br>
                            <!-- END: MailChimp Signup Form -->
                            <span class="text-center retro">@lang('messages.also_recovery')</span>
                            <br>
                            <section class="icon-list">
                                <!-- twitter -->
                                <a href="https://twitter.com/Retrogamingmkt" target="_blank" data-toggle="tooltip"
                                    title="Twitter"><i class="nes-icon twitter is-large"></i></a>
                                <!-- facebook -->
                                <a href="https://www.facebook.com/Retrogamingmarket-102746414551509" target="_blank"
                                    data-toggle="tooltip" title="Facebook"><i class="nes-icon facebook is-large"></i></a>

                                <!-- instagram -->
                                <a href="https://www.instagram.com/retrogamingmarket/" data-toggle="tooltip" target="_blank"
                                    title="instagram"><i class="nes-icon instagram is-large"></i></a>
                            </section>
                        </div>
                        <div class="nk-gap-3"></div>


                    </div>



                </div>

            </div>
        </div>
    @endif
@endsection

@section('content-script-include')
@endsection
@section('content-script')
@endsection
