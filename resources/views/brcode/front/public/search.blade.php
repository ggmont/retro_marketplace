@extends('brcode.front.layout.app')
@section('content-css-include')
    @livewireStyles

    @if (auth()->user() && (auth()->user()->frontRole || auth()->user()->adminRole))
    @endif
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    @php($nombre_cat = Request::get('cat') ? Request::get('cat') : '')
        @php($plt_cat = Request::get('plt') ? Request::get('plt') : '')
            @php($plataformas = false)
                <div class="container">
                    <ul class="nk-breadcrumbs">
                        <li><a href="/">{{ __('Inicio') }} </a></li>
                        <li><span class="fa fa-angle-right"></span></li><br><br>
                        <li>
                            <div class="section-title1">
                                <h3 class="retro" style="font-size:20px;">{{ __('Tienda') }}</h3>
                            </div>
                        </li>
                    </ul>



                </div>

                <!--Heading Banner Area Start-->
                <div class="heading-banner-area pt-30">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="heading-banner">
                                    <div class="breadcrumbs">
                                        <ul>
                                            <li><a href="index.html">Home</a><span class="breadcome-separator">></span></li>
                                            <li>Shop</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Heading Banner Area End-->
                <!--Product List Grid View Area Start-->
                <div class="mt-20 product-list-grid-view-area">
                    <div class="container bg-white">
                        <div class="row">
                            <!--Shop Product Area Start-->
                            <div class="order-1 col-lg-9 order-lg-2">
                                <div class="shop-desc-container">
                                    <div class="row">
                                        <!--Shop Product Image Start-->
                                        <div class="col-lg-12">
                                            <div class="shop-product-img mb-30 img-full">
                                                <img src="{{ asset('img/banner/retro.jpg') }}" alt="">
                                            </div>
                                        </div>
                                        <!--Shop Product Image Start-->
                                    </div>
                                </div>
                                <div class="row nk-form nk-form-style-1">
                                   
                                    <div class="col-md-12">
                                        <center>
                                      
                                            <div class="block mt-1 ml-6 rounded-md shadow-sm form-input">
                                                <select wire:model="perPage" class="text-sm text-gray-500 outline-none">
                                                    <option value="5">5 Por página</option>
                                                    <option value="10">10 Por página</option>
                                                    <option value="15">15 Por página</option>
                                                    <option value="25">25 Por página</option>
                                                    <option value="50">50 Por página</option>
                                                    <option value="10">100 Por página</option>
                                                </select>
                                            </div>
                                        </center>
                                        
                                        <input
                                            class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                            type="text" placeholder="Buscar...">
                                        <div class="input-group">
                                            <select wire:model="search_box" id="filter-box-conditio"
                                                class="retro form-control br-filtering" data-filtering="box_condition" data-width="100%"
                                                title="">
                                                <option value="">{{ __('Categoria') }}</option>
                                                <option value="">{{ __('Juegos') }}</option>
                                                <option value="">{{ __('Consolas') }}</option>
                                                <option value="">{{ __('Perifericos') }}</option>
                                                <option value="">{{ __('Merchadising') }}</option>

                                            </select>

                                            <select wire:model="search_manual" id="filter-manual-conditio"
                                                class="retro form-control br-filtering" data-filtering="manual_conditio"
                                                data-width="100%" title="{{ __('Plataforma') }}">
                                                <option value="">{{ __('Plataforma') }}</option>

                                            </select>


                                        </div>



                                    </div>
                                    
                                </div>


                                <div class="flex items-center justify-between px-4 py-3 bg-white border-t border-gray-200 sm:px-6">
                                   
                                    <div class="shop-tab">
                                        <ul class="nav">
                                            <li><a class="active" data-toggle="tab" href="#grid-view"><i
                                                        class="ion-android-apps"></i></a></li>
                                            <li><a data-toggle="tab" href="#list-view"><i class="ion-navicon-round"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="block mt-1 ml-6 rounded-md shadow-sm form-input">

                                    </div>
                                    <div class="float-right">
                                        <button type="button" class="nes-btn is-warning"><span
                                                class="text-right retro font-weight-bold color-primary small text-nowrap">¿Como Comprar
                                                en RGM?</span></button>
                                    </div>
                                </div>

                         
                                <!--Shop Tab Menu End-->
                                <!--Shop Product Area Start-->
                                <div class="shop-product-area">
                                    <div class="tab-content">
                                        <!--Grid View Start-->
                                        <div id="grid-view" class="tab-pane fade show active">
                                            <div class="row product-container">
                                                @isset($inventory_latest_users)

                                                    @foreach ($inventory_latest_users as $i)
                                                        <!--Single Product Start-->
                                                        <div class="col-lg-3 col-md-3 item-col2">
                                                            <div class="single-product">
                                                                <div class="product-img">
                                                                    <a href="single-product.html">
                                                                        <img class="first-img"
                                                                            src="{{ asset('assets/images/art-not-found.jpg') }}" alt="">
                                                                        <img class="hover-img"
                                                                            src="{{ asset('assets/images/art-not-found.jpg') }}" alt="">
                                                                    </a>
                                                                    <span class="sicker">-12%</span>
                                                                    <ul class="product-action">
                                                                        <li><a href="#" data-toggle="tooltip" title="Add to Wishlist"><i
                                                                                    class="ion-android-favorite-outline"></i></a></li>
                                                                        <li><a href="#" data-toggle="tooltip" title="Compare"><i
                                                                                    class="ion-ios-shuffle-strong"></i></a></li>
                                                                        <li><a href="#" data-toggle="modal" title="Quick View"
                                                                                data-target="#myModal"><i
                                                                                    class="ion-android-expand"></i></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="product-content">
                                                                    <h2 class="text-green-900"><a href="single-product.html">Auctor
                                                                            furniture</a></h2>
                                                                    <div class="rating">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </div>
                                                                    <div class="product-price">
                                                                        <span class="new-price">$75.00</span>
                                                                        <a class="button add-btn" href="#" data-toggle="tooltip"
                                                                            title="Add to Cart">add to cart</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endisset

                                                <!--Single Product End-->
                                            </div>


                                        </div>
                                        <!--Grid View End-->
                                        <!--List View Start-->
                                        <div id="list-view" class="tab-pane fade">
                                            <div class="pt-10 row all-prodict-item-list">



                                                <!--Single Product List Start-->
                                                <div class="col-lg-12">
                                                    <div class="single-item">
                                                        <div class="row product-img img-full">
                                                            <div class="col-lg-4 col-md-4">
                                                                <a href="single-product.html">
                                                                    <img class="first-img"
                                                                        src="{{ asset('assets/images/art-not-found.jpg') }}" alt="">
                                                                    <img class="hover-img"
                                                                        src="{{ asset('assets/images/art-not-found.jpg') }}" alt="">
                                                                </a>
                                                                <span class="sicker">-7%</span>
                                                            </div>
                                                            <div class="col-lg-8 col-md-8">
                                                                <div class="product-content-2">
                                                                    <h2><a href="single-product.html">Auctor furniture</a></h2>
                                                                    <div class="rating">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </div>
                                                                    <div class="mb-20 product-price">
                                                                        <span class="old-price">$79.00</span>
                                                                        <span class="new-price">$69.00</span>
                                                                    </div>
                                                                    <div class="product-discription">
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                                            Fusce posuere metus vitae arcu imperdiet, id aliquet ante
                                                                            scelerisque. Sed sit amet sem vitae urna fringilla tempus.
                                                                        </p>
                                                                    </div>
                                                                    <div class="pro-action-2">
                                                                        <ul class="product-cart-area-list">
                                                                            <li><a class="action-btn big" href="#" data-toggle="tooltip"
                                                                                    title="Add to Cart">Add to cart</a></li>
                                                                            <li><a class="action-btn small" href="#"
                                                                                    data-toggle="tooltip" title="Add to Wishlist"><i
                                                                                        class="ion-android-favorite-outline"></i></a>
                                                                            </li>
                                                                            <li><a class="action-btn small" href="#" data-toggle="modal"
                                                                                    title="Quick View" data-target="#myModal"><i
                                                                                        class="ion-android-expand"></i></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <!--Single Product List End-->
                                            </div>
                                        </div>
                                        <!--List View End-->
                                    </div>
                                </div>
                                <!--Shop Product Area End-->
                                <!--Pagination Start-->
                                <div class="pb-10 pagination">
                                    <ul class="page-number">
                                        <li class="active"><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                                    </ul>
                                </div>
                                <!--Pagination End-->
                            </div>
                            <!--Shop Product Area End-->
                            <!--Left Sidebar Start-->
                            <div class="order-2 col-lg-3 order-lg-1">
                                <!--Widget Shop Categories start-->
                                <div class="widget widget-shop-categories">
                                    <h3 class="widget-shop-title">Shop By Categories</h3>
                                    <div class="widget-content">
                                        <ul class="product-categories">
                                            <li><a href="#">Contruction</a></li>
                                            <li><a href="#">Electronics</a></li>
                                            <li><a href="#">Health & Beauty</a></li>
                                            <li><a href="#">Mobile & Laptop</a></li>
                                            <li><a href="#">Organic</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!--Widget Shop Categories End-->
                                <!--Widget Price Slider Start-->
                                <div class="widget widget-price-slider">
                                    <h3 class="widget-title">Filter by price</h3>
                                    <div class="widget-content">
                                        <div class="price-filter">
                                            <form action="#">
                                                <div id="slider-range"></div>
                                                <span>Price:<input id="amount" class="amount" readonly="" type="text"></span>
                                                <input class="price-button" value="Filter" type="button">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!--Widget Price Slider End-->
                                <!--Widget Brand Start-->
                                <div class="widget widget-brand">
                                    <h3 class="widget-title">Brands</h3>
                                    <div class="widget-content">
                                        <ul class="brand-menu">
                                            <li><input type="checkbox"><a href="#">Brand2</a> <span class="pull-right">(1)</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <!--Widget Brand End-->
                                <!--Widget Manufacture Start-->
                                <div class="widget widget-manufacture">
                                    <h3 class="widget-title">MANUFACTURER</h3>
                                    <div class="widget-content">
                                        <ul class="brand-menu">
                                            <li><input type="checkbox"><a href="#">Pellentesque</a> <span class="pull-right">(1)</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--Widget Manufacture End-->
                                <!--Widget Color Start-->
                                <div class="widget widget-color">
                                    <h3 class="widget-title">Color</h3>
                                    <div class="widget-content">
                                        <ul class="brand-menu">
                                            <li><input type="checkbox"><a href="#">Gold</a> <span class="pull-right">(1)</span></li>
                                            <li><input type="checkbox"><a href="#">Green</a> <span class="pull-right">(1)</span></li>
                                            <li><input type="checkbox"><a href="#">White</a> <span class="pull-right">(1)</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <!--Widget Color End-->
                                <!--Widget Compare Start-->
                                <div class="widget widget-compare">
                                    <h3 class="widget-compare-title">Compare</h3>
                                    <div class="widget-content">
                                        <ul class="compare-menu">
                                            <li>
                                                <a class="title" href="#">Cillum dolore</a>
                                                <a class="pull-right" href="#"><i class="fa fa-times"></i></a>
                                            </li>
                                            <li>
                                                <a class="title" href="#">Cillum dolore</a>
                                                <a class="pull-right" href="#"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                        <a class="clear-all" href="#">Clear all</a>
                                        <a class="compare-btn" href="#">compare</a>
                                    </div>
                                </div>
                                <!--Widget Compare End-->
                                <!--Widget Tag Start-->
                                <div class="widget widget-tag">
                                    <h3 class="widget-shop-tag-title">Popular Tags</h3>
                                    <ul>
                                        <li><a href="#">asian</a></li>
                                        <li><a href="#">brown</a></li>
                                        <li><a href="#">camera</a></li>
                                        <li><a href="#">chilled</a></li>
                                        <li><a href="#">coctail</a></li>
                                        <li><a href="#">cool</a></li>
                                        <li><a href="#">dark</a></li>
                                        <li><a href="#">euro</a></li>
                                        <li><a href="#">food</a></li>
                                        <li><a href="#">france</a></li>
                                        <li><a href="#">hardware</a></li>
                                        <li><a href="#">hat</a></li>
                                        <li><a href="#">hipster</a></li>
                                        <li><a href="#">holidays</a></li>
                                        <li><a href="#">light</a></li>
                                        <li><a href="#">mac</a></li>
                                        <li><a href="#">place</a></li>
                                        <li><a href="#">retro</a></li>
                                        <li><a href="#">t-shirt</a></li>
                                        <li><a href="#">teen</a></li>
                                        <li><a href="#">travel</a></li>
                                        <li><a href="#">video-2</a></li>
                                        <li><a href="#">watch</a></li>
                                        <li><a href="#">white</a></li>
                                    </ul>
                                </div>
                                <!--Widget Tag End-->
                            </div>
                            <!--Left Sidebar End-->
                        </div>
                    </div>
                </div>
                <!--Product List Grid View Area End-->
                <!--Brand Area Start-->

                <!--Brand Area End-->

            @endsection

            @section('content-script-include-ang')
                <script src="{{ asset('brcode/js/app.public.angjs-search.js') }}"></script>
            @endsection

            @section('content-script')
                @livewireScripts
                <script>
                    var categories = {!! json_encode($viewData['category_ids']) !!};
                    var searchText = '{!! $viewData['search_text'] !!}';
                </script>
                <script>
                    $(document).ready(function() {
                        $('.pager').removeAttr('style');
                        $('.pagination-first.ng-scope.disabled a').html(
                            '<i class="fa fa-angle-double-left" aria-hidden="true"></i>');
                        $('.pagination-last.ng-scope.disabled a').html(
                            '<i class="fa fa-angle-double-right" aria-hidden="true"></i>');
                        $('.pagination-first.ng-scope a').html('<i class="fa fa-angle-double-left" aria-hidden="true"></i>');
                        $('.pagination-last.ng-scope a').html('<i class="fa fa-angle-double-right" aria-hidden="true"></i>');
                        $('.pagination-first.ng-scope a').html('<i class="fa fa-angle-double-left" aria-hidden="true"></i>');
                        $('.pagination-last.ng-scope a').html('<i class="fa fa-angle-double-right" aria-hidden="true"></i>');
                        $('.pagination-prev.ng-scope a').html('<i class="fa fa-angle-left" aria-hidden="true"></i>');
                        $('.pagination-next.ng-scope a').html('<i class="fa fa-angle-right" aria-hidden="true"></i>');
                    });
                </script>
                <script>
                    var sList = "";

                    $(document).ready(function() {
                        $('input[type=checkbox]').each(function() {

                            if (this.checked) {
                                console.log('#accordion-' + $(this).data('type'));
                                console.log($('a[href$="' + '#accordion-' + $(this).data('type') + '"]'));
                                $('a[href$="' + '#accordion-' + $(this).data('type') + '"]').click();

                                //$(x)
                            }
                            //sList += "(" + $(this).val() + "-" + (this.checked ? "checked" : "not checked") + ")";

                        });
                    });
                    //console.log (sList);
                </script>
                @if (auth()->user() && (auth()->user()->frontRole || auth()->user()->adminRole))
                    <!-- jQuery UI 1.11.4 -->
                    <script src="{{ asset('assets/jQueryUI/jquery-ui.min.js') }}"></script>
                    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
                    <script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
                    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
                    <script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
                    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
                    <script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
                    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
                    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
                    <!-- The basic File Upload plugin -->
                    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
                    <!-- The File Upload processing plugin -->
                    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
                    <!-- The File Upload image preview & resize plugin -->
                    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>
                    <script>
                        var global_lang = [];
                        global_lang['app.uploader_file_size_bigger'] = 'file bigger';
                        global_lang['app.uploader_file_ext_not_supported'] = 'ext not supported';
                    </script>
                    <script>
                        function cargarImagen(x) {

                            $(".file-uploader-preview").remove();
                            $('.progress-bar').css('width', '0%').attr('aria-valuenow', 0);
                            console.log("{{ url('/11w5Cj9WDAjnzlg0/marketplace/products/images') }}" + '/' + $(x).data('id'));
                            $.get("{{ url('/11w5Cj9WDAjnzlg0/marketplace/products/images') }}" + '/' + $(x).data('id'), function(data,
                                status) {

                                console.log("Datas: " + data.quant + "\nStatus: " + status);
                                if (data.quant > 0) {
                                    $.each(data.images, function(k, v) {
                                        console.log(v);
 

                                    });
                                }

                            });

                            $('.modal-title').html($(x).data('name'));
                            $('#model_id').val($(x).data('id'));
                            $('#imageModal').modal('show');
                        }

                        var frm = $('#form-image');
                        frm.submit(function(ev) {
                            $.ajax({
                                type: frm.attr('method'),
                                url: frm.attr('action'),
                                data: frm.serialize(),
                                success: function(data) {
                                    $('#imageModal').modal('toggle');
                                    var buttons = [{
                                            addClass: 'btn btn-primary btn-sm pull-left',
                                            text: '{{ Lang::get('app.delete') }}',
                                            onClick: function($noty) {
                                                $noty.close();
                                                deleteRecord()
                                            }
                                        },
                                        {
                                            addClass: 'btn btn-danger btn-sm pull-right',
                                            text: '{{ Lang::get('app.cancel') }}',
                                            onClick: function($noty) {
                                                $noty.close();
                                                $('#br-btn-delete').button('reset');
                                            }
                                        },
                                    ];

                                    presentNotyMessage('{{ Lang::get('app.image_change') }}', 'success', 1);
                                }
                            });

                            ev.preventDefault();
                        });

                        $('#fileupload').fileupload({
                            singleFileUploads: true,
                            sequentialUploads: true,
                            limitConcurrentUploads: 1,
                            dropZone: $('#dropzone'),
                            add: function(e, data) {
                                data.context = $('<div/>').addClass('file-uploader-preview').appendTo('#dropzone');
                                $.each(data.files, function(index, file) {
                                    if (file.size > 3072000) {
                                        var msg = $('<p/>')
                                            .text('{{ Lang::get('app.uploader_file_size_bigger') }}'.replace(
                                                '_FILENAME_', file.name));
                                        msg.appendTo($('#file-uploader-messages'));
                                        data.context.remove();
                                        return false;
                                    } else if ($.inArray(file.name.split('.').pop().toLowerCase(), ['gif', 'png', 'jpg',
                                            'jpeg'
                                        ]) == -1) {
                                        var msg = $('<p/>')
                                            .text('{{ Lang::get('app.uploader_file_ext_not_supported') }}'.replace(
                                                '_FILENAME_', file.name));
                                        msg.appendTo($('#file-uploader-messages'));
                                        data.context.remove();
                                        return false;
                                    }
                                    var img = $('<img/>').attr('src', URL.createObjectURL(file));
                                    data.imgEl = img;
                                    data.inputName = $('#dropzone').find('#fileupload-name').val();
                                    var node = $('<p/>')
                                        .append(img);
                                    node.appendTo(data.context);

                                });
                                data.submit();
                            },
                            done: function(e, data) {
                                var img = data.imgEl;
                                var parent = img.closest('.file-uploader-preview');
                                var removeButton = $('<div/>').html('<i class="fa fa-times-circle-o"></i>').addClass(
                                    'file-uploader-remove').appendTo(parent);
                                var input = $('<input/>').attr('type', 'hidden').attr('name', data.inputName).attr('value', data
                                    .result.name).appendTo(parent);

                            },
                            progress: function(e, data) {
                                var progress = parseInt(data.loaded / data.total * 100, 10);
                                $('#progress .progress-bar').css(
                                    'width',
                                    progress + '%'
                                );
                            },
                        });

                        $(document).on('click', '.file-uploader-remove', function() {
                            var parent = $(this).closest('.file-uploader-preview');
                            var image = parent.find('input[type=hidden]').val();
                            $.ajax({
                                url: '{{ url('/11w5Cj9WDAjnzlg0/marketplace/products/remove_file') }}',
                                data: {
                                    '_token': $('form input[name="_token"]:eq(0)').val(),
                                    img: image
                                },
                                type: 'POST',
                                success: function(data) {
                                    parent.hide('fade', 250, function() {
                                        $(this).remove()
                                    });
                                },
                                error: function(data) {

                                }
                            })
                        });

                        $('#dropzone').on('dragenter dragover', function() {
                            $(this).addClass('hover');
                        });

                        $('#dropzone').on('dragleave dragexit drop', function() {
                            $(this).removeClass('hover');
                        });
                    </script>
                @endif
                <script>
                    var select2 = $('.select2').select2({
                        width: "100%",
                        theme: "material"
                    });
                </script>
                @if ($plt_cat != '')
                    <script>
                        $(document).ready(function() {
                            $("#br-search-platform-sort").val('{{ $plt_cat }}');
                            $("#br-search-platform-sort").trigger("change");
                        });
                    </script>
                @endif
            @endsection
