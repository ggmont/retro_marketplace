@extends('brcode.front.layout.app_conversation')
@section('content-css-include')
    <link href="/assets/dropzone-master/dist/min/dropzone.min.css" rel="stylesheet" type="text/css" />
    <style>
        .dropzone .dz-preview .dz-error-message {
            top: 175px !important;
        }
    </style>


    @livewireStyles
@endsection

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div class="header-area" id="headerArea">
            <div class="container h-100 d-flex align-items-center justify-content-between">
                <!-- Back Button-->
                <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                <!-- Page Title-->
                <div class="page-heading">
                    <h6 class="mb-0 font-extrabold">Subir/Vender Producto</h6>
                </div>
                <!-- Navbar Toggler-->

                @if (Auth::user())
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @else
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @endif
            </div>
        </div>
        <div id="appCapsule">
            <div class="section mt-2">

                <div class="container">
                    <div class="section-heading mt-3">
                        <center>
                            <h4 class="mb-1 font-bold">Detalles del inventario</h4>
                        </center>
                    </div>
                    <!-- Contact Form-->
                    <div class="section mt-2 mb-5">

                        <form class="w-full" action="{{ route('auction_normal_upload_full') }}" method="POST"
                            enctype="multipart/form-data">
                            {{ method_field('POST') }}
                            {{ csrf_field() }}


                            <div class="form-group boxed">

                                <label class="form-label">Agrega imagenes de tu producto </label>

                                <section style="display:none" id="fileupload-files" required data-name="image_path[]">
                                </section>
                                <section id="dropzone">
                                    <section class="dz-message text-dark" data-dz-message>
                                        <span class="retro">@lang('messages.image_sell_mobile')</span>
                                    </section>
                                </section>

                            </div>

                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label">Título</label>
                                    <input type="text" placeholder="Titulo del artículo" id="name_field" name="title"
                                        required class="form-control">
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label" for="city5">Categoría</label>
                                    <select name="category" required class="form-control form-select" id="default_select">
                                        <option data-html="Juegos" value="1">Juegos</option>
                                        <option data-html="Consolas" value="2">Consolas</option>
                                        <option data-html="Periféricos" value="3">Periféricos</option>
                                        <option data-html="Accesorios" value="4">Accesorios</option>
                                        <option data-html="Merchadising" value="172">Merchadising</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label" for="platformSelect">Plataforma (Opcional)</label>
                                    <select name="platform" class="form-control form-select" id="platformSelect">
                                        <option value="" selected>Selecciona</option>
                                        @foreach ($platform as $p)
                                            <option value="{{ $p->value }}">{{ $p->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label" for="regionSelect">Región (Opcional)</label>
                                    <select name="region" class="form-control form-select" id="regionSelect">
                                        <option value="" selected>Selecciona</option>
                                        @foreach ($region as $r)
                                            <option value="{{ $r->value_id }}">{{ $r->value_id }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <input type="hidden" name="quantity" value="1">
                            <input type="hidden" name="max_bid" value="1">

                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label" for="city5">¿Como es tu producto?</label>
                                    <select name="size" required class="form-control form-select" id="default_select">
                                        <option data-html="Pequeño (Juegos, cartuchos, mandos...)" value="0">Pequeño
                                            (Juegos, cartuchos, mandos...)</option>
                                        <option data-html="Mediano - (consolas, lote de juegos) " value="1">Mediano -
                                            (consolas, lote de juegos) </option>
                                        <option data-html="Grande - (consolas en caja, lote grande de juegos)"
                                            value="2">Grande - (consolas en caja, lote grande de juegos)</option>
                                        <option data-html="XXL - (Televisores, colecciones completas)" value="3">XXL -
                                            (Televisores, colecciones completas)</option>
                                        <option data-html="Super XXL (Maquinas arcade, expositores, muebles)"
                                            value="4">
                                            Super XXL (Maquinas arcade, expositores, muebles)</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label" for="city5">¿Cuanto pesa?</label>
                                    <select name="kg" required class="form-control form-select" id="default_select">
                                        <option data-html="0 a 1 Kg" value="250">0 a 1 Kg</option>
                                        <option data-html="1 a 3 Kg" value="1100">1 a 3 Kg</option>
                                        <option data-html="3 a 5 Kg" value="3100">3 a 5 Kg</option>
                                        <option data-html="5 a 10 Kg" value="5100">5 a 10 Kg</option>
                                        <option data-html="10 a 20 Kg" value="10100">10 a 20 Kg</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label" for="city5"> {{ __('Descripción') }}</label>
                                    <textarea id="comments" required name="comments" rows="4" class="form-control" placeholder="Comentario"></textarea>
                                    <div class="textarea-counter" data-text-max="300"
                                        data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></div>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <section class="text-center form-group col-md-12">
                                <br>
                                <input type="submit" class="btn btn-submit btn-danger w-100 text-lg font-extrabold"
                                    value="{{ __('Subir') }}">
                            </section>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    @else
        @include('partials.flash')


        <br>
        <div class="top-products-area clearfix py-5">

            <div class="relative py-10 pb-20">
                <div class="absolute inset-0"
                    style="background-image: url('https://www.retrogamingmarket.eu/landing/assets/images/wp-20151115-4-1000x561.jpg'); filter: blur(10px);">
                </div>
                <div class="container mx-auto text-center text-white relative z-10">
                    <h1 class="text-4xl font-bold mb-2"
                        style="text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5); color: white;">
                        SUBIR/VENDER PRODUCTO</h1>


                    <br>

                    <h5 class="font-bold mb-2" style="text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5); color: white;">
                        DETALLES DEL INVENTARIO</h5>

                    <button type="button" data-toggle="modal" data-target="#informationModal"
                        class="bg-red-500 text-white px-4 py-2 rounded-full hover:bg-red-600 ml-2">
                        <span class="whitespace-nowrap">MAS INFORMACIÓN</span>
                    </button>
                </div>
            </div>

            <br><br>
            <div class="container bg-white border border-black rounded-lg">
                <!-- Contact Form-->
                <div class="section mt-5 mb-5">

                    <form class="w-full" action="{{ route('auction_normal_upload_full') }}" method="POST"
                        enctype="multipart/form-data">
                        {{ method_field('POST') }}
                        {{ csrf_field() }}


                        <div class="form-group boxed">

                            <label class="form-label">Agrega imagenes de tu producto </label>

                            <section style="display:none" id="fileupload-files" required data-name="image_path[]">
                            </section>
                            <section id="dropzone">
                                <section class="dz-message text-dark" data-dz-message>
                                    <span class="retro">@lang('messages.image_sell_mobile')</span>
                                </section>
                            </section>

                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label">Título</label>
                                <input type="text" placeholder="Titulo del artículo" id="name_field" name="title"
                                    required class="form-control">
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5">Categoría</label>
                                <select name="category" required class="form-control form-select" id="default_select">
                                    <option data-html="Juegos" value="1">Juegos</option>
                                    <option data-html="Consolas" value="2">Consolas</option>
                                    <option data-html="Periféricos" value="3">Periféricos</option>
                                    <option data-html="Accesorios" value="4">Accesorios</option>
                                    <option data-html="Merchadising" value="172">Merchadising</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label">Cantidad</label>
                                <input type="number" placeholder="Cantidad de artículos" id="name_field"
                                    name="quantity" min="1" required class="form-control">
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5">¿Cómo es tu producto?</label>
                                <select name="size" required class="form-control form-select" id="default_select">
                                    <option data-html="Pequeño (Juegos, cartuchos, mandos...)" value="0">Pequeño
                                        (Juegos, cartuchos, mandos...)</option>
                                    <option data-html="Mediano - (consolas, lote de juegos) " value="1">Mediano -
                                        (consolas, lote de juegos) </option>
                                    <option data-html="Grande - (consolas en caja, lote grande de juegos)" value="2">
                                        Grande - (consolas en caja, lote grande de juegos)</option>
                                    <option data-html="XXL - (Televisores, colecciones completas)" value="3">XXL -
                                        (Televisores, colecciones completas)</option>
                                    <option data-html="Super XXL (Maquinas arcade, expositores, muebles)" value="4">
                                        Super XXL (Maquinas arcade, expositores, muebles)</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5">¿Cuánto pesa?</label>
                                <select name="kg" required class="form-control form-select" id="default_select">
                                    <option data-html="0 a 1 Kg" value="250">0 a 1 Kg</option>
                                    <option data-html="1 a 3 Kg" value="1100">1 a 3 Kg</option>
                                    <option data-html="3 a 5 Kg" value="3100">3 a 5 Kg</option>
                                    <option data-html="5 a 10 Kg" value="5100">5 a 10 Kg</option>
                                    <option data-html="10 a 20 Kg" value="10100">10 a 20 Kg</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="city5"> {{ __('Descripción') }}</label>
                                <textarea id="comments" required name="comments" rows="4" class="form-control" placeholder="Comentario"></textarea>
                                <div class="textarea-counter" data-text-max="300"
                                    data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></div>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <section class="text-center form-group col-md-12">
                            <br>
                            <input type="submit" class="btn btn-submit btn-danger w-100 text-lg font-extrabold"
                                value="{{ __('Subir') }}">
                        </section>

                    </form>
                </div>
            </div>
        </div>
        <!-- Internet Connection Status-->
        <div class="internet-connection-status" id="internetStatus"></div>
    @endif
@endsection

@section('content-script-include')
    @livewireScripts


    <script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>

    <script src="{{ asset('assets/dropzone-master/dist/min/dropzone.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/quagga/0.12.1/quagga.js"></script>
    <script src="{{ asset('vendor/sweetalert2/sweetalert2.all.min.js') }}"></script>
    <script>
        var global_lang = [];
        global_lang['app.uploader_file_size_bigger'] = 'file bigger';
        global_lang['app.uploader_file_ext_not_supported'] = 'ext not supported';
    </script>

    <script>
        $(document).ready(function() {
            // obtener el formulario
            const form = $('form');

            // agregar un manejador de eventos para el envío del formulario
            form.on('submit', function(e) {
                // deshabilitar el botón después de hacer clic
                $('input[type="submit"]').prop('disabled', true);
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $('#platformSelect').select2(); // Inicializa Select2 en el campo de plataforma
            $('#regionSelect').select2(); // Inicializa Select2 en el campo de región
        });
    </script>

    <script>
        $(document).ready(function() {
            // obtener campos de cantidad y precio
            const quantityInput = $('input[name="quantity"]');
            const priceInput = $('input[name="max_bid"]');

            // obtener botón de enviar
            const submitButton = $('input[type="submit"]');

            // función para habilitar o deshabilitar botón de enviar
            function enableDisableSubmitButton() {
                if (quantityInput.val() > 0 && priceInput.val() > 0) {
                    submitButton.prop('disabled', false);
                } else {
                    submitButton.prop('disabled', true);
                }
            }

            // llamar a la función al cargar la página
            enableDisableSubmitButton();

            // agregar eventos para verificar cambios en campos de cantidad y precio
            quantityInput.on('change', function() {
                enableDisableSubmitButton();
            });

            priceInput.on('change', function() {
                enableDisableSubmitButton();
            });
        });
    </script>


    <!-- Juego -->
    <script>
        var acceptedFileTypes = "image/*";
        var fileList = new Array;
        var i = 0;
        var myDropzone = $("#dropzone").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('/account/inventory/upload_file_request') }}",
            addRemoveLinks: true,
            maxFiles: 8,
            maxfilesexceeded: function(file) {
                Swal.fire({
                    title: '<span class="retro">Error</span>',
                    text: 'Haz Alcanzado el maximo de Imagenes (Max 8)',
                    footer: '<a class="retro" href="/site/guia-de-venta" target="_blank">¿Como Vender En RGM?</a>',
                })
                this.removeFile(file);
            },
            acceptedFiles: ".jpg,.jpeg,.png",
            dictInvalidFileType: "No puedes subir archivos de este tipo , solo Imagenes tipo PNG O JPG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Drop files here to upload",
            init: function() {
                myDropzone = this;
                $(this.element).addClass("dropzone");
                this.on("success", function(file, serverFileName) {
                    fileList[i] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": i
                    };
                    input = $('#fileupload-files').data('name');
                    $('#fileupload-files').append(
                        `<input type="hidden" id="file_${ i }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    i += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileList.length; f++) {
                        if (fileList[f].fileName == file.name) {
                            rmvFile = fileList[f].serverFileName;
                            $("#file_" + fileList[f].fileId).remove();
                        }
                    }
                });
            }
        });


        j = 0;
        var fileListEan = new Array;
        var myDropzone = $("#dropzone-ean").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('/account/inventory/upload_file_ean') }}",
            addRemoveLinks: true,
            maxFiles: 8, //change limit as per your requirements
            dictMaxFilesExceeded: "Maximum upload limit reached",
            acceptedFiles: "image/*",
            dictInvalidFileType: "upload only JPG/PNG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Drop files here to upload",
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileListEan[j] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": j
                    };
                    input = $('#fileupload-files-ean').data('name');
                    $('#fileupload-files-ean').append(
                        `<input type="hidden" id="file_${ j }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    j += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileListEan.length; f++) {
                        if (fileListEan[f].fileName == file.name) {
                            rmvFile = fileListEan[f].serverFileName;
                            $("#file_" + fileListEan[f].fileId).remove();
                        }
                    }
                });
                //here

            }
        });
    </script>
@endsection
@section('content-script')
@endsection
