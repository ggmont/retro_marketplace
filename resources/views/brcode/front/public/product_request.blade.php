@extends('brcode.front.layout.app')
@section('content-css-include')
    <link rel="stylesheet" href="{{ asset('plugins/fileinput/css/fileinput.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/jquery-ui.css') }}">

    <style>
        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Jost', sans-serif;
            font-weight: 800;
        }

        .select2-dropdown {
            background-color: white;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-sizing: border-box;
            display: block;
            position: absolute;
            left: -100000px;
            width: 100%;
            z-index: 1051;
        }

    </style>




@endsection

@section('content')
    <div class="nk-main">
        <!-- START: Breadcrumbs -->
        <div class="nk-gap-1"></div>
        <div class="container">
            <ul class="nk-breadcrumbs">
                <li>

                    <div class="section-title1">
                        <h3 class="retro">@lang('messages.up_sell')</h3>
                    </div>

                </li>
            </ul>
            <div class="nk-gap-1"></div>
            <div class="row">
                <div class="col-md-12" style="background-color: #ffffff; border: solid 1px #db2e2e">
                    <center>
                        <span class="retro">@include('partials.flash')</span>
                        <br>
                        <h4 class="block mb-2 text-lg font-bold tracking-wide text-gray-700 uppercase retro">
                            <center><i class="nes-icon coin is-small"></i> - @lang('messages.up_product') - <i
                                    class="nes-icon coin is-small"></i></center>
                        </h4>
                        <br>
                        <h5 class="block mb-2 font-bold tracking-wide text-base text-gray-900 uppercase retro" for="grid-city">
                            @lang('messages.rellen_form_alternative') <br>
                            @lang('messages.rellen_form_alternative_two')
                            <br> @lang('messages.rellen_form_two') <br>

                            <br>
                            @lang('messages.our_faq') <a href="/site/faqs" target="_blank">
                                <font color="red">FAQ</font>
                            </a>
                        </h5>
                        <br>
                    </center>

                    <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro">
                        <center>@lang('messages.select_sell')</center>
                    </h5>
                    <br>
                    <div class="retro nes-select">
                        <select required id="type">
                            <option value="" disabled selected hidden>@lang('messages.category_two')...</option>
                            <option value="Juegos">@lang('messages.games')</option>
                            <option value="Consolas">@lang('messages.consoles')</option>
                            <option value="Perifericos">@lang('messages.peripherals')</option>
                            <option value="Accesorios">@lang('messages.accesories')</option>
                            <option value="Merchandising">Merchandising</option>
                        </select>
                    </div>
                    <br>
                    <div id="target">
                        <div id="Juegos">
                            <form class="w-full" action="{{ route('send_product_request_exist') }}" method="POST"
                                enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                                <input name="category" type="hidden" value="Juegos">
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro">
                                    <center>Detalles Principales</center>
                                </h5>
                                <center>
                                    <small class="retro">Tienes dudas con las regiones? Recuerda - <svg
                                            width="20px" height="76px" data-html="true" data-toggle="popover" data-content="
                                           - Si la caja está en diferentes idiomas - PAL-EU <br />
                                           - Si la caja está solo en Español - PAL-ESP <br />
                                           - Si la caja está en Alemán - PAL-DE <br />
                                           - Si la caja está en Italiano - PAL-ITA <br />
                                           - Si la caja está en Francés - PAL-FRA <br />
                                           - Si el juego está en inglés y tiene etiqueta con una M en el frontal - es NTSC-U <br />
                                           - Si el juego tiene una etiqueta en el frontal marcada con USK es PAL-UK<br />
                                           - Si la portada e información del juego está en japonés es NTSC-J"
                                            data-placement="top" data-trigger="hover" viewBox="0 0 76 76"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path id="greyshadow" d="M8 8h68v68h-68z" fill="#BFBFBF" />
                                            <path id="blackborder" d="M4 4h68v68h-68z" fill="#000" />
                                            <path id="background" d="M4 4h64v64h-64z" fill="#FFC07C" />
                                            <path id="borderlefttop" d="M4 0h64M0 4v64" stroke="#DE5917" stroke-width="8" />
                                            <path id="rivets" d="M8 8h4v4h-4zM60 60h4v4h-4zM8 60h4v4h-4zM60 8h4v4h-4z"
                                                fill="#000" />
                                            <path id="questionshadow"
                                                d="M24 20h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM36 52h8v8h-8z"
                                                fill="#000" />
                                            <path id="question"
                                                d="M20 16h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM32 48h8v8h-8z"
                                                fill="#DE5917" />
                                        </svg>
                                    </small>
                                </center>
                                <section class="col-md-12">

                                    <select name="product" id='selProduct' style='width: 200px;'>
                                        <option value='0'>-- @lang('messages.search_product') --
                                        </option>
                                    </select>

                                </section>
                                <br>
                                <center>
                                    <a href="{{ route('product_not_found') }}" class="nes-btn is-warning text-base retro">@lang('messages.not_found_your_product')</a>
                                </center>
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro">
                                    <center>@lang('messages.detail_product_sell')</center>
                                </h5>
                                <br>
                                <section class="flex flex-wrap mb-2 -mx-3">

                                    <section class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            {{ __('Cantidad') }}
                                        </label>
                                        <input type="number" name="quantity" class="nes-input" min="1" value="1" />
                                    </section>
                                    <section class="w-full px-3 md:w-1/2">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="grid-last-name">
                                            {{ __('Precio') }}
                                        </label>
                                        <section class="nes-field">
                                            <input type="text" name="price" class="text-right nes-input" step="0.01"
                                                placeholder="0.00" />
                                        </section>
                                    </section>
                                </section>
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro"
                                    for="grid-city">
                                    @lang('messages.conditions_product')
                                </h5>
                                <br>
                                <section class="flex flex-wrap mb-2 -mx-3">
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            @lang('messages.box')
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="box_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>

                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>

                                                </select>

                                            </section>

                                        </section>
                                    </section>
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            @lang('messages.games')
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="game_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>
                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                                </select>

                                            </section>

                                        </section>
                                    </section>
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            Manual
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="manual_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>
                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                                </select>
                                            </section>

                                        </section>

                                    </section>

                                </section>
                                <br>
                                <section class="flex flex-wrap mb-2 -mx-3">

                                    <section class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            @lang('messages.cover')
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="cover_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>
                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                                </select>
                                            </section>

                                        </section>
                                    </section>
                                    <section class="w-full px-3 md:w-1/2">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            Extras
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="extra_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>
                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                                </select>
                                            </section>

                                        </section>
                                    </section>


                                </section>

                                <label for="input-res-1"
                                    class="text-xs font-bold retro">@lang('messages.add_new_image_obligated')
                                    <font color="red">@lang('messages.max_image')</font>
                                </label>

                                <section class="file-loading">
                                    <input id="input-20" name="image_path[]" type="file" data-browse-on-zone-click="true"
                                        multiple="multiple" required>
                                </section>
                                <section class="form-group col-sm-12">
                                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                        {{ __('Comentarios') }}
                                    </label>
                                    <textarea id="comments" name="comments" cols="10" rows="10" class="nes-textarea"
                                        placeholder="@lang('messages.add_commnet_respect')"></textarea>
                                    <section class="textarea-counter" data-text-max="350"
                                        data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></section>
                                </section>
                                <section class="text-center form-group col-md-12">
                                    <br>
                                    <input type="submit" class="nes-btn is-success text-lg retro"
                                        value="@lang('messages.publish')">
                                </section>
                            </form>
                        </div>
                        <div id="Consolas">
                            <form class="w-full" action="{{ route('send_product_request_exist') }}" method="POST"
                                enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                                <input name="category" type="hidden" value="Consolas">
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro">
                                    <center>Detalles Principales</center>
                                </h5>
                                <center>
                                    <small class="retro">Tienes dudas con las regiones? Recuerda - <svg
                                            width="20px" height="76px" data-html="true" data-toggle="popover" data-content="
                                           - Si la caja está en diferentes idiomas - PAL-EU <br />
                                           - Si la caja está solo en Español - PAL-ESP <br />
                                           - Si la caja está en Alemán - PAL-DE <br />
                                           - Si la caja está en Italiano - PAL-ITA <br />
                                           - Si la caja está en Francés - PAL-FRA <br />
                                           - Si el juego está en inglés y tiene etiqueta con una M en el frontal - es NTSC-U <br />
                                           - Si el juego tiene una etiqueta en el frontal marcada con USK es PAL-UK<br />
                                           - Si la portada e información del juego está en japonés es NTSC-J"
                                            data-placement="top" data-trigger="hover" viewBox="0 0 76 76"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path id="greyshadow" d="M8 8h68v68h-68z" fill="#BFBFBF" />
                                            <path id="blackborder" d="M4 4h68v68h-68z" fill="#000" />
                                            <path id="background" d="M4 4h64v64h-64z" fill="#FFC07C" />
                                            <path id="borderlefttop" d="M4 0h64M0 4v64" stroke="#DE5917" stroke-width="8" />
                                            <path id="rivets" d="M8 8h4v4h-4zM60 60h4v4h-4zM8 60h4v4h-4zM60 8h4v4h-4z"
                                                fill="#000" />
                                            <path id="questionshadow"
                                                d="M24 20h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM36 52h8v8h-8z"
                                                fill="#000" />
                                            <path id="question"
                                                d="M20 16h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM32 48h8v8h-8z"
                                                fill="#DE5917" />
                                        </svg>
                                    </small>
                                </center>
                                <section class="col-md-12">

                                    <select name="product" id='selProductConsole' style='width: 200px;'>
                                        <option value='0'>-- @lang('messages.search_product') --
                                        </option>
                                    </select>

                                </section>
                                <br>
                                <center>
                                    <a href="{{ route('product_not_found') }}" class="nes-btn is-warning text-base retro">@lang('messages.not_found_your_product')</a>
                                </center>
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro">
                                    <center>@lang('messages.detail_inventory')</center>
                                </h5>
                                <br>
                                <section class="flex flex-wrap mb-2 -mx-3">

                                    <section class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            {{ __('Cantidad') }}
                                        </label>
                                        <input type="number" name="quantity" class="nes-input" min="1" value="1" />
                                    </section>
                                    <section class="w-full px-3 md:w-1/2">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="grid-last-name">
                                            {{ __('Precio') }}
                                        </label>
                                        <section class="nes-field">
                                            <input type="text" autocomplete="off" name="price"
                                                class="text-right nes-input br-input-number price-check" step="0.01"
                                                placeholder="0.00" />
                                        </section>
                                    </section>
                                </section>
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro"
                                    for="grid-city">
                                    @lang('messages.conditions_product')
                                </h5>
                                <br>
                                <section class="flex flex-wrap mb-2 -mx-3">
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            @lang('messages.box')
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="box_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>

                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>

                                                </select>

                                            </section>

                                        </section>
                                    </section>
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            Consola
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="game_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>
                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                                </select>

                                            </section>

                                        </section>
                                    </section>
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            @lang('messages.inside')
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="cover_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>
                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                                </select>
                                            </section>

                                        </section>

                                    </section>

                                </section>
                                <br>
                                <section class="flex flex-wrap mb-2 -mx-3">
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            Manual
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="manual_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>

                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>

                                                </select>

                                            </section>

                                        </section>
                                    </section>
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            Cables
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="inside_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>
                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                                </select>

                                            </section>

                                        </section>
                                    </section>
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            Extras
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="extra_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>
                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                                </select>
                                            </section>

                                        </section>

                                    </section>

                                </section>

                                <label for="input-res-1"
                                    class="text-xs font-bold retro">@lang('messages.add_new_image_obligated')
                                    <font color="red">@lang('messages.max_image')</font>
                                </label>

                                <section class="file-loading">
                                    <input id="input-19" name="image_path[]" type="file" data-browse-on-zone-click="true"
                                        multiple="multiple" required>
                                </section>

                                <section class="form-group col-sm-12">
                                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                        {{ __('Comentarios') }}
                                    </label>
                                    <textarea id="comments" name="comments" cols="10" rows="10" class="nes-textarea"
                                        placeholder="@lang('messages.add_commnet_respect')"></textarea>
                                    <section class="textarea-counter" data-text-max="350"
                                        data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></section>
                                </section>
                                <section class="text-center form-group col-md-12">
                                    <br>
                                    <input type="submit" class="nes-btn is-success text-lg retro"
                                        value="@lang('messages.publish')">
                                </section>
                            </form>
                        </div>
                        <div id="Perifericos">
                            <form class="w-full" action="{{ route('send_product_request_exist') }}" method="POST"
                                enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                                <input name="category" type="hidden" value="Perifericos">
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro">
                                    <center>Detalles Principales</center>
                                </h5>
                                <center>
                                    <small class="retro">Tienes dudas con las regiones? Recuerda - <svg
                                            width="20px" height="76px" data-html="true" data-toggle="popover" data-content="
                                           - Si la caja está en diferentes idiomas - PAL-EU <br />
                                           - Si la caja está solo en Español - PAL-ESP <br />
                                           - Si la caja está en Alemán - PAL-DE <br />
                                           - Si la caja está en Italiano - PAL-ITA <br />
                                           - Si la caja está en Francés - PAL-FRA <br />
                                           - Si el juego está en inglés y tiene etiqueta con una M en el frontal - es NTSC-U <br />
                                           - Si el juego tiene una etiqueta en el frontal marcada con USK es PAL-UK<br />
                                           - Si la portada e información del juego está en japonés es NTSC-J"
                                            data-placement="top" data-trigger="hover" viewBox="0 0 76 76"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path id="greyshadow" d="M8 8h68v68h-68z" fill="#BFBFBF" />
                                            <path id="blackborder" d="M4 4h68v68h-68z" fill="#000" />
                                            <path id="background" d="M4 4h64v64h-64z" fill="#FFC07C" />
                                            <path id="borderlefttop" d="M4 0h64M0 4v64" stroke="#DE5917" stroke-width="8" />
                                            <path id="rivets" d="M8 8h4v4h-4zM60 60h4v4h-4zM8 60h4v4h-4zM60 8h4v4h-4z"
                                                fill="#000" />
                                            <path id="questionshadow"
                                                d="M24 20h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM36 52h8v8h-8z"
                                                fill="#000" />
                                            <path id="question"
                                                d="M20 16h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM32 48h8v8h-8z"
                                                fill="#DE5917" />
                                        </svg>
                                    </small>
                                </center>
                                <section class="col-md-12">

                                    <select name="product" id='selProductPeriferico' style='width: 200px;'>
                                        <option value='0'>-- @lang('messages.search_product') --
                                        </option>
                                    </select>

                                </section>
                                <br>
                                <center>
                                    <a href="{{ route('product_not_found') }}" class="nes-btn is-warning text-base retro">@lang('messages.not_found_your_product')</a>
                                </center>
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro">
                                    <center>@lang('messages.detail_inventory')</center>
                                </h5>
                                <br>
                                <section class="flex flex-wrap mb-2 -mx-3">

                                    <section class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            {{ __('Cantidad') }}
                                        </label>
                                        <input type="number" name="quantity" class="nes-input" min="1" value="1" />
                                    </section>
                                    <section class="w-full px-3 md:w-1/2">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="grid-last-name">
                                            {{ __('Precio') }}
                                        </label>
                                        <section class="nes-field">
                                            <input type="text" autocomplete="off" name="price"
                                                class="text-right nes-input br-input-number price-check" step="0.01"
                                                placeholder="0.00" />
                                        </section>
                                    </section>
                                </section>
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro"
                                    for="grid-city">
                                    @lang('messages.conditions_product')
                                </h5>
                                <br>
                                <section class="flex flex-wrap mb-2 -mx-3">
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            @lang('messages.box')
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="box_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>

                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>

                                                </select>

                                            </section>

                                        </section>
                                    </section>
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            @lang('messages.products')
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="game_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>
                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                                </select>

                                            </section>

                                        </section>
                                    </section>
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            Extras
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="extra_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>
                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                                </select>
                                            </section>

                                        </section>

                                    </section>

                                </section>
                                <label for="input-res-1"
                                    class="text-xs font-bold retro">@lang('messages.add_new_image_obligated')
                                    <font color="red">@lang('messages.max_image')</font>
                                </label>

                                <section class="file-loading">
                                    <input id="input-18" name="image_path[]" type="file" data-browse-on-zone-click="true"
                                        multiple="multiple" required>
                                </section>
                                <br>
                                <section class="form-group col-sm-12">
                                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                        {{ __('Comentarios') }}
                                    </label>
                                    <textarea id="comments" name="comments" cols="10" rows="10" class="nes-textarea"
                                        placeholder="@lang('messages.add_commnet_respect')"></textarea>
                                    <section class="textarea-counter" data-text-max="350"
                                        data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></section>
                                </section>
                                <section class="text-center form-group col-md-12">
                                    <br>
                                    <input type="submit" class="nes-btn is-success text-lg retro"
                                        value="@lang('messages.publish')">
                                </section>
                            </form>
                        </div>
                        <div id="Accesorios">
                            <form class="w-full" action="{{ route('send_product_request_exist') }}" method="POST"
                                enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                                <input name="category" type="hidden" value="Accesorios">
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro">
                                    <center>Detalles Principales</center>
                                </h5>
                                <center>
                                    <small class="retro">Tienes dudas con las regiones? Recuerda - <svg
                                            width="20px" height="76px" data-html="true" data-toggle="popover" data-content="
                                           - Si la caja está en diferentes idiomas - PAL-EU <br />
                                           - Si la caja está solo en Español - PAL-ESP <br />
                                           - Si la caja está en Alemán - PAL-DE <br />
                                           - Si la caja está en Italiano - PAL-ITA <br />
                                           - Si la caja está en Francés - PAL-FRA <br />
                                           - Si el juego está en inglés y tiene etiqueta con una M en el frontal - es NTSC-U <br />
                                           - Si el juego tiene una etiqueta en el frontal marcada con USK es PAL-UK<br />
                                           - Si la portada e información del juego está en japonés es NTSC-J"
                                            data-placement="top" data-trigger="hover" viewBox="0 0 76 76"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path id="greyshadow" d="M8 8h68v68h-68z" fill="#BFBFBF" />
                                            <path id="blackborder" d="M4 4h68v68h-68z" fill="#000" />
                                            <path id="background" d="M4 4h64v64h-64z" fill="#FFC07C" />
                                            <path id="borderlefttop" d="M4 0h64M0 4v64" stroke="#DE5917" stroke-width="8" />
                                            <path id="rivets" d="M8 8h4v4h-4zM60 60h4v4h-4zM8 60h4v4h-4zM60 8h4v4h-4z"
                                                fill="#000" />
                                            <path id="questionshadow"
                                                d="M24 20h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM36 52h8v8h-8z"
                                                fill="#000" />
                                            <path id="question"
                                                d="M20 16h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM32 48h8v8h-8z"
                                                fill="#DE5917" />
                                        </svg>
                                    </small>
                                </center>
                                <section class="col-md-12">

                                    <select name="product" id='selProductAccesorio' style='width: 200px;'>
                                        <option value='0'>-- @lang('messages.search_product') --
                                        </option>
                                    </select>

                                </section>
                                <br>
                                <center>
                                    <a href="{{ route('product_not_found') }}" class="nes-btn is-warning text-base retro">@lang('messages.not_found_your_product')</a>
                                </center>
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro">
                                    <center>@lang('messages.detail_inventory')</center>
                                </h5>
                                <br>
                                <section class="flex flex-wrap mb-2 -mx-3">

                                    <section class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            {{ __('Cantidad') }}
                                        </label>
                                        <input type="number" name="quantity" class="nes-input" min="1" value="1" />
                                    </section>
                                    <section class="w-full px-3 md:w-1/2">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="grid-last-name">
                                            {{ __('Precio') }}
                                        </label>
                                        <section class="nes-field">
                                            <input type="text" autocomplete="off" name="price"
                                                class="text-right nes-input br-input-number price-check" step="0.01"
                                                placeholder="0.00" />
                                        </section>
                                    </section>
                                </section>
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro"
                                    for="grid-city">
                                    @lang('messages.conditions_product')
                                </h5>
                                <br>
                                <section class="flex flex-wrap mb-2 -mx-3">
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            @lang('messages.box')
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="box_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>

                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>

                                                </select>

                                            </section>

                                        </section>
                                    </section>
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            @lang('messages.products')
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="game_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>
                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                                </select>

                                            </section>

                                        </section>
                                    </section>
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            Extras
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="extra_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>
                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                                </select>
                                            </section>

                                        </section>

                                    </section>

                                </section>
                                <label for="input-res-1"
                                    class="text-xs font-bold retro">@lang('messages.add_new_image_obligated')<font
                                        color="red">
                                        @lang('messages.max_image')</font></label>

                                <section class="file-loading">
                                    <input id="input-17" name="image_path[]" type="file" data-browse-on-zone-click="true"
                                        multiple="multiple" required>
                                </section>
                                <br>
                                <section class="form-group col-sm-12">
                                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                        {{ __('Comentarios') }}
                                    </label>
                                    <textarea id="comments" name="comments" cols="10" rows="10" class="nes-textarea"
                                        placeholder="@lang('messages.add_commnet_respect')"></textarea>
                                    <section class="textarea-counter" data-text-max="350"
                                        data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></section>
                                </section>
                                <section class="text-center form-group col-md-12">
                                    <br>
                                    <input type="submit" class="nes-btn is-success text-lg retro"
                                        value="@lang('messages.publish')">
                                </section>
                            </form>
                        </div>
                        <div id="Merchandising">
                            <form class="w-full" action="{{ route('send_product_request_exist') }}" method="POST"
                                enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                                <input name="category" type="hidden" value="Merchandising">
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro">
                                    <center>Detalles Principales</center>
                                </h5>
                                <br>
                                <section class="col-md-12">

                                    <select name="product" id='selProductMerchandising' style='width: 200px;'>
                                        <option value='0'>-- @lang('messages.search_product') --
                                        </option>
                                    </select>

                                </section>
                                <br>
                                <center>
                                    <a href="{{ route('product_not_found') }}" class="nes-btn is-warning text-base retro">@lang('messages.not_found_your_product')</a>
                                </center>
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro">
                                    <center>@lang('messages.detail_inventory')</center>
                                </h5>
                                <br>
                                <section class="flex flex-wrap mb-2 -mx-3">

                                    <section class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            {{ __('Cantidad') }}
                                        </label>
                                        <input type="number" name="quantity" class="nes-input" min="1" value="1" />
                                    </section>
                                    <section class="w-full px-3 md:w-1/2">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="grid-last-name">
                                            {{ __('Precio') }}
                                        </label>
                                        <section class="nes-field">
                                            <input type="text" autocomplete="off" name="price"
                                                class="text-right nes-input br-input-number price-check" step="0.01"
                                                placeholder="0.00" />
                                        </section>
                                    </section>
                                </section>
                                <br>
                                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro"
                                    for="grid-city">
                                    @lang('messages.conditions_product')
                                </h5>
                                <br>
                                <section class="flex flex-wrap mb-2 -mx-3">
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            @lang('messages.box')
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="box_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>

                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>

                                                </select>

                                            </section>

                                        </section>
                                    </section>
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            @lang('messages.products')
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="game_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>
                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                                </select>

                                            </section>

                                        </section>
                                    </section>
                                    <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                            Extras
                                        </label>
                                        <section class="relative">
                                            <section class="nes-select">

                                                <select name="extra_condition" required id="default_select"
                                                    class="retro">
                                                    <option value="" disabled selected hidden>@lang('messages.selects')
                                                    </option>
                                                    <option value="NEW">@lang('messages.new_inventory')</option>
                                                    <option value="USED-NEW">@lang('messages.used_new_inventory')</option>
                                                    <option value="USED">@lang('messages.used_inventory')</option>
                                                    <option value="USED-VERY">@lang('messages.most_used_inventory')</option>
                                                    <option value="NOT-WORK">@lang('messages.not_work_inventory')</option>
                                                    <option value="NOT-PRES">@lang('messages.not_found_inventory')</option>
                                                </select>
                                            </section>

                                        </section>

                                    </section>

                                </section>
                                <label for="input-res-1"
                                    class="text-xs font-bold retro">@lang('messages.add_new_image_obligated') <font
                                        color="red">
                                        @lang('messages.max_image')</font></label>

                                <section class="file-loading">
                                    <input id="input-16" name="image_path[]" type="file" data-browse-on-zone-click="true"
                                        multiple="multiple" required>
                                </section>
                                <br>
                                <section class="form-group col-sm-12">
                                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                                        {{ __('Comentarios') }}
                                    </label>
                                    <textarea id="comments" name="comments" cols="10" rows="10" class="nes-textarea"
                                        placeholder="@lang('messages.add_commnet_respect')"></textarea>
                                    <section class="textarea-counter" data-text-max="350"
                                        data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></section>
                                </section>
                                <section class="text-center form-group col-md-12">
                                    <br>
                                    <input type="submit" class="nes-btn is-success text-lg retro"
                                        value="@lang('messages.publish')">
                                </section>
                            </form>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>
@endsection

@section('content-script-include')
    <script>
        $(document).ready(function() {
            //hide all in target div
            $("div", "div#target").hide();
            $("select#type").change(function() {
                // hide previously shown in target div
                $("div", "div#target").hide();

                // read id from your select
                var value = $(this).val();

                // show rlrment with selected id
                $("div#" + value).show();
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.comments').popover();
            $('[data-toggle="popover"]').popover();
        });
    </script>
    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#selProduct").select2({
                minimumInputLength: 3,
                width: '100%',

                ajax: {
                    url: "{{ route('SearchbyGame') }}",
                    type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            _token: CSRF_TOKEN,
                            search_product: params.term // search term
                        };
                    },
                    processResults: function(response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }

            });

        });
    </script>
    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#selProductConsole").select2({
                minimumInputLength: 3,
                width: '100%',

                ajax: {
                    url: "{{ route('SearchbyConsole') }}",
                    type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            _token: CSRF_TOKEN,
                            search_product: params.term // search term
                        };
                    },
                    processResults: function(response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }

            });

        });
    </script>
    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#selProductPeriferico").select2({
                minimumInputLength: 3,
                width: '100%',

                ajax: {
                    url: "{{ route('SearchbyPeriferico') }}",
                    type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            _token: CSRF_TOKEN,
                            search_product: params.term // search term
                        };
                    },
                    processResults: function(response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }

            });

        });
    </script>
    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#selProductAccesorio").select2({
                minimumInputLength: 3,
                width: '100%',

                ajax: {
                    url: "{{ route('SearchbyAccesorio') }}",
                    type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            _token: CSRF_TOKEN,
                            search_product: params.term // search term
                        };
                    },
                    processResults: function(response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }

            });

        });
    </script>
    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#selProductMerchandising").select2({
                minimumInputLength: 3,
                width: '100%',

                ajax: {
                    url: "{{ route('SearchbyMerchandising') }}",
                    type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            _token: CSRF_TOKEN,
                            search_product: params.term // search term
                        };
                    },
                    processResults: function(response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }

            });

        });
    </script>
    <script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
    <script src="{{ asset('plugins/fileinput/js/fileinput.min.js') }}"></script>
    <script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('plugins/datepicker/jquery-ui.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                maxFileCount: 4
            });
            $("#input-19").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                maxFileCount: 4
            });
            $("#input-18").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                maxFileCount: 4
            });
            $("#input-17").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                maxFileCount: 4
            });
            $("#input-16").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                maxFileCount: 4
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#region_merchandising').select2();
            $('#platform_merchandising').select2();
            $('#region_accesorio').select2();
            $('#platform_accesorio').select2();
            $('#platform_periferico').select2();
            $('#region_periferico').select2();
            $('#region_platform').select2();
            $('#consola_platform').select2();
            $('#platform').select2();
            $('#region').select2();
        });
    </script>
@endsection
