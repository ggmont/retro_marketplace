@extends('brcode.front.layout.app_home')
@section('content-css-include')
    <style>
        .btn-notification {
            display: none;
            /* oculta el botón por defecto */
        }
    </style>
    @livewireStyles
@endsection

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        @include('partials.flash')
        <div class="header-area" id="headerArea">
            <div class="container h-100 d-flex align-items-center justify-content-between">
                <!-- Back Button-->
                <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                <!-- Page Title-->
                <div class="page-heading">
                    <h6 class="mb-0 font-extrabold">Problemas con la cuenta</h6>
                </div>
                <!-- Navbar Toggler-->

                <div class="normal">
                    @if (Auth::user())
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @else
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div id="appCapsule">

            <div class="container">
                <span class="retro">@include('partials.flash')</span>
                <br><br><br>
                <h4>
                    <center>¿Tienes algun problema?</center>
                </h4>
                <br>
                <select class="form-control form-select" required id="type">
                    <option value="inicio" disabled selected hidden>Selecciona...</option>
                    <option value="forgot_password">Olvido de contraseña</option>
                    <option value="forgot_user">Olvido de usuario</option>
                    <option value="forgot_email">No llega el correo de activación</option>
                </select>
                <br>
                <div id="target">
                    <div id="inicio">
                        <h4>¿Necesitas ayuda con tu cuenta?</h4>
                        <p>
                            Sabemos que a veces pueden surgir problemas con tu cuenta, ya sea por olvidar la contraseña, el nombre de usuario o si no has recibido el correo de activación. Estamos aquí para ayudarte a resolverlo.
                        </p>
                        <h4>Selecciona el tipo de problema:</h4>
                        <p>
                            Utiliza el siguiente formulario para indicarnos qué tipo de problema estás experimentando. Podrás elegir entre opciones como "Olvidé mi contraseña", "No recuerdo mi nombre de usuario" o "No he recibido el correo de activación". Una vez que hayas seleccionado el problema, nos encargaremos de ayudarte a resolverlo.
                        </p>                        
                    </div>
                    <div id="forgot_password">
                        <section class="section text-center">
                            <h4>@lang('messages.recovery_password')</h4>
                            <section class="font-normal">@lang('messages.recovery_password_email')</section>
                            <section class="wide-block pb-1 pt-2">
                                <section class="form-group boxed">
                                    <form method="post" action="{{ route('passResetzero') }}">
                                        {{ csrf_field() }}
                                        <section class="input-wrapper">
                                            <input type="email" value="" name="email" class="form-control"
                                                placeholder="@lang('messages.recovery_ingress_email')">
                                        </section>
                                        <button type="submit" class="btn btn-success mt-3 btn-lg w-100"
                                            type="submit">@lang('messages.the_recovery')</button>

                                    </form>
                                </section>
                                <br>
                                <!-- Redes Sociales -->
                                <span class="text-center retro">@lang('messages.follow_social')</span>
                                <section class="mt-2">
                                    <a href="https://www.facebook.com/people/Retrogamingmarket/100063863901900/"
                                        class="btn btn-icon btn-sm btn-facebook">
                                        <ion-icon name="logo-facebook"></ion-icon>
                                    </a>
                                    <a href="https://twitter.com/Retrogamingmkt" class="btn btn-icon btn-sm btn-twitter">
                                        <ion-icon name="logo-twitter"></ion-icon>
                                    </a>
                                    <a href="https://www.instagram.com/retrogamingmarket/"
                                        class="btn btn-icon btn-sm btn-instagram">
                                        <ion-icon name="logo-instagram"></ion-icon>
                                    </a>
                                </section>
                            </section>
                        </section>
                    </div>
                    <div id="forgot_user">
                        <section class="section text-center">
                            <h4>@lang('messages.recovery_user')</h4>
                            <section class="font-normal">Escribe tu Email para recordar tu usuario</section>
                            <section class="wide-block pb-1 pt-2">
                                <section class="form-group boxed">
                                    <form method="post" action="{{ route('resetUser') }}">
                                        {{ csrf_field() }}
                                        <section class="input-wrapper">
                                            <input type="email" value="" name="email" class="form-control"
                                                placeholder="@lang('messages.recovery_ingress_email')">
                                        </section>
                                        <button type="submit" class="btn btn-success mt-3 btn-lg w-100"
                                            type="submit">@lang('messages.the_recovery')</button>
            
                                    </form>
                                </section>
                                <br>
                                <!-- Redes Sociales -->
                                <span class="text-center retro">@lang('messages.follow_social')</span>
                                <section class="mt-2">
                                    <a href="https://www.facebook.com/people/Retrogamingmarket/100063863901900/"
                                        class="btn btn-icon btn-sm btn-facebook">
                                        <ion-icon name="logo-facebook"></ion-icon>
                                    </a>
                                    <a href="https://twitter.com/Retrogamingmkt" class="btn btn-icon btn-sm btn-twitter">
                                        <ion-icon name="logo-twitter"></ion-icon>
                                    </a>
                                    <a href="https://www.instagram.com/retrogamingmarket/" class="btn btn-icon btn-sm btn-instagram">
                                        <ion-icon name="logo-instagram"></ion-icon>
                                    </a>
                                </section>
                            </section>
                        </section>
                    </div>
                    <div id="forgot_email">
                        <section class="section text-center">
                            <h4>@lang('messages.resend_message')</h4>
                            <section class="font-norml">@lang('messages.recovery_activation_email')</section>
                            <section class="wide-block pb-1 pt-2">
                                <section class="form-group boxed">
                                    <form method="post" action="{{ route('resendActivation') }}">
                                        {{ csrf_field() }}
                                        <section class="input-wrapper">
                                            <input type="email" value="" name="email" class="form-control"
                                                placeholder="@lang('messages.recovery_ingress_email')">
                                        </section>
                                        <button type="submit" class="btn btn-success mt-3 btn-lg w-100"
                                            type="submit">@lang('messages.the_recovery')</button>
            
                                    </form>
                                </section>
                                <br>
                                <!-- Redes Sociales -->
                                <span class="text-center retro">@lang('messages.follow_social')</span>
                                <section class="mt-2">
                                    <a href="https://www.facebook.com/people/Retrogamingmarket/100063863901900/"
                                        class="btn btn-icon btn-sm btn-facebook">
                                        <ion-icon name="logo-facebook"></ion-icon>
                                    </a>
                                    <a href="https://twitter.com/Retrogamingmkt" class="btn btn-icon btn-sm btn-twitter">
                                        <ion-icon name="logo-twitter"></ion-icon>
                                    </a>
                                    <a href="https://www.instagram.com/retrogamingmarket/" class="btn btn-icon btn-sm btn-instagram">
                                        <ion-icon name="logo-instagram"></ion-icon>
                                    </a>
                                </section>
                            </section>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="nk-fullscreen-block">
            <div class="nk-fullscreen-block-middle">
                <div class="container text-center">
                    <div class="row">
                        <div class="col-md-6 offset-md-3 col-lg-4 offset-lg-4">
                            <span class="text-gray-900 retro">@lang('messages.recovery_password')</span>
                            <div class="text-center text-red-700 retro">@lang('messages.recovery_password_email')</div>
                            <div class="nk-gap-2"></div>
                            <span class="text-xs retro">
                                @include('partials.flash')
                            </span>

                            <!-- START: MailChimp Signup Form -->
                            <form method="post" action="{{ route('passResetzero') }}">
                                {{ csrf_field() }}
                                <div class="nes-field is-inline">
                                    <input type="email" value="" name="email" class="nes-input"
                                        placeholder="@lang('messages.recovery_ingress_email')">
                                    <button class="nes-btn inline_field" type="submit">
                                        <span class="text-gray-900 retro">@lang('messages.the_recovery')</span>
                                    </button>
                                </div>

                            </form>
                            <br>
                            <!-- END: MailChimp Signup Form -->
                            <span class="text-center retro">@lang('messages.also_recovery')</span>
                            <br>
                            <section class="icon-list">
                                <!-- twitter -->
                                <a href="https://twitter.com/Retrogamingmkt" target="_blank" data-toggle="tooltip"
                                    title="Twitter"><i class="nes-icon twitter is-large"></i></a>
                                <!-- facebook -->
                                <a href="https://www.facebook.com/Retrogamingmarket-102746414551509" target="_blank"
                                    data-toggle="tooltip" title="Facebook"><i class="nes-icon facebook is-large"></i></a>

                                <!-- instagram -->
                                <a href="https://www.instagram.com/retrogamingmarket/" data-toggle="tooltip" target="_blank"
                                    title="instagram"><i class="nes-icon instagram is-large"></i></a>
                            </section>
                        </div>
                        <div class="nk-gap-3"></div>


                    </div>



                </div>

            </div>
        </div>
    @endif
@endsection

@section('content-script-include')
<script>
    $(document).ready(function() {
        //$("#gate").val('Gateway 2');
        //hide all in target div
        $("div", "div#target").hide();
        $("div#" + 'inicio').show();
        $("select#type").change(function() {
            // hide previously shown in target div
            $("div", "div#target").hide();
            //$("select#type").val('Juegos');
            // read id from your select
            var value = $(this).val();

            console.log(value);

            // show rlrment with selected id
            $("div#" + value).show();
        });
    });
</script>
@endsection
@section('content-script')
@endsection
