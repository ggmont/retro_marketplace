@extends('brcode.front.layout.app')
@section('content-css-include')
    <!-- File Uploader -->
    <link rel="stylesheet" href="{{ asset('assets/jQuery-FileUpload/css/jquery.fileupload.css') }}">
@endsection
@section('content')
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <div class="container">
        <div class="nk-gap-3"></div>
        <div class="nk-gap-3"></div>
        <div class="row">

            <div class="col-md-12">

                <h3 class="br-ol-title retro text-gray-900">{{ __('Primero') }}...</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-sm-12">
                <div class="nk-gap-3"></div>
                <p class="font-bold br-ol-message retro text-md" data-acc-option="register"><br>
                    <br>
                    @lang('messages.message_stop') <a href="#"><font color="red"><i class="fa fa-link" aria-hidden="true"></i> aquí</font> </a>
                </p>
                <div class="nk-gap"></div>
                <p class="font-bold br-ol-message retro text-md" data-acc-option="login">
                    @lang('messages.message_stop_two')
                    <a href="#"><font color="red"><i class="fa fa-link" aria-hidden="true"></i> aquí</font> </a>
                </p>
            </div>
            <div class="col-md-5 col-sm-12">
                <div class="text-center thumbnail br-ol-user-plus">
                    <img class="first-img" src="{{ asset('img/USER.png') }}" alt="">

                </div>
            </div>
        </div>
    </div><!-- /.container -->
@endsection

@section('content-script-include')
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>
@endsection
@section('content-script')
@endsection
