@extends('brcode.front.layout.app')
@section('content-css-include')
    <style>
        .br-captcha img {
            width: 220px;
        }

    </style>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
    <div class="header-area" id="headerArea">
        <div class="container h-100 d-flex align-items-center justify-content-between">
            <!-- Back Button-->
            <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
            <!-- Page Title-->
            <div class="page-heading">
                <h6 class="mb-0 font-extrabold">Contactanos</h6>
            </div>
            <!-- Navbar Toggler-->

            @if (Auth::user())
                <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                    data-bs-target="#sidebarPanel">
                    <span></span><span></span><span></span>
                </div>
            @else
                <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                    data-bs-target="#sidebarPanel">
                    <span></span><span></span><span></span>
                </div>
            @endif
        </div>
    </div>
    <div class="login-form">
        <div class="page-content-wrapper py-3">
            <div class="container">
                @include('partials.flash')
                <form action="{{ url('/contact-us') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="section mt-2 mb-3">

                        <div class="form-group boxed">
                            <label class="form-label" for="name5"> {{ __('Correo') }}</label>
                            <div class="input-wrapper">
                                <input type="text" class="form-control" name="contact_email" value="{{ old('contact_email') }}" type="text"
                                placeholder="{{ __('Correo') }}"
                                    reqauired>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <label class="form-label" for="name5">Nombre</label>
                            <div class="input-wrapper">
                                <input type="text" class="form-control" name="contact_name" value="{{ old('contact_name') }}" type="text"
                                placeholder="{{ __('Nombre') }}"
                                    required>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <label class="form-label" for="name5">Asunto</label>
                            <div class="input-wrapper">
                                <input type="text" class="form-control" name="contact_subject" placeholder="{{ __('Asunto') }}"
                                value="{{ old('contact_subject') }}"
                                    required>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="address5">Mensaje</label>
                                <textarea id="address5" name="contact_message" rows="2" class="form-control" required></textarea>
                                
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <label class="form-label" for="name5">Captcha</label>
                            <div class="captcha">
                                {!! NoCaptcha::renderJs() !!}
                                {!! NoCaptcha::display() !!}
                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <br>
                        <button class="btn mb-3 btn-danger font-extrabold btn-lg w-100"
                            type="submit">Enviar</button>



                    </div>
                </form>
            </div>

        </div>
    </div>
    @else
    <section class="content">
        <div class="container">

            <div class="row justify-content-md-center">
                <!---->
                <div class="container">
                    <ul class="nk-breadcrumbs">
                        <li>

                            <div class="section-title1">
                                <h3 class="retro">@lang('messages.contact_us')</h3>
                            </div>

                        </li>
                    </ul>

                    <br>

                    <div class="col-md-12" style="background-color: #ffffff; border: solid 1px #db2e2e">
                        @if (strlen(session('contact_form_message')) == 0)
                            <form action="{{ url('/contact-us') }}" method="post"
                                class='w-full nk-mchimp nk-form nk-form-style-1 validate'>

                                <div class="flex flex-wrap mb-6 -mx-3">
                                    <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                                        <label
                                            class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase retro"
                                            for="grid-first-name">
                                            {{ __('Correo') }}
                                        </label>
                                        <input
                                            class="block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border border-red-500 rounded appearance-none focus:outline-none focus:bg-white"
                                            name="contact_email" value="{{ old('contact_email') }}" type="text"
                                            placeholder="{{ __('Correo') }}">
                                    </div>
                                    <div class="w-full px-3 md:w-1/2">
                                        <label
                                            class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase retro"
                                            for="grid-last-name">
                                            {{ __('Nombre') }}
                                        </label>
                                        <input
                                            class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                            name="contact_name" value="{{ old('contact_name') }}" type="text"
                                            placeholder="{{ __('Nombre') }}">
                                    </div>
                                </div>
                                <div class="flex flex-wrap mb-6 -mx-3">
                                    <div class="w-full px-3">
                                        <label
                                            class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase retro"
                                            for="grid-password">
                                            {{ __('Asunto') }}
                                        </label>
                                        <input
                                            class="block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                            type="text" name="contact_subject" placeholder="{{ __('Asunto') }}"
                                            value="{{ old('contact_subject') }}">
                                    </div>
                                </div>
                                <div class="flex flex-wrap mb-6 -mx-3">
                                    <div class="w-full px-3">
                                        <label
                                            class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase retro">
                                            {{ __('Mensaje') }}
                                        </label>
                                        <textarea id="comments" name="contact_message" cols="10" rows="10"
                                            class="nes-textarea"
                                            placeholder="{{ __('Mensaje') }}">{{ old('contact_message') }}</textarea>
                                        <div class="textarea-counter" data-text-max="350"
                                            data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="captcha">
                                        {!! NoCaptcha::renderJs() !!}
                                        {!! NoCaptcha::display() !!}
                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div style="display: none;">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </div>
                                @if ($errors->any())
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                @endif
                                <button type="submit" class="border-b retro nk-btn-xs nk-btn-rounded nes-btn is-error btn-block">{{ __('Enviar') }}</button>
                            </form>
                        @else
                            <span class="br-big-message">{{ session('contact_form_message') }}</span>
                        @endif
                    </div><!-- /.col-md-* -->
                </div><!-- /.row -->
            </div><!-- /.container -->
    </section>
    @endif
    <!-- Main content -->


@endsection

@section('content-script-include')

@endsection
@section('content-script')
@endsection
