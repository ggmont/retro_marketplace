@extends('brcode.front.layout.app_tab')
@section('content-css-include')
    <style>
        @charset "UTF-8";

        main .jumbo {
            width: 100%;
            height: 400px;
            background-size: cover;
        }

        main .icons {
            display: flex;
            justify-content: space-between;
        }

        main .icons .big-icon {
            width: 180px;
            height: 180px;
            border-radius: 50%;
            border: 2px solid white;
            margin-top: -90px;
        }

        main .icons .rate {
            width: 125px;
            display: flex;
            justify-content: space-between;
            order: -1;
        }

        main .icons .rate .star-btn {
            margin-top: -27.5px;
        }

        main .icons .rate .star-btn i {
            font-size: 26px;
        }

        main .icons .rate .like-btn {
            margin-top: -27.5px;
        }

        main .icons .rate .like-btn i {
            font-size: 22px;
        }

        main .icons .add {
            width: 125px;
            display: flex;
            justify-content: flex-end;
        }


        .edition {
            padding-left: 25px;
            background: url("/img/message.png") no-repeat center #ffffff;
            background-size: 18px;
            padding-top: 10px;
        }

        main .icons .add .add-btn {
            margin-top: -27.5px;
        }

        main .icons .add .add-btn i {
            font-size: 28px;
        }

        main .details {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-bottom: 20px;
        }

        .special {
            font-size: 28px;
            margin-top: 15px;
            background: radial-gradient(ellipse farthest-corner at right bottom, #FEDB37 0%, #FDB931 8%, #9f7928 30%, #8A6E2F 40%),
                radial-gradient(ellipse farthest-corner at left top, #FFFFFF 0%, #FFFFAC 8%, #D1B464 25%, #5d4a1f 62.5%, #5d4a1f 100%);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }

        main .details p {
            color: #727272;
            margin-top: 0px;
        }

        main .bio {
            margin-bottom: 80px;
        }

        main .bio .title h6 {
            color: #212121;
            font-size: 18px;
        }

        main .bio .content p {
            color: #727272;
        }

        main .pics {
            margin-bottom: 50px;
        }

        main .pics .title {
            margin-bottom: 20px;
        }

        main .pics .title h6 {
            color: #212121;
            font-size: 18px;
        }

        main .pics .row-1 .s12:nth-of-type(2) {
            margin-bottom: -10px;
        }

        main .pics .row .s12:nth-of-type(1) {
            margin-bottom: 10px;
        }

        main .pics .row .col .card {
            height: 260px;
        }

        main .pics .row .col .card #first-img {
            height: 100%;
            background-size: cover;
        }

        main .pics .row .col .card #second-img {
            height: 100%;
            background-size: cover;
        }

        main .pics .row .col .card #third-img {
            height: 100%;
            background-size: cover;
        }

        main .pics .row .col .card #forth-img {
            height: 100%;
            background-size: cover;
        }

        main .posts {
            margin-bottom: 50px;
        }

        main .posts .title h6 {
            color: #212121;
            font-size: 18px;
            margin-bottom: 20px;
        }

        main .posts .row {
            margin-bottom: 0px;
        }

        main .posts .row .col .card {
            position: relative;
        }

        main .posts .row .col .card .card-action {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        main .posts .row .col .card .card-action .tags {
            display: flex;
        }

        main .posts .row .col .card .card-action .tags .chip:first-child {
            margin-right: 5px;
        }

        main .posts .row .col .card .card-action .card-love {
            cursor: pointer;
            font-size: 25px;
            position: absolute;
            right: 10px;
            top: 10px;
            color: white;
            transition: all 100ms ease-in-out;
        }

        main .posts .row .col .card .card-action .card-love:hover {
            color: #f9a825;
        }

        main .blogs {
            margin-bottom: 80px;
        }

        main .blogs .title {
            margin-bottom: 20px;
        }

        main .blogs .title h6 {
            color: #212121;
            font-size: 18px;
        }

        main .likes .title {
            margin-bottom: 20px;
        }

        main .likes .title h6 {
            color: #212121;
            font-size: 18px;
        }

        main .likes .row .col .tabs {
            margin-bottom: 10px;
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        }

        main .stretch {
            height: 500px;
        }

        main .fab {
            transition: all 120ms ease-in-out;
            bottom: 20px !important;
        }

        main .fab .btn-large i {
            font-size: 40px;
        }

        main .fab .orange i {
            font-size: 20px;
        }

        @media screen and (max-width: 600px) {
            main {
                padding-top: 40px;
            }

            .jumbo {
                height: 200px !important;
            }

            .icons {
                justify-content: center !important;
            }

            .rate {
                display: none !important;
            }

            .add {
                display: none !important;
            }
        }

        @media screen and (min-width: 600px) {
            .fab {
                display: none;
            }
        }
    </style>
@endsection
@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <br><br>
        <div class="header-area" id="headerArea">
            <div class="container h-100 d-flex align-items-center justify-content-between">
                <!-- Back Button-->
                <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                <!-- Page Title-->
                <div class="page-heading">
                    <h6 class="mb-0 font-extrabold">Perfil Publico</h6>
                </div>
                <!-- Navbar Toggler-->

                <div class="normal">
                    @if (Auth::user())
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @else
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @endif
                </div>
            </div>
        </div>






        @if ($usuario->cover_picture)
            <div class="overflow-hidden rounded-tl-lg rounded-tr-lg cursor-pointer">
                <a href="{{ url($usuario->cover_picture) }}" class="fancybox" data-fancybox="RGMCOVER">
                    <img class="rounded-tl-xl rounded-tr-xl" src="{{ url($usuario->cover_picture) }}" />
                </a>

            </div>
        @else
            <div class="overflow-hidden rounded-tl-lg rounded-tr-lg cursor-pointer">
                <a href="{{ url('uploads/carousels-images/carousel_image__1_6.jpg') }}" class="fancybox"
                    data-fancybox="RGMCOVER">
                    <img class="rounded-tl-xl rounded-tr-xl"
                        src="{{ url('uploads/carousels-images/carousel_image__1_6.jpg') }}" />
                </a>

            </div>
        @endif
        <div class="card user-info-card">

            <div class="card-body d-flex align-items-center">
                <div class="user-profile me-3">
                    @if (Auth::user())
                        @if ($usuario->profile_picture)
                            <a href="{{ url($usuario->profile_picture) }}" class="fancybox" data-fancybox="RGMPROFILE">
                                <img src="{{ url($usuario->profile_picture) }}"
                                    class="rounded-full h-100 w-100 border border-red-500" alt="">
                            </a>
                            @if (Auth::user()->id == $usuario->id)
                            @elseif(Auth::user())
                                <button class="form-control-message edition new-message">
                                </button>
                            @else
                                <a href="#" id="showLogin" data-nav-toggle="#nk-nav-mobile" class="showLogin item">
                                    <button class="form-control-message edition">
                                    </button>
                                </a>
                            @endif
                        @else
                            <a href="{{ asset('img/profile-picture-not-found.png') }}" class="fancybox"
                                data-fancybox="RGMPROFILE">
                                <img src="{{ asset('img/profile-picture-not-found.png') }}"
                                    class="rounded-full h-100 w-100 border border-red-500" alt="">
                            </a>
                            @if (Auth::user()->id == $usuario->id)
                            @elseif(Auth::user())
                                <button class="form-control-message edition new-message">
                                </button>
                            @else
                                <a href="#" id="showLogin" data-nav-toggle="#nk-nav-mobile" class="showLogin item">
                                    <button class="form-control-message edition">
                                    </button>
                                </a>
                            @endif
                        @endif
                    @else
                        @if ($usuario->profile_picture)
                            <a href="{{ url($usuario->profile_picture) }}" class="fancybox" data-fancybox="RGMPROFILE">
                                <img src="{{ url($usuario->profile_picture) }}"
                                    class="rounded-full h-100 w-100 border border-red-500" alt="">
                            </a>
                            @if (Auth::user())
                                <button class="form-control-message edition new-message">
                                </button>
                            @else
                                <a href="#" id="showLogin" data-nav-toggle="#nk-nav-mobile" class="showLogin item">
                                    <button class="form-control-message edition">
                                    </button>
                                </a>
                            @endif
                        @else
                            <a href="{{ asset('img/profile-picture-not-found.png') }}" class="fancybox"
                                data-fancybox="RGMPROFILE">
                                <img src="{{ asset('img/profile-picture-not-found.png') }}"
                                    class="rounded-full h-100 w-100 border border-red-500" alt="">
                            </a>
                            @if (Auth::user())
                                <button class="form-control-message edition new-message">
                                </button>
                            @else
                                <a href="#" id="showLogin" data-nav-toggle="#nk-nav-mobile" class="showLogin item">
                                    <button class="form-control-message edition">
                                    </button>
                                </a>
                            @endif
                        @endif
                    @endif
                </div>
                <div class="user-info">
                    <div class="d-flex align-items-center">

                        <h5 class="mb-1">{{ $usuario->user_name }}</h5><span
                            class="badge bg-danger ms-2 rounded-pill">Vendedor Profesional</span>
                    </div>
                    <p class="mb-0">@lang('messages.member_date') -
                        {{ date_format($usuario->created_at, 'Y') }}</p>

                    <div class="mb-0"> {{ $usuario->score[1] }}
                    </div>

                    <p class="mb-0">Compartir Perfil
                        <a href="#" id="wha-btn" class="btn btn-icon btn-sm btn-whatsapp">
                            <ion-icon name="logo-whatsapp"></ion-icon>
                        </a>
                        <a href="#" id="tele-btn" class="btn btn-icon btn-sm btn-telegram">
                            <i class="fab fa-telegram ml-80 mt-90"></i>
                        </a>
                    </p>




                </div>
            </div>
        </div>


        <ul class="nav nav-tabs capsuled" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-bs-toggle="tab" href="#favorite" role="tab" aria-selected="true">
                    Destacados
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#collection" role="tab" aria-selected="false">
                    @lang('messages.the_collection')
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#inventories" role="tab" aria-selected="false">

                    Inventario
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#example3b" role="tab" aria-selected="false">

                    Valoraciones
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#information" role="tab">

                    Información
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade show active" id="favorite" role="tabpanel">
                @if ($sell_favorite > 0)
                    @livewire('mobile.profile.profile-profesional', ['user' => $user])
                @else
                    <div class="container">
                        <!-- Offline Area-->

                        <div class="offline-text text-center">
                            <h3 class="mb-3 px-3">@lang('messages.not_available')</h3>
                        </div>

                    </div>
                @endif
            </div>
            <div class="tab-pane fade" id="collection" role="tabpanel">
                @if ($sell > 0)
                    @livewire('mobile.profile.profile-collection', ['user' => $user])
                @else
                    <div class="container">
                        <!-- Offline Area-->

                        <div class="offline-text text-center">
                            <h3 class="mb-3 px-3">@lang('messages.not_available')</h3>
                        </div>

                    </div>
                @endif
            </div>
            <div class="tab-pane fade" id="inventories" role="tabpanel">
                @if ($sell > 0)
                    @livewire('mobile.profile.profile-inventory', ['user' => $user])
                @else
                    <div class="container">
                        <!-- Offline Area-->

                        <div class="offline-text text-center">
                            <h3 class="mb-3 px-3">@lang('messages.not_available')</h3>
                        </div>

                    </div>
                @endif
            </div>
            <div class="tab-pane fade" id="example3b" role="tabpanel">

                @livewire('mobile.profile.rating-profile', ['user' => $user])

            </div>
            <div class="tab-pane fade" id="information" role="tabpanel">

                <div class="accordion" id="accordionExample1">

                    <div>
                        <ul class="listview image-listview media accordion-item">
                            <li class="item">
                                <h2 class="accordion-header">
                                    <button class="listview image-listview media accordion-button collapsed"
                                        type="button" data-bs-toggle="collapse" data-bs-target="#accordion1">
                                        Método de venta
                                    </button>
                                </h2>
                                <div id="accordion1" class="accordion-collapse collapse"
                                    data-bs-parent="#accordionExample1">
                                    <div class="accordion-body">
                                        <div class="item-body-wrapper">
                                            <dl class="row evaluations-descriptionList mb-2">
                                                <dt class="col-12 color-primary">
                                                    {{ $usuario->terms_and_conditions ? $usuario->terms_and_conditions : 'No hay datos' }}
                                                </dt>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>

                <div class="accordion" id="accordionExample2">

                    <div>
                        <ul class="listview image-listview media accordion-item">
                            <li class="item">
                                <h2 class="accordion-header">
                                    <button class="listview image-listview media accordion-button collapsed"
                                        type="button" data-bs-toggle="collapse" data-bs-target="#accordion2">
                                        Redes
                                    </button>
                                </h2>
                                <div id="accordion2" class="accordion-collapse collapse"
                                    data-bs-parent="#accordionExample2">
                                    <div class="accordion-body">
                                        <div class="item-body-wrapper">


                                            @if ($social > 0)
                                                <dl class="row evaluations-descriptionList mb-2">
                                                    <dt class="col-6 text-2xl color-primary">
                                                        <a class="color-primary"
                                                            href="{{ url('https://www.twitch.tv/' . $social_user->twitch) }}">
                                                            <ion-icon name="logo-twitch"></ion-icon>
                                                        </a>
                                                    </dt>
                                                    <dd class="col-6 text-right color-primary font-weight-bold">
                                                        @if ($social_user->twitch)
                                                            {{ $social_user->twitch }}
                                                        @else
                                                            -
                                                        @endif
                                                    </dd>
                                                    <dt class="col-6 text-2xl color-primary">
                                                        <a class="color-primary"
                                                            href="{{ url('https://www.youtube.com/' . $social_user->youtube) }}">
                                                            <ion-icon name="logo-youtube"></ion-icon>
                                                        </a>
                                                    </dt>
                                                    <dd class="col-6 text-right color-primary font-weight-bold">
                                                        @if ($social_user->youtube)
                                                            {{ $social_user->youtube }}
                                                        @else
                                                            -
                                                        @endif
                                                    </dd>
                                                    <dt class="col-6 text-2xl color-primary">
                                                        <a class="color-primary"
                                                            href="{{ url('https://www.tiktok.com/' . $social_user->tiktok) }}">
                                                            <ion-icon name="logo-tiktok"></ion-icon>
                                                        </a>
                                                    </dt>
                                                    <dd class="col-6 text-right color-primary font-weight-bold">
                                                        @if ($social_user->tiktok)
                                                            {{ $social_user->tiktok }}
                                                        @else
                                                            -
                                                        @endif
                                                    </dd>
                                                    <dt class="col-6 text-2xl color-primary">
                                                        <a class="color-primary"
                                                            href="{{ url('https://www.instagram.com/' . $social_user->instagram) }}">
                                                            <ion-icon name="logo-instagram"></ion-icon>
                                                        </a>
                                                    </dt>
                                                    <dd class="col-6 text-right color-primary font-weight-bold">
                                                        @if ($social_user->instagram)
                                                            {{ $social_user->instagram }}
                                                        @else
                                                            -
                                                        @endif
                                                    </dd>
                                                </dl>
                                            @else
                                                <dl class="row evaluations-descriptionList mb-2">
                                                    <dt class="col-6 text-2xl color-primary">
                                                        <ion-icon name="logo-twitch"></ion-icon>
                                                    </dt>
                                                    <dd class="col-6 text-right color-primary font-weight-bold">
                                                        -</dd>
                                                    <dt class="col-6 text-2xl color-primary">
                                                        <ion-icon name="logo-youtube"></ion-icon>
                                                    </dt>
                                                    <dd class="col-6 text-right color-primary font-weight-bold">
                                                        -
                                                    </dd>
                                                    <dt class="col-6 text-2xl color-primary">
                                                        <ion-icon name="logo-tiktok"></ion-icon>
                                                    </dt>
                                                    <dd class="col-6 text-right color-primary font-weight-bold">
                                                        -
                                                    </dd>
                                                    <dt class="col-6 text-2xl color-primary">
                                                        <ion-icon name="logo-instagram"></ion-icon>
                                                    </dt>
                                                    <dd class="col-6 text-right color-primary font-weight-bold">
                                                        -
                                                    </dd>
                                                </dl>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>

                <ul class="listview image-listview">
                    <li>
                        <div class="item">

                            <div class="in">
                                <div>Valoraciones</div>
                                <span
                                    class="badge badge-danger">{{ App\AppOrgUserRating::where('seller_user_id', $usuario->id)->where('processig', 1)->where('packaging', 1)->where('desc_prod', 1)->count() }}</span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item">
                            <div class="in">
                                <div>Compras</div>
                                <span
                                    class="badge badge-danger">{{ App\AppOrgOrder::where('buyer_user_id', $usuario->id)->whereIn('status', ['DD', 'ST', 'CR', 'PC'])->count() }}</span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item">
                            <div class="in">
                                <div>Ventas</div>
                                <span
                                    class="badge badge-danger">{{ App\AppOrgOrder::where('seller_user_id', $usuario->id)->whereIn('status', ['DD'])->count() }}</span>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        @if (Auth::user())
            <div class="modal fade" id="modalNewMessage" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <center>
                                <h5 class="modal-title retro" id="exampleModalLabel">Enviar Mensaje a :
                                    {{ $usuario->user_name }}
                                </h5>
                            </center>
                            <button class="close-message btn btn-close p-1 ms-auto me-0" class="close"
                                data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="{{ route('newMessage') }} ">
                                {{ csrf_field() }}
                                <input type="hidden" autocomplete="off" class="form-control" name="name"
                                    value="{{ $usuario->user_name }}" required>
                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <textarea name="content" rows="2" placeholder="@lang('messages.write_msg')" class="form-control"></textarea>
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>
                                <br>
                                <button type="submit"
                                    class="btn confirmclosed btn-submit btn-primary w-100 text-lg font-extrabold">Enviar</button>
                            </form>
                        </div>



                    </div>
                </div>
            </div>
        @endif
        <!-- Internet Connection Status-->
        <div class="internet-connection-status" id="internetStatus"></div>
    @else
        <div class="relative">
            <div class="overflow-hidden cursor-pointer mb-6">
                @if ($usuario->cover_picture)
                    <a href="{{ url($usuario->cover_picture) }}" class="fancybox" data-fancybox="RGMCOVER">
                        <img class="w-full object-cover h-96" src="{{ url($usuario->cover_picture) }}" />
                    </a>
                @else
                    <a href="{{ url('uploads/carousels-images/carousel_image__1_6.jpg') }}" class="fancybox"
                        data-fancybox="RGMCOVER">
                        <img class="w-full object-cover h-96"
                            src="{{ url('uploads/carousels-images/carousel_image__1_6.jpg') }}" />
                    </a>
                @endif
            </div>


            <div class="container mx-auto">
                <div class="relative">
                    <div class="bg-white rounded-lg shadow-lg p-6 -mt-16 md:-mt-20">
                        <div class="flex">
                            <div class="flex-shrink-0">
                                <a href="{{ url($usuario->profile_picture) }}" class="fancybox"
                                    data-fancybox="RGMPROFILE">
                                    <img src="{{ url($usuario->profile_picture) }}"
                                        class="rounded-full h-24 w-24 border border-red-500" alt="Profile Picture">
                                </a>
                            </div>
                            <div class="ml-4">
                                <h3 class="text-2xl font-bold mb-2">{{ $usuario->user_name }}</h3>
                                <div class="mt-1">
                                    <span
                                        class="inline-block bg-red-500 text-white font-bold px-2 py-1 rounded">{{ $usuario->sales }}</span>
                                    <span class="text-lg font-bold ml-1">Ventas</span>
                                    <span
                                        class="inline-block bg-red-500 text-white font-bold px-2 py-1 ml-2">{{ $usuario->sales }}</span>
                                    <span class="text-lg font-bold ml-1">Compras</span>
                                </div>
                                <div class="mt-2">
                                    <span class="text-lg">@lang('messages.member_date') -</span>
                                    <span class="text-lg ml-1">{{ date_format($usuario->created_at, 'Y') }}</span>
                                </div>
                            </div>
                            <div class="flex-grow"></div>
                            <div class="text-right">
                                <h3 class="text-2xl font-bold mr-10">Evaluación general</h3>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <div class="flex items-center">
                                            <img src="{{ asset('img/new.png') }}" width="20px" data-toggle="popover"
                                                data-content="@lang('messages.good')" data-placement="top"
                                                data-trigger="hover" class="w-6 h-6">
                                            <span class="ml-1 mt-n1 text-lg">100%</span>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="flex items-center">
                                            <img src="{{ asset('img/used.png') }}" width="20px" data-toggle="popover"
                                                data-content="Neutral" data-placement="top" data-trigger="hover"
                                                class="w-6 h-6">
                                            <span class="ml-1 mt-n1 text-lg">100%</span>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="flex items-center">
                                            <img src="{{ asset('img/no-funciona.png') }}" width="20px"
                                                data-toggle="popover" data-content="@lang('messages.bad')"
                                                data-placement="top" data-trigger="hover" class="w-6 h-6">
                                            <span class="ml-1 mt-n1 text-lg">100%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="standard-tab">
                        <ul class="nav rounded-lg mb-2 p-2 shadow-sm" id="affanTabs1" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="btn active" id="star-tab" data-bs-toggle="tab" data-bs-target="#star"
                                    type="button" role="tab" aria-controls="star"
                                    aria-selected="true">Destacados</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="btn" id="inventory-tab" data-bs-toggle="tab"
                                    data-bs-target="#inventory" type="button" role="tab" aria-controls="collection"
                                    aria-selected="false">@lang('messages.the_inventory')</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="btn" id="collection-tab" data-bs-toggle="tab"
                                    data-bs-target="#collection" type="button" role="tab"
                                    aria-controls="collection" aria-selected="false">@lang('messages.the_collection')</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="btn" id="valoration-tab" data-bs-toggle="tab"
                                    data-bs-target="#valoration" type="button" role="tab"
                                    aria-controls="valoration" aria-selected="false">@lang('messages.ratings')</button>
                            </li>
                        </ul>
                        <div class="tab-content rounded-lg p-3 shadow-sm" id="affanTabs1Content">
                            <div class="tab-pane fade show active" id="star" role="tabpanel"
                                aria-labelledby="star-tab">
                                @if ($sell > 0)
                                    @livewire('mobile.profile.profile-profesional', ['user' => $user])
                                @else
                                    <div class="offline-text text-center">
                                        <br>
                                        <h3 class="mb-3 px-3">@lang('messages.not_available')</h3>
                                    </div>
                                @endif
                            </div>
                            <div class="tab-pane fade" id="inventory" role="tabpanel" aria-labelledby="inventory-tab">
                                @if ($sell > 0)
                                    @livewire('mobile.profile.profile-inventory', ['user' => $user])
                                @else
                                    <div class="offline-text text-center">
                                        <br>
                                        <h3 class="mb-3 px-3">@lang('messages.not_available')</h3>
                                    </div>
                                @endif
                            </div>
                            <div class="tab-pane fade" id="collection" role="tabpanel" aria-labelledby="collection-tab">
                                @if ($sell > 0)
                                    @livewire('mobile.profile.profile-collection', ['user' => $user])
                                @else
                                    <div class="offline-text text-center">
                                        <br>
                                        <h3 class="mb-3 px-3">@lang('messages.not_available')</h3>
                                    </div>
                                @endif
                            </div>
                            <div class="tab-pane fade" id="valoration" role="tabpanel" aria-labelledby="valoration-tab">
                                @livewire('mobile.profile.rating-profile-pc', ['user' => $user])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if (Auth::user())
            <div class="modal fade" id="modalNewMessage" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <center>
                                <h5 class="modal-title retro" id="exampleModalLabel">Enviar Mensaje a :
                                    {{ $usuario->user_name }}
                                </h5>
                            </center>
                            <button class="close-message btn btn-close p-1 ms-auto me-0" class="close"
                                data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="{{ route('newMessage') }} ">
                                {{ csrf_field() }}
                                <input type="hidden" autocomplete="off" class="form-control" name="name"
                                    value="{{ $usuario->user_name }}" required>
                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <textarea name="content" rows="2" placeholder="@lang('messages.write_msg')" class="form-control"></textarea>
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>
                                <br>
                                <button type="submit"
                                    class="btn confirmclosed btn-submit btn-primary w-100 text-lg font-extrabold">Enviar</button>
                            </form>
                        </div>



                    </div>
                </div>
            </div>
        @endif
        <!-- Internet Connection Status-->
        <div class="internet-connection-status" id="internetStatus"></div>
    @endif

@endsection

@section('content-script-include')
    <script>
        // Social Share links.
        const whatsappBtn2 = document.getElementById('wha-btn');
        const telegramBtn2 = document.getElementById('tele-btn');

        // producturl posttitle
        let productUrl = encodeURI(document.location.href);
        let productTitle = encodeURI('{{ $usuario->user_name }}');
        whatsappBtn2.setAttribute("href", `https://wa.me/?text=${productTitle} ${productUrl}`);
        telegramBtn2.setAttribute("href", `https://telegram.me/share/url?url=${productUrl}&text=${productTitle}`);
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/js/materialize.min.js"></script>
    <script src="{{ asset('brcode/js/pty.components.js') }}"></script>
    <script src="{{ asset('assets/noty/packaged/jquery.noty.packaged.min.js') }}"></script>
@endsection
@section('content-script')
    <script>
        $('.new-message').click(function() {
            $('#modalNewMessage').modal('show');
        });

        $('.close-message').click(function() {
            $('#modalNewMessage').modal('hide');
        });
    </script>

    @livewireScripts
    <script>
        window.addEventListener('show-form', event => {
            $('#viewDetails').modal('show');
        })

        window.addEventListener('hide-form', event => {
            $('#viewDetails').modal('hide');
        })
    </script>
    <script>
        Livewire.on('star', function() {


            var $inventoryAdd = $('.br-btn-product-add');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('alert', function() {
            Swal.fire(
                'Listo!',
                '<span class="retro">Tu Producto fue eliminado</span>',
                'success'
            )
        })
    </script>
@endsection
