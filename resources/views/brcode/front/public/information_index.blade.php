@extends('brcode.front.layout.app')

@section('content-css-include')
    <style>
        svg:hover>#rivets,
        svg:hover>#question {
            fill: #FFC07C;
        }

        svg:hover>#borderlefttop {
            stroke: #FFC07C;
        }

        svg:active>#rivets,
        svg:active>#borderlefttop {
            fill: #000;
            stroke: #000;
        }

        svg:hover>#background,
        svg:active>#question,
        svg:active>#questionshadow {
            fill: #DE5917;
        }

        svg:hover {
            animation: jump .2s;
        }


        @keyframes jump {
            50% {
                transform: translatey(-10px);
            }

            to {
                transform: translatey(0);
            }
        }
    </style>

    @livewireStyles
@endsection

@section('content')
    <div class="header-area" id="headerArea">
        <div class="container h-100 d-flex align-items-center justify-content-between">
            <!-- Back Button-->
            <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
            <!-- Page Title-->
            <div class="page-heading">
                <h6 class="mb-0 font-extrabold">Información</h6>
            </div>
            <!-- Navbar Toggler-->

            @if (Auth::user())
                <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                    data-bs-target="#sidebarPanel">
                    <span></span><span></span><span></span>
                </div>
            @else
                <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                    data-bs-target="#sidebarPanel">
                    <span></span><span></span><span></span>
                </div>
            @endif
        </div>
    </div>

    <div class="page-content-wrapper py-3">
        <div class="container">

            <div class="listview-title mt-2">Información</div>
            <ul class="listview link-listview">
                @foreach ($viewData['page_information'] as $p)
                    <li><a href="/site/{{ $p->url }}">
                            {{ $p->title }}
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="listview-title mt-2">Ayuda</div>
            <ul class="listview link-listview">
                <li>
                    <a href="{{ route('listUsers') }}">
                        Listado de usuarios
                    </a>
                </li>
                <li>
                    <a href="/site/faqs">
                        FAQ
                    </a>
                </li>

                @foreach ($viewData['page_guide'] as $p)
                    <li><a href="/site/{{ $p->url }}">
                            {{ $p->title }}
                        </a>
                    </li>
                @endforeach

            </ul>
            <div class="listview-title mt-2">Contacto</div>
            <div class="accordion" id="accordionExample2">

                <div>
                    <ul class="listview image-listview media accordion-item">
                        <li class="item">
                            <h2 class="accordion-header">
                                <button class="listview image-listview media accordion-button collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#accordion2">
                                    Redes sociales
                                </button>
                            </h2>
                            <div id="accordion2" class="accordion-collapse collapse" data-bs-parent="#accordionExample2">
                                <div class="accordion-body">
                                    <div class="item-body-wrapper">
                                        <div class="appFooter">
                                            <div class="mt-2">
                                                <div class="flex justify-center space-x-4">
                                                    <a href="https://www.facebook.com/people/Retrogamingmarket/100063863901900/"
                                                        class="btn btn-icon btn-sm btn-facebook">
                                                        <ion-icon name="logo-facebook"></ion-icon>
                                                    </a>
                                                    <a href="https://twitter.com/Retrogamingmkt"
                                                        class="btn btn-icon btn-sm btn-twitter">
                                                        <ion-icon name="logo-twitter"></ion-icon>
                                                    </a>
                                                    <a href="https://www.instagram.com/retrogamingmarket/"
                                                        class="btn btn-icon btn-sm btn-instagram">
                                                        <ion-icon name="logo-instagram"></ion-icon>
                                                    </a>
                                                    <a href="https://www.twitch.tv/retrogamingmarket"
                                                        class="btn btn-icon btn-sm btn-twitch">
                                                        <ion-icon name="logo-twitch"></ion-icon>
                                                    </a>
                                                    <a href="https://www.tiktok.com/@retrogamingmarket"
                                                        class="btn btn-icon btn-sm btn-tiktok">
                                                        <ion-icon name="logo-tiktok"></ion-icon>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>

            </div>
            <ul class="listview link-listview">


                <li>
                    <a href="/contact-us">
                        Formulario
                    </a>
                </li>
            </ul>
        </div>

    </div>
@endsection

@section('content-script-include')
    @livewireScripts

    <!-- Flickity -->
    <script src="{{ asset('assets/vendor/flickity/dist/flickity.pkgd.min.js') }}"></script>

    <!-- Hammer.js -->
    <script src="{{ asset('assets/vendor/hammerjs/hammer.min.js') }}"></script>
@endsection
