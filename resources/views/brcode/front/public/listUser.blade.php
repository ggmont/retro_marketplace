@extends('brcode.front.layout.app')
@section('content-css-include')
    @livewireStyles
@endsection
@section('content')
@if ((new \Jenssegers\Agent\Agent())->isMobile())
<div class="header-area" id="headerArea">
    <div class="container h-100 d-flex align-items-center justify-content-between">
        <!-- Back Button-->
        <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
        <!-- Page Title-->
        <div class="page-heading">
            <h6 class="mb-0 font-extrabold">@lang('messages.list_user')</h6>
        </div>
        <!-- Navbar Toggler-->

        @if (Auth::user())
            <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                data-bs-target="#sidebarPanel">
                <span></span><span></span><span></span>
            </div>
        @else
            <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                data-bs-target="#sidebarPanel">
                <span></span><span></span><span></span>
            </div>
        @endif
    </div>
</div>
@livewire('mobile.mobile-list-user')

@else
<div class="container">
    <div class="nk-gap-2"></div>
    <h3 class="nk-decorated-h-2">


        <span class="retro"><span class="text-main-1">@lang('messages.users_list') </span><font color="black">@lang('messages.registers_list'):  {{ $usuario->count() }} </font></span>

    </h3>
    <div class="nk-gap-2"></div>


</div>

<livewire:list-user></livewire:list-user>
@endif


@endsection

@section('content-script-include')
    @livewireScripts
    <script>
        window.addEventListener('show-form-message', event => {
            $('#modalNewMessage').modal('show');
        })

        window.addEventListener('hide-form-message', event => {
            $('#modalNewMessage').modal('hide');
        })
    </script>
    <script>
        $('.new-message').click(function() {
            $('#modalNewMessage').modal('show');
        });

        $('.close-message').click(function() {
            $('#modalNewMessage').modal('hide');
        });
    </script>
    <script>
        Livewire.on('alert', function() {
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
        })
    </script>
@endsection
