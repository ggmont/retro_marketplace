@extends('brcode.front.layout.app')
@section('content-css-include')

@endsection

@section('content')
<div class="container">
    <ul class="nk-breadcrumbs">
        <li><a href="/">{{ __('Inicio') }}</a></li>
        <li><span class="fa fa-angle-right"></span></li>
        <li><span>{{ __('Blog') }}</span></li>
    </ul>
</div>

<div class="nk-gap-1"></div>

<div class="container">
    <div class="row vertical-gap">
        <div class="col-lg-8">

            <!-- START: Posts Grid -->
            <div class="nk-blog-grid">
                <div class="row">
                    @foreach($blogs as $b )
                        <div class="col-md-6">
                            <!-- START: Post -->
                            <div class="nk-blog-post">
                                <a href="{{ route('singleblogview') }}?blog={{$b->id + 2000}}" class="nk-post-img">
                                    <img src="/uploads/blogs-images/{{$b->image_path}}" alt="{{$b->title}}">
                                    <!-- <span class="nk-post-comments-count">4</span> -->
                                </a>
                                <div class="nk-gap"></div>
                                <h2 class="nk-post-title h4"><a href="{{ route('singleblogview') }}">{{$b->title}}</a></h2>
                                <div class="nk-post-by">
                                   {{$b->created_at->diffForHumans()}}
                                </div>
                                <div class="nk-gap"></div>
                                <div class="nk-post-text">
                                    <p>
                                        <!-- {{$b->description}} -->
                                        {{ Illuminate\Support\Str::limit($b->description, 190, '...') }}
                                    </p>
                                </div>
                                <div class="nk-gap"></div>
                                <a href="{{ route('singleblogview') }}?blog={{$b->id + 2000}}" class="nk-btn nk-btn-rounded nk-btn-color-dark-3 nk-btn-hover-color-main-1">{{ __('Leer mas') }}</a>
                            </div>
                            <!-- END: Post -->
                        </div>
                    @endforeach
                </div>
                
                <!-- START: Pagination -->
                <div class="nk-pagination nk-pagination-center">
                    {{ $blogs->links() }}
                </div>
                <!-- END: Pagination -->
            </div>
            <!-- END: Posts Grid -->

        </div>
        <div class="col-lg-4">
            <aside class="nk-sidebar nk-sidebar-right nk-sidebar-sticky">
				<div class="nk-widget nk-widget-highlighted">
					<h4 class="nk-widget-title"><span><span class="text-main-1">Top </span> Blogs</span></h4>
					<div class="nk-widget-content">
						@foreach($topblogs as $mb)
							<div class="nk-widget-post">
								<a href="{{ route('singleblogview') }}?blog={{$mb->id + 2000}}" class="nk-post-image">
									<img src="/uploads/blogs-images/{{$mb->image_path}}" alt="{{$mb->title}}">
								</a>
								<h3 class="nk-post-title"><a href="{{ route('singleblogview') }}?blog={{$mb->id + 2000}}">{{$mb->title}}</a></h3>
								<div class="nk-post-date"><span class="fa fa-calendar"></span> {{ \Carbon\Carbon::parse(date('Y-m-d', strtotime($mb->created_at)))->format('F')}} {{ date('d, Y', strtotime($mb->created_at)) }}  </div>
							</div>
						@endforeach
					</div>
				</div>


				<div class="nk-widget nk-widget-highlighted">
					<h4 class="nk-widget-title"><span><span class="text-main-1">{{__("Nuestros")}}</span> {{__("Productos")}}</span></h4>
					<div class="nk-widget-content">
					@foreach($random as $cItem)
						<div class="nk-widget-post">
							<a href="/product/{{ $cItem->id + 1000 }}" class="nk-post-image">
								<img src="{{ $cItem->images->first()['image_path'] ? asset('/images/'. $cItem->images->first()['image_path'] ) : asset('assets/images/art-not-found.jpg') }}" alt="{{ $cItem->name }}">
							</a>
							<h3 class="nk-post-title"><a href="/product/{{ $cItem->id + 1000 }}">{{ $cItem->name }}</a></h3>
							<!-- <div class="nk-product-rating" data-rating="4"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="far fa-star"></i></div> -->
							<div class="">{{ $cItem->platform }}</div>
							<div class="">{{ $cItem->region }}</div>
						</div>
					@endforeach
						
						
					</div>
				</div>

            </aside>
            <!-- END: Sidebar -->
        </div>
    </div>
</div>



@endsection

@section('content-script-include')

@endsection
@section('content-script')

@endsection
