@extends('brcode.front.layout.app')
@section('content-css-include')
    <style>
        svg:hover>#rivets,
        svg:hover>#question {
            fill: #FFC07C;
        }

        svg:hover>#borderlefttop {
            stroke: #FFC07C;
        }

        svg:active>#rivets,
        svg:active>#borderlefttop {
            fill: #000;
            stroke: #000;
        }

        svg:hover>#background,
        svg:active>#question,
        svg:active>#questionshadow {
            fill: #DE5917;
        }

        svg:hover {
            animation: jump .2s;
        }


        @keyframes jump {
            50% {
                transform: translatey(-10px);
            }

            to {
                transform: translatey(0);
            }
        }
    </style>

    @livewireStyles
@endsection

@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        @if ($viewData['category_page_info'] == 'faqs')
            <div class="header-area" id="headerArea">
                <div class="container h-100 d-flex align-items-center justify-content-between">
                    <!-- Back Button-->
                    <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                    <!-- Page Title-->
                    <div class="page-heading">
                        <h6 class="mb-0 font-extrabold"> Preguntas y Respuestas</h6>
                    </div>
                    <!-- Navbar Toggler-->

                    @if (Auth::user())
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @else
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @endif
                </div>
            </div>
        @elseif ($viewData['category_page_info'] == 'terminos-y-condiciones')
            <div class="header-area" id="headerArea">
                <div class="container h-100 d-flex align-items-center justify-content-between">
                    <!-- Back Button-->
                    <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                    <!-- Page Title-->
                    <div class="page-heading">
                        <h6 class="mb-0 font-extrabold">Términos y Condiciones</h6>
                    </div>
                    <!-- Navbar Toggler-->

                    @if (Auth::user())
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @else
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @endif
                </div>
            </div>
        @elseif ($viewData['category_page_info'] == 'condiciones-creadores-de-contenido')
            <div class="header-area" id="headerArea">
                <div class="container h-100 d-flex align-items-center justify-content-between">
                    <!-- Back Button-->
                    <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                    <!-- Page Title-->
                    <div class="page-heading">
                        <h6 class="mb-0 font-extrabold">
                            @foreach ($viewData['footer_pages'] as $key)
                                @if ($viewData['category_page_info'] == $key->url)
                                    {!! $key->title !!}
                                @endif
                            @endforeach
                        </h6>
                    </div>
                    <!-- Navbar Toggler-->

                    @if (Auth::user())
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @else
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @endif
                </div>
            </div>
        @elseif ($viewData['category_page_info'] == 'aviso-legal')
            <div class="header-area" id="headerArea">
                <div class="container h-100 d-flex align-items-center justify-content-between">
                    <!-- Back Button-->
                    <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                    <!-- Page Title-->
                    <div class="page-heading">
                        <h6 class="mb-0 font-extrabold">
                            @foreach ($viewData['footer_pages'] as $key)
                                @if ($viewData['category_page_info'] == $key->url)
                                    {!! $key->title !!}
                                @endif
                            @endforeach
                        </h6>
                    </div>
                    <!-- Navbar Toggler-->

                    @if (Auth::user())
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @else
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @endif
                </div>
            </div>
        @elseif ($viewData['category_page_info'] == 'politica-de-privacidad')
            <div class="header-area" id="headerArea">
                <div class="container h-100 d-flex align-items-center justify-content-between">
                    <!-- Back Button-->
                    <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                    <!-- Page Title-->
                    <div class="page-heading">
                        <h6 class="mb-0 font-extrabold">
                            @foreach ($viewData['footer_pages'] as $key)
                                @if ($viewData['category_page_info'] == $key->url)
                                    {!! $key->title !!}
                                @endif
                            @endforeach
                        </h6>
                    </div>
                    <!-- Navbar Toggler-->

                    @if (Auth::user())
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @else
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @endif
                </div>
            </div>
        @elseif ($viewData['category_page_info'] == 'politica-de-cookies')
            <div class="header-area" id="headerArea">
                <div class="container h-100 d-flex align-items-center justify-content-between">
                    <!-- Back Button-->
                    <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                    <!-- Page Title-->
                    <div class="page-heading">
                        <h6 class="mb-0 font-extrabold">
                            @foreach ($viewData['footer_pages'] as $key)
                                @if ($viewData['category_page_info'] == $key->url)
                                    {!! $key->title !!}
                                @endif
                            @endforeach
                        </h6>
                    </div>
                    <!-- Navbar Toggler-->

                    @if (Auth::user())
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @else
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @endif
                </div>
            </div>
        @else
            @foreach ($viewData['footer_pages'] as $key)
                @if ($viewData['category_page_info'] == $key->url)
                    <div class="header-area" id="headerArea">
                        <div class="container h-100 d-flex align-items-center justify-content-between">
                            <!-- Back Button-->
                            <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                            <!-- Page Title-->
                            <div class="page-heading">
                                <h6 class="mb-0 font-extrabold"> {!! $key->title !!}</h6>
                            </div>
                            <!-- Navbar Toggler-->

                            @if (Auth::user())
                                <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                                    data-bs-target="#sidebarPanel">
                                    <span></span><span></span><span></span>
                                </div>
                            @else
                                <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                                    data-bs-target="#sidebarPanel">
                                    <span></span><span></span><span></span>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
            @endforeach
        @endif
        @if ($viewData['category_page_info'] == 'faqs')
            <div id="appCapsule">
                <div class="section mt-2">
                    @foreach ($viewData['footer_faqs'] as $key)
                        <h4>{!! $key->question !!}</h4>

                        {!! $key->answer !!}
                    @endforeach
                </div>
            </div>
        @endif


        <div id="appCapsule">
            <div class="section mt-2">
                @foreach ($viewData['footer_pages'] as $key)
                    @if ($viewData['category_page_info'] == $key->url)
                        <p>{!! $key->content !!}</p>
                    @endif
                @endforeach
            </div>
        </div>
    @else
        <div class="container">
            <div class="row">
                <div class="bg-white col-md">
                    <div class="nk-gap-3"></div>
                    @if ($viewData['category_page_info'] == 'faqs')
                        <center>
                            <h2 class="text-gray-900 retro">- <svg width="76px" height="76px" viewBox="0 0 76 76"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path id="greyshadow" d="M8 8h68v68h-68z" fill="#BFBFBF" />
                                    <path id="blackborder" d="M4 4h68v68h-68z" fill="#000" />
                                    <path id="background" d="M4 4h64v64h-64z" fill="#FFC07C" />
                                    <path id="borderlefttop" d="M4 0h64M0 4v64" stroke="#DE5917" stroke-width="8" />
                                    <path id="rivets" d="M8 8h4v4h-4zM60 60h4v4h-4zM8 60h4v4h-4zM60 8h4v4h-4z"
                                        fill="#000" />
                                    <path id="questionshadow"
                                        d="M24 20h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM36 52h8v8h-8z"
                                        fill="#000" />
                                    <path id="question"
                                        d="M20 16h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM32 48h8v8h-8z"
                                        fill="#DE5917" />
                                </svg>
                                Preguntas y Respuestas <svg width="76px" height="76px" viewBox="0 0 76 76"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path id="greyshadow" d="M8 8h68v68h-68z" fill="#BFBFBF" />
                                    <path id="blackborder" d="M4 4h68v68h-68z" fill="#000" />
                                    <path id="background" d="M4 4h64v64h-64z" fill="#FFC07C" />
                                    <path id="borderlefttop" d="M4 0h64M0 4v64" stroke="#DE5917" stroke-width="8" />
                                    <path id="rivets" d="M8 8h4v4h-4zM60 60h4v4h-4zM8 60h4v4h-4zM60 8h4v4h-4z"
                                        fill="#000" />
                                    <path id="questionshadow"
                                        d="M24 20h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM36 52h8v8h-8z"
                                        fill="#000" />
                                    <path id="question"
                                        d="M20 16h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM32 48h8v8h-8z"
                                        fill="#DE5917" />
                                </svg> -
                            </h2>
                        </center>
                    @elseif ($viewData['category_page_info'] == 'terminos-y-condiciones')
                        <center>
                            <h2 class="text-gray-900 text-2xl retro">- <i class="nes-icon is-medium star"></i>
                                @foreach ($viewData['footer_pages'] as $key)
                                    @if ($viewData['category_page_info'] == $key->url)
                                        {!! $key->title !!}
                                    @endif
                                @endforeach
                                <i class="nes-icon is-medium star"></i> -
                            </h2>
                        </center>
                    @elseif ($viewData['category_page_info'] == 'condiciones-creadores-de-contenido')
                        <center>
                            <h2 class="text-gray-900 text-2xl retro">- <i class="nes-icon is-medium star"></i>
                                @foreach ($viewData['footer_pages'] as $key)
                                    @if ($viewData['category_page_info'] == $key->url)
                                        {!! $key->title !!}
                                    @endif
                                @endforeach
                                <i class="nes-icon is-medium star"></i> -
                            </h2>
                        </center>
                    @elseif ($viewData['category_page_info'] == 'aviso-legal')
                        <center>
                            <h2 class="text-gray-900 text-2xl retro">- <img loading="lazy" class="w-20"
                                    src="{{ asset('img/icon/warning.png') }}">
                                @foreach ($viewData['footer_pages'] as $key)
                                    @if ($viewData['category_page_info'] == $key->url)
                                        {!! $key->title !!}
                                    @endif
                                @endforeach
                                <img loading="lazy" class="w-20" src="{{ asset('img/icon/warning.png') }}"> -
                            </h2>
                        </center>
                    @elseif ($viewData['category_page_info'] == 'politica-de-privacidad')
                        <center>
                            <h2 class="text-gray-900 text-2xl retro">- <img loading="lazy" class="w-20"
                                    src="{{ asset('img/icon/warning.png') }}">
                                @foreach ($viewData['footer_pages'] as $key)
                                    @if ($viewData['category_page_info'] == $key->url)
                                        {!! $key->title !!}
                                    @endif
                                @endforeach
                                <img loading="lazy" class="w-20" src="{{ asset('img/icon/warning.png') }}"> -
                            </h2>
                        </center>
                    @elseif ($viewData['category_page_info'] == 'politica-de-cookies')
                        <center>
                            <h2 class="text-gray-900 text-2xl retro">- <img loading="lazy" class="w-20"
                                    src="{{ asset('img/icon/cookie.png') }}">
                                @foreach ($viewData['footer_pages'] as $key)
                                    @if ($viewData['category_page_info'] == $key->url)
                                        {!! $key->title !!}
                                    @endif
                                @endforeach
                                <img loading="lazy" class="w-20" src="{{ asset('img/icon/cookie.png') }}"> -
                            </h2>
                        </center>
                    @else
                        <center>
                            <h2 class="text-gray-900 text-2xl retro">- <i class="nes-icon is-medium star"></i>
                                @foreach ($viewData['footer_pages'] as $key)
                                    @if ($viewData['category_page_info'] == $key->url)
                                        {!! $key->title !!}
                                    @endif
                                @endforeach
                                <i class="nes-icon is-medium star"></i> -
                            </h2>
                        </center>
                    @endif
                    @if ($viewData['category_page_info'] == 'faqs')
                        @foreach ($viewData['footer_faqs'] as $key)
                            <br>
                            <h5 class="text-xs font-bold text-gray-900 retro">{!! $key->question !!}</h5>
                            {!! $key->answer !!}
                        @endforeach
                    @endif

                    @foreach ($viewData['footer_pages'] as $key)
                        @if ($viewData['category_page_info'] == $key->url)
                            <p>{!! $key->content !!}</p>
                        @endif
                    @endforeach




                </div>
            </div>
        </div>
    @endif
@endsection

@section('content-script-include')
    @livewireScripts

    <script>
        Livewire.on('prueba', function() {
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
        })


        Livewire.on('clearFilters', function() {
            $('#search_country').val('').trigger('change.select2');
        });
    </script>

    <!-- Flickity -->
    <script src="{{ asset('assets/vendor/flickity/dist/flickity.pkgd.min.js') }}"></script>

    <!-- Hammer.js -->
    <script src="{{ asset('assets/vendor/hammerjs/hammer.min.js') }}"></script>
@endsection
