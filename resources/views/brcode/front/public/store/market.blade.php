@extends('brcode.front.layout.app')
@section('content-css-include')
    @livewireStyles
    <style>
        .retro {
            font-family: 'Jost', sans-serif;
            font-weight: 800;
        }
    </style>
@endsection
@section('content')


    <div>
        <div class="mt-20 wishlist-table-area mb-50">
            <div class="container">
                <div class="row justify-content-lg-center">

                    <div class="col-lg-12">
                        <ul class="nk-breadcrumbs">
                            <br>
                            <li>
                                <div class="section-title1">
                                    <h3 class="retro" style="font-size:20px;">@lang('messages.advance_search')</h3>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="my-6 bg-white rounded shadow-md">
                            <div class="nk-form nk-form-style-1">
                                <div class="mt-20 col-12">
                                    <center>
                                        <div class="mt-20 col-12">
                                            <h4 class="text-gray-900 retro"> @lang('messages.advance_filter') </h4>
                                        </div>
                                    </center>
                                </div>
                                <div class="nk-tabs">
                                    <ul class="nav nav-tabs" role="tablist">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#available" role="tab" data-toggle="tab"><span
                                                    class="retro">@lang('messages.advance_products_available')
                                                </span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#allproducts" role="tab" data-toggle="tab"><span
                                                    class="retro">@lang('messages.advance_products')</span></a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade show active" id="available">
                                            @livewire('store-avaiable')
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="allproducts">
                                            @livewire('store')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content-script')
    @livewireScripts
    <script>
        var hash = window.location.hash;
        if (hash == '#allproducts') {
            $('a[href*="#allproducts"]').click();
        }
    </script>
@endsection
