@extends('brcode.front.layout.app')
@section('content-css-include')
    <style>
        .tab {
            overflow: hidden;
        }

        .nk-product-meta ul li::before {
            content: "•";
            margin-right: 0.5rem;
            color: black;
        }


        .tab-content {
            max-height: 0;
            transition: all 0.5s;
        }

        input:checked+.tab-label .test {
            background-color: #000;
        }

        input:checked+.tab-label .test svg {
            transform: rotate(180deg);
            stroke: #fff;
        }

        input:checked+.tab-label::after {
            transform: rotate(90deg);
        }

        input:checked~.tab-content {
            max-height: 100vh;
        }

        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Jost', sans-serif;
            font-weight: 800;
        }

        .w-88 {
            width: 99% !important;
        }
    </style>
@endsection
@section('content')
    @if (Auth::user())
        <div class="modal fade" id="modalComplaint" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog complaint-form" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title">¿Porque quieres denunciar este producto?</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <form action="{{ route('complaint_user_inventory', $producto->id) }}" method="POST">
                                {{ csrf_field() }}
                                <input name="user_id" type="hidden" value="{{ auth()->user()->id }}">
                                <div class="wide-block pb-1 pt-2">
                                    <div class="input-list">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" value="Imagen Equivocada"
                                                name="category" id="radioList1">
                                            <label class="form-check-label" for="radioList1">Imagen Equivocada</label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" value="Producto Falso"
                                                name="category" id="radioList2">
                                            <label class="form-check-label" for="radioList2">Producto Falso</label>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit"
                                class="btn confirmclosed btn-submit btn-danger w-100 text-lg font-extrabold">Enviar</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        @include('partials.flash')
        <div class="header-area" id="headerArea">
            <div class="container h-100 d-flex align-items-center justify-content-between">
                <!-- Back Button-->
                <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                <!-- Page Title-->
                <div class="page-heading">
                    <h6 class="mb-0 font-extrabold">@lang('messages.detail_product')</h6>
                </div>
                <!-- Navbar Toggler-->

                <div class="normal">
                    @if (Auth::user())
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @else
                        <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebarPanel">
                            <span></span><span></span><span></span>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div id="notification-5" class="notification-box">
            <div class="notification-dialog android-style">
                <div class="notification-header">
                    <div class="in">
                        <img src="{{ asset('img/RGM.png') }}"
                            alt="Notificación especial - Retrogaming Market: Alerta importante" class="imaged w24 rounded">
                        <strong>RGM</strong>
                        <span>justo ahora</span>
                    </div>
                    <a href="#" class="close-button">
                        <ion-icon name="close"></ion-icon>
                    </a>
                </div>
                <div class="notification-content">
                    <div class="in">
                        <h3 class="subtitle">Necesitas iniciar sesión para añadir productos al carrito</h3>
                        <div class="text">
                            Inicia sesión <a href="#" id="showLogin" data-nav-toggle="#nk-nav-mobile"
                                class="showLogin">
                                aquí
                            </a> o
                            Regístrate <a href="{{ url('register-user') }}">aquí </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-content-wrapper-inventory">

            <div class="card-body d-flex justify-content-between align-items-center border border-black sm:flex-col">
                <a href="{{ route('user-info', $producto->user->user_name) }}">
                    <div class="d-flex align-items-center">
                        <div class="user-profile me-3 h-16 w-16 rounded-full border-2 border-red-500">
                            @if ($producto->user->profile_picture)
                                <img src="{{ url($producto->user->profile_picture) }}" alt=""
                                    class="rounded-full w-full h-full">
                            @else
                                <img src="{{ asset('img/profile-picture-not-found.png') }}" alt=""
                                    class="rounded-full w-full h-full">
                            @endif
                        </div>
                        <div class="user-info">
                            <div class="d-flex align-items-center">
                                <h5 class="mb-1 text-lg">{{ $producto->user->user_name }}</h5>
                            </div>
                            <p class="mb-0 text-lg">{{ $producto->user->score[1] }}</p>
                        </div>
                    </div>
                </a>
                @if (Auth::user())
                    @if ($producto->user->id !== auth()->id())
                        <button
                            class="bg-red-500 hover:bg-red-600 text-white px-6 py-2 new-message font-bold rounded-full">Chat</button>
                    @endif
                @else
                    <a href="#" id="showLogin" data-nav-toggle="#nk-nav-mobile"
                        class="showLogin bg-red-500 hover:bg-red-600 text-white px-6 py-2 new-message font-bold rounded-full">
                        Chat
                    </a>
                @endif
            </div>




            <!-- END: Breadcrumbs -->
            <div class="product-slides owl-carousel rounded-lg">
                <div class="relative">
                    @if (count($producto->images) > 0)
                        <a href="{{ url('/uploads/inventory-images/' . $producto->images->first()->image_path) }}"
                            class="fancybox" data-fancybox="RGM">
                            <img class="single-product-slide"
                                src="{{ url('/uploads/inventory-images/' . $producto->images->first()->image_path) }}"
                                alt="{{ $producto->product->name }} - RetroGamingMarket">
                        </a>
                        @if (Auth::user())
                            <button id="showComplaint"
                                style="font-size: 20px; border: 1px solid rgb(200, 200, 200);
                            box-shadow: rgba(0, 0, 0, 0.1) 0px 5px 5px 2px;
                            background: rgba(14, 0, 0, 0.6);"
                                class="showComplaint absolute bottom-25 right-0  text-white p-2 rounded hover:bg-blue-800 m-2">
                                <ion-icon name="alert-circle-outline"></ion-icon>
                            </button>
                        @endif
                    @else
                        <img class="single-product-slide" loading="lazy"
                            src="{{ asset('assets/images/art-not-found.jpg') }}"
                            alt="Producto no encontrado - RetroGamingMarket">
                    @endif
                </div>

                @if (count($producto->images) > 1)
                    @foreach ($producto->images as $key)
                        @if (!$loop->first)
                            <div class="relative">
                                @if (count($producto->images) > 0)
                                    <a href="{{ '/uploads/inventory-images/' . $key->image_path }}" class="fancybox"
                                        data-fancybox="RGM">
                                        <img class="single-product-slide"
                                            src="{{ '/uploads/inventory-images/' . $key->image_path }}"
                                            alt="{{ $producto->product->name }} - RetroGamingMarket">
                                    </a>
                                    @if (Auth::user())
                                        <button id="showComplaint"
                                            style="font-size: 20px; border: 1px solid rgb(200, 200, 200);
                                        box-shadow: rgba(0, 0, 0, 0.1) 0px 5px 5px 2px;
                                        background: rgba(14, 0, 0, 0.6);"
                                            class="showComplaint absolute bottom-25 right-0  text-white p-2 rounded hover:bg-blue-800 m-2">
                                            <ion-icon name="alert-circle-outline"></ion-icon>
                                        </button>
                                    @endif
                                @endif
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>





            <div class="product-description pb-3">
                <div class="container">
                    <div class="p-title-price text-center">
                        <h2 class="mt-6 mb-2 font-extrabold">{{ $producto->product->name }}</h2>
                        <h3>{{ $producto->product->platform }} - {{ $producto->product->region }}</h3>
                        <p class="text-xl text-red-700 font-semibold mb-2">Oferta Máxima</p>
                        <div class="flex items-center justify-center mb-2">
                            <h2
                                class="text-center bg-gradient-to-r from-red-600 to-red-800 text-white px-4 py-2 rounded-lg">
                                {{ number_format($producto->max_bid, 2) . ' €' }}

                                @php($qty_det = 0)
                                @php($det_total = 0)
                                @php($iva = 0)
                                @php($det_vol = 0)
                                @php($det_wei = 0)
                                @php($totalWeight = 0)
                                @php($totalSize = 0)
                                @php($total = 0)
                                @php($shipping_cost = 0)

                                @php($det_total += $producto->quantity * $producto->max_bid)
                                @php($iva = $det_total * 0.04)

                                @if ($producto->kg !== null)
                                    @php($det_wei += $producto->quantity * $producto->kg)
                                    @php($totalWeight += $producto->quantity * ($producto->kg / 1000))
                                @else
                                    @php($det_wei += $producto->quantity * $producto->product->weight)
                                    @php($totalWeight += $producto->quantity_sold * ($producto->product->weight / 1000))
                                @endif

                                @if ($producto->size === null)
                                    @if (isset($producto->product->width) && isset($producto->product->large) && isset($producto->product->high))
                                        @php($totalSize += $producto->quantity * ($producto->product->width + $producto->product->large + $producto->product->high))
                                    @endif
                                @elseif ($producto->size == 0)
                                    @php($totalSize += $producto->quantity * (15 + 15 + 20))
                                @elseif ($producto->size == 1)
                                    @php($totalSize += $producto->quantity * (20 + 20 + 30))
                                @elseif ($producto->size == 2)
                                    @php($totalSize += $producto->quantity * (25 + 25 + 45))
                                @elseif ($producto->size == 3)
                                    @php($totalSize += $producto->quantity * (45 + 45 + 30))
                                @elseif ($producto->size == 4)
                                    @php($totalSize += $producto->quantity * (50 + 50 + 50))
                                @endif

                                <div class="text-xs">+ @if ($totalWeight >= 0 && $totalWeight <= 1 && $totalSize >= 0 && $totalSize <= 50)
                                        2.90 €
                                    @elseif($totalWeight > 1 && $totalWeight <= 3 && $totalSize <= 50)
                                        3.90 €
                                    @elseif($totalWeight > 3 && $totalWeight <= 5 && $totalSize <= 95)
                                        4.90 €
                                    @elseif($totalWeight > 5 && $totalWeight <= 10 && $totalSize <= 120)
                                        6.90 €
                                    @elseif($totalWeight > 10 && $totalWeight <= 20 && $totalSize <= 150)
                                        10.90 €
                                    @elseif($totalWeight >= 0 && $totalWeight <= 1)
                                        @if ($totalSize >= 0 && $totalSize <= 50)
                                            2.90 €
                                        @elseif($totalSize >= 50 && $totalSize <= 70)
                                            3.90 €
                                        @elseif($totalSize >= 70 && $totalSize <= 95)
                                            4.90 €
                                        @elseif($totalSize >= 95 && $totalSize <= 120)
                                            6.90 €
                                        @elseif($totalSize >= 120 && $totalSize <= 150)
                                            10.90 €
                                        @endif
                                    @elseif($totalWeight > 1 && $totalWeight <= 3)
                                        @if ($totalSize >= 0 && $totalSize <= 50)
                                            3.90 €
                                        @elseif($totalSize >= 50 && $totalSize <= 70)
                                            3.90 €
                                        @elseif($totalSize >= 70 && $totalSize <= 95)
                                            4.90 €
                                        @elseif($totalSize >= 95 && $totalSize <= 120)
                                            6.90 €
                                        @elseif($totalSize >= 120 && $totalSize <= 150)
                                            10.90 €
                                        @endif
                                    @elseif($totalWeight > 3 && $totalWeight <= 5)
                                        @if ($totalSize >= 0 && $totalSize <= 50)
                                            4.90 €
                                        @elseif($totalSize >= 50 && $totalSize <= 70)
                                            4.90 €
                                        @elseif($totalSize >= 70 && $totalSize <= 95)
                                            4.90 €
                                        @elseif($totalSize >= 95 && $totalSize <= 120)
                                            6.90 €
                                        @elseif($totalSize >= 120 && $totalSize <= 150)
                                            10.90 €
                                        @endif
                                    @elseif($totalWeight > 5 && $totalWeight <= 10)
                                        @if ($totalSize >= 0 && $totalSize <= 50)
                                            6.90 €
                                        @elseif($totalSize >= 50 && $totalSize <= 70)
                                            6.90 €
                                        @elseif($totalSize >= 70 && $totalSize <= 95)
                                            6.90 €
                                        @elseif($totalSize >= 95 && $totalSize <= 120)
                                            6.90 €
                                        @elseif($totalSize >= 120 && $totalSize <= 150)
                                            10.90 €
                                        @endif
                                    @elseif($totalWeight > 10 && $totalWeight <= 20)
                                        @if ($totalSize >= 0 && $totalSize <= 50)
                                            10.90 €
                                        @elseif($totalSize >= 50 && $totalSize <= 70)
                                            10.90 €
                                        @elseif($totalSize >= 70 && $totalSize <= 95)
                                            10.90 €
                                        @elseif($totalSize >= 95 && $totalSize <= 120)
                                            10.90 €
                                        @elseif($totalSize >= 120 && $totalSize <= 150)
                                            10.90 €
                                        @endif
                                    @endif de envío</div>
                            </h2>
                        </div>
                        <div class="flex items-center justify-center text-sm text-gray-500 space-x-8">
                            <p class="ml-4 mb-1">Tiempo restante: <span
                                    class="font-semibold">{{ $producto->countdown_hours }} horas</span></p>
                            <p class="mb-1">Última oferta por: <span class="font-semibold">

                                    @if ($producto->user_bid > 0)
                                        {{ $producto->userbid->user_name }}
                                    @else
                                        -
                                    @endif
                                </span></p>
                        </div>



                        @if (Auth::user())
                            @if ($producto->user_bid > 0)
                                @if ($producto->user_id === auth()->user()->id)
                                    <div class="flex items-center justify-center mt-4 space-x-4">
                                        <button class="bg-green-600 hover:bg-green-700 text-white px-4 py-2 rounded-full"
                                            type="button" data-toggle="modal" data-target="#offerModal">
                                            Aceptar Oferta
                                        </button>

                                        <button class="bg-red-600 hover:bg-red-700 text-white px-4 py-2 rounded-full"
                                            type="button" data-toggle="modal" data-target="#denyModal">
                                            Denegar Oferta
                                        </button>
                                    </div>
                                @endif
                            @endif
                        @endif
                    </div>
                </div>
            </div>





        </div>


        <div class="product-description pb-3">

            <div class="p-specification bg-white">

                <div class="container">
                    <div class="p-title-price text-center">





                        <h5 class="mt-2 mb-2 font-extrabold"> @lang('messages.general_price') </h5>

                        <?php
                        $duration = [];
                        $duration_not_press = [];
                        $duration_used = [];
                        $duration_new = [];
                        $duration_general = [];
                        $t = '';
                        $total_numeros = '';
                        $total_numeros_not_press = '';
                        $total_numeros_used = '';
                        $total_numeros_new = '';
                        $total_numeros_general = '';
                        $suma = '';
                        $suma_not_press = '';
                        $suma_used = '';
                        $suma_new = '';
                        $suma_general = '';
                        $m = '';
                        $promedio_not_press = '';
                        $promedio_used = '';
                        $promedio_new = '';
                        $promedio_general = '';
                        $prueba = '';
                        $fecha = '';
                        $precio = '';
                        
                        foreach ($producto->product->inventory as $item) {
                            $cantidad = $item->price;
                            $duration[] = $cantidad;
                            $total_numeros = count($duration);
                            $total = max($duration);
                            $t = min($duration);
                            $suma = array_sum($duration);
                            $m = $suma / $total_numeros;
                            $prueba = $item->created_at;
                        }
                        
                        //PROMEDIO GENERAL
                        foreach ($seller_sold_general as $p) {
                            $cantidad_general = $p->price;
                            $duration_general[] = $cantidad_general;
                            $total_numeros_general = count($duration_general);
                            $suma_general = array_sum($duration_general);
                            $promedio_general = $suma_general / $total_numeros_general;
                        }
                        
                        //PROMEDIO PARA LAS CONDICIONES TIPO "NOT PRESS - NO TIENE" POKEMON GOLD ES EL QUE NO TIENE ASTERIX Y OBELIS TAMPOCO TIENEN
                        foreach ($seller_sold_not_pres as $p) {
                            $cantidad_press = $p->price;
                            $duration_not_press[] = $cantidad_press;
                            $total_numeros_not_press = count($duration_not_press);
                            $suma_not_press = array_sum($duration_not_press);
                            $promedio_not_press = $suma_not_press / $total_numeros_not_press;
                        }
                        
                        //PROMEDIO PARA LAS CONDICIONES TIPO "USED" WOODY ES EL QUE ESTA USADO
                        foreach ($seller_sold_used as $p_used) {
                            $cantidad_used = $p_used->price;
                            $duration_used[] = $cantidad_used;
                            $total_numeros_used = count($duration_used);
                            $suma_used = array_sum($duration_used);
                            $promedio_used = $suma_used / $total_numeros_used;
                        }
                        
                        //PROMEDIO PARA LAS CONDICIONES TIPO "NEW" XG2 ES EL QUE TIENE EL ESTADO NUEVO
                        foreach ($seller_sold_new as $p_new) {
                            $cantidad_new = $p_new->price;
                            $duration_new[] = $cantidad_new;
                            $total_numeros_new = count($duration_new);
                            $suma_new = array_sum($duration_new);
                            $promedio_new = $suma_new / $total_numeros_new;
                        }
                        
                        ?>

                        <?php
                        $l = $producto->price_solo + $producto->price_used + $producto->price_new / 3;
                        ?>


                        @if ($promedio_general > 0)
                            <div class="row">
                                <div class="col flex items-center justify-center">
                                    <div class="total-result-of-ratings text-center">
                                        <span>@lang('messages.new_inventory')</span> <!-- Título arriba -->
                                        <br>
                                        <span class="inline-block">
                                            @if ($promedio_new > 0)
                                                {{ number_format($promedio_new, 2) }} €
                                            @elseif($producto->product->price_new)
                                                {{ number_format($producto->product->price_new, 2) }} €
                                            @else
                                                -
                                            @endif
                                        </span>
                                    </div>
                                </div>
                                <div class="col flex items-center justify-center">
                                    <div class="total-result-of-ratings-used text-center">
                                        <span>@lang('messages.used_inventory')</span> <!-- Título arriba -->
                                        <br>
                                        <span class="inline-block">
                                            @if ($promedio_used > 0)
                                                {{ number_format($promedio_used, 2) }} €
                                            @elseif($producto->product->price_used)
                                                {{ number_format($producto->product->price_used, 2) }} €
                                            @else
                                                -
                                            @endif
                                        </span>
                                    </div>
                                </div>
                                <div class="col flex items-center justify-center">
                                    <div class="total-result-of-ratings-solo text-center">
                                        <span>@lang('messages.only_inventory')</span> <!-- Título arriba -->
                                        <br>
                                        <span class="inline-block">
                                            @if ($promedio_not_press > 0)
                                                {{ number_format($promedio_not_press, 2) }} €
                                            @elseif($producto->product->price_solo)
                                                {{ number_format($producto->product->price_solo, 2) }} €
                                            @else
                                                -
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="row">
                                <div class="col flex items-center justify-center">
                                    <div class="total-result-of-ratings text-center">
                                        <span>@lang('messages.new_inventory')</span> <!-- Título arriba -->
                                        <br>
                                        <span class="inline-block">

                                            -

                                        </span>
                                    </div>
                                </div>
                                <div class="col flex items-center justify-center">
                                    <div class="total-result-of-ratings-used text-center">
                                        <span>@lang('messages.used_inventory')</span> <!-- Título arriba -->
                                        <br>
                                        <span class="inline-block">
                                            -
                                        </span>
                                    </div>
                                </div>
                                <div class="col flex items-center justify-center">
                                    <div class="total-result-of-ratings-solo text-center">
                                        <span>@lang('messages.only_inventory')</span> <!-- Título arriba -->
                                        <br>
                                        <span class="inline-block">
                                            -
                                        </span>
                                    </div>
                                </div>
                            </div>
                        @endif


                        <div class="divider mt-3 mb-3"></div>

                        <h5 class="mb-1 font-extrabold">Estados</h5>
 
                        @if ($producto->product->catid == 1)
                            <div class="flex items-center justify-center py-2 mx-auto text-center">
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span>Caja</span>
                                    <br>
                                    <img class="w-6 h-6 md:w-8 md:h-8 mx-auto" src="/{{ $producto->box }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->box_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span>Carátula</span>
                                    <br>
                                    <img class="w-6 h-6 md:w-8 md:h-8 mx-auto" src="/{{ $producto->cover }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->cover_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span>Manual</span>
                                    <br>
                                    <img class="w-6 h-6 md:w-8 md:h-8 mx-auto" src="/{{ $producto->manual }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->manual_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span>Juegos</span>
                                    <br>
                                    <img class="w-6 h-6 md:w-8 md:h-8 mx-auto" src="/{{ $producto->game }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->game_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span>Extras</span>
                                    <br>
                                    <img class="w-6 h-6 md:w-8 md:h-8 mx-auto" src="/{{ $producto->extra }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->extra_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                            </div>
                        @elseif ($producto->product->catid == 2)
                            <div class="flex items-center justify-center py-2 mx-auto">
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span>Caja</span>
                                    <br>
                                    <img class="w-6 h-6 md:w-8 md:h-8 mx-auto" src="/{{ $producto->box }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->box_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span>Carátula</span>
                                    <br>
                                    <img class="w-6 h-6 md:w-8 md:h-8 mx-auto" src="/{{ $producto->cover }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->cover_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span>Manual</span>
                                    <br>
                                    <img class="w-6 h-6 md:w-8 md:h-8 mx-auto" src="/{{ $producto->manual }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->manual_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span>Estado</span>
                                    <br>
                                    <img class="w-6 h-6 md:w-8 md:h-8 mx-auto" src="/{{ $producto->game }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->game_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span>Extras</span>
                                    <br>
                                    <img class="w-6 h-6 md:w-8 md:h-8 mx-auto" src="/{{ $producto->extra }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->extra_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                            </div>
                        @else
                            <div class="flex items-center justify-center py-2 mx-auto">
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span class="pl-2">Caja</span>
                                    <br>
                                    <img class="w-6 h-6 md:w-8 md:h-8 mx-auto" src="/{{ $producto->box }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->box_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span class="pl-2">Juegos</span>
                                    <br>
                                    <img class="w-6 h-6 md:w-8 md:h-8 mx-auto" src="/{{ $producto->game }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->game_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span class="pl-2">Extras</span>
                                    <br>
                                    <img class="w-6 h-6 md:w-8 md:h-8 mx-auto" src="/{{ $producto->extra }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->extra_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                            </div>
                        @endif



                        <h5 class="mb-1 font-extrabold"> Comentario </h5>

                    </div>



                    <span class="font-normal py-2">
                        @if ($producto->comments)
                            {{ $producto->comments }}
                        @else
                            Sin comentarios
                        @endif
                    </span>
                </div>
            </div>
        </div>






        <div class="pt-3"></div>

        <div class="bg-gray-100 p-4 rounded-lg">
            <h2 class="flex items-center mb-2">
                <i class="mr-2 text-gray-600 fas fa-shipping-fast"></i> Envíos
            </h2>
            <div class="bg-white p-4 rounded-lg border border-solid border-gray-300">
                <p class="mb-2 text-lg"><i class="mr-2 text-green-500 fas fa-check"></i> En oficina de correos: Desde
                    2,90€
                </p>
                <p class="mb-2 text-lg"><i class="mr-2 text-green-500 fas fa-check"></i> En mi dirección: Desde 5.90€</p>
                <p class="mb-2 text-lg"><i class="mr-2 text-green-500 fas fa-check"></i> Seguro de envío incluido en el
                    precio
                </p>
            </div>

            <h2 class="flex items-center mt-4 mb-2">
                <i class="mr-2 text-gray-600 fas fa-lock"></i> Compra con seguridad
            </h2>
            <div class="bg-white p-4 rounded-lg border border-solid border-gray-300">
                <p class="mb-2 text-lg"><i class="mr-2 text-green-500 fas fa-check"></i> Pago seguro a través de <img
                        src="https://files2.soniccdn.com/files/2021/03/16/beseif-logo.png" class="mb-2"
                        style="display: inline;">
                </p>
                <p class="text-lg"><i class="mr-2 text-green-500 fas fa-check"></i> Garantía de devolución</p>
            </div>
        </div>



        <div class="mt-3 mb-3"></div>

        <div class="product-description pb-3">
            <div class="flex justify-center items-center">
                <!-- Compartir -->
                <div class="text-center flex-1">
                    <div>
                        <h5 class="mb-1 font-extrabold">Compartir</h5>
                        <div class="mt-2">
                            <a href="https://wa.me/?text= http://68.183.38.211/product/inventory/17233" id="wha-btn"
                                class="btn btn-icon mr-2 btn-sm btn-whatsapp">
                                <i class="fab fa-whatsapp"></i>
                            </a>
                            <a href="https://telegram.me/share/url?url=http://68.183.38.211/product/inventory/17233&amp;text="
                                id="tele-btn" class="btn btn-icon btn-sm btn-telegram">
                                <i class="fab fa-telegram ml-80 mt-90"></i>
                            </a>
                        </div>

                    </div>
                </div>

                <!-- Línea vertical -->
                <div class="border-l border-black h-10 mx-2"></div>

                <!-- Comprar -->
                @if (Auth::user())

                    @if ($producto->user_id === auth()->user()->id)
                        <div class="text-center flex-1">
                            <div>
                                <h5 class="mb-1 font-extrabold">-</h5>
                                <div class="mt-2">
                                    <div class="flex justify-center">
                                        @if (Auth::user())


                                            @if ($producto->user_bid === auth()->user()->id)
                                                <div class="text-sm text-black-900">
                                                    Eres el propietario de la oferta máxima en este momento. No es
                                                    necesario hacer más ofertas.
                                                </div>
                                            @else
                                                @if ($producto->user_id != auth()->user()->id)
                                                    <div class="flex items-center">
                                                        <span class="font-extrabold">
                                                            {{ __('Cantidad') }}:
                                                        </span>
                                                        <span data-value="{{ $producto->quantity }}"
                                                            class="font-extrabold">
                                                            &nbsp; {{ $producto->quantity }}
                                                        </span>
                                                        <button class="btn-sm h-10 w-10 btn-danger ml-2" type="button"
                                                            data-toggle="modal" data-target="#auctionModal">
                                                            <i class="fas fa-gavel"></i>
                                                        </button>
                                                    </div>
                                                @else
                                                    <div class="flex items-center">
                                                        <span class="font-extrabold">
                                                            {{ __('Cantidad') }}:
                                                        </span>
                                                        <span data-value="{{ $producto->quantity }}"
                                                            class="font-extrabold">
                                                            &nbsp; {{ $producto->quantity }}
                                                        </span>
                                                    </div>
                                                    <!-- Mensaje destacado para el propietario de la puja máxima -->
                                                @endif
                                            @endif
                                        @else
                                            <span class="font-extrabold">
                                                {{ __('Cantidad') }}:
                                            </span>
                                            <span data-value="{{ $producto->quantity }}" class="font-extrabold">
                                                {{ $producto->quantity }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="text-center flex-1">
                            <div>
                                <h5 class="mb-1 font-extrabold">Hacer Oferta</h5>
                                <div class="mt-2">
                                    <div class="flex justify-center">
                                        @if (Auth::user())


                                            @if ($producto->user_bid === auth()->user()->id)
                                                <div class="text-sm text-black-900">
                                                    Eres el propietario de la oferta máxima en este momento. No es
                                                    necesario hacer más ofertas.
                                                </div>
                                            @else
                                                @if ($producto->user_id != auth()->user()->id)
                                                    <div class="flex items-center">
                                                        <span class="font-extrabold">
                                                            {{ __('Cantidad') }}:
                                                        </span>
                                                        <span data-value="{{ $producto->quantity }}"
                                                            class="font-extrabold">
                                                            &nbsp; {{ $producto->quantity }}
                                                        </span>
                                                        <button class="btn-sm h-10 w-10 btn-danger ml-2" type="button"
                                                            data-toggle="modal" data-target="#auctionModal">
                                                            <i class="fas fa-gavel"></i>
                                                        </button>
                                                    </div>
                                                @else
                                                    <div class="flex items-center">
                                                        <span class="font-extrabold">
                                                            {{ __('Cantidad') }}:
                                                        </span>
                                                        <span data-value="{{ $producto->quantity }}"
                                                            class="font-extrabold">
                                                            &nbsp; {{ $producto->quantity }}
                                                        </span>
                                                        <a href="{{ route('inventory_edit', $producto->id) }}"
                                                            class="btn-sm h-10 w-10 btn-warning ml-2">
                                                            <i class="fa-solid fa-pen-to-square"></i>
                                                        </a>
                                                    </div>
                                                    <!-- Mensaje destacado para el propietario de la puja máxima -->
                                                @endif
                                            @endif
                                        @else
                                            <span class="font-extrabold">
                                                {{ __('Cantidad') }}:
                                            </span>
                                            <span data-value="{{ $producto->quantity }}" class="font-extrabold">
                                                {{ $producto->quantity }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @else
                    <div class="text-center flex-1">
                        <div>
                            <h5 class="mb-1 font-extrabold">Hacer Oferta</h5>
                            <div class="mt-3">

                                <span class="font-extrabold">
                                    {{ __('Cantidad') }}:
                                </span>
                                <span data-value="{{ $producto->quantity }}" class="font-extrabold">
                                    {{ $producto->quantity }}
                                </span>
                                <button type="button" class="btn-sm h-10 w-10 btn-danger ml-2"
                                    onclick="notification('notification-5')"><i class="fas fa-gavel"></i></button>


                            </div>
                        </div>
                    </div>
                @endif

            </div>

 
            <!-- Anuncios de perfil en RGM -->
        </div>



        <div class="modal fade" id="auctionModal" tabindex="-1" role="dialog" aria-labelledby="auctionModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title" id="addnewcontactlabel">Hacer oferta :</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block pb-1 pt-2">
                                @if ($producto->max_bid == 1)
                                    <p class="text-center mb-4">¡Sé el primero en hacer una oferta!</p>
                                @else
                                    <p class="text-center text-base mb-2">Última oferta realizada por: Adri</p>
                                    <p class="text-center text-lg text-red-500 font-semibold">Precio actual de la oferta:
                                        {{ number_format($producto->max_bid, 2) . ' €' }}
                                    </p>
                                @endif

                                <form action="{{ route('MakeAuction', $producto->id) }}" method="POST"
                                    enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Por cuánto quieres hacer la
                                                oferta</label>
                                            <input name="user_bid_price" type="number" min="1"
                                                class="form-control">
                                            <i class="clear-input">
                                                <ion-icon name="close-circle"></ion-icon>
                                            </i>
                                        </div>
                                    </div>
                                    <br>
                                    <button type="submit"
                                        class="btn confirmclosed btn-submit btn-danger w-100 text-lg font-extrabold">Hacer
                                        oferta</button>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if ($producto->user_bid > 0)
            <div class="modal fade" id="offerModal" tabindex="-1" role="dialog" aria-labelledby="offerModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content bg-white">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="d-flex align-items-center justify-content-between mb-4">
                                    <h4 class="modal-title" id="addnewcontactlabel">Aceptar oferta de
                                        {{ $producto->userbid->user_name }}:</h4>
                                    <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="wide-block pb-1 pt-2">
                                    <p class="text-center text-lg font-semibold mb-2">¿Estás seguro de aceptar esta oferta?
                                        Ya no recibirás más ofertas y comenzará el proceso de venta normal.</p>
                                    <p class="text-center text-lg text-red-500 font-semibold">Precio actual de la oferta:
                                        {{ number_format($producto->max_bid, 2) . ' €' }} <br>
                                    </p>


                                    <form action="{{ route('AcceptOffer', $producto->id) }}" method="POST"
                                        enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}

                                        <button type="submit"
                                            class="btn confirmclosed btn-submit btn-danger w-100 text-lg font-extrabold">Aceptar
                                            oferta</button>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="denyModal" tabindex="-1" role="dialog" aria-labelledby="denyModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content bg-white">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="d-flex align-items-center justify-content-between mb-4">
                                    <h4 class="modal-title" id="addnewcontactlabel">Denegar oferta de
                                        {{ $producto->userbid->user_name }}:</h4>
                                    <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="wide-block pb-1 pt-2">
                                    <p class="text-center text-lg font-semibold mb-2">¿Estás seguro de denegar esta oferta?
                                       El precio de la oferta actual sera considerada la inicial a partir de ahora.</p>
                                    <p class="text-center text-lg text-red-500 font-semibold">Precio actual de la oferta:
                                        {{ number_format($producto->max_bid, 2) . ' €' }} <br>
                                    </p>


                                    <form action="{{ route('DenyOffer', $producto->id) }}" method="POST"
                                        enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}

                                        <button type="submit"
                                            class="btn confirmclosed btn-submit btn-danger w-100 text-lg font-extrabold">Denegar
                                            oferta</button>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif



        <br>
        <br>
        <br>




        </div>

        @if (Auth::user())
            <div class="modal fade" id="modalNewMessage" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <center>
                                <h5 class="modal-title retro" id="exampleModalLabel">Enviar Mensaje a :
                                    {{ $producto->user->user_name }}
                                </h5>
                            </center>
                            <button class="close-message btn btn-close p-1 ms-auto me-0" class="close"
                                data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="{{ route('newMessage') }} ">
                                {{ csrf_field() }}
                                <input type="hidden" autocomplete="off" class="form-control" name="name"
                                    value="{{ $producto->user->user_name }}" required>
                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <textarea name="content" rows="2" placeholder="@lang('messages.write_msg')" class="form-control"></textarea>
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>
                                <br>
                                <button type="submit"
                                    class="btn confirmclosed btn-submit btn-primary w-100 text-lg font-extrabold">Enviar</button>
                            </form>
                        </div>



                    </div>
                </div>
            </div>
        @endif

        </div>
    @else
        <div class="nk-main">
            <div class="nk-gap-3"></div>

            <div class="container-detail mx-auto max-w-800 bg-white border border-gray-300">



                <div class="card-body d-flex justify-content-between align-items-center">
                    <div class="d-flex align-items-center">
                        <div class="user-profile me-3 h-16 w-16 rounded-full">
                            @if ($producto->user->profile_picture)
                                <img src="{{ asset('img/profile-picture-not-found.png') }}" alt="">
                            @else
                                <img src="{{ asset('img/profile-picture-not-found.png') }}" alt="">
                            @endif
                        </div>
                        <div class="user-info">
                            <div class="d-flex align-items-center">
                                <h5 class="mb-1 text-lg">{{ $producto->user->user_name }}</h5>
                                @if (App\SysUserRoles::where('user_id', $producto->user->id)->where('role_id', 2)->count() > 0)
                                    <span class="badge bg-danger ms-2 rounded-pill">@lang('messages.profesional_sell')</span>
                                @endif
                            </div>
                            <p class="mb-0 text-lg">{{ $producto->user->score[1] }}</p>
                        </div>
                    </div>
                    <button class="bg-red-500 hover:bg-red-600 text-white px-6 py-2 new-message font-bold">Enviar un
                        mensaje</button>
                </div>





                <div class="product-slides owl-carousel">
                    <div class="relative">
                        @if (count($producto->images) > 0)
                            <a href="{{ url('/uploads/inventory-images/' . $producto->images->first()->image_path) }}"
                                class="fancybox" data-fancybox="RGM">
                                <img class="single-product-slide rounded-2xl p-3 object-contain"
                                    src="{{ url('/uploads/inventory-images/' . $producto->images->first()->image_path) }}"
                                    alt="{{ $producto->product->name }} - RetroGamingMarket">
                            </a>
                            @if (Auth::user())
                                <button id="showComplaint"
                                    style="font-size: 20px; border: 1px solid rgb(200, 200, 200);
                            box-shadow: rgba(0, 0, 0, 0.1) 0px 5px 5px 2px;
                            background: rgba(14, 0, 0, 0.6); top: 0; right: 0;"
                                    class="showComplaint absolute text-white p-2 rounded hover:bg-blue-800 m-2">
                                    <ion-icon name="alert-circle-outline"></ion-icon>
                                </button>
                            @endif
                        @else
                            <img class="single-product-slide rounded-2xl p-3 object-contain" loading="lazy"
                                src="{{ asset('assets/images/art-not-found.jpg') }}"
                                alt="Producto no encontrado - RetroGamingMarket">
                        @endif
                    </div>

                    @if (count($producto->images) > 1)
                        @foreach ($producto->images as $key)
                            @if (!$loop->first)
                                <div class="relative">
                                    @if (count($producto->images) > 0)
                                        <a href="{{ '/uploads/inventory-images/' . $key->image_path }}" class="fancybox"
                                            data-fancybox="RGM">
                                            <img class="single-product-slide rounded-2xl p-3 object-contain"
                                                src="{{ '/uploads/inventory-images/' . $key->image_path }}"
                                                alt="{{ $producto->product->name }} - RetroGamingMarket">
                                        </a>
                                        @if (Auth::user())
                                            <button id="showComplaint"
                                                style="font-size: 20px; border: 1px solid rgb(200, 200, 200);
                                                box-shadow: rgba(0, 0, 0, 0.1) 0px 5px 5px 2px;
                                                background: rgba(14, 0, 0, 0.6);"
                                                class="showComplaint absolute top-0 right-0 z-10 text-white p-2 rounded hover:bg-blue-800 m-2">
                                                <ion-icon name="alert-circle-outline"></ion-icon>
                                            </button>
                                        @endif
                                    @endif
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>

                <div class="product-description pb-3">
                    <!-- Product Title & Meta Data-->



                    <!-- Product Specification-->
                    <div class="p-specification bg-white mb-3 py-3">

                        <div class="container">

                            <div class="p-title-price text-center">
                                <h5 class="mb-1 font-bold">{{ $producto->product->name }}</h5>
                                <h5 class="mb-1 font-bold">{{ $producto->product->region }}</h5>

                                <br>

                                <h5 class="mb-1 font-bold">- @lang('messages.general_price') -</h5>
                                <br>
                                <?php
                                $duration = [];
                                $duration_not_press = [];
                                $duration_used = [];
                                $duration_new = [];
                                $duration_general = [];
                                $t = '';
                                $total_numeros = '';
                                $total_numeros_not_press = '';
                                $total_numeros_used = '';
                                $total_numeros_new = '';
                                $total_numeros_general = '';
                                $suma = '';
                                $suma_not_press = '';
                                $suma_used = '';
                                $suma_new = '';
                                $suma_general = '';
                                $m = '';
                                $promedio_not_press = '';
                                $promedio_used = '';
                                $promedio_new = '';
                                $promedio_general = '';
                                $prueba = '';
                                $fecha = '';
                                $precio = '';
                                
                                foreach ($producto->product->inventory as $item) {
                                    $cantidad = $item->price;
                                    $duration[] = $cantidad;
                                    $total_numeros = count($duration);
                                    $total = max($duration);
                                    $t = min($duration);
                                    $suma = array_sum($duration);
                                    $m = $suma / $total_numeros;
                                    $prueba = $item->created_at;
                                }
                                
                                //PROMEDIO GENERAL
                                foreach ($seller_sold_general as $p) {
                                    $cantidad_general = $p->price;
                                    $duration_general[] = $cantidad_general;
                                    $total_numeros_general = count($duration_general);
                                    $suma_general = array_sum($duration_general);
                                    $promedio_general = $suma_general / $total_numeros_general;
                                }
                                
                                //PROMEDIO PARA LAS CONDICIONES TIPO "NOT PRESS - NO TIENE" POKEMON GOLD ES EL QUE NO TIENE ASTERIX Y OBELIS TAMPOCO TIENEN
                                foreach ($seller_sold_not_pres as $p) {
                                    $cantidad_press = $p->price;
                                    $duration_not_press[] = $cantidad_press;
                                    $total_numeros_not_press = count($duration_not_press);
                                    $suma_not_press = array_sum($duration_not_press);
                                    $promedio_not_press = $suma_not_press / $total_numeros_not_press;
                                }
                                
                                //PROMEDIO PARA LAS CONDICIONES TIPO "USED" WOODY ES EL QUE ESTA USADO
                                foreach ($seller_sold_used as $p_used) {
                                    $cantidad_used = $p_used->price;
                                    $duration_used[] = $cantidad_used;
                                    $total_numeros_used = count($duration_used);
                                    $suma_used = array_sum($duration_used);
                                    $promedio_used = $suma_used / $total_numeros_used;
                                }
                                
                                //PROMEDIO PARA LAS CONDICIONES TIPO "NEW" XG2 ES EL QUE TIENE EL ESTADO NUEVO
                                foreach ($seller_sold_new as $p_new) {
                                    $cantidad_new = $p_new->price;
                                    $duration_new[] = $cantidad_new;
                                    $total_numeros_new = count($duration_new);
                                    $suma_new = array_sum($duration_new);
                                    $promedio_new = $suma_new / $total_numeros_new;
                                }
                                
                                ?>

                                <?php
                                $l = $producto->price_solo + $producto->price_used + $producto->price_new / 3;
                                ?>


                                @if ($promedio_general > 0)
                                    <div class="row">
                                        <div class="col">
                                            <div class="total-result-of-ratings">
                                                <span>
                                                    @if ($promedio_new > 0)
                                                        {{ number_format($promedio_new, 2) }} €
                                                    @elseif($producto->product->price_new)
                                                        {{ number_format($producto->product->price_new, 2) }} €
                                                    @else
                                                        -
                                                    @endif
                                                </span>
                                                <span>@lang('messages.new_inventory')</span>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="total-result-of-ratings-used">
                                                <span>
                                                    @if ($promedio_used > 0)
                                                        {{ number_format($promedio_used, 2) }} €
                                                    @elseif($producto->product->price_used)
                                                        {{ number_format($producto->product->price_used, 2) }} €
                                                    @else
                                                        -
                                                    @endif
                                                </span><span>@lang('messages.used_inventory')</span>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="total-result-of-ratings-solo"><span>
                                                    @if ($promedio_not_press > 0)
                                                        {{ number_format($promedio_not_press, 2) }} €
                                                    @elseif($producto->product->price_solo)
                                                        {{ number_format($producto->product->price_solo, 2) }} €
                                                    @else
                                                        -
                                                    @endif
                                                </span><span>@lang('messages.only_inventory')</span>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="row">
                                        <div class="col">
                                            <div class="total-result-of-ratings">
                                                <span>

                                                    @if ($producto->product->price_new > 0)
                                                        {{ number_format($producto->product->price_new, 2) }} €
                                                    @else
                                                        -
                                                    @endif

                                                </span>
                                                <span>@lang('messages.new_inventory')</span>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="total-result-of-ratings-used"><span>
                                                    @if ($producto->product->price_used > 0)
                                                        {{ number_format($producto->product->price_used, 2) }} €
                                                    @else
                                                        -
                                                    @endif
                                                </span><span>@lang('messages.used_inventory')</span>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="total-result-of-ratings-solo"><span>
                                                    @if ($producto->product->price_solo > 0)
                                                        {{ number_format($producto->product->price_solo, 2) }} €
                                                    @else
                                                        -
                                                    @endif
                                                </span><span>@lang('messages.only_inventory')</span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <br>
                            </div>

                            <h5 class="mb-1 text-center font-bold">- Estados -</h5>

                            <div class="flex items-center justify-center py-4 mx-auto">
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span class="pl-2">Caja</span>
                                    <br>
                                    <img class="w-4 h-4 md:w-6 md:h-6 mx-auto" src="/{{ $producto->box }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->box_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span class="pl-2">Carátula</span>
                                    <br>
                                    <img class="w-4 h-4 md:w-6 md:h-6 mx-auto" src="/{{ $producto->cover }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->cover_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span class="pl-2">Manual</span>
                                    <br>
                                    <img class="w-4 h-4 md:w-6 md:h-6 mx-auto" src="/{{ $producto->manual }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->manual_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span class="pl-2">Juegos</span>
                                    <br>
                                    <img class="w-4 h-4 md:w-6 md:h-6 mx-auto" src="/{{ $producto->game }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->game_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                                <p class="flex-grow text-lg font-bold text-center">
                                    <span class="pl-2">Extras</span>
                                    <br>
                                    <img class="w-4 h-4 md:w-6 md:h-6 mx-auto" src="/{{ $producto->extra }}"
                                        data-toggle="popover"
                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($producto->extra_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                </p>
                            </div>








                            <div class="nk-product-meta">
                                <div class="lists">

                                    <div class="flex justify-between items-center p-4 rounded-lg">
                                        <span
                                            class="font-extrabold text-2xl text-green-700">{{ number_format($producto->price, 2) . ' €' }}</span>

                                        @if (Auth::user())
                                            @if ($producto->user_id != auth()->user()->id)
                                                <select id="quantity"
                                                    class="d-inline font-extrabold br-btn-product-qty focus:outline-none focus:bg-white focus:border-gray-500">
                                                    @for ($i = 1; $i <= $producto->quantity; $i++)
                                                        {{ $i }}
                                                        <option data-value="{{ $i }}"
                                                            value="{{ $i }}">
                                                            {{ $i }}</option>
                                                    @endfor
                                                </select>
                                            @else
                                                <span data-value="{{ $producto->quantity }}"
                                                    class="text-2xl font-extrabold">
                                                    {{ $producto->quantity }}
                                                </span>
                                            @endif
                                        @else
                                            <span data-value="{{ $producto->quantity }}"
                                                class=" text-2xl font-extrabold">
                                                {{ $producto->quantity }}
                                            </span>

                                        @endif

                                        @if (Auth::user())
                                            @if ($producto->user_id != auth()->user()->id)
                                                <div class="flex mr-6 mt-2 text-center">
                                                    <a class="btn-ultra btn-danger"
                                                        href="{{ route('product-show', $producto->product->id) }}"><span
                                                            class="retro text-white font-extrabold">@lang('messages.see_another_offer')</span></a>

                                                    <button class="btn-ultra btn-success cart-add-item-id ms-3"
                                                        data-user-id="{{ $producto->user_id }}"
                                                        data-inventory-id="{{ $producto->id + 1000 }}"
                                                        data-inventory-qty="{{ $producto->quantity }}"
                                                        type="button"><span
                                                            class="retro text-white font-extrabold">@lang('messages.add_to_cart')</span></button>
                                                </div>
                                            @else
                                                <div class="flex mr-6 mt-2 text-center">
                                                    <a class="btn-ultra btn-danger"
                                                        href="{{ route('product-show', $producto->product->id) }}"><span
                                                            class="retro text-white font-extrabold">@lang('messages.see_another_offer')</span></a>
                                                    <a class="btn-ultra btn-warning ms-3"
                                                        href="{{ route('inventory_edit', $producto->id) }}"><span
                                                            class="retro text-white font-extrabold">@lang('messages.edit_product')</span></a>
                                                </div>
                                            @endif
                                        @else
                                            <div class="flex mr-6 mt-2 text-center">
                                                <a class="btn-medium btn-danger py-3 px-4"
                                                    href="{{ route('product-show', $producto->product->id) }}">
                                                    <span class="retro text-white font-extrabold">@lang('messages.see_another_offer')</span>
                                                </a>
                                            </div>
                                        @endif
                                    </div>

                                </div>

                                <br>

                                <div class="p-title-price text-center">
                                    <h5 class="mb-1 font-extrabold">- @lang('messages.share_this_product') -</h5>
                                    <div class="mt-2">
                                        <a href="#" id="wha-btn" class="btn btn-icon btn-sm btn-whatsapp">
                                            <i class="fab fa-whatsapp"></i>
                                        </a>
                                        <a href="#" id="tele-btn" class="btn btn-icon btn-sm btn-telegram">
                                            <i class="fab fa-telegram"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>





                            <!-- Anuncios de perfil en RGM -->



                        </div>
                    </div>
                    <br><br><br>






                </div>


            </div>

        </div>

        @if (Auth::user())
            <div class="modal fade" id="modalNewMessage" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <center>
                                <h5 class="modal-title retro" id="exampleModalLabel">Enviar Mensaje a :
                                    {{ $producto->user->user_name }}
                                </h5>
                            </center>
                            <button class="close-message btn btn-close p-1 ms-auto me-0" class="close"
                                data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="{{ route('newMessage') }} ">
                                {{ csrf_field() }}
                                <input type="hidden" autocomplete="off" class="form-control" name="name"
                                    value="{{ $producto->user->user_name }}" required>
                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <textarea name="content" rows="2" placeholder="@lang('messages.write_msg')" class="form-control"></textarea>
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>
                                <br>
                                <button type="submit"
                                    class="btn confirmclosed btn-submit btn-primary w-100 text-lg font-extrabold">Enviar</button>
                            </form>
                        </div>



                    </div>
                </div>
            </div>
        @endif

    @endif

@endsection

@section('content-script-include')
    <script>
        // Social Share links.
        const whatsappBtn2 = document.getElementById('wha-btn');
        const telegramBtn2 = document.getElementById('tele-btn');

        // producturl posttitle
        let productUrl = encodeURI(document.location.href);
        let productTitle = encodeURI('{{ $producto->name }}');
        whatsappBtn2.setAttribute("href", `https://wa.me/?text=${productTitle} ${productUrl}`);
        telegramBtn2.setAttribute("href", `https://telegram.me/share/url?url=${productUrl}&text=${productTitle}`);
    </script>

    <script src="{{ asset('brcode/js/pty.components.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>


    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="{{ asset('assets/noty/packaged/jquery.noty.packaged.min.js') }}"></script>

    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.4.1/chart.min.js"
        integrity="sha512-5vwN8yor2fFT9pgPS9p9R7AszYaNn0LkQElTXIsZFCL7ucT8zDCAqlQXDdaqgA1mZP47hdvztBMsIoFxq/FyyQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection
@section('content-script')
    @livewireScripts

    <script>
        $('.new-message').click(function() {
            $('#modalNewMessage').modal('show');
        });

        $('.close-message').click(function() {
            $('#modalNewMessage').modal('hide');
        });
    </script>

    <script>
        $(document).ready(function() {
            $('.comments').popover();
            $('[data-toggle="popover"]').popover();

            $("#quantity").change(function() {
                var selectedItem = $(this).val();
                var abc = $('option:selected', this).data("value");
                console.log(abc);
            });

            $('.cart-add-item-id').click(function() {
                var userId = $(this).data('user-id');
                var inventoryId = $(this).data('inventory-id');
                var inventoryQty = $('#quantity').val(); // Obtener directamente el valor de 'quantity'

                var cartItem = $(this).closest('.cart-item');

                $.showBigOverlay({
                    message: '{{ __('Actualizando tu carro de compras') }}',
                    onLoad: function() {
                        $.ajax({
                            url: '{{ url('/cart/add_item') }}',
                            data: {
                                _token: $('input[name="_token"]:eq(0)').val(),
                                inventory_id: inventoryId,
                                inventory_qty: inventoryQty,
                                user_id: userId,
                            },
                            dataType: 'JSON',
                            type: 'POST',
                            success: function(data) {
                                if (data.productCountForVendor >= 4) {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'No puedes agregar más de 4 productos diferentes por usuario y carrito.',
                                        footer: '<a href="#">¿Por qué existe esta limitación?</a>'
                                    });
                                } else {
                                    window.location.href = "{{ url('/cart') }}";
                                }
                                $.showBigOverlay('hide');
                            },
                            error: function(data) {
                                $.showBigOverlay('hide');
                            }
                        });
                    }
                });
            });
        });


        function updateShipping(x) {
            $.showBigOverlay({
                message: '{{ __('Actualizando tu carro de compras') }}',
                onLoad: function() {
                    $.ajax({
                        url: '{{ url('/cart/add_shipping') }}',
                        data: {
                            _token: $('input[name="_token"]:eq(0)').val(),
                            d1: $(x).data('id'),
                            d2: $(x).val(),
                        },
                        dataType: 'JSON',
                        type: 'POST',
                        success: function(data) {
                            if (data.error == 0) {
                                window.location.href = window.location.href;
                            } else {
                                $.showBigOverlay('hide');
                            }
                            $.showBigOverlay('hide');
                        },
                        error: function(data) {

                        }
                    })
                }
            });
        }
    </script>



    <script>
        Livewire.on('game', function() {


            var $inventoryAdd = $('.br-btn-product-add-game');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('console', function() {


            var $inventoryAdd = $('.br-btn-product-add-console');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('periferico', function() {


            var $inventoryAdd = $('.br-btn-product-add-periferico');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('accesorio', function() {


            var $inventoryAdd = $('.br-btn-product-add-accesorio');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('merchandising', function() {


            var $inventoryAdd = $('.br-btn-product-add-merchandising');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('alert', function() {
            Swal.fire(
                'Listo!',
                '<span class="retro">Tu Producto fue eliminado</span>',
                'success'
            )
        })
    </script>




    <script>
        function setup() {
            return {
                activeTab: 0,
                tabs: [
                    "Nuevo",
                    "Solo",
                    "Usado",
                ]
            };
        };
    </script>
    @stack('scripts')
@endsection
