<html>

    <head>
        <style>
            :root {
                --bs-blue: #0d6efd;
                --bs-indigo: #6610f2;
                --bs-purple: #6f42c1;
                --bs-pink: #d63384;
                --bs-red: #dc3545;
                --bs-orange: #fd7e14;
                --bs-yellow: #ffc107;
                --bs-green: #198754;
                --bs-teal: #20c997;
                --bs-cyan: #0dcaf0;
                --bs-white: #fff;
                --bs-gray: #6c757d;
                --bs-gray-dark: #343a40;
                --bs-gray-100: #f8f9fa;
                --bs-gray-200: #e9ecef;
                --bs-gray-300: #dee2e6;
                --bs-gray-400: #ced4da;
                --bs-gray-500: #adb5bd;
                --bs-gray-600: #6c757d;
                --bs-gray-700: #495057;
                --bs-gray-800: #343a40;
                --bs-gray-900: #212529;
                --bs-primary: #0d6efd;
                --bs-secondary: #6c757d;
                --bs-success: #198754;
                --bs-info: #0dcaf0;
                --bs-warning: #ffc107;
                --bs-danger: #dc3545;
                --bs-light: #f8f9fa;
                --bs-dark: #212529;
                --bs-primary-rgb: 13, 110, 253;
                --bs-secondary-rgb: 108, 117, 125;
                --bs-success-rgb: 25, 135, 84;
                --bs-info-rgb: 13, 202, 240;
                --bs-warning-rgb: 255, 193, 7;
                --bs-danger-rgb: 220, 53, 69;
                --bs-light-rgb: 248, 249, 250;
                --bs-dark-rgb: 33, 37, 41;
                --bs-white-rgb: 255, 255, 255;
                --bs-black-rgb: 0, 0, 0;
                --bs-body-color-rgb: 33, 37, 41;
                --bs-body-bg-rgb: 255, 255, 255;
                --bs-font-sans-serif: system-ui, -apple-system, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", "Liberation Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
                --bs-font-monospace: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
                --bs-gradient: linear-gradient(180deg, rgba(255, 255, 255, 0.15), rgba(255, 255, 255, 0));
                --bs-body-font-family: var(--bs-font-sans-serif);
                --bs-body-font-size: 1rem;
                --bs-body-font-weight: 400;
                --bs-body-line-height: 1.5;
                --bs-body-color: #212529;
                --bs-body-bg: #fff;
            }
    
            table {
                width: 75%;
                border-collapse: collapse;
            }
    
            th,
            td {
                text-align: left;
            }
    
            table,
            td,
            th {
                border: 0.5px solid black;
            }
        </style>
    </head>

<body>
    <h3 class="roboto"> <?php echo Lang::get('messages.hi_email'); ?> {{ strtoupper($order->seller->user_name) }} <br>
        {{ strtoupper($order->buyer->user_name) }}
        @if ($order->details->count() > 1)
        <?php echo Lang::get('messages.reserved_email'); ?>
        @else
        <?php echo Lang::get('messages.reserved_one_product'); ?>
        @endif
    </h3>
    <p>

        <br>
        <?php echo Lang::get('messages.user_email_send'); ?> {{ strtoupper($order->buyer->user_name) }} <?php echo Lang::get('messages.user_email_warning_send'); ?> <a
            href="https://www.retrogamingmarket.eu/account/sales/view/{{ $order->order_identification }}"><?php echo Lang::get('messages.link_email'); ?></a>.
        <br>
    </p>
    <h2 class="roboto">
        <center><?php echo Lang::get('messages.information'); ?></center>
    </h2>
    <p>
        <br>
        Pedido <font color="#c53030">{{ $order->order_identification }}</font>
        <br>
        Estado: <font color="#ff9500"><?php echo Lang::get('messages.neto_email_not_paid'); ?></font>
        <br>
        <br>
        {{ $order->buyer->first_name . ' ' . $order->buyer->last_name }}
        @if ($order->buyer->address)
            <br>
            {{ $order->buyer->address }}
        @endif
        @if ($order->buyer->zipcode || $order->buyer->city)
            <br>
            {{ $order->buyer->zipcode . ' ' . $order->buyer->city }}
        @endif
        <br>
        {{ $order->buyer->country->name }}
        <br><br>
        <?php echo Lang::get('messages.total_sale_email'); ?>: <font color="#276749">{{ number_format($order->total, 2) }} €</font><br>
    </p>
    <br>
    <br>
    <h3 class="roboto">
        <?php echo Lang::get('messages.order_detail_two'); ?> :
    </h3>
    <table>
        <thead>
            <tr>
                <th><?php echo Lang::get('messages.product_in_email'); ?></th>
                <th><?php echo Lang::get('messages.total_in_email'); ?></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($order->details as $key)
                <tr>
                    <td>{{ $key->quantity . ' x ' . $key->product->name }} </td>
                    <td> {{ number_format($key->inventory->price, 2) }} €</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td><b><?php echo Lang::get('messages.cost_send_email'); ?></b> </td>
                <td> {{ number_format($order->shipping_price, 2) }} €</td>
            </tr>
            <tr>
                <td><b><?php echo Lang::get('messages.total_in_email'); ?></b> </td>
                <td> {{ number_format($order->total, 2) }} €</td>
            </tr>
        </tfoot>
    </table>

    <br><br>
    </p>

</body>

</html>
