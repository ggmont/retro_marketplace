<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Cancelación del Pedido</title>
</head>

<body>
    <p>Hola {{ $order->seller->user_name }},</p>
    <br>
    <p>Le informamos que tiene un pedido pendiente de pago con identificación
        {{ $order->order_identification }}. El comprador tiene hasta las próximas 24 horas para realizar el pago. Si
        el pago no se realiza dentro de este plazo, el pedido se cancelará automáticamente.</p>
    <br>
    <p>Ponte en contacto con nosotros si tienes alguna duda.<br>Gracias.</p>

</body>

</html>
