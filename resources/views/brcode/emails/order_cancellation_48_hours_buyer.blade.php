<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Se ha cancelado un pedido por impago</title>
</head>

<body class="bg-gray-100">

    <p class="mb-4">Hola {{ $order->buyer->user_name }},</p>
    <p class="mb-4">el pedido n.º {{ $order->order_identification }} ha sido cancelado por impago.</p>


    <p class="mb-4">Hemos registrado el impago en tu cuenta pública.</p>
    <p class="mb-2">Si has tenido algún problema o tienes alguna duda, ponte en contacto con nosotros.</p>
    <br>
    <p>Gracias por usar Retro Gaming Market.</p>

</body>

</html>
