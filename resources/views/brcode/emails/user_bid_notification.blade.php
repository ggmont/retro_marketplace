<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Oferta Aceptada</title>
</head>
<body>
    <h1>¡Felicidades! Tu oferta ha sido aceptada</h1>
    <p>Hola {{ $username }},</p>
    <p>Tu oferta ha sido aceptada y ahora puedes proceder con el proceso de compra.</p>
    
    <p>Haz clic en el siguiente enlace para ver los detalles de tu orden:</p>
    <a href="https://www.retrogamingmarket.eu/account/purchases/view/{{ $orderIdentification }}">
        Ver Detalles de la Orden
    </a>

    <p>Gracias.</p>
</body>
</html>
