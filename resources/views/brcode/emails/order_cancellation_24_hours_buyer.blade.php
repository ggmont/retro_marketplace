<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Pedido pendiente por pagar</title>

</head>

<body>
    <p>Hola {{ $order->buyer->user_name }},</p>
    <br>
    <p>Te recordamos que tienes un pedido pendiente de pagar con identificación
        {{ $order->order_identification }}. Puedes acceder <a
            href="https://www.retrogamingmarket.eu/account/purchases/view/{{ $order->order_identification }}">aquí</a>
        para realizar el pago.</p>
    <br>
    <p>Si en 24h no se ha realizado, se cancelará y quedará constancia en tu perfil público.</p>
    <br>
    <p>Ponte en contacto con nosotros si tienes alguna duda.<br>Gracias.</p>

</body>

</html>
