<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Tu oferta ha sido superada</title>
</head>

<body>
    <p>Hola {{ $inventory->userbid->user_name }},</p>

    <p>Tu oferta por el producto "{{ $inventory->product->name ?? $inventory->title }}" ha sido superada.</p>

    <p>Puedes ver la nueva oferta     <a href="{{ route('product-inventory-show', ['id' => $inventory->id]) }}">Aquí</a></p>
    
    <br>

    <p>Aún estás a tiempo de superarla</p>

    <br>

    <p>Mucha suerte!</p>
</body>

</html>
