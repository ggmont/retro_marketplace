<!DOCTYPE html>
<html>
<head>
    <title>Notificación de Pedido</title>
    <style>
        :root {
            /* Define tus variables CSS personalizadas aquí si es necesario */
        }

        body {
            font-family: Arial, sans-serif;
            background-color: #fff;
            color: #212529;
            margin: 0;
            padding: 20px;
        }

        h3 {
            font-size: 20px;
            font-weight: bold;
        }

        h2 {
            font-size: 24px;
            font-weight: bold;
            text-align: center;
            margin-top: 20px;
            margin-bottom: 20px;
        }

        p {
            font-size: 16px;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        th, td {
            border: 1px solid #ddd;
            padding: 10px;
        }

        th {
            background-color: #f2f2f2;
        }

        tbody tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        tfoot {
            font-weight: bold;
        }

        a {
            text-decoration: none;
            color: #007bff;
        }
    </style>
</head>
<body>
    <h3><?php echo Lang::get('messages.hi_email'); ?> {{ strtoupper($order->Buyer->user_name) }}</h3>
    <p><?php echo Lang::get('messages.the_order_email'); ?> {{ $order->order_identification }} <?php echo Lang::get('messages.required_paid_email'); ?></p>

    <h2><?php echo Lang::get('messages.information'); ?></h2>

    <p>
        Pedido <b><a href="https://www.retrogamingmarket.eu/account/sales/view/{{ $order->order_identification }}">{{ $order->order_identification }}</a></b><br>
        <?php echo Lang::get('messages.state_email'); ?>: <b><?php echo Lang::get('messages.not_paid'); ?></b><br><br>
        {{ $order->buyer->first_name . ' ' . $order->buyer->last_name }}<br>
        @if ($order->buyer->address)
            {{ $order->buyer->address }}<br>
        @endif
        @if ($order->buyer->zipcode || $order->buyer->city)
            {{ $order->buyer->zipcode . ' ' . $order->buyer->city }}<br>
        @endif
    </p>

    <h5><?php echo Lang::get('messages.order_detail_two'); ?>:</h5>

    <table>
        <thead>
            <tr>
                <th><?php echo Lang::get('messages.product_in_email'); ?></th>
                <th><?php echo Lang::get('messages.total_in_email'); ?></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($order->details as $key)
                <tr>
                    <td>{{ $key->quantity . ' x ' . $key->product->name }} </td>
                    <td>{{ number_format($key->inventory->price, 2) }} €</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td><b><?php echo Lang::get('messages.cost_send_email'); ?></b></td>
                <td>{{ number_format($order->shipping_price, 2) }} €</td>
            </tr>
            <tr>
                <td><b><?php echo Lang::get('messages.total_in_email'); ?></b></td>
                <td>{{ number_format($order->total, 2) }} €</td>
            </tr>
        </tfoot>
    </table>

    <br><br>
</body>
</html>
