<!DOCTYPE html>
<html>

<head>
    <title>Restablecimiento de Contraseña</title>
</head>

<body>
    <p>Hola {{ $username }},</p>

    <p>Hemos recibido una solicitud para restablecer tu contraseña. Haz clic en el siguiente enlace para restablecerla:</p>

    <p><a href="https://www.retrogamingmarket.eu/create_new_password/{{ $resetToken }}">Restablecer mi contraseña</a></p>
</body>

</html>
