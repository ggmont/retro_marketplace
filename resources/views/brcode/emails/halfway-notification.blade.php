<!DOCTYPE html>
<html>

<head>
    <title>Tu producto finalizara en 12h</title>
</head>

<body>
    <h1>A tu producto le quedan 12h</h1>
    <p>Hola {{ $inventory->user->name }},</p>
    <p>Tu producto "{{ $inventory->product->name ?? $inventory->title }}" puede seguir recibiendo ofertas durante
        12h más.</p>
    <p>Asegúrate de gestionar las ofertas y mensajes que hayas recibido sobre el producto.</p>
    <p>Gracias por usar nuestro sistema de ofertas.</p>
    <p>Saludos</p>
</body>

</html>
