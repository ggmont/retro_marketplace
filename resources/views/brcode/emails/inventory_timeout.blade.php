<!DOCTYPE html>
<html>

<head>
    <title>Tu producto en oferta a finalizado</title>
</head>

<body>
    <p>Hola {{ $inventory->user->user_name }}</p>
    <p>El tiempo de tu producto "{{ $inventory->product->name ?? $inventory->title }}" ha acabado y lamentablemente
        no ha recibido ninguna oferta.</p>
    <p>Puedes reiniciar el tiempo desde tu inventario y obtener otras 24h</p>

    <p>Asegúrate de compartir el producto en tus redes, chats o grupos para que todo el mundo vea lo que estás vendiendo</p>
    <!-- Puedes agregar un botón o un enlace para permitir al usuario reiniciar el contador -->
    <a href="https://www.retrogamingmarket.eu/account/inventory">Ingresa aqui si deseas entrar en tu inventario</a> (Debes estar
    logueado para poder entrar)

    <p>Gracias por usar nuestro sistema de ofertas</p>
    <p>Saludos</p>
</body>

</html>
