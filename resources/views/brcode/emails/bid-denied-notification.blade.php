<!DOCTYPE html>
<html>

<head>
    <title>El producto "{{ $inventory->product->name ?? $inventory->title }}" ha sido eliminado</title>
</head>

<body>
    <p>Hola {{ $inventory->userbid->user_name }},</p>
    <p>El producto "{{ $inventory->product->name ?? $inventory->title }}" por el cual habías ofertado, ha sido eliminado.</p>
    <p>Lamentamos que no lo hayas conseguido!</p>
    <p>Gracias por usar nuestro sistema de ofertas</p>
    <p>Saludos.</p>
</body>

</html>
