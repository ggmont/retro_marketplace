<!DOCTYPE html>
<html>

<head>
    <title>Oferta finalizada, pero aún tienes una oportunidad</title>
</head>

<body>
    <p>La oferta para el producto "{{ $inventory->product->name ?? $inventory->title }}" ha finalizado, y eres el último
        ofertante.</p>
    <p>¿Qué significa esto? Aún tienes la oportunidad de adquirir el producto si el vendedor acepta tu oferta. Mantente
        atento a tu panel de ofertas para recibir actualizaciones.</p>
    <p>Haz clic en el siguiente enlace para acceder a tu panel de ofertas:</p>
    <p><a href="https://www.retrogamingmarket.eu/Offer">Panel de Ofertas (Asegúrate de haber iniciado sesión)</a></p>
    <p>Gracias por utilizar nuestro sistema de ofertas.</p>
    <p>Saludos.</p>
</body>

</html>
