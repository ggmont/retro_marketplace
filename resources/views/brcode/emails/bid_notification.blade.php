<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Has recibido una nueva oferta</title>
</head>

<body>
    <h2>Hola {{ $inventory->user->user_name }},</h2>
    <p>El usuario {{ $inventory->userbid->user_name }} ha hecho una oferta de {{ $inventory->max_bid }} € para tu
        producto "{{ $inventory->product->name ?? $inventory->title }}" Puedes aceptar o denegar su oferta <a
            href="{{ route('product-inventory-show', ['id' => $inventory->id]) }}">Ver
            aquí</a> </p>
    <p>Recuerda gestionar todas las ofertas antes de que se acabe el plazo de venta.</p>
    <p>Si no deniegas las ofertas, tendrás que venderlo al que más haya ofertado.</p>
    <p>Ponte en contacto con nosotros si tienes dudas.</p>
    <p>Saludos</p>
</body>

</html>
