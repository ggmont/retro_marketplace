<!DOCTYPE html>
<html>

<head>
    <title>Se ha acabado el plazo</title>
</head>

<body>
    <p>La oferta para tu producto "{{ $inventory->product->name ?? $inventory->title }}" ha finalizado, y el último ofertante está esperando tu respuesta.</p>
    <p>¿Qué significa esto? Aún puedes decidir si aceptas la oferta del último interesado. Revisa tu panel de ofertas para tomar una decisión.</p>
    <p>Haz clic en el siguiente enlace para acceder a tu panel de ofertas:</p>
    <p><a href="https://www.retrogamingmarket.eu/Offer">Panel de Ofertas (Asegúrate de haber iniciado sesión)</a></p>
    <p>Gracias por utilizar nuestro sistema de ofertas. Si tienes alguna pregunta o necesitas asistencia, no dudes en contactarnos.</p>
    <p>Saludos cordiales.</p>
</body>

</html>
