<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Oferta Rechazada</title>
</head>

<body>
    <h1>
        Hola {{ $inventory->userbid->user_name }}
        @if (isset($inventory->product) && !empty($inventory->product->name))
            Tu oferta en el producto "{{ $inventory->product->name }}" ha sido rechazada.
        @elseif (!empty($inventory->title))
            Tu oferta en el producto "{{ $inventory->title }}" ha sido rechazada.
        @else
            Tu oferta ha sido rechazada.
        @endif.
    </h1>

    <p>
        Asegúrate de ofrecer un precio razonable. Puedes utilizar nuestra base de datos para orientarte o preguntar en
        nuestra comunidad<br>
        Habla con el vendedor si necesitas más detalles del producto.<br>
        Mucha suerte!
    </p>
</body>

</html>
