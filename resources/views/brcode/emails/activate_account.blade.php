<!DOCTYPE html>
<html>

<head>
    <title>Activación de Cuenta</title>
</head>

<body>
    <p>Hola {{ $userName }},</p>

    <p>Hemos recibido una solicitud para crear una nueva cuenta. Actívala haciendo clic en este enlace:</p>

    <p>
        <a href="https://www.retrogamingmarket.eu/activate_account/{{ $activationToken }}">Activar mi cuenta</a>
    </p>
</body>

</html>
