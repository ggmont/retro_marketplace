<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Nueva solicitud de registro de producto</title>
</head>

<body style="font-family: Arial, sans-serif; color: #333;">
    <h1 style="text-align: center;">Nueva solicitud de registro de producto</h1>
    <p>Hola,</p>
    <p>Se ha recibido una nueva solicitud de registro de producto:</p>
    <ul style="list-style-type: none; padding-left: 0;">
        <li><strong>Producto:</strong> {{ $productData['product'] }}</li>
        <li><strong>Plataforma:</strong> {{ $productData['platform'] }}</li>
        <li><strong>Región:</strong> {{ $productData['region'] }}</li>
        <li><strong>Cantidad:</strong> {{ $productData['quantity'] }}</li>
        <li><strong>Comentarios:</strong> {{ $productData['comments'] }}</li>
        @if ($productData['in_collection'] === 'Y')
            <li><strong>¿En colección?:</strong> Sí, el usuario quiere agregar este producto a su colección.</li>
        @endif
    </ul>
    <p>
        @if ($productData['in_collection'] === 'Y')
            Por favor, revisa la solicitud y agrega el producto a la colección del usuario.
            <br>
            <a href="https://www.retrogamingmarket.eu/11w5Cj9WDAjnzlg0/product-request"
                style="color: #E30613; text-decoration: none;">https://www.retrogamingmarket.eu/11w5Cj9WDAjnzlg0/product-request</a>
        @else
            <p>Por favor, revisa la solicitud en la siguiente página:</p>
            <br>
            <a href="https://www.retrogamingmarket.eu/11w5Cj9WDAjnzlg0/product-request"
                style="color: #E30613; text-decoration: none;">https://www.retrogamingmarket.eu/11w5Cj9WDAjnzlg0/product-request</a>
        @endif
    </p>
</body>

</html>
