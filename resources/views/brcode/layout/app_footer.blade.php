<footer class="main-footer">
    <strong>Copyright &copy; 2021 <a href="https://retrogamingmarket.eu">Retrogamingmarket.eu</a>.</strong>
    Todos los derechos reservados.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version Beta</b> 2.0
    </div>
</footer>
</div><!-- ./wrapper -->

<script src="{{ asset('special/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('special/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('special/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('special/js/adminlte.js') }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{ asset('special/plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('special/plugins/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('special/plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
<script src="{{ asset('special/plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>


<script src="{{ asset('special/plugins/chart.js/Chart.min.js') }}"></script>

<script src="{{ asset('special/js/pages/dashboard2.js') }}"></script>

@yield('content-script-include')
@yield('content-script')
@livewireScripts

</body>

</html>
