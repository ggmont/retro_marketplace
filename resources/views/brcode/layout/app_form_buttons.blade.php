<div class="row">
<!-- left column -->
	<div class="col-md-6">
	  <div class="box-footer">
		<a href="{{ isset($viewData['form_url_list']) ? $viewData['form_url_list'] : url('11w5Cj9WDAjnzlg0/dashboard') }}" class="btn btn-default"><i class="fa fa-chevron-left"></i> {{ Lang::get('app.cancel') }}</a>
		@if(isset($viewData['form_url_post']))
		<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" ></i> {{ Lang::get('app.submit') }}</button>
		@endif
		@if(isset($viewData['form_url_post_delete']) && $viewData['model_id'] > 0)
		<button type="button" class="btn btn-danger pull-right" id="br-btn-delete" data-loading-text="{{ Lang::get('app.delete_confirmation') }}"><i class="fa fa-trash" ></i> {{ Lang::get('app.delete') }}</button>
		@endif
		<input type="hidden" name="model_id" value="{{ $viewData['model_id'] > 0 ? $viewData['model_id'] : '' }}" />
	  </div>
	</div><!-- /.col-lg-6 -->
</div><!-- /.row -->

