@include('brcode.layout.app_header')

@include('brcode.layout.app_sidebar')
	  
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
@yield('content')
</div><!-- /.content-wrapper -->

@include('brcode.layout.app_footer')

