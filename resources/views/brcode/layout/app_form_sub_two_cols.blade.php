<?php

  $subModels = [];

  if(isset($viewData['sub_models'])) {
    $subModels = $viewData['sub_models'];
  }
  else if (isset($viewData['sub_model_view'])) {
    $subModels = array([
      'sub_model_view'  => $viewData['sub_model_view'],
      'sub_model'       => isset($viewData['sub_model']) ? $viewData['sub_model'] : [],
      'sub_model_title' => $viewData['sub_model_title'],
    ]);

    switch($viewData['sub_model_view']) {
      case 'table':
        $subModels[0]['sub_model_cols'] = $viewData['sub_model_cols'];
      break;
      case 'list':
        $subModels[0]['sub_model_list'] = $viewData['sub_model_list'];
      break;
      case 'file-upload':
      break;
    }

  }

?>
<div class="row">

@foreach($subModels as $subModelKey => $subModelValue)
<div class="col-md-6">
  <div class="box box-primary">
  	<div class="box-header with-border">
  	  <h3 class="box-title">{{ Lang::get('app.related_objects') }}: {{ $subModelValue['sub_model_title'] }}</h3>
  	</div><!-- /.box-header -->
	  <div class="box-body">
  		@if($subModelValue['sub_model_view'] == 'table')
  		<table id="" class="table table-bordered table-striped br-table" >
  		<thead>
  			<tr>
  				<th style="text-align: center; width: 50px">
  				@if($subModelValue['sub_model_view'] == 'table')
  				<button type="button" class="btn btn-default btn-sm br-table-add"><i class="fa fa-plus-square-o"></i></button>
  				@else
  				<input type="checkbox" name="check_all[]" value="1" />
  				@endif
  				</th>
  				@foreach($subModelValue['sub_model_cols'] as $key => $col)
  				<th class="{{ $col['type']=='hidden'?'hide':'' }}" style="vertical-align: middle;">{{ isset($col['label']) ? $col['label'] : Lang::get('app.col_' . $key) }}</th>
  				@endforeach
  			</tr>
  		</thead>
  		<tbody>
  			<tr class="br-table-template hide">
  				<td><button type="button" class="btn btn-warning btn-sm br-table-rem"><i class="fa fa-minus-square-o"></i></button></td>
  				@foreach($subModelValue['sub_model_cols'] as $key => $col)
  				<td class="{{ $col['type'] == 'hidden' ? 'hide' : '' }}">
  				@if($col['type'] == 'hidden')
  				  <input type="hidden" class="form-control" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" value="{{ isset($viewData['model'][$key])?$viewData['model'][$key]:'' }}">
  				@elseif($col['type'] == 'text')
  				  <input type="text" class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" value="{{ isset($viewData['model'][$key])?$viewData['model'][$key]:'' }}"  {{ isset($col['attr']) ? $col['attr'] : '' }} />
  				@elseif($col['type'] == 'select')
  				  <select class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]"  {{ isset($col['attr']) ? $col['attr'] : '' }} style="width: 100%">
  					<option value="">...</option>
  					@if(isset($col['select_values']))
  					@foreach($col['select_values'] as $select_row)
  					<option value="{{ $select_row['id'] }}" {{ isset($viewData['model'][$key]) ? ($viewData['model'][$key] == $select_row['id'] ? 'selected="selected"' : '') : '' }}>{{ $select_row['value'] }}</option>
  					@endforeach
  					@endif
  				  </select>
  				@endif
  				</td>
  				@endforeach
  			</tr>
  			@if(isset($subModelValue['sub_model']))
  			@foreach($subModelValue['sub_model'] as $sub_model)
  			<tr >
  				<td><button type="button" class="btn btn-warning btn-sm br-table-rem"><i class="fa fa-minus-square-o"></i></button></td>
  				@foreach($subModelValue['sub_model_cols'] as $key => $col)
  				<td class="{{ $col['type'] == 'hidden' ? 'hide' : '' }}">
  				@if($col['type'] == 'hidden')
  				  <input type="hidden" class="form-control" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" value="{{ isset($viewData['model'][$key])?$viewData['model'][$key]:'' }}">
  				@elseif($col['type'] == 'text')
  				  <input type="text" class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" value="{{ isset($viewData['model'][$key])?$viewData['model'][$key]:'' }}"  {{ isset($col['attr']) ? $col['attr'] : '' }} />
  				@elseif($col['type'] == 'select')
  				  <select class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]"  {{ isset($col['attr']) ? $col['attr'] : '' }} style="width: 100%">
  					<option value="">...</option>
  					@if(isset($col['select_values']))
  					@foreach($col['select_values'] as $select_row)
  					<option value="{{ $select_row['id'] }}" {{ isset($sub_model[$key]) ? ($sub_model[$key] == $select_row['id'] ? 'selected="selected"' : '') : '' }}>{{ $select_row['value'] }}</option>
  					@endforeach
  					@endif
  				  </select>
  				@endif
  				</td>
  				@endforeach
  			</tr>
  			@endforeach
  			@endif
  		</tbody>
  		</table>
		  @elseif($subModelValue['sub_model_view'] == 'list')
  		<table id="" class="table table-bordered table-striped" >
  		<thead>
  			<tr>
  				<th style="text-align: center; width: 50px"><input type="checkbox" name="check_all[]" value="1" /></th>
  				<th>{{ isset($subModelValue['sub_model_list'][2]) ? $subModelValue['sub_model_list'][2] : Lang::get('app.col_' . $subModelValue['sub_model_list'][0]) }}</th>
  			</tr>
  		</thead>
  		<tbody>
  			@foreach($subModelValue['sub_model_list'][1] as $key => $col)
  			<tr>
  				<td class="text-center"><input type="checkbox" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $subModelValue['sub_model_list'][0] }}[]" value="{{ $col['id'] }}"   {{ isset($subModelValue['sub_model']) ? (array_search($col['id'], array_column($subModelValue['sub_model'], $subModelValue['sub_model_list'][0] )) !== false ? 'CHECKED' : '') : '' }} /></td>
  				<td>{{ $col['value'] }}</td>
  			</tr>
  			@endforeach
  		</table>
  		@endif
	  </div><!-- /.box-body -->
  </div><!-- /.box -->
</div><!-- /.col-lg-6 -->
@endforeach
</div><!-- /.row -->
