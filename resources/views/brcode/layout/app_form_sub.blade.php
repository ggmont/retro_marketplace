<?php

  $subModels = [];
  if(isset($viewData['sub_models'])) {
    $subModels = $viewData['sub_models'];
  }
  else if (isset($viewData['sub_model_view'])) {
    $subModels = array([
      'sub_model_view'  => $viewData['sub_model_view'],
      'sub_model'       => isset($viewData['sub_model']) ? $viewData['sub_model'] : [],
      'sub_model_title' => $viewData['sub_model_title'],
    ]);

    switch($viewData['sub_model_view']) {
      case 'table':
        $subModels[0]['sub_model_cols'] = $viewData['sub_model_cols'];
      break;
      case 'list':
        $subModels[0]['sub_model_list'] = $viewData['sub_model_list'];
      break;
      case 'file-uploader':
        $subModels[0]['sub_model_data'] = $viewData['sub_model_data'];
      break;
    }

  }
?>
@foreach($subModels as $subModelKey => $subModelValue)
@if( ! isset($closeRow) || (isset($closeRow) && $closeRow != false ))
<div class="row">
@endif
<!-- left column -->
<div class="{{ isset($viewData['sub_model_width']) ? $viewData['sub_model_width'] : 'col-md-6' }}">
  <div class="box box-primary">
  	<div class="box-header with-border">
  	 
  	</div><!-- /.box-header -->
	  <div class="box-body">
  		@if($subModelValue['sub_model_view'] == 'table')
  		<table id="" class="table table-bordered table-striped br-table br-table-vertical-middle" >
  		<thead>
  			<tr>
  				<th style="text-align: center; width: 50px">
  				@if($subModelValue['sub_model_view'] == 'table')
  				<button type="button" class="btn btn-default btn-sm br-table-add"><i class="fa fa-plus-square-o"></i></button>
  				@else
  				<input type="checkbox" name="check_all[]" value="1" />
  				@endif
  				</th>
  				@foreach($subModelValue['sub_model_cols'] as $key => $col)
          <th class="{{ $col['type']=='hidden'?'hide':'' }}" style="vertical-align: middle;">{{ isset($col['label']) ? $col['label'] : Lang::get('app.col_' . $key) }}</th>
  				@endforeach
  			</tr>
  		</thead>
  		<tbody>
  			<tr class="br-table-template hide">
  				<td><button type="button" class="btn btn-warning btn-sm br-table-rem"><i class="fa fa-minus-square-o"></i></button></td>
  				@foreach($subModelValue['sub_model_cols'] as $key => $col)
  				<td class="{{ $col['type'] == 'hidden' ? 'hide' : '' }}">
  				@if($col['type'] == 'hidden')
  				  <input type="hidden" class="form-control" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" value="">
  				@elseif($col['type'] == 'text')
  				  <input type="text" class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" value="{{ isset($viewData['model'][$key])?$viewData['model'][$key]:'' }}"  {{ isset($col['attr']) ? $col['attr'] : '' }} />
          @elseif(strpos($col['type'],'input') !== false)
            <input type="{{ explode('-',$col['type'])[1] }}" class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" value="{{ isset($viewData['model'][$key])?$viewData['model'][$key]:'' }}"  {{ isset($col['attr']) ? $col['attr'] : '' }} />
          @elseif($col['type'] == 'file-uploader')
            <div class="br-table-file-cont">
              <?php $image = strlen(old($viewData['column_prefix'] . $key) ) > 0 ? old($viewData['column_prefix'] . $key) : (isset($viewData['model']) ? (isset($viewData['model'][$key]) ? $viewData['model'][$key] : '') : '')  ?>
              <img data-file-input="file_{{ $viewData['column_prefix'] . $key }}" data-path="{{ url($col['img-path']) }}" src="{{ strlen($image) > 0 ? url($col['img-path'] . $image) : '' }}" class="img-responsive {{ isset($col['img-class']) ? $col['img-class'] : '' }}" {!! strlen($image) > 0 ? '' : 'style="display: none;"' !!} />
              <div data-file-input="file_{{ $viewData['column_prefix'] . $key }}" class="br-table-no-image" {!! strlen($image) > 0 ? 'style="display: none;"' : '' !!} ><div class="br-no-image-message">{{ Lang::get('app.no_image') }}</div></div>
              <div class="br-table-file-btns">
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <!-- The file input field used as target for the file upload widget -->
                    <input type="file" name="file_{{ $viewData['column_prefix'] . $key }}[]" id="file_{{ $viewData['column_prefix'] . $key }}[]" class="{{ isset($col['class']) ? $col['class'] : '' }}" {!! isset($col['attr']) ? $col['attr'] : '' !!} />
                    <input type="hidden" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" value="{{ $image }}" />
                </span>
                <button type="button" class="btn btn-warning btn-file-uploader" data-file-target="{{ $viewData['column_prefix'] . $key }}" data-file-container="file_{{ $viewData['column_prefix'] . $key }}" data-function="remove"><i class="fa fa-trash" aria-hidden="true"></i></button>
              </div>
              <span class="help-block">{{ str_replace(str_replace('_',' ',$viewData['column_prefix']), '',$errors->first($viewData['column_prefix'] . $key)) }}</span>
            </div>
          @elseif($col['type'] == 'select')
  				  <select class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]"  {{ isset($col['attr']) ? $col['attr'] : '' }} style="width: 100%">
  					<option value="">...</option>
  					@if(isset($col['select_values']))
  					@foreach($col['select_values'] as $select_row)
  					<option value="{{ $select_row['id'] }}" >{{ $select_row['value'] }}</option>
  					@endforeach
  					@endif
  				  </select>
  				@endif
  				</td>
  				@endforeach
  			</tr>
  			@if(isset($subModelValue['sub_model']))
  			@foreach($subModelValue['sub_model'] as $sub_model)
  			<tr >
  				<td><button type="button" class="btn btn-warning btn-sm br-table-rem"><i class="fa fa-minus-square-o"></i></button></td>
  				@foreach($subModelValue['sub_model_cols'] as $key => $col)
  				<td class="{{ $col['type'] == 'hidden' ? 'hide' : '' }}">
  				@if($col['type'] == 'hidden')
  				  <input type="hidden" class="form-control" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" value="{{ array_key_exists($key,$sub_model) ? $sub_model[$key] : '' }}">
  				@elseif($col['type'] == 'text')
  				  <input type="text" class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" value="{{ isset($viewData['model'][$key])?$viewData['model'][$key]:'' }}"  {{ isset($col['attr']) ? $col['attr'] : '' }} />
          @elseif(strpos($col['type'],'input') !== false)
            <input type="{{ explode('-',$col['type'])[1] }}" class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" value="{{ array_key_exists($key,$sub_model) ? $sub_model[$key] : '' }}"  {{ isset($col['attr']) ? $col['attr'] : '' }} />
          @elseif($col['type'] == 'file-uploader')
            <div class="br-table-file-cont">
              <?php $image = strlen(old($viewData['column_prefix'] . $key) ) > 0 ? old($viewData['column_prefix'] . $key) : (array_key_exists($key,$sub_model) ? $sub_model[$key] : '');  ?>
              <img data-file-input="file_{{ $viewData['column_prefix'] . $key }}" data-path="{{ url($col['img-path']) }}" src="{{ strlen($image) > 0 ? url($col['img-path'] . $image) : '' }}" class="img-responsive {{ isset($col['img-class']) ? $col['img-class'] : '' }}" {!! strlen($image) > 0 ? '' : 'style="display: none;"' !!} />
              <div data-file-input="file_{{ $viewData['column_prefix'] . $key }}" class="br-table-no-image" {!! strlen($image) > 0 ? 'style="display: none;"' : '' !!} ><div class="br-no-image-message">{{ Lang::get('app.no_image') }}</div></div>
              <div class="br-table-file-btns">
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <!-- The file input field used as target for the file upload widget -->
                    <input type="file" name="file_{{ $viewData['column_prefix'] . $key }}[]" id="file_{{ $viewData['column_prefix'] . $key }}[]" class="{{ isset($col['class']) ? $col['class'] : '' }}" {!! isset($col['attr']) ? $col['attr'] : '' !!} />
                    <input type="hidden" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" value="{{ $image }}" />
                </span>
                <button type="button" class="btn btn-warning btn-file-uploader" data-file-target="{{ $viewData['column_prefix'] . $key }}" data-file-container="file_{{ $viewData['column_prefix'] . $key }}" data-function="remove"><i class="fa fa-trash" aria-hidden="true"></i></button>
              </div>
              <span class="help-block">{{ str_replace(str_replace('_',' ',$viewData['column_prefix']), '',$errors->first($viewData['column_prefix'] . $key)) }}</span>
            </div>
          @elseif($col['type'] == 'select')
  				  <select class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $key }}[]"  {{ isset($col['attr']) ? $col['attr'] : '' }} style="width: 100%">
  					<option value="">...</option>
  					@if(isset($col['select_values']))
  					@foreach($col['select_values'] as $select_row)
  					<option value="{{ $select_row['id'] }}" {{ isset($sub_model[$key]) ? ($sub_model[$key] == $select_row['id'] ? 'selected="selected"' : '') : '' }}>{{ $select_row['value'] }}</option>
  					@endforeach
  					@endif
  				  </select>
  				@endif
  				</td>
  				@endforeach
  			</tr>
  			@endforeach
  			@endif
  		</tbody>
  		</table>
		  @elseif($subModelValue['sub_model_view'] == 'list')
 
  			@foreach($subModelValue['sub_model_list'][1] as $key => $col)
        <div class="col-md-12">
          <div style="">
              <label>
                  <input type="checkbox" name="{{ 'sub'.$subModelKey.'_' . $viewData['column_prefix'] . $subModelValue['sub_model_list'][0] }}[]" class="nes-checkbox" value="{{ $col['id'] }}"   {{ isset($subModelValue['sub_model']) ? (array_search($col['id'], array_column($subModelValue['sub_model'], $subModelValue['sub_model_list'][0] )) !== false ? 'CHECKED' : '') : '' }} />
                  <span>{{ $col['value'] }}</span>
              </label>
          </div>
       </div>
  			@endforeach
  		</table>
      @elseif($subModelValue['sub_model_view'] == 'file-uploader')
      <!--

      <div id="file-uploader-messages">
      </div>
      <div id="file-uploader-container">
      </div>-->
      <div id="progress" class="progress">
          <div class="progress-bar progress-bar-success"></div>
      </div>
      <div id="dropzone" class="">
        <span class="btn btn-success fileinput-button" style="display: none;">
            <i class="glyphicon glyphicon-plus"></i>

            <input type="file" id="fileupload" name="files[]" data-url="{{$subModelValue['sub_model_data']['upload_url']}}" multiple>
            <input type="hidden" id="fileupload-name" value="{{ 'sub' . $subModelKey .'_' . $viewData['column_prefix'] . $subModelValue['sub_model_data']['column_name']}}[]" >
        </span>
        <span class="dropzone-message">{{ Lang::get('app.drop_files_here') }}</span>
        @foreach($subModelValue['sub_model'] as $itemKey => $itemValue)
        <div class="file-uploader-preview">
          <p>
            <img src="{{ $subModelValue['sub_model_data']['img_path'] . '/' . $itemValue['image_path'] }}">
          </p>
          <div class="file-uploader-remove"><i class="fa fa-times-circle-o"></i></div>
 
        </div>
        @endforeach
      </div>
      <div id="file-uploader-container">
      </div>
  		@endif
	  </div><!-- /.box-body -->
  </div><!-- /.box -->
</div><!-- /.col-lg-6 -->
@if( ! isset($closeRow) || (isset($closeRow) && $closeRow != false ))
</div><!-- /.row -->
@endif
@endforeach
