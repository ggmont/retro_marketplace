 
@foreach($viewData['model_cols'] as $key => $col)
    <?php

    $label = '';
    $lang = isset($viewData['lang'])?$viewData['lang']:'app';

    if(isset($col['label']))
      //$label = $lang.'.col_'.$col['label'];
      $label = $col['label'];
    else if(substr($key,0,4) == 'rel_') {
      $label = $lang.'.col_'.str_replace('rel_','',$key);
    }
    else {
      $label = $lang.'.col_'.$key;
    }
    ?>
      @if($viewData['column_prefix'] . $key == 'br_c_language')
        <div class="form-group"> 
          <label for="{{ $viewData['column_prefix'] . $key  }}">{{ Lang::get($label) }}

           
            @php($selectedValue = [] )
            @if(isset($viewData['plat_dic']))
              @foreach($viewData['plat_dic'] as $key3)
                @php(array_push($selectedValue, $key3) )
              @endforeach
            @endif 

          </label>
          <select @if($viewData['column_prefix'] . $key == 'br_c_language') multiple @endif class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ $viewData['column_prefix'] . $key }}" name="{{ $viewData['column_prefix'] . $key }}@if($viewData['column_prefix'] . $key == 'br_c_language')[]@endif"  {{ isset($col['attr']) ? $col['attr'] : '' }} {{ isset($col['dependency']) ?'dependency='.$viewData['column_prefix'].$col['dependency'] : '' }} style="width: 100%;">
          <option value="">...</option>
          @if(isset($col['select_values']))
          @foreach($col['select_values'] as $select_row)
          <option value="{{ $select_row['id'] }}" {{ in_array($select_row['id'], $selectedValue) ? 'selected="selected"' : '' }} {{ isset($select_row['dependency'])?'dependency='.$select_row['dependency']:'' }} {!! isset($select_row['attr']) ? $select_row['attr'] : '' !!} >{{ $select_row['value'] }}</option>
          @endforeach
          @endif
          </select>

          <span class="help-block">{{ str_replace(str_replace('_',' ',$viewData['column_prefix']), '',$errors->first($viewData['column_prefix'] . $key)) }}</span>
        </div>
      @endif
@endforeach

<div class="row">
<!-- left column -->
<div class="col-md-12">
  <!-- magaya connection -->
  <div class="box box-primary">
	<div class="box-header with-border">
	  <h3 class="box-title">{{ isset($viewData['form_sec_title'])? $viewData['form_sec_title']: '' }}</h3>
	</div><!-- /.box-header -->
	<!-- form start -->
	  <div class="box-body">
		@foreach($viewData['model_cols'] as $key => $col)
    <?php

    $label = '';
    $lang = isset($viewData['lang'])?$viewData['lang']:'app';

    if(isset($col['label']))
      //$label = $lang.'.col_'.$col['label'];
      $label = $col['label'];
    else if(substr($key,0,4) == 'rel_') {
      $label = $lang.'.col_'.str_replace('rel_','',$key);
    }
    else {
      $label = $lang.'.col_'.$key;
    }
    ?>
		@if($col['type'] == 'hidden')
		<div class="form-group hide">
		  <input type="hidden" class="form-control" name="{{ $viewData['column_prefix'] . $key }}" value="{{ strlen(old($viewData['column_prefix'] . $key)) > 0 ? old($viewData['column_prefix'] . $key) : (isset($viewData['model'][$key])?$viewData['model'][$key]:'' ) }}">
		</div>
		@elseif(strpos($col['type'],'input')!== false)
		<?php $type = explode('-',$col['type'])[1]; ?>
		<div class="form-group">
		  <label for="{{ $viewData['column_prefix'] . $key  }}">Pregunta</label>
		  @if($type == 'datetime')
		  <div class="input-group date datetimepicker" >
		  	<input type="text" class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ $viewData['column_prefix'] . $key }}" name="{{ $viewData['column_prefix'] . $key }}" value="{{ strlen(old($viewData['column_prefix'] . $key)) > 0 ? old($viewData['column_prefix'] . $key) : (isset($viewData['model'][$key])?$viewData['model'][$key]:'' ) }}"  {!! isset($col['attr']) ? $col['attr'] : '' !!} />
  			<span class="input-group-addon">
  				<i class="fa fa-calendar"></i>
  			</span>
		  </div>
      @elseif($type == 'autocomplete')
      <input type="{{ explode('-',$col['type'])[1] }}" class="form-control {{ isset($col['class']) ? $col['class'] : '' }}"
        id="{{ $viewData['column_prefix'] . $key }}" name="{{ $viewData['column_prefix'] . $key }}"
        value="{{ strlen(old($viewData['column_prefix'] . $key)) > 0 ? old($viewData['column_prefix'] . $key) : (isset($viewData['model'][$key])?$viewData['model'][$key]:'' ) }}"
         {!! isset($col['attr']) ? $col['attr'] : '' !!} data-typeahead="{{ isset($col['data-typeahead']) ? $col['data-typeahead'] : '' }}" autocomplete="off"/>
      @elseif($type == 'slug')
      <input type="text" class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ $viewData['column_prefix'] . $key }}" name="{{ $viewData['column_prefix'] . $key }}" value="{{ strlen(old($viewData['column_prefix'] . $key)) > 0 ? old($viewData['column_prefix'] . $key) : (isset($viewData['model'][$key])?$viewData['model'][$key]:'' ) }}"  {!! isset($col['attr']) ? $col['attr'] : '' !!} READONLY />
      @else
		  <input type="{{ explode('-',$col['type'])[1] }}" class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ $viewData['column_prefix'] . $key }}" name="{{ $viewData['column_prefix'] . $key }}" value="{{ strlen(old($viewData['column_prefix'] . $key)) > 0 ? old($viewData['column_prefix'] . $key) : (isset($viewData['model'][$key])?$viewData['model'][$key]:'' ) }}"  {!! isset($col['attr']) ? $col['attr'] : '' !!} />
		  @endif
		  <span class="help-block">{{ str_replace(str_replace('_',' ',$viewData['column_prefix']), '',$errors->first($viewData['column_prefix'] . $key)) }}</span>
		</div>
    @elseif($col['type'] == 'file-uploader')
      @if($col['file-type'] == 'image')
      <div class="form-group">
        <?php $image = strlen(old($viewData['column_prefix'] . $key) ) > 0 ? old($viewData['column_prefix'] . $key) : (isset($viewData['model']) ? (isset($viewData['model'][$key]) ? $viewData['model'][$key] : '') : '')  ?>
        <img data-file-input="file_{{ $viewData['column_prefix'] . $key }}" data-path="{{ url($col['img-path']) }}" src="{{ strlen($image) > 0 ? url($col['img-path'] . $image) : '' }}" alt="{{ Lang::get('app.image_alt_profile') }}" class="img-responsive {{ isset($col['img-class']) ? $col['img-class'] : '' }}" {!! strlen($image) > 0 ? '' : 'style="display: none;"' !!} />
        <div data-file-input="file_{{ $viewData['column_prefix'] . $key }}" class="br-no-image" {!! strlen($image) > 0 ? 'style="display: none;"' : '' !!} ><div class="br-no-image-message">{{ Lang::get('app.no_image') }}</div></div>
        <div class="row">
          <div class="col-md-6">
            <span class="btn btn-success btn-block fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>{{ Lang::get('app.profile_select_photo') }}</span>
                <!-- The file input field used as target for the file upload widget -->
                <input type="file" name="file_{{ $viewData['column_prefix'] . $key }}" id="file_{{ $viewData['column_prefix'] . $key }}" class="{{ isset($col['class']) ? $col['class'] : '' }}" {!! isset($col['attr']) ? $col['attr'] : '' !!} />
                <input type="hidden" name="{{ $viewData['column_prefix'] . $key }}" value="{{ $image }}" />
            </span>
          </div>
          <div class="col-md-6">
            <button type="button" class="btn btn-warning btn-block btn-file-uploader" data-file-target="{{ $viewData['column_prefix'] . $key }}" data-file-container="file_{{ $viewData['column_prefix'] . $key }}" data-function="remove">{{ Lang::get('app.profile_remove_photo') }}</button>
          </div>
        </div>
        <span class="help-block">{{ str_replace(str_replace('_',' ',$viewData['column_prefix']), '',$errors->first($viewData['column_prefix'] . $key)) }}</span>
      </div>
      @endif
		@elseif($col['type'] == 'readonly')
		<div class="form-group">
      <label for="{{ $viewData['column_prefix'] . $key  }}">{{ Lang::get($label) }}</label>
		  <p class="form-control-static">{{ isset($col['value'])?$col['value'] : (isset($viewData['model'][$key])?$viewData['model'][$key]:'') }}</p>
		</div>
		@elseif($col['type'] == 'checkbox')
		<div class="form-group">
      <label for="{{ $viewData['column_prefix'] . $key  }}">{{ Lang::get($label) }}</label>
		  <select class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ $viewData['column_prefix'] . $key }}" name="{{ $viewData['column_prefix'] . $key }}"  {{ isset($col['attr']) ? $col['attr'] : '' }} >
			<option value="">...</option>
			@if(isset($col['select_values']))
			@foreach($col['select_values'] as $select_row)
			<option value="{{ $select_row['id'] }}" {{ isset($viewData['model'][$key]) ? ($viewData['model'][$key] == $select_row['id'] ? 'selected="selected"' : '') : '' }}>{{ $select_row['value'] }}</option>
			@endforeach
			@endif
		  </select>
		  <span class="help-block">{{ str_replace(str_replace('_',' ',$viewData['column_prefix']), '',$errors->first($viewData['column_prefix'] . $key)) }}</span>
		</div>
    @elseif($col['type'] == 'textarea')
    <div class="form-group ">
      <label for="{{ $viewData['column_prefix'] . $key  }}">{{ Lang::get($label) }}</label>
      <textarea class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ $viewData['column_prefix'] . $key }}" name="{{ $viewData['column_prefix'] . $key }}" {!! isset($col['attr']) ? $col['attr'] : '' !!}>{{ strlen(old($viewData['column_prefix'] . $key)) > 0 ? old($viewData['column_prefix'] . $key) : (isset($viewData['model'][$key])?$viewData['model'][$key]:'' ) }}</textarea>
      <span class="help-block">{{ str_replace(str_replace('_',' ',$viewData['column_prefix']), '',$errors->first($viewData['column_prefix'] . $key)) }}</span>
    </div>
    @elseif($col['type'] == 'select')
      @if($viewData['column_prefix'] . $key == 'br_c_platform')
        <div class="form-group">
          <label for="{{ $viewData['column_prefix'] . $key  }}">{{ Lang::get($label) }}
           
            @php($selectedValue = [] )
            @if(isset($viewData['plat_dic']))
              @foreach($viewData['plat_dic'] as $key3)
                @php(array_push($selectedValue, $key3) )
              @endforeach
            @endif
          </label>
          <select @if($viewData['column_prefix'] . $key == 'br_c_platform') multiple @endif class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="{{ $viewData['column_prefix'] . $key }}" name="{{ $viewData['column_prefix'] . $key }}@if($viewData['column_prefix'] . $key == 'br_c_platform')[]@endif"  {{ isset($col['attr']) ? $col['attr'] : '' }} {{ isset($col['dependency']) ?'dependency='.$viewData['column_prefix'].$col['dependency'] : '' }} style="width: 100%;">
          <option value="">...</option>
          @if(isset($col['select_values']))
          @foreach($col['select_values'] as $select_row)
          <option value="{{ $select_row['id'] }}" {{ in_array($select_row['id'], $selectedValue) ? 'selected="selected"' : '' }} {{ isset($select_row['dependency'])?'dependency='.$select_row['dependency']:'' }} {!! isset($select_row['attr']) ? $select_row['attr'] : '' !!} >{{ $select_row['value'] }}</option>
          @endforeach
          @endif
          </select>

          <span class="help-block">{{ str_replace(str_replace('_',' ',$viewData['column_prefix']), '',$errors->first($viewData['column_prefix'] . $key)) }}</span>
        </div>
      @else

 



        <div class="form-group">
          <label>Habilitado</label>
          <select class="form-control {{ isset($col['class']) ? $col['class'] : '' }}"  id="{{ $viewData['column_prefix'] . $key }}" name="{{ $viewData['column_prefix'] . $key }}"  {{ isset($col['attr']) ? $col['attr'] : '' }} {{ isset($col['dependency']) ?'dependency='.$viewData['column_prefix'].$col['dependency'] : '' }} style="width: 100%;">
          <option value="">...</option>
          <?php $selectedValue=strlen(old($viewData['column_prefix'] . $key)) > 0 ? old($viewData['column_prefix'] . $key) : (isset($viewData['model'][$key])?$viewData['model'][$key]:'' ) ?>
          @if(isset($col['select_values']))
          @foreach($col['select_values'] as $select_row)
          <option value="{{ $select_row['id'] }}" {{ $selectedValue == $select_row['id'] ? 'selected="selected"' : '' }} {{ isset($select_row['dependency'])?'dependency='.$select_row['dependency']:'' }} {!! isset($select_row['attr']) ? $select_row['attr'] : '' !!} >{{ $select_row['value'] }}</option>
          @endforeach
          @endif
          </select>
          <span class="help-block">{{ str_replace(str_replace('_',' ',$viewData['column_prefix']), '',$errors->first($viewData['column_prefix'] . $key)) }}</span>
        </div>
      @endif

    @elseif($col['type'] == 'html-editor')
    <div class="form-group br-html-editor-container">
      <label>Respuesta</label>
      <textarea
      class="form-control {{ isset($col['class']) ? $col['class'] : '' }}"
      id="file-picker" name="{{ $viewData['column_prefix'] . $key }}"
      {!! isset($col['attr']) ? $col['attr'] : '' !!}>
{{ strlen(old($viewData['column_prefix'] . $key)) > 0 ? old($viewData['column_prefix'] . $key) : (isset($viewData['model'][$key]) ? $viewData['model'][$key] : '') }}
</textarea>
      <!-- <div class="clearfix">
        <button type="button" class="btn btn-default col-sm-6 br-editor-toggle">{{Lang::get('app.toogle_editor')}}</button>
        <button type="button" class="btn btn-default col-sm-6 br-editor-full">{{Lang::get('app.full_screen')}}</button>
      </div> -->
      <span class="help-block">{{ str_replace(str_replace('_',' ',$viewData['column_prefix']), '',$errors->first($viewData['column_prefix'] . $key)) }}</span>
    </div>
		@endif
		@endforeach
	  </div><!-- /.box-body -->
  </div><!-- /.box -->
</div><!-- /.col-lg-6 -->
@if( ! isset($closeRow) || ( isset($closeRow) && $closeRow != false ))
</div><!-- /.row -->
@endif
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script> 
    $(document).ready(function(){

 var multipleCancelButton = new Choices('#choices-multiple-remove-button', {
 removeItemButton: true,
 maxItemCount:5,
 searchResultLimit:5,
 renderChoiceLimit:5
 });


 });</script>