<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>RGM | PROFESSIONAL SELLER MODE</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('special/css/adminlte.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('special/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/goodgames_special.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nes/nes_prueba.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
    <!-- Default CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/special_style.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
    <style>
        .tabs {
            overflow: hidden;
        }

        .tabs-content {
            max-height: 0;
            transition: all 0.5s;
        }

        input:checked+.tabs-label .test {
            background-color: #000;
        }

        input:checked+.tabs-label .test svg {
            transform: rotate(180deg);
            stroke: #fff;
        }

        input:checked+.tabs-label::after {
            transform: rotate(90deg);
        }

        input:checked~.tabs-content {
            max-height: 100vh;
        }

        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Jost', sans-serif;
            font-weight: 800;
        }

    </style>
    @livewireStyles
    @yield('content-css-include')

</head>

<body
    class="hold-transition sidebar-mini   sidebar-mini {{ Cookie::get('sidebar_collapse') !== null && Cookie::get('sidebar_collapse') == 'Y' ? 'sidebar-collapse' : '' }}">
    <div class="wrapper">
        <div class="preloader flex-column justify-content-center align-items-center">
            <img class="animation__wobble" src="{{ asset('img/RGM.png') }}" alt="RGM" height="60" width="70">
        </div>
        <nav class="main-header navbar navbar-expand navbar-dark">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i
                            class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="/" class="nav-link">Inicio</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Salir</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="ml-auto navbar-nav">
                <!-- Navbar Search -->
                <li class="nav-item">
                    <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                        <i class="fas fa-search"></i>
                    </a>
                    <div class="navbar-search-block">
                        <form class="form-inline">
                            <div class="input-group input-group-sm">
                                <input class="form-control form-control-navbar" type="search" placeholder="Buscar"
                                    aria-label="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-navbar" type="submit">
                                        <i class="fas fa-search"></i>
                                    </button>
                                    <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </li>

                <!-- Messages Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown"
                        href="https://www.retrogamingmarket.eu/account/messages">
                        <i class="far fa-comments"></i>
                        @isset($viewData['header_messages'])
                            @if (count($viewData['header_messages']) > 0)
                                <span
                                    class="badge badge-danger navbar-badge">{{ count($viewData['header_messages']) }}</span>
                            @endif
                        @endisset
                    </a>

                </li>
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-bell"></i>
                        @if (count($viewData['header_notifications']) > 0)
                            @if ($viewData['c_h_n'] > 0)
                                <span class="badge badge-warning navbar-badge">{{ $viewData['c_h_n'] }}</span>
                            @endif
                        @endif
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        @if ($viewData['c_h_n'] > 0)
                            <span class="dropdown-item dropdown-header">
                                {{ $viewData['c_h_n'] }} Notificaciones
                            </span>
                            <div class="dropdown-divider"></div>

                            @foreach ($viewData['header_notifications'] as $key)
                                @php($stat = true)
                                @if ($key->paid_out == 'N' || $key->paid_out == 'P')
                                    @php($stat = false)
                                @else
                                    @php($stat = true)
                                @endif
                                @if ($key->tipoSeller)
                                    <a href="#" class="text-sm dropdown-item">
                                        <i class="mr-2"></i>
                                        @if ($key->status == 'DD')
                                            {{ __('El pedido fue entregado') }}
                                        @endif

                                        @if ($key->status == 'RP')
                                            {{ __('Un pedido requiere su atención') }}

                                        @endif

                                        @if ($key->status == 'CR')
                                            {{ __('Tiene un pedido pendiente por enviar') }}

                                        @endif

                                        @if ($key->status == 'CW')
                                            {{ __('Tiene una solicitud de cancelación de pedido') }}

                                        @endif

                                        @if ($key->status == 'PC')
                                            {{ __('Tiene una solicitud de cancelación de un pedido pagado') }}

                                        @endif
                                    </a>
                                    <div class="dropdown-divider"></div>
                                @else
                                @endif
                            @endforeach
                        @else
                            <span class="dropdown-item dropdown-header">
                                No hay Nada por Ahora
                            </span>
                            @foreach ($viewData['header_notifications'] as $key)
                                @php($stat = true)
                                @if ($key->paid_out == 'N' || $key->paid_out == 'P')
                                    @php($stat = false)
                                @else
                                    @php($stat = true)
                                @endif
                                @if ($key->tipoSeller)
                                    <a href="#" class="dropdown-item">
                                        <i class="mr-2 fas fa-envelope"></i> 4 new messages
                                        <span class="float-right text-sm text-muted">3 mins</span>
                                    </a>
                                    <div class="dropdown-divider"></div>
                                @else
                                @endif
                            @endforeach
                            <a href="#" class="dropdown-item dropdown-footer">Mirar Todo</a>
                        @endif
                    </div>
                </li>
            </ul>
        </nav>
