<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('/SpecialZone') }}" class="brand-link">
        <img src="{{ asset('img/RGM.png') }}" alt="RGM" class="brand-image" style="opacity: .8">
        <span class="brand-text font-weight-light">Special Zone</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="pb-3 mt-3 mb-3 user-panel d-flex">
            <div class="image">
                <img src="{{ asset('img/profile-picture-not-found.png') }}" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <br>
            <div class="info">
                <a href="{{ url('/account/profile') }}" class="retro d-block">{{ auth()->user()->user_name }}</a>
            </div>
        </div>

        <!-- SidebarSearch Form -->


        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-item menu">
                    <a href="{{ url('/') }}" class="nav-link">
                        <i class="fas fa-gamepad"></i>
                        <p>
                            RGM
                            <span class="right badge badge-warning">Start</span>
                        </p>
                    </a>
                </li>

                <li class="nav-item menu">
                    <a href="{{ url('/SpecialZone') }}" class="nav-link">
                        <i class="fas fa-home"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>

                <li class="nav-item menu-close">
                    <a href="{{ route('images_product_edition') }}" class="nav-link">
                        <i class="fas fa-pencil-ruler"></i>
                        <p>
                            Ediciones
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        @if (App\SysUserRoles::where('user_id', Auth::id())->where('role_id', 5)->count() > 0)
                            <li class="nav-item">
                                <a href="{{ route('images_product_edition') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Imagenes</p>
                                </a>
                            </li>
                        @endif
                        @if (App\SysUserRoles::where('user_id', Auth::id())->where('role_id', 2)->count() > 0)
                            <li class="nav-item">
                                <a href="{{ route('images_trend') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Perfil Publico</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('images_trend') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Productos Destacados</p>
                                </a>
                            </li>
                        @endif



                    </ul>


                </li>



            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
