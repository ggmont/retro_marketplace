@extends('brcode.layout.app')
@section('content-css-include')
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    {{ $viewData['form_title'] }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li><a href="{{ $viewData['form_url_list'] }}">{{ $viewData['form_list_title'] }}</a></li>
    <li class="active">{{ $viewData['form_title'] }}</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
    <form action="{{ $viewData['form_url_post'] }}" method="post">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	
	@include('brcode.layout.app_form')
	
	@include('brcode.layout.app_form_sub')

	@include('brcode.layout.app_form_buttons')

	</form>
</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="{{ asset('assets/timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('assets/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('assets/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('assets/input-mask/jquery.inputmask.numeric.extensions.js') }}"></script>
<script src="{{ asset('assets/input-mask/jquery.inputmask.extensions.js') }}"></script>
@endsection
@section('content-script')
<script type="text/javascript">
$(function() {
	
});
</script>
@endsection