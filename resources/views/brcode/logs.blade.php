@extends('brcode.layout.app')
@section('content-css-include')
<link rel="stylesheet" href="{{ asset('assets/datatables/dataTables.bootstrap.css') }}">
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    {{ Lang::get('app.logs') }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li class="active">{{ Lang::get('app.logs') }}</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
	<!-- column -->
	<div class="col-md-12">
	  <!-- magaya connection -->
	  <div class="box">
		<div class="box-header">
		  <h3 class="box-title">{{ Lang::get('app.logs') }}</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
		  <table id="table_logs" class="table table-bordered table-striped">
			<thead>
			  <tr>
			  	<th>{{ Lang::get('app.modified_on') }}</th>
				<th>{{ Lang::get('app.filename') }}</th>
				<th>{{ Lang::get('app.size') }}</th>
				<th>{{ Lang::get('app.options') }}</th>
			  </tr>
			</thead>
			<tbody>
				@foreach ($viewData['files'] as $file)
				<tr>
					<td>{{ $file->filelastmodification }}</td>
					<td>{{ $file->filename }}</td>
					<td>{{ $file->filesize }}</td>
					<td class="text-center"><button type="button" data-loading-text="{{ Lang::get('app.loading') }}" class="btn btn-default btn-xs btn-view" view-file="{{ $file->filefullpath }}"><i class="fa fa-search"></i></button> <input type="hidden" name="filepath[]" value="{{ $file->filefullpath }}" /></td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
			  <tr>
			  	<th>{{ Lang::get('app.modified_on') }}</th>
				<th>{{ Lang::get('app.filename') }}</th>
				<th>{{ Lang::get('app.size') }}</th>
				<th>{{ Lang::get('app.options') }}</th>
			  </tr>
			</tfoot>
		  </table>
		  <input type="hidden" name="_token" value="{{ csrf_token() }}">
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
 
    </div><!-- /.col-md-12 -->
    </div><!-- /.row -->
	
	<div id="modal_file_content" class="modal fade" tabindex="-1" role="dialog">
	  <div class="modal-dialog modal-lg">
		<div class="modal-content ">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">{{ Lang::get('app.filecontent') }}</h4>
		  </div>
		  <div class="modal-body">
			<div class="row">
				<div class="col-md-12">
					<textarea class="form-control" rows="25"></textarea>
				</div>
			</div>
		  </div>
		  <div class="modal-footer">
			<input type="hidden" name="modalFilePath" value="" />
			<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('app.close') }}</button>
		  </div>
		</div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="{{ asset('assets/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/datatables/dataTables.bootstrap.min.js') }}"></script>
@endsection
@section('content-script')
<script type="text/javascript">

var xhr;

$(function() {
	
	
	
	$('#table_logs').DataTable({
	  "paging": true,
	  "lengthChange": true,
	  "searching": true,
	  "ordering": true,
	  "info": true,
	  "autoWidth": false,
	  "order": [[ 0, "desc" ]],
	  "columns": [
		{ "type": "date" },
		null,
		null,
		null,
		]
	});
	
	$(document).ready(function() {
		
		var xhr = null;
		var m = $('#modal_file_content');
		var interval;
		
		$(document).on('click','.btn-view',function() {

			var td = $(this).closest('td')
			
			var b = $(this);
			
			b.button('loading');
			
			var file = td.find('input[name="filepath[]"]').val();
			
			m.find('input[name="modalFilePath"]').val(file);
			
			if(xhr) xhr = null;
			
			xhr = $.ajax({
				url: "{{ url('/get_file') }}",
				type: "POST",
				data: { _token: $('input[name="_token"]').val(), filePath: file },
			});
			
			xhr.promise().done(function(data) {
				m.find('.modal-body textarea').val(data);
				m.modal('show');
				b.button('reset');
				interval = setInterval(updateLog,15000);
			});
			
			xhr.promise().fail(function(data) {
				m.find('.modal-body textarea').val('{{ Lang::get('app.cant_get_content') }}');
				m.modal('show');
				b.button('reset');
			});
						
			
		});
		
		m.on('hidden.bs.modal',function(e) {
			m.find('input[name="modalFilePath"]').val('');
			clearInterval(interval);
			
		});
		
		function updateLog() {
			
			var file = m.find('input[name="modalFilePath"]').val();
			
			if(xhr) xhr = null;
			
			xhr = $.ajax({
				url: "{{ url('/get_file') }}",
				type: "POST",
				data: { _token: $('input[name="_token"]').val(), filePath: file },
			});
			
			xhr.promise().done(function(data) {
				m.find('.modal-body textarea').val(data);
			});
			
			xhr.promise().fail(function(data) {
				m.find('.modal-body textarea').val('{{ Lang::get('app.cant_get_content') }}');
				m.modal('show');
			});		
			
		}
		
	});
	

});
</script>
@endsection