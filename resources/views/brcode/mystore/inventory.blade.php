@extends('brcode.layout.app')
@section('content-css-include')
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="{{ asset('assets/jQuery-FileUpload/css/jquery.fileupload.css') }}">
<link rel="stylesheet" href="{{ asset('assets/Jcrop/css/jquery.Jcrop.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    {{ $viewData['form_title'] }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li><a href="{{ $viewData['form_url_list'] }}">{{ $viewData['form_list_title'] }}</a></li>
    <li class="active">{{ $viewData['form_title'] }}</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
    <form action="{{ $viewData['form_url_post'] }}" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

      {{ $closeRow = false }}

			@include('brcode.layout.app_form')

			@include('brcode.layout.app_form_sub')

      @if((isset($closeRow) && $closeRow == false ))
      </div><!-- /.row -->
      @endif

			@include('brcode.layout.app_form_buttons')



		</form>
</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="{{ asset('assets/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
<!-- The basic File Upload plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
<!-- The File Upload processing plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>

<script src="{{ asset('assets/Jcrop/js/jquery.Jcrop.js') }}"></script>

@endsection
@section('content-script')
<script type="text/javascript">
$(function() {
		$('#br-btn-delete').click(function() {
		$(this).button('loading');

    var buttons = [
			{addClass: 'btn btn-primary btn-sm pull-left', text: '{{ Lang::get('app.delete') }}', onClick: function($noty) {$noty.close();deleteRecord()} },
			{addClass: 'btn btn-danger btn-sm pull-right', text: '{{ Lang::get('app.cancel') }}', onClick: function($noty) {$noty.close();$('#br-btn-delete').button('reset');} },
		];

		presentNotyMessage('{{Lang::get('app.delete_are_you_sure') }}','warning',buttons,1);
	});

  $('select.br-select2').select2();

  $('[data-date-format]').each(function() {
    var format = $(this).data('date-format');
    $(this).parent().datetimepicker({format: format});
  });

  $('[data-number]').each(function() {
    $(this).number(true,$(this).data('number'))
  });

  uploadButton = $('<button/>')
    .attr('type','button')
    .addClass('btn btn-primary')
    .text('Upload')
    .on('click', function () {
        var $this = $(this),
            data = $this.data();
        $this
            .off('click')
            .text('Abort')
            .on('click', function () {
                $this.remove();
                data.abort();
            });
        data.submit().always(function () {
            $this.remove();
        });
    });

  editButton = $('<button/>')
  .attr('type','button')
  .addClass('btn btn-primary')
  .text('Edit')
  .on('click', function () {
      var $this = $(this),
          data = $this.data();
      $this.text('Save');
      data.imgEl.Jcrop({aspectRatio: 1/1});
  });

	function deleteRecord() {
		var url = '{{ $viewData['form_url_post_delete'] }}';
		var form = $('<form action="' + url + '" method="post">' +
			'<input type="hidden" name="_token" value="{{ csrf_token() }}">' +
			'<input type="hidden" name="model_id" value="{{ $viewData['model_id'] > 0 ? $viewData['model_id'] : '' }}" />' +
			'</form>');
		$('body').append(form);
		form.submit();
	}

  $('#fileupload').fileupload({
    singleFileUploads: true,
    sequentialUploads: true,
    limitConcurrentUploads: 1,
    dropZone: $('#dropzone'),
    add: function (e, data) {
      data.context = $('<div/>').addClass('file-uploader-preview').appendTo('#dropzone');
      $.each(data.files, function (index, file) {
          if(file.size > 3072000) {
            var msg = $('<p/>')
              .text('{{ Lang::get('app.uploader_file_size_bigger') }}'.replace('_FILENAME_',file.name));
            msg.appendTo($('#file-uploader-messages'));
            data.context.remove();
            return false;
          }
          else if($.inArray(file.name.split('.').pop().toLowerCase(),['gif','png','jpg','jpeg']) == -1) {
            var msg = $('<p/>')
              .text('{{ Lang::get('app.uploader_file_ext_not_supported') }}'.replace('_FILENAME_',file.name));
            msg.appendTo($('#file-uploader-messages'));
            data.context.remove();
            return false;
          }
          var img = $('<img/>').attr('src',URL.createObjectURL(file));
          data.imgEl = img;
          data.inputName = $('#dropzone').find('#fileupload-name').val();
          var node = $('<p/>')
              .append(img);
          node.appendTo(data.context);

      });
      data.submit();
    },
    done: function (e, data) {
      var img = data.imgEl;
      var parent = img.closest('.file-uploader-preview');
      var removeButton = $('<div/>').html('<i class="fa fa-times-circle-o"></i>').addClass('file-uploader-remove').appendTo(parent);
      var input = $('<input/>').attr('type','hidden').attr('name',data.inputName).attr('value',data.result.name).appendTo(parent);

    },
    progress: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    },
  });

  $(document).on('click','.file-uploader-remove',function() {
    var parent = $(this).closest('.file-uploader-preview');
    var image = parent.find('input[type=hidden]').val();
    $.ajax({
      url: '{{ url('/11w5Cj9WDAjnzlg0/my_store/inventory/remove_file') }}',
      data: {'_token': $('form input[name="_token"]:eq(0)').val(), img: image},
      type: 'POST',
      success: function(data) {
        parent.hide('fade',250,function() { $(this).remove() } );
      },
      error: function(data) {

      }
    })
  });

  $('#dropzone').on('dragenter dragover', function() {
    $(this).addClass('hover');
  });

  $('#dropzone').on('dragleave dragexit drop', function() {
    $(this).removeClass('hover');
  });
});
</script>
@endsection
