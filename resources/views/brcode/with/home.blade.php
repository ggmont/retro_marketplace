@extends('brcode.layout.app')
@section('content-css-include')
<link rel="stylesheet" href="{{ asset('assets/datatables/datatables.min.css') }}">
@endsection
@section('content')
<section class="content-header">
  <h1>
  {{ Lang::get('app.withdrawals') }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li class="active">{{ Lang::get('app.withdrawals') }}</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="box">
  <div class="box-header">
    <div class="row">
      <div class="col-md-3">
        <button class="btn btn-primary btn-block" data-id="1" onclick="populateTable(this)">Withdrawals</button>
      </div>
      <div class="col-md-3">
        <button class="btn btn-warning btn-block" data-id="2" onclick="populateTable(this)">Withdrawals Pending</button>
      </div>
      <div class="col-md-3">
        <button class="btn btn-info btn-block" data-id="3" onclick="populateTable(this)">Fees</button>
      </div>
      <div class="col-md-3">
        <button class="btn btn-info btn-block" data-id="4" onclick="populateTable(this)">Purchases</button>
      </div>
    </div>
  </div><!-- /.box-header -->
  <div class="box-body">
    <table id="" class="table table-bordered table-striped br-datatable-withdrawal">
      <thead>
        <tr>
          <th style="width:8%">Status</th>
          <th>Total</th>
          <th>Email </th>
          <th>Name Lastname</th>
          <th>User-Unic</th>
          <th>Transaction</th>
          <th>Date</th>
          <th>Date</th>
          <th style="width:5%">Actions</th>
        </tr>
      </thead>
      <tbody class="fillDataTable">
      
      </tbody>
    </table>
  </div><!-- /.box-body -->
</div><!-- /.box -->
</section><!-- /.content -->
<form action="{{ url('/11w5Cj9WDAjnzlg0/users/confirm_withdrawal')}}" method="post">
{{ csrf_field() }}
<div  class="modal fade" tabindex="-1" role="dialog" id="mystatus">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ __('Confrimar envío de dinero') }}</h4>
	  </div>
	  
      <div class="modal-body">
		  	<br>
				<div class="form-group" style="padding: 10px;">
					<label for="credit">{{ __('Recuerde que al confirmar, acepta haber envía el dinero al cliente, esta opción es irreversible.') }}</label>
					<div id="foundRemove"></div>
				</div>
				<div class="form-group" style="padding: 10px;">
					<label for="pass">{{ __('Confirmar contraseña') }}</label>
					<input type="password" name="pass" class="form-control">
				</div>
				<input  id="orderDetails" name="orderDetails" type="hidden" >
	  </div>
	  
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Cancelar') }}</button>
        <button type="submit" class="btn btn-warning">{{ __('Guardar') }}</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>
@endsection
@section('content-script-include')
<script src="{{ asset('assets/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/datatables/Responsive-2.2.0/js/dataTables.responsive.min.js') }}"></script>
@endsection
@section('content-script')
<script>
  function populateTable(x){
    var table = $('.br-datatable-withdrawal').DataTable(); 
    if($(x).data("id") == 1){
      console.log(1);
      $.get("{{ url('/11w5Cj9WDAjnzlg0/showwiths')}}", function(data, status){
        var i = 0;
        $.each(data, function (key, value) {  
          var rows = table.rows().remove().draw();
          for (x in value) {
            var rowNode = table.row.add( [ 
            ( value[x].d9 == 'N' ? "<td> <span class='label label-warning'> " + value[x].d0 + "</span> </td>" : (value[x].d0 == 'Credito enviado' ? "<td> <span class='label label-success'> " + value[x].d0 + "</span> </td>" : "<td> <span class='label label-info'> " + value[x].d0 + "</span> </td>") ), 
            "<td>" + value[x].d4+ "</td>", 
            "<td>" + value[x].d5+ "</td>",
            "<td>" + value[x].d6+ "</td>", 
            "<td>" + value[x].d2+ "</td>", 
            "<td>" + value[x].d1 + "</td>",
            "<td>" + value[x].d3+ "</td>", 
            "<td>" + value[x].d31+ "</td>", 
            ( value[x].d9 == 'N' 
            ? 
            "<td> <a href='#' data-d1='" + value[x].d1 + "' data-d6='" + value[x].d6 + "' data-d2='" + value[x].d2 + "' data-d7='" + value[x].d7 + "' data-d8='" + value[x].d8 + "' data-d9='" + value[x].d9 + "' class='btn btn-xs btn-primary' onclick='status(this)'> Status </a> </td>" 
            :
            (value[x].d0 == 'Credito enviado' ? "<td> <a href='#' data-d1='" + value[x].d1 + "' data-d6='" + value[x].d6 + "' data-d2='" + value[x].d2 + "' data-d7='" + value[x].d7 + "' data-d8='" + value[x].d8 + "' data-d9='" + value[x].d9 + "' class='btn btn-xs btn-danger' onclick='status(this)'> Status </a> </td>": "")
            ),
            ] ).draw().node();
          }
        });
      });
    }
    
    if($(x).data("id") == 2){
      console.log(2);
      $.get("{{ url('/11w5Cj9WDAjnzlg0/showwithsped')}}", function(data, status){
        var i = 0;
        $.each(data, function (key, value) {  
          var rows = table.rows().remove().draw();
          for (x in value) {
            var rowNode = table.row.add( [ 
            ( value[x].d9 == 'N' ? "<td> <span class='label label-warning'> " + value[x].d0 + "</span> </td>" : (value[x].d0 == 'Credito enviado' ? "<td> <span class='label label-success'> " + value[x].d0 + "</span> </td>" : "<td> <span class='label label-info'> " + value[x].d0 + "</span> </td>") ), 
            "<td>" + value[x].d4+ "</td>", 
            "<td>" + value[x].d5+ "</td>",
            "<td>" + value[x].d6+ "</td>", 
            "<td>" + value[x].d2+ "</td>", 
            "<td>" + value[x].d1 + "</td>",
            "<td>" + value[x].d3+ "</td>", 
            "<td>" + value[x].d31+ "</td>", 
            ( value[x].d9 == 'N' 
            ? 
            "<td> <a href='#' data-d1='" + value[x].d1 + "' data-d6='" + value[x].d6 + "' data-d2='" + value[x].d2 + "' data-d7='" + value[x].d7 + "' data-d8='" + value[x].d8 + "' data-d9='" + value[x].d9 + "' class='btn btn-xs btn-primary' onclick='status(this)'> Status </a> </td>" 
            :
            (value[x].d0 == 'Credito enviado' ? "<td> <a href='#' data-d1='" + value[x].d1 + "' data-d6='" + value[x].d6 + "' data-d2='" + value[x].d2 + "' data-d7='" + value[x].d7 + "' data-d8='" + value[x].d8 + "' data-d9='" + value[x].d9 + "' class='btn btn-xs btn-danger' onclick='status(this)'> Status </a> </td>": "")
            ),
            ] ).draw().node();
          }
        });
      });
    }
    if($(x).data("id") == 3){
      console.log(3);
      $.get("{{ url('/11w5Cj9WDAjnzlg0/showfees')}}", function(data, status){
        var i = 0;
        $.each(data, function (key, value) {  
          var rows = table.rows().remove().draw();
          for (x in value) {
            var rowNode = table.row.add( [ 
            ( value[x].d9 == 'N' ? "<td> <span class='label label-warning'> " + value[x].d0 + "</span> </td>" : "<td> <span class='label label-success'> " + value[x].d0 + "</span> </td>" ), 
            "<td>" + value[x].d4+ "</td>", 
            "<td>" + value[x].d5+ "</td>",
            "<td>" + value[x].d6+ "</td>", 
            "<td>" + value[x].d2+ "</td>", 
            "<td>" + value[x].d1 + "</td>",
            "<td>" + value[x].d3+ "</td>", 
            "<td>" + value[x].d31+ "</td>", 
            ( value[x].d9 == 'N' ? "<td> <a href='#' data-d1='" + value[x].d1 + "' data-d6='" + value[x].d6 + "' data-d2='" + value[x].d2 + "' data-d7='" + value[x].d7 + "' data-d8='" + value[x].d8 + "' data-d9='" + value[x].d9 + "' class='btn btn-xs btn-primary' onclick='status(this)'> Status </a> </td>" : " "), 
            ] ).draw().node();
          }
        });
      });
    }
    if($(x).data("id") == 4){
      console.log(4);
      $.get("{{ url('/11w5Cj9WDAjnzlg0/showpur')}}", function(data, status){
        var i = 0;
        $.each(data, function (key, value) {  
          var rows = table.rows().remove().draw();
          for (x in value) {
            var rowNode = table.row.add( [ 
            ( value[x].d9 == 'N' ? "<td> <span class='label label-warning'> " + value[x].d0 + "</span> </td>" : "<td> <span class='label label-success'> " + value[x].d0 + "</span> </td>" ), 
            "<td>" + value[x].d4+ "</td>", 
            "<td>" + value[x].d5+ "</td>",
            "<td>" + value[x].d6+ "</td>", 
            "<td>" + value[x].d2+ "</td>", 
            "<td>" + value[x].d1 + "</td>",
            "<td>" + value[x].d3+ "</td>", 
            "<td>" + value[x].d31+ "</td>", 
            ( value[x].d9 == 'N' ? "<td> <a href='#' data-d1='" + value[x].d1 + "' data-d6='" + value[x].d6 + "' data-d2='" + value[x].d2 + "' data-d7='" + value[x].d7 + "' data-d8='" + value[x].d8 + "' data-d9='" + value[x].d9 + "' class='btn btn-xs btn-primary' onclick='status(this)'> Status </a> </td>" : " "), 
            ] ).draw().node();
          }
        });
      });
    }
    
  }
  $(document).ready(function() {
    var table = $('.br-datatable-withdrawal').DataTable(); 
    $.get("{{ url('/11w5Cj9WDAjnzlg0/showwiths')}}", function(data, status){
      var i = 0;
      $.each(data, function (key, value) {  
        //$(".fillDataTable").empty();
        var rows = table.rows().remove().draw();
        for (x in value) {
          var rowNode = table.row.add( [ 
            ( value[x].d9 == 'N' ? "<td> <span class='label label-warning'> " + value[x].d0 + "</span> </td>" : (value[x].d0 == 'Credito enviado' ? "<td> <span class='label label-success'> " + value[x].d0 + "</span> </td>" : "<td> <span class='label label-info'> " + value[x].d0 + "</span> </td>") ), 
            "<td>" + value[x].d4+ "</td>", 
            "<td>" + value[x].d5+ "</td>",
            "<td>" + value[x].d6+ "</td>", 
            "<td>" + value[x].d2+ "</td>", 
            "<td>" + value[x].d1 + "</td>",
            "<td>" + value[x].d3+ "</td>", 
            "<td>" + value[x].d31+ "</td>", 
            ( value[x].d9 == 'N' 
            ? 
            "<td> <a href='#' data-d1='" + value[x].d1 + "' data-d6='" + value[x].d6 + "' data-d2='" + value[x].d2 + "' data-d7='" + value[x].d7 + "' data-d8='" + value[x].d8 + "' data-d9='" + value[x].d9 + "' class='btn btn-xs btn-primary' onclick='status(this)'> Status </a> </td>" 
            :
            (value[x].d0 == 'Credito enviado' ? "<td> <a href='#' data-d1='" + value[x].d1 + "' data-d6='" + value[x].d6 + "' data-d2='" + value[x].d2 + "' data-d7='" + value[x].d7 + "' data-d8='" + value[x].d8 + "' data-d9='" + value[x].d9 + "' class='btn btn-xs btn-danger' onclick='status(this)'> Status </a> </td>": "")
            )
            ] ).draw().node();
        }
      }); 
    });
    
	});
  
  function status(x){
    if($(x).data("d9") == 'N'){
      $('#orderDetails').val($(x).data("d1"));
      $('#foundRemove').html(
      "<b> Beneficiario: </b>" + $(x).data("d6") + "<br>" +
      "<b> Usuario Unico Bancario: </b>" + $(x).data("d2") + "<br>" +
      "<b> IBAN: </b>" + $(x).data("d7") + "<br>" +
      "<b> BIC: </b>" + $(x).data("d8") + "<br>" 
      );
      $('#mystatus').modal('show');   
    }
    if($(x).data("d9") == 'Y'){
      $('#orderDetails').val($(x).data("d1"));
      $('#foundRemove').html(
      "<b> Beneficiario: </b>" + $(x).data("d6") + "<br>" +
      "<b> Usuario Unico Bancario: </b>" + $(x).data("d2") + "<br>" +
      "<b> IBAN: </b>" + $(x).data("d7") + "<br>" +
      "<b> BIC: </b>" + $(x).data("d8") + "<br>" +
      "<b> Cuidado, esta apunto de cambiar el estado de la transacción </b>"
      );
      $('#mystatus').modal('show');   
    }
    
  }
</script>
function()
@endsection
