@extends('brcode.layout.app')
@section('content-css-include')
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="{{ asset('assets/timepicker/bootstrap-timepicker.min.css') }}">
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    {{ Lang::get('app.configuration') }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li class="active">{{ Lang::get('app.configuration') }}</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
    <form action="{{ url('/save_password') }}" method="post">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="row">
	<!-- left column -->
	<div class="col-md-8">
	  <!-- magaya connection -->
	  <div class="box box-primary">
		<div class="box-header with-border">
		  <h3 class="box-title">{{ Lang::get('app.change_password') }}</h3>
		</div><!-- /.box-header -->
		<!-- form start -->
		  <div class="box-body">
			<div class="form-group">
			  <label for="user_new_password">{{ Lang::get('app.user_new_password') }}</label>
			  <input type="password" class="form-control" id="user_new_password" name="user_new_password" >
			  <span class="help-block">{{ $errors->first('user_new_password') }}</span>
			</div>
			<div class="form-group">
			  <label for="user_new_password_confirmation">{{ Lang::get('app.user_new_password_confirmation') }}</label>
			  <input type="password" class="form-control" id="user_new_password_confirmation" name="user_new_password_confirmation" >
			  <span class="help-block">{{ $errors->first('user_new_password_confirmation') }}</span>
			</div>
		  </div><!-- /.box-body -->
	  </div><!-- /.box -->
    </div><!-- /.col-lg-6 -->
    </div><!-- /.row -->	
	
	<div class="row">
	<!-- left column -->
	<div class="col-md-8">
	  <div class="box-footer">
		<button type="submit" class="btn btn-primary">Submit</button>
		<input type="hidden" name="user_id" value="{{ Auth::id() }}" />
	  </div>
	</div><!-- /.col-lg-6 -->
    </div><!-- /.row -->
	</form>
</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="{{ asset('assets/timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('assets/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('assets/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('assets/input-mask/jquery.inputmask.numeric.extensions.js') }}"></script>
<script src="{{ asset('assets/input-mask/jquery.inputmask.extensions.js') }}"></script>
@endsection
@section('content-script')
<script type="text/javascript">
$(function() {
	
	$('span.help-block').each(function() {
		var s = $(this);
		
		if(s.text().trim().length > 0) {
			
			var c = s.closest('.bootstrap-timepicker');
			
			if(c.length > 0)
				c.addClass('has-feedback has-error');
			else
				s.closest('.form-group').addClass('has-feedback has-error');
			
		}
	});
	
	$("[data-mask]").inputmask();
	//Timepicker
	$(".timepicker").timepicker({showInputs: false, showMeridian: true});

	// Service recurrence
	$('#service_recurrence').change(function() {
		var v = $(this).val();
		var s = $('#service_hour');
		
		if(v == 'daily' || v == 'weekly' || v == 'monthly')
			s.closest('.bootstrap-timepicker').show();
		else
			s.val('').closest('.bootstrap-timepicker').hide();
	});
});
</script>
@endsection