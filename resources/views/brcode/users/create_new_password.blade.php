@extends('brcode.front.layout.app_home')
@section('content-css-include')
    <style>
        .btn-notification {
            display: none;
            /* oculta el botón por defecto */
        }
    </style>
    @livewireStyles
@endsection

@section('content')

    <div class="header-area" id="headerArea">
        <div class="container h-100 d-flex align-items-center justify-content-between">
            <!-- Back Button-->
            <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
            <!-- Page Title-->
            <div class="page-heading">
                <h6 class="mb-0 font-extrabold">Recuperación de la contraseña</h6>
            </div>
            <!-- Navbar Toggler-->

            <div class="normal">
                @if (Auth::user())
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @else
                    <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebarPanel">
                        <span></span><span></span><span></span>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div id="appCapsule" class="h-screen flex flex-col justify-start items-center bg-gray-100 mt-4">
        <div class="w-full max-w-sm p-6 mt-4 bg-white shadow-md rounded-lg">
            <div class="text-center mb-4">
                <a href="#" class="text-lg font-semibold">{!! config('brcode.app_name_html') !!}</a>
            </div>
            <div>
                @if ($viewData['token'])
                    <p class="text-center text-gray-700 mb-4">{{ Lang::get('login.create_new_password_message') }}</p>
                    <form action="{{ url('/11w5Cj9WDAjnzlg0/create_new_password') }}" method="post">
                        @csrf
                        <input type="hidden" name="_resetToken" value="{{ $viewData['token'] }}" />
                        <div class="mb-4">
                            <input type="hidden" name="user_email" class="w-full p-2 border rounded" placeholder="{{ Lang::get('login.user_email') }}" value="{{ $userEmail }}" readonly>
                        </div>
                        <div class="mb-4">
                            <input type="password" name="password" class="w-full p-2 border rounded" placeholder="{{ Lang::get('login.password') }}">
                            @error('password')
                                <span class="text-red-500">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-4">
                            <input type="password" name="password_confirmation" class="w-full p-2 border rounded" placeholder="{{ Lang::get('login.password_retype') }}">
                            @error('password_confirmation')
                                <span class="text-red-500">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-4">
                            @if (session('login_message') && is_array(session('login_message')))
                                @if (count(session('login_message')) > 0)
                                    <p class="text-center bg-{{ session('login_message')['type'] }} text-white p-2">
                                        {!! session('login_message')['message'] !!}
                                    </p>
                                @endif
                            @endif
                        </div>
                        <div class="mb-4">
                            <button type="submit" class="w-full p-2 bg-red-500 text-white rounded">{{ Lang::get('login.confirm_new_password') }}</button>
                        </div>
                    </form>                    
                @else
                    <p class="text-center bg-red-500 text-white p-2">{{ Lang::get('login.invalid_new_pwd_request') }}</p>
                    <a href="/" class="block text-center mt-2">{{ Lang::get('login.back_to_login_screen') }}</a>
                @endif
            </div>
        </div>
    </div>


    </body>
@endsection

@section('content-script-include')
    <script src="{{ asset('/assets/iCheck/icheck.min.js') }}"></script>

    <script>
        $(function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
@endsection
@section('content-script')
@endsection
