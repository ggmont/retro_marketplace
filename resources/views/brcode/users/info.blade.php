@extends('adminlte::page')

@section('title', 'Admin-Dashboard')
@section('plugins.JqueryDatatable', true)
@section('content_header')
    <h1 class="font-bold text-2xl my-8 text-center">- Información/Historial del Usuario -</h1>
@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/fontawesome.min.css"
        integrity="sha512-cHxvm20nkjOUySu7jdwiUxgGy11vuVPE9YeK89geLMLMMEOcKFyS2i+8wo0FOwyQO/bL8Bvq1KMsqK4bbOsPnA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tw-elements/dist/css/index.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
    <script src="https://cdn.tailwindcss.com/3.2.4"></script>
    <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/style.css?v=2') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/basictable.min.css') }}" />
    <style>
        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .prueba {
            background: radial-gradient(ellipse farthest-corner at right bottom, #FEDB37 0%, #FDB931 8%, #9f7928 30%, #8A6E2F 40%),
                radial-gradient(ellipse farthest-corner at left top, #FFFFFF 0%, #FFFFAC 8%, #D1B464 25%, #5d4a1f 62.5%, #5d4a1f 100%);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }

        .tabs {
            display: flex;
            justify-content: space-around;
            margin: 3px;
            height: 40px;
            box-shadow: 0 0 1px 1px rgba(0, 0, 0, 0.2);
        }

        .tabs>* {
            width: 100%;
            color: dimgray;
            height: 100%;
            cursor: pointer;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .tabs>*:hover:not(.active) {
            background-color: rgb(220, 220, 220);
        }

        .tabs>.active {
            color: white;
            background-color: #c53030;

        }

        .panel {
            display: none;
            opacity: 0;
            transition: opacity 0.2s ease-in-out;
        }

        .panel.active {
            display: block;
            opacity: 1;
            transition: opacity 0.2s ease-in-out;
        }
    </style>


    @livewireStyles
@stop

@section('content')

    <head>
        <script type="text/javascript" src="{{ asset('js/datatable/basictable.min.js') }}"></script>
    </head>

    <section class="content">
        <div class="flex flex-col items-center">
            <div class="opacity-80 inset-0 z-0"></div>
            <!-- dark theme -->
            <div class="w-full mx-auto z-10">
                <!-- default theme -->
                <div class="w-full mx-auto z-10">
                    <div class="flex flex-col">
                        <div class="bg-white border border-white shadow-lg  rounded-3xl p-4 m-4">
                            <div class="flex-none sm:flex">
                                <div class="relative h-32 w-32   sm:mb-0 mb-3">
                                    @foreach ($profile as $p)
                                        @if ($p->profile_picture)
                                            <a href="{{ url($p->profile_picture) }}" data-fancybox="{{ $p->id }}">
                                                <img src="{{ url($p->profile_picture) }}"
                                                    class="w-32 h-32 object-cover rounded-2xl">
                                            </a>
                                        @else
                                            <a href="{{ asset('img/profile-picture-not-found.png') }}"
                                                data-fancybox="{{ $p->id }}">
                                                <img src="{{ asset('img/profile-picture-not-found.png') }}"
                                                    class="w-32 h-32 object-cover rounded-2xl">
                                            </a>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="flex-auto sm:ml-5 justify-evenly">
                                    <div class="flex items-center justify-between sm:mt-2">
                                        <div class="flex items-center">
                                            <div class="flex flex-col">
                                                <div
                                                    class="pl-7 w-full flex-none retro text-xl text-gray-800 font-bold leading-none">
                                                    @foreach ($profile as $p)
                                                        @if (App\SysUserRoles::where('user_id', $p->id)->where('role_id', 2)->count() > 0)
                                                            <span
                                                                class="prueba tect-lg font-extrabold text-center">{{ $p->user_name }}</span>
                                                        @elseif(App\SysUserRoles::where('user_id', $p->id)->where('role_id', 6)->count() > 0)
                                                            <span
                                                                class="prueba tect-lg font-extrabold text-center">{{ $p->user_name }}</span>
                                                        @else
                                                            {{ $p->user_name }}
                                                        @endif
                                                    @endforeach
                                                </div>
                                                <div class="flex-auto retro text-lg my-1">
                                                    <span class="mr-3  pl-7 text-center">
                                                        @foreach ($profile as $p)
                                                            @if (App\SysUserRoles::where('user_id', $p->id)->where('role_id', 6)->count() > 0)
                                                                <span class="text-red-700">(COLABORADOR)</span>
                                                            @else
                                                                ({{ $p->roles->first()->rol->description }})
                                                            @endif
                                                        @endforeach
                                                    </span>
                                                    <span class="mr-3 pl-1  border-r border-gray-200  max-h-0"></span>
                                                    <span class="text-center">
                                                        @foreach ($profile as $p)
                                                            Miembro desde {{ date_format($p->created_at, 'Y-m-d') }}
                                                        @endforeach
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-3 bg-white w-full">
                                        <div class="mt-2 text-base retro">
                                            <div class="grid gap-6 grid-cols-1 md:grid-cols-2 lg:grid-cols-3">
                                                <!-- Card 1 -->
                                                <div
                                                    class="flex items-center justify-center bg-white border-2 border-gray-200 rounded-lg shadow-sm dark:bg-gray-800">
                                                    <div class="p-2 mr-2 text-white rounded-full">
                                                        <img src="{{ asset('img/new.png') }}" width="20px"
                                                            data-toggle="popover" data-content="Bueno" data-placement="top"
                                                            data-trigger="hover">
                                                    </div>
                                                    <div>
                                                        <p class="mb-1 text-sm font-medium text-gray-900"></p>
                                                        <p class="text-lg font-bold text-gray-800">
                                                            {{ number_format($viewData['sc1'] * 100, 2) }} %
                                                        </p>
                                                    </div>
                                                </div>
                                                <!-- Card 2 -->
                                                <div
                                                    class="flex items-center justify-center bg-white border-2 border-gray-200 rounded-lg shadow-sm dark:bg-gray-800">
                                                    <div class="p-2 mr-2 text-white rounded-full">
                                                        <img src="{{ asset('img/used.png') }}" width="20px"
                                                            data-toggle="popover" data-content="Bueno" data-placement="top"
                                                            data-trigger="hover">
                                                    </div>
                                                    <div>
                                                        <p class="mb-1 text-sm font-medium text-gray-900"></p>
                                                        <p class="text-lg font-bold text-gray-800">
                                                            {{ number_format($viewData['sc2'] * 100, 2) }} %
                                                        </p>
                                                    </div>
                                                </div>
                                                <!-- Card 3 -->
                                                <div
                                                    class="flex items-center justify-center bg-white border-2 border-gray-200 rounded-lg shadow-sm dark:bg-gray-800">
                                                    <div class="p-2 mr-2 text-white rounded-full">
                                                        <img src="{{ asset('img/no-funciona.png') }}" width="20px"
                                                            data-toggle="popover" data-content="Bueno" data-placement="top"
                                                            data-trigger="hover">
                                                    </div>
                                                    <div>
                                                        <p class="mb-1 text-sm font-medium text-gray-900"></p>
                                                        <p class="text-lg font-bold text-gray-800">
                                                            {{ number_format($viewData['sc3'] * 100, 2) }} %
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <center><span class="retro text-lg font-bold"> <i class="fas fa-fw fa-arrow-down"></i>
                                            HISTORIAL <i class="fas fa-fw fa-arrow-down"></i></span></center>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">



            @include('partials.flash')


            <div class="tabs">
                <div class="tab active" data-target="historial_user">Usuario</div>
                <div class="tab" data-target="historial_ref">Referidos</div>
                <div class="tab" data-target="historial_promo">Códigos Promocionales</div>
                <div class="tab" data-target="historial_product">Productos</div>
                <div class="tab" data-target="historial_message">Mensajes</div>
            </div>
            <div id="panels" class="container">
                <div class="historial_user panel active">
                    @livewire('admin.user-event-table', ['ultra' => $ultra])
                </div>
                <div class="historial_ref panel">
                    @livewire('admin.user-refered-table', ['ultra' => $ultra])
                </div>
                <div class="historial_promo panel">
                    @livewire('admin.user-promotional-table', ['ultra' => $ultra])
                </div>
                <div class="historial_product panel">
                    @livewire('admin.historial-product', ['ultra' => $ultra])
                </div>
                <div class="historial_message panel">
                    @livewire('admin.historial-message', ['ultra' => $ultra])
                </div>
            </div>

        </div>

    </section>
@stop


@section('js')
    <script>
        const tabs = document.querySelectorAll(".tabs");
        const tab = document.querySelectorAll(".tab");
        const panel = document.querySelectorAll(".panel");

        function onTabClick(event) {
            // deactivate existing active tabs and panel
            for (let i = 0; i < tab.length; i++) {
                tab[i].classList.remove("active");
            }
            for (let i = 0; i < panel.length; i++) {
                panel[i].classList.remove("active");
                panel[i].style.opacity = "0";
            }

            // activate new tabs and panel
            event.target.classList.add("active");
            let classString = event.target.getAttribute("data-target");
            let panelToShow = document
                .getElementById("panels")
                .getElementsByClassName(classString)[0];
            panelToShow.classList.add("active");
            setTimeout(() => {
                panelToShow.style.opacity = "1";
            }, 50);
        }

        for (let i = 0; i < tab.length; i++) {
            tab[i].addEventListener("click", onTabClick, false);
        }
    </script>
    <script>
        $(function() {
            $('[data-toggle="popover"]').popover()
        })
    </script>
    <script>
        Livewire.on('omega', function() {
            console.log('holaaa');
            var $inventoryAdd = $('.br-btn-product-add');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>

    @livewireScripts
@stop
