@extends('brcode.layout.app')
@section('content-css-include')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
<link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
<link rel="stylesheet" href="{{asset('plugins/datepicker/jquery-ui.css')}}">
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
<div class="row">
  <h1>
  Users 
  </h1>

</div>

    @if(isset($total))
    <div class="col-sm-12">
      <div class="alert alert-success" style="padding: 10px;">
          <button type="button" class="close" data-dismiss="alerta">&times;</button>
          <span>Se encontraron <span class="">[{{ $total }}]</span> coincidencias</span>
      </div>
    </div> 
    @endif

  <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li class="active"> Users <li>
  </ol>
</section>
<div class="col-lg-12">
        <div class="label alert-success">
			Mes actual
			<strong class="text-center">
			(@php $meses = array("January","February","March","April","May","June","July","August","September","October","November","December");
			echo $meses[date('n')-1]; @endphp)
			</strong>
		</div>
	 <div class="box box-solid box-default">
	 	<div class="box-header with-border">
	 	<div class="row">
	 	<form action="{{ route('fil') }}" method="POST" class="form-inline well" style="background-color:rgb(0, 0, 0, 0.4);">
			{{ csrf_field() }}
			<div class="form-group">
				<label for="" style="color: #FFFFFF">Filter by Month Or..</label>
				<select onchange="habilitar(this)" name="mes" class="form-control">
					<option value="">Select...</option>
					<option value="1">January</option>
					<option value="2">February</option>
					<option value="3">March</option>
					<option value="4">April</option>
					<option value="5">May</option>
					<option value="6">June</option>
					<option value="7">July</option>
					<option value="8">August</option>
					<option value="9">September</option>
					<option value="10">October</option>
					<option value="11">November</option>
					<option value="12">December</option>
				</select>
			</div>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<div class="form-group">
				<label for="" style="color: #FFFFFF">From </label>
                <input class="form-control fe" id="from" name="desde"  type="text" autocomplete="off">
			</div>
			&nbsp;&nbsp;&nbsp;
			<div class="form-group">
				<label for="" style="color: #FFFFFF">To </label>
                <input class="form-control fe" id="to" name="hasta" type="text" autocomplete="off">
			</div>
			&nbsp;&nbsp;&nbsp;
			<button type="submit" class="btn btn-primary">
			<i class="fa fa-search"></i> Buscar
			</button>
		</form>
	    </div>
	 	</div>
	 </div>
</div>
<!-- Main content -->
<section class="content">
	<div class="row">	 
	<!-- column -->
		<div class="col-md-12">
		<!-- magaya connection -->
			<div class="box">
				<div class="box-body">
					<div class="row br-dataTable-Options">
						<div class="col-md-2">
							<a href="{{ url('/11w5Cj9WDAjnzlg0/users/add')}}" class="btn btn-primary">{{ Lang::get('app.add') }}</a>
						</div>
						<div class="col-md-8"></div>
						@include('partials.flash')
					</div>
					<table class="table data-table table-bordered table-hover table-condensed">
					<thead>
						<tr>
                            <th>id</th>
                            <th>FirstName</th>
                            <th>LastName</th>
                            <th>Email</th>
                            <th>Username</th>
                            <th>Enabled</th>
							<th>{{ Lang::get('app.Concept') }}</th>
							<th>{{ Lang::get('app.Saldo') }} €</th>
							<th>{{ Lang::get('app.action') }} </th>
						</tr>
					</thead>
					<tbody>
						@foreach ($viewData['usuario'] as $user)
						<tr>
						<td>{{ $user->id }}</td>
						<td>{{ucfirst($user->first_name)}}</td>
						<td>{{ucfirst($user->last_name)}}</td>
						<td>{{ucfirst($user->email)}}</td>
						<td>{{ucfirst($user->user_name)}}</td>
						<td>
							@if($user->is_enabled == "Y")
                              Yes
                            @else
                              No
                            @endif
						</td>
						<td>{{ $user->concept }}</td>
						<td>{{$user->getCashAttribute()}}</td>
						<td>
						<a href="{{ url('/11w5Cj9WDAjnzlg0/users/modify',[$user->id]) }}"  class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a>
						<a href="#"  data-toggle="modal" data-id="{{ $user->id }}" data-cre="{{ $user->cash }}" data-option="add" class="btn btn-xs btn-success" onclick="openAdd(this);"><i class="fa fa-plus"></i> Credit </a>
						<a href="#" class="btn btn-xs btn-warning" data-option="remove" data-toggle="modal" data-id="{{ $user->id }}" data-cre="{{ $user->cash }}" onclick="openRemove(this);"><i class="fa fa-minus"></i> Credit </a>
						<a href="{{ url('/11w5Cj9WDAjnzlg0/users/info',[$user->id]) }}" class="btn btn-xs btn-info"><i class="fa fa-info"></i> Info </a>
					    </td>
						@endforeach
					    </tr>
					</tbody>
					</table>
				</div> <!-- /.box-body -->
			</div> <!-- /.box -->
		</div> <!-- /.col-md-12 -->
	</div> <!-- /.row -->

</section><!-- /.content -->


<div  class="modal fade" tabindex="-1" role="dialog" id="myAdd">
  <div class="modal-dialog">
    <div class="modal-content">
		<form method="post" action="{{ url('/11w5Cj9WDAjnzlg0/users/AddCredit') }}">
			{{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ __('Agregar Credito') }}</h4>
	  	</div>
	  
      <div class="modal-body">
		  	<br>
				<div class="form-group" style="padding: 10px;">
					<label for="credit">{{ __('Ingrese la cantidad de credito a agregar') }}</label>
					<input type="number" name="credit" min="0" required step="0.01"  class="form-control">
				</div>
				<div class="form-group" style="padding: 10px;">
					<label for="pass">{{ __('Cotraseña') }}</label>
					<input type="password" name="pass" class="form-control">
				</div>
				<input id="orderDetailsAdd" name="orderDetails" type="hidden" >
				<input name="transaction" type="hidden" value="1">
	  	</div>
	  
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Cancelar') }}</button>
        <button type="submit" class="btn btn-success">{{ __('Guardar') }}</button>
			</div>
		</form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<form action="{{ url('/11w5Cj9WDAjnzlg0/users/AddCredit')}}" method="post">
{{ csrf_field() }}
<div  class="modal fade" tabindex="-1" role="dialog" id="myRemove">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ __('Retirar Credito') }}</h4>
	  </div>
	  
      <div class="modal-body">
		  	<br>
				<div class="form-group" style="padding: 10px;">
					<label for="credit">{{ __('Ingrese la cantidad de credito a retirar') }}</label>
					<input type="number" name="credit" min="0" required step="0.01"  class="form-control">
					<div id="foundRemove"></div>
				</div>
				<div class="form-group" style="padding: 10px;">
					<label for="pass">{{ __('Contraseña') }}</label>
					<input type="password" name="pass" class="form-control">
				</div>
				<input  id="orderDetailsRemove" name="orderDetails" type="hidden" >
				<input name="transaction" type="hidden" value="0">
	  </div>
	  
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Cancelar') }}</button>
        <button type="submit" class="btn btn-warning">{{ __('Guardar') }}</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>


@endsection

@section('content-script-include')
<script src="{{ asset('assets/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/datatables/Responsive-2.2.0/js/dataTables.responsive.min.js') }}"></script>
<script src="{{asset('plugins/datepicker/jquery-ui.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/datatables/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/datatables/extensions/Responsive/js/dataTables.responsive.js')}}"></script>

@endsection
@section('content-script')
<script>

$(function(){
    $("select").change(function(){
        if ( $(this).val() > 0 ) {
            $('#from').prop('disabled', true);
            $('#to').prop('disabled', true);
        }else {
            $("#from").off();
            $('#from, #to').prop('disabled', false);
        }
    });
});

      // -------- datapicker from, to
    $(function() {
      var dateFormat = "dd-mm-yy",

        from = $( "#from" )
          .datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
          })
          .on( "change", function() {
            to.datepicker( "option", "minDate", getDate( this ) );
          }),

        to = $( "#to" ).datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          changeYear: true,
          numberOfMonths: 1
        })
        .on( "change", function() {
          from.datepicker( "option", "maxDate", getDate( this ) );
        });

      function getDate( element ) {
        var date;
        try {
          date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
          date = null;
        }

        return date;
      }
    } );

    $(".fe").datepicker();

</script>
    <script type="text/javascript">
      $(document).ready(function(){
        //Eliminar alertas que no contengan la clase alert-important luego de 7seg
        $('div.span').not('.alert-important').delay(7000).slideUp(300);

        //activar Datatable
        $('.data-table').DataTable({
          responsive: true,
        });
      })
    </script>
<script type="text/javascript">
function openAdd(x){      
	$('#myAdd').modal('show');   
	$('#orderDetailsAdd').val($(x).data('id'));
}
function openRemove(x){      
	$('#myRemove').modal('show');   
	$('#orderDetailsRemove').val($(x).data('id'));
	$('#foundRemove').text("{{ __('Cantidad maxima') }} "+ $(x).data('cre') + " €");
}

$(function() {

	$(document).ready(function() {

		$('.br-datatable').each(function() {

			var url = $(this).attr('br-datatable-url');

			$(this).DataTable({
				processing: true,
				serverSide: true,
				ajax: url,
				bAutoWidth: false,
			});
		});

	});
});
</script>
@endsection
