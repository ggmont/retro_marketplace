<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('brcode.app_name') }} - {{ Lang::get('login.title_register') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="{{ asset('/assets/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/assets/admin-lte/css/AdminLTE.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('/assets/iCheck/square/blue.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style>
    .icheck {
    	margin-top: 5px;
    	margin-bottom: 5px;
    }
  </style>
</head>

<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="#">{!! config('brcode.app_name_html') !!}</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">{{ Lang::get('login.register_new_acc') }}</p>
      <form action="{{ url('/11w5Cj9WDAjnzlg0/register') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
          <div class="col-xs-6">
            <div class="form-group has-feedback">
              <input type="text" name="user_first_name" class="form-control" placeholder="{{ Lang::get('login.user_first_name') }}" value="{{ old('user_first_name') }}">
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
              <span class="help-block">{{ $errors->first('user_first_name') }}</span>
            </div>
          </div>
          <div class="col-xs-6">
            <div class="form-group has-feedback">
              <input type="text" name="user_last_name" class="form-control" placeholder="{{ Lang::get('login.user_last_name') }}" value="{{ old('user_last_name') }}">
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
              <span class="help-block">{{ $errors->first('user_last_name') }}</span>
            </div>
          </div>
        </div>
        <div class="form-group has-feedback">
          <input type="text" name="user_name" class="form-control" placeholder="{{ Lang::get('login.user_name') }}" value="{{ old('user_name') }}">
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
          <span class="help-block">{{ $errors->first('user_name') }}</span>
        </div>
        <div class="form-group has-feedback  ">
          <input type="user_name" name="user_email" class="form-control" placeholder="{{ Lang::get('login.user_email') }}" value="{{ old('user_email') }}">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          <span class="help-block">{{ $errors->first('user_email') }}</span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" name="password" class="form-control" placeholder="{{ Lang::get('login.password') }}">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          <span class="help-block">{{ $errors->first('password') }}</span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" name="password_confirmation" class="form-control" placeholder="{{ Lang::get('login.password_retype') }}">
          <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
          <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
        </div>
        <div class="form-group has-feedback">
          <p class="bg-warning">{{ strlen($errors->first('invalid_new_account')) > 0 ? Lang::get($errors->first('invalid_new_account')) : '' }}</p>
        </div>
        <div class="row">
          <div class="col-xs-8">
            <div class="checkbox icheck">
              <label><input type="checkbox" name="terms" > {!! Lang::get('login.agree_to_terms') !!}</label>
            </div>
            <span class="help-block">{{ $errors->first('terms') }}</span>
          </div>
          <!-- /.col -->
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">{{ Lang::get('login.register') }}</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <a href="/11w5Cj9WDAjnzlg0/" class="text-center">{{ Lang::get('login.already_have_membership') }}</a>

    </div>
    <!-- /.login-box-body -->
  </div>
  <!-- /.login-box -->

  <!-- jQuery 2.1.4 -->
  <script src="{{ asset('/assets/jQuery/jQuery-2.1.4.min.js') }}"></script>
  <!-- Bootstrap 3.3.5 -->
  <script src="{{ asset('/assets/bootstrap/js/bootstrap.min.js') }}"></script>
  <!-- iCheck -->
  <script src="{{ asset('/assets/iCheck/icheck.min.js') }}"></script>
  <script>
    $(function() {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      });
    });
  </script>
</body>

</html>
