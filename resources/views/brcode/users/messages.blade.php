@extends('brcode.layout.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  	Messages
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li class="active">Messages</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

<div class="messaging">
  <div class="inbox_msg">
	<div class="inbox_people">
	  <div class="headind_srch">
			<div class="recent_heading">
				<h4>Recent</h4>
			</div>
			<!--
			<div class="srch_bar">
				<div class="stylish-input-group">
				<input type="text" class="search-bar"  placeholder="Search" >
				</div>
			</div>
			--->
		</div>
		<div class="list-group" style="width:100%; height: 550px; max-height: 550px;">
			@foreach($conversations as $key)
				<a class="list-group-item" data-u="{{ $key->userName ? $key->userName->user_name : '' }}" data-c="{{ $key->conName ? $key->conName->user_name : '' }}" onclick="changeConversation(this)">
					<div class="row">
							<div class="col-lg-2">
									<img src="https://www.cobdoglaps.sa.edu.au/wp-content/uploads/2017/11/placeholder-profile-sq.jpg" height="48" width="48" class="img-circle" />
							</div>
							<div class="col-lg-10">
									{{ $key->userName ? $key->userName->user_name : '' }} | {{ $key->conName ? $key->conName->user_name : '' }} <br>
									<small>
											Last message at: {{date('Y/m/d H:i', strtotime($key->last_time))}}
									</small>
							</div>
					</div>
				</a>
			@endforeach	
		</div>
	</div>

	<div class="mesgs">
	  <div class="msg_history">
			
	  </div>
	</div>
  </div>
</div>

</section><!-- /.content -->

@endsection

@section('content-script-include')
	<script src="{{ asset('/assets2019/js/moment.js') }}"></script>
@endsection
@section('content-script')
<script>
	function changeConversation(x){
		$.post("{{ url('/11w5Cj9WDAjnzlg0/messagesUs') }}" ,
		{
			u: $(x).data('u'),
			c: $(x).data('c'),
			_token: '{{csrf_token()}}'
		},
		function(data, status){
			if(data.success){
				$('.msg_history').html('');
				$.each(data.messages , function(index, val) { 
					if(val['written_by_me']){
						$('.msg_history').append(
							"<div class='media incoming_msg received_withd_msg sent_msg'>" + 
							
								"<div class='media-left'>" +
									"<img style='margin-right: 5px;' src='https://www.cobdoglaps.sa.edu.au/wp-content/uploads/2017/11/placeholder-profile-sq.jpg' class='img-circle'  height='48' width='48'>" +			
								"</div>" +
								"<div class='media-body'><p>" +	val['content'] + "<br> <span class='time_date'> <b>" + data.us['user_name'] + "</b> " + moment(val['created_at'], "YYYY/MM/DD hh:mm").locale('es').fromNow() + " " +  (val['read'] > 0 ? "<i class='fa fa-check-circle-o text-success' aria-hidden='true'></i>": "<i class='fa fa-circle-o text-success' aria-hidden='true'></i>") +" "+
											
										"</span> "+
									"</p>"+
								"</div>"+

							"</div>"
						);
					}else{
						$('.msg_history').append(
							"<div class='media incoming_msg received_withd_msg'>" + 
							
								"<div class='media-left'>" +
									"<img style='margin-right: 5px;' src='https://www.cobdoglaps.sa.edu.au/wp-content/uploads/2017/11/placeholder-profile-sq.jpg' class='img-circle'  height='48' width='48'>" +			
								"</div>" +
								"<div class='media-body'><p>" +	val['content'] + "<br> <span class='time_date'> <b>" + data.con['user_name'] + "</b> " + moment(val['created_at'], "YYYY/MM/DD hh:mm").locale('es').fromNow() + " " +  (val['read'] > 0 ? "<i class='fa fa-check-circle-o text-success' aria-hidden='true'></i>": "<i class='fa fa-circle-o text-success' aria-hidden='true'></i>") +" "+
											
										"</span> "+
									"</p>"+
								"</div>"+

							"</div>"
						);
					}
				});
			}
			
		});
	}

</script>
@endsection

@section('content-css-include')
<style>
	/*---------chat window---------------*/
	.container{
			max-width:900px;
	}
	.inbox_people {
		background: #fff;
		float: left;
		overflow: hidden;
		width: 30%;
		border-right: 1px solid #ddd;
	}

	.inbox_msg {
		border: 1px solid #ddd;
		clear: both;
		overflow: hidden;
	}

	.top_spac {
		margin: 20px 0 0;
	}

	.recent_heading {
		float: left;
		width: 40%;
	}

	.srch_bar {
		display: inline-block;
		text-align: right;
		width: 60%;
		padding:
	}

	.headind_srch {
		padding: 10px 29px 10px 20px;
		overflow: hidden;
		border-bottom: 1px solid #c4c4c4;
	}

	.recent_heading h4 {
		color: #0465ac;
			font-size: 16px;
			margin: auto;
			line-height: 29px;
	}

	.srch_bar input {
		outline: none;
		border: 1px solid #cdcdcd;
		border-width: 0 0 1px 0;
		width: 80%;
		padding: 2px 0 4px 6px;
		background: none;
	}

	.srch_bar .input-group-addon button {
		background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
		border: medium none;
		padding: 0;
		color: #707070;
		font-size: 18px;
	}

	.srch_bar .input-group-addon {
		margin: 0 0 0 -27px;
	}

	.chat_ib h5 {
		font-size: 15px;
		color: #464646;
		margin: 0 0 8px 0;
	}

	.chat_ib h5 span {
		font-size: 13px;
		float: right;
	}

	.chat_ib p {
			font-size: 12px;
			color: #989898;
			margin: auto;
			display: inline-block;
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
	}

	.chat_img {
		float: left;
		width: 11%;
	}

	.chat_img img {
		width: 100%
	}

	.chat_ib {
		float: left;
		padding: 0 0 0 15px;
		width: 88%;
	}

	.chat_people {
		overflow: hidden;
		clear: both;
	}

	.chat_list {
		border-bottom: 1px solid #ddd;
		margin: 0;
		padding: 18px 16px 10px;
	}

	.inbox_chat {
		height: 550px;
		overflow-y: scroll;
	}

	.active_chat {
		background: #e8f6ff;
	}

	.incoming_msg_img {
		display: inline-block;
		width: 6%;
	}

	.incoming_msg_img img {
		width: 100%;
	}

	.received_msg {
		display: inline-block;
		padding: 0 0 0 10px;
		vertical-align: top;
		width: 92%;
	}

	.received_withd_msg p {
		background: #ebebeb none repeat scroll 0 0;
		border-radius: 0 15px 15px 15px;
		color: #646464;
		font-size: 14px;
		margin: 0;
		padding: 5px 10px 5px 12px;
		width: 100%;
	}

	.time_date {
		color: #747474;
		display: block;
		font-size: 12px;
		margin: 8px 0 0;
	}

	.received_withd_msg {
		width: 57%;
	}

	.mesgs{
		float: left;
		width:70%;
		height: 620px;
	}

	.sent_msg p {
		background:#0465ac;
		border-radius: 12px 15px 15px 0;
		font-size: 14px;
		margin: 0;
		color: #fff;
		padding: 5px 10px 5px 12px;
		width: 100%;
	}

	.outgoing_msg {
		overflow: hidden;
		margin: 26px 0 26px;
	}

	.sent_msg {
		float: right;
		width: 46%;
	}

	.input_msg_write input {
		background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
		border: medium none;
		color: #4c4c4c;
		font-size: 15px;
		min-height: 48px;
		width: 100%;
		outline:none;
	}

	.type_msg {
		border-top: 1px solid #c4c4c4;
		position: relative;
	}

	.msg_send_btn {
		background: #05728f none repeat scroll 0 0;
		border:none;
		border-radius: 50%;
		color: #fff;
		cursor: pointer;
		font-size: 15px;
		height: 33px;
		position: absolute;
		right: 0;
		top: 11px;
		width: 33px;
	}

	.messaging {
		padding: 0 0 50px 0;
	}

	.msg_history {
		height: 600px;
		overflow-y: auto;
	}

	.write_msg{
        background-color : #d1d1d1; 
    }

    .msg_send_btn {
        background: #bd1919 none repeat scroll 0 0;
        border: medium none;
        border-radius: 50%;
        color: #fff;
        cursor: pointer;
        font-size: 17px;
        height: 33px;
        position: absolute;
        right: 3px;
        top: 0px;
        width: 33px;
    }

    .input_msg_write input {
    background: #fff none repeat scroll 0 0;
    padding-left: 10px;
    border: medium none;
    color: #4c4c4c;
    font-size: 15px;
    min-height: 33px;
    width: 100%;
    }

    .img_none{
        display: none;
        
    }
    .received_withd_msg { 
        width: 57%;
    }


    .messaging { padding: 0 0 50px 0;}

    .msg_history {
        height: 100%;
        width:100%;
        overflow-y: auto;
        padding: 0px 10px 0 10px;
    }
    .outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
    .sent_msg {
        float: right;
        width: 60%;
    }
    .received_msg {
    float: left;
    width: 60%;
    }

    .incoming_msg_img {
        display: inline-block;
        width: 6%;
    }

    .received_withd_msg p {
        background: #ffffff none repeat scroll 0 0;
        border-radius: 3px;
        color: #646464;
        font-size: 14px;
        margin: 0;
        padding: 5px 10px 5px 12px;
        width: 100%;
        margin-top: 3px !important;
        margin-bottom: 3px !important;
    }

		.media {
			margin-top: 0px !important;
			margin-bottom: 0px !important;
		}

    .active{
        background: rgb(190, 190, 190) !important;
    }

    .sent_msg p {
        background: #fff none repeat scroll 0 0;
        border-radius: 3px;
        font-size: 14px;
        color:#646464;
        padding: 8px 10px 8px 12px;
        width:100%;
    }

    .time_date {
    color: #747474;
    display: block;
    font-size: 12px;
    margin: 8px 0 0;
    }
    .recent_heading {float: left; width:40%;}
    .recent_heading h4 {
        color: #dd163b;
        font-size: 18px;
        margin: auto;
    }
</style>
@endsection