@extends('brcode.layout.app')
@section('content-css-include')
<link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/chartjs/Chart.min.css') }}">

@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    {{ Lang::get('app.dashboard') }}
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li class="active">{{ Lang::get('app.dashboard') }}</li>
  </ol>
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{ $viewData['user_register']}}</h3>
          <p>User Registrations</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        
      </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{ $viewData['order_user']}}</h3>
          <p>Orders</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        
      </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{ $viewData['order_user_complete']}}</h3>
          <p>Orders Complete</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
        
      </div>
    </div><!-- ./col -->
    
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{ $viewData['product_user']}} / {{ $viewData['product_user_qty']}} </h3>
          <p>Productos por usuarios / cantidad</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
        
      </div>
    </div><!-- ./col -->
  </div><!-- /.row -->
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Beneficios por mes</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <canvas id="myChart" width="600px"></canvas>
        </div><!-- /.box-body -->
      </div>

    </section><!-- /.Left col -->
    
  </div><!-- /.row (main row) -->

</section><!-- /.content -->
@endsection
@section('content-script-include')

  <script src="{{ asset('assets/chartjs/Chart.min.js') }}"></script>
  <script src="{{ asset('assets/chartjs/Chart.bundle.min.js') }}"></script>
  <script src="{{ asset('assets/moment/moment.local.min.js') }}"></script>
  <script>
    var ctx = document.getElementById('myChart');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {

            labels: [
              @foreach($viewData['resumen'] as $key)
                moment().locale('{{App::getLocale()}}').month({{$key->month - 1}}).format("MMMM").toUpperCase() + " {{$key->years}}",
              @endforeach
            ],
            
            datasets: [{
                label: 'Beneficios por mes',

                data: [
                  @foreach($viewData['resumen'] as $key)
                   {{$key->sum}},
                  @endforeach
                ],

                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
          responsive:true,
          maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
    </script>
@endsection