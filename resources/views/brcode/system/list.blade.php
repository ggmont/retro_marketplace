@extends('brcode.layout.app')
@section('content-css-include')
<link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    {{ $viewData['form_title'] }}
  </h1>
  @include('brcode.layout.app_breadcrumb')
</section>

<!-- Main content -->
<section class="content">
    <form action="{{ $viewData['form_url_post'] }}" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			@include('brcode.layout.app_form')

			@include('brcode.layout.app_form_sub')

			@include('brcode.layout.app_form_buttons')

		</form>
</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="{{ asset('assets/typeahead/typeahead.js') }}"></script>
@endsection
@section('content-script')
<script type="text/javascript">
$(function() {
		$('#br-btn-delete').click(function() {
		$(this).button('loading');

    var buttons = [
			{addClass: 'btn btn-primary btn-sm pull-left', text: '{{ Lang::get('app.delete') }}', onClick: function($noty) {$noty.close();deleteRecord()} },
			{addClass: 'btn btn-danger btn-sm pull-right', text: '{{ Lang::get('app.cancel') }}', onClick: function($noty) {$noty.close();$('#br-btn-delete').button('reset');} },
		];

		presentNotyMessage('{{Lang::get('app.delete_are_you_sure') }}','warning',buttons,1);
	});

  $('select.br-select2').select2();

  function substringMatcher(strs) {
    return function findMatches(q, cb) {
      var matches, substringRegex;

      // an array that will be populated with substring matches
      matches = [];

      // regex used to determine if a string contains the substring `q`
      substrRegex = new RegExp(q, 'i');

      // iterate through the pool of strings and for any string that
      // contains the substring `q`, add it to the `matches` array
      $.each(strs, function(i, str) {
        if (substrRegex.test(str)) {
          matches.push(str);
        }
      });

      cb(matches);
    };
  }

  $('[data-typeahead]').each(function() {
    var source = $(this).data('typeahead');
    $(this).typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: 'source',
      source: substringMatcher(source)
    });
  });

	function deleteRecord() {
		var url = '{{ isset($viewData['form_url_post_delete'])?$viewData['form_url_post_delete']:'' }}';
		var form = $('<form action="' + url + '" method="post">' +
			'<input type="hidden" name="_token" value="{{ csrf_token() }}">' +
			'<input type="hidden" name="model_id" value="{{ $viewData['model_id'] > 0 ? $viewData['model_id'] : '' }}" />' +
			'</form>');
		$('body').append(form);
		form.submit();
	}
});
</script>
@endsection
