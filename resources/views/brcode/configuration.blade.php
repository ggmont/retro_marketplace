@extends('brcode.layout.app')
@section('content-css-include')
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="{{ asset('assets/timepicker/bootstrap-timepicker.min.css') }}">
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    {{ Lang::get('app.configuration') }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li class="active">{{ Lang::get('app.configuration') }}</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
    <form action="{{ url('/save_configuration') }}" method="post">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="row">
	<!-- left column -->
	<div class="col-md-8">
	  <!-- magaya connection -->
	  <div class="box box-primary">
		<div class="box-header with-border">
		  <h3 class="box-title">{{ Lang::get('app.magaya_connection') }}</h3>
		</div><!-- /.box-header -->
		<!-- form start -->
		  <div class="box-body">
			<div class="form-group">
			  <label for="magaya_host">{{ Lang::get('app.magaya_host') }}</label>
			  <input type="text" class="form-control" id="magaya_host" name="magaya_host" data-inputmask="'alias': 'ip'" data-mask value="{{ isset($viewData['configuration']->magaya_host)?$viewData['configuration']->magaya_host:'' }}">
			  <span class="help-block">{{ $errors->first('magaya_host') }}</span>
			</div>
			<div class="form-group">
			  <label for="magaya_port">{{ Lang::get('app.magaya_port') }}</label>
			  <input type="text" class="form-control" id="magaya_port" name="magaya_port" data-inputmask="'mask' : '9[9][9][9][9]'" data-mask value="{{ isset($viewData['configuration']->magaya_port)?$viewData['configuration']->magaya_port:'' }}">
			  <span class="help-block">{{ $errors->first('magaya_port') }}</span>
			</div>
			<div class="form-group">
			  <label for="magaya_user">{{ Lang::get('app.magaya_user') }}</label>
			  <input type="text" class="form-control" id="magaya_user" name="magaya_user" value="{{ isset($viewData['configuration']->magaya_user)?$viewData['configuration']->magaya_user:'' }}">
			  <span class="help-block">{{ $errors->first('magaya_user') }}</span>
			</div>
			<div class="form-group">
			  <label for="magaya_password">{{ Lang::get('app.magaya_password') }}</label>
			  <input type="text" class="form-control" id="magaya_password" name="magaya_password" value="{{ isset($viewData['configuration']->magaya_password)?$viewData['configuration']->magaya_password:'' }}">
			  <span class="help-block">{{ $errors->first('magaya_password') }}</span>
			</div>
		  </div><!-- /.box-body -->
	  </div><!-- /.box -->
    </div><!-- /.col-lg-6 -->
    </div><!-- /.row -->	
	
	<!-- oracle connection -->
	<div class="row">
	<!-- left column -->
	<div class="col-md-8">
	  <!-- general form elements -->
	  <div class="box box-primary">
		<div class="box-header with-border">
		  <h3 class="box-title">{{ Lang::get('app.oracle_connection') }}</h3>
		</div><!-- /.box-header -->
		<!-- form start -->
		  <div class="box-body">
			<div class="form-group">
			  <label for="oracle_user">{{ Lang::get('app.oracle_user') }}</label>
			  <input type="text" class="form-control" id="oracle_user" placeholder="" name="oracle_user" value="{{ isset($viewData['configuration']->oracle_user)?$viewData['configuration']->oracle_user:'' }}">
			  <span class="help-block">{{ $errors->first('oracle_user') }}</span>
			</div>
			<div class="form-group">
			  <label for="oracle_password">{{ Lang::get('app.oracle_password') }}</label>
			  <input type="text" class="form-control" id="oracle_password" placeholder="" name="oracle_password" value="{{ isset($viewData['configuration']->oracle_password)?$viewData['configuration']->oracle_password:'' }}">
			  <span class="help-block">{{ $errors->first('oracle_password') }}</span>
			</div>
			<div class="form-group">
			  <label for="oracle_datasource">{{ Lang::get('app.oracle_datasource') }}</label>
			  <textarea class="form-control" id="oracle_datasource" rows="5" name="oracle_datasource" >{{ isset($viewData['configuration']->oracle_datasource)?$viewData['configuration']->oracle_datasource:'' }}</textarea>
			  <span class="help-block">{{ $errors->first('oracle_datasource') }}</span>
			</div>
		  </div><!-- /.box-body -->
	  </div><!-- /.box -->
    </div><!-- /.col-lg-6 -->
    </div><!-- /.row -->
	
	
	<!-- recurrence -->
	<div class="row">
	<!-- left column -->
	<div class="col-md-8">
	  <!-- general form elements -->
	  <div class="box box-primary">
		<div class="box-header with-border">
		  <h3 class="box-title">{{ Lang::get('app.service_recurrence_details') }}</h3>
		</div><!-- /.box-header -->
		<!-- form start -->
		  <div class="box-body">
			<div class="form-group">
			  <label for="service_recurrence">{{ Lang::get('app.service_recurrence') }}</label>
			  <?php $service_recurrence = isset($viewData['configuration']->service_recurrence)?$viewData['configuration']->service_recurrence:'' ?>
			  <select class="form-control" id="service_recurrence" name="service_recurrence">
				<option value="hour_1" {{ $service_recurrence=='hour_1'?'selected="selected"':'' }} >{{ Lang::get('app.recurrence_hour_1') }}</option>
				<option value="hour_2" {{ $service_recurrence=='hour_2'?'selected="selected"':'' }} >{{ Lang::get('app.recurrence_hour_2') }}</option>
				<option value="hour_4" {{ $service_recurrence=='hour_4'?'selected="selected"':'' }} >{{ Lang::get('app.recurrence_hour_4') }}</option>
				<option value="hour_8" {{ $service_recurrence=='hour_8'?'selected="selected"':'' }} >{{ Lang::get('app.recurrence_hour_8') }}</option>
				<option value="hour_12" {{ $service_recurrence=='hour_12'?'selected="selected"':'' }} >{{ Lang::get('app.recurrence_hour_12') }}</option>
				<option value="daily" {{ $service_recurrence=='daily'?'selected="selected"':'' }} >{{ Lang::get('app.recurrence_daily') }}</option>
				<option class="hide" value="weekly" {{ $service_recurrence=='weekly'?'selected="selected"':'' }} >{{ Lang::get('app.recurrence_weekly') }}</option>
				<option  class="hide"  value="monthly" {{ $service_recurrence=='monthly'?'selected="selected"':''}} >{{ Lang::get('app.recurrence_monthly') }}</option>
			  </select>
			</div>
			<div class="bootstrap-timepicker" style="{{ ($service_recurrence=='daily' || $service_recurrence=='weekly' || $service_recurrence=='monthly')?'':'display: none;' }}" >
				<div class="form-group">
				  <label for="service_hour">{{ Lang::get('app.service_hour') }} {{ $service_recurrence }}</label>
				   <div class="input-group">
					<input  type="text" class="form-control timepicker" id="service_hour"  name="service_hour" value="{{ isset($viewData['configuration']->service_hour)?$viewData['configuration']->service_hour:'' }}">
					<div class="input-group-addon">
					  <i class="fa fa-clock-o"></i>
					</div>
				  </div><!-- /.input group -->
				  <span class="help-block">{{ $errors->first('service_hour') }}</span>
				</div>
			</div> <!-- /.bootstrap-timepicker -->
		  </div><!-- /.box-body -->
	  
	  </div><!-- /.box -->
    </div><!-- /.col-lg-6 -->
    </div><!-- /.row -->
	
	<!-- Magaya Data -->
	<div class="row">
	<!-- left column -->
	<div class="col-md-8">
	  <!-- general form elements -->
	  <div class="box box-primary">
		<div class="box-header with-border">
		  <h3 class="box-title">{{ Lang::get('app.magaya_data') }}</h3>
		</div><!-- /.box-header -->
		<!-- form start -->
		  <div class="box-body">
			<div class="form-group">
			  <label for="magaya_data_from">{{ Lang::get('app.magaya_data_from') }}</label>
			  <?php $data_from = isset($viewData['configuration']->magaya_data_from)?$viewData['configuration']->magaya_data_from:'' ?>
			  <select class="form-control" id="magaya_data_from" name="magaya_data_from">
				<option value="30" {{ $data_from=='30'?'selected="selected"':'' }} >{{ Lang::get('app.thirty_days') }}</option>
				<option value="60" {{ $data_from=='60'?'selected="selected"':'' }} >{{ Lang::get('app.sixty_days') }}</option>
				<option value="90" {{ $data_from=='90'?'selected="selected"':'' }} >{{ Lang::get('app.ninety_days') }}</option>
				<option value="120" {{ $data_from=='120'?'selected="selected"':'' }} >{{ Lang::get('app.onetwenty_days') }}</option>
				<option value="150" {{ $data_from=='150'?'selected="selected"':'' }} >{{ Lang::get('app.onefifty_days') }}</option>
				<option value="180" {{ $data_from=='180'?'selected="selected"':'' }} >{{ Lang::get('app.oneeighty_days') }}</option>
			  </select>
			</div>
			<div class="form-group">
			  <label for="magaya_items">{{ Lang::get('app.magaya_items') }}</label>
			   <div class="col-md-12 form-inline">
				<div class="checkbox"><label><input type="checkbox" name="magaya_items_whr"  value="Y"  {{ isset($viewData['configuration']) ? $viewData['configuration']->magaya_items_whr == 'Y' ? 'checked="checked"' : '' : '' }} /> {{ Lang::get('app.magaya_items_whr') }}</label></div>
				<div class="checkbox"><label><input type="checkbox" name="magaya_items_cr"  value="Y"  {{ isset($viewData['configuration']) ? $viewData['configuration']->magaya_items_cr == 'Y' ? 'checked="checked"' : '' : '' }} /> {{ Lang::get('app.magaya_items_cr') }}</label></div>
				<div class="checkbox"><label><input type="checkbox" name="magaya_items_bill"  value="Y"  {{ isset($viewData['configuration']) ? $viewData['configuration']->magaya_items_bill == 'Y' ? 'checked="checked"' : '' : '' }} /> {{ Lang::get('app.magaya_items_bill') }}</label></div>
				<div class="checkbox"><label><input type="checkbox" name="magaya_items_inv"  value="Y"  {{ isset($viewData['configuration']) ? $viewData['configuration']->magaya_items_inv == 'Y' ? 'checked="checked"' : '' : '' }} /> {{ Lang::get('app.magaya_items_inv') }}</label></div>
				<div class="checkbox"><label><input type="checkbox" name="magaya_items_salesord"  value="Y"  {{ isset($viewData['configuration']) ? $viewData['configuration']->magaya_items_salesord == 'Y' ? 'checked="checked"' : '' : '' }} /> {{ Lang::get('app.magaya_items_salesord') }}</label></div>
			   </div>
			</div>
		  </div><!-- /.box-body -->
	  
	  </div><!-- /.box -->
    </div><!-- /.col-lg-6 -->
    </div><!-- /.row -->
	
	
	<div class="row">
	<!-- left column -->
	<div class="col-md-8">
	  <div class="box-footer">
		<button type="submit" class="btn btn-primary">Submit</button>
		<input type="hidden" name="configuration_id" value="{{ isset($viewData['configuration']->id)?$viewData['configuration']->id:'' }}" />
	  </div>
	</div><!-- /.col-lg-6 -->
    </div><!-- /.row -->
	</form>
</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="{{ asset('assets/timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('assets/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('assets/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('assets/input-mask/jquery.inputmask.numeric.extensions.js') }}"></script>
<script src="{{ asset('assets/input-mask/jquery.inputmask.extensions.js') }}"></script>
@endsection
@section('content-script')
<script type="text/javascript">
$(function() {
	
	$('span.help-block').each(function() {
		var s = $(this);
		
		if(s.text().trim().length > 0) {
			
			var c = s.closest('.bootstrap-timepicker');
			
			if(c.length > 0)
				c.addClass('has-feedback has-error');
			else
				s.closest('.form-group').addClass('has-feedback has-error');
			
		}
	});
	
	$("[data-mask]").inputmask();
	//Timepicker
	$(".timepicker").timepicker({showInputs: false, showMeridian: true});

	// Service recurrence
	$('#service_recurrence').change(function() {
		var v = $(this).val();
		var s = $('#service_hour');
		
		if(v == 'daily' || v == 'weekly' || v == 'monthly')
			s.closest('.bootstrap-timepicker').show();
		else
			s.val('').closest('.bootstrap-timepicker').hide();
	});
	
	var message = "{{  $viewData['message']}}";
	
	if(message.length > 0) alert(message);
});
</script>
@endsection