<!DOCTYPE html>
<html>
<style>
  html, body {
    width: 100%;
    height: 100%;
    padding: 0;
    margin: 0;
  }
  #map {
    height: 100%;
    width: 100%;
  }
  .infoWindow {
    width: 300px;
    margin-bottom: 30px;
  }
  .infoWindow .title {
    text-align: center;
  }
  .infoWindow .subtitle {
    font-weight: bold;
    min-width: 75px;
    display: inline-block;
  }
  .infoWindow ul {
    padding: 0;
    margin: 0;
  }
  #custom-control {
    opacity: 0;
  }
  #custom-control > div {
    position: relative;
    display: block;
    margin-top: 10px;
  }
  input[type=text], input[type=autocomplete] {
      display: block;
      width: 300px;
      padding: 8px;
      font-size: 14px;
      line-height: 1.42857143;
      color: #555;
      background-color: #fff;
      background-image: none;
      border: 1px solid #ccc;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
      box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
      -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
      -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
      transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
  }
  .tt-menu {
    background-color: #fff;
    width: 100%;
  }
  .tt-menu .tt-suggestion {
    font-size: 12px;
    padding: 8px;
    cursor: pointer;
  }
</style>
<body>
  <div id="custom-control">
    <div>
      <input id="custom-search" type="text" autocomplete="off" placeholder="Search for an airport" />
    </div>
  </div>
  <div id="map"></div>
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
  </script>
  <script async defer
  src="https://maps.googleapis.com/maps/api/js?key={{ $viewData['google_key'] }}&callback=initMap">
  </script>
  <!-- datepicker -->
  <script src="{{ asset('assets/typeahead/typeahead.js') }}"></script>
  <script>

  var airfields;
  var markers = [];
  var map;
  var tmplInfo = '<div class="infoWindow"><div><h3 class="title">_NAME_</h3></div><div><ul>_INFO_</ul></div></div>';

  try {
    airfields = JSON.parse('{!! json_encode($viewData['airfields'],JSON_HEX_APOS) !!}');
  }
  catch(e) {
    airfields = [];
  }

  function initMap() {
    var uluru = {lat: -25.363, lng: 131.044};
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 5,
      center: uluru,
      //mapTypeId: 'satellite'
    });

    var customControl = document.getElementById('custom-control');
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(customControl)

    markers = airfields.map(function(airfield, i) {

      var li    = '<li><span class="subtitle">Code:</span> ' + airfield.code + '</li>';
      li += '<li><span class="subtitle">Alt Name:</span> ' + airfield.alt_name + '</li>';
      li += '<li><span class="subtitle">Latitude:</span> ' + airfield.latitude_dms + '</li>';
      li += '<li><span class="subtitle">Longitude:</span> ' + airfield.longitude_dms + '</li>';

      var tmpl  = tmplInfo.replace('_NAME_',airfield.name).replace('_INFO_',li);

      var infowindow = new google.maps.InfoWindow({
        content: tmpl
      });

      var marker = new google.maps.Marker({
        position: {lat: parseFloat(airfield.latitude_dec),lng: parseFloat(airfield.longitude_dec) }
      });

      marker.addListener('click',function() {
        infowindow.open(map, marker);
      })

      return marker;
    });

    // Add a marker clusterer to manage the markers.
    var markerCluster = new MarkerClusterer(map, markers,
        {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

    google.maps.event.addListenerOnce(map, 'tilesloaded', function() {
      customControl.style.opacity = 1;
    });


  }

  $(document).ready(function() {
    function substringMatcher(values) {
      return function findMatches(q, cb) {
        var matches, substringRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        try {
          substrRegex = new RegExp(q, 'i');
        }
        catch(e) {
          console.log('Error');
          cb(matches);
          return;
        }

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(values, function(i, value) {
          if (substrRegex.test(value.name)) {
            matches.push(value);
          }
        });

        cb(matches);
      };
    }

    $('#custom-search').each(function() {
      var source = airfields;

      $(this).typeahead({
        hint: true,
        highlight: true,
        minLength: 1
      },
      {
        name: 'source',
        source: substringMatcher(source),
        displayKey: 'name',
        templates: {
          empty: [
            '<div class="empty-message">',
              'unable to find any Best Picture winners that match the current query',
            '</div>'
          ].join('\n'),
          suggestion: function(data) {
            return '<p >'+data.name+'</p>';
          },
        }
      });

    });

    $('#custom-search').on('typeahead:selected', function (e, item) {
      // Search selected airport and move the map
      var position = {lat:parseFloat(item.latitude_dec),lng:parseFloat(item.longitude_dec)};
      map.panTo(position);
      map.setZoom(14);
    });
  })
  </script>
</body>
</html>
