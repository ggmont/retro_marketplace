@extends('brcode.layout.app')
@section('content-css-include')
<link rel="stylesheet" href="{{ asset('assets/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  {{ $viewData['form_title'] }} EAN
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li class="active">{{ $viewData['form_title'] }}</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
	<!-- column -->
		<div class="col-md-12">
		<!-- magaya connection -->
			<div class="box">
				<div class="box-body">
					<div class="row br-dataTable-Options">

					</div>
					<table id="table" class="table table-bordered table-striped br-datatable" br-datatable-url="{{ $viewData['form_url_dt'] }}">
					<thead>
						<tr>
							<th>ID</th>
							<th>Username</th>
							<th>Game</th>
							<th>EAN</th>
							<th>Images</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						@foreach($inventories as $key)
							<tr class="ean_{{$key->id}}">
								<td> {{ $key->id }} </td>
								<td> {{ $key->user->user_name }}</td>
								<td> {{ $key->product->name }} </td>
								<td> <input type="text" id="ean{{ $key->id }}" class="form-control form-control-xs" value="{{ $key->ean }}"> <button class="btn btn-success btn-sm btn-ean" data-id="{{ $key->id }}"><i class="fa fa-check"></i></button> </td>
								<td> 
									@foreach($key->imagesean as $keyimg)
										<a href="{{ url('/uploads/ean-images/'. $keyimg->image_path) }}" target="_blank"> <i class="fa fa-image"></i> </a>
									@endforeach
								</td>
								<td> 
									<button class="btn btn-warning btn-sm btn-ean-check" data-id="{{ $key->id }}"> <i class="fa fa-check"></i> Revisado </button>
								</td>
							</tr>
						@endforeach
					</tbody>
					</table>
				</div> <!-- /.box-body -->
			</div> <!-- /.box -->
		</div> <!-- /.col-md-12 -->
	</div> <!-- /.row -->

</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="{{ asset('assets/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/datatables/Responsive-2.2.0/js/dataTables.responsive.min.js') }}"></script>
@endsection
@section('content-script')
<script>
	$('.btn-ean-check').click(function(){
		if(confirm('Se verifico el código EAN?')){

			id = $(this).data('id');
			text = "?eancheck=" + id;

			$.ajax({
				url: "/11w5Cj9WDAjnzlg0/marketplace/inventory/ean" + text,
			}).done(function(data) {
				if(data.status){
					notySettings = {
						layout: 'bottomRight',
						text: "Actualizado correctamente",
						theme: 'bootstrapTheme',
						type: "success",
						timeout: 5000,
					}
					var n = noty(notySettings);
					$('.ean_' + id).remove();
				} else {
					notySettings = {
						layout: 'bottomRight',
						text: "Ocurrio un error al procesar la solicitud",
						theme: 'bootstrapTheme',
						type: "error",
						timeout: 5000,
					}
					var n = noty(notySettings);
				}
			});
		}
	});
	// $("#ean"+id).val();
</script>
<script type="text/javascript">
$(function() {
	$(document).ready(function() {
		$('.br-datatable').DataTable();

		$('.btn-ean').click(function(){
			id = $(this).data('id');
			// $("#ean"+id).val();
			text = "?ean=" +$("#ean"+id).val()+"&id="+id;
			$.ajax({
				url: "/11w5Cj9WDAjnzlg0/marketplace/inventory/ean"+text,
			}).done(function( data ) {
				if(data.status){
					notySettings = {
						layout: 'bottomRight',
						text: "Actualizado correctamente",
						theme: 'bootstrapTheme',
						type: "success",
						timeout: 5000,
					}
					var n = noty(notySettings);
				} else {
					notySettings = {
						layout: 'bottomRight',
						text: "Ocurrio un error al procesar la solicitud",
						theme: 'bootstrapTheme',
						type: "error",
						timeout: 5000,
					}
					var n = noty(notySettings);
				}
			});
		});
	});
});
</script>
@endsection
