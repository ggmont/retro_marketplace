@extends('brcode.layout.app')
@section('content-css-include')
<link rel="stylesheet" href="{{ asset('assets/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/select2/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  {{ $viewData['form_title'] }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li class="active">{{ $viewData['form_title'] }}</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
	<!-- column -->
		<div class="col-md-12">
		<!-- magaya connection -->
			<div class="box">
				<div class="box-body">
					<form action="{{url('/11w5Cj9WDAjnzlg0/marketplace/orders')}}" id="form_order"method="post">
						{{csrf_field()}}
						<div class="row">
							<div class="col-md-3">
									<label>ID</label>
									<select class="form-control select_data" aria-hidden="true" name="d1" onchange="filterData(this)">
										<option value=""></option>
										@foreach($ids as $key)
											<option value="{{$key->order_identification}}" {{ isset($d1) ? ( $d1 == $key->order_identification ? 'selected': '') :'' }} >{{$key->order_identification}}</option>
										@endforeach
									</select>
							</div><!-- /.col -->
							<div class="col-md-3">
									<label>Comprador</label>
									<select class="form-control select_data" aria-hidden="true" name="d2" onchange="filterData(this)">
										<option value=""></option>
										@foreach($buyers as $key)
											<option value="{{ $key->buyer ? $key->buyer->user_name  : '' }}"  {{ $key->buyer ? (isset($d2) ? ( $d2 == $key->buyer->user_name ? 'selected': '') :'') : '' }} >{{ $key->buyer ? (ucfirst($key->buyer->last_name) . ' ' . ucfirst($key->buyer->first_name)) : '' }}</option>
										@endforeach
									</select>
							</div><!-- /.col -->
							<div class="col-md-3">
									<label>Vendedor</label>
									<select class="form-control select_data" aria-hidden="true" name="d3" onchange="filterData(this)">
										<option value=""></option>
										@foreach($sellers as $key)
											<option value="{{ $key->seller ? $key->seller->user_name  : '' }}" {{ $key->seller ? (isset($d3) ? ( $d3 == $key->seller->user_name ? 'selected': '') :'') : '' }}>{{ $key->seller ? (ucfirst($key->seller->last_name) . ' ' . ucfirst($key->seller->first_name)) : '' }}</option>
										@endforeach
									</select>
							</div><!-- /.col --> 
							<div class="col-md-3">
									<label>Estado</label>
									<select class="form-control select_data" aria-hidden="true" name="d4" onchange="filterData(this)">
										<option value=""></option>
										@foreach($status as $key)
											<option value="{{ $key->status }}"  {{ isset($d4) ? ( $d4 == $key->status ? 'selected': '') :'' }}>{{ ucfirst($key->statusDes)  }}</option>
										@endforeach
									</select>
							</div><!-- /.col --> 
						</div>
					</form>
					<br>
					<table id="" class="table table-bordered table-striped" >
					<thead>
						<tr>
							<td>ID</td>
							<td>IP</td>
							<td>Comprador</td>
							<td>Vendedor</td>
							<td>Status</td>
							<td>Fecha</td>
							<td>Total</td>
						</tr>
					</thead>
					<tbody>
						@foreach($orders as $key)
							<tr>
								<td>{{$key->order_identification}}</td>
								
								<td>{{ App\AppOrgUserEvent::where('message_values', 'like',  '%' . $key->order_identification . '%' )->first() ? App\AppOrgUserEvent::where('message_values', 'like',  '%' . $key->order_identification . '%' )->first()->message_values['ip_user'] : '' }}</td>
								<td>{{ $key->buyer ? ($key->buyer->first_name . ' '. $key->buyer->last_name  ) : ''  }}</td>
								<td>{{ $key->seller ? ($key->seller->first_name . ' '. $key->seller->last_name) : '' }}</td>
								<td>{{$key->statusDes}}</td>
								<td> {{$key->updated_at->format('d/m/Y')}} </td>
								<td>{{ number_format($key->total, 2)}}</td>
							</tr>
						@endforeach
					</tbody>
					</table>
					{{ $orders->links() }}
				</div> <!-- /.box-body -->
			</div> <!-- /.box -->
		</div> <!-- /.col-md-12 -->
	</div> <!-- /.row -->

</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="{{ asset('assets/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/datatables/Responsive-2.2.0/js/dataTables.responsive.min.js') }}"></script>

@endsection
@section('content-script')
<script type="text/javascript">
$(function() {

	$(document).ready(function() {
		$('.select_data').select2({
			sorter: function(data) {
        /* Sort data using lowercase comparison */
        return data.sort(function (a, b) {
            a = a.text.toLowerCase();
            b = b.text.toLowerCase();
            if (a > b) {
                return 1;
            } else if (a < b) {
                return -1;
            }
            return 0;
        });
    	}
		});

	});
});
</script>
<script>
	function filterData(x){
		$('#form_order').submit();
	}
</script>
@endsection
