@extends('brcode.layout.app')
@section('content-css-include')
<link rel="stylesheet" href="{{ asset('assets/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
@endsection
@section('content')
	<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
	Shipping
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/11w5Cj9WDAjnzlg0') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li><a href="{{ url('/11w5Cj9WDAjnzlg0/marketplace/shipping') }}">Shipping</a></li>
    <li class="active">Add new shipping</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
            <!-- left column -->
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">New record</h3>
				</div><!-- /.box-header -->
				<form action="{{ url('/11w5Cj9WDAjnzlg0/marketplace/shipping/add') }}" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="box-body">
						@if ($errors->any())
							<div class="alert alert-danger">
								Corrija los errores en el formulario
							</div>
						@endif
						<div class="form-group @if($errors->has('name'))	has-error @endif">
							<label for="name">{{ Lang::get('app.Nombre de envío') }}</label>
							<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
							@if($errors->has('name'))	
							<span class="label label-danger">
								@foreach ($errors->get('name') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
							
						</div>
						<div class="form-group @if($errors->has('certified'))	has-error @endif">
							<label for="certified">{{ Lang::get('app.Certificado') }}</label>
							<select name="certified" id="certified" class="form-control">
								<option value="">...</option>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
							@if($errors->has('certified'))	
							<span class="label label-danger">
								@foreach ($errors->get('certified') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>

						<div class="form-group @if($errors->has('type'))	has-error @endif">
							<label for="type">{{ Lang::get('app.Tipo de envío') }}</label>

							<select name="type" id="type" class="form-control" onchange="updateType(this)">
								<option value="">...</option>
								<option value="PA">Paquete</option>
								<option value="CA">Carta</option>
							</select>

							@if($errors->has('type'))	
							<span class="label label-danger">
								@foreach ($errors->get('type') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>
						
						<div class="form-group @if($errors->has('from_id'))	has-error @endif">
							<label for="from_id">{{ Lang::get('app.País Origen') }}</label>
							<select name="from_id" id="from_id" class="form-control">
								<option value="">...</option>
								@foreach($countries as $key)
									<option value="{{ $key->id }}">{{ $key->name }}</option>
								@endforeach
							</select>
							@if($errors->has('from_id'))	
							<span class="label label-danger">
								@foreach ($errors->get('from_id') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>

						<div class="form-group @if($errors->has('to_id'))	has-error @endif">
							<label for="to_id">{{ Lang::get('app.País Destinatario') }}</label>
							<select name="to_id" id="to_id" class="form-control">
								<option value="">...</option>
								@foreach($countries as $key)
									<option value="{{ $key->id }}">{{ $key->name }}</option>
								@endforeach
							</select>
							@if($errors->has('to_id'))	
							<span class="label label-danger">
								@foreach ($errors->get('to_id') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>

						<div class="form-group @if($errors->has('large'))	has-error @endif">
							<label for="large">{{ Lang::get('app.Largo') }}</label>
							<input type="number" class="form-control" id="large" name="large" value="{{ old('large') }}">
							@if($errors->has('large'))	
							<span class="label label-danger">
								@foreach ($errors->get('large') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>
						<div class="form-group @if($errors->has('width'))	has-error @endif">
							<label for="width">{{ Lang::get('app.Ancho') }}</label>
							<input type="number" class="form-control" id="width" name="width" value="{{ old('width') }}">
							@if($errors->has('width'))	
							<span class="label label-danger">
								@foreach ($errors->get('width') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>
						<div class="form-group @if($errors->has('hight'))	has-error @endif">
							<label for="hight">{{ Lang::get('app.Alto') }}</label>
							<input type="number" class="form-control" id="hight" name="hight" value="{{ old('hight') }}">
							@if($errors->has('hight'))	
							<span class="label label-danger">
								@foreach ($errors->get('hight') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>
						<div class="form-group @if($errors->has('max_weight'))	has-error @endif">
							<label for="max_weight">{{ Lang::get('app.Peso máximo') }}</label>
							<input type="number" class="form-control" id="max_weight" name="max_weight"  value="{{ old('max_weight') }}">
							@if($errors->has('max_weight'))	
							<span class="label label-danger">
								@foreach ($errors->get('max_weight') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>
						<div class="form-group @if($errors->has('max_value'))	has-error @endif">
							<label for="max_value">{{ Lang::get('app.Valor máximo de pedido') }}</label>
							<input type="number" class="form-control" id="max_value" name="max_value" step="0.01" value="{{ old('max_value') }}">
							@if($errors->has('max_value'))	
							<span class="label label-danger">
								@foreach ($errors->get('max_value') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>
						<div class="form-group @if($errors->has('stamp_price'))	has-error @endif">
							<label for="stamp_price">{{ Lang::get('app.Franqueo') }}</label>
							<input type="number" onchange="priceStamp(this)" class="form-control" id="stamp_price" name="stamp_price" step="0.01"  value="{{ old('stamp_price') }}">
							@if($errors->has('stamp_price'))	
							<span class="label label-danger">
								@foreach ($errors->get('stamp_price') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>
						<div class="form-group @if($errors->has('price'))	has-error @endif">
							<label for="price">{{ Lang::get('app.Precio') }}</label>
							<input type="text"  class="form-control" readonly value="{{ old('price') }}">
							@if($errors->has('price'))	
							<span class="label label-danger">
								@foreach ($errors->get('price') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>
						
					</div><!-- /.box-body -->
					<div class="box-footer">
						<a href="{{ url('/11w5Cj9WDAjnzlg0/marketplace/shipping')}}" class="btn btn-default"><i class="fa fa-chevron-left"></i> Cancel</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Submit</button>
					</div>	
				</form>
			</div>
		</div>
	</div>
</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="{{ asset('assets/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/datatables/Responsive-2.2.0/js/dataTables.responsive.min.js') }}"></script>
@endsection
@section('content-script')
<script type="text/javascript">
$(document).ready(function() {
	const certifiedOldValue = '{{ old('certified') }}';
	const fromOldValue = '{{ old('from_id') }}';
	const toOldValue = '{{ old('to_id') }}';
	const toTypeValue = '{{ old('type') }}';
	if(certifiedOldValue !== '') {
		$('#certified').val(certifiedOldValue);
	}
	if(fromOldValue !== '') {
		$('#from_id').val(fromOldValue);
	}
	if(toOldValue !== '') {
		$('#to_id').val(toOldValue);
	}
	if(toTypeValue !== '') {
		$('#type').val(toTypeValue);
	}
});

function priceStamp(x){
	if($('#type').val() == 'PA' || $('#type').val() == 'CA'){
		if($('#type').val() == 'PA'){
			$('#price').val( parseFloat($('#stamp_price').val()) + 1);
		}
		if($('#type').val() == 'CA'){
			$('#price').val( parseFloat($('#stamp_price').val()) + 0.5);
		}
	}else{
		$('#stamp_price').val('');
		alert('Seleccione el tipo de envío');
	}
}
function updateType(x){
	$('#stamp_price').val('');
}
</script>
@endsection
