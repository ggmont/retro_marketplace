@extends('brcode.layout.app')
@section('content-css-include')
<link rel="stylesheet" href="{{ asset('assets/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  {{ $viewData['form_title'] }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li class="active">{{ $viewData['form_title'] }}</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
	<!-- column -->
		<div class="col-md-12">
		<!-- magaya connection -->
			<div class="box">
				<div class="box-body">
					<div class="box-header with-border">
						<h3 class="box-title"><a href="{{ url('/11w5Cj9WDAjnzlg0/marketplace/shipping/add')}}" class="btn btn-primary btn-md">Add New Record</a></h3>
					</div>
					<div class="row br-dataTable-Options">
						
					</div>
					<table id="" class="table table-bordered table-striped br-datatable-shipping">
					<thead>
						<tr>
							@if(isset($viewData['table_cols']))
							@foreach($viewData['table_cols'] as $col)
								@if(Lang::get($col) == 'Name' || Lang::get($col) == 'Volume')
								<th style="width:20%">{{ Lang::get($col) }}</th>
								@else
								<th>{{ Lang::get($col) }}</th>
								@endif

							@endforeach
							@endif
							<!--<th>{{ Lang::get('app.action') }}</th>-->
						</tr>
					</thead>
					<tbody>

					</tbody>
					</table>
				</div> <!-- /.box-body -->
			</div> <!-- /.box -->
		</div> <!-- /.col-md-12 -->
	</div> <!-- /.row -->

</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="{{ asset('assets/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/datatables/Responsive-2.2.0/js/dataTables.responsive.min.js') }}"></script>
@endsection
@section('content-script')
<script type="text/javascript">
$(document).ready(function() {
    var table = $('.br-datatable-shipping').DataTable(); 
    $.get("{{ url('/11w5Cj9WDAjnzlg0/marketplace/shipping/list-dt')}}", function(data, status){
      var i = 0;
      $.each(data, function (key, value) {  
        //$(".fillDataTable").empty();
        var rows = table.rows().remove().draw();
        for (x in value) {
          var rowNode = table.row.add( [ 
            "<td>" + value[x].d0 + "</td>", 
            "<td>" + value[x].d1 + "</td>", 
            "<td>" + value[x].d2 + "</td>",
            "<td>" + value[x].d3 + "</td>", 
            "<td>" + value[x].d4 + "</td>", 
            "<td>" + value[x].d5 + "</td>",
            "<td>" + value[x].d6+ "</td>", 
            "<td> <a class='btn btn-primary btn-xs' data-id='"+ value[x].d7+ "' onclick='update(this)'> Edit </a></td>", 
            ] ).draw().node();
        }
      }); 
    });
    
});
function update(id){
		console.log($(id).data('id'));
		window.open("{{ url('/11w5Cj9WDAjnzlg0/marketplace/shipping/modify')}}" + '/'+ $(id).data('id') ,"_self")
}
</script>
@endsection
