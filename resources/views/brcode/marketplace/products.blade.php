@extends('brcode.layout.app')
@section('content-css-include')
<link rel="stylesheet" href="{{ asset('brcode/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/jQuery-FileUpload/css/jquery.fileupload.css') }}">
<link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  {{ $viewData['form_title'] }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li class="active">{{ $viewData['form_title'] }} as</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	
	<div class="row">
	<!-- column -->
		<div class="col-md-12">
			
		<!-- magaya connection -->
			<div class="box">
				<div class="box-body">
					@include('partials.flash')
					<div class="row br-dataTable-Options">
						<div class="col-md-6">
							<a href="{{ route('product.create') }}" class="btn btn-primary">{{ Lang::get('app.add') }}</a>
						</div>
						<div class="col-md-3">
							<select id="platform-select" class="form-control">
								<option value="all">Todas las plataformas</option>
								@foreach($viewData['platform_sys'] as $key)
									<option value="{{$key->value}}">{{$key->value}}</option>
								@endforeach		
							</select>
						</div>
						<div class="col-md-3 ">
							<div class="form-group">
								<label for="image-select">
									<input type="checkbox" id="image-select">
									Productos sin imagenes <i class="fa fa-image"></i>
								</label>

							</div>
						</div>

					</div>
					<table id="" class="table table-bordered table-striped br-datatable" br-datatable-url="{{ $viewData['form_url_dt'] }}">
					<thead>
						<tr>
							@if(isset($viewData['table_cols']))
							@foreach($viewData['table_cols'] as $col)
							<th>{{ Lang::get($col) }}</th>
							@endforeach
							@endif
							<th>{{ Lang::get('app.action') }}</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
					</table>
				</div> <!-- /.box-body -->
			</div> <!-- /.box -->
		</div> <!-- /.col-md-12 -->
	</div> <!-- /.row -->

</section><!-- /.content -->
<form action="{{ url('/11w5Cj9WDAjnzlg0/marketplace/products/images') }}" method="post" id="form-image">
<div class="modal fade" id="modal1"tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><div class="name-product"></div></h4>
			</div>
			<div class="modal-body">
			
				<!---->
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="model_id" id="model_id">
				
					<div id="progress" class="progress">
						<div class="progress-bar progress-bar-success"></div>
					</div>
					<div id="dropzone" class="">
						<span class="btn btn-success fileinput-button" style="display: none;">
							<i class="glyphicon glyphicon-plus"></i>

							<input type="file" id="fileupload" name="files[]" data-url="{{ url('/11w5Cj9WDAjnzlg0/marketplace/products/upload_file') }}" multiple>
						
						</span>
						<span class="dropzone-message">{{ Lang::get('app.drop_files_here') }}</span>

					</div>

					<div id="file-uploader-container">
					</div>
				<!---->
			
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>
@endsection

@section('content-script-include')
<script src="{{ asset('assets/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>

<script src="{{ asset('brcode/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('brcode/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('brcode/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('brcode/js/responsive.bootstrap4.min.js') }}"></script>

<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
<!-- The basic File Upload plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
<!-- The File Upload processing plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>
<script src="{{ asset('assets/js/dropzone.js') }}"></script>

@endsection
@section('content-script')
<script type="text/javascript">
$(function() {
	$('[data-number]').each(function() {
		$(this).number(true,$(this).data('number'))
	});
	
	$(document).ready(function() {
		$('#platform-select').val('3DO');
		$('#platform-select').trigger( "change" );
		// $('.br-datatable').each(function() {

		// 	var url = $(this).attr('br-datatable-url');

		// 	$(this).DataTable({
		// 		ajax: url,
		// 		bAutoWidth: false,
		// 		lengthMenu: [[25, 50, 100, 500], [25, 50, 100, 500]],
		// 		columns: [
		// 			{data: 'id', name: 'p.id'},
		// 			{data: 'name', name: 'p.name'},
		// 			{data: 'category', name: 'c.category_text'},
		// 			{data: 'release_date', name: 'p.release_date'},
		// 			{data: 'language', name: 'language'},
		// 			{data: 'platform', name: 'p.platform'},
		// 			{data: 'region', name: 'p.region'},
		// 			{data: 'is_enabled', name: 'p.is_enabled'},
		// 			{data: 'action', searchable: false}
		// 		],
		// 	});

		// });
		

	});
	
});
</script>
<script>
	$('#platform-select').change(function() {
			var table = $('.br-datatable').DataTable();
			table.destroy();
				
			var url = `/11w5Cj9WDAjnzlg0/marketplace/products/list-search/${$('#platform-select').val()}_img_${$('#image-select').prop('checked')}`;
			// var url = `/11w5Cj9WDAjnzlg0/marketplace/products/list-search/3DO_img_false`;
			// $('#platform-select').val('3DO');

			console.log(url);

			$('.br-datatable').DataTable({
				ajax: url,
				bAutoWidth: false,
				lengthMenu: [[25, 50, 100, 500], [25, 50, 100, 500]],
				columns: [
					{data: 'id', name: 'p.id'},
					{data: 'name', name: 'p.name'},
					{data: 'category', name: 'c.category_text'},
					{data: 'release_date', name: 'p.release_date'},
					{data: 'language', name: 'language'},
					{data: 'platform', name: 'p.platform'},
					{data: 'region', name: 'p.region'},
					{data: 'is_enabled', name: 'p.is_enabled'},
					{data: 'action', searchable: false}
				],
			});
	});

	$('#image-select').change(function() {
			var table = $('.br-datatable').DataTable();
			table.destroy();
			if (this.checked) {
        var val = 'true';
    	}else{
				var val = 'false';
			}
			var url = `/11w5Cj9WDAjnzlg0/marketplace/products/list-search/${$('#platform-select').val()}_img_${$('#image-select').prop('checked')}`;
			console.log(url);
			
			$('.br-datatable').DataTable({
				ajax: url,
				bAutoWidth: false,
				lengthMenu: [[25, 50, 100, 500], [25, 50, 100, 500]],
				columns: [
					{data: 'id', name: 'p.id'},
					{data: 'name', name: 'p.name'},
					{data: 'category', name: 'c.category_text'},
					{data: 'release_date', name: 'p.release_date'},
					{data: 'language', name: 'language'},
					{data: 'platform', name: 'p.platform'},
					{data: 'region', name: 'p.region'},
					{data: 'is_enabled', name: 'p.is_enabled'},
					{data: 'action', searchable: false}
				],
			});
	});
	

	function openModalImage(x){
		
		$(".file-uploader-preview").remove();
		$('.progress-bar').css('width', '0%').attr('aria-valuenow', 0);  
		$.get("{{ url('/11w5Cj9WDAjnzlg0/marketplace/products/images') }}" + '/'+$(x).data('product')  , function(data, status){
			
			console.log("Datas: " + data.quant + "\nStatus: " + status);
			if(data.quant > 0){
 
			}
			
		});
		$('.name-product').html('Images from: ' + $(x).data('name') + ' ' + $(x).data('product'));	
		$('#model_id').val($(x).data('product'));
		$('#modal1').modal();
		
	}
	$('#modal1').on('hidden.bs.modal', function () {
		$(".file-uploader-preview").remove();
		$('.progress-bar').css('width', '0%').attr('aria-valuenow', 0);  
	})
	
	var frm = $('#form-image');
    frm.submit(function (ev) {
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {
				$('#modal1').modal('toggle');
                var buttons = [
					{addClass: 'btn btn-primary btn-sm pull-left', text: '{{ Lang::get('app.delete') }}', onClick: function($noty) {$noty.close();deleteRecord()} },
					{addClass: 'btn btn-danger btn-sm pull-right', text: '{{ Lang::get('app.cancel') }}', onClick: function($noty) {$noty.close();$('#br-btn-delete').button('reset');} },
				];

				presentNotyMessage('{{Lang::get('app.image_change') }}','success',1);
            }
        });

        ev.preventDefault();
    });
</script>
<script type="text/javascript">
$(function() {
	$('#br-btn-delete').click(function() {
		$(this).button('loading');

    	var buttons = [
			{addClass: 'btn btn-primary btn-sm pull-left', text: '{{ Lang::get('app.delete') }}', onClick: function($noty) {$noty.close();deleteRecord()} },
			{addClass: 'btn btn-danger btn-sm pull-right', text: '{{ Lang::get('app.cancel') }}', onClick: function($noty) {$noty.close();$('#br-btn-delete').button('reset');} },
		];

		presentNotyMessage('{{Lang::get('app.delete_are_you_sure') }}','warning',buttons,1);
	});

  function select2Template(state) {
		if (!state.id) { return state.text; }

		var children = $(state.element).data('children') || 0;

	  var $state = $(
	    '<span '+(children > 0 ? 'style="font-weight: bold;"':'')+'>' + state.text + '</span>'
	  );
	  return $state;
	}

  $('select.br-select2').select2({
    templateResult: select2Template
  });

  $('[data-date-format]').each(function() {
    var format = $(this).data('date-format');
    $(this).parent().datetimepicker({format: format});
  });


  uploadButton = $('<button/>')
    .attr('type','button')
    .addClass('btn btn-primary')
    .text('Upload')
    .on('click', function () {
        var $this = $(this),
            data = $this.data();
        $this
            .off('click')
            .text('Abort')
            .on('click', function () {
                $this.remove();
                data.abort();
            });
        data.submit().always(function () {
            $this.remove();
        });
    });

  editButton = $('<button/>')
  .attr('type','button')
  .addClass('btn btn-primary')
  .text('Edit')
  .on('click', function () {
      var $this = $(this),
          data = $this.data();
      $this.text('Save');
      data.imgEl.Jcrop({aspectRatio: 1/1});
  });

  $('#fileupload').fileupload({
    singleFileUploads: true,
    sequentialUploads: true,
    limitConcurrentUploads: 1,
    dropZone: $('#dropzone'),
    add: function (e, data) {
      data.context = $('<div/>').addClass('file-uploader-preview').appendTo('#dropzone');
      $.each(data.files, function (index, file) {
          if(file.size > 3072000) {
            var msg = $('<p/>')
              .text('{{ Lang::get('app.uploader_file_size_bigger') }}'.replace('_FILENAME_',file.name));
            msg.appendTo($('#file-uploader-messages'));
            data.context.remove();
            return false;
          }
          else if($.inArray(file.name.split('.').pop().toLowerCase(),['gif','png','jpg','jpeg']) == -1) {
            var msg = $('<p/>')
              .text('{{ Lang::get('app.uploader_file_ext_not_supported') }}'.replace('_FILENAME_',file.name));
            msg.appendTo($('#file-uploader-messages'));
            data.context.remove();
            return false;
          }
          var img = $('<img/>').attr('src',URL.createObjectURL(file));
          data.imgEl = img;
          data.inputName = $('#dropzone').find('#fileupload-name').val();
          var node = $('<p/>')
              .append(img);
          node.appendTo(data.context);

      });
      data.submit();
    },
    done: function (e, data) {
      var img = data.imgEl;
      var parent = img.closest('.file-uploader-preview');
      var removeButton = $('<div/>').html('<i class="fa fa-times-circle-o"></i>').addClass('file-uploader-remove').appendTo(parent);
      var input = $('<input/>').attr('type','hidden').attr('name',data.inputName).attr('value',data.result.name).appendTo(parent);

    },
    progress: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    },
  });

  $(document).on('click','.file-uploader-remove',function() {
    var parent = $(this).closest('.file-uploader-preview');
    var image = parent.find('input[type=hidden]').val();
    $.ajax({
      url: '{{ url('/11w5Cj9WDAjnzlg0/marketplace/products/remove_file') }}',
      data: {'_token': $('form input[name="_token"]:eq(0)').val(), img: image},
      type: 'POST',
      success: function(data) {
        parent.hide('fade',250,function() { $(this).remove() } );
      },
      error: function(data) {

      }
    })
  });

  $('#dropzone').on('dragenter dragover', function() {
    $(this).addClass('hover');
  });

  $('#dropzone').on('dragleave dragexit drop', function() {
    $(this).removeClass('hover');
  });


  $(document).ready(function() {
    $('[data-mask]').each(function() {
      var mask = $( this ).data('mask');
      $( this ).inputmask({ mask: mask});
    });
  })

});
</script>
@endsection
