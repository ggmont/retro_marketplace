@extends('brcode.layout.app')
@section('content-css-include')
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="{{ asset('assets/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <!-- File Uploader -->
    <link rel="stylesheet" href="{{ asset('plugins/fileinput/css/fileinput.min.css') }}">
    <link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Products</a></li>
            <li class="active">Agregar</li>
        </ol>
    </section>
    <br>
    <div class="row">
        <div class="content">
            <div class="box">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <center>
                            <h3 class="box-title center"><b>NUEVO PRODUCTO</b></h3>
                        </center>
                        <div class="box-tools pull-right">
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12" style="background-color: #FFFFFF">
						<form class="" action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data">
							{{ method_field( 'POST' ) }}
							{{ csrf_field() }}
                            <br>
                            <div class="form-group col-sm-12">
                                <label class="control-label" for="modelo"><b>DETALLES BASICOS: *</b></label>

                            </div>
                            <div class="form-group col-sm-4">
                                <label class="control-label">Nombre: <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="name"
                                    placeholder="Name" required>
                            </div>

                            <div class="form-group col-sm-4">
                                <label class="control-label">Nombre Alternativo: <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="name_en"
                                    placeholder="Alternative Name" required>
                            </div>

                            <div class="form-group col-sm-4">
                                <label class="control-label">Fecha de lanzamiento: <span
                                        class="text-danger">(Opcional)</span></label>
                                <input class="form-control fe" type="text" name="release_date"
                                    placeholder="Date">
                            </div>

                            <div class="form-group col-sm-12">
                                <label class="control-label" for="modelo"><b>DETALLES DEL PRODUCTO: *</b></label>

                            </div>

                            <div class="form-group col-sm-12">
                                <label class="control-label">Categoria: <span class="text-danger">*</span></label>
                                <select name="prod_category_id[]" multiple="multiple" required="" id="categories"
                                    class="form-control" required="">

                                    @foreach ($categories as $c)
                                        <option value="{{ $c->id }}">{{ $c->category_text }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-sm-6">
                                <label class="control-label">Genero: <span class="text-danger">*</span></label>
                                <select id="genero" name="game_category[]" multiple="multiple" required="" class="form-control">
                                    @foreach ($genero as $g)
                                    <option value="{{ $g->value }}">{{ $g->value }}</option>
                                @endforeach
                                </select>
                            </div>

                            <div class="form-group col-sm-6">
                                <label class="control-label">Localizacion: <span class="text-danger">*</span></label>
                                <select id="location" name="game_location[]" multiple="multiple" class="form-control" required="">
                                    @foreach ($location as $l)
                                    <option value="{{ $l->value }}">{{ $l->value }}</option>
                                @endforeach
                                </select>
                            </div>

                            <div class="form-group col-sm-4">
                                <label class="control-label">Generacion: <span class="text-danger">*</span></label>
                                <select id="generation" name="game_generation[]"  class="form-control">
                                    <option value="">Selecciona</option>
                                    @foreach ($generation as $ge)
                                    <option value="{{ $ge->value_id }}">{{ $ge->value }}</option>
                                @endforeach
                                </select>
                            </div>

                            <div class="form-group col-sm-4">
                                <label class="control-label">Lenguaje: <span class="text-danger">*</span></label>
                                <select id="language" name="language[]" multiple="multiple" class="form-control" required="">
                                    @foreach ($language as $la)
                                    <option value="{{ $la->value }}">{{ $la->value }}</option>
                                @endforeach
                                </select>
                            </div>

                            <div class="form-group col-sm-4">
                                <label class="control-label">Lenguaje (CAJA): <span class="text-danger">(Opcional)</span></label>
                                <select id="box_language" name="box_language[]" multiple="multiple"  class="form-control">
                                    @foreach ($language as $la)
                                    <option value="{{ $la->value }}">{{ $la->value }}</option>
                                @endforeach
                                </select>
                            </div>

                            <div class="form-group col-sm-12">
                                <label class="control-label" for="modelo"><b>DETALLES TECNICOS: *</b></label>

                            </div>

                            <div class="form-group col-sm-4">
                                <label class="control-label">Media: <span class="text-danger">*</span></label>
                                <select id="media" name="media[]" multiple="multiple" class="form-control" required="">
                                    @foreach ($media as $m)
                                    <option value="{{ $m->value }}">{{ $m->value }}</option>
                                @endforeach
                                </select>
                            </div>

                            <div class="form-group col-sm-4">
                                <label class="control-label">Plataforma: <span class="text-danger">*</span></label>
                                <select id="platform" name="platform[]" multiple="multiple" class="form-control" required="">
                                    @foreach ($platform as $p)
                                    <option value="{{ $p->value }}">{{ $p->value }}</option>
                                @endforeach
                                </select>
                            </div>

                            <div class="form-group col-sm-4">
                                <label class="control-label">Region: <span class="text-danger">*</span></label>
                                <select id="region" name="region[]" multiple="multiple" class="form-control" required="">
                                    @foreach ($region as $r)
                                    <option value="{{ $r->value }}">{{ $r->value }}</option>
                                @endforeach
                                </select>
                            </div>

                            <div class="form-group col-sm-4">
                                <label class="control-label">EAN / UPC: <span
                                    class="text-danger"><b>(Opcional)</b></span>
                                </label>
                                <input type="text" name="ean_upc" class="form-control" placeholder="010001">
                            </div>

                            <div class="form-group col-sm-4">
                                <label class="control-label">Volume:
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="volume" class="form-control" placeholder="0,00"
                                    required="">
                            </div>

                            <div class="form-group col-sm-4">
                                <label class="control-label">Peso:
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="number" name="weight" class="form-control"
                                    placeholder="0,00" required="">
                            </div>

                            <label for="input-res-1">Agregar Imagen del Producto <span
                                    class="text-danger"><b>(Opcional)</b></span></label>
                            <div class="file-loading">
                                <input id="input-20" name="image_path[]" type="file" data-browse-on-zone-click="true"
                                    multiple="multiple">
                            </div>
                            <br>
                            <div class="form-group col-sm-12">
                                <label>
                                    Descripcion (Opcional)
                                </label>
                                <textarea id="comments" name="comments" cols="20" rows="10" class="form-control"
                                    placeholder="Añada un comentario con respecto al producto"></textarea>
                            </div>

                            @if (count($errors) > 0)
                                <div class="alert alert-danger alert-important">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="form-group col-sm-12">
                                <span class="text-left"><i class="fa fa-exclamation-circle text-warning"></i> Nota: <small>
                                        Campos requeridos *</small></span>
                                <button class="btn btn-danger pull-right" type="submit">
                                    <i class="fa fa-send"></i> Guardar
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('content-script-include')
    <script src="{{ asset('assets/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->



@endsection
@section('content-script')
    <script src="{{ asset('plugins/fileinput/js/fileinput.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false
            });
            $(".fe").datepicker();
        });

    </script>
    <script>
        $(document).ready(function() {
            $('#categories').select2();
            $('#genero').select2();
            $('#location').select2();
            $('#generation').select2();
            $('#language').select2();
            $('#box_language').select2();
            $('#media').select2();
            $('#platform').select2();
            $('#region').select2();
        });

    </script>
@endsection
