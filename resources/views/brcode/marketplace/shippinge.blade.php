@extends('brcode.layout.app')
@section('content-css-include')
<link rel="stylesheet" href="{{ asset('assets/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
<style>
	.example-modal .modal {
	position: relative;
	top: auto;
	bottom: auto;
	right: auto;
	left: auto;
	display: block;
	z-index: 1;
	}
	.example-modal .modal {
	background: transparent !important;
	}
</style>
@endsection
@section('content')
	<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
		Shipping
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/11w5Cj9WDAjnzlg0') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li><a href="{{ url('/11w5Cj9WDAjnzlg0/marketplace/shipping') }}">Shipping</a></li>
    <li class="active">Add new shipping</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
            <!-- left column -->
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Modify record</h3>
				</div><!-- /.box-header -->
				<form action="{{ url('/11w5Cj9WDAjnzlg0/marketplace/shipping/modify/'.$shipping->id) }}"  method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="box-body">
								@if ($errors->any())
									<div class="alert alert-danger">
										Corrija los errores en el formulario
									</div>
                @endif
						<div class="form-group @if($errors->has('name'))	has-error @endif">
							<label for="name">{{ Lang::get('app.Nombre de envío') }}</label>
							<input type="text" class="form-control" id="name" name="name" placeholder="Enter {{ Lang::get('app.Nombre de envío') }}" value="{{ old('name', $shipping->name) }}">
							@if($errors->has('name'))	
							<span class="label label-danger">
								@foreach ($errors->get('name') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
							
						</div>
						<div class="form-group @if($errors->has('certified'))	has-error @endif">
							<label for="certified">{{ Lang::get('app.Certificado') }}</label>
							<select name="certified" id="certified" class="form-control">
								<option value=""></option>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
							@if($errors->has('certified'))	
							<span class="label label-danger">
								@foreach ($errors->get('certified') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>

						<div class="form-group @if($errors->has('type'))	has-error @endif">
							<label for="type">{{ Lang::get('app.Tipo de envío') }}</label>

							<select name="type" id="type" class="form-control" onchange="updateType(this)">
								<option value="">...</option>
								<option value="PA">Paquete</option>
								<option value="CA">Carta</option>
							</select>

							@if($errors->has('type'))	
								<span class="label label-danger">
									@foreach ($errors->get('type') as $error)
										{{$error}}
									@endforeach
								</span>
							@endif
						</div>

						<div class="form-group @if($errors->has('from_id'))	has-error @endif">
							<label for="from_id">{{ Lang::get('app.País Origen') }}</label>
							<select name="from_id" id="from_id" class="form-control">
								<option value="">...</option>
								@foreach($countries as $key)
									<option value="{{ $key->id }}">{{ $key->name }}</option>
								@endforeach
							</select>
							@if($errors->has('from_id'))	
							<span class="label label-danger">
								@foreach ($errors->get('from_id') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>

						<div class="form-group @if($errors->has('to_id'))	has-error @endif">
							<label for="to_id">{{ Lang::get('app.País Destinatario') }}</label>
							<select name="to_id" id="to_id" class="form-control">
								<option value="">...</option>
								@foreach($countries as $key)
									<option value="{{ $key->id }}">{{ $key->name }}</option>
								@endforeach
							</select>
							@if($errors->has('to_id'))	
							<span class="label label-danger">
								@foreach ($errors->get('to_id') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>
						
						<div class="form-group @if($errors->has('large'))	has-error @endif">
							<label for="large">{{ Lang::get('app.Largo') }}</label>
							<input type="number" class="form-control" id="large" name="large" placeholder="Enter {{ Lang::get('app.Largo') }}" value="{{ old('large', $shipping->large) }}">
							@if($errors->has('large'))	
							<span class="label label-danger">
								@foreach ($errors->get('large') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>
						<div class="form-group @if($errors->has('width'))	has-error @endif">
							<label for="width">{{ Lang::get('app.Ancho') }}</label>
							<input type="number" class="form-control" id="width" name="width" placeholder="Enter {{ Lang::get('app.Ancho') }}" value="{{ old('width', $shipping->width) }}">
							@if($errors->has('width'))	
							<span class="label label-danger">
								@foreach ($errors->get('width') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>
						<div class="form-group @if($errors->has('hight'))	has-error @endif">
							<label for="hight">{{ Lang::get('app.Alto') }}</label>
							<input type="number" class="form-control" id="hight" name="hight" placeholder="Enter {{ Lang::get('app.Alto') }}" value="{{ old('hight', $shipping->hight) }}">
							@if($errors->has('hight'))	
							<span class="label label-danger">
								@foreach ($errors->get('hight') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>
						<div class="form-group @if($errors->has('max_weight'))	has-error @endif">
							<label for="max_weight">{{ Lang::get('app.Peso máximo') }}</label>
							<input type="number" class="form-control" id="max_weight" name="max_weight" placeholder="Enter {{ Lang::get('app.Peso máximo') }}" value="{{ old('max_weight', $shipping->max_weight) }}">
							@if($errors->has('max_weight'))	
							<span class="label label-danger">
								@foreach ($errors->get('max_weight') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>
						<div class="form-group @if($errors->has('max_value'))	has-error @endif">
							<label for="max_value">{{ Lang::get('app.Valor máximo de pedido') }}</label>
							<input type="number" class="form-control" id="max_value" name="max_value" step="0.01" placeholder="Enter {{ Lang::get('app.Valor máximo de pedido') }}" value="{{ old('max_value', $shipping->max_value) }}">
							@if($errors->has('max_value'))	
							<span class="label label-danger">
								@foreach ($errors->get('max_value') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>
						<div class="form-group @if($errors->has('stamp_price'))	has-error @endif">
							<label for="stamp_price">{{ Lang::get('app.Franqueo') }}</label>
							<input type="number" onchange="priceStamp(this)" class="form-control" id="stamp_price" name="{{ Lang::get('app.Franqueo') }}" step="0.01" placeholder="Enter stamp_price" value="{{ old('stamp_price', $shipping->stamp_price) }}">
							@if($errors->has('stamp_price'))	
							<span class="label label-danger">
								@foreach ($errors->get('stamp_price') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>
						<div class="form-group @if($errors->has('price'))	has-error @endif">
							<label for="price">{{ Lang::get('app.Precio') }}</label>
							<input type="text" readonly class="form-control" id="price" name="price" placeholder="Enter price" value="{{ old('price', $shipping->price) }}">
							@if($errors->has('price'))	
							<span class="label label-danger">
								@foreach ($errors->get('price') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
						</div>
						
					</div><!-- /.box-body -->
					<div class="box-footer">
						<div class="row">
							<div class="col-md-6" >
								<a href="{{ url('/11w5Cj9WDAjnzlg0/marketplace/shipping')}}" class="btn btn-default"><i class="fa fa-chevron-left"></i> Cancel</a>
								<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Submit</button>
							</div>
							<div class="col-md-6 text-right">
							<a href="#" class="btn btn-danger" onclick="deleteShip()"><i class="fa fa-trash"></i> Delete</a>
						</div>
						</div>
						
					</div>	
				</form>
			</div>
		</div>
	</div>
</section><!-- /.content -->
<form id="form1" action="{{ url('/11w5Cj9WDAjnzlg0/marketplace/shipping/delete')}}" method="post">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="id" value="{{ $shipping->id }}">
</form>
@endsection

@section('content-script-include')
<script src="{{ asset('assets/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/datatables/Responsive-2.2.0/js/dataTables.responsive.min.js') }}"></script>
@endsection
@section('content-script')
<script type="text/javascript">
	$(document).ready(function() {
		const genderOldValue = '{{ old('certified', $shipping->certified) }}';
		const fromOldValue = '{{ old('from_id', $shipping->from_id) }}';
		const toOldValue = '{{ old('to_id', $shipping->to_id) }}';
		const toTypeValue = '{{ old('type', $shipping->type) }}';
		console.log('{{$shipping->type}}');
		if(genderOldValue !== '') {
			$('#certified').val(genderOldValue);
		}
		if(fromOldValue !== '') {
			$('#from_id').val(fromOldValue);
		}
		if(toOldValue !== '') {
			$('#to_id').val(toOldValue);
		}
		if(toTypeValue !== '') {
			$('#type').val(toTypeValue);
			console.log(toTypeValue);
			if(toTypeValue == 'Paquete'){
				$('#type').val('PA');
			}
			if(toTypeValue == 'Carta'){
				$('#type').val('CA');
			}

		}
		
	});

	function priceStamp(x){
		if($('#type').val() == 'PA' || $('#type').val() == 'CA'){
			if($('#type').val() == 'PA'){
				$('#price').val( parseFloat($('#stamp_price').val()) + 1);
			}
			if($('#type').val() == 'CA'){
				$('#price').val( parseFloat($('#stamp_price').val()) + 0.5);
			}
		}else{
			$('#stamp_price').val('');
			alert('Seleccione el tipo de envío');
		}
	}

	function deleteShip(){
		var buttons = [
			{addClass: 'btn btn-primary btn-sm pull-left', text: '{{ Lang::get('app.delete') }}', onClick: function($noty) {$noty.close();document.getElementById("form1").submit()} },
			{addClass: 'btn btn-danger btn-sm pull-right', text: '{{ Lang::get('app.cancel') }}', onClick: function($noty) {$noty.close();} },
		];
		presentNotyMessage('{{Lang::get('app.delete_are_you_sure') }}','warning',buttons,1);
		
		//console.log('hola');
		//
	}

	function updateType(x){
		$('#stamp_price').val('');
	}
</script>
@endsection
