@extends('brcode.layout.app')
@section('content-css-include')
<link rel="stylesheet" href="{{ asset('assets/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/select2/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  {{ $viewData['form_title'] }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li class="active">{{ $viewData['form_title'] }}</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
	<!-- column -->
		<div class="col-md-12">
		<!-- magaya connection -->
			<div class="box">
				<div class="box-body">
					
					<table id="" class="table table-bordered table-striped" >
					<thead>
						<tr>
							<th style="font-size: 10px;"> {{ __('ID')}}</th>
							<th style="font-size: 10px;"> {{ __('Comprador')}}</th>
							<th style="font-size: 10px;"> {{ __('Vendedor')}}</th>
							<th style="font-size: 10px;"> {{ __('Total')}}</th>
							<th style="font-size: 10px;"> {{ __('Estado')}}</th>
							<th style="font-size: 10px;"> {{ __('Tarcking')}}</th>
							<th style="font-size: 10px;"> {{ __('Fecha gestion')}}</th>
							<th style="font-size: 10px;"> {{ __('Fecha Vencimiento')}}</th>
							<th style="font-size: 10px;"> {{ __('Fecha solución')}}</th>
							<th style="font-size: 10px;"> {{ __('Acciones')}}</th>
						</tr>
					</thead>
					<tbody>
						@foreach($orders as $key)
						<tr style="font-size: 12px;">
								<td>{{ $key->order_identification }}</td>
								<td>{{ $key->buyer ? $key->buyer->user_name : 'None' }}</td>
								<td>{{ $key->seller ? $key->seller->user_name : 'None' }}</td>
								<td>{{  number_format($key->total , 2)  }} €</td>
								<td>{{ $key->statusDes }}</td>
								<td>{{ $key->tracking_number }}</td>
								<td>{{ $key->management_date ? date('d/m/Y', strtotime($key->management_date)) : '' }}</td>
								<td class="bg-{{ $key->expiration_date ? (date('Y-m-d H:i:s') >= $key->expiration_date && $key->shipping_status == 'No' ? 'danger' : ($key->shipping_status == 'Yes' ? 'success' : '')) : '' }}">{{ $key->expiration_date ? date('d/m/Y', strtotime($key->expiration_date)) : ''  }}</td>
								<td>{{ $key->solution_date ? date('d/m/Y', strtotime($key->solution_date)) : ''  }}</td>
								<td>
									<div class="btn-group" role="group" aria-label="Basic example">
										@if(! $key->management_date)
										<form action="{{ route('requestGestion')}}" method="POST">
											{{ csrf_field() }}
											<input type="hidden" name="order" value="{{$key->order_identification}}">
											<button type="submit" class="btn btn-light"  onclick="confirm('Iniciar seguimiento del pedido?')">Gestion</button>
										</form>
										@endif
									</div>
									@if($key->shipping_status == 'No' && ! $key->solution_date)
									<form action="{{ route('requestSolucion')}}" method="POST">
										{{ csrf_field() }}
										<input type="hidden" name="order" value="{{$key->order_identification}}">
										<button type="submit" class="btn btn-light"  onclick="confirm('Recibio el pedido?')">Soluciona</button>
									
									</form>
									@endif
								</td>
								
							</tr>
						@endforeach
					</tbody>
					</table>
					{{ $orders->links() }}
				</div> <!-- /.box-body -->
			</div> <!-- /.box -->
		</div> <!-- /.col-md-12 -->
	</div> <!-- /.row -->

</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="{{ asset('assets/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/datatables/Responsive-2.2.0/js/dataTables.responsive.min.js') }}"></script>

@endsection
@section('content-script')
<script type="text/javascript">
$(function() {

	$(document).ready(function() {
		$('.select_data').select2({
			sorter: function(data) {
        /* Sort data using lowercase comparison */
        return data.sort(function (a, b) {
            a = a.text.toLowerCase();
            b = b.text.toLowerCase();
            if (a > b) {
                return 1;
            } else if (a < b) {
                return -1;
            }
            return 0;
        });
    	}
		});

	});
});
</script>
<script>
	function filterData(x){
		$('#form_order').submit();
	}
</script>
@endsection
