@extends('brcode.layout.app')
@section('content-css-include')
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="{{ asset('assets/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    {{ $viewData['form_title'] }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li><a href="{{ $viewData['form_url_list'] }}">{{ $viewData['form_list_title'] }}</a></li>
    <li class="active">{{ $viewData['form_title'] }}</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
    <form action="{{ $viewData['form_url_post'] }}" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			@include('brcode.layout.app_form')

			@include('brcode.layout.app_form_sub')

			@include('brcode.layout.app_form_buttons')

		</form>
</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="{{ asset('assets/niceditor/nicEdit.js') }}"></script>
@endsection
@section('content-script')
<script type="text/javascript">
$(function() {
		$('#br-btn-delete').click(function() {
		$(this).button('loading');

    var buttons = [
			{addClass: 'btn btn-primary btn-sm pull-left', text: '{{ Lang::get('app.delete') }}', onClick: function($noty) {$noty.close();deleteRecord()} },
			{addClass: 'btn btn-danger btn-sm pull-right', text: '{{ Lang::get('app.cancel') }}', onClick: function($noty) {$noty.close();$('#br-btn-delete').button('reset');} },
		];

		presentNotyMessage('{{Lang::get('app.delete_are_you_sure') }}','warning',buttons,1);
	});

  function select2Template(state) {
		if (!state.id) { return state.text; }

		var children = $(state.element).data('children') || 0;

	  var $state = $(
	    '<span '+(children > 0 ? 'style="font-weight: bold;"':'')+'>' + state.text + '</span>'
	  );
	  return $state;
	}

  $('select.br-select2').select2({
    templateResult: select2Template
  });



	function deleteRecord() {
		var url = '{{ $viewData['form_url_post_delete'] }}';
		var form = $('<form action="' + url + '" method="post">' +
			'<input type="hidden" name="_token" value="{{ csrf_token() }}">' +
			'<input type="hidden" name="model_id" value="{{ $viewData['model_id'] > 0 ? $viewData['model_id'] : '' }}" />' +
			'</form>');
		$('body').append(form);
		form.submit();
	}

});
</script>
@endsection
