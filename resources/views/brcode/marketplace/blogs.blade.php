@extends('brcode.layout.app')
@section('content-css-include')
<link rel="stylesheet" href="{{ asset('assets/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  {{ $viewData['form_title'] }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> {{ Lang::get('app.home') }}</a></li>
    <li class="active">{{ $viewData['form_title'] }}</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
	<!-- column -->
		<div class="col-md-12">
		<!-- magaya connection -->
			<div class="box">
				<div class="box-body">
					<div class="row br-dataTable-Options">
						<div class="col-md-12">
							<a href="{{ $viewData['form_url_add'] }}" class="btn btn-primary">{{ Lang::get('app.add') }}</a>
						</div>
					</div>
					<table id="" class="table table-bordered table-striped br-datatable" br-datatable-url="{{ $viewData['form_url_dt'] }}">
					<thead>
						<tr>
							@if(isset($viewData['table_cols']))
							@foreach($viewData['table_cols'] as $col)
							<th>{{ Lang::get($col) }}</th>
							@endforeach
							@endif
							<th>{{ Lang::get('app.action') }}</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
					</table>
				</div> <!-- /.box-body -->
			</div> <!-- /.box -->
		</div> <!-- /.col-md-12 -->
	</div> <!-- /.row -->

</section><!-- /.content -->
@endsection

@section('content-script-include')
<script src="{{ asset('assets/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/datatables/Responsive-2.2.0/js/dataTables.responsive.min.js') }}"></script>
@endsection
@section('content-script')
<script type="text/javascript">
$(function() {

	$(document).ready(function() {

		$('.br-datatable').each(function() {

			var url = $(this).attr('br-datatable-url');

			$(this).DataTable({

				processing: true,
				serverSide: true,
				ajax: url,
				bAutoWidth: false,
        columns: [
          {data: 'id', name: 'n.id'},
          {data: 'title', name: 'n.title'},
        //   {data: 'source_url', name: 'n.source_url'},
          {data: 'image_path', name: 'n.image_path'},
          {data: 'is_enabled', name: 'is_enabled'},
          {data: 'action', searchable: false}
        ],
			});

		});

	});
});
</script>
@endsection
