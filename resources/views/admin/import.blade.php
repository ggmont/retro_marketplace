@extends('adminlte::page')

@section('title', 'Admin-Dashboard - CSV')
@section('plugins.SelectPicker', true)

@section('content_header')
    <meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0, user-scalable=no" />
    <center>
        <h1>- CSV -</h1>
    </center>
@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/fontawesome.min.css"
        integrity="sha512-cHxvm20nkjOUySu7jdwiUxgGy11vuVPE9YeK89geLMLMMEOcKFyS2i+8wo0FOwyQO/bL8Bvq1KMsqK4bbOsPnA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/style.css?v=2') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/basictable.min.css') }}" />
    @livewireStyles
@stop

@section('content')


    <body>
        <section class="content">
            <span class="retro">@include('partials.flash')</span>

            <div class="flex items-center justify-center">

                <div class="bg-white border-2 border-red-500 rounded-lg shadow-lg p-6 max-w-md w-full">
                    <h2 class="text-2xl font-bold mb-4 text-center">Importar productos desde otros lugares</h2>
                    <p class="text-gray-600 mb-6 text-center">Utiliza esta funcionalidad para añadir productos de otros
                        lugares a los usuarios. Recuerda usar minúsculas en los campos</p>
                    <div class="flex justify-center">
                        <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data" class="w-full">
                            @csrf
                            <div class="mb-4">
                                <label for="csv_file" class="block text-gray-700 text-sm font-bold mb-2">Selecciona un
                                    archivo CSV:</label>
                                <input type="file" name="csv_file" accept=".csv"
                                    class="border border-gray-300 py-2 px-3 rounded-lg focus:outline-none focus:ring focus:border-blue-300 w-full">
                            </div>
                            <button type="submit" id="importButton"
                                class="bg-red-500 hover:bg-red-600 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline w-full"
                                disabled>
                                Importar
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </body>



@stop

@section('js')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var fileInput = document.querySelector('input[name="csv_file"]');
            var importButton = document.getElementById('importButton');

            fileInput.addEventListener('change', function() {
                if (fileInput.files.length > 0) {
                    importButton.removeAttribute('disabled');
                } else {
                    importButton.setAttribute('disabled', 'disabled');
                }
            });
        });
    </script>
    @livewireScripts
@stop
