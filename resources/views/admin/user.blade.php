@extends('adminlte::page')

@section('title', 'Admin-Dashboard - Usuarios')
@section('plugins.DatePicker', true)

@section('content_header')
    <meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0, user-scalable=no" />
    <center>
        <h1>- Usuarios -</h1>
    </center>
@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/fontawesome.min.css"
        integrity="sha512-cHxvm20nkjOUySu7jdwiUxgGy11vuVPE9YeK89geLMLMMEOcKFyS2i+8wo0FOwyQO/bL8Bvq1KMsqK4bbOsPnA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tw-elements/dist/css/index.min.css" />
    <script src="https://cdn.tailwindcss.com/3.2.4"></script>
    <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/style.css?v=2') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/basictable.min.css') }}" />
    <style>
        .tabs {
            display: flex;
            justify-content: space-around;
            margin: 3px;
            height: 40px;
            box-shadow: 0 0 1px 1px rgba(0, 0, 0, 0.2);
        }

        .tabs>* {
            width: 100%;
            color: dimgray;
            height: 100%;
            cursor: pointer;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .tabs>*:hover:not(.active) {
            background-color: rgb(220, 220, 220);
        }

        .tabs>.active {
            color: white;
            background-color: #c53030;

        }

        .panel {
            display: none;
            opacity: 0;
            transition: opacity 0.2s ease-in-out;
        }

        .panel.active {
            display: block;
            opacity: 1;
            transition: opacity 0.2s ease-in-out;
        }
    </style>

    @livewireStyles
@stop

@section('content')

    <head>
        <script type="text/javascript" src="{{ asset('js/datatable/basictable.min.js') }}"></script>
    </head>

    <section class="content">
        <div class="container-fluid">
            <span class="retro">@include('partials.flash')</span>


            <div class="row">


                <div class="col-sm-12">
                    <div class="grid grid-cols-1 gap-6 p-6 bg-white border border-gray-300 rounded-lg shadow-lg">
                        <form action="{{ route('filter_month') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="row">

                                <div class="flex items-center col-md-6">

                                    <select onchange="habilitar(this)" id="mes" name="mes"
                                        class="select2 form-control br-filtering" data-width="100%">
                                        <option value="">Filtrar por mes o...</option>
                                        <option value="1">Enero</option>
                                        <option value="2">Febrero</option>
                                        <option value="3">Marzo</option>
                                        <option value="4">Abril</option>
                                        <option value="5">Mayo</option>
                                        <option value="6">Junio</option>
                                        <option value="7">Julio</option>
                                        <option value="8">Agosto</option>
                                        <option value="9">Septiembre</option>
                                        <option value="10">Octubre</option>
                                        <option value="11">Noviembre</option>
                                        <option value="12">Diciembre</option>
                                    </select>

                                </div>

                                <div class="flex items-center col-md-3">
                                    <label for="simple-search" class="sr-only">Buscar</label>
                                    <div class="relative w-full">
                                        <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                            <i class="fa-sharp fa-regular fa-user"></i>
                                        </div>
                                        <input type="text" type="text" id="from" name="desde"
                                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Desde">
                                    </div>
                                </div>

                                <div class="flex items-center col-md-3">
                                    <label for="simple-search" class="sr-only">Buscar</label>
                                    <div class="relative w-full">
                                        <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                            <i class="fa-sharp fa-regular fa-user"></i>
                                        </div>
                                        <input type="text" id="to" name="hasta"
                                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Buscar por Vendedor">
                                    </div>
                                    <button type="submit"
                                        class="p-2.5 ml-2 text-sm font-medium text-white bg-red-700 rounded-lg border border-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                        <i class="fa fa-search"></i>
                                        <span class="sr-only fa-solid fa-plus">Buscar</span>
                                    </button>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>




            </div>

            <br>


            <div class="tabs">
                <div class="tab" data-target="collaborator">Colaboradores</div>
                <div class="tab active" data-target="standard">Estandar</div>
                <div class="tab" data-target="profesional">Profesional</div>
            </div>
            <div id="panels" class="container">
                <div class="collaborator panel">
                    <livewire:admin.gestion-user-collaborator-table></livewire:admin.gestion-user-collaborator-table>
                </div>
                <div class="standard panel active">
                    <livewire:admin.gestion-user-table></livewire:admin.gestion-user-table>
                </div>
                <div class="profesional panel">
                    <livewire:admin.gestion-user-profesional-table></livewire:admin.gestion-user-profesional-table>
                </div>
            </div>

            <div class="modal fade" tabindex="-1" role="dialog" id="myAdd">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="post" action="{{ route('user_credit') }}">
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">{{ __('Agregar Credito Normal o...') }}</h4>
                            </div>

                            <div class="modal-body">
                                <br>
                                <div class="form-group" style="padding: 10px;">
                                    <label for="credit">{{ __('Tipo de Credito') }}</label>
                                    <select class="form-control" name="type">
                                        <option value="Normal" selected>Normal</option>
                                        <option value="Promocion">Promocion</option>
                                    </select>
                                </div>
                                <div class="form-group" style="padding: 10px;">
                                    <label for="credit">{{ __('Ingrese la cantidad de credito a agregar') }}</label>
                                    <input type="number" name="credit" min="0" required step="0.01"
                                        class="form-control">
                                </div>
                                <div class="form-group" style="padding: 10px;">
                                    <label for="pass">{{ __('Cotraseña') }}</label>
                                    <input type="password" name="pass" class="form-control">
                                </div>
                                <input id="orderDetailsAdd" name="orderDetails" type="hidden">
                                <input name="transaction" type="hidden" value="1">
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default"
                                    data-dismiss="modal">{{ __('Cancelar') }}</button>
                                <button type="submit" class="btn btn-success">{{ __('Guardar') }}</button>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div class="modal fade" tabindex="-1" role="dialog" id="myRemove">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="post" action="{{ route('user_minus_credit') }}">
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">{{ __('Retirar Credito') }}</h4>
                            </div>

                            <div class="modal-body">
                                <br>
                                <div class="form-group" style="padding: 10px;">
                                    <label for="credit">{{ __('Ingrese la cantidad de credito a retirar') }}</label>
                                    <input type="number" name="credit" min="0" required step="0.01"
                                        class="form-control">
                                    <div id="foundRemove"></div>
                                </div>
                                <div class="form-group" style="padding: 10px;">
                                    <label for="pass">{{ __('Contraseña') }}</label>
                                    <input type="password" name="pass" class="form-control">
                                </div>
                                <input id="orderDetailsRemove" name="orderDetails" type="hidden">
                                <input name="transaction" type="hidden" value="0">
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default"
                                    data-dismiss="modal">{{ __('Cancelar') }}</button>
                                <button type="submit" class="btn btn-success">{{ __('Guardar') }}</button>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div class="modal fade" tabindex="-1" role="dialog" id="myAddSpecial">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="post" action="{{ route('user_credit_special') }}">
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">{{ __('Agregar Credito/Saldo Especial') }}</h4>
                            </div>

                            <div class="modal-body">
                                <br>
                                <div class="form-group" style="padding: 10px;">
                                    <label for="credit">{{ __('Ingrese la cantidad de credito a agregar') }}</label>
                                    <input type="number" name="credit" min="0" required step="0.01"
                                        class="form-control">
                                </div>
                                <div class="form-group" style="padding: 10px;">
                                    <label for="pass">{{ __('Cotraseña') }}</label>
                                    <input type="password" name="pass" class="form-control">
                                </div>
                                <input id="orderDetailsAddSpecial" name="orderDetails" type="hidden">
                                <input name="transaction" type="hidden" value="1">
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default"
                                    data-dismiss="modal">{{ __('Cancelar') }}</button>
                                <button type="submit" class="btn btn-success">{{ __('Guardar') }}</button>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div class="modal fade" tabindex="-1" role="dialog" id="myRemoveSpecial">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="post" action="{{ route('user_credit_special') }}">
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">{{ __('Retirar Credito Especial') }}</h4>
                            </div>

                            <div class="modal-body">
                                <br>
                                <div class="form-group" style="padding: 10px;">
                                    <label for="credit">{{ __('Ingrese la cantidad de credito a retirar') }}</label>
                                    <input type="number" name="credit" min="0" required step="0.01"
                                        class="form-control">
                                    <div id="foundRemove"></div>
                                </div>
                                <div class="form-group" style="padding: 10px;">
                                    <label for="pass">{{ __('Contraseña') }}</label>
                                    <input type="password" name="pass" class="form-control">
                                </div>
                                <input id="orderDetailsRemoveSpecial" name="orderDetails" type="hidden">
                                <input name="transaction" type="hidden" value="0">
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default"
                                    data-dismiss="modal">{{ __('Cancelar') }}</button>
                                <button type="submit" class="btn btn-success">{{ __('Guardar') }}</button>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

        </div>
    </section>

@stop


@section('js')

    <script>
        const tabs = document.querySelectorAll(".tabs");
        const tab = document.querySelectorAll(".tab");
        const panel = document.querySelectorAll(".panel");

        function onTabClick(event) {
            // deactivate existing active tabs and panel
            for (let i = 0; i < tab.length; i++) {
                tab[i].classList.remove("active");
            }
            for (let i = 0; i < panel.length; i++) {
                panel[i].classList.remove("active");
                panel[i].style.opacity = "0";
            }

            // activate new tabs and panel
            event.target.classList.add("active");
            let classString = event.target.getAttribute("data-target");
            let panelToShow = document
                .getElementById("panels")
                .getElementsByClassName(classString)[0];
            panelToShow.classList.add("active");
            setTimeout(() => {
                panelToShow.style.opacity = "1";
            }, 50);
        }

        for (let i = 0; i < tab.length; i++) {
            tab[i].addEventListener("click", onTabClick, false);
        }
    </script>

    <script>
        Livewire.on('twoAxistandard', function() {

            const table = new basictable('.table');

            new basictable('#table-breakpoint', {
                breakpoint: 768,
            });

            new basictable('#table-container-breakpoint', {
                containerBreakpoint: 485,
            });

            new basictable('#table-force-off', {
                forceResponsive: false,
            });

            new basictable('#table-max-height', {
                tableWrap: true
            });

            new basictable('#table-no-resize', {
                noResize: true,
            });

            new basictable('#table-two-axis2');

        })
    </script>

    <script>
        Livewire.on('twoAxisPro', function() {

            new basictable('#table-two-axis3');

        })
    </script>

    <script>
        Livewire.on('twoAxisCollaborator', function() {

            new basictable('#table-two-axis4');

        })
    </script>


    <script>
        $(function() {
            $("select").change(function() {
                if ($(this).val() > 0) {
                    $('#from').prop('disabled', true);
                    $('#to').prop('disabled', true);
                } else {
                    $("#from").off();
                    $('#from, #to').prop('disabled', false);
                }
            });
        });

        // -------- datapicker from, to
        $(function() {
            var dateFormat = "dd-mm-yy",

                from = $("#from")
                .datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1
                })
                .on("change", function() {
                    to.datepicker("option", "minDate", getDate(this));
                }),

                to = $("#to").datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1
                })
                .on("change", function() {
                    from.datepicker("option", "maxDate", getDate(this));
                });

            function getDate(element) {
                var date;
                try {
                    date = $.datepicker.parseDate(dateFormat, element.value);
                } catch (error) {
                    date = null;
                }

                return date;
            }
        });

        $("·fe").datepicker();
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            //Eliminar alertas que no contengan la clase alert-important luego de 7seg
            $('div.span').not('.alert-important').delay(7000).slideUp(300);

            //activar Datatable
            $('.data-table').DataTable({
                responsive: true,
            });
        })
    </script>
    <script type="text/javascript">
        function openAdd(x) {
            $('#myAdd').modal('show');
            $('#orderDetailsAdd').val($(x).data('id'));
        }

        function openAddSpecial(x) {
            $('#myAddSpecial').modal('show');
            $('#orderDetailsAddSpecial').val($(x).data('id'));
        }

        function openRemove(x) {
            $('#myRemove').modal('show');
            $('#orderDetailsRemove').val($(x).data('id'));
            $('#foundRemove').text("{{ __('Cantidad maxima') }} " + $(x).data('cre') + " €");
        }

        function openRemoveSpecial(x) {
            $('#myRemoveSpecial').modal('show');
            $('#orderDetailsRemoveSpecial').val($(x).data('id'));
            $('#foundRemove').text("{{ __('Cantidad maxima') }} " + $(x).data('cre') + " €");
        }

        $(function() {

            $(document).ready(function() {

                $('.br-datatable').each(function() {

                    var url = $(this).attr('br-datatable-url');

                    $(this).DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: url,
                        bAutoWidth: false,
                    });
                });

            });
        });
    </script>
    @livewireScripts
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
@stop
