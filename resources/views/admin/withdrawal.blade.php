@extends('adminlte::page')

@section('title', 'Admin-Dashboard - Retiros')

@section('plugins.JqueryDatatable', true)

@section('content_header')
<h1 class="font-bold text-2xl my-8 text-center">- Retiros -</h1>
@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/fontawesome.min.css"
        integrity="sha512-cHxvm20nkjOUySu7jdwiUxgGy11vuVPE9YeK89geLMLMMEOcKFyS2i+8wo0FOwyQO/bL8Bvq1KMsqK4bbOsPnA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">.
    <style>
        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }
    </style>


    @livewireStyles
@stop

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box">
                    <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-euro-sign fa-lg"></i></span>
                    <div class="info-box-content">
                      <span class="font-weight-bold info-box-text">Total</span>
                      <span class="text-lg font-weight-bold info-box-number">{{ $withdrawal->count() }}</span>
                    </div>
                  </div>
                </div>
              
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="mb-3 info-box">
                        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-exclamation-triangle fa-lg text-white"></i></span>
                
                        <div class="info-box-content retro">
                            <span class="font-weight-bold text-dark info-box-text">Total</span>
                            <span class="text-lg info-box-number">{{ $withdrawal_pending->count() }}</span>
                        </div>
                    </div>
                </div>
                
              
                <div class="clearfix hidden-md-up"></div>
              
                <div class="col-12 col-sm-6 col-md-3">
                  <div class="mb-3 info-box">
                    <span class="info-box-icon bg-success elevation-1"><i class="fas fa-percent fa-lg"></i></span>
                    <div class="info-box-content">
                      <span class="font-weight-bold info-box-text">Total</span>
                      <span class="text-lg font-weight-bold info-box-number">{{ $fees->count() }}</span>
                    </div>
                  </div>
                </div>
              
                <div class="col-12 col-sm-6 col-md-3">
                  <div class="mb-3 info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-shopping-cart fa-lg"></i></span>
                    <div class="info-box-content">
                      <span class="font-weight-bold info-box-text">Total</span>
                      <span class="text-lg font-weight-bold info-box-number">{{ $purchases->count() }}</span>
                    </div>
                  </div>
                </div>
              </div>
              
            

            <!-- Exportable Table -->
            <div class="clearfix row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="box-header mt-1">
                            <div class="flex justify-center w-full">
                                <button
                                    class="bg-blue-600 hover:bg-blue-800 text-white font-bold py-2 px-4 border border-blue-800 rounded-md shadow-md mx-1 active:bg-blue-900 active:shadow-none w-full"
                                    data-id="1" onclick="populateTable(this)">
                                    <b>Retiros</b>
                                </button>
                                <button
                                    class="bg-yellow-600 hover:bg-yellow-800 text-white font-bold py-2 px-4 border border-yellow-800 rounded-md shadow-md mx-1 active:bg-yellow-900 active:shadow-none w-full"
                                    data-id="2" onclick="populateTable(this)">
                                    <b>Retiros Pendientes</b>
                                </button>
                                <button
                                    class="bg-green-600 hover:bg-green-800 text-white font-bold py-2 px-4 border border-green-800 rounded-md shadow-md mx-1 active:bg-green-900 active:shadow-none w-full"
                                    data-id="3" onclick="populateTable(this)">
                                    <b>Tarifa</b>
                                </button>
                                <button
                                    class="bg-red-600 hover:bg-red-800 text-white font-bold py-2 px-4 border border-red-800 rounded-md shadow-md mx-1 active:bg-red-900 active:shadow-none w-full"
                                    data-id="4" onclick="populateTable(this)">
                                    <b>Compras</b>
                                </button>
                            </div>
                        </div>
                        <br>
                        <div class="header">
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table
                                    class="table table-bordered table-striped br-datatable-withdrawal table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th>Total</th>
                                            <th>Correo</th>
                                            <th>Nombre y Apellido</th>
                                            <th>Usuario-Unico</th>
                                            <th>Transaccion</th>
                                            <th>Fecha Creacion</th>
                                            <th>Fecha Actualizacion</th>
                                            <th style="width:5%">Accion</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Status</th>
                                            <th>Total</th>
                                            <th>Correo</th>
                                            <th>Nombre y Apellido</th>
                                            <th>Usuario-Unico</th>
                                            <th>Transaccion</th>
                                            <th>Fecha Creacion</th>
                                            <th>Fecha Actualizacion</th>
                                            <th style="width:5%">Accion</th>
                                        </tr>
                                    </tfoot>
                                    <tbody class="fillDataTable">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <form action="{{ route('confirm_withdrawal') }}" method="post">
        {{ csrf_field() }}
        <div class="modal fade" tabindex="-1" role="dialog" id="mystatus">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">{{ __('Confrimar envío de dinero') }}</h4>
                    </div>

                    <div class="modal-body">
                        <br>
                        <div class="form-group" style="padding: 10px;">
                            <label
                                for="credit">{{ __('Recuerde que al confirmar, acepta haber envía el dinero al cliente, esta opción es irreversible.') }}</label>
                            <div id="foundRemove"></div>
                        </div>
                        <div class="form-group" style="padding: 10px;">
                            <label for="pass">{{ __('Confirmar contraseña') }}</label>
                            <input type="password" name="pass" class="form-control">
                        </div>
                        <input id="orderDetails" name="orderDetails" type="hidden">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ __('Cancelar') }}</button>
                        <button type="submit" class="btn btn-warning">{{ __('Guardar') }}</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </form>

@stop


@section('js')

    <script>
        function populateTable(x) {
            var table = $('.br-datatable-withdrawal').DataTable();
            if ($(x).data("id") == 1) {
                console.log(1);
                $.get("{{ url('/11w5Cj9WDAjnzlg0/showwiths') }}", function(data, status) {
                    var i = 0;
                    $.each(data, function(key, value) {
                        var rows = table.rows().remove().draw();
                        for (x in value) {
                            var rowNode = table.row.add([
                                (value[x].d9 == 'N' ? "<td> <small class='badge badge-danger'> " +
                                    value[x].d0 + "</span> </td>" : (value[x].d0 ==
                                        'Credito enviado' ?
                                        "<td> <span class='label label-success'> " + value[x].d0 +
                                        "</span> </td>" : "<td> <span class='label label-info'> " +
                                        value[x].d0 + "</span> </td>")),
                                "<td>" + value[x].d4 + "</td>",
                                "<td>" + value[x].d5 + "</td>",
                                "<td>" + value[x].d6 + "</td>",
                                "<td>" + value[x].d2 + "</td>",
                                "<td>" + value[x].d1 + "</td>",
                                "<td>" + value[x].d3 + "</td>",
                                "<td>" + value[x].d31 + "</td>",
                                (value[x].d9 == 'N' ?
                                    "<td> <a href='#' data-d1='" + value[x].d1 + "' data-d6='" +
                                    value[x].d6 + "' data-d2='" + value[x].d2 + "' data-d7='" +
                                    value[x].d7 + "' data-d8='" + value[x].d8 + "' data-d9='" +
                                    value[x].d9 +
                                    "' class='btn btn-xs btn-primary' onclick='status(this)'> Status </a> </td>" :
                                    (value[x].d0 == 'Credito enviado' ?
                                        "<td> <a href='#' data-d1='" + value[x].d1 + "' data-d6='" +
                                        value[x].d6 + "' data-d2='" + value[x].d2 + "' data-d7='" +
                                        value[x].d7 + "' data-d8='" + value[x].d8 + "' data-d9='" +
                                        value[x].d9 +
                                        "' class='btn btn-xs btn-danger' onclick='status(this)'> Status </a> </td>" :
                                        "")
                                ),
                            ]).draw().node();
                        }
                    });
                });
            }

            if ($(x).data("id") == 2) {
                console.log(2);
                $.get("{{ url('/11w5Cj9WDAjnzlg0/showwithsped') }}", function(data, status) {
                    var i = 0;
                    $.each(data, function(key, value) {
                        var rows = table.rows().remove().draw();
                        for (x in value) {
                            var rowNode = table.row.add([
                                (value[x].d9 == 'N' ? "<td> <span class='label label-warning'> " +
                                    value[x].d0 + "</span> </td>" : (value[x].d0 ==
                                        'Credito enviado' ?
                                        "<td> <span class='label label-success'> " + value[x].d0 +
                                        "</span> </td>" : "<td> <span class='label label-info'> " +
                                        value[x].d0 + "</span> </td>")),
                                "<td>" + value[x].d4 + "</td>",
                                "<td>" + value[x].d5 + "</td>",
                                "<td>" + value[x].d6 + "</td>",
                                "<td>" + value[x].d2 + "</td>",
                                "<td>" + value[x].d1 + "</td>",
                                "<td>" + value[x].d3 + "</td>",
                                "<td>" + value[x].d31 + "</td>",
                                (value[x].d9 == 'N' ?
                                    "<td> <a href='#' data-d1='" + value[x].d1 + "' data-d6='" +
                                    value[x].d6 + "' data-d2='" + value[x].d2 + "' data-d7='" +
                                    value[x].d7 + "' data-d8='" + value[x].d8 + "' data-d9='" +
                                    value[x].d9 +
                                    "' class='btn btn-xs btn-primary' onclick='status(this)'> Status </a> </td>" :
                                    (value[x].d0 == 'Credito enviado' ?
                                        "<td> <a href='#' data-d1='" + value[x].d1 + "' data-d6='" +
                                        value[x].d6 + "' data-d2='" + value[x].d2 + "' data-d7='" +
                                        value[x].d7 + "' data-d8='" + value[x].d8 + "' data-d9='" +
                                        value[x].d9 +
                                        "' class='btn btn-xs btn-danger' onclick='status(this)'> Status </a> </td>" :
                                        "")
                                ),
                            ]).draw().node();
                        }
                    });
                });
            }
            if ($(x).data("id") == 3) {
                console.log(3);
                $.get("{{ url('/11w5Cj9WDAjnzlg0/showfees') }}", function(data, status) {
                    var i = 0;
                    $.each(data, function(key, value) {
                        var rows = table.rows().remove().draw();
                        for (x in value) {
                            var rowNode = table.row.add([
                                (value[x].d9 == 'N' ? "<td> <span class='label label-warning'> " +
                                    value[x].d0 + "</span> </td>" :
                                    "<td> <span class='label label-success'> " + value[x].d0 +
                                    "</span> </td>"),
                                "<td>" + value[x].d4 + "</td>",
                                "<td>" + value[x].d5 + "</td>",
                                "<td>" + value[x].d6 + "</td>",
                                "<td>" + value[x].d2 + "</td>",
                                "<td>" + value[x].d1 + "</td>",
                                "<td>" + value[x].d3 + "</td>",
                                "<td>" + value[x].d31 + "</td>",
                                (value[x].d9 == 'N' ? "<td> <a href='#' data-d1='" + value[x].d1 +
                                    "' data-d6='" + value[x].d6 + "' data-d2='" + value[x].d2 +
                                    "' data-d7='" + value[x].d7 + "' data-d8='" + value[x].d8 +
                                    "' data-d9='" + value[x].d9 +
                                    "' class='btn btn-xs btn-primary' onclick='status(this)'> Status </a> </td>" :
                                    " "),
                            ]).draw().node();
                        }
                    });
                });
            }
            if ($(x).data("id") == 4) {
                console.log(4);
                $.get("{{ url('/11w5Cj9WDAjnzlg0/showpur') }}", function(data, status) {
                    var i = 0;
                    $.each(data, function(key, value) {
                        var rows = table.rows().remove().draw();
                        for (x in value) {
                            var rowNode = table.row.add([
                                (value[x].d9 == 'N' ? "<td> <span class='label label-warning'> " +
                                    value[x].d0 + "</span> </td>" :
                                    "<td> <span class='label label-success'> " + value[x].d0 +
                                    "</span> </td>"),
                                "<td>" + value[x].d4 + "</td>",
                                "<td>" + value[x].d5 + "</td>",
                                "<td>" + value[x].d6 + "</td>",
                                "<td>" + value[x].d2 + "</td>",
                                "<td>" + value[x].d1 + "</td>",
                                "<td>" + value[x].d3 + "</td>",
                                "<td>" + value[x].d31 + "</td>",
                                (value[x].d9 == 'N' ? "<td> <a href='#' data-d1='" + value[x].d1 +
                                    "' data-d6='" + value[x].d6 + "' data-d2='" + value[x].d2 +
                                    "' data-d7='" + value[x].d7 + "' data-d8='" + value[x].d8 +
                                    "' data-d9='" + value[x].d9 +
                                    "' class='btn btn-xs btn-primary' onclick='status(this)'> Status </a> </td>" :
                                    " "),
                            ]).draw().node();
                        }
                    });
                });
            }

        }
        $(document).ready(function() {
            var table = $('.br-datatable-withdrawal').DataTable();
            $.get("{{ url('/11w5Cj9WDAjnzlg0/showwiths') }}", function(data, status) {
                var i = 0;
                $.each(data, function(key, value) {
                    //$(".fillDataTable").empty();
                    var rows = table.rows().remove().draw();
                    for (x in value) {
                        var rowNode = table.row.add([
                            (value[x].d9 == 'N' ?
                                "<td> <span class='label label-warning'> " + value[x].d0 +
                                "</span> </td>" : (value[x].d0 == 'Credito enviado' ?
                                    "<td> <span class='label label-success'> " + value[x]
                                    .d0 + "</span> </td>" :
                                    "<td> <span class='label label-info'> " + value[x].d0 +
                                    "</span> </td>")),
                            "<td>" + value[x].d4 + "</td>",
                            "<td>" + value[x].d5 + "</td>",
                            "<td>" + value[x].d6 + "</td>",
                            "<td>" + value[x].d2 + "</td>",
                            "<td>" + value[x].d1 + "</td>",
                            "<td>" + value[x].d3 + "</td>",
                            "<td>" + value[x].d31 + "</td>",
                            (value[x].d9 == 'N' ?
                                "<td> <a href='#' data-d1='" + value[x].d1 + "' data-d6='" +
                                value[x].d6 + "' data-d2='" + value[x].d2 + "' data-d7='" +
                                value[x].d7 + "' data-d8='" + value[x].d8 + "' data-d9='" +
                                value[x].d9 +
                                "' class='btn btn-xs btn-primary' onclick='status(this)'> Status </a> </td>" :
                                (value[x].d0 == 'Credito enviado' ?
                                    "<td> <a href='#' data-d1='" + value[x].d1 +
                                    "' data-d6='" + value[x].d6 + "' data-d2='" + value[x]
                                    .d2 + "' data-d7='" + value[x].d7 + "' data-d8='" +
                                    value[x].d8 + "' data-d9='" + value[x].d9 +
                                    "' class='btn btn-xs btn-danger' onclick='status(this)'> Status </a> </td>" :
                                    "")
                            )
                        ]).draw().node();
                    }
                });
            });

        });

        function status(x) {
            if ($(x).data("d9") == 'N') {
                $('#orderDetails').val($(x).data("d1"));
                $('#foundRemove').html(
                    "<b> Beneficiario: </b>" + $(x).data("d6") + "<br>" +
                    "<b> Usuario Unico Bancario: </b>" + $(x).data("d2") + "<br>" +
                    "<b> IBAN: </b>" + $(x).data("d7") + "<br>" +
                    "<b> BIC: </b>" + $(x).data("d8") + "<br>"
                );
                $('#mystatus').modal('show');
            }
            if ($(x).data("d9") == 'Y') {
                $('#orderDetails').val($(x).data("d1"));
                $('#foundRemove').html(
                    "<b> Beneficiario: </b>" + $(x).data("d6") + "<br>" +
                    "<b> Usuario Unico Bancario: </b>" + $(x).data("d2") + "<br>" +
                    "<b> IBAN: </b>" + $(x).data("d7") + "<br>" +
                    "<b> BIC: </b>" + $(x).data("d8") + "<br>" +
                    "<b> Cuidado, esta apunto de cambiar el estado de la transacción </b>"
                );
                $('#mystatus').modal('show');
            }

        }
    </script>
@stop
