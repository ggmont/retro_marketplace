@extends('adminlte::page')

@section('title', 'Admin-Dashboard')

@section('content_header')
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nes/nes_prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/goodgames.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/selectpicker/dist/css/bootstrap-select.min.css') }}">
    <style>
        .avatar {
            height: 50px;
            width: 50px;
        }

        .list-group-item:hover,
        .list-group-item:focus {
            background: rgba(24, 32, 23, 0.37);
            cursor: pointer;
        }

        .chatbox {
            height: 80vh !important;
            overflow-y: scroll;
        }

        .message-box {
            height: 70vh !important;
            overflow-y: scroll;
            display: flex;
            flex-direction: column-reverse;
        }

        .single-message {
            background: #f1f0f0;
            border-radius: 12px;
            padding: 10px;
            margin-bottom: 10px;
            width: fit-content;
        }

        .received {
            margin-right: auto !important;
        }

        .sent {
            margin-left: auto !important;
            background: #3490dc;
            color: white !important;
        }

        .sent small {
            color: white !important;
        }

        .link:hover {
            list-style: none !important;
            text-decoration: none;
        }

        .online-icon {
            font-size: 11px !important;
        }

        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Press Start 2P', cursive;
        }

    </style>
    @livewireStyles
@stop

@section('content')

    <section class="content">
        <div class="container-fluid">


            @livewire('messages')


        </div>
    </section>

@stop


@section('js')
    @livewireScripts
@stop
