@extends('adminlte::page')

@section('plugins.JqueryDatatable', true)

@section('title', 'Admin-Dashboard - Visitas')

@section('content_header')
    <meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0, user-scalable=no" />
    <center>
        <h1>- Visitas -</h1>
    </center>
@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/fontawesome.min.css"
        integrity="sha512-cHxvm20nkjOUySu7jdwiUxgGy11vuVPE9YeK89geLMLMMEOcKFyS2i+8wo0FOwyQO/bL8Bvq1KMsqK4bbOsPnA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/style.css?v=2') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/basictable.min.css') }}" />
    @livewireStyles
@stop

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-4">
                    <div class="info-box">
                        <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-trophy"></i></span>
            
                        <div class="info-box-content retro">
                            <span class="info-box-text">Total de visitas por día</span>
                            <span class="info-box-number">{{ $totalVisitsDay }}</span>
                        </div>
                    </div>
                </div>
            
                <div class="col-12 col-sm-4">
                    <div class="info-box">
                        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-trophy"></i></span>
            
                        <div class="info-box-content retro">
                            <span class="info-box-text">Total de visitas por mes</span>
                            <span class="info-box-number">{{ $totalVisitsMonth }}</span>
                        </div>
                    </div>
                </div>
            
                <div class="col-12 col-sm-4">
                    <div class="info-box">
                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-trophy"></i></span>
            
                        <div class="info-box-content retro">
                            <span class="info-box-text">Total de visitas por año</span>
                            <span class="info-box-number">{{ $totalVisitsYear }}</span>
                        </div>
                    </div>
                </div>
            </div>
            

            <!-- Exportable Table -->
            <livewire:admin.visitor></livewire:admin.visitor>
        </div>
    </section>

    <footer class="relative bg-white border-red-700 footer">
        <div class="container px-6 mx-auto">
            <div class="mt-16 border-t-2 border-gray-300">
                <strong>Copyright &copy; 2021 <a href="https://retrogamingmarket.eu">Retrogamingmarket.eu</a>.</strong>
                Todos los derechos reservados.
                <div class="float-right d-none d-sm-inline-block">
                    <b>Version Beta</b> 2.0
                </div>
            </div>
        </div>
    </footer>

@stop


@section('js')

@stop
