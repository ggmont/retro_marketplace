@extends('adminlte::page')

@section('title', 'Admin-Dashboard')

@section('content_header')
    <!-- PWA -->
    @laravelPWA
@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/fontawesome.min.css"
        integrity="sha512-cHxvm20nkjOUySu7jdwiUxgGy11vuVPE9YeK89geLMLMMEOcKFyS2i+8wo0FOwyQO/bL8Bvq1KMsqK4bbOsPnA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
    <style>
        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }
    </style>


    @livewireStyles
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>

                        <div class="info-box-content">
                            <span class="font-weight-bold info-box-text">Usuarios en línea</span>
                            <span class="text-lg font-weight-bold info-box-number">
                                {{ $viewData['user_online'] }}
                                / {{ $viewData['user_all'] }}
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-warning elevation-1"><i
                                class="fas fa-user-plus  text-white"></i></span>

                        <div class="info-box-content">
                            <span class="font-weight-bold info-box-text">Registrados por mes</span>
                            <span class="text-lg font-weight-bold info-box-number">
                                {{ $viewData['user_register'] }}
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-orange elevation-1"><i
                                class="fas fa-shopping-cart  text-white"></i></span>

                        <div class="info-box-content">
                            <span class="font-weight-bold info-box-text">Pedidos Pendientes</span>
                            <span class="text-lg font-weight-bold info-box-number">
                                {{ $viewData['order_user'] }}
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-red elevation-1"><i class="fas fa-cart-arrow-down"></i></span>

                        <div class="info-box-content">
                            <span class="font-weight-bold info-box-text">Pedidos Completos</span>
                            <span class="text-lg font-weight-bold info-box-number">
                                {{ $viewData['order_user_complete'] }}
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-exclamation"></i></span>

                        <div class="info-box-content">
                            <span class="font-weight-bold info-box-text">Denuncias del inventario</span>
                            <span class="text-lg font-weight-bold info-box-number">
                                {{ $viewData['sys_complaint'] }}

                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-purple elevation-1"><i
                                class="fas fa-user-plus  text-white"></i></span>

                        <div class="info-box-content">
                            <span class="font-weight-bold info-box-text">Registrados por día</span>
                            <span class="text-lg font-weight-bold info-box-number">
                                {{ $viewData['user_daily'] }}
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>


                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-warning elevation-1"><i
                                class="fas fa-exclamation text-white"></i></span>

                        <div class="info-box-content">
                            <span class="font-weight-bold info-box-text">Denuncias de Producto</span>
                            <span class="text-lg font-weight-bold info-box-number">
                                {{ $viewData['sys_complaint_user'] }}
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-orange elevation-1"><i
                                class="fas fa-exclamation text-white"></i></span>

                        <div class="info-box-content">
                            <span class="font-weight-bold info-box-text">Denuncias del usuario</span>
                            <span class="text-lg font-weight-bold info-box-number">
                                {{ $viewData['sys_complaint_inventory'] }}
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

            </div>


            <div class="row">


                <div class="col-12 col-sm-6 col-md-6">
                    <div class="small-box bg-green">
                        <div class="inner font-bold">
                            <h1> {{ $viewData['product_request'] }}</h1>

                            <p class="font-weight-bold info-box-text">Solicitudes de producto</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-solid fa-store"></i>
                        </div>
                        <a href="{{ route('product_request_index') }}" class="small-box-footer">Mas Información <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                    <!-- /.info-box -->
                </div>

                <!-- ./col -->
                <!-- ./col -->
                <div class="col-12 col-sm-6 col-md-6">
                    <!-- small box -->
                    <div class="small-box bg-blue">
                        <div class="inner font-bold">
                            <h1> {{ $viewData['product_user'] }}</h1>

                            <p class="font-weight-bold info-box-text">Productos usuario / cantidad</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-fw fa-chart-pie"></i>
                        </div>
                        <a href="{{ route('inventory_admin') }}" class="small-box-footer">Mas Información <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <!-- ./col -->



            </div>

            <div class="row">


                <div class="col-12 col-sm-12 col-md-12">
                    <div class="small-box bg-red">
                        <div class="inner font-bold">
                            <h1> {{ $viewData['purchase_all'] }}</h1>
                            <p class="font-weight-bold info-box-text">Pedidos totales</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-solid fa-euro-sign"></i>
                        </div>
                        <a href="{{ route('product_request_index') }}" class="small-box-footer">Mas Información <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                    <!-- /.info-box -->
                </div>

                <!-- ./col -->



            </div>

            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <section class="col-lg-12">
                    <div class="relative">
                        <canvas id="myChart" width="400" height="400"></canvas>
                    </div>
                </section><!-- /.Left col -->

            </div><!-- /.row (main row) -->
        </div>


    </section>
@stop

@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>




    <script src="{{ asset('assets/noty/packaged/jquery.noty.packaged.min.js') }}"></script>

    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.4.1/chart.min.js"
        integrity="sha512-5vwN8yor2fFT9pgPS9p9R7AszYaNn0LkQElTXIsZFCL7ucT8zDCAqlQXDdaqgA1mZP47hdvztBMsIoFxq/FyyQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset('assets/moment/moment.local.min.js') }}"></script>
    <script>
        var ctx = document.getElementById('myChart');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {

                labels: [
                    @foreach ($viewData['resumen'] as $key)
                        [moment().locale('{{ App::getLocale() }}').month({{ $key->month - 1 }}).format("MMMM")
                            .toUpperCase() + " {{ $key->years }}"
                        ],
                    @endforeach
                ],

                datasets: [{
                    label: 'Beneficios por mes',

                    data: [
                        @foreach ($viewData['resumen'] as $key)
                            {{ $key->sum }},
                        @endforeach
                    ],

                    backgroundColor: [
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                interaction: {
                    mode: 'index',
                    intersect: false,
                },
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
@stop
