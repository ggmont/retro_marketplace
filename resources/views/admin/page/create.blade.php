@extends('adminlte::page')

@section('title', 'Admin-Dashboard')
@section('plugins.Select2', true)

@section('content_header')

@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
    <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/style.css?v=2') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/basictable.min.css') }}" />
    @livewireStyles
@stop

@section('content')

    <section class="content">

        <div class="row">

            <div class="col-md-12" style="background-color: #ffffff; border: solid 1px #db2e2e">

                <form action="{{ route('save_page') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @foreach ($viewData['model_cols'] as $key => $col)
                        <?php
                        
                        $label = '';
                        $lang = isset($viewData['lang']) ? $viewData['lang'] : 'app';
                        
                        if (isset($col['label'])) {
                            //$label = $lang.'.col_'.$col['label'];
                            $label = $col['label'];
                        } elseif (substr($key, 0, 4) == 'rel_') {
                            $label = $lang . '.col_' . str_replace('rel_', '', $key);
                        } else {
                            $label = $lang . '.col_' . $key;
                        }
                        ?>
                        @if ($viewData['column_prefix'] . $key == 'br_c_language')
                            <div class="form-group">
                                <label for="{{ $viewData['column_prefix'] . $key }}">{{ Lang::get($label) }}


                                    @php($selectedValue = [])
                                    @if (isset($viewData['plat_dic']))
                                        @foreach ($viewData['plat_dic'] as $key3)
                                            @php(array_push($selectedValue, $key3))
                                        @endforeach
                                    @endif

                                </label>
                                <select @if ($viewData['column_prefix'] . $key == 'br_c_language') multiple @endif
                                    class="form-control {{ isset($col['class']) ? $col['class'] : '' }}"
                                    id="{{ $viewData['column_prefix'] . $key }}"
                                    name="{{ $viewData['column_prefix'] . $key }}@if ($viewData['column_prefix'] . $key == 'br_c_language') [] @endif"
                                    {{ isset($col['attr']) ? $col['attr'] : '' }}
                                    {{ isset($col['dependency']) ? 'dependency=' . $viewData['column_prefix'] . $col['dependency'] : '' }}
                                    style="width: 100%;">
                                    <option value="">...</option>
                                    @if (isset($col['select_values']))
                                        @foreach ($col['select_values'] as $select_row)
                                            <option value="{{ $select_row['id'] }}"
                                                {{ in_array($select_row['id'], $selectedValue) ? 'selected="selected"' : '' }}
                                                {{ isset($select_row['dependency']) ? 'dependency=' . $select_row['dependency'] : '' }}
                                                {!! isset($select_row['attr']) ? $select_row['attr'] : '' !!}>{{ $select_row['value'] }}</option>
                                        @endforeach
                                    @endif
                                </select>

                                <span
                                    class="help-block">{{ str_replace(str_replace('_', ' ', $viewData['column_prefix']), '', $errors->first($viewData['column_prefix'] . $key)) }}</span>
                            </div>
                        @endif
                    @endforeach


                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- magaya connection -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    {{ isset($viewData['form_sec_title']) ? $viewData['form_sec_title'] : '' }}</h3>
                            </div><!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">

                                <div class="flex flex-wrap -mx-3 mb-6">
                                    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                                        <label for="title"
                                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Título
                                            <font color="red">*</font>
                                        </label>

                                        <input type="text" id="br_c_title" name="br_c_title"
                                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="RGM" required>

                                    </div>
                                    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                                        <label for="url"
                                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">URL
                                            <font color="red">*</font>
                                        </label>

                                        <input type="text" id="br_c_url" name="br_c_url"
                                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            data-url-from="br_c_title" readonly>

                                    </div>
                                </div>

                                <div class="flex flex-wrap -mx-3 mb-6">
                                    <div class="w-full px-3">
                                        <label for="language"
                                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Idiomas
                                            <font color="red">*</font>
                                        </label>
                                        <select name="br_c_lang" id="br_c_lang" class="form-control show-tick ms select2">
                                            <option value="Seleccione">Seleccione</option>
                                            <option value="es" selected>Español</option>
                                            <option value="en">Ingles</option>
                                            <option value="fr">Francés</option>
                                            <option value="ita">Italia</option>
                                            <option value="al">Aleman</option>
                                            <option value="prt">Portugues</option>
                                        </select>
                                    </div>
                                </div>

                                @foreach ($viewData['model_cols'] as $key => $col)
                                    <?php
                                    
                                    $label = '';
                                    $lang = isset($viewData['lang']) ? $viewData['lang'] : 'app';
                                    
                                    if (isset($col['label'])) {
                                        //$label = $lang.'.col_'.$col['label'];
                                        $label = $col['label'];
                                    } elseif (substr($key, 0, 4) == 'rel_') {
                                        $label = $lang . '.col_' . str_replace('rel_', '', $key);
                                    } else {
                                        $label = $lang . '.col_' . $key;
                                    }
                                    ?>


                                    @if ($col['type'] == 'html-editor')
                                        <div class="form-group br-html-editor-container">
                                            <label for="url"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Contenido
                                                <font color="red">*</font>
                                            </label>
                                            <textarea class="form-control {{ isset($col['class']) ? $col['class'] : '' }}" id="file-picker"
                                                name="{{ $viewData['column_prefix'] . $key }}" {!! isset($col['attr']) ? $col['attr'] : '' !!}>
                                        {{ strlen(old($viewData['column_prefix'] . $key)) > 0 ? old($viewData['column_prefix'] . $key) : (isset($viewData['model'][$key]) ? $viewData['model'][$key] : '') }}
                                      </textarea>
                                            <span
                                                class="help-block">{{ str_replace(str_replace('_', ' ', $viewData['column_prefix']), '', $errors->first($viewData['column_prefix'] . $key)) }}</span>
                                        </div>
                                    @endif
                                @endforeach
                                <div class="col-sm-12">
                                    <div class="mb-3">
                                        <label for="countries"
                                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Habilitar
                                            para todo
                                            público
                                            <font color="red">*</font>
                                        </label>
                                        <select name="br_c_is_enabled" id="br_c_is_enabled"
                                            class="form-control show-tick ms select2">
                                            <option value="Seleccione">Seleccione</option>
                                            <option value="Y">Si</option>
                                            <option value="N">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="mb-3">
                                        <label for="countries"
                                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Tipo
                                            <font color="red">*</font>
                                        </label>
                                        <select name="br_c_type" id="br_c_type"
                                            class="form-control show-tick ms select2">
                                            <option value="Seleccione">Seleccione</option>
                                            <option value="guia">Guia</option>
                                            <option value="informacion">Informacion</option>
                                        </select>
                                    </div>
                                </div>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.col-lg-6 -->



                    <div class="text-center form-group col-md-12">
                        <input type="submit" class="btn confirmclosed btn-danger font-bold w-100" value="Agregar">
                        <input type="hidden" name="model_id"
                            value="{{ $viewData['model_id'] > 0 ? $viewData['model_id'] : '' }}" />
                    </div>
                </form>

            </div>
        </div>
    </section>


@stop


@section('js')

    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('assets/tinymce/tinymce.min.js') }}"></script>
    <script>
        function convertToSlug(Text) {
            return Text
                .toLowerCase()
                .replace(/ /g, '-')
                .replace(/[^\w-]+/g, '');
        }

        $('[data-url-from]').each(function() {
            var from = $('#' + $(this).data('url-from'));
            var o = $(this);
            from.keyup(function() {
                o.val(convertToSlug(this.value));
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#br-btn-delete').click(function() {
                $(this).button('loading');

                var buttons = [{
                        addClass: 'btn btn-primary btn-sm pull-left',
                        text: '{{ Lang::get('app.delete') }}',
                        onClick: function($noty) {
                            $noty.close();
                            deleteRecord()
                        }
                    },
                    {
                        addClass: 'btn btn-danger btn-sm pull-right',
                        text: '{{ Lang::get('app.cancel') }}',
                        onClick: function($noty) {
                            $noty.close();
                            $('#br-btn-delete').button('reset');
                        }
                    },
                ];

                presentNotyMessage('{{ Lang::get('app.delete_are_you_sure') }}', 'warning', buttons, 1);
            });

            function select2Template(state) {
                if (!state.id) {
                    return state.text;
                }

                var children = $(state.element).data('children') || 0;

                var $state = $(
                    '<span ' + (children > 0 ? 'style="font-weight: bold;"' : '') + '>' + state.text + '</span>'
                );
                return $state;
            }

            $('select.br-select2').select2({
                templateResult: select2Template
            });

            var htmlEditors = [];
            if ($(".br-html-editor").length > 0) {


                tinymce.init({
                    selector: 'textarea',
                    language: "es_419",
                    image_class_list: [{
                        title: 'img-responsive',
                        value: 'img-responsive'
                    }, ],
                    height: 500,
                    setup: function(editor) {
                        editor.on('init change', function() {
                            editor.save();
                        });
                    },
                    plugins: [
                        "advlist autolink lists link image charmap print preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media table contextmenu paste imagetools"
                    ],
                    toolbar: "insertfile undo redo | fontselect fontsizeselect formatselect  styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ",

                    image_title: true,
                    automatic_uploads: true,
                    images_upload_url: '/11w5Cj9WDAjnzlg0/pages/upload',
                    file_picker_types: 'image',
                    file_picker_callback: function(cb, value, meta) {
                        var input = document.createElement('input');
                        input.setAttribute('type', 'file');
                        input.setAttribute('accept', 'image/*');
                        input.onchange = function() {
                            var file = this.files[0];

                            var reader = new FileReader();
                            reader.readAsDataURL(file);
                            reader.onload = function() {
                                var id = 'blobid' + (new Date()).getTime();
                                var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                                var base64 = reader.result.split(',')[1];
                                var blobInfo = blobCache.create(id, file, base64);
                                blobCache.add(blobInfo);
                                cb(blobInfo.blobUri(), {
                                    title: file.name
                                });
                            };
                        };
                        input.click();
                    }
                });

            }

            // $('textarea.br-html-editor').each(function(i,e) {

            //   $(this).val($(this).val().trim());
            //   var id = $(this).attr('id');
            //   var editor = new nicEditor({fullPanel : true}).panelInstance(id);
            //   $(this).attr('data-editor-index',i);
            //   htmlEditors.push(editor);

            //   // var toggle = $(this).closest('.br-html-editor-container').find('.br-editor-toggle:eq(0)');
            //   // var full = $(this).closest('.br-html-editor-container').find('.br-editor-full:eq(0)');

            //   toggle.click(function() {
            //     if( ! htmlEditors[i] ) {
            //       htmlEditors[i] = new nicEditor({fullPanel : true}).panelInstance(id);
            //     }
            //     else {
            //       htmlEditors[i].removeInstance(id);
            //       htmlEditors[i] = null;
            //     }
            //   });
            // });

            $('.image-uploader:eq(0)').fileupload({
                dataType: 'json',
                add: function(e, data) {
                    if (file = data.files[0]) {
                        img = new Image();
                        img.onload = function() {
                            $(e.target).prev().text('Uploading');
                            data.submit();
                        };
                        img.src = URL.createObjectURL(file);
                    }
                },
                done: function(e, data) {
                    var target = $(e.target);
                    var img = $('img[data-file-input="' + target.attr('id') + '"]');
                    var noimg = $('div[data-file-input="' + target.attr('id') + '"]');
                    img.attr('src', img.data('path') + '/' + data.result.name + '?' + new Date()
                        .getTime());
                    img.show();
                    if (noimg.length > 0) noimg.hide();
                    $(e.target).prev().text('Select photo');
                    $(e.target).next().val(data.result.name);
                }
            });

            $('[data-function="remove"]').click(function() {
                var input = $('input[name="' + $(this).data('file-target') + '"]');
                var img = $('img[data-file-input="' + $(this).data('file-container') + '"]');
                var div = $('div[data-file-input="' + $(this).data('file-container') + '"]');

                input.val('');
                img.removeAttr('src');
                img.hide();
                div.show();
            });

            function deleteRecord() {
                var url = '{{ $viewData['form_url_post_delete'] }}';
                var form = $('<form action="' + url + '" method="post">' +
                    '<input type="hidden" name="_token" value="{{ csrf_token() }}">' +
                    '<input type="hidden" name="model_id" value="{{ $viewData['model_id'] > 0 ? $viewData['model_id'] : '' }}" />' +
                    '</form>');
                $('body').append(form);
                form.submit();
            }

        });
    </script>

    @livewireScripts
@stop
