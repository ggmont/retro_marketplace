@extends('adminlte::page')

@section('title', 'Admin-Dashboard')
@section('plugins.FileInput', true)
@section('plugins.Select2', true)

@section('content_header')
    <h1 class="font-bold text-2xl my-8 text-center">- Editar Banner -</h1>
@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
    <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/style.css?v=2') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/basictable.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/fileinput/css/fileinput.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    @livewireStyles
@stop

@section('content')

    <section class="content">
        <div class="row">
            <div class="col-md-12" style="background-color: #ffffff; border: solid 1px #db2e2e">
                <br>

                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro" for="grid-city">
                    @if ($banner->location == 'BNR_MAIN_1')
                        <center>Imagen Banner 1 (1280 x 165)</center>
                    @elseif($banner->location == 'BNR_MAIN_2')
                        <center>Imagen Banner 2 (1280 x 165)</center>
                    @else
                        <center>Imagen Banner 3 (912 x 1280)</center>
                    @endif
                </h5>
                @if ($banner->location == 'BNR_MAIN_1')
                    <form action="{{ route('banner_update', ['id' => $banner->id]) }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf

                        <label for="link" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Link
                            <font color="red">*</font>
                        </label>
                        <input type="text" value="{{ $banner->link }}" name="link"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="https://www.retrogamingmarket.eu"required>
                    @elseif($banner->location == 'BNR_MAIN_2')
                        <form action="{{ route('banner_update', ['id' => $banner->id]) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            <label for="link" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Link
                                <font color="red">*</font>
                            </label>
                            <input type="text" value="{{ $banner->link }}" name="link"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="https://www.retrogamingmarket.eu"required>
                        @elseif($banner->location == 'IMAGE_MAIN_MAGAZINE')
                            <form action="{{ route('banner_update_magazine', ['id' => $banner->id]) }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <label for="link"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Link
                                    <font color="red">*</font>
                                </label>
                                <input type="text" value="{{ $banner->link }}" name="link"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="https://www.retrogamingmarket.eu"required>
                            @else
                                <form action="{{ route('banner_update_category', ['id' => $banner->id]) }}" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf

                                    <label for="category"
                                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Categoria
                                        <font color="red">*</font>
                                    </label>

                                    <div class="relative">
                                        <select
                                            class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                            id="category" name="category">
                                            <option value="{{ $banner->category }}">{{ $banner->category }}
                                            </option>
                                            <option value="faqs">faqs</option>
                                            @foreach ($pagina as $p)
                                                <option value="{{ $p->url }}">{{ $p->url }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <br>
                @endif

                <br>

                <label for="enabled" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Habilitar
                    <font color="red">*</font>
                </label>

                <div class="relative">
                    <select
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        id="is_enabled" name="is_enabled">
                        <option value="{{ $banner->is_enabled }}">{{ $banner->is_enabled }}
                        </option>
                        <option value="Y">Si
                        </option>
                        <option value="N">No
                        </option>
                    </select>
                </div>

                <br>
                <div class="flex flex-wrap mb-6 -mx-3">

                    <div class="w-full">
                        <div class="file-loading">
                            <input id="input-20" name="image_path" type="file" data-browse-on-zone-click="true">
                        </div>
                    </div>
                </div>


                <input type="submit" class="btn confirmclosed btn-danger font-bold w-100 close-modal" value="Actualizar">
                <br><br>
            </div>





            </form>





            <br>

        </div>
        </div>
    </section>

@stop

@section('js')
    <script>
        < script src = "https://code.jquery.com/jquery-3.3.1.min.js"
        crossorigin = "anonymous" >
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" crossorigin="anonymous">
    </script>
    </script>

    <script src="{{ asset('plugins/fileinput/js/fileinput.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                overwriteInitial: true,
                @if (isset($banner->image_path))
                    initialPreview: [
                        // IMAGE DATA
                        '<img src="{{ url('uploads/banners-images/' . $banner->image_path) }}" class="kv-preview-data file-preview-image">',
                    ],
                @endif
            });
            $("#input-19").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                overwriteInitial: true,
                @if (isset($banner->image_path))
                    initialPreview: [
                        // IMAGE DATA
                        '<img src="{{ url('uploads/banners-images/' . $banner->image_path) }}" class="kv-preview-data file-preview-image">',
                    ],
                @endif
            });
            $("#input-18").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false
            });
            $(".fe").datepicker();
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#grid').select2();
            $('#genero').select2();
            $('#location').select2();
            $('#generation').select2();
            $('#language').select2();
            $('#box_language').select2();
            $('#media').select2();
            $('#platform').select2();
            $('#region').select2();
        });
    </script>

    @livewireScripts
@stop
