@extends('adminlte::page')

@section('title', 'Admin-Dashboard')
@section('plugins.Select2', true)

@section('content_header')
    <h1 class="font-bold">Editar Faq - {{ $prueba->question }}</h1>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nes/nes_prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <!-- Default CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/default.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">

    @yield('content-css-include')

    <!-- Fancybox CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">


    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <style>
        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Press Start 2P', cursive;
        }

    </style>


    @livewireStyles
@stop

@section('content')

    <div>

        <div class="row">

            <div class="col-md-12" style="background-color: #ffffff; border: solid 1px #db2e2e">

                <br>
                <form action="{{ $viewData['form_url_post'] }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @include('brcode.layout.app_form')

                    @include('brcode.layout.app_form_sub')

                    <div class="text-center form-group row col-md-12">
                        <br>
                        <input type="submit" class="nes-btn is-warning retro btn btn-warning" value="Actualizar">
                        <input type="hidden" name="model_id"
                            value="{{ $viewData['model_id'] > 0 ? $viewData['model_id'] : '' }}" />
                    </div>
                </form>

            </div>
        </div>
    </div>


@stop


@section('js')

<script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
<script src="{{ asset('assets/tinymce/tinymce.min.js') }}"></script>

    <script type="text/javascript">
        $(function() {
            $('#br-btn-delete').click(function() {
                $(this).button('loading');

                var buttons = [{
                        addClass: 'btn btn-primary btn-sm pull-left',
                        text: '{{ Lang::get('app.delete') }}',
                        onClick: function($noty) {
                            $noty.close();
                            deleteRecord()
                        }
                    },
                    {
                        addClass: 'btn btn-danger btn-sm pull-right',
                        text: '{{ Lang::get('app.cancel') }}',
                        onClick: function($noty) {
                            $noty.close();
                            $('#br-btn-delete').button('reset');
                        }
                    },
                ];

                presentNotyMessage('{{ Lang::get('app.delete_are_you_sure') }}', 'warning', buttons, 1);
            });

            function select2Template(state) {
                if (!state.id) {
                    return state.text;
                }

                var children = $(state.element).data('children') || 0;

                var $state = $(
                    '<span ' + (children > 0 ? 'style="font-weight: bold;"' : '') + '>' + state.text + '</span>'
                );
                return $state;
            }

            $('select.br-select2').select2({
                templateResult: select2Template
            });

            var htmlEditors = [];
            if ($(".br-html-editor").length > 0) {


                tinymce.init({
                    selector: 'textarea',
                    language: "es_419",
                    image_class_list: [{
                        title: 'img-responsive',
                        value: 'img-responsive'
                    }, ],
                    height: 500,
                    setup: function(editor) {
                        editor.on('init change', function() {
                            editor.save();
                        });
                    },
                    plugins: [
                        "advlist autolink lists link image charmap print preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media table contextmenu paste imagetools"
                    ],
                    toolbar: "insertfile undo redo | fontselect fontsizeselect formatselect  styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ",

                    image_title: true,
                    automatic_uploads: true,
                    images_upload_url: '/11w5Cj9WDAjnzlg0/pages/upload',
                    file_picker_types: 'image',
                    file_picker_callback: function(cb, value, meta) {
                        var input = document.createElement('input');
                        input.setAttribute('type', 'file');
                        input.setAttribute('accept', 'image/*');
                        input.onchange = function() {
                            var file = this.files[0];

                            var reader = new FileReader();
                            reader.readAsDataURL(file);
                            reader.onload = function() {
                                var id = 'blobid' + (new Date()).getTime();
                                var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                                var base64 = reader.result.split(',')[1];
                                var blobInfo = blobCache.create(id, file, base64);
                                blobCache.add(blobInfo);
                                cb(blobInfo.blobUri(), {
                                    title: file.name
                                });
                            };
                        };
                        input.click();
                    }
                });

            }

            // $('textarea.br-html-editor').each(function(i,e) {

            //   $(this).val($(this).val().trim());
            //   var id = $(this).attr('id');
            //   var editor = new nicEditor({fullPanel : true}).panelInstance(id);
            //   $(this).attr('data-editor-index',i);
            //   htmlEditors.push(editor);

            //   // var toggle = $(this).closest('.br-html-editor-container').find('.br-editor-toggle:eq(0)');
            //   // var full = $(this).closest('.br-html-editor-container').find('.br-editor-full:eq(0)');

            //   toggle.click(function() {
            //     if( ! htmlEditors[i] ) {
            //       htmlEditors[i] = new nicEditor({fullPanel : true}).panelInstance(id);
            //     }
            //     else {
            //       htmlEditors[i].removeInstance(id);
            //       htmlEditors[i] = null;
            //     }
            //   });
            // });

            $('.image-uploader:eq(0)').fileupload({
                dataType: 'json',
                add: function(e, data) {
                    if (file = data.files[0]) {
                        img = new Image();
                        img.onload = function() {
                            $(e.target).prev().text('Uploading');
                            data.submit();
                        };
                        img.src = URL.createObjectURL(file);
                    }
                },
                done: function(e, data) {
                    var target = $(e.target);
                    var img = $('img[data-file-input="' + target.attr('id') + '"]');
                    var noimg = $('div[data-file-input="' + target.attr('id') + '"]');
                    img.attr('src', img.data('path') + '/' + data.result.name + '?' + new Date()
                        .getTime());
                    img.show();
                    if (noimg.length > 0) noimg.hide();
                    $(e.target).prev().text('Select photo');
                    $(e.target).next().val(data.result.name);
                }
            });

            $('[data-function="remove"]').click(function() {
                var input = $('input[name="' + $(this).data('file-target') + '"]');
                var img = $('img[data-file-input="' + $(this).data('file-container') + '"]');
                var div = $('div[data-file-input="' + $(this).data('file-container') + '"]');

                input.val('');
                img.removeAttr('src');
                img.hide();
                div.show();
            });

            function deleteRecord() {
                var url = '{{ $viewData['form_url_post_delete'] }}';
                var form = $('<form action="' + url + '" method="post">' +
                    '<input type="hidden" name="_token" value="{{ csrf_token() }}">' +
                    '<input type="hidden" name="model_id" value="{{ $viewData['model_id'] > 0 ? $viewData['model_id'] : '' }}" />' +
                    '</form>');
                $('body').append(form);
                form.submit();
            }

        });
    </script>

    @livewireScripts
@stop
