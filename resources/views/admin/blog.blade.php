@extends('adminlte::page')

@section('title', 'Admin-Dashboard')

@section('content_header')
 <h1>Blogs</h1>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/tailwind.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/selectpicker/dist/css/bootstrap-select.min.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
     @livewireStyles
@stop

@section('content')

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fas fa-pen-alt"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total</span>
                <span class="info-box-number">
                  {{ $blog->count() }}
                </span>
              </div>

              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->

          </div>

        <div class="col-sm-9">




              <!-- /.info-box-content -->

            <!-- /.info-box -->

          </div>




        </div>


        <livewire:admin.blog-table></livewire:admin.blog-table>


       </div>
    </section>

@stop


@section('js')
    @livewireScripts
@stop
