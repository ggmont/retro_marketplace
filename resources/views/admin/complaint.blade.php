@extends('adminlte::page')

@section('title', 'Admin-Dashboard - Denuncias')

@section('content_header')
    <meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0, user-scalable=no" />
    <center>
        <h1>- Incidencias -</h1>
    </center>
@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/fontawesome.min.css"
        integrity="sha512-cHxvm20nkjOUySu7jdwiUxgGy11vuVPE9YeK89geLMLMMEOcKFyS2i+8wo0FOwyQO/bL8Bvq1KMsqK4bbOsPnA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/style.css?v=2') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/basictable.min.css') }}" />
    @livewireStyles
@stop

@section('content')

    <head>
        <script type="text/javascript" src="{{ asset('js/datatable/basictable.min.js') }}"></script>
    </head>

    <body>
        <section class="content">
            <div class="container-fluid">

                <div id="page">
                    <livewire:admin.user-complaint></livewire:admin.user-complaint>
                </div>
            </div>
        </section>

    </body>




@stop

@section('js')

    @livewireScripts
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.15.4/js/all.js"
        integrity="sha384-rOA1PnstxnOBLzCLMcre8ybwbTmemjzdNlILg8O7z1lUkLXozs4DHonlDtnE7fpc" crossorigin="anonymous">
    </script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $('.order-deny').submit(function(e) {
            e.preventDefault();
            Swal.fire({
                title: '<span class="text-base retro">AVISO</span>',
                text: "¿Estas seguro de aprobar la denuncia? ",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si,Seguro',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {

                    this.submit();
                }
            })
        })
    </script>
@stop
