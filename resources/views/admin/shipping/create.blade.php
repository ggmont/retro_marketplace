@extends('adminlte::page')

@section('title', 'Admin-Dashboard - Nuevo Envio')
@section('plugins.SelectPicker', true)

@section('content_header')
<center>
    <h1 class="retro">- <i class="nes-jp-logo"></i> Nuevo Envio <i class="nes-logo"></i> -</h1>
  </center>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nes/nes_prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <!-- Default CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/default.css') }}">


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">

    @yield('content-css-include')

    <!-- Fancybox CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">


    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <style>
        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Press Start 2P', cursive;
        }

    </style>


    @livewireStyles
@stop

@section('content')

    <section class="content">
        <livewire:admin.shipping-create></livewire:admin.shipping-create>
    </section>

@stop


@section('js')
<script>

</script>
<script>
    $(document).ready(function() {
    $('#grid').select2();
    $('#genero').select2();
    $('#location').select2();
    $('#generation').select2();
    $('#language').select2();
    $('#box_language').select2();
    $('#media').select2();
    $('#platform').select2();
    $('#region').select2();
});
</script>

    @livewireScripts
@stop
