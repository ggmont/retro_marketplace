@extends('adminlte::page')

@section('title', 'Admin-Dashboard - Paginas')

@section('content_header')
    <meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0, user-scalable=no" />
    <center>
        <h1>- Paginas -</h1>
    </center>
@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/fontawesome.min.css"
        integrity="sha512-cHxvm20nkjOUySu7jdwiUxgGy11vuVPE9YeK89geLMLMMEOcKFyS2i+8wo0FOwyQO/bL8Bvq1KMsqK4bbOsPnA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tw-elements/dist/css/index.min.css" />
    <script src="https://cdn.tailwindcss.com/3.2.4"></script>
    <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/style.css?v=2') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/basictable.min.css') }}" />
    <style>
        .tabs {
            display: flex;
            justify-content: space-around;
            margin: 3px;
            height: 40px;
            box-shadow: 0 0 1px 1px rgba(0, 0, 0, 0.2);
        }

        .tabs>* {
            width: 100%;
            color: dimgray;
            height: 100%;
            cursor: pointer;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .tabs>*:hover:not(.active) {
            background-color: rgb(220, 220, 220);
        }

        .tabs>.active {
            color: white;
            background-color: #c53030;

        }

        .panel {
            display: none;
            opacity: 0;
            transition: opacity 0.2s ease-in-out;
        }

        .panel.active {
            display: block;
            opacity: 1;
            transition: opacity 0.2s ease-in-out;
        }
    </style>
    @livewireStyles
@stop

@section('content')

    <head>
        <script type="text/javascript" src="{{ asset('js/datatable/basictable.min.js') }}"></script>
    </head>

    <body>
        <section class="content">
            <div class="container-fluid">
                <div class="tabs">
                    <div class="tab active" data-target="spanish">Español</div>
                    <div class="tab" data-target="english">Ingles</div>
                    <div class="tab" data-target="french">Frances</div>
                    <div class="tab" data-target="italy">Italia</div>
                    <div class="tab" data-target="aleman">Aleman</div>
                    <div class="tab" data-target="portuguese">Portugues</div>
                </div>
                <div id="panels" class="container">
                    <div class="spanish panel active">
                        <livewire:admin.page-table></livewire:admin.page-table>
                    </div>
                    <div class="english panel">
                        <livewire:admin.page-table-english></livewire:admin.page-table-english>
                    </div>
                    <div class="french panel">
                        <livewire:admin.page-table-english></livewire:admin.page-table-english>
                    </div>
                    <div class="italy panel">
                        <livewire:admin.page-table-english></livewire:admin.page-table-english>
                    </div>
                    <div class="aleman panel">
                        <livewire:admin.page-table-english></livewire:admin.page-table-english>
                    </div>
                    <div class="portuguese panel">
                        <livewire:admin.page-table-english></livewire:admin.page-table-english>
                    </div>
                </div>
            </div>
        </section>

    </body>




@stop

@section('js')

    <script>
        const tabs = document.querySelectorAll(".tabs");
        const tab = document.querySelectorAll(".tab");
        const panel = document.querySelectorAll(".panel");

        function onTabClick(event) {
            // deactivate existing active tabs and panel
            for (let i = 0; i < tab.length; i++) {
                tab[i].classList.remove("active");
            }
            for (let i = 0; i < panel.length; i++) {
                panel[i].classList.remove("active");
                panel[i].style.opacity = "0";
            }

            // activate new tabs and panel
            event.target.classList.add("active");
            let classString = event.target.getAttribute("data-target");
            let panelToShow = document
                .getElementById("panels")
                .getElementsByClassName(classString)[0];
            panelToShow.classList.add("active");
            setTimeout(() => {
                panelToShow.style.opacity = "1";
            }, 50);
        }

        for (let i = 0; i < tab.length; i++) {
            tab[i].addEventListener("click", onTabClick, false);
        }
    </script>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/tw-elements/dist/js/index.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.15.4/js/all.js"
        integrity="sha384-rOA1PnstxnOBLzCLMcre8ybwbTmemjzdNlILg8O7z1lUkLXozs4DHonlDtnE7fpc" crossorigin="anonymous">
    </script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@stop
