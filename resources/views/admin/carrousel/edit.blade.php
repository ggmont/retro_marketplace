@extends('adminlte::page')

@section('title', 'Admin-Dashboard')
@section('plugins.FileInput', true)
@section('plugins.Select2', true)

@section('content_header')
<center>
    <h1 class="retro">- <i class="nes-icon coin is-medium"></i> Actualizacion <i class="nes-icon coin is-medium"></i> -
    </h1>
</center>
@stop

@section('css')
<link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <!-- File Uploader -->

    <link rel="stylesheet" href="{{ asset('plugins/fileinput/css/fileinput.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <style>
        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Press Start 2P', cursive;
        }

    </style>

    @livewireStyles
@stop

@section('content')

    <section class="content">
        <div class="row">
            <div class="col-md-12" style="background-color: #ffffff; border: solid 1px #db2e2e">
                <br>
                @if ($carrousel->carousel_id == 3)
                    <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro" for="grid-city">
                        <center>Imagen Secundaria 2 (540 x 357)</center>
                    </h5>
                    <form action="{{ route('carrousel_update_image_secundary_two', ['id' => $carrousel->id]) }}"
                        method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="flex flex-wrap -mx-3 mb-6">
                            <div class="w-full px-3">
                                <label class="block retro text-xs uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                                    for="grid-password">
                                    Link
                                </label>
                                <input
                                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                    name="link"  value="{{ $carrousel->link }}" type="text" placeholder="https://www.retrogamingmarket.eu/">
                            </div>
                        </div>

                        <div class="flex flex-wrap mb-6 -mx-3">

                            <div class="w-full">
                                <div class="file-loading">
                                    <input id="input-20" name="image_path" type="file" data-browse-on-zone-click="true">
                                </div>
                            </div>
                        </div>


                        <div class="text-center form-group col-md-12">
                            <br>
                            <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i>
                                Atrás</a>
                            <input type="submit" class="btn btn-warning" value="Actualizar">
                        </div>

                    </form>
                @elseif($carrousel->carousel_id == 2)
                    <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro" for="grid-city">
                        <center>Imagen Secundaria 1 (540 x 357)</center>
                    </h5>
                    <form action="{{ route('carrousel_update_image_secundary_one', ['id' => $carrousel->id]) }}"
                        method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="flex flex-wrap -mx-3 mb-6">
                            <div class="w-full px-3">
                                <label class="block retro text-xs uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                                    for="grid-password">
                                    Link
                                </label>
                                <input
                                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                    name="link" value="{{ $carrousel->link }}" type="text" placeholder="https://www.retrogamingmarket.eu/">
                            </div>
                        </div>
                        <div class="flex flex-wrap mb-6 -mx-3">

                            <div class="w-full">
                                <div class="file-loading">
                                    <input id="input-19" name="image_path" type="file" data-browse-on-zone-click="true">
                                </div>
                            </div>
                        </div>

                        <div class="text-center form-group col-md-12">
                            <br>
                            <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i>
                                Atrás</a>
                            <input type="submit" class="btn btn-warning" value="Actualizar">
                        </div>
                    </form>
                @else
                    <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase retro" for="grid-city">
                        <center>Imagen Principal (1098 x 384)</center>
                    </h5>
                    <form action="{{ route('carrousel_update_image', ['id' => $carrousel->id]) }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="flex flex-wrap mb-6 -mx-3">
                            <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                           
                                    <label class="block retro text-xs uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                                        for="grid-password">
                                        Link
                                    </label>
                                    <input
                                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        name="link" value="{{ $carrousel->link }}" type="text" placeholder="https://www.retrogamingmarket.eu/">
                                 
                                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                    for="grid-first-name">
                                    Titulo
                                </label>
                                <input
                                    class="block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border border-red-500 rounded appearance-none focus:outline-none focus:bg-white"
                                    id="grid-first-name" value="{{ $carrousel->content_title }}" name="content_title"
                                    type="text" placeholder="RGM">

                                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                    for="grid-state">
                                    Habilitado
                                </label>
                                <div class="relative">
                                    <select name="is_enabled"
                                        class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                        id="grid-state">
                                        <option value="{{ $carrousel->is_enabled }}">{{ $carrousel->is_enabled }}
                                        </option>
                                        <option value="Y">Si
                                        </option>
                                        <option value="N">No
                                        </option>
                                    </select>
                                    <div
                                        class="absolute inset-y-0 right-0 flex items-center px-2 text-gray-700 pointer-events-none">
                                        <svg class="w-4 h-4 fill-current" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20">
                                            <path
                                                d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                        </svg>
                                    </div>
                                </div>
                                <br>
                                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                    for="grid-first-name">
                                    Descripcion (Opcional)
                                </label>
                                <textarea id="content_text" name="content_text" cols="20" rows="10" class="form-control"
                                    placeholder="Añada un comentario con respecto al producto">{{ $carrousel->content_text }}</textarea>
                                    
                            </div>
                            <div class="w-full px-3 md:w-1/2">
                                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                    for="grid-last-name">
                                    Imagen
                                </label>
                                <div class="file-loading">
                                    <input id="input-20" name="image_path" type="file" data-browse-on-zone-click="true"
                                        multiple="multiple">
                                </div>
                            </div>
                        </div>
                        <div class="text-center form-group col-md-12">
                            <br>
                            <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i>
                                Atrás</a>
                            <input type="submit" class="btn btn-warning" value="Actualizar">
                        </div>
                    </form>
                @endif


                <br>

            </div>
        </div>
    </section>

@stop


@section('js')
<script>
    < script src = "https://code.jquery.com/jquery-3.3.1.min.js"
    crossorigin = "anonymous" >
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" crossorigin="anonymous">
</script>
</script>

<script src="{{ asset('plugins/fileinput/js/fileinput.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $("#input-20").fileinput({
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: false,
            showUpload: false,
            allowedFileExtensions: ["jpg", "jpeg", "png"],
            overwriteInitial: true,
            @if (isset($carrousel->image_path))
                initialPreview: [
                // IMAGE DATA
                '<img src="{{ url('uploads/carousels-images/' . $carrousel->image_path) }}" class="kv-preview-data file-preview-image">',
                ],
            @endif
        });
        $("#input-19").fileinput({
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: false,
            showUpload: false,
            allowedFileExtensions: ["jpg", "jpeg", "png"],
            overwriteInitial: true,
            @if (isset($carrousel->image_path))
                initialPreview: [
                // IMAGE DATA
                '<img src="{{ url('uploads/carousels-images/' . $carrousel->image_path) }}" class="kv-preview-data file-preview-image">',
                ],
            @endif
        });
        $("#input-18").fileinput({
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: false,
            showUpload: false
        });
        $(".fe").datepicker();
    });
</script>
<script>
    $(document).ready(function() {
        $('#grid').select2();
        $('#genero').select2();
        $('#location').select2();
        $('#generation').select2();
        $('#language').select2();
        $('#box_language').select2();
        $('#media').select2();
        $('#platform').select2();
        $('#region').select2();
    });
</script>

    @livewireScripts
@stop
