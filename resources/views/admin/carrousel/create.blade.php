@extends('adminlte::page')

@section('title', 'Admin-Dashboard')
@section('plugins.FileInput', true)
@section('plugins.Select2', true)

@section('content_header')
    <h1>Agregar Nuevas Imagenes para el Home</h1>
@stop

@section('css')
<link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <!-- File Uploader -->

    <link rel="stylesheet" href="{{ asset('plugins/fileinput/css/fileinput.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">


    @livewireStyles
@stop

@section('content')

    <section class="content">
        <div class="row">
            <div class="col-md-12" style="background-color: #ffffff; border: solid 1px #db2e2e">
                <br>
                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase" for="grid-city">
                    <center>Imagen Principal</center>
                </h5>
                <br>
                <form class="" method="POST" action="{{ route('carrousel_store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="flex flex-wrap mb-6 -mx-3">
                        <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                            <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                for="grid-first-name">
                                Titulo
                            </label>
                            <input
                                class="block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border border-red-500 rounded appearance-none focus:outline-none focus:bg-white"
                                id="grid-first-name" name="content_title" type="text" placeholder="RGM">
                            <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                for="grid-first-name">
                                Descripcion (Opcional)
                            </label>
                            <textarea id="content_text" name="content_text" cols="20" rows="10" class="form-control"
                                placeholder="Añada un comentario con respecto al producto"></textarea>
                        </div>
                        <div class="w-full px-3 md:w-1/2">
                            <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                for="grid-last-name">
                                Imagen
                            </label>
                            <div class="file-loading">
                                <input id="input-20" name="image_path" type="file" data-browse-on-zone-click="true"
                                    multiple="multiple">
                            </div>
                        </div>
                    </div>


                    <div class="text-center form-group col-md-12">
                        <br>
                        <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i>
                            Atrás</a>
                        <input type="submit" class="btn btn-danger" value="Registrar">
                    </div>

                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" style="background-color: #ffffff; border: solid 1px #db2e2e">
                <br>
                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase" for="grid-city">
                    <center>Imagen Secundaria 1</center>
                </h5>
                <br>
                <form class="" method="POST" action="{{ route('carrousel_store_secundary_one') }}" enctype="multipart/form-data">
                    @csrf




                    <div class="file-loading">
                        <input id="input-19" name="image_path" type="file" data-browse-on-zone-click="true"
                            multiple="multiple">
                    </div>




                    <div class="text-center form-group col-md-12">
                        <br>
                        <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i>
                            Atrás</a>
                        <input type="submit" class="btn btn-danger" value="Registrar">
                    </div>

                </form>
            </div>
            <div class="col-md-6" style="background-color: #ffffff; border: solid 1px #db2e2e">
                <br>
                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase" for="grid-city">
                    <center>Imagen Secundaria 2</center>
                </h5>
                <br>
                <form class="" method="POST" action="{{ route('carrousel_store_secundary_one') }}" enctype="multipart/form-data">
                    @csrf




                    <div class="file-loading">
                        <input id="input-18" name="image_path" type="file" data-browse-on-zone-click="true"
                            multiple="multiple">
                    </div>




                    <div class="text-center form-group col-md-12">
                        <br>
                        <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i>
                            Atrás</a>
                        <input type="submit" class="btn btn-danger" value="Registrar">
                    </div>

                </form>
           
        </div>
        </div>
   

    </section>

@stop


@section('js')
    <script>
        < script src = "https://code.jquery.com/jquery-3.3.1.min.js"
        crossorigin = "anonymous" >
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" crossorigin="anonymous">
    </script>
    </script>

    <script src="{{ asset('plugins/fileinput/js/fileinput.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false
            });
            $("#input-19").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false
            });
            $("#input-18").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false
            });
            $(".fe").datepicker();
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#grid').select2();
            $('#genero').select2();
            $('#location').select2();
            $('#generation').select2();
            $('#language').select2();
            $('#box_language').select2();
            $('#media').select2();
            $('#platform').select2();
            $('#region').select2();
        });
    </script>

    @livewireScripts
@stop
