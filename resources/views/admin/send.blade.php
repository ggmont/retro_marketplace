@extends('adminlte::page')

@section('title', 'Admin-Dashboard - Noticias')

@section('content_header')
    <center>
        <h1 class="font-bold">- Envíos (Zona de prueba) -</h1>
    </center>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nes/nes_prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <!-- Default CSS -->
    <link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/default.css') }}">

    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">

    @yield('content-css-include')

    <!-- Fancybox CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">


    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <style>
        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Press Start 2P', cursive;
        }
    </style>


    @livewireStyles
@stop

@section('content')

    <section class="content">
        <div class="container-fluid">

            <span class="retro">@include('partials.flash')</span>



            <form action="{{ route('beseif') }}" method="post">
                {{ csrf_field() }}


                <input type="hidden" class="form-control" name="email_vendedor" value="vendedor@mail.com" required>

                <input type="hidden" class="form-control" name="email_comprador" value="comprador@mail.com"
                    required>

                <input type="number" class="form-control" name="precio" 
                    required>

                <button type="submit" class="btn btn-danger btn-flat">Permitir Notificaciones
                </button>

            </form>
            <div class="col-12">


            </div>



        </div>
    </section>
@stop


@section('js')

@stop
