@extends('adminlte::page')

@section('title', 'Admin-Dashboard')
@section('plugins.Select2', true)

@section('content_header')
    <h1 class="font-bold">Editar Noticia - {{ $prueba->title }}</h1>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nes/nes_prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <!-- Default CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/default.css') }}">

    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">

    @yield('content-css-include')

    <!-- Fancybox CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">


    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <style>
        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Press Start 2P', cursive;
        }

    </style>


    @livewireStyles
@stop

@section('content')

    <div>

        <div class="row">

            <div class="col-md-12" style="background-color: #ffffff; border: solid 1px #db2e2e">

                <br>
                <form action="{{ route('update_new') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <label>Titulo</label>

                    <input type="text" class="form-control" id="br_c_title" name="br_c_title" value="{{ $prueba->title }}" />

                    @foreach ($viewData['model_cols'] as $key => $col)
                        <?php
                        
                        $label = '';
                        $lang = isset($viewData['lang']) ? $viewData['lang'] : 'app';
                        
                        if (isset($col['label'])) {
                            //$label = $lang.'.col_'.$col['label'];
                            $label = $col['label'];
                        } elseif (substr($key, 0, 4) == 'rel_') {
                            $label = $lang . '.col_' . str_replace('rel_', '', $key);
                        } else {
                            $label = $lang . '.col_' . $key;
                        }
                        ?>
                        @if ($viewData['column_prefix'] . $key == 'br_c_language')
                            <div class="form-group">
                                <label for="{{ $viewData['column_prefix'] . $key }}">Contenido


                                    @php($selectedValue = [])
                                    @if (isset($viewData['plat_dic']))
                                        @foreach ($viewData['plat_dic'] as $key3)
                                            @php(array_push($selectedValue, $key3))
                                        @endforeach
                                    @endif

                                </label>

                            </div>
                        @endif
                    @endforeach

                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- magaya connection -->
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">
                                        {{ isset($viewData['form_sec_title']) ? $viewData['form_sec_title'] : '' }}</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <div class="box-body">
                                    @foreach ($viewData['model_cols'] as $key => $col)
                                        <?php
                                        
                                        $label = '';
                                        $lang = isset($viewData['lang']) ? $viewData['lang'] : 'app';
                                        
                                        if (isset($col['label'])) {
                                            //$label = $lang.'.col_'.$col['label'];
                                            $label = $col['label'];
                                        } elseif (substr($key, 0, 4) == 'rel_') {
                                            $label = $lang . '.col_' . str_replace('rel_', '', $key);
                                        } else {
                                            $label = $lang . '.col_' . $key;
                                        }
                                        ?>
                                        @if ($col['type'] == 'hidden')

                                        @elseif(strpos($col['type'],'input')!== false)





                                        @elseif($col['type'] == 'html-editor')
                                            <div class="form-group br-html-editor-container">
                                                <label
                                                    for="{{ $viewData['column_prefix'] . $key }}">Contenido</label>
                                                <textarea
                                                    class="form-control {{ isset($col['class']) ? $col['class'] : '' }}"
                                                    id="{{ $viewData['column_prefix'] . $key }}"
                                                    name="{{ $viewData['column_prefix'] . $key }}"
                                                    {!! isset($col['attr']) ? $col['attr'] : '' !!}>
                {{ strlen(old($viewData['column_prefix'] . $key)) > 0 ? old($viewData['column_prefix'] . $key) : (isset($viewData['model'][$key]) ? $viewData['model'][$key] : '') }}
              </textarea>
                                                <!-- <div class="clearfix">
                <button type="button" class="btn btn-default col-sm-6 br-editor-toggle">{{ Lang::get('app.toogle_editor') }}</button>
                <button type="button" class="btn btn-default col-sm-6 br-editor-full">{{ Lang::get('app.full_screen') }}</button>
              </div> -->
                                                <span
                                                    class="help-block">{{ str_replace(str_replace('_', ' ', $viewData['column_prefix']), '', $errors->first($viewData['column_prefix'] . $key)) }}</span>
                                            </div>
                                        @endif
                                    @endforeach

                                    <div class="col-md-12">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="grid-state">
                                            Habilitado
                                        </label>
                                        <div class="relative">
                                            <select
                                                class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                                id="br_c_is_enabled" name="br_c_is_enabled" required="">
                                                <option value="{{ $prueba->is_enabled }}">{{ $prueba->is_enabled }}
                                                </option>
                                                <option value="Y">Si
                                                </option>
                                                <option value="N">No
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- /.col-lg-6 -->

                        

                        <div class="text-center form-group row col-md-12">
                            <br>
                            <input type="submit" class="nes-btn is-warning retro btn btn-warning" value="Actualizar">
                            <input type="hidden" name="model_id"
                                value="{{ $viewData['model_id'] > 0 ? $viewData['model_id'] : '' }}" />
                        </div>
                </form>

            </div>
        </div>
    </div>


@stop


@section('js')

    <script src="{{ asset('assets/niceditor/nicEdit.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#br_c_is_enabled').select2();
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#br-btn-delete').click(function() {
                $(this).button('loading');

                var buttons = [{
                        addClass: 'btn btn-primary btn-sm pull-left',
                        text: '{{ Lang::get('app.delete') }}',
                        onClick: function($noty) {
                            $noty.close();
                            deleteRecord()
                        }
                    },
                    {
                        addClass: 'btn btn-danger btn-sm pull-right',
                        text: '{{ Lang::get('app.cancel') }}',
                        onClick: function($noty) {
                            $noty.close();
                            $('#br-btn-delete').button('reset');
                        }
                    },
                ];

                presentNotyMessage('{{ Lang::get('app.delete_are_you_sure') }}', 'warning', buttons, 1);
            });

            function select2Template(state) {
                if (!state.id) {
                    return state.text;
                }

                var children = $(state.element).data('children') || 0;

                var $state = $(
                    '<span ' + (children > 0 ? 'style="font-weight: bold;"' : '') + '>' + state.text + '</span>'
                );
                return $state;
            }

            $('select.br-select2').select2({
                templateResult: select2Template
            });

            var htmlEditors = [];

            $('textarea.br-html-editor').each(function(i, e) {
                $(this).val($(this).val().trim());
                var id = $(this).attr('id');
                var editor = new nicEditor({
                    fullPanel: true
                }).panelInstance(id);
                $(this).attr('data-editor-index', i);
                htmlEditors.push(editor);

                var toggle = $(this).closest('.br-html-editor-container').find('.br-editor-toggle:eq(0)');
                var full = $(this).closest('.br-html-editor-container').find('.br-editor-full:eq(0)');

                toggle.click(function() {
                    if (!htmlEditors[i]) {
                        htmlEditors[i] = new nicEditor({
                            fullPanel: true
                        }).panelInstance(id);
                    } else {
                        htmlEditors[i].removeInstance(id);
                        htmlEditors[i] = null;
                    }
                });
            });


            function deleteRecord() {
                var url = '{{ $viewData['form_url_post_delete'] }}';
                var form = $('<form action="' + url + '" method="post">' +
                    '<input type="hidden" name="_token" value="{{ csrf_token() }}">' +
                    '<input type="hidden" name="model_id" value="{{ $viewData['model_id'] > 0 ? $viewData['model_id'] : '' }}" />' +
                    '</form>');
                $('body').append(form);
                form.submit();
            }

        });
    </script>

    @livewireScripts
@stop
