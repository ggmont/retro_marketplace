@extends('adminlte::page')

@section('title', 'Admin-Dashboard')
@section('plugins.FileInput', true)
@section('plugins.Select2', true)

@section('content_header')
    <h1>Editar Producto - {{ $product->name }}</h1>
@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
    <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/style.css?v=2') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/basictable.min.css') }}" />
    <link href="/assets/dropzone-master/dist/min/dropzone.min.css" rel="stylesheet" type="text/css" />
    <style>
        .dropzone .dz-preview .dz-error-message {
            top: 175px !important;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    @livewireStyles
@stop


@section('content')

    <div>

        <div class="row">

            <div class="col-md-12" style="background-color: #ffffff; border: solid 1px #db2e2e">
                @include('partials.flash')
                <br>
                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase" for="grid-city">
                    Detalles Basicos
                </h5>
                <br>
                <form class="w-full" action="{{ route('update_product', $product->id) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="flex flex-wrap mb-2 -mx-3">
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                            <label for="name"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nombre <font
                                    color="red">*</font></label>
                            <input type="text" value="{{ $product->name }}" name="name"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Retro" required>

                        </div>

                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                            <label for="name_en"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nombre Alternativo
                                <font color="red">*</font>
                            </label>
                            <input type="text" value="{{ $product->name_en }}" name="name_en"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="RGM" required>

                        </div>

                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                            <label for="release_date"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Fecha
                                de Lanzamiento <font color="red">(Opcional)</font></label>
                            <input type="text" id="datepicker" value="{{ $product->release_date }}" name="release_date"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="14/03/2023">

                        </div>
                    </div>
                    <br>
                    <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase" for="grid-city">
                        Detalles del Producto
                    </h5>
                    <br>
                    <div class="flex flex-wrap mb-2 -mx-3">
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                            <label for="category"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Categoria
                                <font color="red">*</font>
                            </label>
                            <div class="relative">
                                <select
                                    class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="grid" name="prod_category_id" required="">
                                    <option value="{{ $product->prod_category_id }}">{{ $product->category->name }}
                                    </option>
                                    @foreach ($categories as $c)
                                        @if (!in_array($c->category_text, ['Juegos', 'Consolas', 'Accesorios', 'Periféricos']))
                                            <option value="{{ $c->id }}">{{ $c->category_text }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                            <label for="gener"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Genero
                                <font color="red">(Opcional)</font>
                            </label>
                            <div class="relative">
                                <select
                                    class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="genero" name="game_category">
                                    <option value="{{ $product->game_category }}">{{ $product->game_category }}
                                    </option>
                                    @foreach ($genero as $g)
                                        <option value="{{ $g->value }}">{{ $g->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                            <label for="location"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Localización
                                <font color="red">*</font>
                            </label>
                            <div class="relative">
                                <select
                                    class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="location" name="game_location" required="">
                                    <option value="{{ $product->game_location }}">{{ $product->game_location }}
                                    </option>
                                    @foreach ($location as $l)
                                        <option value="{{ $l->value }}">{{ $l->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="flex flex-wrap mb-2 -mx-3">
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                            <label for="generation"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Generación
                                <font color="red">(Opcional)</font>
                            </label>
                            <div class="relative">
                                <select
                                    class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="generation" name="game_generation">
                                    <option value="{{ $product->game_generation }}">{{ $product->game_generation }}
                                    </option>
                                    @foreach ($generation as $ge)
                                        <option value="{{ $ge->value }}">{{ $ge->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                            <label for="language"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Lenguaje
                                <font color="red">*</font>
                            </label>
                            <div class="relative">
                                <select
                                    class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="language" name="language[]" multiple required="">
                                    <option value="{{ $product->language }}" selected>{{ $product->language }}</option>
                                    @foreach ($language as $la)
                                        <option value="{{ $la->value }}">{{ $la->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                            <label for="box_language"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Lenguaje de caja
                                <font color="red">(Opcional)</font>
                            </label>
                            <div class="relative">
                                <select
                                    class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="box_language" multiple name="box_language[]">
                                    <option value="{{ $product->box_language }}" selected>{{ $product->box_language }}
                                    </option>
                                    @foreach ($language as $la)
                                        <option value="{{ $la->value }}">{{ $la->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase" for="grid-city">
                        Detalles Tecnicos
                    </h5>
                    <br>
                    <div class="flex flex-wrap mb-2 -mx-3">
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                            <label for="media"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Media
                                <font color="red">*</font>
                            </label>
                            <div class="relative">
                                <select
                                    class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="media" name="media" required="">
                                    <option value="{{ $product->media }}">{{ $product->media }}</option>
                                    @foreach ($media as $m)
                                        <option value="{{ $m->value }}">{{ $m->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                            <label for="platform"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Plataforma
                                <font color="red">*</font>
                            </label>
                            <div class="relative">
                                <select
                                    class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="platform" name="platform" required="">
                                    <option value="{{ $product->platform }}">{{ $product->platform }}</option>
                                    @foreach ($platform as $p)
                                        <option value="{{ $p->value }}">{{ $p->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                            <label for="region"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Región
                                <font color="red">*</font>
                            </label>
                            <div class="relative">
                                <select
                                    class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="region" name="region" required="">
                                    <option value="{{ $product->region }}">{{ $product->region }}</option>
                                    @foreach ($region as $r)
                                        <option value="{{ $r->value }}">{{ $r->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="flex flex-wrap mb-2 -mx-3">
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                            <label for="ean_upc" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">EAN
                                / UPC
                                <font color="red">(Opcional)</font>
                            </label>

                            <input type="text" value="{{ $product->ean_upc }}" name="ean_upc"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="010203">

                        </div>

                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                            <label for="volume"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Volume
                                <font color="red">*</font>
                            </label>

                            <input type="number" step="0.0001" value="{{ $product->volume }}" name="volume"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="0.0001" required="">

                        </div>

                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                            <label for="volume"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Peso
                                <font color="red">*</font>
                            </label>

                            <input type="text" step="0.0001" value="{{ $product->weight }}" name="weight"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="KG" required="">

                        </div>
                    </div>
                    <br>
                    <div class="flex flex-wrap mb-2 -mx-3">
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                            <label for="large"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Largo
                                <font color="red">*</font>
                            </label>

                            <input type="number" value="{{ $product->large }}" name="large"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="10" required="">


                        </div>
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                            <label for="width"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Ancho
                                <font color="red">*</font>
                            </label>

                            <input type="number" step="0.0001" value="{{ $product->width }}" name="width"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="20" required="">

                        </div>
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                            <label for="high"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Alto
                                <font color="red">*</font>
                            </label>

                            <input type="number" value="{{ $product->high }}" name="high"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="30" required="">

                        </div>
                    </div>
                    <br>
                    <div class="flex flex-wrap mb-2 -mx-3">

                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                            <label for="price"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Precio
                                <font color="#A93226">solo</font>
                            </label>

                            <input type="number" autocomplete="off" data-number="2" step="0.01"
                                value="{{ $product->price_solo }}" name="price_solo"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="0,00" required="">


                        </div>

                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                            <label for="price"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Precio
                                <font color="#B7950B">usado</font>
                            </label>

                            <input type="number" autocomplete="off" data-number="2" step="0.01"
                                value="{{ $product->price_used }}" name="price_used"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="0,00" required="">


                        </div>

                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                            <label for="price"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Precio
                                <font color="#1E8449">nuevo</font>
                            </label>

                            <input type="number" autocomplete="off" data-number="2" step="0.01"
                                value="{{ $product->price_new }}" name="price_new"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="0,00" required="">

                        </div>

                    </div>
                    <br>

                    <div style="display:none" id="fileupload-files" data-name="image_path[]">
                        @foreach ($product->images as $p)
                            @php($i = 0)
                            <input type="hidden" id="file_{{ $i }}" name="image_path[]"
                                value="{{ $p->image_path }}">
                            @php($i++)
                        @endforeach
                    </div>
                    <div id="dropzone">
                        <div class="dz-message text-dark" data-dz-message>
                            <span
                                class="retro">{{ __('Suelte los archivos aquí o haga clic para cargar Max - 4.') }}</span>
                        </div>
                    </div>
                    <br>
                    <div class="form-group col-sm-12">
                        <label for="comments"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Descripción
                            <font color="red">(Opcional)</font>
                        </label>
                        <textarea id="comments" name="comments" cols="20" rows="10" class="form-control"
                            placeholder="Añada un comentario con respecto al producto">{{ $product->comments }}</textarea>
                    </div>



                    <div class="text-center form-group col-md-12">
                        <br>
                        <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i>
                            Atrás</a>
                        <input type="submit" class="btn btn-warning" value="Actualizar">
                    </div>

                </form>
            </div>
        </div>
    </div>


@stop


@section('js')
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>

    <script src="{{ asset('assets/dropzone-master/dist/min/dropzone.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/quagga/0.12.1/quagga.js"></script>
    <script src="{{ asset('vendor/sweetalert2/sweetalert2.all.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                overwriteInitial: true,
                @if ($product->images)
                    initialPreview: [
                        // IMAGE DATA
                        '<img src="{{ url('/uploads/profile-images/' . auth()->user()->profile_picture) }}"
                        class = "kv-preview-data file-preview-image" > ',
                    ],
                @endif
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#grid').select2();
            $('#genero').select2();
            $('#location').select2();
            $('#generation').select2();
            $('#language').select2();
            $('#box_language').select2();
            $('#media').select2();
            $('#platform').select2();
            $('#region').select2();
        });
    </script>

    <script>
        var acceptedFileTypes = "image/*"; //dropzone requires this param be a comma separated list
        var fileList = new Array;
        var i = 0;
        var myDropzone = $("#dropzone").dropzone({
            paramName: "file",
            maxFilesize: 20,
            url: "{{ url('11w5Cj9WDAjnzlg0/product/upload_file/' . $product->id) }}",
            addRemoveLinks: true,
            removedfile: function(file) {
                var name = file.name;
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: "{{ url('11w5Cj9WDAjnzlg0/product/delete_file/' . $product->id) }}",
                    data: {
                        filename: name
                    },
                    success: function(data) {
                        console.log("Se a borrado correctamente");
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            maxFiles: 4, //change limit as per your requirements
            maxfilesexceeded: function(file) {
                Swal.fire({
                    title: '<span class="retro">Error</span>',
                    text: 'Haz Alcanzado el maximo de Imagenes (Max 4)',
                    footer: '<a class="retro" href="">¿Como Vender En RGM?</a>',
                    imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/a6cff967-2930-4785-a8b8-48a6e39101b8/dbrz0sh-36c848a3-b37d-4fd1-8d7e-90a27dcb8130.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2E2Y2ZmOTY3LTI5MzAtNDc4NS1hOGI4LTQ4YTZlMzkxMDFiOFwvZGJyejBzaC0zNmM4NDhhMy1iMzdkLTRmZDEtOGQ3ZS05MGEyN2RjYjgxMzAuZ2lmIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.QwzwRFsxRbUrhacQuJV4fSAij3GxImG_4dkxWDTgkMc',
                    imageWidth: 400,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                })
                this.removeFile(file);
            },
            acceptedFiles: ".jpg,.jpeg,.png",
            dictInvalidFileType: "No puedes subir archivos de este tipo , solo Imagenes tipo PNG O JPG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Drop files here to upload",
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    Swal.fire({
                        title: '<span class="retro">Listo!</span>',
                        text: 'La imagen fue agregada correctamente',
                        imageUrl: 'https://phoneky.co.uk/thumbs/screensavers/down/games/supermario_knocmls3.gif',
                        imageWidth: 400,
                        imageHeight: 200,
                        imageAlt: 'Custom image',
                    })
                    fileList[i] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": i
                    };
                    input = $('#fileupload-files').data('name');
                    $('#fileupload-files').append(
                        `<input type="hidden" id="file_${ i }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    i += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileList.length; f++) {
                        if (fileList[f].fileName == file.name) {
                            rmvFile = fileList[f].serverFileName;
                            $("#file_" + fileList[f].fileId).remove();
                        }
                    }
                });

                @foreach ($product->images as $p)
                    var mockFile = {
                        name: "{{ $p->image_path }}",
                        size: 12345
                    };

                    myDropzone.emit("addedfile", mockFile);

                    myDropzone.emit("thumbnail", mockFile, "/images/{{ $p->image_path }}");
                    myDropzone.emit("complete", mockFile);
                    fileList[i] = {
                        "serverFileName": "{{ $p->image_path }}",
                        "fileName": "{{ $p->image_path }}",
                        "fileId": i
                    };
                    i += 1;
                @endforeach

            }
        });
    </script>
    @livewireScripts
@stop
