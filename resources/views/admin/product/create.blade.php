@extends('adminlte::page')

@section('title', 'Admin-Dashboard')
@section('plugins.FileInput', true)
@section('plugins.Select2', true)
@section('plugins.DatePicker', true)

@section('content_header')
    <h1>Agregar Nuevo Producto</h1>
@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
    <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/style.css?v=2') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/basictable.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/fileinput/css/fileinput.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    @livewireStyles
@stop

 
@section('content')

    <section class="content">
        <livewire:admin.product-create></livewire:admin.product-create>
    </section>

@stop


@section('js')
    <script>
        < script src = "https://code.jquery.com/jquery-3.3.1.min.js"
        crossorigin = "anonymous" >
    </>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" crossorigin="anonymous">
    </script>
    </script>

    <script src="{{ asset('plugins/fileinput/js/fileinput.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false
            });
            $("#input-19").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false
            });
            $("#input-18").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false
            });
            $("#datepicker").datepicker();
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#grid').select2();
            $('#genero').select2();
            $('#location').select2();
            $('#generation').select2();
            $('#language').select2();
            $('#box_language').select2();
            $('#media').select2();
            $('#platform').select2();
            $('#region').select2();
        });
    </script>

    @livewireScripts
@stop
