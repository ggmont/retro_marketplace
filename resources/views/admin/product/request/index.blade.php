@extends('adminlte::page')

@section('title', 'Admin-Dashboard')

@section('content_header')
    <meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0, user-scalable=no" />
    <center>
        <h1>- Solicitudes de Productos -</h1>
    </center>
@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/fontawesome.min.css"
        integrity="sha512-cHxvm20nkjOUySu7jdwiUxgGy11vuVPE9YeK89geLMLMMEOcKFyS2i+8wo0FOwyQO/bL8Bvq1KMsqK4bbOsPnA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/style.css?v=2') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/basictable.min.css') }}" />
    @livewireStyles
@stop

@section('content')

    <head>
        <script type="text/javascript" src="{{ asset('js/datatable/basictable.min.js') }}"></script>
    </head>

    <body>
        <section class="content">
            <div class="container-fluid">
                @include('partials.flash')
                <div id="page">
                    <livewire:admin.product-request-table></livewire:admin.product-request-table>
                </div>
            </div>
        </section>

    </body>




@stop

@section('js')

    <script>
        Livewire.on('twoAxisRequest', function() {

            const table = new basictable('.table');

            new basictable('#table-breakpoint', {
                breakpoint: 768,
            });

            new basictable('#table-container-breakpoint', {
                containerBreakpoint: 485,
            });

            new basictable('#table-force-off', {
                forceResponsive: false,
            });

            new basictable('#table-max-height', {
                tableWrap: true
            });

            new basictable('#table-no-resize', {
                noResize: true,
            });

            new basictable('#table-two-axis');

        })
    </script>

    @livewireScripts
@stop
