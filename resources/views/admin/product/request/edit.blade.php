@extends('adminlte::page')

@section('title', 'Admin-Dashboard')
@section('plugins.FileInput', true)
@section('plugins.Select2', true)

@section('content_header')

    @if ($product_request->auction == 'Y')
        <center>
            <h1>Edicion de Solicitud de Producto <font color="red">EN OFERTA</font> por
                {{ $product_request->user->user_name }}</h1>
        </center>
    @else
        <center>
            <h1>Edicion de Solicitud de Producto <font color="green">NORMAL</font> por {{ $product_request->user->user_name }}</h1>
        </center>
    @endif

@stop


@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/fontawesome.min.css"
        integrity="sha512-cHxvm20nkjOUySu7jdwiUxgGy11vuVPE9YeK89geLMLMMEOcKFyS2i+8wo0FOwyQO/bL8Bvq1KMsqK4bbOsPnA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tw-elements/dist/css/index.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
    <script src="https://cdn.tailwindcss.com/3.2.4"></script>
    <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/style.css?v=2') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/basictable.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/fileinput/css/fileinput.min.css') }}">
    <style>
        .tabs {
            display: flex;
            justify-content: space-around;
            margin: 3px;
            height: 40px;
            box-shadow: 0 0 1px 1px rgba(0, 0, 0, 0.2);
        }

        .tabs>* {
            width: 100%;
            color: dimgray;
            height: 100%;
            cursor: pointer;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .tabs>*:hover:not(.active) {
            background-color: rgb(220, 220, 220);
        }

        .tabs>.active {
            color: white;
            background-color: #c53030;

        }

        .panel {
            display: none;

        }

        .panel.active {
            display: block;

        }
    </style>
    @livewireStyles
@stop

@section('content')

    <head>
        <script type="text/javascript" src="{{ asset('js/datatable/basictable.min.js') }}"></script>
    </head>

    <body>
        <section class="content">
            <div class="container-fluid">
                <div class="tabs">
                    <div class="tab active" data-target="request">Solicitud</div>
                    <div class="tab" data-target="search_product">Busqueda del producto</div>
                </div>

                <div class="col-md-12 pt-6" style="background-color: #ffffff; border: solid 1px #db2e2e">

                    <div id="panels" class="container">
                        <div class="request panel active">

                            <div class="text-center form-group col-md-12 mb-50">
                                <button data-toggle="modal" data-target="#not"
                                    class="btn confirmclosed btn-danger font-bold w-100">Desaprobar</button>
                            </div>

                            <div class="modal fade" id="not" tabindex="-1" role="dialog"
                                aria-labelledby="categoryModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content bg-white">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="d-flex align-items-center justify-content-between mb-4">
                                                    <h4 class="modal-title" id="addnewcontactlabel"> - :</h4>
                                                    <button class="btn btn-close p-1 ms-auto me-0" class="close"
                                                        data-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="wide-block pb-1 pt-2">
                                                    <form class="nk-form" method="POST"
                                                        action="{{ route('delete_product_request', $product_request->id) }}">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <div class="row vertical-gap">
                                                            <div class="col-md-12">
                                                                <h4 class="text-center text-lg font-bold retro">¿Está seguro
                                                                    de desaprobar esta solicitud?</h4>
                                                                <br>
                                                            </div>
                                                        </div>
                                                        <div class="nk-gap-2"></div>
                                                        <div class="row">
                                                            <div class="col-md-12 flex justify-between">
                                                                <button class="btn-danger py-2 px-4 rounded"
                                                                    data-dismiss="modal">
                                                                    <span class="retro text-lg">CANCELAR</span>
                                                                </button>
                                                                <button class="btn confirmclosed text-white bg-green-800"
                                                                    type="submit" style="min-width: 100px;">
                                                                    <span class="retro text-lg">Aceptar</span>
                                                                </button>

                                                            </div>
                                                        </div>


                                                    </form>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>




                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    @if ($product_request->category == 'Juegos')
                                        <table id="table-two-axis" class="two-axis">
                                            <thead>
                                                <tr class="border-b border-gray-500 hover:bg-gray-200">
                                                    <th class="text-xs">PRODUCTO</th>
                                                    <th class="text-xs">PLATAFORMA</th>
                                                    <th class="text-xs">REGION</th>
                                                    <th class="text-xs">CAJA</th>
                                                    <th class="text-xs">CARATULA</th>
                                                    <th class="text-xs">MANUAL</th>
                                                    <th class="text-xs">JUEGO</th>
                                                    <th class="text-xs">EXTRA</th>
                                                    <th class="text-xs"><i class="fa fa-image"></i></th>
                                                    <th class="text-xs"> <i class="fa fa-barcode" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="text-xs"><i class="fa fa-comment"></i></th>
                                                    <th class="text-xs">PRECIO</th>
                                                    <th class="text-xs">STOCK</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <tr class="border-b border-gray-500 hover:bg-gray-200">
                                                    <td class="text-sm font-semibold text-gray-900">
                                                        {{ $product_request->product }} </td>
                                                    <td class="text-sm font-semibold text-gray-900">
                                                        {{ $product_request->platform }} </td>
                                                    <td class="text-sm font-semibold text-gray-900">
                                                        {{ $product_request->region }} </td>
                                                    <td class="text-sm font-semibold text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $product_request->box }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($product_request->box_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    <td class="text-base text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $product_request->cover }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($product_request->cover_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    <td class="text-base text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $product_request->manual }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($product_request->manual_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    <td class="text-base text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $product_request->game }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($product_request->game_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    <td class="text-base text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $product_request->extra }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($product_request->extra_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    @if ($imagen_product->first())
                                                        <td class="text-center product-cart-img item">

                                                            <a href="{{ url('/uploads/inventory-images/' . $imagen_product->first()->image_path) }}"
                                                                class="fancybox" data-fancybox="RGM">
                                                                <img src="{{ url('/uploads/inventory-images/' . $imagen_product->first()->image_path) }}"
                                                                    width="50px" height="50px" />
                                                            </a>


                                                            @foreach ($imagen_product as $p)
                                                                @if (!$loop->first)
                                                                    <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                        data-fancybox="RGM">
                                                                        <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                            width="0px" height="0px"
                                                                            style="position:absolute;" />
                                                                    </a>
                                                                @endif
                                                            @endforeach


                                                        </td>
                                                    @else
                                                        <td class="product-price">
                                                            <font color="black">
                                                                <i class="fa fa-times" data-toggle="popover"
                                                                    data-content="No Posee" data-placement="top"
                                                                    data-trigger="hover"></i>
                                                            </font>
                                                        </td>
                                                    @endif


                                                    @if ($imagen_product_ean->first())
                                                        <td class="text-center product-cart-img item">

                                                            <a href="{{ url('/uploads/ean-images/' . $imagen_product_ean->first()->image_ean) }}"
                                                                class="fancybox" data-fancybox="RGMCODE">
                                                                <img src="{{ url('/uploads/ean-images/' . $imagen_product_ean->first()->image_ean) }}"
                                                                    width="50px" height="50px" />
                                                            </a>


                                                            @foreach ($imagen_product_ean as $p)
                                                                @if (!$loop->first)
                                                                    <a href="{{ '/uploads/ean-images/' . $p->image_ean }}"
                                                                        data-fancybox="RGMCODE">
                                                                        <img src="{{ url('/uploads/ean-images/' . $p->image_ean) }}"
                                                                            width="0px" height="0px"
                                                                            style="position:absolute;" />
                                                                    </a>
                                                                @endif
                                                            @endforeach


                                                        </td>
                                                    @else
                                                        <td class="product-price">
                                                            <font color="black">
                                                                <i class="fa fa-times" data-toggle="popover"
                                                                    data-content="No Posee" data-placement="top"
                                                                    data-trigger="hover"></i>
                                                            </font>
                                                        </td>
                                                    @endif

                                                    <td>
                                                        @if ($product_request->comments == '')
                                                            <center>
                                                                <font color="black">
                                                                    <i class="fa fa-times" data-toggle="popover"
                                                                        data-content="No Posee" data-placement="top"
                                                                        data-trigger="hover"></i>
                                                                </font>
                                                            </center>
                                                        @else
                                                            <center>
                                                                <font color="black">
                                                                    <i class="fa fa-comment" data-toggle="popover"
                                                                        data-content="{{ $product_request->comments }}"
                                                                        data-placement="top" data-trigger="hover"></i>
                                                                </font>
                                                            </center>
                                                        @endif
                                                    </td>

                                                    @if ($product_request->in_collection == 'Y')
                                                        <td>
                                                            <font color="green"> <span
                                                                    class="text-xs text-right retro font-weight-bold color-primary small text-nowrap">
                                                                    Este producto esta para colección
                                                                </span> </font>
                                                        </td>
                                                    @else
                                                        <td>
                                                            <font color="green"> <span
                                                                    class="text-xs text-right retro font-weight-bold color-primary small text-nowrap">
                                                                    {{ number_format($product_request->price, 2) }}
                                                                    € </span> </font>
                                                        </td>
                                                    @endif

                                                    <td class="text-base text-gray-900">

                                                        {{ $product_request->quantity }}

                                                    </td>

                                                </tr>

                                            </tbody>
                                        </table>
                                    @elseif($product_request->category == 'Consolas')
                                        <table id="table-two-axis" class="two-axis">
                                            <thead>
                                                <tr class="border-b border-gray-500 hover:bg-gray-200">
                                                    <th class="text-xs">PRODUCTO</th>
                                                    <th class="text-xs">PLATAFORMA</th>
                                                    <th class="text-xs">REGION</th>
                                                    <th class="text-xs">CAJA</th>
                                                    <th class="text-xs">ESTADO</th>
                                                    <th class="text-xs">MANUAL</th>
                                                    <th class="text-xs">CONSOLA</th>
                                                    <th class="text-xs">EXTRA</th>
                                                    <th class="text-xs">CABLES</th>
                                                    <th class="text-xs"><i class="fa fa-image"></i></th>
                                                    <th class="text-xs"> <i class="fa fa-barcode" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="text-xs"><i class="fa fa-comment"></i></th>
                                                    <th class="text-xs">PRECIO</th>
                                                    <th class="text-xs">STOCK</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <tr class="border-b border-gray-500 hover:bg-gray-200">
                                                    <td class="text-sm font-semibold text-gray-900">
                                                        {{ $product_request->product }} </td>
                                                    <td class="text-sm font-semibold text-gray-900">
                                                        {{ $product_request->platform }} </td>
                                                    <td class="text-sm font-semibold text-gray-900">
                                                        {{ $product_request->region }} </td>
                                                    <td class="text-sm font-semibold text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $product_request->box }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($product_request->box_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    <td class="text-base text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $product_request->cover }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($product_request->cover_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    <td class="text-base text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $product_request->manual }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($product_request->manual_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    <td class="text-base text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $product_request->game }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($product_request->game_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    <td class="text-base text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $product_request->extra }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($product_request->extra_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    <td class="text-base text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $product_request->inside }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($product_request->inside_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    @if ($imagen_product->first())
                                                        <td class="text-center product-cart-img item">

                                                            <a href="{{ url('/uploads/inventory-images/' . $imagen_product->first()->image_path) }}"
                                                                class="fancybox" data-fancybox="RGM">
                                                                <img src="{{ url('/uploads/inventory-images/' . $imagen_product->first()->image_path) }}"
                                                                    width="50px" height="50px" />
                                                            </a>


                                                            @foreach ($imagen_product as $p)
                                                                @if (!$loop->first)
                                                                    <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                        data-fancybox="RGM">
                                                                        <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                            width="0px" height="0px"
                                                                            style="position:absolute;" />
                                                                    </a>
                                                                @endif
                                                            @endforeach


                                                        </td>
                                                    @else
                                                        <td class="product-price">
                                                            <font color="black">
                                                                <i class="fa fa-times" data-toggle="popover"
                                                                    data-content="No Posee" data-placement="top"
                                                                    data-trigger="hover"></i>
                                                            </font>
                                                        </td>
                                                    @endif


                                                    @if ($imagen_product_ean->first())
                                                        <td class="text-center product-cart-img item">

                                                            <a href="{{ url('/uploads/ean-images/' . $imagen_product_ean->first()->image_ean) }}"
                                                                class="fancybox" data-fancybox="RGMCODE">
                                                                <img src="{{ url('/uploads/ean-images/' . $imagen_product_ean->first()->image_ean) }}"
                                                                    width="50px" height="50px" />
                                                            </a>


                                                            @foreach ($imagen_product_ean as $p)
                                                                @if (!$loop->first)
                                                                    <a href="{{ '/uploads/ean-images/' . $p->image_ean }}"
                                                                        data-fancybox="RGMCODE">
                                                                        <img src="{{ url('/uploads/ean-images/' . $p->image_ean) }}"
                                                                            width="0px" height="0px"
                                                                            style="position:absolute;" />
                                                                    </a>
                                                                @endif
                                                            @endforeach


                                                        </td>
                                                    @else
                                                        <td class="product-price">
                                                            <font color="black">
                                                                <i class="fa fa-times" data-toggle="popover"
                                                                    data-content="No Posee" data-placement="top"
                                                                    data-trigger="hover"></i>
                                                            </font>
                                                        </td>
                                                    @endif

                                                    <td>
                                                        @if ($product_request->comments == '')
                                                            <center>
                                                                <font color="black">
                                                                    <i class="fa fa-times" data-toggle="popover"
                                                                        data-content="No Posee" data-placement="top"
                                                                        data-trigger="hover"></i>
                                                                </font>
                                                            </center>
                                                        @else
                                                            <center>
                                                                <font color="black">
                                                                    <i class="fa fa-comment" data-toggle="popover"
                                                                        data-content="{{ $product_request->comments }}"
                                                                        data-placement="top" data-trigger="hover"></i>
                                                                </font>
                                                            </center>
                                                        @endif
                                                    </td>

                                                    @if ($product_request->in_collection == 'Y')
                                                        <td>
                                                            <font color="green"> <span
                                                                    class="text-xs text-right retro font-weight-bold color-primary small text-nowrap">
                                                                    Este producto esta para colección
                                                                </span> </font>
                                                        </td>
                                                    @else
                                                        <td>
                                                            <font color="green"> <span
                                                                    class="text-xs text-right retro font-weight-bold color-primary small text-nowrap">
                                                                    {{ number_format($product_request->price, 2) }}
                                                                    € </span> </font>
                                                        </td>
                                                    @endif

                                                    <td class="text-base text-gray-900">

                                                        {{ $product_request->quantity }}

                                                    </td>

                                                </tr>

                                            </tbody>
                                        </table>
                                    @else
                                        <table id="table-two-axis" class="two-axis">
                                            <thead>
                                                <tr class="border-b border-gray-500 hover:bg-gray-200">
                                                    <th class="text-xs">PRODUCTO</th>
                                                    <th class="text-xs">PLATAFORMA</th>
                                                    <th class="text-xs">REGION</th>
                                                    <th class="text-xs">CAJA</th>
                                                    <th class="text-xs">ESTADO</th>
                                                    <th class="text-xs">EXTRA</th>
                                                    <th class="text-xs"><i class="fa fa-image"></i></th>
                                                    <th class="text-xs"> <i class="fa fa-barcode" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="text-xs"><i class="fa fa-comment"></i></th>
                                                    <th class="text-xs">PRECIO</th>
                                                    <th class="text-xs">STOCK</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <tr class="border-b border-gray-500 hover:bg-gray-200">
                                                    <td class="text-sm font-semibold text-gray-900">
                                                        {{ $product_request->product }} </td>
                                                    <td class="text-sm font-semibold text-gray-900">
                                                        {{ $product_request->platform }} </td>
                                                    <td class="text-sm font-semibold text-gray-900">
                                                        {{ $product_request->region }} </td>
                                                    <td class="text-sm font-semibold text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $product_request->box }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($product_request->box_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    <td class="text-base text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $product_request->game }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($product_request->game_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    <td class="text-base text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $product_request->extra }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($product_request->extra_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    @if ($imagen_product->first())
                                                        <td class="text-center product-cart-img item">

                                                            <a href="{{ url('/uploads/inventory-images/' . $imagen_product->first()->image_path) }}"
                                                                class="fancybox" data-fancybox="RGM">
                                                                <img src="{{ url('/uploads/inventory-images/' . $imagen_product->first()->image_path) }}"
                                                                    width="50px" height="50px" />
                                                            </a>


                                                            @foreach ($imagen_product as $p)
                                                                @if (!$loop->first)
                                                                    <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                        data-fancybox="RGM">
                                                                        <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                            width="0px" height="0px"
                                                                            style="position:absolute;" />
                                                                    </a>
                                                                @endif
                                                            @endforeach


                                                        </td>
                                                    @else
                                                        <td class="product-price">
                                                            <font color="black">
                                                                <i class="fa fa-times" data-toggle="popover"
                                                                    data-content="No Posee" data-placement="top"
                                                                    data-trigger="hover"></i>
                                                            </font>
                                                        </td>
                                                    @endif


                                                    @if ($imagen_product_ean->first())
                                                        <td class="text-center product-cart-img item">

                                                            <a href="{{ url('/uploads/ean-images/' . $imagen_product_ean->first()->image_ean) }}"
                                                                class="fancybox" data-fancybox="RGMCODE">
                                                                <img src="{{ url('/uploads/ean-images/' . $imagen_product_ean->first()->image_ean) }}"
                                                                    width="50px" height="50px" />
                                                            </a>


                                                            @foreach ($imagen_product_ean as $p)
                                                                @if (!$loop->first)
                                                                    <a href="{{ '/uploads/ean-images/' . $p->image_ean }}"
                                                                        data-fancybox="RGMCODE">
                                                                        <img src="{{ url('/uploads/ean-images/' . $p->image_ean) }}"
                                                                            width="0px" height="0px"
                                                                            style="position:absolute;" />
                                                                    </a>
                                                                @endif
                                                            @endforeach


                                                        </td>
                                                    @else
                                                        <td class="product-price">
                                                            <font color="black">
                                                                <i class="fa fa-times" data-toggle="popover"
                                                                    data-content="No Posee" data-placement="top"
                                                                    data-trigger="hover"></i>
                                                            </font>
                                                        </td>
                                                    @endif

                                                    <td>
                                                        @if ($product_request->comments == '')
                                                            <center>
                                                                <font color="black">
                                                                    <i class="fa fa-times" data-toggle="popover"
                                                                        data-content="No Posee" data-placement="top"
                                                                        data-trigger="hover"></i>
                                                                </font>
                                                            </center>
                                                        @else
                                                            <center>
                                                                <font color="black">
                                                                    <i class="fa fa-comment" data-toggle="popover"
                                                                        data-content="{{ $product_request->comments }}"
                                                                        data-placement="top" data-trigger="hover"></i>
                                                                </font>
                                                            </center>
                                                        @endif
                                                    </td>

                                                    @if ($product_request->in_collection == 'Y')
                                                        <td>
                                                            <font color="green"> <span
                                                                    class="text-xs text-right retro font-weight-bold color-primary small text-nowrap">
                                                                    Este producto esta para colección
                                                                </span> </font>
                                                        </td>
                                                    @else
                                                        <td>
                                                            <font color="green"> <span
                                                                    class="text-xs text-right retro font-weight-bold color-primary small text-nowrap">
                                                                    {{ number_format($product_request->price, 2) }}
                                                                    € </span> </font>
                                                        </td>
                                                    @endif

                                                    <td class="text-base text-gray-900">

                                                        {{ $product_request->quantity }}

                                                    </td>

                                                </tr>

                                            </tbody>
                                        </table>
                                    @endif
                                </div>

                            </div>

                            <br>
                            <center><span class="retro text-base font-bold">¿EL PRODUCTO EXISTE?</span></center>

                            <select class="form-control show-tick ms" required id="type">
                                <option value="" selected>Selecciona...
                                </option>
                                <option value="yes">Si,Existe</option>
                                <option value="no">No,No Existe</option>
                            </select>

                            <br><br><br>


                            <div id="target">
                                <div id="yes">
                                    <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase"
                                        for="grid-city">
                                        ¿A que producto se refiere el usuario?
                                    </h5>

                                    @if ($product_request->auction == 'Y')
                                        <form
                                            action="{{ route('update_product_request_exist_auction', ['id' => $product_request->id]) }}"
                                            method="POST" enctype="multipart/form-data">
                                            @csrf
                                    @endif

                                    @if ($product_request->in_collection == 'Y')
                                        <form
                                            action="{{ route('update_exist_collection', ['id' => $product_request->id]) }}"
                                            method="POST" enctype="multipart/form-data">
                                            @csrf
                                        @else
                                            <form
                                                action="{{ route('update_product_request_exist', ['id' => $product_request->id]) }}"
                                                method="POST" enctype="multipart/form-data">
                                                @csrf
                                    @endif
                                    <input name="user_id" type="hidden" value="{{ $product_request->user_id }}">
                                    <input name="eee" type="hidden" value="{{ $product_request->email }}">
                                    <input name="box" type="hidden" value="{{ $product_request->box_condition }}">
                                    <input name="manual" type="hidden"
                                        value="{{ $product_request->manual_condition }}">
                                    <input name="cover" type="hidden"
                                        value="{{ $product_request->cover_condition }}">
                                    <input name="game" type="hidden" value="{{ $product_request->game_condition }}">
                                    <input name="extra" type="hidden"
                                        value="{{ $product_request->extra_condition }}">
                                    <input name="inside" type="hidden"
                                        value="{{ $product_request->inside_condition }}">
                                    <input name="quantity" type="hidden" value="{{ $product_request->quantity }}">
                                    <input name="price" type="hidden" value="{{ $product_request->price }}">
                                    <input name="description" type="hidden" value="{{ $product_request->comments }}">

                                    @foreach ($imagen_product as $p)
                                        <input name="image_inventory[]" type="hidden" value="{{ $p->image_path }}">
                                    @endforeach
                                    @foreach ($imagen_product_ean as $p)
                                        <input name="image_inventory_ean[]" type="hidden" value="{{ $p->image_path }}">
                                    @endforeach
                                    <section class="col-md-12">

                                        <select name="product" id="selProduct" style="width: 200px;" required>
                                            <option value="0">-- @lang('messages.search_product') --</option>
                                        </select>


                                    </section>
                                    <section class="text-center form-group col-md-12">
                                        <br>

                                        @if ($product_request->auction == 'Y')
                                            <input type="submit" id="aprobarBtn"
                                                class="btn confirmclosed text-white font-bold w-100 bg-red-800"
                                                value="Aprobar Oferta" disabled>
                                        @else
                                            <input type="submit" id="aprobarBtn"
                                                class="btn confirmclosed text-white font-bold w-100 bg-red-800"
                                                value="Aprobar" disabled>
                                        @endif
                                    </section>
                                    </form>
                                </div>
                                <div id="no">
                                    <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase"
                                        for="grid-city">
                                        Detalles del producto solicitado
                                    </h5>
                                    <br>

                                    @if ($product_request->auction == 'Y')
                                        <form
                                            action="{{ route('update_product_request_auction', ['id' => $product_request->id]) }}"
                                            method="POST" enctype="multipart/form-data">
                                            @csrf
                                    @endif

                                    @if ($product_request->in_collection == 'Y')
                                        <form
                                            action="{{ route('update_collection_request', ['id' => $product_request->id]) }}"
                                            method="POST" enctype="multipart/form-data">
                                            @csrf
                                        @else
                                            <form
                                                action="{{ route('update_product_request', ['id' => $product_request->id]) }}"
                                                method="POST" enctype="multipart/form-data">
                                                @csrf
                                    @endif


                                    <input name="user_id" type="hidden" value="{{ $product_request->user_id }}">
                                    <input name="box" type="hidden" value="{{ $product_request->box_condition }}">
                                    <input name="manual" type="hidden"
                                        value="{{ $product_request->manual_condition }}">
                                    <input name="cover" type="hidden"
                                        value="{{ $product_request->cover_condition }}">
                                    <input name="game" type="hidden"
                                        value="{{ $product_request->game_condition }}">
                                    <input name="extra" type="hidden"
                                        value="{{ $product_request->extra_condition }}">
                                    <input name="inside" type="hidden"
                                        value="{{ $product_request->inside_condition }}">
                                    <input name="quantity" type="hidden" value="{{ $product_request->quantity }}">
                                    <input name="price" type="hidden" value="{{ $product_request->price }}">
                                    <input name="description" type="hidden" value="{{ $product_request->comments }}">

                                    @foreach ($imagen_product as $p)
                                        <input name="image_inventory[]" type="hidden" value="{{ $p->image_path }}">
                                    @endforeach

                                    @foreach ($imagen_product_ean as $p)
                                        <input name="image_inventory_ean[]" type="hidden"
                                            value="{{ $p->image_path }}">
                                    @endforeach

                                    <section class="flex flex-wrap mb-2 -mx-3">

                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                                            <label for="name"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">@lang('messages.name')
                                                <font color="red">*</font>
                                            </label>
                                            <input type="text" name="name"
                                                value="{{ $product_request->product }}"
                                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="Retro" required>

                                        </section>
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                                            <label for="platform"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">@lang('messages.platform')
                                                <font color="red">*</font>
                                            </label>

                                            <section class="relative">
                                                <select style="width: 100%"
                                                    class="box-border absolute z-50 block float-left w-auto bg-white border-2 border-gray-600 border-solid"
                                                    id="platform" name="platform">
                                                    <option value="">@lang('messages.selects')
                                                    </option>
                                                    @foreach ($platform as $p)
                                                        <option value="{{ $p->value }}">
                                                            {{ $p->value }}</option>
                                                    @endforeach
                                                </select>
                                            </section>
                                        </section>
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                                            <label for="platform"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Región
                                                <font color="red">*</font>
                                            </label>

                                            <section class="relative">
                                                <select style="width: 100%"
                                                    class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                                    id="region" name="region">
                                                    <option value="">@lang('messages.selects')
                                                    </option>
                                                    @foreach ($region as $r)
                                                        <option value="{{ $r->value }}">
                                                            {{ $r->value }}</option>
                                                    @endforeach
                                                </select>
                                            </section>
                                        </section>
                                    </section>

                                    <br>

                                    <section class="flex flex-wrap mb-2 -mx-3">
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                                            <label for="name_alt"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nombre
                                                - Alternativo <font color="red">*</font></label>
                                            <input type="text" name="name_en"
                                                value="{{ $product_request->product }}"
                                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="RGM" required>


                                        </section>
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                                            <label for="category"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Categoria
                                                <font color="red">*</font>
                                            </label>

                                            <section class="relative">
                                                <select style="width: 100%"
                                                    class="box-border absolute z-50 block float-left w-auto bg-white border-2 border-gray-600 border-solid"
                                                    id="grid" name="prod_category_id" required="">
                                                    <option value="">@lang('messages.selects')</option>
                                                    @foreach ($categories as $c)
                                                        @if (!in_array($c->category_text, ['Juegos', 'Consolas', 'Accesorios', 'Periféricos']))
                                                            <option value="{{ $c->id }}">{{ $c->category_text }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </section>
                                        </section>
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label for="release_date"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Fecha
                                                de Lanzamiento <font color="red">(Opcional)</font></label>
                                            <input type="text" id="datepicker" name="release_date"
                                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="14/03/2023">
                                        </section>
                                    </section>

                                    <br>

                                    <section class="flex flex-wrap mb-2 -mx-3">
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label for="media"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Media
                                                <font color="red">*</font>
                                            </label>
                                            <section class="relative">
                                                <select style="width: 100%"
                                                    class="box-border absolute z-50 block float-left w-auto bg-white border-2 border-gray-600 border-solid"
                                                    id="media" name="media" required="">
                                                    <option value="">@lang('messages.selects')
                                                    </option>
                                                    @foreach ($media as $m)
                                                        <option value="{{ $m->value }}">
                                                            {{ $m->value }}</option>
                                                    @endforeach
                                                </select>
                                            </section>
                                        </section>
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label for="gener"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Genero
                                                <font color="red">(Opcional)</font>
                                            </label>
                                            <section class="relative">
                                                <select style="width: 100%"
                                                    class="box-border absolute z-50 block float-left w-auto bg-white border-2 border-gray-600 border-solid"
                                                    id="genero" name="game_category">
                                                    <option value="">@lang('messages.selects')
                                                    </option>
                                                    @foreach ($genero as $g)
                                                        <option value="{{ $g->value }}">
                                                            {{ $g->value }}</option>
                                                    @endforeach
                                                </select>
                                            </section>
                                        </section>
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label for="location"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Localización
                                                <font color="red">*</font>
                                            </label>

                                            <section class="relative">
                                                <select style="width: 100%"
                                                    class="box-border absolute z-50 block float-left w-auto bg-white border-2 border-gray-600 border-solid"
                                                    id="location" name="game_location">
                                                    <option value="">@lang('messages.selects')
                                                    </option>
                                                    @foreach ($location as $l)
                                                        <option value="{{ $l->value }}">
                                                            {{ $l->value }}</option>
                                                    @endforeach
                                                </select>
                                            </section>
                                        </section>
                                    </section>



                                    <br>


                                    <section class="flex flex-wrap mb-2 -mx-3">
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label for="generation"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Generación
                                                <font color="red">(Opcional)</font>
                                            </label>
                                            <section class="relative">
                                                <select style="width: 100%"
                                                    class="box-border absolute z-50 block float-left w-auto bg-white border-2 border-gray-600 border-solid"
                                                    id="generation" name="media">
                                                    <option value="">@lang('messages.selects')
                                                    </option>
                                                    @foreach ($generation as $ge)
                                                        <option value="{{ $ge->value }}">
                                                            {{ $ge->value }}</option>
                                                    @endforeach
                                                </select>
                                            </section>
                                        </section>
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label for="language"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Lenguaje
                                                <font color="red">*</font>
                                            </label>
                                            <section class="relative">
                                                <select style="width: 100%"
                                                    class="box-border absolute z-50 block float-left w-auto bg-white border-2 border-gray-600 border-solid"
                                                    id="language" name="language[]" multiple="multiple" required="">
                                                    @foreach ($language as $la)
                                                        <option value="{{ $la->value }}">
                                                            {{ $la->value }}</option>
                                                    @endforeach
                                                </select>
                                            </section>
                                        </section>
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label for="box_language"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Lenguaje
                                                de caja
                                                <font color="red">(Opcional)</font>
                                            </label>
                                            <section class="relative">
                                                <select style="width: 100%"
                                                    class="box-border absolute z-50 block float-left w-auto bg-white border-2 border-gray-600 border-solid"
                                                    id="box_language" name="box_language[]" multiple="multiple">
                                                    <option value="">@lang('messages.selects')
                                                    </option>
                                                    @foreach ($language as $la)
                                                        <option value="{{ $la->value }}">
                                                            {{ $la->value }}</option>
                                                    @endforeach
                                                </select>
                                            </section>
                                        </section>
                                    </section>




                                    <br>


                                    <section class="flex flex-wrap mb-2 -mx-3">
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label for="ean_upc"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">EAN
                                                / UPC
                                                <font color="red">(Opcional)</font>
                                            </label>
                                            <input type="text" name="ean_upc"
                                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="010203">
                                        </section>
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label for="volume"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Volume
                                                <font color="red">*</font>
                                            </label>
                                            <input type="number" step="0.0001" name="volume"
                                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="0.0001" required="">
                                        </section>
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                                            <label for="volume"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Peso
                                                <font color="red">*</font>
                                            </label>
                                            <input type="text" step="0.0001" name="weight"
                                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="KG" required="">
                                        </section>
                                    </section>

                                    <br>


                                    <section class="flex flex-wrap mb-2 -mx-3">
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                                            <label for="large"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Largo
                                                <font color="red">*</font>
                                            </label>

                                            <input type="number" name="large"
                                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="10" required="">

                                        </section>
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                                            <label for="width"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Ancho
                                                <font color="red">*</font>
                                            </label>

                                            <input type="number" step="0.0001" name="width"
                                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="20" required="">

                                        </section>
                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                                            <label for="high"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Alto
                                                <font color="red">*</font>
                                            </label>

                                            <input type="number" name="high"
                                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="30" required="">

                                        </section>
                                    </section>

                                    <br>

                                    <section class="flex flex-wrap mb-2 -mx-3">

                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                                            <label for="price"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Precio
                                                <font color="#A93226">solo</font>
                                            </label>

                                            <input type="number" autocomplete="off" data-number="2" step="0.01"
                                                name="price_solo"
                                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="0,00" required="">


                                        </section>

                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                                            <label for="price"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Precio
                                                <font color="#B7950B">usado</font>
                                            </label>

                                            <input type="number" autocomplete="off" data-number="2" step="0.01"
                                                name="price_used"
                                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="0,00" required="">


                                        </section>

                                        <section class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                                            <label for="price"
                                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Precio
                                                <font color="#1E8449">nuevo</font>
                                            </label>

                                            <input type="number" autocomplete="off" data-number="2" step="0.01"
                                                name="price_new"
                                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="0,00" required="">

                                        </section>

                                    </section>


                                    <label for="input-res-1">Agregar Imagen del Producto
                                        (Opcional)</label>
                                    <section class="file-loading">
                                        <input id="input-20" name="image_path[]" type="file"
                                            data-browse-on-zone-click="true" multiple="multiple">
                                    </section>
                                    <br>
                                    <section class="form-group col-sm-12">
                                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                            for="grid-city">
                                            Descripcion (Opcional)
                                        </label>
                                        <textarea id="comments" name="comments" cols="20" rows="10" class="form-control"
                                            placeholder="Añada un comentario con respecto al producto"></textarea>
                                    </section>

                                    <section class="text-center form-group col-md-12">
                                        <br>

                                        @if ($product_request->auction == 'Y')
                                            <input type="submit"
                                                class="btn confirmclosed text-white font-bold w-100 bg-red-700"
                                                value="Aprobar Oferta">
                                        @else
                                            <input type="submit"
                                                class="btn confirmclosed text-white font-bold w-100 bg-red-700"
                                                value="Aprobar">
                                        @endif


                                    </section>

                                    </form>
                                </div>
                            </div>

                        </div>
                        <div class="search_product panel">
                            <livewire:admin.product-table></livewire:admin.product-table>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    </body>




@stop


@section('js')
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" crossorigin="anonymous">
    </script>
    <script src="{{ asset('plugins/fileinput/js/fileinput.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/quagga/0.12.1/quagga.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script>
        const tabs = document.querySelectorAll(".tabs");
        const tab = document.querySelectorAll(".tab");
        const panel = document.querySelectorAll(".panel");

        function onTabClick(event) {

            // deactivate existing active tabs and panel

            for (let i = 0; i < tab.length; i++) {
                tab[i].classList.remove("active");
            }

            for (let i = 0; i < panel.length; i++) {
                panel[i].classList.remove("active");
            }


            // activate new tabs and panel
            event.target.classList.add('active');
            let classString = event.target.getAttribute('data-target');
            console.log(classString);
            document.getElementById('panels').getElementsByClassName(classString)[0].classList.add("active");
        }

        for (let i = 0; i < tab.length; i++) {
            tab[i].addEventListener('click', onTabClick, false);
        }
    </script>
    <script>
        $(document).ready(function() {
            //hide all in target div
            $("div", "div#target").hide();
            $("select#type").change(function() {
                // hide previously shown in target div
                $("div", "div#target").hide();

                // read id from your select
                var value = $(this).val();

                // show rlrment with selected id
                $("div#" + value).show();
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            // Inicializa el campo select con Select2
            $("#selProduct").select2();

            // Escucha el evento de cambio en el campo select
            $("#selProduct").on("change", function() {
                // Verifica si se ha seleccionado una opción válida
                if ($(this).val() !== "0") {
                    // Habilita el botón "Aprobar"
                    $("#aprobarBtn").prop("disabled", false);
                } else {
                    // Deshabilita el botón "Aprobar"
                    $("#aprobarBtn").prop("disabled", true);
                }
            });
        });
    </script>
    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#selProduct").select2({
                minimumInputLength: 3,
                width: '100%',

                ajax: {
                    url: "{{ route('SearchbyProduct') }}",
                    type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            _token: CSRF_TOKEN,
                            search_product: params.term // search term
                        };
                    },
                    processResults: function(response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }

            });

        });
    </script>
    <script>
        $(document).ready(function() {
            $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.comments').popover();
            $('[data-toggle="popover"]').popover();
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#grid').select2();
            $('#genero').select2();
            $('#location').select2();
            $('#generation').select2();
            $('#language').select2();
            $('#box_language').select2();
            $('#media').select2();
            $('#platform').select2();
            $('#region').select2();
            $("#datepicker").datepicker();
        });
    </script>
    @livewireScripts
@stop
