@extends('adminlte::page')

@section('title', 'Admin-Dashboard')
@section('plugins.FileInput', true)
@section('plugins.Select2', true)

@section('content_header')
    <h1>Editar Usuario - {{ $user->user_name }}</h1>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nes/nes_prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <!-- Default CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/default.css') }}">


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">

    @yield('content-css-include')

    <!-- Fancybox CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">

    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <style>
        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Press Start 2P', cursive;
        }

    </style>


    @livewireStyles
@stop

@section('content')

    <div>

        <div class="row">

            <div class="col-md-12" style="background-color: #ffffff; border: solid 1px #db2e2e">

                <br>
                <form action="{{ $viewData['form_url_post'] }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="flex flex-wrap mb-2 -mx-3">
                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                            <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                for="grid-city">
                                Nombre
                            </label>
                            <input
                                class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                id="br_c_first_name" value="{{ $user->first_name }}" name="br_c_first_name" type="text"
                                placeholder="Mario Bros" required="">
                        </div>

                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                            <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="grid-zip">
                                Apellido
                            </label>
                            <input
                                class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                id="br_c_last_name" value="{{ $user->last_name }}" name="br_c_last_name" type="text"
                                placeholder="Super Mario" required="">
                        </div>

                        <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                            <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                for="grid-zip">
                                Email
                            </label>
                            <input
                                class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                id="br_c_email" value="{{ $user->email }}" name="br_c_email" type="email"
                                placeholder="Release Date">
                        </div>
                    </div>
                    <br>
                    <div class="flex flex-wrap mb-6 -mx-3">
                        <div class="w-full px-3">
                            <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                for="grid-password">
                                Nombre de Usuario
                            </label>
                            <input
                                class="block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                id="br_c_user_name" name="br_c_user_name" value="{{ $user->user_name }}" type="text">
                        </div>
                    </div>
                    <div class="flex flex-wrap mb-6 -mx-3">
                        <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                            <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                for="grid-first-name">
                                Contraseña
                            </label>
                            <input
                                class="block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border border-red-500 rounded appearance-none focus:outline-none focus:bg-white"
                                id="br_c_password" name="br_c_password" type="password" placeholder="****">

                        </div>
                        <div class="w-full px-3 md:w-1/2">
                            <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                for="grid-last-name">
                                Confirmar Contraseña
                            </label>
                            <input
                                class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                id="br_c_password_confirmation" name="br_c_password_confirmation" type="password"
                                placeholder="****">
                        </div>
                    </div>

                    <div class="flex flex-wrap mb-6 -mx-3">
                        <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                            <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                for="grid-first-name">
                                Habilitar
                            </label>
                            <select
                                class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                id="br_c_is_enabled" name="br_c_is_enabled" required="">
                                <option value="{{ $user->is_enabled }}">{{ $user->is_enabled }}</option>
                                <option value="Y">Si</option>
                                <option value="N">No</option>
                            </select>
                        </div>
                        <div class="w-full px-3 md:w-1/2">
                            <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                                for="grid-last-name">
                                Activado
                            </label>
                            <select
                                class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                id="br_c_is_activated" name="br_c_is_activated" required="">
                                <option value="{{ $user->is_activated }}">{{ $user->is_activated }}</option>
                                <option value="Y">Si</option>
                                <option value="N">No</option>
                            </select>
                        </div>
                    </div>
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="grid-first-name">
                        - Roles -
                    </label>
                    @include('brcode.layout.app_form_sub')
                    <div class="text-center form-group row col-md-12">
                        <br>
                        <input type="submit" class="nes-btn is-warning retro btn btn-warning" value="Actualizar">
                        <input type="hidden" name="model_id"
                            value="{{ $viewData['model_id'] > 0 ? $viewData['model_id'] : '' }}" />
                    </div>
                </form>

            </div>
        </div>
    </div>


@stop


@section('js')

    <script>
        $(document).ready(function() {
            $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#grid').select2();
            $('#is_activated').select2();
            $('#rol').select2();
        });
    </script>

    @livewireScripts
@stop
