@extends('adminlte::page')

@section('title', 'Admin-Dashboard')
@section('plugins.DatePicker', true)
@section('plugins.JqueryDatatable', true)

@section('content_header')
 
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nes/nes_prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <!-- Default CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/default.css') }}">

    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">

    @yield('content-css-include')

    <!-- Fancybox CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">


    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <style>
        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Press Start 2P', cursive;
        }

    </style>


    @livewireStyles
@stop

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-red elevation-1"><i class="fas fa-users"></i></span>

                        <div class="info-box-content">
                            <span class="font-bold info-red-text">Total</span>
                            <span class="info-box-number retro">
                                {{ $user->count() }}
                            </span>
                        </div>

                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->

                </div>

            </div>
            <span class="retro">@include('partials.flash')</span>


            <div class="row">


                <div class="col-sm-12">
                    <div class="grid grid-cols-1 gap-6 p-6 bg-white border border-gray-300 rounded-lg shadow-lg">
                        <div class="label alert-success">
                            Mes actual
                            <strong class="text-center">
                                (@php $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                                echo $meses[date('n')-1]; @endphp)
                            </strong>
                        </div>
                        <div class="grid grid-cols-1 gap-4 md:grid-cols-2">

                            <div class="grid gap-2 p-2 border border-gray-200 rounded">
                                <form action="{{ route('filter_month') }}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="flex items-center p-2 bg-gray-300 border rounded ">
                                        <svg class="w-5 mr-2 text-gray-800 fill-current" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 24 24" width="24" height="24">
                                            <path class="heroicon-ui"
                                                d="M12 12a5 5 0 1 1 0-10 5 5 0 0 1 0 10zm0-2a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm9 11a1 1 0 0 1-2 0v-2a3 3 0 0 0-3-3H8a3 3 0 0 0-3 3v2a1 1 0 0 1-2 0v-2a5 5 0 0 1 5-5h8a5 5 0 0 1 5 5v2z" />
                                        </svg>
                                        <select onchange="habilitar(this)" id="mes" name="mes"
                                            class="text-gray-700 bg-gray-300 focus:outline-none form-control">
                                            <option value="">Filter by Month Or...</option>
                                            <option value="1">Enero</option>
                                            <option value="2">Febrero</option>
                                            <option value="3">Marzo</option>
                                            <option value="4">Abril</option>
                                            <option value="5">Mayo</option>
                                            <option value="6">Junio</option>
                                            <option value="7">Julio</option>
                                            <option value="8">Agosto</option>
                                            <option value="9">Septiembre</option>
                                            <option value="10">Octubre</option>
                                            <option value="11">Noviembre</option>
                                            <option value="12">Diciembre</option>
                                        </select>
                                    </div>
                            </div>
                            <div class="grid grid-cols-2 gap-2 p-2 border border-gray-200 rounded">
                                <div class="flex items-center p-2 bg-gray-300 border rounded ">
                                    <svg class="w-5 mr-2 text-gray-800 fill-current" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 24 24" width="24" height="24">
                                        <path class="heroicon-ui"
                                            d="M14 5.62l-4 2v10.76l4-2V5.62zm2 0v10.76l4 2V7.62l-4-2zm-8 2l-4-2v10.76l4 2V7.62zm7 10.5L9.45 20.9a1 1 0 0 1-.9 0l-6-3A1 1 0 0 1 2 17V4a1 1 0 0 1 1.45-.9L9 5.89l5.55-2.77a1 1 0 0 1 .9 0l6 3A1 1 0 0 1 22 7v13a1 1 0 0 1-1.45.89L15 18.12z" />
                                    </svg>
                                    <input type="text" id="from" name="desde" placeholder="Desde - From"
                                        class="max-w-full text-gray-700 bg-gray-300 form control fe focus:outline-none" />
                                </div>
                                <div class="flex items-center p-2 bg-gray-300 border rounded ">
                                    <svg class="w-5 mr-2 text-gray-800 fill-current" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 24 24" width="24" height="24">
                                        <path class="heroicon-ui"
                                            d="M13.04 14.69l1.07-2.14a1 1 0 0 1 1.2-.5l6 2A1 1 0 0 1 22 15v5a2 2 0 0 1-2 2h-2A16 16 0 0 1 2 6V4c0-1.1.9-2 2-2h5a1 1 0 0 1 .95.68l2 6a1 1 0 0 1-.5 1.21L9.3 10.96a10.05 10.05 0 0 0 3.73 3.73zM8.28 4H4v2a14 14 0 0 0 14 14h2v-4.28l-4.5-1.5-1.12 2.26a1 1 0 0 1-1.3.46 12.04 12.04 0 0 1-6.02-6.01 1 1 0 0 1 .46-1.3l2.26-1.14L8.28 4zm7.43 5.7a1 1 0 1 1-1.42-1.4L18.6 4H16a1 1 0 0 1 0-2h5a1 1 0 0 1 1 1v5a1 1 0 0 1-2 0V5.41l-4.3 4.3z" />
                                    </svg>
                                    <input type="text" id="to" name="hasta" placeholder="Hasta - To"
                                        class="max-w-full text-gray-700 bg-gray-300 form control fe focus:outline-none" />
                                </div>
                            </div>
                        </div>
                        <div class="flex justify-center"><button type="submit"
                                class="w-1/4 p-2 text-white bg-gray-800 border rounded-md"><i class="fa fa-search"></i>
                                Buscar</button></div>
                    </div>
                    </form>
                    <br>
                </div>




            </div>


            <div class="clearfix row">
                <div class="col-lg-12">
                    <div class="card">
                        <br>
                        <div class="header">
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table
                                    class="table table-bordered table-striped br-datatable-withdrawal table-hover dataTable js-exportable">
                                    <thead>
                                        <tr class="text-sm leading-normal text-gray-100 uppercase bg-red-700">
                                            <th>Nombre Completo</th>
                                            <th>Email</th>
                                            <th>Usuario</th>
                                            <th>Habilitado</th>
                                            <th>Transferencia</th>
                                            <th>Cuenta Creada</th>
                                            <th>Saldo</th>
                                            <th style="width:5%">Accion</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Nombre Completo</th>
                                            <th>Email</th>
                                            <th>Usuario</th>
                                            <th>Habilitado</th>
                                            <th>Transferencia</th>
                                            <th>Cuenta Creada</th>
                                            <th>Saldo</th>
                                            <th style="width:5%">Accion</th>
                                        </tr>
                                    </tfoot>
                                    <tbody class="text-sm font-light text-gray-600">
                                        @foreach ($user as $u)
                                            <tr class="border-b border-gray-500 hover:bg-gray-200">
                                                <td class="px-6 py-4 whitespace-nowrap">
                                                    <div class="text-sm text-gray-900">{{ $u->first_name }} /
                                                        {{ $u->last_name }}</div>
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap">
                                                    <div class="text-sm text-gray-900">{{ $u->email }}</div>
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap">
                                                    <div class="text-sm text-gray-900">{{ $u->user_name }}</div>
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap">
                                                    <span
                                                        class="inline-flex px-2 text-xs font-semibold leading-5 text-green-800 bg-green-100 rounded-full">
                                                        @if ($u->is_enabled == 'Y')
                                                            Si
                                                        @else
                                                            No
                                                        @endif
                                                    </span>
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap">
                                                    <div class="text-sm text-gray-900">{{ $u->concept }}</div>
                                                </td>
                                                <td class="py-5 px-7 whitespace-nowrap">
                                                    <div class="text-sm text-gray-900">{{ date_format($u->created_at, 'Y-m-d') }}
                                                    </div>
                                                </td>
                                                <td class="py-5 px-7 whitespace-nowrap">
                                                    <div class="text-sm text-gray-900">{{ $u->getCashAttribute() }} €
                                                    </div>
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap">
                                                    <div class="row">
                                                        <a href="{{ route('edit_user', [$u->id]) }}"
                                                            class="btn btn-primary btn-sm" title="Editar"><i
                                                                class="fa fa-edit"></i></a>
                                                        <a href="#" data-toggle="modal" data-id="{{ $u->id }}"
                                                            data-cre="{{ $u->cash }}" data-option="add"
                                                            class="btn btn-xs btn-success" onclick="openAdd(this);"><i
                                                                class="fa fa-plus"></i> Credit </a>
                                                        <a href="#" class="btn btn-xs btn-warning" data-option="remove"
                                                            data-toggle="modal" data-id="{{ $u->id }}"
                                                            data-cre="{{ $u->cash }}" onclick="openRemove(this);"><i
                                                                class="fa fa-minus"></i> Credit </a>
                                                        <a href="{{ url('/11w5Cj9WDAjnzlg0/users/info', [$u->id]) }}"
                                                            class="btn btn-xs btn-info"><i class="fa fa-info"></i>
                                                            Info </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="modal fade" tabindex="-1" role="dialog" id="myAdd">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" action="{{ route('user_credit') }}">
                        {{ csrf_field() }}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">{{ __('Agregar Credito') }}</h4>
                        </div>

                        <div class="modal-body">
                            <br>
                            <div class="form-group" style="padding: 10px;">
                                <label for="credit">{{ __('Ingrese la cantidad de credito a agregar') }}</label>
                                <input type="number" name="credit" min="0" required step="0.01" class="form-control">
                            </div>
                            <div class="form-group" style="padding: 10px;">
                                <label for="pass">{{ __('Cotraseña') }}</label>
                                <input type="password" name="pass" class="form-control">
                            </div>
                            <input id="orderDetailsAdd" name="orderDetails" type="hidden">
                            <input name="transaction" type="hidden" value="1">
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('Cancelar') }}</button>
                            <button type="submit" class="btn btn-success">{{ __('Guardar') }}</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" tabindex="-1" role="dialog" id="myRemove">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" action="{{ route('user_credit') }}">
                        {{ csrf_field() }}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">{{ __('Retirar Credito') }}</h4>
                        </div>

                        <div class="modal-body">
                            <br>
                              <div class="form-group" style="padding: 10px;">
                                  <label for="credit">{{ __('Ingrese la cantidad de credito a retirar') }}</label>
                                  <input type="number" name="credit" min="0" required step="0.01"  class="form-control">
                                  <div id="foundRemove"></div>
                              </div>
                              <div class="form-group" style="padding: 10px;">
                                  <label for="pass">{{ __('Contraseña') }}</label>
                                  <input type="password" name="pass" class="form-control">
                              </div>
                              <input  id="orderDetailsRemove" name="orderDetails" type="hidden" >
                              <input name="transaction" type="hidden" value="0">
                       </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('Cancelar') }}</button>
                            <button type="submit" class="btn btn-success">{{ __('Guardar') }}</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </section>

@stop


@section('js')
    <script>
        $(function() {
            $("select").change(function() {
                if ($(this).val() > 0) {
                    $('#from').prop('disabled', true);
                    $('#to').prop('disabled', true);
                } else {
                    $("#from").off();
                    $('#from, #to').prop('disabled', false);
                }
            });
        });

        // -------- datapicker from, to
        $(function() {
            var dateFormat = "dd-mm-yy",

                from = $("#from")
                .datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1
                })
                .on("change", function() {
                    to.datepicker("option", "minDate", getDate(this));
                }),

                to = $("#to").datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1
                })
                .on("change", function() {
                    from.datepicker("option", "maxDate", getDate(this));
                });

            function getDate(element) {
                var date;
                try {
                    date = $.datepicker.parseDate(dateFormat, element.value);
                } catch (error) {
                    date = null;
                }

                return date;
            }
        });

        $("·fe").datepicker();
    </script>
    @livewireScripts
@stop
