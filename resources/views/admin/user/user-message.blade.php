@extends('adminlte::page')

@section('title', 'Admin-Dashboard')

@section('content_header')
    <center>
        <h3 class="retro font-extrabold">- Denuncias -</h3>
    </center>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nes/nes_prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <!-- Default CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/default.css') }}">


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
        integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous" />

    @yield('content-css-include')
    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
    <!-- Fancybox CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">


    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <style>
        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Jost', sans-serif;
            font-weight: 800;
        }

    </style>


    @livewireStyles
@stop

@section('content')

    <section class="content">
        <div class="container-fluid">

            <span class="retro">@include('partials.flash')</span>



            

        </div>


    </section>

    <footer class="relative bg-white border-red-700 footer">
        <div class="container px-6 mx-auto">
            <div class="mt-16 border-t-2 border-gray-300">
                <strong>Copyright &copy; 2021 <a href="https://retrogamingmarket.eu">Retrogamingmarket.eu</a>.</strong>
                Todos los derechos reservados.
                <div class="float-right d-none d-sm-inline-block">
                    <b>Version Beta</b> 2.0
                </div>
            </div>
        </div>
    </footer>

@stop


@section('js')

    @livewireScripts
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.15.4/js/all.js"
        integrity="sha384-rOA1PnstxnOBLzCLMcre8ybwbTmemjzdNlILg8O7z1lUkLXozs4DHonlDtnE7fpc" crossorigin="anonymous">
    </script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    $('.order-deny').submit(function(e) {
        e.preventDefault();
        Swal.fire({
            title: '<span class="text-base retro">AVISO</span>',
            text: "¿Estas seguro de aprobar la denuncia? ",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,Seguro',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {

                this.submit();
            }
        })
    })
</script>
@stop
