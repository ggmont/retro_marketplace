@extends('adminlte::page')

@section('title', 'Admin-Dashboard')

@section('content_header')
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="{{ asset('assets/css/goodgames.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/selectpicker/dist/css/bootstrap-select.min.css') }}">
    <style>
        .avatar {
            height: 50px;
            width: 50px;
        }

        .list-group-item:hover,
        .list-group-item:focus {
            background: rgba(24, 32, 23, 0.37);
            cursor: pointer;
        }

        .chatbox {
            height: 80vh !important;
            overflow-y: scroll;
        }

        .message-box {
            height: 70vh !important;
            overflow-y: scroll;
            display: flex;
            flex-direction: column-reverse;
        }

        .single-message {
            background: #f1f0f0;
            border-radius: 12px;
            padding: 10px;
            margin-bottom: 10px;
            width: fit-content;
        }

        .received {
            margin-right: auto !important;
        }

        .sent {
            margin-left: auto !important;
            background: #3490dc;
            color: white !important;
        }

        .sent small {
            color: white !important;
        }

        .link:hover {
            list-style: none !important;
            text-decoration: none;
        }

        .online-icon {
            font-size: 11px !important;
        }

        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }
    </style>
    @livewireStyles
@stop

@section('content')



    <section class="content">
        <div class="container-fluid">


            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <center>
                            <label class="block mb-2 text-lg font-extrabold tracking-wide text-white uppercase">
                                chat entre <br>
                                {{ $user_one->user_name }} y {{ $user_two->user_name }}
                            </label>
                        </center>
                    </div>
                    <div class="card-body message-box">
                        @foreach ($viewData['message'] as $mgs)
                            <div class="single-message @if ($mgs->from_id == $user_one->id) sent @else received @endif">

                                <p class="my-0 text-base text-black retro font-weight-bolder">
                                    @if ($mgs->from_id == $user_one->id)
                                    {{ $user_one->user_name }}
                                    @else
                                    {{ $user_two->user_name }}
                                    @endif
                                </p>
                               
                                <span class="text-sm font-bold"> {!!$mgs->body!!}</span>


                                <br><small class="text-muted w-100">@lang('messages.msg_sends')
                                    <em>{{ $mgs->created_at }}</em></small>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>


        </div>
    </section>



@stop


@section('js')
    @livewireScripts
@stop
