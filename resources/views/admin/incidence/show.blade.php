@extends('adminlte::page')

@section('title', 'Admin-Dashboard - Detalles de la orden')

@section('content_header')

    <h1 class="font-bold text-2xl my-8 text-center">- Detalles de la orden -</h1>

@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/fontawesome.min.css"
        integrity="sha512-cHxvm20nkjOUySu7jdwiUxgGy11vuVPE9YeK89geLMLMMEOcKFyS2i+8wo0FOwyQO/bL8Bvq1KMsqK4bbOsPnA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tw-elements/dist/css/index.min.css" />
    <script src="https://cdn.tailwindcss.com/3.2.4"></script>
    <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/style.css?v=2') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
    <link rel="stylesheet" href="{{ asset('css/mobile/mobilekit.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatable/basictable.min.css') }}" />
    @livewireStyles
@stop

@section('content')

    <head>
        <script type="text/javascript" src="{{ asset('js/datatable/basictable.min.js') }}"></script>
    </head>

    <section class="content">

        <div class="container-fluid">


            <div class="col-md-12 pb-1" style="background-color: #ffffff; border: solid 1px #db2e2e">
                <span class="retro">@include('partials.flash')</span>

                <div class="page-content-wrapper">



                    <ul class="listview image-listview">
                        <li>
                            <div class="item" bis_skin_checked="1">
                                <div class="in" bis_skin_checked="1">
                                    <div bis_skin_checked="1">{{ __('ID') }}</div>
                                    <span class="text-muted text-sm font-semibold">{{ $incidence->order->order_identification }}</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="item" bis_skin_checked="1">
                                <div class="in" bis_skin_checked="1">
                                    <div bis_skin_checked="1">{{ __('Vendedor') }}</div>
                                    <span class="text-muted text-sm font-semibold">{{ $incidence->order->seller->user_name }}</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="item" bis_skin_checked="1">
                                <div class="in" bis_skin_checked="1">
                                    <div bis_skin_checked="1">{{ __('Comprador') }}</div>
                                    <span class="text-muted text-sm font-semibold">{{ $incidence->order->buyer->user_name }}</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="item" bis_skin_checked="1">
                                <div class="in" bis_skin_checked="1">
                                    <div bis_skin_checked="1">{{ __('Artículos comprados') }}</div>
                                    <span class="text-muted text-sm font-semibold">{{ $incidence->order->quantity }}</span>
                                </div>
                            </div>
                        </li>

                        <?php
                        $duration = [];
                        $total_iva = 0;
                        
                        foreach ($details as $cartItem) {
                            $cantidad = $cartItem->quantity;
                            $duration[] = $cantidad;
                        }
                        
                        $total = array_sum($duration);
                        
                        $total_iva += $total * 0.04;
                        
                        ?>

                        @php($totalWeight = 0)
                        @php($totalSize = 0)

                        @foreach ($details as $cartItem)

                            @if ($cartItem->kg !== null) 
                                @php($totalWeight += $cartItem->qty * ($cartItem->kg / 1000))
                            @else 
                                @php($totalWeight += $cartItem->qty * (1 / 1000))
                            @endif

                            @if ($cartItem->size == 0)
                                @php($totalSize += $cartItem->qty * (15 + 15 + 20))
                            @elseif($cartItem->size == 1)
                                @php($totalSize += $cartItem->qty * (25 + 25 + 45))
                            @elseif($cartItem->size == 2)
                                @php($totalSize += $cartItem->qty * (45 + 45 + 30))
                            @elseif($cartItem->size == 3)
                                @php($totalSize += $cartItem->qty * (50 + 50 + 50))
                            @else
                                @php($totalSize += $cartItem->qty * ($cartItem->product->width + $cartItem->product->large + $cartItem->product->high))
                            @endif
                        @endforeach

                        <li>
                            <div class="item" bis_skin_checked="1">
                                <div class="in" bis_skin_checked="1">
                                    <div bis_skin_checked="1">{{ __('Coste de envío') }}</div>
                                    <span class="text-muted text-sm font-semibold">

                                        @if ($totalWeight >= 0 && $totalWeight <= 1 && $totalSize >= 0 && $totalSize <= 50)
                                            2.90 €
                                        @elseif($totalWeight >= 5 && $totalSize <= 95)
                                            4.90 €
                                        @elseif($totalWeight >= 10 && $totalSize <= 120)
                                            6.90 €
                                        @elseif($totalWeight >= 20 && $totalSize <= 150)
                                            10.90 €
                                        @else
                                            2.90 €
                                        @endif


                                    </span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="item" bis_skin_checked="1">
                                <div class="in" bis_skin_checked="1">
                                    <div bis_skin_checked="1">{{ __('Gastos de gestión') }}</div>
                                    <span class="text-muted text-sm font-semibold"> {{ number_format($total_iva, 2) }}
                                        €</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="item" bis_skin_checked="1">
                                <div class="in" bis_skin_checked="1">
                                    <div bis_skin_checked="1">{{ __('Total (EUR)') }}</div>
                                    <span class="text-muted text-sm font-semibold">

                                        @if ($totalWeight >= 0 && $totalWeight <= 1 && $totalSize >= 0 && $totalSize <= 50)
                                            {{ number_format($incidence->order->total + $total_iva + 2.9, 2) }} €
                                        @elseif($totalWeight >= 5 && $totalSize <= 95)
                                            {{ number_format($incidence->order->total + $total_iva + 4.9, 2) }} €
                                        @elseif($totalWeight >= 10 && $totalSize <= 120)
                                            {{ number_format($incidence->order->total + $total_iva + 6.9, 2) }} €
                                        @elseif($totalWeight >= 20 && $totalSize <= 150)
                                            {{ number_format($incidence->order->total + $total_iva + 10.9, 2) }} €
                                        @else
                                            {{ number_format($incidence->order->total + $total_iva + 2.9, 2) }} €
                                        @endif

                                    </span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="item" bis_skin_checked="1">
                                <div class="in" bis_skin_checked="1">
                                    <div bis_skin_checked="1">{{ __('Estado') }}</div>
                                    <span class="text-muted text-sm font-semibold">{{ __($incidence->order->statusDes) }}</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>


                <div class="nk-gap"></div>

                @php($normal = 0)
                @foreach ($details as $cartItem)
                    @if ($cartItem->inventory->title !== null)
                        @php($normal = 1)
                    @endif
                @endforeach


                @if($normal == 1)

                @if (isset($details))


                    <div class="nk-gap"></div>
                    <div class="listview-title mt-2"></div>
                    <div class="col-md-12">
                        <table id="table-two-axis" class="two-axis">
                            <thead>
                                <tr class="border-b border-gray-500 hover:bg-gray-200">
                                    <th class="text-xs">Producto</th>
                                    <th class="text-xs">Imagen</th>
                                    <th class="text-xs">Comentario</th>
                                    <th class="text-xs">Cantidad</th>
                                    <th class="text-xs">Precio</th>
                                    <th class="text-xs">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($details as $detail)
                                <tr class="border-b border-gray-500 hover:bg-gray-200">
                                    <td class="text-sm font-semibold text-gray-900">
                                             {{ $detail->inventory->title }}
                                    </td>
                                    <td class="text-center">
                                        @if ($detail->inventory->images->first())
                                            <a href="{{ '/uploads/inventory-images/' . $detail->inventory->images[0]->image_path }}"
                                                data-fancybox="{{ $detail->inventory->id }}">
                                                <img width="50px" height="50px"
                                                    src="{{ count($detail->inventory->images) > 0 ? url('/uploads/inventory-images/' . $detail->inventory->images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                            </a>

                                            @foreach ($detail->inventory->images as $p)
                                                @if (!$loop->first)
                                                    <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                        data-fancybox="{{ $detail->inventory->id }}">
                                                        <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                            width="0px" height="0px"
                                                            style="position:absolute;" />
                                                    </a>
                                                @endif
                                            @endforeach
                                        @else
                                            <center>
                                                <font color="black">
                                                    <i class="fa fa-times" data-toggle="popover"
                                                        data-content="@lang('messages.not_working_profile')"
                                                        data-placement="top" data-trigger="hover"></i>
                                                </font>
                                            </center>
                                        @endif
                                    </td>

                                    <td>
                                        @if ($detail->inventory->comments == '')
                                            <center>
                                                <font color="black">
                                                    <i class="fa fa-times" data-toggle="popover"
                                                        data-content="No Posee" data-placement="top"
                                                        data-trigger="hover"></i>
                                                </font>
                                            </center>
                                        @else
                                        {{ $detail->inventory->comments }}
                                        @endif
                                    </td>

                                    <td class="text-sm font-semibold text-gray-900">

                                        {{ $detail->quantity }}

                                    </td>

                                    <td>
                                        <span class="text-lg font-semibold text-green-900"
                                            data-value="{{ $detail->price }}">
                                            {{ number_format($detail->price, 2) }}
                                            € </span>


                                    </td>
                                    <td>
                                        <span class="text-lg font-semibold text-green-900"
                                            data-value="{{ $detail->price }}">
                                            {{ number_format($detail->total, 2) }}
                                            € </span>


                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                @endif

                @else

                @if (isset($details))
                    @foreach (App\AppOrgCategory::whereIn('id', [1, 2, 3, 4, 172])->get() as $CatItem)
                        @php($contador = 0)

                        @foreach ($details as $cartItem)
                            @if ($cartItem->product->catid == $CatItem->id)
                                @php($contador += 1)
 
                            @endif
                        @endforeach
                        <div class="nk-gap"></div>
                        <div class="listview-title mt-2" style="display: {{ $contador > 0 ? 'block' : 'none' }};">
                            {{ __($CatItem->name) }}</div>
                        <div class="col-md-12">
                            <table id="table-two-axis" class="two-axis"
                                style="display: {{ $contador > 0 ? 'table' : 'none' }}; width:100%;">
                                <thead>
                                    @if ($CatItem->id == 1)
                                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                                            <th class="text-xs">Producto</th>
                                            <th class="text-xs">Caja</th>
                                            <th class="text-xs">Caratula</th>
                                            <th class="text-xs">Manual</th>
                                            <th class="text-xs">Juego</th>
                                            <th class="text-xs">Extra</th>
                                            <th class="text-xs">Imagen</th>
                                            <th class="text-xs">Codigo de barras</th>
                                            <th class="text-xs">Comentario</th>
                                            <th class="text-xs">Cantidad</th>
                                            <th class="text-xs">Precio</th>
                                            <th class="text-xs">Total</th>
                                        </tr>
                                    @elseif($CatItem->id == 2)
                                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                                            <th class="text-xs">Producto</th>
                                            <th class="text-xs">Caja</th>
                                            <th class="text-xs">Estado</th>
                                            <th class="text-xs">Manual</th>
                                            <th class="text-xs">Consola</th>
                                            <th class="text-xs">Extra</th>
                                            <th class="text-xs">Cables</th>
                                            <th class="text-xs">Imagen</th>
                                            <th class="text-xs">Codigo de barras</th>
                                            <th class="text-xs">Comentario</th>
                                            <th class="text-xs">Cantidad</th>
                                            <th class="text-xs">Precio</th>
                                            <th class="text-xs">Total</th>
                                        </tr>
                                    @else
                                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                                            <th class="text-xs">Producto</th>
                                            <th class="text-xs">Caja</th>
                                            <th class="text-xs">Estado</th>
                                            <th class="text-xs">Extra</th>
                                            <th class="text-xs">Imagen</th>
                                            <th class="text-xs">Codigo de Barras</th>
                                            <th class="text-xs">Comentario</th>
                                            <th class="text-xs">Cantidad</th>
                                            <th class="text-xs">Precio</th>
                                            <th class="text-xs">Total</th>
                                        </tr>
                                    @endif
                                </thead>
                                <tbody>
                                    @foreach ($details as $detail)
                                        @if ($detail->product->catid == $CatItem->id)
                                            @if ($CatItem->id == 1)
                                                <tr class="border-b border-gray-500 hover:bg-gray-200">
                                                    <td class="text-sm font-semibold text-gray-900">

                                                
                                                             {{ $detail->product->name }}
                                                                    @if ($detail->product->name_en)
                                                                        <br><small>{{ $detail->product->name_en }}</small>
                                                                    @endif
                                                            
                                                       



                                                        <br> {{ $detail->product->platform }}
                                                            -
                                                            {{ $detail->product->region }} 
                                                    </td>
                                                    <td class="text-sm font-semibold text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $detail->inventory->box }}"
                                                                data-toggle="popover"
                                                                data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    <td class="text-base text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $detail->inventory->cover }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($detail->inventory->cover_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    <td class="text-base text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $detail->inventory->manual }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($detail->inventory->manual_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    <td class="text-base text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $detail->inventory->game }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    <td class="text-base text-gray-900">
                                                        <center>
                                                            <img width="15px" src="/{{ $detail->inventory->extra }}"
                                                                data-toggle="popover"
                                                                data-content="{{ App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>
                                                    <td>
                                                        @if (count($detail->product->images) > 0)
                                                            <center>
                                                                <a href="/product/{{ $detail->product->id }}"><img
                                                                        width="70px" height="70px"
                                                                        src="{{ url('/images/' . $detail->product->images[0]->image_path) }}"></a>
                                                            </center>
                                                        @else
                                                            <center>
                                                                <a href="/product/{{ $detail->product->id }}"><img
                                                                        width="70px" height="70px"
                                                                        src="{{ url('assets/images/art-not-found.jpg') }}"></a>
                                                            </center>
                                                        @endif
                                                    </td>

                                                    <td>
                                                        A
                                                    </td>

                                                    <td>
                                                        @if ($detail->inventory->comments == '')
                                                            <center>
                                                                <font color="black">
                                                                    <i class="fa fa-times" data-toggle="popover"
                                                                        data-content="No Posee" data-placement="top"
                                                                        data-trigger="hover"></i>
                                                                </font>
                                                            </center>
                                                        @else
                                                            <center>
                                                                <font color="black">
                                                                    <i class="fa fa-comment" data-toggle="popover"
                                                                        data-content="{{ $detail->inventory->comments }}"
                                                                        data-placement="top" data-trigger="hover"></i>
                                                                </font>
                                                            </center>
                                                        @endif
                                                    </td>

                                                    <td class="text-sm font-semibold text-gray-900">

                                                        {{ $detail->quantity }}

                                                    </td>

                                                    <td>
                                                        <span class="text-lg font-semibold text-green-900"
                                                            data-value="{{ $detail->price }}">
                                                            {{ number_format($detail->price, 2) }}
                                                            € </span>


                                                    </td>
                                                    <td>
                                                        <span class="text-lg font-semibold text-green-900"
                                                            data-value="{{ $detail->price }}">
                                                            {{ number_format($detail->total, 2) }}
                                                            € </span>


                                                    </td>

                                                </tr>
                                            @elseif($CatItem->id == 2)
                                                <tr class="border-b border-gray-500 hover:bg-gray-200">
                                                    <td class="product-cart-img">

                                                        @if (count($detail->product->images) > 0)
                                                            <center>
                                                                <a href="/product/{{ $detail->product->id }}"><img
                                                                        width="70px" height="70px"
                                                                        src="{{ url('/images/' . $detail->product->images[0]->image_path) }}"></a>
                                                            </center>
                                                        @else
                                                            <center>
                                                                <a href="/product/{{ $detail->product->id }}"><img
                                                                        width="70px" height="70px"
                                                                        src="{{ url('assets/images/art-not-found.jpg') }}"></a>
                                                            </center>
                                                        @endif
                                                    </td>
                                                    <td class="text-left">

                                                        <span class="text-base retro font-weight-bold color-primary small">
                                                            <font color="black"><b>{{ $detail->product->name }}
                                                                    @if ($detail->product->name_en)
                                                                        <br><small>{{ $detail->product->name_en }}</small>
                                                                    @endif
                                                                </b>
                                                            </font>
                                                        </span>



                                                        <br><span
                                                            class="h7 text-secondary">{{ $detail->product->platform }}
                                                            -
                                                            {{ $detail->product->region }}</span>

                                                    </td>
                                                    <td class="product-name">
                                                        <center>
                                                            <img width="15px" src="/{{ $detail->inventory->box }}"
                                                                data-toggle="popover"
                                                                data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>

                                                    </td>

                                                    <td class="product-stock-status">
                                                        <center>
                                                            <img width="15px" src="/{{ $detail->inventory->cover }}"
                                                                data-toggle="popover"
                                                                data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->cover_condition)) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>

                                                    </td>

                                                    <td class="product-stock-status">
                                                        <center>
                                                            <img width="15px" src="/{{ $detail->inventory->manual }}"
                                                                data-toggle="popover"
                                                                data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->manual_condition)) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>

                                                    </td>

                                                    <td class="product-price">
                                                        <center>
                                                            <img width="15px" src="/{{ $detail->inventory->game }}"
                                                                data-toggle="popover"
                                                                data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>

                                                    <td class="product-price">
                                                        <center>
                                                            <img width="15px" src="/{{ $detail->inventory->extra }}"
                                                                data-toggle="popover"
                                                                data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>

                                                    </td>
                                                    <td class="product-price">
                                                        <center>
                                                            <img width="15px" src="/{{ $detail->inventory->inside }}"
                                                                data-toggle="popover"
                                                                data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->inside_condition)) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>

                                                    </td>
                                                    <td class="text-center">
                                                        @if ($detail->inventory->images->first())
                                                            <a href="{{ '/uploads/inventory-images/' . $detail->inventory->images[0]->image_path }}"
                                                                data-fancybox="{{ $detail->product->id }}">
                                                                <img width="50px" height="50px"
                                                                    src="{{ count($detail->inventory->images) > 0 ? url('/uploads/inventory-images/' . $detail->inventory->images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                            </a>

                                                            @foreach ($detail->inventory->images as $p)
                                                                @if (!$loop->first)
                                                                    <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                        data-fancybox="{{ $detail->product->id }}">
                                                                        <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                            width="0px" height="0px"
                                                                            style="position:absolute;" />
                                                                    </a>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            <center>
                                                                <font color="black">
                                                                    <i class="fa fa-times" data-toggle="popover"
                                                                        data-content="@lang('messages.not_working_profile')"
                                                                        data-placement="top" data-trigger="hover"></i>
                                                                </font>
                                                            </center>
                                                        @endif
                                                    </td>
                                                    <td class="product-stock-status">
                                                        @if ($detail->inventory->comments == '')
                                                            <center>
                                                                <font color="black">
                                                                    <i class="fa fa-times" data-toggle="popover"
                                                                        data-content="@lang('messages.not_working_profile')"
                                                                        data-placement="top" data-trigger="hover"></i>
                                                                </font>
                                                            </center>
                                                        @else
                                                            <center>
                                                                <font color="black">
                                                                    <i class="fa fa-comment" data-toggle="popover"
                                                                        data-content="{{ $detail->inventory->comments }}"
                                                                        data-placement="top" data-trigger="hover"></i>
                                                                </font>
                                                            </center>
                                                        @endif
                                                    </td>

                                                    <div class="retro">
                                                        <td class="text-center retro h7">
                                                            <font color="black">{{ $detail->quantity }}</font>
                                                        </td>
                                                    </div>
                                                    <td>
                                                        <font color="green"> <span
                                                                class="text-base text-right retro font-extrabold color-primary text-nowrap"
                                                                data-value="{{ $detail->price }}">
                                                                {{ number_format($detail->price, 2) }}
                                                                € </span> </font>


                                                    </td>
                                                    <td>
                                                        <font color="green"> <span
                                                                class="text-base text-right retro font-extrabold color-primary text-nowrap"
                                                                data-value="{{ $detail->price }}">
                                                                {{ number_format($detail->total, 2) }}
                                                                € </span> </font>


                                                    </td>

                                                </tr>
                                            @else
                                                <tr class="border-b border-gray-500 hover:bg-gray-200">
                                                    <td class="product-cart-img">

                                                        @if (count($detail->product->images) > 0)
                                                            <center>
                                                                <a href="/product/{{ $detail->product->id }}"><img
                                                                        width="70px" height="70px"
                                                                        src="{{ url('/images/' . $detail->product->images[0]->image_path) }}"></a>
                                                            </center>
                                                        @else
                                                            <center>
                                                                <a href="/product/{{ $detail->product->id }}"><img
                                                                        width="70px" height="70px"
                                                                        src="{{ url('assets/images/art-not-found.jpg') }}"></a>
                                                            </center>
                                                        @endif
                                                    </td>
                                                    <td class="text-left">
                                                        <a href="/product/{{ $detail->product->id }}" target="_blank">
                                                            <span
                                                                class="text-xs retro font-weight-bold color-primary small">
                                                                <font color="black"><b>{{ $detail->product->name }}
                                                                        @if ($detail->product->name_en)
                                                                            <br><small>{{ $detail->product->name_en }}</small>
                                                                        @endif
                                                                    </b>
                                                                </font>
                                                            </span>
                                                        </a>


                                                        <br><span
                                                            class="h7 text-secondary">{{ $detail->product->platform }}
                                                            -
                                                            {{ $detail->product->region }}</span>

                                                    </td>
                                                    <td class="product-name">
                                                        <center>
                                                            <img width="15px" src="/{{ $detail->inventory->box }}"
                                                                data-toggle="popover"
                                                                data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>

                                                    </td>


                                                    <td class="product-price">
                                                        <center>
                                                            <img width="15px" src="/{{ $detail->inventory->game }}"
                                                                data-toggle="popover"
                                                                data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>
                                                    </td>

                                                    <td class="product-price">
                                                        <center>
                                                            <img width="15px" src="/{{ $detail->inventory->extra }}"
                                                                data-toggle="popover"
                                                                data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                                data-placement="top" data-trigger="hover">
                                                        </center>

                                                    </td>
                                                    <td class="text-center">
                                                        @if ($detail->inventory->images->first())
                                                            <a href="{{ '/uploads/inventory-images/' . $detail->inventory->images[0]->image_path }}"
                                                                data-fancybox="{{ $detail->product->id }}">
                                                                <img width="50px" height="50px"
                                                                    src="{{ count($detail->inventory->images) > 0 ? url('/uploads/inventory-images/' . $detail->inventory->images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                            </a>

                                                            @foreach ($detail->inventory->images as $p)
                                                                @if (!$loop->first)
                                                                    <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                        data-fancybox="{{ $detail->product->id }}">
                                                                        <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                            width="0px" height="0px"
                                                                            style="position:absolute;" />
                                                                    </a>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            <center>
                                                                <font color="black">
                                                                    <i class="fa fa-times" data-toggle="popover"
                                                                        data-content="@lang('messages.not_working_profile')"
                                                                        data-placement="top" data-trigger="hover"></i>
                                                                </font>
                                                            </center>
                                                        @endif
                                                    </td>
                                                    <td class="product-stock-status">
                                                        @if ($detail->inventory->comments == '')
                                                            <center>
                                                                <font color="black">
                                                                    <i class="fa fa-times" data-toggle="popover"
                                                                        data-content="@lang('messages.not_working_profile')"
                                                                        data-placement="top" data-trigger="hover"></i>
                                                                </font>
                                                            </center>
                                                        @else
                                                            <center>
                                                                <font color="black">
                                                                    <i class="fa fa-comment" data-toggle="popover"
                                                                        data-content="{{ $detail->inventory->comments }}"
                                                                        data-placement="top" data-trigger="hover"></i>
                                                                </font>
                                                            </center>
                                                        @endif
                                                    </td>

                                                    <div class="retro">
                                                        <td class="text-center retro h7">
                                                            <font color="black">{{ $detail->quantity }}</font>
                                                        </td>
                                                    </div>
                                                    <td>
                                                        <font color="green"> <span
                                                                class="text-right retro font-weight-bold color-primary small text-nowrap"
                                                                data-value="{{ $detail->price }}">
                                                                {{ number_format($detail->price, 2) }}
                                                                € </span> </font>


                                                    </td>
                                                    <td>
                                                        <font color="green"> <span
                                                                class="text-right retro font-weight-bold color-primary small text-nowrap"
                                                                data-value="{{ $detail->price }}">
                                                                {{ number_format($detail->total, 2) }}
                                                                € </span> </font>


                                                    </td>

                                                </tr>
                                            @endif
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endforeach
                @endif

                @endif
                
                <div class="nk-gap"></div>
                <div class="nk-gap"></div>
            </div>

        </div>

        <br><br><br><br>


    </section>


@stop


@section('js')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        Livewire.on('alert', function() {
            Swal.fire(
                'Listo!',
                '<span class="retro">Nueva Promocion Agregada</span>',
                'success'
            )
        })
    </script>

    <script>
        const table = new basictable('.table');

        new basictable('#table-breakpoint', {
            breakpoint: 768,
        });

        new basictable('#table-container-breakpoint', {
            containerBreakpoint: 485,
        });

        new basictable('#table-force-off', {
            forceResponsive: false,
        });

        new basictable('#table-max-height', {
            tableWrap: true
        });

        new basictable('#table-no-resize', {
            noResize: true,
        });

        new basictable('#table-two-axis');
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.15.4/js/all.js"
        integrity="sha384-rOA1PnstxnOBLzCLMcre8ybwbTmemjzdNlILg8O7z1lUkLXozs4DHonlDtnE7fpc" crossorigin="anonymous">
    </script>

    @livewireScripts
    <script>
        $(document).ready(function() {
            $('.comments').popover();
            $('[data-toggle="popover"]').popover();
        });
    </script>
@stop
