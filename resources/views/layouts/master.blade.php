<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
 
        <link rel="stylesheet" href="{{asset('plugins/jquery-spinner/css/bootstrap-spinner.css')}}"/>
 
 
    </head>
 
    
        <!-- Page Loader -->
 <div id="mdb-preloader" class="flex-center">
  <div class="preloader-wrapper active">
    <div class="spinner-layer spinner-blue-only">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div>
      <div class="gap-patch">
        <div class="circle"></div>
      </div>
      <div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>
  </div>
</div>
        <!-- Overlay For Sidebars -->
 
        </section>
 
 
     <script>
     	$(window).on('load', function() {
  $('#mdb-preloader').delay(1000).fadeOut(300);
});
     </script>
 
 
    </body>
</html>