<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Invoice - #123</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }

        body {
            margin: 0px;
        }

        * {
            font-family: Verdana, Arial, sans-serif;
        }

        a {
            color: #fff;
            text-decoration: none;
        }

        table {
            font-size: x-small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .invoice table {
            margin-top: 15px;
            margin-left: 55px;
            margin-right: 55px;
        }

        .invoice h3 {
            margin-left: 55px;
        }

        .information {
            color: #FFF;

        }

        .information .logo {
            margin: 5px;
        }

        .information table {
            padding: 10px;
        }

    </style>

</head>

<body>

    <div class="information">
        <table width="100%">
            <tr>
                <td align="left" style="width: 40%;">
                    <h3 style="font-family: Verdana">
                       -</h3>
                    <pre style="font-family: Verdana">
{{ $resumen->first()->buyer->address }}
{{ $resumen->first()->buyer->zipcode }} {{ $resumen->first()->buyer->city }}
{{ $resumen->first()->buyer->country->name }}
<br /><br />
Date: {{ date('d/m/Y') }}
Status: Paid
</pre>


                </td>
                <td align="center">
                    <img src="assets/images/logo.png" alt="Logo" width="164" class="logo" />
                </td>

                <td align="right" style="width: 40%;">

                    <h3 style="font-family: Verdana">CompanyName</h3>
                    <pre style="font-family: Verdana">
                    {{ $url }}

                    Street 26
                    123456 City
                    United Kingdom
                </pre>
                </td>
            </tr>

        </table>
    </div>


    <br />

    <div class="invoice">
        <h3>Registro de comisiones {{ $date }}</h3>
        <table width="100%" style="border-collapse: collapse; border: 0.1px solid black; font-family: Arial">
            <thead style="border: 0.1px solid black;">
                <tr>
                    <th style="border: 0.1px solid black;">Fecha</th>
                    <th style="border: 0.1px solid black;">Producto</th>
                    <th style="border: 0.1px solid black;">Importe</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($resumen as $key)
                    <tr>
                        <td style="border: 0.1px solid black;">{{ date('d/m/Y', strtotime($key->updated_at)) }}</td>
                        <td style="border: 0.1px solid black;">Comisión del pedido:
                            <strong>{{ $key->instructions }}</strong>
                        </td>
                        <td style="border: 0.1px solid black;" align="right">{{ number_format($key->total, 2) }}</td>
                    </tr>
                @endforeach

            </tbody>

            <tfoot>
                <tr>
                    <td style="border: 0.1px solid black;" align="right" colspan="2">Subtotal</td>
                    <td style="border: 0.1px solid black;" align="right" class="gray">
                        €{{ number_format($total - $total * 0.21, 2) }}</td>
                </tr>
                <tr>
                    <td style="border: 0.1px solid black;" align="right" colspan="2">Impuestos</td>
                    <td style="border: 0.1px solid black;" align="right" class="gray">
                        €{{ number_format($total * 0.21, 2) }}</td>
                </tr>
                <tr>
                    <td style="border: 0.1px solid black;" align="right" colspan="2">Total</td>
                    <td style="border: 0.1px solid black;" align="right" class="gray">
                        €{{ number_format($total, 2) }}</td>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="information" style="position: absolute; bottom: 0;">
        <table width="100%" style="background-color: #000; color: #fff">
            <tr>
                <td align="left" style="width: 50%;">
                    &copy; {{ date('Y') }} {{ $url }} - All rights reserved.
                </td>
                <td align="right" style="width: 50%;">
                    {{ config('app.name') }}
                </td>
            </tr>

        </table>
    </div>
</body>

</html>
