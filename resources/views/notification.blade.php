@extends('adminlte::page')

@section('title', 'Admin-Dashboard - Noticias')

@section('content_header')
    <center>
        <h1 class="font-bold">- Notificaciones (Zona de prueba) -</h1>
    </center>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nes/nes_prueba.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <!-- Default CSS -->
    <link rel="stylesheet" href="{{ asset('brcode/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/default.css') }}">

    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">

    @yield('content-css-include')

    <!-- Fancybox CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">


    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <style>
        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Press Start 2P', cursive;
        }
    </style>


    @livewireStyles
@stop

@section('content')

    <section class="content">
        <div class="container-fluid">

            <span class="retro">@include('partials.flash')</span>


            <button onclick="initFirebaseMessagingRegistration()" class="btn btn-danger btn-flat">Permitir Notificaciones
            </button>

            <div class="col-12">

                <form class="w-full" action="{{ route('send.web-notification', auth()->user()->id) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="p-4 mx-auto mb-6 overflow-hidden bg-white rounded-lg shadow max-w-7xl">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="mb-3">
                                    <label for="" class="mb-2 form-label">Titulo</label>
                                    <input name="title" id="title" placeholder="Title" class="form-control">
                                    @error('title')
                                        <span class="text-xs text-red-700 error retro">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>


                            <br>

                            <div class="col-sm-12">
                                <div class="mb-3">
                                    <label for="" class="mb-2 form-label">Contenido</label>
                                    <textarea name="body" id="editor" cols="10" rows="5" class="form-control" placeholder="Content"></textarea>
                                    @error('content')
                                        <span class="text-xs text-red-700 error retro">{{ $body }}</span>
                                    @enderror
                                </div>
                            </div>

                            <br>

                            <div class="col-sm-12">
                                <div class="mb-3">
                                    <button type="submit" class="btn btn-success btn-block">Enviar Notificación</button>
                                </div>
                            </div>


                        </div>

                    </div>

                </form>

            </div>

            <div class="card-body">
                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                @endif

                <form action="{{ route('notification') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" name="title">
                    </div>
                    <div class="form-group">
                        <label>Body</label>
                        <textarea class="form-control" name="message"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Send Notification</button>
                </form>

            </div>



        </div>
    </section>
@stop


@section('js')
    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js"></script>

    <!-- TODO: Add SDKs for Firebase products that you want to use
        https://firebase.google.com/docs/web/setup#available-libraries -->

    <script>
        // Your web app's Firebase configuration
        var firebaseConfig = {
            apiKey: "AIzaSyDqCbmiW7VzXWUk71AmAfCDltT5Ne_xGJc",
            authDomain: "retrogamingmarket.firebaseapp.com",
            projectId: "retrogamingmarket",
            storageBucket: "retrogamingmarket.appspot.com",
            messagingSenderId: "628245415654",
            appId: "1:628245415654:web:20bf0e8e77a0cea7dadd27",
            measurementId: "G-G6NFK11DJ5"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);

        const messaging = firebase.messaging();

        function initFirebaseMessagingRegistration() {
            messaging.requestPermission().then(function() {
                return messaging.getToken()
            }).then(function(token) {

                axios.post("{{ route('fcmToken') }}", {
                    _method: "PATCH",
                    token
                }).then(({
                    data
                }) => {
                    console.log(data)
                }).catch(({
                    response: {
                        data
                    }
                }) => {
                    console.error(data)
                })

            }).catch(function(err) {
                console.log(`Token Error :: ${err}`);
            });
        }

        initFirebaseMessagingRegistration();

        messaging.onMessage(function({
            data: {
                body,
                title
            }
        }) {
            new Notification(title, {
                body
            });
        });
    </script>
@stop
