@extends('brcode.layout.app')
@section('content-css-include')

@endsection
@section('content')
    <section class="content-header">
        <center>
            <h3 class="retro">- <i class="nes-jp-logo"></i> Dashboard principal <i class="nes-logo"></i> -
            </h3>
        </center>

    </section>
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-info elevation-1"><i class="fas fa-boxes"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Inventario</span>
                            <span class="text-xs info-box-number retro">
                                {{ $viewData['user_inventory'] }}
                                <small></small>
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>



                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-red elevation-1"><i class="fas fa-trophy"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Logros</span>
                            <span class="text-xs retro info-box-number">
                                ??? / ???
                                <small></small>
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>



                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-money-check-alt"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Compras</span>
                            <span class="text-xs retro info-box-number">
                                {{ App\AppOrgOrder::where('buyer_user_id', Auth::id())->whereIn('status', ['DD', 'ST', 'CR'])->count() }}
                                <small></small>
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>



                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-primary elevation-1"><i class="fab fa-google-play"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Ventas</span>
                            <span class="text-xs retro info-box-number">
                                {{ App\AppOrgOrder::where('seller_user_id', Auth::id())->whereIn('status', ['DD'])->count() }}
                                <small></small>
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </div>

        </div>
    </section>
@endsection
@section('content-script-include')
    <script src="{{ asset('assets/timepicker/bootstrap-timepicker.min.js') }}"></script>
@endsection
@section('content-script')
    <script type="text/javascript">

    </script>
@endsection
