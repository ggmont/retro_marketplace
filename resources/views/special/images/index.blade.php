@extends('brcode.layout.app')
@section('content-css-include')

@endsection
@section('content')
    <section class="content-header">
        <center>
            <h3 class="retro">- <i class="nes-jp-logo"></i> Edicion de Imagenes <i class="nes-logo"></i> -
            </h3>
        </center>

    </section>
    <section class="content">
        <div class="container-fluid">

            @include('partials.flash')



            <livewire:special.image-product-table></livewire:special.image-product-table>

        </div>


    </section>
@endsection
@section('content-script-include')
    <script src="{{ asset('assets/timepicker/bootstrap-timepicker.min.js') }}"></script>
@endsection
@section('content-script')
    <script type="text/javascript">

    </script>
@endsection
