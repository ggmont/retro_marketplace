@extends('brcode.layout.app')
@section('content-css-include')
    <link href="/assets/dropzone-master/dist/min/dropzone.min.css" rel="stylesheet" type="text/css" />
    <style>
        .dropzone .dz-preview .dz-error-message {
            top: 175px !important;
        }

    </style>
@endsection
@section('content')
    <section class="content-header">
        <center>
            <h1 class="retro">- <i class="nes-jp-logo"></i> Edicion de Imagenes <i class="nes-logo"></i> -
            </h1>
        </center>

    </section>
    <section class="content">
        <div class="container-fluid">
            @if (session()->has('jsAlert'))
                <script>
                    alert({{ session()->get('jsAlert') }});
                </script>
            @endif
            <div class="row">

                <div class="col-md-12" style="background-color: #ffffff; border: solid 1px #db2e2e">
                    @include('partials.flash')
                    <form class="w-full" action="" method="POST">
                        <meta name="csrf-token" content="{{ csrf_token() }}" />

                        <div style="display:none" id="fileupload-files" data-name="image_path[]">
                            @foreach ($product->images as $p)
                                @php($i = 0)
                                <input type="hidden" id="file_{{ $i }}" name="image_path[]"
                                    value="{{ $p->image_path }}">
                                @php($i++)
                            @endforeach
                        </div>
                        <div id="dropzone">
                            <div class="dz-message text-dark" data-dz-message>
                                <span
                                    class="retro">{{ __('Suelte los archivos aquí o haga clic para cargar Max - 4.') }}</span>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>


    </section>
@endsection
@section('content-script-include')
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>

    <script src="{{ asset('assets/dropzone-master/dist/min/dropzone.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/quagga/0.12.1/quagga.js"></script>
    <script src="{{ asset('vendor/sweetalert2/sweetalert2.all.min.js') }}"></script>
    <script>
        var acceptedFileTypes = "image/*"; //dropzone requires this param be a comma separated list
        var fileList = new Array;
        var i = 0;
        var myDropzone = $("#dropzone").dropzone({
            paramName: "file",
            maxFilesize: 10,
            url: "{{ url('SpecialZone/edition/upload_file/' . $product->id) }}",
            addRemoveLinks: true,
            removedfile: function(file) {
                var name = file.name;
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: "{{ url('SpecialZone/edition/delete_file/' . $product->id) }}",
                    data: {
                        filename: name
                    },
                    success: function(data) {
                        console.log("Se a borrado correctamente");
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            maxFiles: 4, //change limit as per your requirements
            maxfilesexceeded: function(file) {
                Swal.fire({
                    title: '<span class="retro">Error</span>',
                    text: 'Haz Alcanzado el maximo de Imagenes (Max 4)',
                    footer: '<a class="retro" href="">¿Como Vender En RGM?</a>',
                    imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/a6cff967-2930-4785-a8b8-48a6e39101b8/dbrz0sh-36c848a3-b37d-4fd1-8d7e-90a27dcb8130.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2E2Y2ZmOTY3LTI5MzAtNDc4NS1hOGI4LTQ4YTZlMzkxMDFiOFwvZGJyejBzaC0zNmM4NDhhMy1iMzdkLTRmZDEtOGQ3ZS05MGEyN2RjYjgxMzAuZ2lmIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.QwzwRFsxRbUrhacQuJV4fSAij3GxImG_4dkxWDTgkMc',
                    imageWidth: 400,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                })
                this.removeFile(file);
            },
            acceptedFiles: ".jpg,.jpeg,.png",
            dictInvalidFileType: "No puedes subir archivos de este tipo , solo Imagenes tipo PNG O JPG",
            params: {
                "_token": "{{ csrf_token() }}",
                "dropzone": "hola",
            },
            dictDefaultMessage: "Drop files here to upload",
            init: function() {
                myDropzone = this;
                // Hack: Add the dropzone class to the element
                $(this.element).addClass("dropzone");

                // this.on("error", function(file, response) {
                //     // do stuff here.
                //   alert(response);

                // });

                this.on("success", function(file, serverFileName) {
                    fileList[i] = {
                        "serverFileName": serverFileName.name,
                        "fileName": file.name,
                        "fileId": i
                    };
                    input = $('#fileupload-files').data('name');
                    $('#fileupload-files').append(
                        `<input type="hidden" id="file_${ i }" name="${ input }" value="${ serverFileName.name }">`
                    );
                    $('.dz-message').show();
                    i += 1;
                });
                this.on("removedfile", function(file) {
                    var rmvFile = "";
                    for (var f = 0; f < fileList.length; f++) {
                        if (fileList[f].fileName == file.name) {
                            rmvFile = fileList[f].serverFileName;
                            $("#file_" + fileList[f].fileId).remove();
                        }
                    }
                });

                @foreach ($product->images as $p)
                    var mockFile = { name: "{{ $p->image_path }}", size: 12345 };
                
                    myDropzone.emit("addedfile", mockFile);
                
                    myDropzone.emit("thumbnail", mockFile, "/images/{{ $p->image_path }}");
                    myDropzone.emit("complete", mockFile);
                    fileList[i] = {
                    "serverFileName": "{{ $p->image_path }}",
                    "fileName": "{{ $p->image_path }}",
                    "fileId": i
                    };
                    i += 1;
                @endforeach

            }
        });
    </script>
@endsection
@section('content-script')

@endsection
