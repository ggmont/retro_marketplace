@extends('brcode.layout.app')
@section('content-css-include')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
@endsection
@section('content')
    <section class="content-header">
        <center>
            <h3 class="retro">- <i class="nes-jp-logo"></i> Edicion del Inventario <i class="nes-logo"></i> -
            </h3>
        </center>

    </section>
    <section class="content">
        <div class="container-fluid">

            @include('partials.flash')

            <div class="col-lg-12">

                <div class="nk-gap"></div>
                <!-- START: Tabs  -->
                <div class="nk-tabs">

                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" href="#star" role="tab" data-toggle="tab"><span
                                    class="retro">Destacados</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="#tabs-1-1" role="tab" data-toggle="tab"><span
                                    class="retro">Juegos</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-2" role="tab" data-toggle="tab"><span
                                    class="retro">Consolas</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-3" role="tab" data-toggle="tab"><span
                                    class="retro">Perifericos</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-4" role="tab" data-toggle="tab"><span
                                    class="retro">Accesorios</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-5" role="tab" data-toggle="tab"><span
                                    class="retro">Merchadising</span></a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade" id="star">
                            <div class="nk-gap"></div>
                            <livewire:profile.inventory.user-inventory-star>
                            </livewire:profile.inventory.user-inventory-star>

                        </div>
                        <div role="tabpanel" class="tab-pane fade show active" id="tabs-1-1">
                            <div class="nk-gap"></div>
                            <livewire:profile.inventory.user-inventory-game>
                            </livewire:profile.inventory.user-inventory-game>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-2">
                            <div class="nk-gap"></div>
                            <livewire:profile.inventory.user-inventory-console>
                            </livewire:profile.inventory.user-inventory-console>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-3">
                            <div class="nk-gap"></div>
                            <livewire:profile.inventory.user-inventory-periferico>
                            </livewire:profile.inventory.user-inventory-periferico>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-4">
                            <div class="nk-gap"></div>
                            <livewire:profile.inventory.user-inventory-accesorio>
                            </livewire:profile.inventory.user-inventory-accesorio>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-5">
                            <div class="nk-gap"></div>
                            <livewire:profile.inventory.user-inventory-merchadising>
                            </livewire:profile.inventory.user-inventory-merchadising>

                        </div>
                    </div>
                </div>
                <!-- END: Tabs -->
            </div>


        </div>


    </section>
@endsection
@section('content-script-include')
    @livewireScripts
    <script src="{{ asset('assets/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
@endsection
@section('content-script')
    <script>
        Livewire.on('favorite', function() {
            Swal.fire({
                title: '<span class="retro">Listo!</span>',
                text: 'El Producto a sido destacado correctamente',
                imageUrl: 'https://25.media.tumblr.com/c7962f0a224f88f965e375a33953a8c5/tumblr_msh4mj2arL1scncwdo1_500.gif',
                imageWidth: 400,
                imageHeight: 200,
                imageAlt: 'Custom image',
            })
        })
    </script>

    <script>
        Livewire.on('desfavorite', function() {
            Swal.fire({
                title: '<span class="retro">Listo!</span>',
                text: 'El Producto a sido quitado de destacados correctamente',
                imageUrl: 'https://c.tenor.com/6gTKZthPqykAAAAj/mario-sparkle.gif',
                imageWidth: 400,
                imageHeight: 200,
                imageAlt: 'Custom image',
            })
        })
    </script>

    <script>
        Livewire.on('game', function() {


            var $inventoryAdd = $('.br-btn-product-add');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('console', function() {


            var $inventoryAdd = $('.br-btn-product-add-console');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('periferico', function() {


            var $inventoryAdd = $('.br-btn-product-add-periferico');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('accesorio', function() {


            var $inventoryAdd = $('.br-btn-product-add-accesorio');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('merchandising', function() {


            var $inventoryAdd = $('.br-btn-product-add-merchandising');
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
            $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                        .val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data
                                            .items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append(
                                                    '<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>

    <script>
        Livewire.on('alert', function() {
            Swal.fire(
                'Listo!',
                '<span class="retro">Tu Producto fue eliminado</span>',
                'success'
            )
        })
    </script>
@endsection
