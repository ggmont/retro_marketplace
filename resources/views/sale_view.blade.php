@extends('brcode.front.layout.app')
@section('content-css-include')
    <style>
        body {
            margin-top: 20px;
        }

        h3 {
            font-size: 16px;
        }

        .text-navy {
            color: #1ab394;
        }

        .cart-product-imitation {
            text-align: center;
            padding-top: 30px;
            height: 80px;
            width: 80px;
            background-color: #f8f8f9;
        }

        .product-imitation.xl {
            padding: 120px 0;
        }

        .product-desc {
            padding: 20px;
            position: relative;
        }

        .ecommerce .tag-list {
            padding: 0;
        }

        .ecommerce .fa-star {
            color: #d1dade;
        }

        .ecommerce .fa-star.active {
            color: #f8ac59;
        }

        .ecommerce .note-editor {
            border: 1px solid #e7eaec;
        }

        table.shoping-cart-table {
            margin-bottom: 0;
        }

        table.shoping-cart-table tr td {
            border: none;
            text-align: right;
        }

        table.shoping-cart-table tr td.desc,
        table.shoping-cart-table tr td:first-child {
            text-align: left;
        }

        table.shoping-cart-table tr td:last-child {
            width: 80px;
        }

        .ibox {
            clear: both;
            margin-bottom: 25px;
            margin-top: 0;
            padding: 0;
        }

        .ibox.collapsed .ibox-content {
            display: none;
        }

        .ibox:after,
        .ibox:before {
            display: table;
        }

        .ibox-title {
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            background-color: #ffffff;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 3px 0 0;
            color: inherit;
            margin-bottom: 0;
            padding: 14px 15px 7px;
            min-height: 48px;
        }

        .ibox-content {
            background-color: #ffffff;
            color: inherit;
            padding: 15px 20px 20px 20px;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 1px 0;
        }

        .ibox-footer {
            color: inherit;
            border-top: 1px solid #e7eaec;
            font-size: 90%;
            background: #ffffff;
            padding: 10px 15px;
        }

        /* width */
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #888;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555;
        }

        .scroller {}

    </style>
@endsection
@section('content')

    <div class="nk-gap-2"></div>
    <div class="nk-gap-2"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="nk-decorated-h-3">
                    <div class="section-title2">
                        <h3 class="retro">
                            {{ __('Detalle de la venta') . ' - ' . $viewData['order_header']->order_identification }}
                            {{ $viewData['order_header']->status }} - </b></h3>
                    </div>
                </h3>
            </div>
        </div>
    </div>
    <div class="nk-gap"></div>
    <div class="container wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-9">
                <div class="ibox">
                    <div class="ibox-title">
                        <h3 class="text-2xl font-semibold text-gray-900 retro">{{ __('Encabezado de orden') }}</h3>
                    </div>
                    <div class="ibox-content">
                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                            <span class="text-sm font-semibold uppercase">{{ __('ID') }}</span>
                            <span class="text-sm font-semibold">
                                {{ $viewData['order_header']->order_identification }}
                        </div>
                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                            <span class="text-sm font-semibold uppercase">{{ __('Comprador') }}</span>
                            <span class="text-sm font-semibold">
                                {{ $viewData['order_header']->buyer->user_name }}
                        </div>
                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                            <span class="text-sm font-semibold uppercase">{{ __('Artículos comprados') }}</span>
                            <span class="text-sm font-semibold">
                                {{ $viewData['order_header']->quantity }}
                        </div>
                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                            <span class="text-sm font-semibold uppercase">{{ __('Metodo de envío') }}</span>
                            <span class="text-sm font-semibold">
                                <b>{{ $viewData['order_shipping']->name }}</b> <br>
                                Certificado: {{ $viewData['order_shipping']->certified }} <br>
                                Peso Maximo: {{ $viewData['order_shipping']->max_weight }} g<br>
                                Volume: L {{ $viewData['order_shipping']->large }} x W
                                {{ $viewData['order_shipping']->weight }} x H
                                {{ $viewData['order_shipping']->hight }} (mm)<br>
                                <b>Precio:
                                    {{ number_format($viewData['order_header']->shipping_price, 2) }}</b>
                        </div>
                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                            <span class="text-sm font-semibold uppercase">{{ __('Envío hacia') }}</span>
                            <span class="text-sm font-semibold">
                                Nombre: <b>{{ $viewData['user_shipping']->first_name }}
                                    {{ $viewData['user_shipping']->last_name }}</b> <br>
                                Direccion: {{ $viewData['user_shipping']->address }} <br>
                                Código postal: {{ $viewData['user_shipping']->zipcode }}<br>
                                Pais: {{ $viewData['user_shipping']->country->name }}<br>
                                Ciudad: {{ $viewData['user_shipping']->city }}<br>
                        </div>
                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                            <span class="text-sm font-semibold uppercase">
                                {{ __('Total') }}
                                ({{ config('brcode.paypal_currency') }})
                            </span>
                            <span class="text-sm font-semibold text-green-900 retro">
                                {{ number_format($viewData['order_header']->total, 2) }}
                        </div>
                        <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                            <span class="text-sm font-semibold uppercase">{{ __('Estado') }}</span>
                            <span class="text-sm font-semibold text-red-900 retro">
                                {{ __($viewData['order_header']->statusDes) }}
                        </div>
                        @if (isset($viewData['order_header']->tracking_number) && $viewData['order_header']->tracking_number != '')
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('Número de rastreo') }}</span>
                                <span class="text-sm font-semibold">
                                    {{ $viewData['order_header']->tracking_number }}
                            </div>
                        @endif


                    </div>
                </div>
            </div>
            <div class="col-md-3">


                @if (in_array($viewData['order_header']->status, ['CW', 'PC']))
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5 class="pb-8 text-xs font-semibold text-gray-900 retro">{{ __('Solicitudes') }}
                            </h5>
                        </div>
                        <div class="ibox-content">
                            @if ($viewData['order_header']->cancelOrder->solicited_by == 'S')
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Número de rastreo') }}</span>
                                    <span class="text-sm font-semibold">
                                        {{ $viewData['order_header']->tracking_number }}
                                </div>
                            @else
                                <form
                                    action="{{ url('/account/sales/view/' . $viewData['order_header']->order_identification) }}"
                                    method="post">
                                    {{ csrf_field() }}
                                    <div class="text-left col-md-12">
                                        @if ($viewData['order_header']->status == 'PC')
                                            <span class="font-bold">El comprador ha <b>pagado</b> el pedido pero a
                                                decidido cancelarlo
                                            </span>
                                        @else
                                            <span class="font-bold">El comprador ha decidido cancelar el
                                                pedido
                                            </span>
                                        @endif
                                        <br>
                                        <br>
                                        <div class="ibox-title">
                                            <h5 class="text-xs text-gray-900 retro">Motivo</h5>
                                        </div>
                                        <p>{{ $viewData['order_header']->cancelOrder->cause }}</p>
                                        <label class="text-xs retro">@lang('messages.order_accept_cancel')</label>
                                        <br>

                                        <label>
                                            <input type="radio" class="nes-radio" name="optradio" value="Y" checked />
                                            <span class="retro">Yes</span>
                                        </label>

                                        <label>
                                            <input type="radio" class="nes-radio" name="optradio" value="N" />
                                            <span class="retro">No</span>
                                        </label>

                                        @if ($errors->has('optradio'))
                                            <br>
                                            <div class="alert alert-danger" role="alert">
                                                @foreach ($errors->get('optradio') as $error)
                                                    {{ $error }}
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-lg-12">
                                        <br>
                                        <label class="text-xs text-gray-900 retro">Razon</label>
                                        <textarea name="reason" required cols="30" rows="3"
                                            class="nes-textarea @if ($errors->has('reason')) is-invalid @endif"></textarea>
                                        <div class="textarea-counter" data-text-max="350"
                                            data-counter-text="{{ __('_QTY_ / _TOTAL_ caracteres') }}">
                                        </div>
                                        @if ($errors->has('reason'))
                                            <br>
                                            <div class="alert alert-danger" role="alert">
                                                @foreach ($errors->get('reason') as $error)
                                                    {{ $error }}
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-lg-12">
                                        <br>
                                        <button type="submit"
                                            class="nk-btn nk-btn-lg nk-btn-rounded nes-btn is-error btn-block">@lang('messages.save')</button>
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                @endif




                @if ($viewData['order_header']->rating)
                    <div class="ibox">
                        @if ($viewData['order_header']->rating->processig > 0)
                            <div class="ibox-title">
                                <h5 class="pb-8 text-xs font-semibold text-gray-900 retro">{{ __('Calificacion') }}
                                </h5>
                            </div>
                            <div class="ibox-content">
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Rating') }}</span>
                                    <span class="text-sm font-semibold ">
                                        <div id="orderID"><i
                                                class="{{ $viewData['order_header']->rating->ratingIcon }} text-{{ $viewData['order_header']->rating->ratingType }}"
                                                data-toggle="tooltip" data-placement="top"
                                                title="{{ $viewData['order_header']->rating->ratingStatus }}"></i>
                                        </div>
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span
                                        class="text-sm font-semibold uppercase">{{ __('Velocidad de procesamiento') }}</span>
                                    <span class="text-sm font-semibold ">
                                        <div id="orderID">{{ $viewData['order_header']->rating->pro }} </div>
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Empaquetado') }}</span>
                                    <span class="text-sm font-semibold ">
                                        <div id="orderID">{{ $viewData['order_header']->rating->pac }} </div>
                                    </span>
                                </div>
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Descripción') }}</span>
                                    <span class="text-sm font-semibold ">
                                        <div id="orderID">{{ $viewData['order_header']->rating->des }} </div>
                                    </span>
                                </div>
                                <div class="pb-8 mt-10 mb-10 border-b">
                                    <label class="text-sm font-semibold uppercase retro"><b>{{ __('Comentario') }}
                                            :</b></label>
                                    <br>
                                    <span class="text-sm font-semibold text-center">
                                        {{ $viewData['order_header']->rating->description }}
                                    </span>
                                </div>
                            </div>
                        @else
                            <div class="ibox-title">
                                <h5 class="pb-8 text-xs font-semibold text-gray-900 retro">{{ __('Calificacion') }}
                                </h5>
                            </div>
                            <div class="ibox-content">
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Rating') }}</span>
                                    <span class="text-sm font-semibold ">
                                        <div id="orderID"> {{ __('Sin Calificar') }} </div>
                                    </span>
                                </div>
                            </div>


                        @endif
                    </div>
                @endif

                @if ($viewData['order_header']->status == 'DD')
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5 class="pb-8 text-xs font-semibold text-gray-900 retro">{{ __('Solicitud Reembolso') }}
                            </h5>
                        </div>
                        <div class="ibox-content">
                            <form action="{{ route('requestRefund') }}" method="post">
                                {{ csrf_field() }}

                                <input type="hidden" name="id"
                                    value="{{ $viewData['order_header']->order_identification }}">

                                <div class="input-group">
                                    <input type="number" name="cash" autocomplete="off" step="0.01" placeholder="0.00"
                                        min="1" max="100" class="nes-input">

                                </div>
                                <span>Max. 100€</span>
                                <div class="nk-gap"></div>
                                <button type="submit"
                                    class="nk-btn nk-btn-lg nk-btn-rounded nes-btn is-error btn-block">Enviar
                                    solicitud</button>
                            </form>
                        </div>
                    </div>
                @endif





                @if ($viewData['order_header']->status == 'CR' || $viewData['order_header']->status == 'PD')
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5 class="pb-8 text-xs font-semibold text-gray-900 retro">{{ __('Detalle del envio') }}
                            </h5>
                        </div>
                        <div class="ibox-content">
                            <form action="{{ url('/account/sales/update') }}" method="post">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" class="form-control" id="id" name="id"
                                    value="{{ $viewData['order_header']->order_identification }}">

                                @if ($viewData['order_shipping']->certified == 'Yes')
                                    <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                        <span
                                            class="text-sm font-semibold uppercase">{{ __('Número de rastreo') }}</span>
                                        <span class="text-sm font-semibold">
                                            <input type="text"
                                                class="nes-input {{ $errors->has('tracking_number') ? 'is-invalid' : '' }}"
                                                id="tracking_number" required name="tracking_number"
                                                value="@if (isset($viewData['order_header']->tracking_number)) {{ old('tracking_number', $viewData['order_header']->tracking_number) }}@else{{ old('tracking_number') }} @endif">
                                    </div>
                                    @if ($errors->has('tracking_number'))
                                        <div class="invalid-feedback">
                                            @foreach ($errors->get('tracking_number') as $error)
                                                {{ $error }}
                                            @endforeach
                                        </div>
                                    @endif
                                @endif
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span
                                        class="text-sm font-semibold uppercase">{{ __('Confirmar la orden enviada') }}</span>
                                    <span class="text-sm font-semibold">
                                        <button type="submit"
                                            class="nk-btn nes-btn nk-btn-md nk-btn-rounded nk-btn-color-white btn-block"><i
                                                class="fa fa-truck"></i> @lang('messages.confirm_order_two') </button>
                                </div>
                        </div>
                    </div>
                    </form><br>

                @elseif($viewData['order_header']->status == 'DD' ||
                    $viewData['order_header']->status == 'ST')
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5 class="pb-8 text-xs font-semibold text-gray-900 retro">{{ __('Detalle del envio') }}
                            </h5>
                        </div>
                        <div class="ibox-content">
                            @if ($viewData['order_header']->company_code)
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Código de compania') }}</span>
                                    <span class="text-sm font-semibold">
                                        {{ $viewData['order_header']->company_code }}
                                </div>
                            @endif
                            @if ($viewData['order_header']->tracking_number)
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Número de rastreo') }}</span>
                                    <span class="text-sm font-semibold">
                                        {{ $viewData['order_header']->tracking_number }}
                                </div>
                            @endif
                            <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                <span class="text-sm font-semibold uppercase">{{ __('Fecha de envío') }}</span>
                                <span class="text-sm font-semibold">
                                    {{ $viewData['order_header']->sent_on }}
                            </div>
                            @if ($viewData['order_header']->status == 'DD')
                                <div class="flex justify-between pb-8 mt-10 mb-5 border-b">
                                    <span class="text-sm font-semibold uppercase">{{ __('Fecha de entrega') }}</span>
                                    <span class="text-sm font-semibold">
                                        {{ $viewData['order_header']->delivered_on }}
                                </div>
                            @endif
                        </div>
                    </div>
                @endif

                @if ($viewData['order_header']->status == 'CR' || $viewData['order_header']->status == 'ST')
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5 class="pb-8 text-xs font-semibold text-gray-900 retro">{{ __('Solicitud Reembolso') }}
                            </h5>
                        </div>
                        <div class="ibox-content">
                            <form action="{{ route('requestRefund') }}" method="post">
                                {{ csrf_field() }}

                                <input type="hidden" name="id"
                                    value="{{ $viewData['order_header']->order_identification }}">

                                <div class="input-group">
                                    <input type="number" name="cash" autocomplete="off" step="0.01" placeholder="0.00"
                                        min="1" max="100" class="nes-input">

                                </div>
                                <span>Max. 100€</span>
                                <div class="nk-gap"></div>
                                <button type="submit"
                                    class="nk-btn nk-btn-lg nk-btn-rounded nes-btn is-error btn-block">@lang('messages.soli_send')</button>
                            </form>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    </div>


    <body class="bg-gray-100">


        <!-- Main content -->






        <div class="nk-gap-2"></div>
        @if (isset($viewData['order_details']))
            <div class="container">
                <div class="col-lg-12">
                    <h3 class="nk-decorated-h-3">
                        <div class="section-title2">
                            <h3 class="retro">{{ __('Detalle de la orden') }}
                                </b>
                            </h3>
                        </div>
                    </h3>
                </div>
            </div>
            @if (count($viewData['order_details']) > 2)
                <div class="container px-12 py-12 bg-white scroller">
                @else
                    <div class="container px-12 py-12 bg-white">
            @endif

            @foreach (App\AppOrgCategory::whereIn('id', [1, 2, 3, 4,172])->get() as $CatItem)
                @php($contador = 0)

                @foreach ($viewData['order_details'] as $cartItem)
                    @if ($cartItem->product->catid == $CatItem->id)
                        @php($contador += 1)
                    @endif
                @endforeach
                <div class="nk-gap"></div>
                <h5 class="text-gray-900 mt-15 retro" style="display: {{ $contador > 0 ? 'block' : 'none' }};">
                    <b>{{ __($CatItem->name) }}</b>

                </h5>
                <div class="wishlist-table table-responsive">
                    <table style="display: {{ $contador > 0 ? 'table' : 'none' }}; width:100%;">
                        <thead>
                            @if ($CatItem->id == 1)
                                <tr class="bg-red-700" style="font-size:10px;">
                                    <th class="product-cart-img">
                                        <span class="nobr"><i class="fa fa-image"></i></span>
                                    </th>
                                    <th class="product-name">
                                        <span class="nobr">@lang('messages.products')</span>
                                    </th>
                                    <th class="product-name">
                                        <span class="nobr">@lang('messages.box')</span>
                                    </th>
                                    <th class="product-price">
                                        <span class="nobr">@lang('messages.cover')</span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> Manual </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr">@lang('messages.game_product_inventory')</span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> Extra </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> <i class="fa fa-image"></i> </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> <i class="fa fa-comment"></i>
                                        </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> @lang('messages.cants') </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> @lang('messages.price') </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> Total </span>
                                    </th>

                                </tr>
                            @elseif($CatItem->id == 2)
                                <tr class="bg-red-700" style="font-size:10px;">
                                    <th class="product-cart-img">
                                        <span class="nobr"><i class="fa fa-image"></i></span>
                                    </th>
                                    <th class="product-name">
                                        <span class="nobr">@lang('messages.products')</span>
                                    </th>
                                    <th class="product-name">
                                        <span class="nobr">@lang('messages.box')</span>
                                    </th>
                                    <th class="product-price">
                                        <span class="nobr">@lang('messages.inside')</span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> Manual </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr">@lang('messages.console')</span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> Extra </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> Cables </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> <i class="fa fa-image"></i> </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> <i class="fa fa-comment"></i>
                                        </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr">@lang('messages.cants') </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr">@lang('messages.price') </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> Total </span>
                                    </th>
                                    <th class="product-add-to-cart">Accion</th>
                                </tr>

                            @else
                                <tr class="bg-red-700" style="font-size:10px;">
                                    <th class="product-cart-img">
                                        <span class="nobr"><i class="fa fa-image"></i></span>
                                    </th>
                                    <th class="product-name">
                                        <span class="nobr">@lang('messages.products')</span>
                                    </th>
                                    <th class="product-name">
                                        <span class="nobr">@lang('messages.box')</span>
                                    </th>
                                    <th class="product-price">
                                        <span class="nobr">@lang('messages.state_inventory')</span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> Extra </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> <i class="fa fa-image"></i> </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> <i class="fa fa-comment"></i>
                                        </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr">@lang('messages.cants') </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr">@lang('messages.price') </span>
                                    </th>
                                    <th class="product-stock-stauts">
                                        <span class="nobr"> Total </span>
                                    </th>
                                    <th class="product-add-to-cart">Accion</th>
                                </tr>


                            @endif
                        </thead>
                        <tbody>
                            @foreach ($viewData['order_details'] as $detail)
                                @if ($detail->product->catid == $CatItem->id)
                                    @if ($CatItem->id == 1)
                                        <tr>
                                            <td class="product-cart-img">

                                                @if (count($detail->product->images) > 0)
                                                    <center>
                                                        <a href="/product/{{ $detail->product->id }}"><img width="70px"
                                                                height="70px"
                                                                src="{{ url('/images/' . $detail->product->images[0]->image_path) }}"></a>
                                                    </center>
                                                @else
                                                    <center>
                                                        <a href="/product/{{ $detail->product->id }}"><img width="70px"
                                                                height="70px"
                                                                src="{{ url('assets/images/art-not-found.jpg') }}"></a>
                                                    </center>
                                                @endif
                                            </td>
                                            <td class="text-left">
                                                <a href="/product/{{ $detail->product->id }}" target="_blank">
                                                    <span class="text-xs retro font-weight-bold color-primary small">
                                                        <font color="black"><b>{{ $detail->product->name }}
                                                                @if ($detail->product->name_en)
                                                                    <br><small>{{ $detail->product->name_en }}</small>
                                                                @endif
                                                            </b>
                                                        </font>
                                                    </span>
                                                </a>


                                                <br><span class="h7 text-secondary">{{ $detail->product->platform }}
                                                    -
                                                    {{ $detail->product->region }}</span>

                                            </td>


                                            <td class="product-name">
                                                <center>
                                                    <img width="15px" src="/{{ $detail->inventory->box }}"
                                                        data-toggle="popover"
                                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </center>

                                            </td>

                                            <td class="product-stock-status">
                                                <center>
                                                    <img width="15px" src="/{{ $detail->inventory->cover }}"
                                                        data-toggle="popover"
                                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->cover_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </center>

                                            </td>

                                            <td class="product-stock-status">
                                                <center>
                                                    <img width="15px" src="/{{ $detail->inventory->manual }}"
                                                        data-toggle="popover"
                                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->manual_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </center>

                                            </td>

                                            <td class="product-price">
                                                <center>
                                                    <img width="15px" src="/{{ $detail->inventory->game }}"
                                                        data-toggle="popover"
                                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </center>
                                            </td>

                                            <td class="product-price">
                                                <center>
                                                    <img width="15px" src="/{{ $detail->inventory->extra }}"
                                                        data-toggle="popover"
                                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </center>

                                            </td>
                                            <td class="text-center">

                                                @if ($detail->inventory->images->first())
                                                    <a href="{{ '/uploads/inventory-images/' . $detail->inventory->images[0]->image_path }}"
                                                        data-fancybox="{{ $detail->product->id }}">
                                                        <img width="50px" height="50px"
                                                            src="{{ count($detail->inventory->images) > 0 ? url('/uploads/inventory-images/' . $detail->inventory->images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                    </a>

                                                    @foreach ($detail->inventory->images as $p)

                                                        @if (!$loop->first)

                                                            <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                data-fancybox="{{ $detail->product->id }}">
                                                                <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                    width="0px" height="0px" style="position:absolute;" />
                                                            </a>
                                                        @endif

                                                    @endforeach
                                                @else

                                                    <center>
                                                        <font color="black">
                                                            <i class="fa fa-times" data-toggle="popover"
                                                                data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                                data-trigger="hover"></i>
                                                        </font>
                                                    </center>
                                                @endif
                                            </td>
                                            <td class="product-stock-status">
                                                @if ($detail->inventory->comments == '')
                                                    <center>
                                                        <font color="black">
                                                            <i class="fa fa-times" data-toggle="popover"
                                                                data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                                data-trigger="hover"></i>
                                                        </font>
                                                    </center>
                                                @else
                                                    <center>
                                                        <font color="black">
                                                            <i class="fa fa-comment" data-toggle="popover"
                                                                data-content="{{ $detail->inventory->comments }}"
                                                                data-placement="top" data-trigger="hover"></i>
                                                        </font>
                                                    </center>

                                                @endif
                                            </td>

                                            <div class="retro">
                                                <td class="text-center retro h7">
                                                    <font color="black">{{ $detail->quantity }}</font>
                                                </td>
                                            </div>
                                            <td>
                                                <font color="green"> <span
                                                        class="text-right retro font-weight-bold color-primary small text-nowrap"
                                                        data-value="{{ $detail->price }}">
                                                        {{ number_format($detail->price, 2) }}
                                                        € </span> </font>


                                            </td>
                                            <td>
                                                <font color="green"> <span
                                                        class="text-right retro font-weight-bold color-primary small text-nowrap"
                                                        data-value="{{ $detail->price }}">
                                                        {{ number_format($detail->total, 2) }}
                                                        € </span> </font>


                                            </td>

                                        </tr>
                                    @elseif($CatItem->id == 2)
                                        <tr>
                                            <td class="product-cart-img">

                                                @if (count($detail->product->images) > 0)
                                                    <center>
                                                        <a href="/product/{{ $detail->product->id }}"><img width="70px"
                                                                height="70px"
                                                                src="{{ url('/images/' . $detail->product->images[0]->image_path) }}"></a>
                                                    </center>
                                                @else
                                                    <center>
                                                        <a href="/product/{{ $detail->product->id }}"><img width="70px"
                                                                height="70px"
                                                                src="{{ url('assets/images/art-not-found.jpg') }}"></a>
                                                    </center>
                                                @endif
                                            </td>
                                            <td class="text-left">
                                                <a href="/product/{{ $detail->product->id }}" target="_blank">
                                                    <span class="text-xs retro font-weight-bold color-primary small">
                                                        <font color="black"><b>{{ $detail->product->name }}
                                                                @if ($detail->product->name_en)
                                                                    <br><small>{{ $detail->product->name_en }}</small>
                                                                @endif
                                                            </b>
                                                        </font>
                                                    </span>
                                                </a>


                                                <br><span class="h7 text-secondary">{{ $detail->product->platform }}
                                                    -
                                                    {{ $detail->product->region }}</span>

                                            </td>


                                            <td class="product-name">
                                                <center>
                                                    <img width="15px" src="/{{ $detail->inventory->box }}"
                                                        data-toggle="popover"
                                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </center>

                                            </td>

                                            <td class="product-stock-status">
                                                <center>
                                                    <img width="15px" src="/{{ $detail->inventory->cover }}"
                                                        data-toggle="popover"
                                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->cover_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </center>

                                            </td>

                                            <td class="product-stock-status">
                                                <center>
                                                    <img width="15px" src="/{{ $detail->inventory->manual }}"
                                                        data-toggle="popover"
                                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->manual_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </center>

                                            </td>

                                            <td class="product-price">
                                                <center>
                                                    <img width="15px" src="/{{ $detail->inventory->game }}"
                                                        data-toggle="popover"
                                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </center>
                                            </td>

                                            <td class="product-price">
                                                <center>
                                                    <img width="15px" src="/{{ $detail->inventory->extra }}"
                                                        data-toggle="popover"
                                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </center>

                                            </td>
                                            <td class="product-price">
                                                <center>
                                                    <img width="15px" src="/{{ $detail->inventory->inside }}"
                                                        data-toggle="popover"
                                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->inside_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </center>

                                            </td>
                                            <td class="text-center">

                                                @if ($detail->inventory->images->first())
                                                    <a href="{{ '/uploads/inventory-images/' . $detail->inventory->images[0]->image_path }}"
                                                        data-fancybox="{{ $detail->product->id }}">
                                                        <img width="50px" height="50px"
                                                            src="{{ count($detail->inventory->images) > 0 ? url('/uploads/inventory-images/' . $detail->inventory->images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                    </a>

                                                    @foreach ($detail->inventory->images as $p)

                                                        @if (!$loop->first)

                                                            <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                data-fancybox="{{ $detail->product->id }}">
                                                                <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                    width="0px" height="0px" style="position:absolute;" />
                                                            </a>
                                                        @endif

                                                    @endforeach
                                                @else

                                                    <center>
                                                        <font color="black">
                                                            <i class="fa fa-times" data-toggle="popover"
                                                                data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                                data-trigger="hover"></i>
                                                        </font>
                                                    </center>
                                                @endif
                                            </td>
                                            <td class="product-stock-status">
                                                @if ($detail->inventory->comments == '')
                                                    <center>
                                                        <font color="black">
                                                            <i class="fa fa-times" data-toggle="popover"
                                                                data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                                data-trigger="hover"></i>
                                                        </font>
                                                    </center>
                                                @else
                                                    <center>
                                                        <font color="black">
                                                            <i class="fa fa-comment" data-toggle="popover"
                                                                data-content="{{ $detail->inventory->comments }}"
                                                                data-placement="top" data-trigger="hover"></i>
                                                        </font>
                                                    </center>

                                                @endif
                                            </td>

                                            <div class="retro">
                                                <td class="text-center retro h7">
                                                    <font color="black">{{ $detail->quantity }}</font>
                                                </td>
                                            </div>
                                            <td>
                                                <font color="green"> <span
                                                        class="text-right retro font-weight-bold color-primary small text-nowrap"
                                                        data-value="{{ $detail->price }}">
                                                        {{ number_format($detail->price, 2) }}
                                                        € </span> </font>


                                            </td>
                                            <td>
                                                <font color="green"> <span
                                                        class="text-right retro font-weight-bold color-primary small text-nowrap"
                                                        data-value="{{ $detail->price }}">
                                                        {{ number_format($detail->total, 2) }}
                                                        € </span> </font>


                                            </td>

                                        </tr>
                                    @else
                                        <tr>
                                            <td class="product-cart-img">

                                                @if (count($detail->product->images) > 0)
                                                    <center>
                                                        <a href="/product/{{ $detail->product->id }}"><img width="70px"
                                                                height="70px"
                                                                src="{{ url('/images/' . $detail->product->images[0]->image_path) }}"></a>
                                                    </center>
                                                @else
                                                    <center>
                                                        <a href="/product/{{ $detail->product->id }}"><img width="70px"
                                                                height="70px"
                                                                src="{{ url('assets/images/art-not-found.jpg') }}"></a>
                                                    </center>
                                                @endif
                                            </td>
                                            <td class="text-left">
                                                <a href="/product/{{ $detail->product->id }}" target="_blank">
                                                    <span class="text-xs retro font-weight-bold color-primary small">
                                                        <font color="black"><b>{{ $detail->product->name }}
                                                                @if ($detail->product->name_en)
                                                                    <br><small>{{ $detail->product->name_en }}</small>
                                                                @endif
                                                            </b>
                                                        </font>
                                                    </span>
                                                </a>


                                                <br><span class="h7 text-secondary">{{ $detail->product->platform }}
                                                    -
                                                    {{ $detail->product->region }}</span>

                                            </td>


                                            <td class="product-name">
                                                <center>
                                                    <img width="15px" src="/{{ $detail->inventory->box }}"
                                                        data-toggle="popover"
                                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->box_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </center>

                                            </td>


                                            <td class="product-price">
                                                <center>
                                                    <img width="15px" src="/{{ $detail->inventory->game }}"
                                                        data-toggle="popover"
                                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->game_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </center>
                                            </td>

                                            <td class="product-price">
                                                <center>
                                                    <img width="15px" src="/{{ $detail->inventory->extra }}"
                                                        data-toggle="popover"
                                                        data-content="{{ __(App\AppOrgUserInventory::getCondicionName($detail->inventory->extra_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                </center>

                                            </td>
                                            <td class="text-center">

                                                @if ($detail->inventory->images->first())
                                                    <a href="{{ '/uploads/inventory-images/' . $detail->inventory->images[0]->image_path }}"
                                                        data-fancybox="{{ $detail->product->id }}">
                                                        <img width="50px" height="50px"
                                                            src="{{ count($detail->inventory->images) > 0 ? url('/uploads/inventory-images/' . $detail->inventory->images[0]->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                    </a>

                                                    @foreach ($detail->inventory->images as $p)

                                                        @if (!$loop->first)

                                                            <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                data-fancybox="{{ $detail->product->id }}">
                                                                <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                    width="0px" height="0px" style="position:absolute;" />
                                                            </a>
                                                        @endif

                                                    @endforeach
                                                @else

                                                    <center>
                                                        <font color="black">
                                                            <i class="fa fa-times" data-toggle="popover"
                                                                data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                                data-trigger="hover"></i>
                                                        </font>
                                                    </center>
                                                @endif
                                            </td>
                                            <td class="product-stock-status">
                                                @if ($detail->inventory->comments == '')
                                                    <center>
                                                        <font color="black">
                                                            <i class="fa fa-times" data-toggle="popover"
                                                                data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                                data-trigger="hover"></i>
                                                        </font>
                                                    </center>
                                                @else
                                                    <center>
                                                        <font color="black">
                                                            <i class="fa fa-comment" data-toggle="popover"
                                                                data-content="{{ $detail->inventory->comments }}"
                                                                data-placement="top" data-trigger="hover"></i>
                                                        </font>
                                                    </center>

                                                @endif
                                            </td>

                                            <div class="retro">
                                                <td class="text-center retro h7">
                                                    <font color="black">{{ $detail->quantity }}</font>
                                                </td>
                                            </div>
                                            <td>
                                                <font color="green"> <span
                                                        class="text-right retro font-weight-bold color-primary small text-nowrap"
                                                        data-value="{{ $detail->price }}">
                                                        {{ number_format($detail->price, 2) }}
                                                        € </span> </font>


                                            </td>
                                            <td>
                                                <font color="green"> <span
                                                        class="text-right retro font-weight-bold color-primary small text-nowrap"
                                                        data-value="{{ $detail->price }}">
                                                        {{ number_format($detail->total, 2) }}
                                                        € </span> </font>


                                            </td>

                                        </tr>
                                    @endif
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endforeach
        @endif
        </div>
        @php($paypal = 0)
        @if (isset($viewData['total_paypal']))
            @php($paypal = $viewData['total_paypal'])
        @endif


        <div class="nk-gap-3"></div>


    @endsection
    @section('content-script-include')
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
    @endsection
    @section('content-script')
        <script>
            $(document).ready(function() {
                $('.comments').popover();
                $('[data-toggle="popover"]').popover();
            });
        </script>
        <script>
            $('.nk-cancel-btn').click(function() {
                $('#form-cancel-sol').toggle();
            });
        </script>
        <script>
            $(function() {
                $('#sent_on').datepicker();
                $('#delivered_on').datepicker();
            });
        </script>

    @endsection
