<div>
    <div class="p-4 mx-auto mb-6 overflow-hidden bg-white rounded-lg shadow max-w-7xl">
        <div class="row">
            <div class="col-sm-6">
                <div class="mb-3">
                    <label for="" class="mb-2 form-label">Nombre</label>
                    <input wire:model="name" id="name" placeholder="Nombre" class="form-control">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="mb-3">
                    <label for="" class="mb-2 form-label">Habilitado</label>
                    <select wire:model="is_enabled" id="is_enabled" class="form-control show-tick ms select2">
                        <option value="">Seleccione</option>
                        <option value="Y">Si</option>
                        <option value="N">No</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="mb-3">
                    <label for="" class="mb-2 form-label">Descripción</label>
                    <textarea wire:model="description" id="description" cols="10" rows="5" class="form-control"
                        placeholder="Descripcion"></textarea>
                    @error('content') <span class="text-xs text-red-700 error retro">{{ $message }}</span> @enderror
                </div>
            </div>
        </div>
        @if ($accion == 'store')
            <button wire:click="store"
                class="px-4 py-2 mb-2 font-bold text-white bg-blue-500 rounded hover:bg-blue-700">Crear</button>
        @else
            <button wire:click="update"
                class="px-4 py-2 mb-2 font-bold text-white bg-yellow-500 rounded hover:bg-blue-700">Actualizar</button>
            <button wire:click="default"
                class="px-4 py-2 mb-2 font-bold text-white bg-yellow-500 rounded hover:bg-blue-700">Cancelar</button>
        @endif
    </div>
</div>
