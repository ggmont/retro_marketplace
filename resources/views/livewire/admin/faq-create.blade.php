<div>
    <form wire:submit.prevent="store">
        <div class="p-4 mx-auto mb-6 overflow-hidden bg-white rounded-lg shadow max-w-7xl">
            <div class="row">
                <div class="col-sm-12">
                    <div class="mb-3">
                        <div class="mb-3">
                            <label for="" class="mb-2 form-label">Pregunta</label>
                            <input wire:model="question" id="question" placeholder="¿?" class="form-control">
                            @error('question') <span class="error retro text-xs text-red-700">{{ $message }}</span> @enderror
                        </div>
                    </div>

                </div>
                <br>

                <div class="col-sm-12">
                    <div class="mb-3" wire:ignore>
                        <label for="" class="mb-2 form-label">Respuesta</label>
                        <textarea wire:model="answer" id="editor" cols="10" rows="5" class="form-control"
                            placeholder="Respuesta"></textarea>
                        @error('answer') <span class="error retro text-xs text-red-700">{{ $message }}</span> @enderror
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="mb-3">
                        <label for="" class="mb-2 form-label">Habilitado</label>
                        <select wire:model="is_enabled" id="is_enabled" class="form-control show-tick ms select2">
                            <option value="">Selecciona</option>
                            <option value="Y">Si</option>
                            <option value="N">No</option>
                        </select>
                        @error('is_enabled') <span class="error retro text-xs text-red-700">{{ $message }}</span> @enderror
                    </div>
                </div>
            </div>
            @if ($accion == 'store')
                <button wire:click="store"
                    class="px-4 py-2 mb-2 font-bold text-white bg-blue-500 rounded hover:bg-blue-700">Crear</button>
            @else
                <button wire:click="update"
                    class="px-4 py-2 mb-2 font-bold text-white bg-yellow-500 rounded hover:bg-blue-700">Actualizar</button>
                <button wire:click="default"
                    class="px-4 py-2 mb-2 font-bold text-white bg-yellow-500 rounded hover:bg-blue-700">Cancelar</button>
            @endif
    </form>
</div>
@push('js')
    <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/classic/ckeditor.js"></script>

    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .then(function(editor) {
                editor.model.document.on('change:data', () => {
                    @this.set('answer', editor.getData());
                })
            })
            .catch(error => {
                console.error(error);
            });
    </script>
@endpush
</div>
