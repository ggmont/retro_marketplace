<div>


    <div class="row">
        <div class="col-md-12">
            <table id="table-two-axis" class="two-axis">
                <thead>
                    <tr class="border-b border-gray-500 hover:bg-gray-200">
                        <th class="text-xs">Nombre completo</th>
                        <th class="text-xs">Usuario</th>
                        <th class="text-xs">Producto</th>
                        <th class="text-xs">Imagen</th>
                        <th class="text-xs">Comentario</th>
                        <th class="text-xs">Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($historial_inventory as $r)
                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                            <td class="text-sm font-semibold text-gray-900">
                                {{ $r->user->first_name . ' ' . $r->user->last_name }}
                            </td>
                            <td class="text-base text-gray-900">
                                {{ $r->user->user_name }}
                            </td>
                            <td class="text-base text-gray-900">
                                <a href="{{ route('product-show', $r->product->id) }}">

                                    <font color="black">
                                        <b>{{ $r->product->name }}
                                            @if ($r->product->name_en)
                                                <br><small>{{ $r->product->name_en }}</small>
                                            @endif
                                        </b>
                                    </font>
                                </a>
                                    <br>
                                    @if ($r->product->catid == 1)
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->box }}"
                                            data-toggle="popover"
                                            data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($r->box_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->cover }}"
                                            data-toggle="popover"
                                            data-content="Carátula - {{ __(App\AppOrgUserInventory::getCondicionName($r->cover_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->manual }}"
                                            data-toggle="popover"
                                            data-content="Manual - {{ __(App\AppOrgUserInventory::getCondicionName($r->manual_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->game }}"
                                            data-toggle="popover"
                                            data-content="Juego - {{ __(App\AppOrgUserInventory::getCondicionName($r->game_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->extra }}"
                                            data-toggle="popover"
                                            data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($r->extra_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                    @elseif($r->product->catid == 2)
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->box }}" data-toggle="popover"
                                            data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($r->box_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img  style="display: inline-block;"width="15px" src="/{{ $r->cover }}" data-toggle="popover"
                                            data-content="Carátula - {{ __(App\AppOrgUserInventory::getCondicionName($r->cover_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->manual }}" data-toggle="popover"
                                            data-content="Manual - {{ __(App\AppOrgUserInventory::getCondicionName($r->manual_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->game }}" data-toggle="popover"
                                            data-content="Juego - {{ __(App\AppOrgUserInventory::getCondicionName($r->game_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->extra }}" data-toggle="popover"
                                            data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($r->extra_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->inside }}" data-toggle="popover"
                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($r->inside_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                    @else
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->box }}" data-toggle="popover"
                                            data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($r->box_condition)) }}"
                                            data-placement="top" data-trigger="hover">

                                        <img style="display: inline-block;" width="15px" src="/{{ $r->game }}" data-toggle="popover"
                                            data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($r->game_condition)) }}"
                                            data-placement="top" data-trigger="hover">

                                        <img style="display: inline-block;" width="15px" src="/{{ $r->extra }}" data-toggle="popover"
                                            data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($r->extra_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                    @endif

                               
                            </td>
                            <td>
                                @if ($r->comments == '')
                                    <font color="black">
                                        <i class="fa fa-times" data-toggle="popover" data-content="@lang('messages.not_working_profile')"
                                            data-placement="top" data-trigger="hover"></i>
                                    @else
                                        <font color="black">
                                            <i class="fa fa-comment" data-toggle="popover"
                                                data-content="{{ $r->comments }}" data-placement="top"
                                                data-trigger="hover"></i>
                                        </font>
                                @endif
                            </td>
                            <td>
                                @if ($r->images->first())
                                    <a href="{{ url('/uploads/inventory-images/' . $r->images->first()->image_path) }}"
                                        class="fancybox" data-fancybox="{{ $r->id }}">
                                        <img src="{{ url('/uploads/inventory-images/' . $r->images->first()->image_path) }}"
                                            width="50px" height="50px" />
                                    </a>
                                    @foreach ($r->images as $p)
                                        @if (!$loop->first)
                                            <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                data-fancybox="{{ $r->id }}">
                                                <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                    width="0px" height="0px" style="position:absolute;" />
                                            </a>
                                        @endif
                                    @endforeach
                                @else
                                    <font color="black">
                                        <i class="fa fa-times" data-toggle="popover" data-content="@lang('messages.not_working_profile')"
                                            data-placement="top" data-trigger="hover"></i>
                                    </font>
                                @endif
                            </td>
                            <td>
                                -
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="flex flex-col items-center">
                @if ($historial_inventory->count())
                    <div
                        class="flex items-center font-bold justify-between px-1 py-1 border-t border-gray-200 sm:px-6">
                        {{ $historial_inventory->links('pagination') }}
                    </div>
                @else
                    <div
                        class="flex items-center justify-between px-1 py-1 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                        No hay resultados para la busqueda  
                    </div>
                @endif
            </div>
        </div>

    </div>



</div>

<script>
    document.addEventListener('livewire:load', function() {
        var $inventoryAdd = $('.br-btn-product-add');
        $(function() {
                $('[data-toggle="popover"]').popover()
            }),
            $('.nk-btn-modify-inventory').click(function() {
                var url = $(this).data('href');
                var tr = $(this).closest('tr');
                var sel = $(this).closest('tr').find('select').prop('value');
                console.log(sel);
                //console.log(url);
                location.href = `${url}/${sel}`;
            }),
            $inventoryAdd.click(function() {
                var inventoryId = $(this).data('inventory-id');
                var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty').val();
                var stock = $(this).closest('tr').find('div.qty-new');
                var tr = $(this).closest('tr');
                var select = $(this).closest('tr').find('select.br-btn-product-qty');

                $.showBigOverlay({
                    message: '{{ __('Agregando producto a tu carro de compras') }}',
                    onLoad: function() {
                        $.ajax({
                            url: '{{ url('/cart/add_item') }}',
                            data: {
                                _token: $('meta[name="csrf-token"]').attr(
                                    'content'),
                                inventory_id: inventoryId,
                                inventory_qty: inventoryQty,
                            },
                            dataType: 'JSON',
                            type: 'POST',
                            success: function(data) {
                                if (data.error == 0) {
                                    $('#br-cart-items').text(data.items);
                                    stock.html(data.Qty);
                                    if (data.Qty == 0) {
                                        tr.remove();
                                    } else {
                                        select.html('');
                                        for (var i = 1; i < data.Qty +
                                            1; i++) {
                                            select.append('<option value=' +
                                                i + '>' + i +
                                                '</option>');
                                        }
                                    }
                                } else {

                                }
                                $.showBigOverlay('hide');
                            },
                            error: function(data) {
                                $.showBigOverlay('hide');
                            }
                        })
                    }
                });
            });
    })
</script>
