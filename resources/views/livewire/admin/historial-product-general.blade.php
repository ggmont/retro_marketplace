<div>

    <div class="row">
        <div class="flex items-center col-md-6">
            <label for="simple-search" class="sr-only">Buscar</label>
            <div class="relative w-full">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <i class="fa-sharp fa-regular fa-user"></i>
                </div>
                <input type="text" wire:model="search"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Buscar por usuario">
            </div>
        </div>

        <div class="flex items-center col-md-6">
            <label for="simple-search" class="sr-only">Buscar</label>
            <div class="relative w-full">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <i class="fas fa-sharp fa-gamepad "></i>
                </div>
                <input type="text" wire:model="search_product"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Buscar por producto">
            </div>
            <a href="{{ route('product_create') }}"
                class="p-2.5 ml-2 text-sm font-medium text-white bg-red-700 rounded-lg border border-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                <i class="fa-solid fa-book"></i>
                <span class="sr-only fa-solid fa-plus">Buscar</span>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table id="table-two-axis" class="two-axis">
                <thead>
                    <tr class="border-b border-gray-500 hover:bg-gray-200">
                        <th class="text-xs">NOMBRE COMPLETO</th>
                        <th class="text-xs">USUARIO</th>
                        <th class="text-xs">PRODUCTO</th>
                        <th class="text-xs"><i class="fa fa-image"></i></th>
                        <th class="text-xs"><i class="fa fa-comment"></i></th>
                        <th class="text-xs">ACCION</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($historial_inventory as $r)
                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                            <td class="text-sm font-semibold text-gray-900">
                                {{ $r->user->first_name . ' ' . $r->user->last_name }} </td>
                            <td class="text-sm font-semibold text-gray-900"> {{ $r->user->user_name }} </td>
                            <td class="px-6 py-4 whitespace-nowrap">
                                <div class="text-sm text-gray-900">
                                    <a href="{{ route('product-show', $r->product->id) }}" target="_blank">
                                        <span class="text-sm retro font-bold color-primary">
                                            <font color="black">
                                                <b>{{ $r->product->name }}
                                                    @if ($r->product->name_en)
                                                        <br><small>{{ $r->product->name_en }}</small>
                                                    @endif
                                                </b>
                                            </font>
                                    </a>
                                    <br>
                                    @if ($r->product->catid == 1)
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->box }}"
                                            data-toggle="popover"
                                            data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($r->box_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->cover }}"
                                            data-toggle="popover"
                                            data-content="Carátula - {{ __(App\AppOrgUserInventory::getCondicionName($r->cover_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->manual }}"
                                            data-toggle="popover"
                                            data-content="Manual - {{ __(App\AppOrgUserInventory::getCondicionName($r->manual_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->game }}"
                                            data-toggle="popover"
                                            data-content="Juego - {{ __(App\AppOrgUserInventory::getCondicionName($r->game_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->extra }}"
                                            data-toggle="popover"
                                            data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($r->extra_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                    @elseif($r->product->catid == 2)
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->box }}"
                                            data-toggle="popover"
                                            data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($r->box_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;"width="15px" src="/{{ $r->cover }}"
                                            data-toggle="popover"
                                            data-content="Carátula - {{ __(App\AppOrgUserInventory::getCondicionName($r->cover_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;" width="15px" src="/{{ $r->manual }}"
                                            data-toggle="popover"
                                            data-content="Manual - {{ __(App\AppOrgUserInventory::getCondicionName($r->manual_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;" width="15px"
                                            src="/{{ $r->game }}" data-toggle="popover"
                                            data-content="Juego - {{ __(App\AppOrgUserInventory::getCondicionName($r->game_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;" width="15px"
                                            src="/{{ $r->extra }}" data-toggle="popover"
                                            data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($r->extra_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        <img style="display: inline-block;" width="15px"
                                            src="/{{ $r->inside }}" data-toggle="popover"
                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($r->inside_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                    @else
                                        <img style="display: inline-block;" width="15px"
                                            src="/{{ $r->box }}" data-toggle="popover"
                                            data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($r->box_condition)) }}"
                                            data-placement="top" data-trigger="hover">

                                        <img style="display: inline-block;" width="15px"
                                            src="/{{ $r->game }}" data-toggle="popover"
                                            data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($r->game_condition)) }}"
                                            data-placement="top" data-trigger="hover">

                                        <img style="display: inline-block;" width="15px"
                                            src="/{{ $r->extra }}" data-toggle="popover"
                                            data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($r->extra_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                    @endif
                                    </span>

                                    <br>
                                    <span class="h7 text-secondary">{{ $r->product->platform }}
                                        -
                                        {{ $r->product->region }}</span>
                                </div>
                            </td>
                            <td class="text-base text-gray-900">
                                @if ($r->images->first())
                                    <a href="{{ url('/uploads/inventory-images/' . $r->images->first()->image_path) }}"
                                        class="fancybox" data-fancybox="{{ $r->id }}">
                                        <img src="{{ url('/uploads/inventory-images/' . $r->images->first()->image_path) }}"
                                            width="50px" height="50px" />
                                    </a>
                                    @foreach ($r->images as $p)
                                        @if (!$loop->first)
                                            <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                data-fancybox="{{ $r->id }}">
                                                <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                    width="0px" height="0px" style="position:absolute;" />
                                            </a>
                                        @endif
                                    @endforeach
                                @else
                                    <font color="black">
                                        <i class="fa fa-times" data-toggle="popover" data-content="@lang('messages.not_working_profile')"
                                            data-placement="top" data-trigger="hover"></i>
                                    </font>
                                @endif
                            </td>
                            <td>
                                @if ($r->comments == '')
                                    <font color="black">
                                        <i class="fa fa-times" data-toggle="popover" data-content="@lang('messages.not_working_profile')"
                                            data-placement="top" data-trigger="hover"></i>
                                    @else
                                        <font color="black">
                                            <i class="fa fa-comment" data-toggle="popover"
                                                data-content="{{ $r->comments }}" data-placement="top"
                                                data-trigger="hover"></i>
                                        </font>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('inventory_edit', $r->id) }}" class="btn btn-success btn-xs">
                                    <i class="far fa-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="flex flex-col items-center">
                @if ($historial_inventory->count())
                    <div
                        class="flex items-center font-bold justify-between px-1 py-1 border-t border-gray-200 sm:px-6">
                        {{ $historial_inventory->links('pagination') }}
                    </div>
                @else
                    <div
                        class="flex items-center justify-between px-1 py-1 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                        No hay resultados para la busqueda "{{ $search }}" en la página
                        {{ $page }}
                        al mostrar {{ $perPage }}
                    </div>
                @endif
            </div>
        </div>

    </div>

    <script>
        const table = new basictable('.table');

        new basictable('#table-breakpoint', {
            breakpoint: 768,
        });

        new basictable('#table-container-breakpoint', {
            containerBreakpoint: 485,
        });

        new basictable('#table-force-off', {
            forceResponsive: false,
        });

        new basictable('#table-max-height', {
            tableWrap: true
        });

        new basictable('#table-no-resize', {
            noResize: true,
        });

        new basictable('#table-two-axis');
    </script>

    <script>
        document.addEventListener('livewire:load', function() {
            // Inicializar los popovers
            $(function() {
                $('[data-toggle="popover"]').popover();
            });
        });
    </script>


</div>
