<div>

    <div class="row">
        <div class="col-md-12" style="background-color: #ffffff; border: solid 1px #db2e2e">
            <br>
            <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase" for="grid-city">
                Detalles Basicos
            </h5>
            <br>
            <form class="w-full" action="{{ route('product_store') }}" method="POST" enctype="multipart/form-data">
                {{ method_field('POST') }}
                {{ csrf_field() }}
                <div class="flex flex-wrap mb-2 -mx-3">
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                        <label for="name"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nombre <font
                                color="red">*</font></label>
                        <input type="text" name="name"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="Retro" required>

                    </div>

                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                        <label for="name_en"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nombre Alternativo
                            <font color="red">*</font>
                        </label>
                        <input type="text" name="name_en"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="RGM" required>

                    </div>

                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                        <label for="release_date"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Fecha
                            de Lanzamiento <font color="red">(Opcional)</font></label>
                        <input type="text" id="datepicker" name="release_date"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="14/03/2023">

                    </div>
                </div>
                <br>
                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase" for="grid-city">
                    Detalles del Producto
                </h5>
                <br>
                <div class="flex flex-wrap mb-2 -mx-3">
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                        <label for="category"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Categoria
                            <font color="red">*</font>
                        </label>

                        <div class="relative">
                            <select
                                class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                id="grid" name="prod_category_id" required="">
                                <option value="">Selecciona</option>
                                @foreach ($categories as $c)
                                    <option value="{{ $c->id }}">{{ $c->category_text }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                        <label for="gener"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Genero
                            <font color="red">(Opcional)</font>
                        </label>

                        <div class="relative">
                            <select
                                class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                id="genero" name="game_category">
                                <option value="">Selecciona</option>
                                @foreach ($genero as $g)
                                    <option value="{{ $g->value }}">{{ $g->value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                        <label for="location"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Localización
                            <font color="red">*</font>
                        </label>

                        <div class="relative">
                            <select
                                class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                id="location" name="game_location" required="">
                                <option value="">Selecciona</option>
                                @foreach ($location as $l)
                                    <option value="{{ $l->value }}">{{ $l->value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="flex flex-wrap mb-2 -mx-3">
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                        <label for="generation"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Generación
                            <font color="red">(Opcional)</font>
                        </label>

                        <div class="relative">
                            <select
                                class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                id="generation" name="game_generation">
                                <option value="">Selecciona</option>
                                @foreach ($generation as $ge)
                                    <option value="{{ $ge->value }}">{{ $ge->value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                        <label for="language"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Lenguaje
                            <font color="red">*</font>
                        </label>

                        <div class="relative">
                            <select
                                class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                id="language" name="language[]" multiple="multiple" required="">
                                @foreach ($language as $la)
                                    <option value="{{ $la->value }}">{{ $la->value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                        <label for="box_language"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Lenguaje de caja
                            <font color="red">(Opcional)</font>
                        </label>

                        <div class="relative">
                            <select
                                class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                id="box_language" name="box_language[]" multiple="multiple">
                                @foreach ($language as $la)
                                    <option value="{{ $la->value }}">{{ $la->value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase" for="grid-city">
                    Detalles Tecnicos
                </h5>
                <br>
                <div class="flex flex-wrap mb-2 -mx-3">
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                        <label for="media"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Media
                            <font color="red">*</font>
                        </label>

                        <div class="relative">
                            <select
                                class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                id="media" name="media" required="">
                                <option value="">Selecciona</option>
                                @foreach ($media as $m)
                                    <option value="{{ $m->value }}">{{ $m->value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                        <label for="platform"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Plataforma
                            <font color="red">*</font>
                        </label>

                        <div class="relative">
                            <select
                                class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                id="platform" name="platform" required="">
                                <option value="">Selecciona</option>
                                @foreach ($platform as $p)
                                    <option value="{{ $p->value }}">{{ $p->value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                        <label for="region"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Región
                            <font color="red">*</font>
                        </label>

                        <div class="relative">
                            <select
                                class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                id="region" name="region" required="">
                                <option value="">Selecciona</option>
                                @foreach ($region as $r)
                                    <option value="{{ $r->value }}">{{ $r->value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <br>

                <div class="flex flex-wrap mb-2 -mx-3">
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                        <label for="ean_upc" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">EAN
                            / UPC
                            <font color="red">(Opcional)</font>
                        </label>

                        <input type="text" name="ean_upc"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="010203">

                    </div>
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                        <label for="volume"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Volume
                            <font color="red">*</font>
                        </label>

                        <input type="number" step="0.0001" name="volume"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="0.0001" required="">


                    </div>
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                        <label for="volume"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Peso
                            <font color="red">*</font>
                        </label>

                        <input type="text" step="0.0001" name="weight"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="KG" required="">

                    </div>
                </div>

                <br>

                <div class="flex flex-wrap mb-2 -mx-3">
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                        <label for="large"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Largo
                            <font color="red">*</font>
                        </label>

                        <input type="number" name="large"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="10" required="">

                    </div>
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                        <label for="width"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Ancho
                            <font color="red">*</font>
                        </label>

                        <input type="number" step="0.0001" name="width"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="20" required="">

                    </div>
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">

                        <label for="high"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Alto
                            <font color="red">*</font>
                        </label>

                        <input type="number" name="high"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="30" required="">

                    </div>
                </div>

                <br>

                <div class="col-md-12">

                    <label for="price" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Precio
                        estandar
                        <font color="red">*</font>
                    </label>

                    <input type="number" autocomplete="off" data-number="2" step="0.01" name="price"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        placeholder="0,00" required="">

                </div>
                <br>
                <label for="image" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Agregar Imagen del Producto
                    <font color="red">(Opcional)</font>
                </label>

                <div class="file-loading">
                    <input id="input-20" name="image_path[]" type="file" data-browse-on-zone-click="true"
                        multiple="multiple">
                </div>
                <br>
                <div class="form-group col-sm-12">
                    <label for="comments" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Descripción
                        <font color="red">(Opcional)</font>
                    </label>
                    <textarea id="comments" name="comments" cols="20" rows="10" class="form-control"
                        placeholder="Añada un comentario con respecto al producto"></textarea>
                </div>



                <div class="text-center form-group col-md-12">
                    <br>
                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i>
                        Atrás</a>
                    <input type="submit" class="btn btn-danger" value="Registrar">
                </div>

            </form>
        </div>
    </div>
</div>
