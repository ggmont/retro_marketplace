<div>
    <div class="row">
        <div class="flex items-center col-md-6">
            <label for="simple-search" class="sr-only">Buscar</label>
            <div class="relative w-full">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <i class="fa-sharp fa-regular fa-user"></i>
                </div>
                <input type="text" wire:model="search_user"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Buscar por Usuario">
            </div>
        </div>

        <div class="flex items-center col-md-6">
            <label for="simple-search" class="sr-only">Buscar</label>
            <div class="relative w-full">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <i class="fa-sharp fa-regular fa-user"></i>
                </div>
                <input type="text" wire:model="search_product"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Buscar por Producto">
            </div>
        </div>
        <div class="col-md-12">
            <div wire:ignore>
                <select wire:model="search_status" class="form-control br-filtering" data-filtering="search_status"
                    data-width="100%">
                    <option value="">{{ __('Estados') }}</option>
                    <option value="A">Aprobados</option>
                    <option value="P">Pendiente</option>
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table id="table-two-axis" class="two-axis">
                <thead>
                    <tr class="border-b border-gray-500 hover:bg-gray-200">
                        <th class="text-xs">USUARIO</th>
                        <th class="text-xs">PRODUCTO</th>
                        <th class="text-xs">CATEGORIA</th>
                        <th class="text-xs">TIPO</th>
                        <th class="text-xs">PLATAFORMA</th>
                        <th class="text-xs">REGION</th>
                        <th class="text-xs">FECHA</th>
                        <th class="text-xs">STATUS</th>
                        <th class="text-xs">ACCION</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($product_request as $p)
                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                            <td class="text-sm font-semibold text-gray-900"> {{ $p->user->user_name }} </td>
                            <td class="text-sm font-semibold text-gray-900">
                                {{ $p->product }}
                            </td>
                            <td class="text-base text-gray-900">
                                {{ $p->category }}
                            </td>
                            <td class="text-base text-gray-900">
                                @if ($p->auction == 'Y')
                                   <font color="red">OFERTA</font> 
                                @else
                                    NORMAL
                                @endif
                            </td>
                            <td class="text-base text-gray-900">
                                {{ $p->platform }}
                            </td>
                            <td class="text-base text-gray-900">
                                {{ $p->region }}
                            </td>
                            <td class="text-base text-gray-900">
                                {{ date_format($p->created_at, 'Y/m/d') }}
                            </td>
                            <td class="text-base text-gray-900">
                                @if ($p->status == 'P')
                                    Pendiente
                                @else
                                    Aprobado
                                    por <br> - {{ $p->aprobed_by }} -
                                @endif
                            </td>
                            <td>

                                @if ($p->status == 'P')
                                    <a href="{{ route('edit_product_request', [$p->id]) }}"
                                        class="btn btn-primary btn-xs">
                                        <font color="white"><i class="far fa-edit"></i></font>
                                    </a>
                                @else
                                    <i class="fas fa-thumbs-up fa-2x text-red-700"></i>
                                @endif

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="flex flex-col items-center">
                @if ($product_request->count())
                    <div class="flex items-center font-bold justify-between px-1 py-1 border-t border-gray-200 sm:px-6">
                        {{ $product_request->links('pagination') }}
                    </div>
                @else
                    <div
                        class="flex items-center justify-between px-1 py-1 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                        No hay resultados para la busqueda en la página
                        {{ $page }}
                        al mostrar {{ $perPage }}
                    </div>
                @endif
            </div>
        </div>

    </div>

    <script>
        const table = new basictable('.table');

        new basictable('#table-breakpoint', {
            breakpoint: 768,
        });

        new basictable('#table-container-breakpoint', {
            containerBreakpoint: 485,
        });

        new basictable('#table-force-off', {
            forceResponsive: false,
        });

        new basictable('#table-max-height', {
            tableWrap: true
        });

        new basictable('#table-no-resize', {
            noResize: true,
        });

        new basictable('#table-two-axis');
    </script>

</div>
