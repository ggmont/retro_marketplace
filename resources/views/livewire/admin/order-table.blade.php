<div>

    <div class="row">
        <div class="flex items-center col-md-6">
            <label for="simple-search" class="sr-only">Buscar</label>
            <div class="relative w-full">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <i class="fa-sharp fa-regular fa-user"></i>
                </div>
                <input type="text" wire:model="search_buyer"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Buscar por Comprador">
                <input type="text" wire:model="search_buyer_name"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Buscar por Usuario Comprador">
            </div>
        </div>

        <div class="flex items-center col-md-6">
            <label for="simple-search" class="sr-only">Buscar</label>
            <div class="relative w-full">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <i class="fa-sharp fa-regular fa-user"></i>
                </div>
                <input type="text" wire:model="search_seller"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Buscar por Vendedor">
                <input type="text" wire:model="search_seller_name"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Buscar por Usuario Vendedor">
            </div>
        </div>
        <div class="col-md-12">
            <div wire:ignore>
                <select wire:model="search_status" class="select2 form-control br-filtering"
                    data-filtering="search_status" data-width="100%">
                    <option value="">{{ __('Estados') }}</option>
                    <option value="CR">
                        {{ __('Pedido pendiente de envío') }}</option>
                    <option value="RP">
                        {{ __('Pedido sin pagar') }}</option>
                    <option value="PC">
                        {{ __('Solicitud de cancelación de un pedido pagado') }}</option>
                    <option value="CN">
                        {{ __('Pedido cancelado') }}</option>
                    <option value="ST">
                        {{ __('Pedido enviado') }}</option>
                    <option value="DD">
                        {{ __('Pedido entregado') }}</option>
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table id="table-two-axis" class="two-axis">
                <thead>
                    <tr class="border-b border-gray-500 hover:bg-gray-200">
                        <th class="text-xs">ID</th>
                        <th class="text-xs">IP</th>
                        <th class="text-xs">USUARIO C</th>
                        <th class="text-xs">COMPRADOR</th>
                        <th class="text-xs">VENDEDOR</th>
                        <th class="text-xs">USUARIO V</th>
                        <th class="text-xs">STATUS</th>
                        <th class="text-xs">FECHA</th>
                        <th class="text-xs">TOTAL</th>
                        <th class="text-xs">ACCION</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $o)
                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                            <td class="text-sm font-semibold text-gray-900"> {{ $o->order_identification }}</td>
                            <td class="text-sm font-semibold text-gray-900">
                                {{ App\AppOrgUserEvent::where('message_values', 'like', '%' . $o->order_identification . '%')->first() ? App\AppOrgUserEvent::where('message_values', 'like', '%' . $o->order_identification . '%')->first()->message_values['ip_user'] : ' - ' }}
                            </td>
                            <td class="text-base text-gray-900">
                                {{ $o->buyer->user_name }}
                            </td>
                            <td class="text-base text-gray-900">
                                {{ $o->buyer ? $o->buyer->first_name . ' ' . $o->buyer->last_name : '' }}
                            </td>
                            <td class="text-base text-gray-900">
                                {{ $o->seller ? $o->seller->first_name . ' ' . $o->seller->last_name : '' }}
                            </td>
                            <td class="text-base text-gray-900">
                                {{ $o->seller->user_name }}
                            </td>
                            <td class="text-base text-gray-900">
                                {{ $o->statusDes }}
                            </td>
                            <td class="text-base text-gray-900">
                                {{ $o->updated_at->format('d/m/Y') }}
                            </td>
                            <td class="text-base text-gray-900">
                                {{ number_format($o->total, 2) }} €
                            </td>
                            <td>
                                <a href="{{ route('view_order', [$o->id]) }}" class="btn btn-primary btn-xs">
                                    <font color="white">
                                        <i class="far fa-eye"></i>
                                    </font>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="flex flex-col items-center">
                @if ($orders->count())
                    <div class="flex items-center font-bold justify-between px-1 py-1 border-t border-gray-200 sm:px-6">
                        {{ $orders->links('pagination') }}
                    </div>
                @else
                    <div
                        class="flex items-center justify-between px-1 py-1 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                        No hay resultados para la busqueda en la página
                        {{ $page }}
                        al mostrar {{ $perPage }}
                    </div>
                @endif
            </div>
        </div>

    </div>


    

    <script>
        document.addEventListener('livewire:load', function() {
            $('.select2').select2({

            });
            $('.select2').on('change', function() {
                /*  alert(this.value) */
                @this.set('search_status', this.value);
            });
        })
        const table = new basictable('.table');

        new basictable('#table-breakpoint', {
            breakpoint: 768,
        });

        new basictable('#table-container-breakpoint', {
            containerBreakpoint: 485,
        });

        new basictable('#table-force-off', {
            forceResponsive: false,
        });

        new basictable('#table-max-height', {
            tableWrap: true
        });

        new basictable('#table-no-resize', {
            noResize: true,
        });

        new basictable('#table-two-axis');
    </script>
</div>
