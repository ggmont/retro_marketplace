<div>

    <div class="flex items-center">
        <label for="simple-search" class="sr-only">Buscar</label>
        <div class="relative w-full">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor"
                    viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                        d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                        clip-rule="evenodd"></path>
                </svg>
            </div>
            <input type="text" wire:model="search"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Buscar por nombre">
        </div>
        <a href="{{ route('product_create') }}"
            class="p-2.5 ml-2 text-sm font-medium text-white bg-red-700 rounded-lg border border-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
            <i class="fa fa-plus"></i>
            <span class="sr-only fa-solid fa-plus">Buscar</span>
        </a>
    </div>

    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <button type="button" class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed"
            data-toggle="modal" data-target="#exampleModal">
            <span class="fa-solid fa-filter"></span>
        </button>
    @else
        <div class="row">

            <div class="col-md-12">
                <div wire:ignore>
                    <select wire:model="search_platform" class="select2 form-control br-filtering" data-width="100%">
                        <option value="">{{ __('Plataforma') }}</option>
                        @foreach ($platform as $p)
                            <option value="{{ $p->value }}">{{ $p->value }}</option>
                        @endforeach
                    </select>
                </div>
                <div wire:ignore class="input-group">

                    <select wire:model="search_category" class="categories form-control br-filtering">
                        <option value="">{{ __('Categoria') }}</option>
                        @foreach ($category as $c)
                            <option value="{{ $c->category_text }}">{{ $c->category_text }}
                            </option>
                        @endforeach
                    </select>

                    <select wire:model="search_language" class="language br-filtering form-control">
                        <option value="">{{ __('Idioma Juego') }}</option>
                        @foreach ($language as $l)
                            <option value="{{ $l->value_id }}">{{ $l->value_id }}
                            </option>
                        @endforeach
                    </select>

                    <select wire:model="search_box_language" class="box_language form-control br-filtering">
                        <option value="">{{ __('Idioma Caja') }}</option>
                        @foreach ($language as $l)
                            <option value="{{ $l->value_id }}">{{ $l->value_id }}
                            </option>
                        @endforeach
                    </select>

                    <select wire:model="search_region" class="region wire:form-control br-filtering">
                        <option value="">{{ __('Region') }}</option>
                        @foreach ($region as $r)
                            <option value="{{ $r->value_id }}">{{ $r->value_id }}
                            </option>
                        @endforeach
                    </select>
                    <select wire:model="search_enabled" class="form-control br-filtering" title="">
                        <option value="">{{ __('Habilitado') }}</option>

                        <option value="Y">Si
                        </option>

                        <option value="N">No
                        </option>

                    </select>

                </div>

            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <table id="table-two-axis" class="two-axis">
                <thead>
                    <tr class="border-b border-gray-500 hover:bg-gray-200">
                        <th class="text-xs">Nombre</th>
                        <th class="text-xs">Imagen</th>
                        <th class="text-xs">Categoria</th>
                        <th class="text-xs">Idioma Juego</th>
                        <th class="text-xs">Idioma Caja</th>
                        <th class="text-xs">Plataforma</th>
                        <th class="text-xs">Region</th>
                        <th class="text-xs">Accion</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($product as $p)
                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                            <td class="text-sm font-semibold text-gray-900">{{ $p->name }}</td>
                            <td>
                                <div class="flex items-center">
                                    <div class="flex-shrink-0 w-10 h-10">
                                        @if ($p->images->first())
                                            <a href="{{ url('/images/' . $p->images->first()->image_path) }}" data-fancybox="RGM{{ $p->id }}">
                                                <img class="fancybox product-thumbnail d-block w-10 h-10 object-cover rounded" src="{{ count($p->images) > 0 ? url('/images/' . $p->images->first()->image_path) : url('assets/images/art-not-found.jpg') }}" alt="Product Image">
                                            </a>
                                        @else
                                            <a href="{{ url('img/art-not-found.jpg') }}" data-fancybox="RGM{{ $p->id }}">
                                                <img class="fancybox product-thumbnail d-block w-10 h-10 object-cover rounded" src="{{ url('img/art-not-found.jpg') }}" alt="Product Image">
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </td>
                            <td class="text-base text-gray-900">
                                @if ($p->categoryProd == '')
                                    <b>No info</b>
                                @else
                                    {{ $p->category->name }}
                                @endif
                            </td>
                            <td class="text-base text-gray-900">
                                @if ($p->language == '')
                                    <b>No info</b>
                                @elseif($p->language == 'ESP')
                                    Castellano
                                @elseif($p->language == 'ESP')
                                    Castellano
                                @elseif($p->language == 'ESP')
                                    Castellano
                                @elseif($p->language == 'ESP')
                                    Castellano
                                @else
                                    {{ $p->language }}
                                @endif
                            </td>
                            <td class="text-base text-gray-900">
                                @if ($p->box_language == '')
                                    <b>No info</b>
                                @else
                                    {{ $p->box_language }}
                                @endif
                            </td>
                            <td class="text-base text-gray-900">{{ $p->platform }}</td>
                            <td class="text-base text-gray-900">{{ $p->region }}</td>
                            <td>
                                <a href="{{ route('edit_product', [$p->id]) }}" class="btn btn-success btn-xs">
                                    <font color="white">
                                        <i class="fa fa-edit"></i>
                                    </font>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="flex flex-col items-center">
                @if ($product->count())
                    <div class="flex items-center font-bold justify-between px-1 py-1 border-t border-gray-200 sm:px-6">
                        {{ $product->links('pagination') }}
                    </div>
                @else
                    <div
                        class="flex items-center justify-between px-1 py-1 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                        No hay resultados para la busqueda "{{ $search }}" en la página
                        {{ $page }}
                        al mostrar {{ $perPage }}
                    </div>
                @endif
            </div>
        </div>

    </div>

    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.filter_by') :</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block pb-1 pt-2">

                                <form>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">@lang('messages.platform')</label>
                                            <div wire:ignore>
                                                <select wire:model="search_platform" style="width: 100%"
                                                    class="form-control select2 form-select">
                                                    <option value="" selected> @lang('messages.alls')
                                                    </option>
                                                    @foreach ($platform as $p)
                                                        <option value="{{ $p->value }}">{{ $p->value }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">@lang('messages.the_categorie')</label>
                                            <div wire:ignore>
                                                <select wire:model="search_category" style="width: 100%"
                                                    class="form-control categories2 form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    <optgroup label="@lang('messages.category')">
                                                        <option value="Juegos">@lang('messages.games')</option>
                                                        <option value="Consolas">@lang('messages.consoles')</option>
                                                        <option value="Periféricos">@lang('messages.peripherals')</option>
                                                        <option value="Accesorios">@lang('messages.accesories')</option>
                                                        <option value="Merchandising">@lang('messages.merch')</option>
                                                    </optgroup>
                                                    <optgroup label="@lang('messages.sub_category')">
                                                        <option value="Mandos">@lang('messages.controls')</option>
                                                        <option value="Micrófonos">@lang('messages.microphones')</option>
                                                        <option value="Teclados">@lang('messages.keyboard')</option>
                                                        <option value="Fundas">@lang('messages.funded')</option>
                                                        <option value="Cables">@lang('messages.cable')</option>
                                                        <option value="Cargadores">@lang('messages.chargers')</option>
                                                        <option value="Merchandising -> Logos 3d">@lang('messages.3d_logo')
                                                        </option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">{{ __('Idioma Juego') }}</label>
                                            <div wire:ignore>
                                                <select wire:model="search_region" style="width: 100%"
                                                    class="form-control language2 form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    @foreach ($language as $l)
                                                        <option value="{{ $l->value_id }}">{{ $l->value_id }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">{{ __('Idioma Caja') }}</label>
                                            <div wire:ignore>
                                                <select wire:model="search_box_language" style="width: 100%"
                                                    class="form-control box_language2 form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    @foreach ($region as $r)
                                                        <option value="{{ $r->value_id }}">{{ $r->value_id }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">@lang('messages.the_region')</label>
                                            <div wire:ignore>
                                                <select wire:model="search_region" style="width: 100%"
                                                    class="form-control region2 form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    @foreach ($region as $r)
                                                        <option value="{{ $r->value_id }}">{{ $r->value_id }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">{{ __('Habilitado') }}</label>
                                            <div wire:ignore>
                                                <select wire:model="search_enabled" style="width: 100%"
                                                    class="form-control form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    <option value="Y">Si
                                                    </option>

                                                    <option value="N">No
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>





        </div>
    @endif


    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <script>
            document.addEventListener('livewire:load', function() {
                $('.select2').select2({
                    dropdownParent: $('#exampleModal')
                });
                $('.categories2').select2({
                    dropdownParent: $('#exampleModal')
                });
                $('.region2').select2({
                    dropdownParent: $('#exampleModal')
                });
                $('.language2').select2({
                    dropdownParent: $('#exampleModal')
                });
                $('.box_language2').select2({
                    dropdownParent: $('#exampleModal')
                });
                $('.select2').on('change', function() {
                    /*  alert(this.value) */
                    @this.set('search_platform', this.value);
                });
                $('.ultra2').on('change', function() {
                    /*  alert(this.value) */
                    @this.set('search_category', this.value);
                });
                $('.region2').on('change', function() {
                    /*  alert(this.value) */
                    @this.set('search_region', this.value);
                });
                $('.country2').on('change', function() {
                    /*  alert(this.value) */
                    @this.set('search_country', this.value);
                });
                $('.language2').on('change', function() {
                    /*  alert(this.value) */
                    @this.set('search_language', this.value);
                });
                $('.box_language2').on('change', function() {
                    /*  alert(this.value) */
                    @this.set('search_box_language', this.value);
                });
            })
        </script>
    @endif

    @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <script>
            document.addEventListener('livewire:load', function() {
                //Plataforma
                $('.select2').select2();
                $('.select2').on('change', function() {
                    @this.set('search_platform', this.value);
                });
                //Categorias
                $('.categories').select2();
                $('.categories').on('change', function() {
                    @this.set('search_category', this.value);
                });
                //Lenguaje
                $('.language').select2();
                $('.language').on('change', function() {
                    @this.set('search_language', this.value);
                });
                //Idioma de caja
                $('.box_language').select2();
                $('.box_language').on('change', function() {
                    @this.set('search_box_language', this.value);
                });
                //Region
                $('.region').select2();
                $('.region').on('change', function() {
                    @this.set('search_region', this.value);
                });
                //Habilitar
                $('.enabled').select2();
                $('.enabled').on('change', function() {
                    @this.set('search_enabled', this.value);
                });
            })
        </script>
    @endif

    <script>
        const table = new basictable('.table');

        new basictable('#table-breakpoint', {
            breakpoint: 768,
        });

        new basictable('#table-container-breakpoint', {
            containerBreakpoint: 485,
        });

        new basictable('#table-force-off', {
            forceResponsive: false,
        });

        new basictable('#table-max-height', {
            tableWrap: true
        });

        new basictable('#table-no-resize', {
            noResize: true,
        });

        new basictable('#table-two-axis');
    </script>


</div>
