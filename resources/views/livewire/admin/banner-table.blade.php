<div>
    <div class="flex items-center">
        <label for="simple-search" class="sr-only">Buscar</label>
        <div class="relative w-full">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor"
                    viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                        d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                        clip-rule="evenodd"></path>
                </svg>
            </div>
            <input type="text" wire:model="search"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Buscar por nombre">
        </div>

        <button type="button"
            class="p-2.5 ml-2 text-sm font-medium text-white bg-red-700 rounded-lg border border-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            data-toggle="modal" data-target="#categoryModal">
            <i class="fa fa-plus"></i>
            <span class="sr-only fa-solid fa-plus">Buscar</span>
        </button>

    </div>

    <div class="row">
        <div class="col-md-12">
            <table id="table-two-axis" class="two-axis">
                <thead>
                    <tr class="border-b border-gray-500 hover:bg-gray-200">
                        <th class="text-xs">Imagen</th>
                        <th class="text-xs">UBICACION</th>
                        <th class="text-xs">CATEGORIA </th>
                        <th class="text-xs">HABILITADO</th>
                        <th class="text-xs">ACCION</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($banner as $b)
                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                            <td class="text-sm font-semibold text-gray-900">
                                <div class="flex-shrink-0 w-40 h-20">
                                    <a href="{{ asset('uploads/banners-images/' . $b->image_path) }}"
                                        data-fancybox="{{ $b->id }}">
                                        <img class="h-10"
                                            src="{{ asset('uploads/banners-images/' . $b->image_path) }}"
                                            alt="">
                                    </a>
                                </div>
                            </td>
                            <td class="text-sm font-semibold text-gray-900"> {{ $b->location }} </td>
                            <td class="text-base text-gray-900">
                                {{ $b->category }}
                            </td>
                            <td class="text-base text-gray-900">
                                @if ($b->is_enabled == 'N')
                                    No
                                @elseif($b->is_enabled == 'Y')
                                    Si
                                @else
                                    Si
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('edit_banner', [$b->id]) }}" class="btn btn-success btn-xs"> <i
                                        class="far fa-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="flex flex-col items-center">
                @if ($banner->count())
                    <div class="flex items-center font-bold justify-between px-1 py-1 border-t border-gray-200 sm:px-6">
                        {{ $banner->links('pagination') }}
                    </div>
                @else
                    <div
                        class="flex items-center justify-between px-1 py-1 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                        No hay resultados para la busqueda "{{ $search }}" en la página
                        {{ $page }}
                        al mostrar {{ $perPage }}
                    </div>
                @endif
            </div>
        </div>

    </div>

</div>
