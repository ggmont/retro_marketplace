<div>

    <div class="row">
        <div class="col-md-12">
            <table id="table-two-axis" class="two-axis">
                <thead>
                    <tr class="border-b border-gray-500 hover:bg-gray-200">
                        <th class="text-xs">Usuario</th>
                        <th class="text-xs">Ultimo mensaje</th>
                        <th class="text-xs">Fecha</th>
                        <th class="text-xs">Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($beta as $s)
                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                            <td class="text-sm font-semibold text-gray-900">
                                @if ($s->prueba->user_name == auth()->user()->user_name)
                                    {{ $s->prueba->user_name }}
                                @else
                                    @if (App\SysUserRoles::where('user_id', $s->prueba->id)->whereIn('role_id', [7])->count() > 0)
                                        Moderador
                                    @else
                                        {{ $s->user->user_name }}
                                    @endif
                                @endif
                            </td>
                            <td class="text-base text-gray-900">
                                {!! mb_strimwidth($s->last_message, 0, 50, '...', 'UTF-8') !!}
                            </td>
                            <td class="text-base text-gray-900">
                                {{ date_format($s->created_at, 'Y-m-d') }}
                            </td>
                            <td>
                                @if ($s->prueba->user_name == $s->user->user_name)
                                    <a href="{{ route('message_user', ['userId' => $s->prueba->id, 'MsgId' => $s->user->id]) }}"
                                        class="btn btn-primary btn-sm" title="Editar"><i class="fa fa-eye"></i></a>
                                @else
                                    <a href="{{ route('message_user', ['userId' => $s->prueba->id, 'MsgId' => $s->user->id]) }}"
                                        class="btn btn-primary btn-sm" title="Editar"><i class="fa fa-eye"></i></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="flex flex-col items-center">
                @if ($beta->count())
                    <div class="flex items-center font-bold justify-between px-1 py-1 border-t border-gray-200 sm:px-6">
                        {{ $beta->links('pagination') }}
                    </div>
                @else
                    <div
                        class="flex items-center justify-between px-1 py-1 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                        No hay resultados para la busqueda 
                    </div>
                @endif
            </div>
        </div>

    </div>

    <script>
        const table = new basictable('.table');

        new basictable('#table-breakpoint', {
            breakpoint: 768,
        });

        new basictable('#table-container-breakpoint', {
            containerBreakpoint: 485,
        });

        new basictable('#table-force-off', {
            forceResponsive: false,
        });

        new basictable('#table-max-height', {
            tableWrap: true
        });

        new basictable('#table-no-resize', {
            noResize: true,
        });

        new basictable('#table-two-axis');
    </script>

</div>
