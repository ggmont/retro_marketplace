<div>
  <div class="mt-20 wishlist-table-area mb-50">
      <div class="container">
          <div class="row">
              <div class="col-lg-12">
                      <div class="col-lg-12">
                          <div
                              class="flex items-center justify-between px-4 py-3 bg-white border-t border-gray-200 sm:px-6">

                              <div class="nes-select">
                                  <select wire:model="perPage" class="text-gray-500 retro">
                                      <option value="5">5 Por pagina</option>
                                      <option value="10">10 Por pagina</option>
                                      <option value="15">15 Por pagina</option>
                                      <option value="25">25 Por pagina</option>
                                      <option value="50">50 Por pagina</option>
                                      <option value="100">100 Por pagina</option>
                                  </select>
                              </div>


                          </div>
                      </div>
                      <div class="wishlist-table table-responsive">
                          <table class="w-full table-auto min-w-max">
                              <thead>
                                  <tr class="text-sm leading-normal text-gray-100 uppercase bg-red-700">
                                      <th class="px-6 py-3 text-center">#</th>
                                      <th class="px-6 py-3 text-center">Titulo</th>
                                      <th class="px-6 py-3 text-center">Contenido</th>
                                      <th class="px-6 py-3 text-center">Habilitado</th>
                                      <th class="px-6 py-3 text-center">Accion</th>
                                  </tr>
                              </thead>
                              <tbody class="text-sm font-light text-gray-600">
                                  @foreach ($news as $n)
                                      <tr class="border-b border-gray-500 hover:bg-gray-200">
                                          <td class="px-6 py-3 text-center">
                                              <div class="text-sm font-medium text-gray-900">
                                                  {{$loop->index+1}}
                                              </div>
                                          </td>
                                          <td class="px-6 py-3 text-center">
                                              <div class="text-sm font-medium text-gray-900">
                                                {{ $n->title }}
                                              </div>
                                          </td>
                                          <td class="px-6 py-3 text-center">
                                              <span class="text-sm">
                                                  {!! $n->content !!}
                                              </span>
                                          </td>
                                          <td class="px-6 py-3 text-center">
                                              <span class="font-mono text-xs retro" style="color: #d44e4e">

                                                {{ $n->is_enabled }}

                                              </span>
                                          </td>
                                          <td class="px-6 py-3 text-center">
                                              <div class="text-sm text-gray-900">
                                                  <a href="{{ route('edit_new', [$n->id]) }}" target="_blank"
                                                      class="btn btn-primary"> <i class="far fa-edit"></i></a>
                                              </div>
                                          </td>
                                      </tr>
                                  @endforeach
                              </tbody>
                          </table>
                          <div class="flex flex-col items-center">
                              @if ($news->count())
                                  <div
                                      class="flex items-center justify-between px-4 py-3 border-t border-gray-200  sm:px-6">
                                      {{ $news->links('pagination') }}
                                  </div>
                              @else
                                  <div class="row">
                                      <div class="col-lg-12">
                                          <blockquote class="nk-blockquote">
                                              <div class="nk-blockquote-icon"></div>
                                              <div class="text-center nk-blockquote-content h4 retro">
                                                  <font color="black">
                                                      {{ __('No hay datos disponibles para esta seccion') }}
                                                  </font>
                                              </div>
                                              <div class="nk-gap"></div>
                                              <div class="nk-blockquote-author"></div>
                                          </blockquote>
                                      </div>
                                  </div>
                              @endif
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
