<div>
    <div class="flex items-center">
        <label for="simple-search" class="sr-only">Buscar</label>
        <div class="relative w-full">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor"
                    viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                        d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                        clip-rule="evenodd"></path>
                </svg>
            </div>
            <input type="text" wire:model="search"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Buscar por codigo">
        </div>

        <button type="button"
            class="p-2.5 ml-2 text-sm font-medium text-white bg-red-700 rounded-lg border border-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            data-toggle="modal" data-target="#codeModal">
            <i class="fa fa-plus"></i>
            <span class="sr-only fa-solid fa-plus">Buscar</span>
        </button>

    </div>

    <div class="row">
        <div class="col-md-12">
            <table id="table-two-axis" class="two-axis">
                <thead>
                    <tr class="border-b border-gray-500 hover:bg-gray-200">
                        <th class="text-xs">CODIGO PROMOCIONAL</th>
                        <th class="text-xs">TIPO</th>
                        <th class="text-xs">OFERTA</th>
                        <th class="text-xs">DESCRIPCION</th>
                        <th class="text-xs">HABILITADO</th>
                        <th class="text-xs">ACCION</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($promotion as $s)
                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                            <td class="text-sm font-semibold text-gray-900"> {{ $s->code }}</td>
                            <td class="text-sm font-semibold text-gray-900"> {{ $s->type }} </td>
                            <td class="text-base text-gray-900">
                                {{ $s->special }}%
                            </td>
                            <td class="text-base text-gray-900">
                                {{ $s->description }}
                            </td>
                            <td class="text-base text-gray-900">
                                @if ($s->is_enabled == 'Y')
                                    Si
                                @else
                                    No
                                @endif
                            </td>
                            <td>
                                <a href="#" class="btn btn-success btn-xs"
                                    wire:click.prevent="showEditModal({{ $s->id }})" data-toggle="modal"
                                    data-target="#editModal">
                                    <font color="white">
                                        <i class="fa fa-edit"></i>
                                    </font>
                                </a>
                            </td>
                        </tr>

                        <div wire:ignore.self class="modal fade" id="editModal" tabindex="-1" role="dialog"
                            aria-labelledby="editModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content bg-white">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="d-flex align-items-center justify-content-between mb-4">
                                                <h4 class="modal-title" id="addnewcontactlabel">Editar:</h4>
                                                <button class="btn btn-close p-1 ms-auto me-0" class="close"
                                                    data-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="wide-block pb-1 pt-2">
                                                <form>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label for=""
                                                                class="mb-2 form-label text-center">CODIGO
                                                                PROMOCIONAL</label>
                                                            <input wire:model="code" id="edit_code"
                                                                class="form-control">
                                                            @error('code')
                                                                <span
                                                                    class="error retro text-xs text-red-700">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                        <br>
                                                        <div class="col-sm-6">
                                                            <div class="mb-3">
                                                                <label for=""
                                                                    class="mb-2 form-label">Tipo</label>
                                                                <select wire:model="type" id="edit_type"
                                                                    class="form-control show-tick ms select2">
                                                                    <option value="">Seleccione</option>
                                                                    <option value="descuento">Descuento</option>
                                                                </select>
                                                                @error('type')
                                                                    <span
                                                                        class="error retro text-xs text-red-700">{{ $message }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="mb-3">
                                                                <label for=""
                                                                    class="mb-2 form-label">Special</label>
                                                                <input wire:model="special" id="special"
                                                                    type="number"
                                                                    placeholder="Ingrese la cantidad que quieres que sea el total de especial para este tipo de codigo"
                                                                    class="form-control">
                                                                @error('special')
                                                                    <span
                                                                        class="error retro text-xs text-red-700">{{ $message }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="mb-3">
                                                                <label for=""
                                                                    class="mb-2 form-label">Descripcion</label>
                                                                <textarea wire:model="description" id="description" cols="10" rows="5" class="form-control"
                                                                    placeholder="Descripcion"></textarea>
                                                                @error('description')
                                                                    <span
                                                                        class="error retro text-xs text-red-700">{{ $message }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="mb-3">
                                                                <label for=""
                                                                    class="mb-2 form-label">Habilitado</label>
                                                                <select wire:model="is_enabled" id="is_enabled"
                                                                    class="form-control show-tick ms select2">
                                                                    <option value="">Seleccione</option>
                                                                    <option value="Y">Si</option>
                                                                    <option value="N">No</option>
                                                                </select>
                                                                @error('is_enabled')
                                                                    <span
                                                                        class="error retro text-xs text-red-700">{{ $message }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>

                                                </form>

                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" wire:click="update({{ $s->id }})"
                                                data-dismiss="modal" class="btn confirmclosed btn-danger font-bold w-100 close-modal">Actualizar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </tbody>
            </table>
            <div class="flex flex-col items-center">
                @if ($promotion->count())
                    <div
                        class="flex items-center font-bold justify-between px-1 py-1 border-t border-gray-200 sm:px-6">
                        {{ $promotion->links('pagination') }}
                    </div>
                @else
                    <div
                        class="flex items-center justify-between px-1 py-1 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                        No hay resultados para la busqueda "{{ $search }}" en la página
                        {{ $page }}
                        al mostrar {{ $perPage }}
                    </div>
                @endif
            </div>
        </div>

    </div>

    <div wire:ignore.self class="modal fade" id="codeModal" tabindex="-1" role="dialog"
        aria-labelledby="codeModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">Crear nuevo :</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">

                            <form>

                                <div class="row">
                                    <div class="col-sm-12">

                                        <label for="" class="mb-2 form-label text-center">CODIGO
                                            PROMOCIONAL</label>
                                        <input wire:model="code" id="code" class="form-control">
                                        @error('code')
                                            <span class="error retro text-xs text-red-700">{{ $message }}</span>
                                        @enderror


                                    </div>
                                    <br>
                                    <div class="col-sm-6">
                                        <div class="mb-3">
                                            <label for="" class="mb-2 form-label">Tipo</label>
                                            <select wire:model="type" id="type"
                                                class="form-control show-tick ms select2">
                                                <option value="">Seleccione</option>
                                                <option value="descuento">Descuento</option>
                                            </select>
                                            @error('type')
                                                <span class="error retro text-xs text-red-700">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="mb-3">
                                            <label for="" class="mb-2 form-label">Special</label>
                                            <input wire:model="special" id="special" type="number"
                                                placeholder="Ingrese la cantidad que quieres que sea el total de especial para este tipo de codigo"
                                                class="form-control">
                                            @error('special')
                                                <span class="error retro text-xs text-red-700">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="mb-3">
                                            <label for="" class="mb-2 form-label">Descripcion</label>
                                            <textarea wire:model="description" id="description" cols="10" rows="5" class="form-control"
                                                placeholder="Descripcion"></textarea>
                                            @error('description')
                                                <span class="error retro text-xs text-red-700">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="mb-3">
                                            <label for="" class="mb-2 form-label">Habilitado</label>
                                            <select wire:model="is_enabled" id="is_enabled"
                                                class="form-control show-tick ms select2">
                                                <option value="">Seleccione</option>
                                                <option value="Y">Si</option>
                                                <option value="N">No</option>
                                            </select>
                                            @error('is_enabled')
                                                <span class="error retro text-xs text-red-700">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="store"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">Crear</button>
                    </div>
                </div>
            </div>
        </div>




    </div>

    <script>
        const table = new basictable('.table');

        new basictable('#table-breakpoint', {
            breakpoint: 768,
        });

        new basictable('#table-container-breakpoint', {
            containerBreakpoint: 485,
        });

        new basictable('#table-force-off', {
            forceResponsive: false,
        });

        new basictable('#table-max-height', {
            tableWrap: true
        });

        new basictable('#table-no-resize', {
            noResize: true,
        });

        new basictable('#table-two-axis');
    </script>

    @push('scripts')
        <script>
            Livewire.on('promotionUpdated', () => {
                $('#editModal').modal('hide');
                toastr.success('La promoción se ha actualizado correctamente.');
            });
        </script>
    @endpush


</div>
