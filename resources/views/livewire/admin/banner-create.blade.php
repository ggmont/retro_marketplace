<div>
    <div class="p-4 mx-auto mb-6 overflow-hidden bg-white rounded-lg shadow max-w-7xl">
        <div class="row">
            <div class="col-sm-12">
                <div class="mb-3">
                    <label for="" class="mb-2 form-label">Enlazado</label>
                    <select wire:model="category_text" id="category_text" data-live-search="true"
                        data-live-search-style="startsWith" class="selectpicker form-control">
                        <option value="">Selecciona</option>
                        @foreach ($categories as $c)
                            <option value="{{ $c->category_text }}">{{ $c->category_text }}</option>
                        @endforeach
                    </select>
                </div>

            </div>
            <br>
            <div class="col-sm-6">
                <div class="mb-3">
                    <label for="" class="mb-2 form-label">Nombre</label>
                    <input wire:model="name" id="name" placeholder="Ingrese un nombre" class="form-control">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="mb-3">
                    <label for="" class="mb-2 form-label">Posicion</label>
                    <input wire:model="order_by" id="order_by" type="number" placeholder="Ingrese una Posicion"
                        class="form-control">
                </div>
            </div>

            <div class="col-sm-12">
                <div class="mb-3">
                    <label for="" class="mb-2 form-label">Habilitado</label>
                    <select wire:model="is_enabled" id="is_enabled" class="form-control show-tick ms select2">
                        <option value="Y">Si</option>
                        <option value="N">No</option>
                    </select>
                </div>
            </div>
        </div>
        @if ($accion == 'store')
            <button wire:click="store"
                class="px-4 py-2 mb-2 font-bold text-white bg-blue-500 rounded hover:bg-blue-700">Crear</button>
        @else
            <button wire:click="update"
                class="px-4 py-2 mb-2 font-bold text-white bg-yellow-500 rounded hover:bg-blue-700">Actualizar</button>
            <button wire:click="default"
                class="px-4 py-2 mb-2 font-bold text-white bg-yellow-500 rounded hover:bg-blue-700">Cancelar</button>
        @endif
    </div>
</div>
