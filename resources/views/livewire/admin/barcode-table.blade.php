<div>

    <div class="flex items-center">
        <label for="simple-search" class="sr-only">Buscar</label>
        <div class="relative w-full">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor"
                    viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                        d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                        clip-rule="evenodd"></path>
                </svg>
            </div>
            <input type="text" wire:model="search"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Buscar por nombre">
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <table id="table-two-axis" class="two-axis">
                <thead>
                    <tr class="border-b border-gray-500 hover:bg-gray-200">

                        <th class="text-xs">PRODUCTO</th>
                        <th class="text-xs"> <i class="fa fa-barcode"></i></th>
                        <th class="text-xs">USUARIO</th>
                        <th class="text-xs">PLATAFORMA</th>
                        <th class="text-xs">REGION</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($inventories_ean as $p)
                        @foreach ($p->imagesean as $p)
                            <tr class="border-b border-gray-500 hover:bg-gray-200">
                                <td class="text-sm font-semibold text-gray-900"> {{ $p->inventory->product->name }}
                                </td>
                                <td class="text-sm font-semibold text-gray-900">
                                    <div class="flex items-center">
                                        <div class="flex-shrink-0 w-10 h-10">

                                            <a href="{{ url('/uploads/ean-images/' . $p->image_path) }}"
                                                class="fancybox product-thumbnail d-block"
                                                data-fancybox="RGM{{ $p->id }}">
                                                <img src="{{ url('/uploads/ean-images/' . $p->image_path) }}"
                                                    width="50px" height="50px" alt="">
                                            </a>

                                        </div> 
                                    </div>
                                </td>
                                <td class="text-base text-gray-900">
                                    @if ($p->inventory->user)
                                        {{ $p->inventory->user->user_name }}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td class="text-base text-gray-900">
                                    {{ $p->inventory->product->platform }}
                                </td>
                                <td class="text-base text-gray-900">
                                    {{ $p->inventory->product->region }}
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                </tbody>
            </table>
            <div class="flex flex-col items-center">
                @if ($inventories_ean->count())
                    <div class="flex items-center font-bold justify-between px-1 py-1 border-t border-gray-200 sm:px-6">
                        {{ $inventories_ean->links('pagination') }}
                    </div>
                @else
                    <div
                        class="flex items-center justify-between px-1 py-1 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                        No hay resultados para la busqueda "{{ $search }}" en la página
                        {{ $page }}
                        al mostrar {{ $perPage }}
                    </div>
                @endif
            </div>
        </div>

    </div>
    <script>
        const table = new basictable('.table');

        new basictable('#table-breakpoint', {
            breakpoint: 768,
        });

        new basictable('#table-container-breakpoint', {
            containerBreakpoint: 485,
        });

        new basictable('#table-force-off', {
            forceResponsive: false,
        });

        new basictable('#table-max-height', {
            tableWrap: true
        });

        new basictable('#table-no-resize', {
            noResize: true,
        });

        new basictable('#table-two-axis');
    </script>
</div>
