<div>
    <div class="p-4 mx-auto mb-6 overflow-hidden bg-white rounded-lg shadow max-w-7xl">
        <div class="row">
            <div class="col-sm-12">

                <label for="" class="mb-2 form-label text-center">CODIGO PROMOCIONAL</label>
                <input wire:model="code" id="code" class="form-control">
                @error('code') <span class="error retro text-xs text-red-700">{{ $message }}</span> @enderror


            </div>
            <br>
            <div class="col-sm-6">
                <div class="mb-3">
                    <label for="" class="mb-2 form-label">Tipo</label>
                    <select wire:model="type" id="type" class="form-control show-tick ms select2">
                        <option value="">Seleccione</option>
                        <option value="descuento">Descuento</option>
                    </select>
                    @error('type') <span class="error retro text-xs text-red-700">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="col-sm-6">
                <div class="mb-3">
                    <label for="" class="mb-2 form-label">Special</label>
                    <input wire:model="special" id="special" type="number"
                        placeholder="Ingrese la cantidad que quieres que sea el total de especial para este tipo de codigo"
                        class="form-control">
                    @error('special') <span class="error retro text-xs text-red-700">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="col-sm-12">
                <div class="mb-3">
                    <label for="" class="mb-2 form-label">Descripcion</label>
                    <textarea wire:model="description" id="description" cols="10" rows="5" class="form-control"
                        placeholder="Descripcion"></textarea>
                    @error('description') <span class="error retro text-xs text-red-700">{{ $message }}</span> @enderror
                </div>
            </div>

            <div class="col-sm-12">
                <div class="mb-3">
                    <label for="" class="mb-2 form-label">Habilitado</label>
                    <select wire:model="is_enabled" id="is_enabled" class="form-control show-tick ms select2">
                        <option value="">Seleccione</option>
                        <option value="Y">Si</option>
                        <option value="N">No</option>
                    </select>
                    @error('is_enabled') <span class="error retro text-xs text-red-700">{{ $message }}</span>
                    @enderror
                </div>
            </div>
        </div>
        <center>
            @if ($accion == 'store')
                <div class="col-md-12">
                    <button wire:click="store"
                        class="px-64 retro py-2 mb-2 font-bold text-white bg-blue-500 rounded hover:bg-blue-700">Crear</button>
                </div>
            @else
                <button wire:click="update"
                    class="px-4 py-2 mb-2 font-bold text-white bg-yellow-500 rounded hover:bg-blue-700">Actualizar</button>
                <button wire:click="default"
                    class="px-4 py-2 mb-2 font-bold text-white bg-yellow-500 rounded hover:bg-blue-700">Cancelar</button>
            @endif
        </center>
    </div>
</div>
