<div class="row">
    <div class="col-md-12">
        <table id="table-two-axisEvent" class="two-axis">
            <thead>
                <tr class="border-b border-gray-500 hover:bg-gray-200">
                    <th class="text-xs">Nombre Completo</th>
                    <th class="text-xs">Acciones</th>
                    <th class="text-xs">IPs</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($userEvents as $u)
                    <tr class="border-b border-gray-500 hover:bg-gray-200">
                        <td class="text-sm font-semibold text-gray-900">
                            {{ $u->user->first_name . ' ' . $u->user->last_name }}
                        </td>
                        <td class="text-base text-gray-900">
                            {{ str_replace(':date', $u->message_values['date'], $u->message_code) }}
                        </td>
                        <td class="text-base text-gray-900">
                            @if (isset($u->message_values['ip_user']))
                                {{ $u->message_values['ip_user'] ? $u->message_values['ip_user'] : 'Sin Ip' }}
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="flex flex-col items-center">
            @if ($userEvents->count())
                <div class="flex items-center font-bold justify-between px-1 py-1 border-t border-gray-200 sm:px-6">
                    {{ $userEvents->links('pagination') }}
                </div>
            @else
                <div
                    class="flex items-center justify-between px-1 py-1 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                    No hay resultados para la busqueda  
                </div>
            @endif
        </div>
    </div>

    <script>
        new basictable('#table-two-axisEvent');
    </script>

</div>
