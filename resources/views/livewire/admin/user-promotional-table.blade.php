<div class="row">
    <div class="col-md-12">
        <table id="table-two-axisPromo" class="two-axis">
            <thead>
                <tr class="border-b border-gray-500 hover:bg-gray-200">
                    <th class="text-xs">Nombre completo</th>
                    <th class="text-xs">Usuario</th>
                    <th class="text-xs">Codigo promocional</th>
                    <th class="text-xs">Descuento</th>
                    <th class="text-xs">Total del pedido (Sin envío)</th>
                    <th class="text-xs">Se ha descontado</th>
                    <th class="text-xs">Ver detalles</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($promotionalHistory as $p)
                    <tr class="border-b border-gray-500 hover:bg-gray-200">
                        <td class="text-sm font-semibold text-gray-900">
                            {{ $p->first_name . ' ' . $p->last_name }}
                        </td>
                        <td class="text-base text-gray-900">
                            {{ $p->user_name }}
                        </td>
                        <td class="text-base text-gray-900">
                            {{ $p->promotion->code }}
                        </td>
                        <td class="text-base text-gray-900">
                            {{ $p->promotion->special }}%
                        </td>
                        <td class="text-base text-gray-900">
                            {{ number_format($p->total, 2) . ' €' }}
                        </td>
                        <td class="text-base text-gray-900">
                            <?php
                            $special_promotion = ($p->total * $p->promotion->special) / 100;
                            ?>

                            {{ number_format($special_promotion, 2) . ' €' }}
                        </td>
                        <td class="text-base text-gray-900">
                            <a href="{{ route('historial_view', $r->id) }}" class="btn btn-primary"> <i
                                    class="far fa-eye"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="flex flex-col items-center">
            @if ($promotionalHistory->count())
                <div class="flex items-center font-bold justify-between px-1 py-1 border-t border-gray-200 sm:px-6">
                    {{ $promotionalHistory->links('pagination') }}
                </div>
            @else
                <div
                    class="flex items-center justify-between px-1 py-1 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                    No hay resultados para la busqueda en la página
                </div>
            @endif
        </div>
    </div>

    <script>
        new basictable('#table-two-axisPromo');
    </script>

</div>
