<div>

	<div class="flex items-center justify-between px-4 py-3 bg-white border-t border-gray-200 sm:px-6">
	   	<input wire:model="search" class="block w-full mt-1 rounded-md shadow-sm form-input" type="text" placeholder="Buscar...">
	   	<div class="block mt-1 ml-6 rounded-md shadow-sm form-input">
	   		<select wire:model="perPage" class="text-sm text-gray-500 outline-none">
	   			<option value="5">5 Por página</option>
	   			<option value="10">10 Por página</option>
	   			<option value="15">15 Por página</option>
	   			<option value="25">25 Por página</option>
	   			<option value="50">50 Por página</option>
	   			<option value="10">100 Por página</option>
	   		</select>
	   	</div>
	    <div class="block mt-1 ml-6 rounded-md shadow-sm form-input">
	    	@if($search !== '')
	    	<button wire:click="clear" class="block mt-1 ml-6 rounded-md shadow-sm form-input">X</button>
	    	@endif
	   	</div>
	</div>
	<div class="flex flex-col">
  <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
    <div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
      <div class="overflow-hidden border-b border-gray-200 shadow sm:rounded-lg">
        <table class="min-w-full divide-y divide-gray-200">
          <thead class="bg-gray-50">
            <tr>
              <th scope="col" class="px-6 py-3 text-xs font-medium font-bold tracking-wider text-left text-gray-700 uppercase">
                #
              </th>
              <th scope="col" class="px-6 py-3 text-xs font-medium font-bold tracking-wider text-left text-gray-500 uppercase">
                Titulo
              </th>
              <th scope="col" class="px-6 py-3 text-xs font-medium font-bold tracking-wider text-left text-gray-500 uppercase">
                Habilitado
              </th>
              <th scope="col" class="px-6 py-3 text-xs font-medium font-bold tracking-wider text-left text-gray-500 uppercase">
                Action
              </th>

            </tr>
          </thead>
          <tbody class="bg-white divide-y divide-gray-200">
          	 @foreach($blog as $b)
            <tr>
              <td class="px-6 py-4 whitespace-nowrap">
                <div class="flex items-center">
                  <div class="flex-shrink-0 h-20 w-30">
                    <img class="h-20 w-30" src="{{ asset('uploads/blogs-images/'.$b->image_path) }}" alt="">
                  </div>
                  <div class="ml-4">
                    <div class="text-sm font-medium text-gray-900">
                      {{ $b->id }}
                    </div>
                  </div>
                </div>
              </td>
              <td class="px-6 py-4 whitespace-nowrap">
                <div class="text-sm text-gray-900">{{ $b->title }}</div>
              </td>
              <td class="px-6 py-4 whitespace-nowrap">
                <div class="text-sm text-gray-900">{{ $b->is_enabled }}</div>
              </td>
              <td class="px-6 py-4 whitespace-nowrap">
                <div class="text-sm text-gray-900">{{ $b->is_enabled }}</div>
              </td>
            </tr>
                @endforeach

            <!-- More items... -->
          </tbody>

        </table>
        @if($blog->count())
          <div class="flex items-center justify-between px-4 py-3 bg-white border-t border-gray-200 sm:px-6">
           {{ $blog->links() }}
          </div>
        @else
        <div class="flex items-center justify-between px-4 py-3 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
        	No hay resultados para la busqueda "{{ $search }}" en la página {{ $page }} al mostrar {{ $perPage }}
        @endif
      </div>
    </div>
  </div>
</div>
</div>
