<div>

    <div class="row">
        <div class="col-md-12" style="background-color: #ffffff; border: solid 1px #db2e2e">
            <br>
            <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase" for="grid-city">
                Detalles Basicos
            </h5>
            <br>
            <form class="w-full" method="POST" action="{{ route('shipping.store') }}" enctype="multipart/form-data">
                @csrf
                @if ($errors->any())
                <div class="alert alert-danger">
                    Corrija los errores en el formulario
                </div>
               @endif
                <div class="flex flex-wrap mb-6 -mx-3">
                    <div class="w-full px-3 mb-6 md:w-1/2 md:mb-0">
                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                            for="grid-first-name">
                            Nombre de envio
                        </label>
                        <input
                            class="block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border border-red-500 rounded appearance-none focus:outline-none focus:bg-white"
                            name="name" type="text" placeholder="Name shipping">
							@if($errors->has('name'))
							<span class="label label-danger">
								@foreach ($errors->get('name') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
                    </div>
                    <div class="w-full px-3 md:w-1/2">
                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                            for="grid-last-name">
                            Certificado
                        </label>
                        <div class="relative">
                            <select
                                class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                name="certified" required="">

                                <option value="">Selecciona</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>

                            </select>
                        </div>

                        @if($errors->has('certified'))
                        <span class="label label-danger">
                            @foreach ($errors->get('certified') as $error)
                                {{$error}}
                            @endforeach
                        </span>
                        @endif

                    </div>
                </div>

                <h5 class="block mb-2 font-bold tracking-wide text-gray-700 uppercase" for="grid-city">
                    Detalles De Envio
                </h5>
                <br>
                <div class="flex flex-wrap mb-2 -mx-3">
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                            for="grid-state">
                            Tipo de Envio
                        </label>
                        <div class="relative">
                            <select class="form-control selectpicker" name="type" data-live-search="true"
                                data-live-search-style="startsWith" required="">
                                <option value="">Selecciona</option>
                                <option value="PA">Paquete</option>
                                <option value="CA">Carta</option>
                            </select>
                        </div>
                        @if($errors->has('type'))
                        <span class="label label-danger">
                            @foreach ($errors->get('type') as $error)
                                {{$error}}
                            @endforeach
                        </span>
                        @endif
                    </div>
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                            for="grid-state">
                            Origen
                        </label>
                        <div class="relative">
                            <select class="form-control selectpicker" name="from_id" data-live-search="true"
                                data-live-search-style="startsWith" required="">
                                <option value="">Selecciona</option>
                                @foreach ($country as $c)
                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('from_id'))
							<span class="label label-danger">
								@foreach ($errors->get('from_id') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
                        </div>
                    </div>
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                            for="grid-state">
                            Destinatario
                        </label>
                        <div class="relative">
                            <select class="form-control selectpicker" name="to_id" data-live-search="true"
                                data-live-search-style="startsWith" required="">
                                <option value="">Selecciona</option>
                                @foreach ($country as $c)
                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('to_id'))
							<span class="label label-danger">
								@foreach ($errors->get('to_id') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
                        </div>
                    </div>
                </div>
                <br>
                <div class="flex flex-wrap mb-2 -mx-3">
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                            for="grid-city">
                            Largo
                        </label>
                        <input
                            class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                            name="large" type="number" placeholder="Large">
                            @if($errors->has('large'))
							<span class="label label-danger">
								@foreach ($errors->get('large') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
                    </div>

                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                            for="grid-city">
                            Ancho
                        </label>
                        <input
                            class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                            name="width" type="number" placeholder="Width">
                            @if($errors->has('width'))
							<span class="label label-danger">
								@foreach ($errors->get('width') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
                    </div>

                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                            for="grid-zip">
                            Alto
                        </label>
                        <input
                            class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                            name="hight" type="number" placeholder="Hight">
                            @if($errors->has('hight'))
							<span class="label label-danger">
								@foreach ($errors->get('hight') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
                    </div>
                </div>
                <br>
                <div class="flex flex-wrap mb-2 -mx-3">
                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                            for="grid-city">
                            Max Weight
                        </label>
                        <input
                            class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                            name="max_weight" type="number" placeholder="Max Weight">
                            @if($errors->has('max_weight'))
							<span class="label label-danger">
								@foreach ($errors->get('max_weight') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
                    </div>

                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                            for="grid-city">
                            Max value
                        </label>
                        <input
                            class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                            name="max_value" type="number" placeholder="Max Value">
                            @if($errors->has('max_value'))
							<span class="label label-danger">
								@foreach ($errors->get('max_value') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
                    </div>

                    <div class="w-full px-3 mb-6 md:w-1/3 md:mb-0">
                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                            for="grid-zip">
                            Stamp Price
                        </label>
                        <input
                            class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                            name="stamp_price" type="number" placeholder="Stamp Price">
                            @if($errors->has('stamp_price'))
							<span class="label label-danger">
								@foreach ($errors->get('stamp_price') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
                    </div>
                </div>
                <br>
                <div class="flex flex-wrap mb-6 -mx-3">
                    <div class="w-full px-3">
                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase"
                            for="grid-password">
                            Precio
                        </label>
                        <input
                            class="block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                            name="price" type="number" placeholder="Price">
                            @if($errors->has('price'))
							<span class="label label-danger">
								@foreach ($errors->get('price') as $error)
									{{$error}}
								@endforeach
							</span>
							@endif
                    </div>
                </div>

                <div class="text-center form-group col-md-12">

                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i>
                        Atrás</a>
                    <input type="submit" class="btn btn-danger" value="Registrar">
                </div>

            </form>
        </div>
    </div>
</div>
