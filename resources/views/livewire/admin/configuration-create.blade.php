<div>
    <div class="p-4 mx-auto mb-6 overflow-hidden bg-white rounded-lg shadow max-w-7xl">
        <div class="row">

            <div class="col-sm-4">
                <div class="mb-3">
                    <label for="" class="mb-2 form-label">Codigo</label>
                    <select wire:model="code" id="code" placeholder="Code" class="form-control" required="">

                        <option value="">Selecciona</option>
                        <option value="GAME_PLATFORM">GAME_PLATFORM</option>
                        <option value="GAME_CATEGORY">GAME_CATEGORY</option>
                        <option value="GAME_LANGUAGE">GAME_LANGUAGE</option>
                        <option value="GAME_LOCATION">GAME_LOCATION</option>

                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="mb-3">
                    <label for="" class="mb-2 form-label"><b>Value ID</b></label>
                    <input wire:model="value_id" id="value_id" type="text" placeholder="Value_id" class="form-control"
                        required="">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="mb-3">
                    <label for="" class="mb-2 form-label"><b>Value</b></label>
                    <input wire:model="value" id="value" type="text" placeholder="Value" class="form-control"
                        required="">
                </div>
            </div>

            <br>

            <div class="col-sm-6">
                <div class="mb-3">
                    <div wire:ignore>
                        <label for="" class="mb-2 form-label">Parent (Opcional)</label>
                        <select wire:model="parent_id" id="parent_id" class="form-control">
                            <option value="">Selecciona</option>
                            @foreach ($list as $l)
                                <option value="{{ $l->id }}">{{ $l->code }} - {{ $l->value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="mb-3">
                    <label for="" class="mb-2 form-label"><b>Posicion</b></label>
                    <input wire:model="order_by" id="order_by" type="number" placeholder="Order By"
                        class="form-control" required="">
                </div>
            </div>

        </div>
        @if ($accion == 'store')
            <button wire:click="store"
                class="px-4 py-2 mb-2 font-bold text-white bg-blue-500 rounded hover:bg-blue-700">Crear</button>
        @else
            <button wire:click="update"
                class="px-4 py-2 mb-2 font-bold text-white bg-yellow-500 rounded hover:bg-blue-700">Actualizar</button>
            <button wire:click="default"
                class="px-4 py-2 mb-2 font-bold text-white bg-yellow-500 rounded hover:bg-blue-700">Cancelar</button>
        @endif
    </div>
</div>
</div>
