<div class="row">
    <div class="col-md-12">
        <table id="table-two-axisRefered" class="two-axis">
            <thead>
                <tr class="border-b border-gray-500 hover:bg-gray-200">
                    <th class="text-xs">Nombre Completo</th>
                    <th class="text-xs">Usuario</th>
                    <th class="text-xs">Cuenta Creada</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($refered as $r)
                    <tr class="border-b border-gray-500 hover:bg-gray-200">
                        <td class="text-sm font-semibold text-gray-900">
                            {{ $r->first_name . ' ' . $r->last_name }}
                        </td>
                        <td class="text-base text-gray-900">
                            {{ $r->user_name }}
                        </td>
                        <td class="text-base text-gray-900">
                            {{ date_format($r->created_at, 'Y-m-d') }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="flex flex-col items-center">
            @if ($refered->count())
                <div class="flex items-center font-bold justify-between px-1 py-1 border-t border-gray-200 sm:px-6">
                    {{ $refered->links('pagination') }}
                </div>
            @else
                <div
                    class="flex items-center justify-between px-1 py-1 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                    No hay resultados para la busqueda 
                </div>
            @endif
        </div>
    </div>

    <script>
        new basictable('#table-two-axisRefered');
    </script>

</div>
