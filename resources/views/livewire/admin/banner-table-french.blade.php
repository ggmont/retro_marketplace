<div>
    <div class="wishlist-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bg-white rounded shadow-md">
                        <div class="wishlist-table table-responsive">
                            <table class="w-full table-auto min-w-max">
                                <thead>
                                    <tr class="text-sm leading-normal text-gray-100 uppercase bg-red-700">
                                        <th class="px-6 py-3 text-center">#</th>
                                        <th class="px-6 py-3 text-center">Ubicacion</th>
                                        <th class="px-6 py-3 text-center">Categoria / Links</th>
                                        <th class="px-6 py-3 text-center">Habilitado</th>
                                        <th class="px-6 py-3 text-center">Accion</th>
                                    </tr>
                                </thead>
                                <tbody class="text-sm font-light text-gray-600">
                                    @foreach ($banner as $b)
                                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <center>
                                                    <div class="flex-shrink-0 w-40 h-20">
                                                        <a href="{{ asset('uploads/banners-images/' . $b->image_path) }}"
                                                            data-fancybox="{{ $b->id }}">
                                                            <img class="h-10"
                                                                src="{{ asset('uploads/banners-images/' . $b->image_path) }}"
                                                                alt="">
                                                        </a>
                                                    </div>
                                                </center>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{ $b->location }}</div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-xs text-gray-900 retro">{{ $b->category }}</div>
                                            </td>
                                            <td class="px-6 py-3 text-center">
                                                <span class="font-mono text-xs retro" style="color: #d44e4e">
                                                    @if ($b->is_enabled == 'N')
                                                        N
                                                    @elseif($b->is_enabled == 'Y')
                                                        Y
                                                    @else
                                                        Y
                                                    @endif
                                                </span>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">
                                                    <a href="{{ route('edit_banner', [$b->id]) }}" target="_blank"
                                                        class="btn btn-primary"> <i class="far fa-edit"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="flex flex-col items-center">
                                @if ($banner->count())
                                    <div
                                        class="flex items-center justify-between px-4 py-3 border-t border-gray-200 sm:px-6">
                                        {{ $banner->links('pagination') }}
                                    </div>
                                @else
                                    <div
                                        class="flex items-center justify-between px-4 py-3 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                                        No hay resultados para la busqueda "{{ $search }}" en la página
                                        {{ $page }}
                                        al mostrar {{ $perPage }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
