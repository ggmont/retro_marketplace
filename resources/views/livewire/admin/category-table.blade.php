<div>

    <div class="flex items-center">
        <label for="simple-search" class="sr-only">Buscar</label>
        <div class="relative w-full">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor"
                    viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                        d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                        clip-rule="evenodd"></path>
                </svg>
            </div>
            <input type="text" wire:model="search"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Buscar por nombre">
        </div>

        <button type="button"
            class="p-2.5 ml-2 text-sm font-medium text-white bg-red-700 rounded-lg border border-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            data-toggle="modal" data-target="#categoryModal">
            <i class="fa fa-plus"></i>
            <span class="sr-only fa-solid fa-plus">Buscar</span>
        </button>

    </div>

    <div class="row">
        <div class="col-md-12">
            <table id="table-two-axis" class="two-axis">
                <thead>
                    <tr class="border-b border-gray-500 hover:bg-gray-200">
                        <th class="text-xs">NOMBRE</th>
                        <th class="text-xs">CATEGORIA</th>
                        <th class="text-xs">POSICION</th>
                        <th class="text-xs">HABILITADO</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categories as $c)
                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                            <td class="text-sm font-semibold text-gray-900"> {{ $c->name }}</td>
                            <td class="text-sm font-semibold text-gray-900"> {{ $c->category_text }} </td>
                            <td class="text-base text-gray-900">
                                {{ $c->order_by }}
                            </td>
                            <td class="text-base text-gray-900">
                                @if ($c->is_enabled == 'Y')
                                    Si
                                @else
                                    No
                                @endif
                            </td>
                        </tr>

                    @endforeach
                </tbody>
            </table>
            <div class="flex flex-col items-center">
                @if ($categories->count())
                    <div class="flex items-center font-bold justify-between px-1 py-1 border-t border-gray-200 sm:px-6">
                        {{ $categories->links('pagination') }}
                    </div>
                @else
                    <div
                        class="flex items-center justify-between px-1 py-1 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                        No hay resultados para la busqueda "{{ $search }}" en la página
                        {{ $page }}
                        al mostrar {{ $perPage }}
                    </div>
                @endif
            </div>
        </div>

    </div>

    <div wire:ignore.self class="modal fade" id="categoryModal" tabindex="-1" role="dialog"
        aria-labelledby="categoryModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">Crear nuevo :</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">

                            <form>

                                <div class="row">

                                    <div class="col-sm-12">
                                        <div class="mb-3">
                                            <div wire:ignore>
                                                <label for="" class="mb-2 form-label">ENLAZADO</label>
                                                <select wire:model="category_text" id="category_text"
                                                    class="form-control show-tick ms select2" data-width="100%">
                                                    <option value="">Selecciona</option>
                                                    @foreach ($categories as $c)
                                                        <option value="{{ $c->category_text }}">
                                                            {{ $c->category_text }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="col-sm-12">

                                        <label class="mb-2 form-label text-center">NOMBRE</label>
                                        <input wire:model="name" placeholder="Ingrese un nombre" id="name"
                                            class="form-control">
                                        @error('name')
                                            <span class="error retro text-xs text-red-700">{{ $message }}</span>
                                        @enderror


                                    </div>
                                    <br><br>



                                    @error('category_text')
                                        <span class="error retro text-xs text-red-700">{{ $message }}</span>
                                    @enderror
                                    <div class="col-sm-12 pt-6">
                                        <div class="mb-3">
                                            <label for="" class="mb-2 form-label">HABILITADO</label>
                                            <select wire:model="is_enabled" id="is_enabled"
                                                class="form-control show-tick ms">
                                                <option value="">Seleccione</option>
                                                <option value="Y">Si</option>
                                                <option value="N">No</option>
                                            </select>
                                            @error('is_enabled')
                                                <span class="error retro text-xs text-red-700">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="store"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal">Crear</button>
                    </div>
                </div>
            </div>
        </div>




    </div>

    <script>
        document.addEventListener('livewire:load', function() {
            $('.select2').select2({
                dropdownParent: $('#categoryModal')
            });
            $('.select2').on('change', function() {
                /*  alert(this.value) */
                @this.set('category_text', this.value);
            });
        })
        const table = new basictable('.table');

        new basictable('#table-breakpoint', {
            breakpoint: 768,
        });

        new basictable('#table-container-breakpoint', {
            containerBreakpoint: 485,
        });

        new basictable('#table-force-off', {
            forceResponsive: false,
        });

        new basictable('#table-max-height', {
            tableWrap: true
        });

        new basictable('#table-no-resize', {
            noResize: true,
        });

        new basictable('#table-two-axis');
    </script>
</div>
