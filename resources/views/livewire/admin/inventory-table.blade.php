<div>
    <div class="row">
        <div class="flex items-center col-md-6">
            <label for="simple-search" class="sr-only">Buscar</label>
            <div class="relative w-full">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <i class="fa-sharp fa-regular fa-user"></i>
                </div>
                <input type="text" wire:model="search"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Buscar por usuario">
            </div>
        </div>

        <div class="flex items-center col-md-6">
            <label for="simple-search" class="sr-only">Buscar</label>
            <div class="relative w-full">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <i class="fas fa-gamepad"></i>

                </div>
                <input type="text" wire:model="search_product"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Buscar por producto">
            </div>
            <a href="{{ route('historial_product') }}"
                class="p-2.5 ml-2 text-sm font-medium text-white bg-red-700 rounded-lg border border-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                <i class="fa-solid fa-book"></i>
                <span class="sr-only fa-solid fa-plus">Buscar</span>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table id="table-two-axis" class="two-axis">
                <thead>
                    <tr class="border-b border-gray-500 hover:bg-gray-200">
                        <th class="text-xs">Producto</th>
                        <th class="text-xs">Usuario</th> 
                        <th class="text-xs">En Venta</th>
                        <th class="text-xs">Vendidos</th>
                        <th class="text-xs">Fecha del Registro</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($inventory as $i)
                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                            <td class="text-sm font-semibold text-gray-900"> {{ $i->product->name }}</td>
                            <td class="text-sm font-semibold text-gray-900"> {{ $i->user->user_name }} </td>
                            <td class="text-base text-gray-900">
                                @if ($i->quantity == 0)
                                    <b>No posee</b>
                                @else
                                    {{ $i->quantity }}
                                @endif
                            </td>
                            <td class="text-base text-gray-900">
                                @if ($i->quantity_sold == 0)
                                    <b>Ninguno</b>
                                @else
                                    {{ $i->quantity_sold }}
                                @endif
                            </td>
                            <td class="text-base text-gray-900">
                                {{ date_format($i->created_at, 'Y/m/d') }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="flex flex-col items-center">
                @if ($inventory->count())
                    <div class="flex items-center font-bold justify-between px-1 py-1 border-t border-gray-200 sm:px-6">
                        {{ $inventory->links('pagination') }}
                    </div>
                @else
                    <div
                        class="flex items-center justify-between px-1 py-1 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                        No hay resultados para la busqueda "{{ $search }}" en la página
                        {{ $page }}
                        al mostrar {{ $perPage }}
                    </div>
                @endif
            </div>
        </div>

    </div>

    <script>
        document.addEventListener('livewire:load', function() {
            //Plataforma
            $('.select2').select2();
            $('.select2').on('change', function() {
                @this.set('search_platform', this.value);
            });
            //Categorias
            $('.categories').select2();
            $('.categories').on('change', function() {
                @this.set('search_category', this.value);
            });
            //Lenguaje
            $('.language').select2();
            $('.language').on('change', function() {
                @this.set('search_language', this.value);
            });
            //Idioma de caja
            $('.box_language').select2();
            $('.box_language').on('change', function() {
                @this.set('search_box_language', this.value);
            });
            //Region
            $('.region').select2();
            $('.region').on('change', function() {
                @this.set('search_region', this.value);
            });
            //Habilitar
            $('.enabled').select2();
            $('.enabled').on('change', function() {
                @this.set('search_enabled', this.value);
            });
        })
        const table = new basictable('.table');

        new basictable('#table-breakpoint', {
            breakpoint: 768,
        });

        new basictable('#table-container-breakpoint', {
            containerBreakpoint: 485,
        });

        new basictable('#table-force-off', {
            forceResponsive: false,
        });

        new basictable('#table-max-height', {
            tableWrap: true
        });

        new basictable('#table-no-resize', {
            noResize: true,
        });

        new basictable('#table-two-axis');
    </script>

</div>
