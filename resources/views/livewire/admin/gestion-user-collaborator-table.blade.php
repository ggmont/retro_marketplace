<div>

    <div class="row">
        <div class="flex items-center col-md-3">
            <label for="simple-search" class="sr-only">Buscar</label>
            <div class="relative w-full">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <i class="fa-sharp fa-regular fa-user"></i>
                </div>
                <input type="text" wire:model="search_name"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Buscar por Nombre">
            </div>
        </div>

        <div class="flex items-center col-md-3">
            <label for="simple-search" class="sr-only">Buscar</label>
            <div class="relative w-full">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <i class="fa-sharp fa-regular fa-user"></i>

                </div>
                <input type="text" wire:model="search_last"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Buscar por Apellido">
            </div>
        </div>

        <div class="flex items-center col-md-3">
            <label for="simple-search" class="sr-only">Buscar</label>
            <div class="relative w-full">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <i class="fa-regular fa-user"></i>

                </div>
                <input type="text" wire:model="search_user_name"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Buscar por Usuario">
            </div>
        </div>

        <div class="flex items-center col-md-3">
            <label for="simple-search" class="sr-only">Buscar</label>
            <div class="relative w-full">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <i class="fa-sharp fa-solid fa-envelope"></i>

                </div>
                <input type="text" wire:model="search_email"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Buscar por Correo">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table id="table-two-axis4" class="two-axis">
                <thead>
                    <tr class="border-b border-gray-500 hover:bg-gray-200">
                        <th class="text-xs">NOMBRE</th>
                        <th class="text-xs">IMAGEN</th>
                        <th class="text-xs">EMAIL</th>
                        <th class="text-xs">USUARIO</th>
                        <th class="text-xs">TELÉFONO</th>
                        <th class="text-xs">TRANSFERENCIA</th>
                        <th class="text-xs">CUENTA CREADA</th>
                        <th class="text-xs">ACCION</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($user as $u)
                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                            <td class="text-sm font-semibold text-gray-900">
                                {{ $u->first_name }} / {{ $u->last_name }}
                            </td>
                            <td class="text-sm font-semibold text-gray-900">
                                <div class="flex items-center">
                                    <div class="flex-shrink-0 w-10 h-10">
                                        @if ($u->profile_picture)
                                            <a href="{{ url($u->profile_picture) }}"
                                                data-fancybox="{{ $u->id }}">
                                                <img class="w-10 h-10 rounded-full" src="{{ url($u->profile_picture) }}"
                                                    alt="">
                                            </a>
                                        @else
                                            <a href="{{ asset('img/profile-picture-not-found.png') }}"
                                                data-fancybox="{{ $u->id }}">
                                                <img class="w-10 h-10 rounded-full"
                                                    src="{{ asset('img/profile-picture-not-found.png') }}">
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </td>
                            <td class="text-base text-gray-900">
                                {{ $u->email }}
                            </td>
                            <td class="text-base text-gray-900">
                                {{ $u->user_name }}
                            </td>
                            <td class="text-base text-gray-900">
                                @if ($u->phone)
                                    {{ $u->phone }}
                                @else
                                    -
                                @endif
                            </td>
                            <td class="text-base text-gray-900">
                                {{ $u->concept }}
                            </td>
                            <td class="text-base text-gray-900">
                                {{ date_format($u->created_at, 'Y-m-d') }}
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap">

                                <a href="{{ route('edit_user', [$u->id]) }}" class="btn btn-success btn-sm"
                                    title="Editar"><i class="fa fa-edit"></i></a>
                                <a href="{{ url('/11w5Cj9WDAjnzlg0/users/info', [$u->id]) }}"
                                    class="btn btn-primary btn-sm"><i class="fa fa-eye"></i>
                                    Info/Referidos </a>
                                <a href="{{ route('edit_user', [$u->id]) }}" class="btn btn-danger btn-sm"
                                    title="Editar"><i class="fa fa-history"></i> Ver Historial</a>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="flex flex-col items-center">
                @if ($user->count())
                    <div class="flex items-center font-bold justify-between px-1 py-1 border-t border-gray-200 sm:px-6">
                        {{ $user->links('pagination') }}
                    </div>
                @else
                    <div
                        class="flex items-center justify-between px-1 py-1 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                        No hay resultados para la busqueda en la página
                        {{ $page }}
                        al mostrar {{ $perPage }}
                    </div>
                @endif
            </div>
        </div>

    </div>

    <script>
        new basictable('#table-two-axis4');
    </script>

</div>
