<div>

    <div class="header-area" id="headerArea">
        <div class="container h-100 mt-1 align-items-center justify-content-between">
            <!-- Logo Wrapper-->

            <div class="relative">

                <section class="flexbox">
                    <div class="stretch">
                        <input type="text" class="form-control" placeholder="@lang('messages.search_product')"
                            wire:model.debounce.300ms="query">
                    </div>
                    <div class="normal">
                        @if (Auth::user())
                            <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebarPanel">
                                <span></span><span></span><span></span>
                            </div>
                        @else
                            <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebarPanel">
                                <span></span><span></span><span></span>
                            </div>
                        @endif
                    </div>
                </section>
            </div>


        </div>
    </div>

    <div class="top-products-area clearfix py-3">
        <div class=" container section-heading d-flex align-items-center justify-content-between">
            <h5>@lang('messages.the_categories')</h5>
        </div>
        <div wire:ignore class="flash-sale-slide owl-carousel">

            <div class="owl-item active" style="width: 154px; margin-right: 18px;  padding: 0px 10px;">
                <div class="card collection-card"><a
                        href="https://www.retrogamingmarket.eu/?search_category=Juegos"><img class="rgm-img"
                            src="{{ asset('img/juegos.png') }}" alt="@lang('messages.games')"></a>
                    <div class="collection-title"><span
                            class="font-lg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@lang('messages.games')</span>
                    </div>
                </div>
            </div>

            <div class="owl-item active" style="width: 154px; margin-right: 18px;  padding: 0px 10px;">
                <div class="card collection-card"><a
                        href="https://www.retrogamingmarket.eu/?search_category=Consolas"><img class="rgm-img"
                            width="1280" height="720" src="{{ asset('img/consolas.png') }}"
                            alt="@lang('messages.consoles')"></a>
                    <div class="collection-title"><span>&nbsp;&nbsp;&nbsp;@lang('messages.consoles')</span>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 154px; margin-right: 18px;  padding: 0px 10px;">
                <div class="card collection-card"><a
                        href="https://www.retrogamingmarket.eu/?search_category=Periféricos"><img width="1280"
                            height="720" class="rgm-img" src="{{ asset('img/perifericos.png') }}"
                            alt="@lang('messages.peripherals')"></a>
                    <div class="collection-title"><span>&nbsp;&nbsp;&nbsp;@lang('messages.peripherals')</span>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 154px; margin-right: 18px;  padding: 0px 10px;">
                <div class="card collection-card"><a
                        href="https://www.retrogamingmarket.eu/?search_category=Accesorios"><img width="1280"
                            height="720" class="rgm-img" src="{{ asset('img/accesorios.png') }}"
                            alt="@lang('messages.accesories')"></a>
                    <div class="collection-title"><span>&nbsp;&nbsp;&nbsp;@lang('messages.accesories')</span>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 154px; margin-right: 18px;  padding: 0px 10px;">
                <div class="card collection-card"><a
                        href="https://www.retrogamingmarket.eu/?search_category=Merchandising"><img width="1280"
                            height="720" class="rgm-img" src="{{ asset('img/productos/merchandising.png') }}"
                            alt="@lang('messages.merch')"></a>
                    <div class="collection-title"><span>@lang('messages.merch')</span>
                    </div>
                </div>
            </div>




        </div>
        <br>
        <br>



        <button type="button" class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed"
            data-toggle="modal" data-target="#exampleModal">
            <span class="fa-solid fa-filter"></span>
        </button>
        <div class="container">
            <div class="section-heading d-flex align-items-center justify-content-between">
                <h5 class="ml-1">@lang('messages.last_product')</h5>
            </div>

            <div wire:loading.delay.class="opacity-50" class="row g-3">
                <!-- Single Top Product Card-->
                @isset($seller)
                    @foreach ($seller as $i)
                        <div @if ($loop->last) id="last_record" @endif class="col-6 col-md-4 col-lg-3">
                            <div class="card top-product-card">
                                <div class="card-body">
                                    <a href="{{ route('product-inventory-show', $i->id) }}">
                                        <div class="figure">
                                            <a href="{{ route('product-inventory-show', $i->id) }}">
                                                @if ($i && $i->images && $i->images->first())
                                                    @if (str_starts_with($i->images->first()->image_path, 'https'))
                                                        <img class="card-product-image-holder image-main"
                                                            src="{{ $i->images->first()->image_path }}"
                                                            alt="{{ $i->title }} - RetroGamingMarket">
                                                    @elseif ($i->product_id > 0)
                                                        <img class="card-product-image-holder image-main"
                                                            src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                            alt="{{ $i->product->name }} - RetroGamingMarket">
                                                    @else
                                                        <img class="card-product-image-holder image-main"
                                                            src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                            alt="{{ $i->title }} - RetroGamingMarket">
                                                    @endif
                                                @elseif ($i && $i->product && $i->product->images && $i->product->images->first())
                                                    <img class="card-product-image-holder image-main"
                                                        src="{{ url('/images/' . $i->product->images->first()->image_path) }}"
                                                        alt="{{ $i->product->name }} - RetroGamingMarket">
                                                @else
                                                    <img loading="lazy" class="card-product-image-holder image-main"
                                                        src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                        alt="Aqui esta el error RetroGamingMarket">
                                                @endif
                                            </a>
                                        </div>



                                        <span class="text-sm font-bold sicker">
                                            <a href="{{ route('user-info', $i->user->user_name) }}">
                                                <font color="white">
                                                    {{ str_limit($i->user->user_name, 10) }} - @include('partials.pais_user_home')
                                                </font>
                                            </a>
                                        </span>

                                        @if ($i->title !== null)
                                            <span class="product-title ml-1">{{ str_limit($i->title, 16) }}</span>
                                            <br>
                                            <span class="progress-title ml-1">{{ $i->platform }}</span>
                                            <br>
                                            <span class="progress-title ml-1">{{ $i->region }}</span>
                                        @else
                                            @if ($i->product && $i->product->name)
                                                <span
                                                    class="product-title ml-1">{{ str_limit($i->product->name, 16) }}</span>
                                                <br>
                                                <span
                                                    class="progress-title ml-1">{{ optional($i->product)->platform }}</span>
                                                <br>
                                                <span
                                                    class="progress-title ml-1">{{ optional($i->product)->region }}</span>
                                            @else
                                                <!-- Aquí puedes manejar el caso en el que $i->product o $i->product->name no existan -->
                                            @endif
                                        @endif

                                        <hr style='margin-top:0.5em; margin-bottom:0.5em' />
                                        @if ($i->auction_type > 0)
                                            <div class="flex items-center">
                                                <p class="sale-price ml-1 bg-red-700 text-white px-2 py-1 rounded">
                                                    {{ str_replace(',', '', number_format($i->max_bid, 2, '.', '')) . ' €' }}
                                                </p>

                                                <div wire:ignore class="ml-auto mb-1 flex items-center">
                                                    <ion-icon size="small" class="mr-1 text-gray-900"
                                                        data-toggle="popover"
                                                        data-content="{{ $i->countdown_hours }} horas"
                                                        data-placement="auto" data-offset="0,10" data-trigger="hover"
                                                        name="timer-outline">
                                                    </ion-icon>
                                                    <!-- Icono de reloj de Ionicons con mensaje de 24 horas -->
                                                    <ion-icon size="small" class="mr-1 text-gray-900"
                                                        data-toggle="popover"
                                                        data-content="Envío certificado con seguro incluído desde 2,90€"
                                                        data-placement="auto" data-offset="0,10" data-trigger="hover"
                                                        name="paper-plane">
                                                    </ion-icon>
                                                    <!-- Icono de envío de Ionicons -->
                                                    <ion-icon size="small" class="mr-1 text-gray-900"
                                                        data-toggle="popover" data-html="true"
                                                        data-content="Pago seguro con Beseif. <a href='https://www.retrogamingmarket.eu/site/gastos-de-envio'>Más Información</a>"
                                                        data-placement="auto" data-offset="0,10" data-trigger="hover"
                                                        name="shield-checkmark">
                                                    </ion-icon>
                                                    <!-- Icono de seguridad de Font Awesome -->
                                                </div>
                                            </div>
                                        @else
                                            <div class="flex items-center">
                                                <p class="sale-price ml-1">{{ number_format($i->price, 2) . ' €' }}</p>
                                                <div wire:ignore class="ml-auto mb-1 flex items-center">
                                                    <ion-icon size="small" class="mr-1 text-gray-900"
                                                        data-toggle="popover"
                                                        data-content="Envío certificado con seguro incluído desde 2,90€"
                                                        data-placement="auto" data-offset="0,10" data-trigger="hover"
                                                        name="paper-plane">
                                                    </ion-icon>
                                                    <!-- Icono de envío de Ionicons -->
                                                    <ion-icon size="small" class="mr-1 text-gray-900"
                                                        data-toggle="popover" data-html="true"
                                                        data-content="Pago seguro con Beseif. <a href='https://www.retrogamingmarket.eu/site/gastos-de-envio'>Más Información</a>"
                                                        data-placement="auto" data-offset="0,10" data-trigger="hover"
                                                        name="shield-checkmark">
                                                    </ion-icon>
                                                    <!-- Icono de seguridad de Font Awesome -->
                                                </div>
                                            </div>
                                        @endif
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endisset
            </div>
            @if ($loadAmount >= $totalRecords)
                <p class="text-gray-800 font-bold text-2xl text-center my-10">@lang('messages.not_product')</p>
            @endif

            @if ($seller->isEmpty())
                <p class="text-gray-800 font-bold text-2xl text-center my-10">No existe ningún producto relacionado a
                    tu búsqueda</p>
            @endif

        </div>
        @if (
            $query !== '' ||
                $search_country !== '' ||
                $search_platform !== '' ||
                $search_category !== '' ||
                $search_region !== '')
            <button wire:click="clear" type="button"
                class="filterToggleDelete d-lg-none btn btn-lg btn-danger btn-rounded btn-absolute">
                <span class="fa-solid fa-trash"></span>
            </button>
        @endif
        <!-- Modal -->
        <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.filter_by') :</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block pb-1 pt-2">

                                <form>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Tipo de ventas</label>

                                            <select wire:model="search_sales" style="width: 100%" id="search_sales"
                                                class="form-control form-select">
                                                <option value="" selected> @lang('messages.alls')
                                                </option>
                                                <option value="0">Precio fijo
                                                </option>
                                                <option value="1">Ofertas
                                                </option>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">@lang('messages.country')</label>
                                            <div wire:ignore>
                                                <select wire:model="search_country" style="width: 100%"
                                                    id="search_country" class="form-control country2 form-select">
                                                    <option value="" selected> @lang('messages.alls')
                                                    </option>
                                                    @foreach ($country as $c)
                                                        <option value="{{ $c->name }}">{{ $c->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">@lang('messages.platform')</label>
                                            <div wire:ignore>
                                                <select wire:model="search_platform" style="width: 100%"
                                                    id="search_platform" class="form-control select2 form-select">
                                                    <option value="" selected> @lang('messages.alls')
                                                    </option>
                                                    @foreach ($platform as $p)
                                                        <option value="{{ $p->value }}">{{ $p->value }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">@lang('messages.the_categorie')</label>
                                            <div wire:ignore>
                                                <select wire:model="search_category" style="width: 100%"
                                                    id="search_category" class="form-control ultra2 form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    <optgroup label="@lang('messages.category')">
                                                        <option value="Juegos">@lang('messages.games')</option>
                                                        <option value="Consolas">@lang('messages.consoles')</option>
                                                        <option value="Periféricos">@lang('messages.peripherals')</option>
                                                        <option value="Accesorios">@lang('messages.accesories')</option>
                                                        <option value="Merchandising">@lang('messages.merch')</option>
                                                    </optgroup>
                                                    <optgroup label="@lang('messages.sub_category')">
                                                        <option value="Mandos">@lang('messages.controls')</option>
                                                        <option value="Micrófonos">@lang('messages.microphones')</option>
                                                        <option value="Teclados">@lang('messages.keyboard')</option>
                                                        <option value="Fundas">@lang('messages.funded')</option>
                                                        <option value="Cables">@lang('messages.cable')</option>
                                                        <option value="Cargadores">@lang('messages.chargers')</option>
                                                        <option value="Merchandising -> Logos 3d">@lang('messages.3d_logo')
                                                        </option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">@lang('messages.the_region')</label>
                                            <div wire:ignore>
                                                <select wire:model="search_region" style="width: 100%"
                                                    id="search_region" class="form-control region2 form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    @foreach ($region as $r)
                                                        <option value="{{ $r->value_id }}">{{ $r->value_id }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>

            <script>
                document.addEventListener('livewire:load', function() {
                    $('.sale2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.select2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.ultra2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.region2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.country2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.language2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.media2').select2({
                        dropdownParent: $('#exampleModal')
                    });

                    // Aplicar valores de filtros cuando cambian
                    $('.sale2').on('change', function() {
                        @this.set('search_sales', this.value);
                    });

                    $('.select2').on('change', function() {
                        @this.set('search_platform', this.value);
                    });

                    $('.ultra2').on('change', function() {
                        @this.set('search_category', this.value);
                    });

                    $('.region2').on('change', function() {
                        @this.set('search_region', this.value);
                    });

                    $('.country2').on('change', function() {
                        @this.set('search_country', this.value);
                    });

                    $('.language2').on('change', function() {
                        @this.set('search_language', this.value);
                    });

                    $('.media2').on('change', function() {
                        @this.set('search_media', this.value);
                    });
                })



                document.addEventListener('DOMContentLoaded', function() {
                    // Recuperar la última posición de desplazamiento
                    const lastScrollPosition = localStorage.getItem('lastScrollPosition');
                    if (lastScrollPosition) {
                        window.scrollTo(0, lastScrollPosition);
                    }

                    const lastRecord = document.getElementById('last_record');
                    if (lastRecord) {
                        const options = {
                            root: null,
                            threshold: 1,
                            rootMargin: '0px'
                        };
                        const observer = new IntersectionObserver((entries, observer) => {
                            entries.forEach(entry => {
                                if (entry.isIntersecting) {
                                    @this.loadMore();
                                }
                            });
                        }, options);
                        observer.observe(lastRecord);
                    }

                    // Almacenar la posición de desplazamiento al salir de la página
                    window.addEventListener('beforeunload', function() {
                        localStorage.setItem('lastScrollPosition', window.scrollY);
                    });
                });
            </script>



        </div>
    </div>
</div>
