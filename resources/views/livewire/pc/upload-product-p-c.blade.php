<div>
    <div class="relative py-10 pb-20">
        <div class="absolute inset-0"
            style="background-image: url('https://www.retrogamingmarket.eu/landing/assets/images/wp-20151115-4-1000x561.jpg'); filter: blur(10px);">
        </div>
        <div class="container mx-auto text-center text-white relative z-10">
            <h1 class="text-4xl font-bold mb-2" style="text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5); color: white;">
                SUBIR/VENDER PRODUCTO</h1>

            <br>

            <button type="button" data-toggle="modal" data-target="#informationModal"
                class="bg-red-500 text-white px-4 py-2 rounded-full hover:bg-red-600 ml-2">
                <span class="whitespace-nowrap">MAS INFORMACIÓN</span>
            </button>
        </div>
    </div>

    <div class="container mx-auto py-5">
        <form class="flex items-center">
            <input type="text" wire:model.debounce.300ms="query" placeholder="Buscar"
                class="px-4 py-2 border border-gray-400 rounded-l w-full focus:outline-none">
            <button type="button" data-toggle="modal" data-target="#exampleModal"
                class="bg-red-500 text-white px-4 py-2 rounded-full hover:bg-red-600 ml-2">
                <span class="whitespace-nowrap">Búsqueda avanzada</span>
            </button>
        </form>
    </div>


    <div class="top-products-area clearfix py-3">

        <button type="button" class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed"
            data-toggle="modal" data-target="#exampleModal">
            <span class="fa-solid fa-filter"></span>
        </button>
        <div class="container">
            <div class="section-heading d-flex align-items-center justify-content-between">
                <h5 class="text-2xl">Productos</h5>
            </div>

            <div wire:loading.delay.class="opacity-50" class="row g-3">
                @if (strlen($query) == 0)
                    @isset($random)
                        @foreach ($random as $r)
                            <div @if ($loop->last) id="last_record" @endif class="col-6 col-md-4 col-lg-3">
                                <div class="card top-product-card">
                                    <div class="card-body">
                                        <div class="figure">
                                            @if ($r->images->first())
                                                <img class="card-product-image-holder image-main"
                                                    src="{{ url('/images/' . $r->imageSearchBar()) }}" alt="">
                                            @elseif ($r->images->first())
                                                <img class=" card-product-image-holder image-main"
                                                    src="{{ url('/images/' . $r->images->first()->image_path) }}"
                                                    alt="">
                                            @else
                                                <img loading="lazy" class="card-product-image-holder image-main"
                                                    src="{{ asset('assets/images/art-not-found.jpg') }}" alt="">
                                            @endif
                                        </div>
                                        @if ($r->title !== null)
                                            <span class="product-title ml-1">{{ str_limit($r->title, 16) }}
                                            </span>

                                            <br>
                                            <br>
                                        @else
                                            <span class="product-title ml-1">{{ str_limit($r->name, 16) }}
                                            </span>

                                            <br>
                                            <span class="progress-title ml-1">
                                                {{ $r->platform }}

                                            </span>
                                            <br>
                                            <span class="progress-title ml-1">

                                                {{ $r->region }}
                                            </span>
                                        @endif



                                        @if ($r->comments == '')
                                            <br>
                                            <span class="progress-title ml-1">

                                                -

                                            </span>
                                        @else
                                            <br>
                                            <span class="progress-title ml-1">

                                                {{ str_limit($r->comments, 20) }}

                                            </span>
                                        @endif

                                        <hr style='margin-top:0.5em; margin-bottom:0.5em' />
                                        <a href="/offerproduct/{{ $r['id'] + 1000 }}"
                                            class="btn btn-success w-100 font-bold">VENDER</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endisset
                    @if ($products->count() > 0)
                        @isset($random)
                            @foreach ($random as $r)
                                <div @if ($loop->last) id="last_record" @endif
                                    class="col-6 col-md-4 col-lg-3">
                                    <div class="card top-product-card">
                                        <div class="card-body">
                                            <div class="figure">
                                                @if ($r->images->first())
                                                    <img class="card-product-image-holder image-main"
                                                        src="{{ url('/images/' . $r->imageSearchBar()) }}" alt="">
                                                @elseif ($r->images->first())
                                                    <img class=" card-product-image-holder image-main"
                                                        src="{{ url('/images/' . $r->images->first()->image_path) }}"
                                                        alt="">
                                                @else
                                                    <img loading="lazy" class="card-product-image-holder image-main"
                                                        src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                        alt="">
                                                @endif
                                            </div>
                                            @if ($r->title !== null)
                                                <span class="product-title ml-1">{{ str_limit($r->title, 16) }}
                                                </span>

                                                <br>
                                                <br>
                                            @else
                                                <span class="product-title ml-1">{{ str_limit($r->name, 16) }}
                                                </span>

                                                <br>
                                                <span class="progress-title ml-1">
                                                    {{ $r->platform }}

                                                </span>
                                                <br>
                                                <span class="progress-title ml-1">

                                                    {{ $r->region }}
                                                </span>
                                            @endif



                                            @if ($r->comments == '')
                                                <br>
                                                <span class="progress-title ml-1">

                                                    -

                                                </span>
                                            @else
                                                <br>
                                                <span class="progress-title ml-1">

                                                    {{ str_limit($r->comments, 20) }}

                                                </span>
                                            @endif

                                            <hr style='margin-top:0.5em; margin-bottom:0.5em' />
                                            <a href="/offerproduct/{{ $r['id'] + 1000 }}"
                                                class="btn btn-success w-100 font-bold">VENDER</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endisset
                    @endif
                @else
                    @isset($products)
                        @foreach ($products as $p)
                            <div @if ($loop->last) id="last_record" @endif class="col-6 col-md-4 col-lg-3">
                                <div class="card top-product-card">
                                    <div class="card-body">
                                        <div class="figure">
                                            @if ($p->images->first())
                                                <img class="card-product-image-holder image-main"
                                                    src="{{ url('/images/' . $p->imageSearchBar()) }}" alt="">
                                            @elseif ($p->images->first())
                                                <img class=" card-product-image-holder image-main"
                                                    src="{{ url('/images/' . $p->images->first()->image_path) }}"
                                                    alt="">
                                            @else
                                                <img loading="lazy" class="card-product-image-holder image-main"
                                                    src="{{ asset('assets/images/art-not-found.jpg') }}" alt="">
                                            @endif
                                        </div>
 
                                            <span class="product-title ml-1">{{ mb_strimwidth($p->name, 0, 34, '...') }}
                                            </span>

                                            <br>
                                            <span class="progress-title ml-1">
                                                {{ $p->platform }}

                                            </span>
                                            <br>
                                            <span class="progress-title ml-1">

                                                {{ $p->region }}
                                            </span>
                                  



                                        @if ($p->comments == '')
                                            <br>
                                            <span class="progress-title ml-1">

                                                -

                                            </span>
                                        @else
                                            <br>
                                            <span class="progress-title ml-1">

                                                {{ str_limit($p->comments, 20) }}

                                            </span>
                                        @endif

                                        <hr style='margin-top:0.5em; margin-bottom:0.5em' />
                                        <a href="/offerproduct/{{ $p['id'] + 1000 }}"
                                            class="btn btn-success w-100 font-bold">VENDER</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endisset
                @endif
            </div>
            @if ($query !== '' || $search_platform !== '' || $search_category !== '' || $search_region !== '')
                <button wire:click="clear" type="button"
                    class="filterToggleDelete d-lg-none btn btn-lg btn-danger btn-rounded btn-absolute">
                    <span class="fa-solid fa-trash"></span>
                </button>
            @endif
            <!-- Modal -->
            <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content bg-white">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="d-flex align-items-center justify-content-between mb-4">
                                    <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.filter_by') :</h4>
                                    <button class="btn btn-close p-1 ms-auto me-0" class="close"
                                        data-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="wide-block pb-1 pt-2">

                                    <form>

                                        <div class="form-group boxed">
                                            <div class="input-wrapper">
                                                <label class="form-label" for="city5">@lang('messages.platform')</label>
                                                <div wire:ignore>
                                                    <select wire:model="search_platform" style="width: 100%"
                                                        class="form-control select2 form-select">
                                                        <option value="" selected> @lang('messages.alls')
                                                        </option>
                                                        @foreach ($platform as $p)
                                                            <option value="{{ $p->value }}">{{ $p->value }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group boxed">
                                            <div class="input-wrapper">
                                                <label class="form-label" for="city5">@lang('messages.the_categorie')</label>
                                                <div wire:ignore>
                                                    <select wire:model="search_category" style="width: 100%"
                                                        class="form-control ultra2 form-select">
                                                        <option value="" selected>@lang('messages.alls')
                                                        </option>
                                                        <optgroup label="@lang('messages.category')">
                                                            <option value="Juegos">@lang('messages.games')</option>
                                                            <option value="Consolas">@lang('messages.consoles')</option>
                                                            <option value="Periféricos">@lang('messages.peripherals')</option>
                                                            <option value="Accesorios">@lang('messages.accesories')</option>
                                                            <option value="Merchandising">@lang('messages.merch')</option>
                                                        </optgroup>
                                                        <optgroup label="@lang('messages.sub_category')">
                                                            <option value="Mandos">@lang('messages.controls')</option>
                                                            <option value="Micrófonos">@lang('messages.microphones')</option>
                                                            <option value="Teclados">@lang('messages.keyboard')</option>
                                                            <option value="Fundas">@lang('messages.funded')</option>
                                                            <option value="Cables">@lang('messages.cable')</option>
                                                            <option value="Cargadores">@lang('messages.chargers')</option>
                                                            <option value="Merchandising -> Logos 3d">
                                                                @lang('messages.3d_logo')
                                                            </option>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group boxed">
                                            <div class="input-wrapper">
                                                <label class="form-label" for="city5">@lang('messages.the_region')</label>
                                                <div wire:ignore>
                                                    <select wire:model="search_region" style="width: 100%"
                                                        class="form-control region2 form-select">
                                                        <option value="" selected>@lang('messages.alls')
                                                        </option>
                                                        @foreach ($region as $r)
                                                            <option value="{{ $r->value_id }}">{{ $r->value_id }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </form>

                                </div>
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>
                </div>

                <script>
                    document.addEventListener('livewire:load', function() {
                        $('.select2').select2({
                            dropdownParent: $('#exampleModal')
                        });
                        $('.ultra2').select2({
                            dropdownParent: $('#exampleModal')
                        });
                        $('.region2').select2({
                            dropdownParent: $('#exampleModal')
                        });
                        $('.country2').select2({
                            dropdownParent: $('#exampleModal')
                        });
                        $('.language2').select2({
                            dropdownParent: $('#exampleModal')
                        });
                        $('.media2').select2({
                            dropdownParent: $('#exampleModal')
                        });
                        $('.select2').on('change', function() {
                            /*  alert(this.value) */
                            @this.set('search_platform', this.value);
                        });
                        $('.ultra2').on('change', function() {
                            /*  alert(this.value) */
                            @this.set('search_category', this.value);
                        });
                        $('.region2').on('change', function() {
                            /*  alert(this.value) */
                            @this.set('search_region', this.value);
                        });
                        $('.country2').on('change', function() {
                            /*  alert(this.value) */
                            @this.set('search_country', this.value);
                        });
                        $('.language2').on('change', function() {
                            /*  alert(this.value) */
                            @this.set('search_language', this.value);
                        });
                        $('.media2').on('change', function() {
                            /*  alert(this.value) */
                            @this.set('search_media', this.value);
                        });
                    })
                    document.addEventListener('DOMContentLoaded', function() {
                        const lastRecord = document.getElementById('last_record');
                        if (lastRecord) {
                            const options = {
                                root: null,
                                threshold: 1,
                                rootMargin: '0px'
                            };
                            const observer = new IntersectionObserver((entries, observer) => {
                                entries.forEach(entry => {
                                    if (entry.isIntersecting) {
                                        @this.loadMore();
                                    }
                                });
                            }, options);
                            observer.observe(lastRecord);
                        }
                    });
                </script>



            </div>
        </div>


        <div wire:ignore.self class="modal fade" id="informationModal" tabindex="-1" role="dialog"
            aria-labelledby="informationModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title text-center" id="informationModalLabel">Información Básica</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block pb-1 pt-2">
                                <h4>¿Por qué tengo que buscar el producto?</h4>
                                <p>
                                    Seleccionando un producto de la base de datos podemos ofrecerte una estimacion del
                                    precio de mercado
                                    aumentando
                                    la posibilidad de venta.
                                </p>
                                <h4>¿No encuentras tu producto?</h4>
                                <p>
                                    Si no encuentras tu producto, no te preocupes.<br>
                                    Accede al formulario "No encuentro mi producto" y nosotros lo subiremos por ti.
                                </p>
                                <br>
                                <div class="text-center">
                                    <a href="{{ route('product_not_found') }}"
                                        class="btn btn-danger w-100 font-bold">NO ENCUENTRO MI
                                        PRODUCTO</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
