<div>


    <div class="relative py-10 pb-20">
        <div class="absolute inset-0"
            style="background-image: url('https://www.retrogamingmarket.eu/landing/assets/images/wp-20151115-4-1000x561.jpg'); filter: blur(10px);">
        </div>
        <div class="container mx-auto text-center text-white relative z-10">
            <h1 class="text-6xl font-extrabold mb-4 text-white" style="text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);">
                RETRO GAMING MARKET PC
            </h1>
            <p class="text-2xl mb-8 font-semibold text-white" style="text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);">
                COMPRA, VENDE Y COLECCIONA EN TODA EUROPA
            </p>
    
            <p class="text-2xl mb-8 font-semibold text-white" style="text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);">
                PÁGINA EN CONSTRUCCIÓN
            </p>
    
            <button type="button" data-toggle="modal" data-target="#informationModal"
                class="bg-red-500 text-white px-6 py-3 rounded-full hover:bg-red-600 ml-2">
                <span class="whitespace-nowrap">MAS INFORMACIÓN</span>
            </button>
        </div>
    </div>
    
 
    <div wire:ignore.self class="modal fade" id="uploadModal" tabindex="-1" role="dialog"
        aria-labelledby="informationModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title text-center" id="informationModalLabel">¿Cómo quieres publicar tu anuncio?</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <h4>Normal</h4>
                            <p>
                                Pon el titulo , describe tu producto y cuánto pesa.
                            </p>
            
                            <div class="text-center">
                                <a href="{{ route('normal_upload') }}" class="btn btn-danger w-100 font-bold">NORMAL</a>
                            </div>

                            <br>

                            <h4>Coleccionista</h4>
                            <p>
                               Aumenta la probabilidad de venta subiendo aqui tu producto.<br>
                               Busca el juego en la base de datos y descubre a cuánto lo puedes vender detallando al máximo su estado.  
                            </p>
            
                            <div class="text-center">
                                <a href="{{ route('upload_product') }}" class="btn btn-danger w-100 font-bold">COLECCIONISTA</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="informationModal" tabindex="-1" role="dialog"
        aria-labelledby="informationModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title text-center" id="informationModalLabel">¡Versión para PC en
                                construcción!</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <p>Estamos trabajando arduamente en la versión para PC de RetroGamingMarket. Mientras tanto,
                                te
                                invitamos a disfrutar de nuestra versión móvil o descargar nuestra aplicación de Android
                                para
                                una experiencia completa.</p>
                        </div>
                        <div class="mt-4 text-center">
                            <div class="p-2">
                                <h4 class="text-lg font-semibold mb-3">Síguenos en nuestras redes sociales</h4>
                                <div class="flex justify-center space-x-4">
                                    <a href="https://www.facebook.com/people/Retrogamingmarket/100063863901900/"
                                        class="btn btn-icon btn-sm btn-facebook">
                                        <ion-icon name="logo-facebook"></ion-icon>
                                    </a>
                                    <a href="https://twitter.com/Retrogamingmkt"
                                        class="btn btn-icon btn-sm btn-twitter">
                                        <ion-icon name="logo-twitter"></ion-icon>
                                    </a>
                                    <a href="https://www.instagram.com/retrogamingmarket/"
                                        class="btn btn-icon btn-sm btn-instagram">
                                        <ion-icon name="logo-instagram"></ion-icon>
                                    </a>
                                    <a href="https://www.twitch.tv/retrogamingmarket"
                                        class="btn btn-icon btn-sm btn-twitch">
                                        <ion-icon name="logo-twitch"></ion-icon>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.filter_by') :</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">

                            <form>

                                <div wire:ignore>
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">@lang('messages.country')</label>

                                            <select wire:model="search_country" style="width: 100%"
                                                class="form-control country2 form-select" id="city5">
                                                <option value="" selected> @lang('messages.alls')
                                                </option>
                                                @foreach ($country as $c)
                                                    <option value="{{ $c->name }}">
                                                        {{ __($c->name) }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="city5">@lang('messages.platform')</label>
                                        <div wire:ignore>
                                            <select wire:model="search_platform" style="width: 100%"
                                                class="form-control select2 form-select">
                                                <option value="" selected> @lang('messages.alls')
                                                </option>
                                                @foreach ($platform as $p)
                                                    <option value="{{ $p->value }}">{{ $p->value }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="city5">@lang('messages.the_categorie')</label>
                                        <div wire:ignore>
                                            <select wire:model="search_category" style="width: 100%"
                                                class="form-control ultra2 form-select">
                                                <option value="" selected>@lang('messages.alls')
                                                </option>
                                                <optgroup label="@lang('messages.category')">
                                                    <option value="Juegos">@lang('messages.games')</option>
                                                    <option value="Consolas">@lang('messages.consoles')</option>
                                                    <option value="Periféricos">@lang('messages.peripherals')</option>
                                                    <option value="Accesorios">@lang('messages.accesories')</option>
                                                    <option value="Merchandising">@lang('messages.merch')</option>
                                                </optgroup>
                                                <optgroup label="@lang('messages.sub_category')">
                                                    <option value="Mandos">@lang('messages.controls')</option>
                                                    <option value="Micrófonos">@lang('messages.microphones')</option>
                                                    <option value="Teclados">@lang('messages.keyboard')</option>
                                                    <option value="Fundas">@lang('messages.funded')</option>
                                                    <option value="Cables">@lang('messages.cable')</option>
                                                    <option value="Cargadores">@lang('messages.chargers')</option>
                                                    <option value="Merchandising -> Logos 3d">@lang('messages.3d_logo')
                                                    </option>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="city5">@lang('messages.the_region')</label>
                                        <div wire:ignore>
                                            <select wire:model="search_region" style="width: 100%"
                                                class="form-control region2 form-select">
                                                <option value="" selected>@lang('messages.alls')
                                                </option>
                                                @foreach ($region as $r)
                                                    <option value="{{ $r->value_id }}">{{ $r->value_id }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>

        <script>
            document.addEventListener('livewire:load', function() {
                $('.select2').select2({
                    dropdownParent: $('#exampleModal')
                });
                $('.ultra2').select2({
                    dropdownParent: $('#exampleModal')
                });
                $('.region2').select2({
                    dropdownParent: $('#exampleModal')
                });
                $('.country2').select2({
                    dropdownParent: $('#exampleModal')
                });
                $('.language2').select2({
                    dropdownParent: $('#exampleModal')
                });
                $('.media2').select2({
                    dropdownParent: $('#exampleModal')
                });
                $('.select2').on('change', function() {
                    /*  alert(this.value) */
                    @this.set('search_platform', this.value);
                });
                $('.ultra2').on('change', function() {
                    /*  alert(this.value) */
                    @this.set('search_category', this.value);
                });
                $('.region2').on('change', function() {
                    /*  alert(this.value) */
                    @this.set('search_region', this.value);
                });
                $('.country2').on('change', function() {
                    /*  alert(this.value) */
                    @this.set('search_country', this.value);
                });
                $('.language2').on('change', function() {
                    /*  alert(this.value) */
                    @this.set('search_language', this.value);
                });
                $('.media2').on('change', function() {
                    /*  alert(this.value) */
                    @this.set('search_media', this.value);
                });
            })
            document.addEventListener('DOMContentLoaded', function() {
                const lastRecord = document.getElementById('last_record');
                if (lastRecord) {
                    const options = {
                        root: null,
                        threshold: 1,
                        rootMargin: '0px'
                    };
                    const observer = new IntersectionObserver((entries, observer) => {
                        entries.forEach(entry => {
                            if (entry.isIntersecting) {
                                @this.loadMore();
                            }
                        });
                    }, options);
                    observer.observe(lastRecord);
                }
            });
        </script>



    </div>
</div>
