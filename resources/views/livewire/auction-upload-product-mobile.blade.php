<div>
    <div class="form-group searchbox-product">
        <input type="text" class="form-control" placeholder="@lang('messages.search_product')" wire:model.debounce.300ms="query">

    </div>
    <div class="form-group boxed">
        <div class="input-wrapper">
            <label class="form-label" for="city5">Categoria</label>
            <div wire:ignore>
                <select wire:model="search_category" class="form-control category2 form-select" id="city5">
                    <option value="" selected>@lang('messages.alls')
                    </option>
                    <optgroup label="@lang('messages.category')">
                        <option value="Juegos">@lang('messages.games')</option>
                        <option value="Consolas">@lang('messages.consoles')</option>
                        <option value="Periféricos">@lang('messages.peripherals')</option>
                        <option value="Accesorios">@lang('messages.accesories')</option>
                        <option value="Merchandising">Merchandising</option>
                    </optgroup>
                    <optgroup label="@lang('messages.sub_category')">
                        <option value="Mandos">@lang('messages.controls')</option>
                        <option value="Micrófonos">@lang('messages.microphones')</option>
                        <option value="Teclados">@lang('messages.keyboard')</option>
                        <option value="Fundas">@lang('messages.funded')</option>
                        <option value="Cables">@lang('messages.cable')</option>
                        <option value="Cargadores">@lang('messages.chargers')</option>
                        <option value="Merchandising -> Logos 3d">@lang('messages.3d_logo')</option>
                    </optgroup>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group boxed">
        <div class="input-wrapper">
            <label class="form-label" for="city5">Plataforma</label>
            <div wire:ignore>
                <select wire:model="search_platform" class="form-control select2 form-select">
                    <option value="" selected> @lang('messages.alls')
                    </option>
                    @foreach ($platform as $p)
                        <option value="{{ $p->value }}">{{ $p->value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="form-group boxed">
        <div class="input-wrapper">
            <label class="form-label" for="city5">Region - <svg width="20px" height="20px" data-html="true"
                    data-toggle="popover"
                    data-content="
               - Si la caja está en diferentes idiomas - PAL-EU <br />
               - Si la caja está solo en Español - PAL-ESP <br />
               - Si la caja está en Alemán - PAL-DE <br />
               - Si la caja está en Italiano - PAL-ITA <br />
               - Si la caja está en Francés - PAL-FRA <br />
               - Si el juego está en inglés y tiene etiqueta con una M en el frontal - es NTSC-U <br />
               - Si el juego tiene una etiqueta en el frontal marcada con USK es PAL-UK<br />
               - Si la portada e información del juego está en japonés es NTSC-J"
                    data-placement="top" data-trigger="hover" viewBox="0 0 76 76" xmlns="http://www.w3.org/2000/svg">
                    <path id="greyshadow" d="M8 8h68v68h-68z" fill="#BFBFBF" />
                    <path id="blackborder" d="M4 4h68v68h-68z" fill="#000" />
                    <path id="background" d="M4 4h64v64h-64z" fill="#FFC07C" />
                    <path id="borderlefttop" d="M4 0h64M0 4v64" stroke="#DE5917" stroke-width="8" />
                    <path id="rivets" d="M8 8h4v4h-4zM60 60h4v4h-4zM8 60h4v4h-4zM60 8h4v4h-4z" fill="#000" />
                    <path id="questionshadow" d="M24 20h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM36 52h8v8h-8z"
                        fill="#000" />
                    <path id="question" d="M20 16h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM32 48h8v8h-8z"
                        fill="#DE5917" />
                </svg></label>
            <div wire:ignore>
                <select wire:model="search_region" class="form-control region2 form-select">
                    <option value="" selected>@lang('messages.alls')
                    </option>
                    @foreach ($region as $r)
                        <option value="{{ $r->value_id }}">{{ $r->value_id }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    @if (strlen($query) == 0)
        <br>

        <h4>¿Por qué tengo que buscar el producto?</h4>
        <p>
            Seleccionando un producto de la base de datos podemos ofrecerte una estimacion del precio de mercado
            aumentando
            la posibilidad de venta.
        </p>
        <h4>¿No encuentras tu producto?</h4>
        <p>
            Si no encuentras tu producto, no te preocupes.<br>
            Accede al formulario "No encuentro mi producto" y nosotros lo subiremos por ti.
        </p>
        <br>
        <div class="text-center">
            <a href="{{ route('product_auction_not_found') }}" class="btn btn-danger w-100 font-bold">NO ENCUENTRO MI
                PRODUCTO</a>

        </div>
    @else
        <br>
        @if ($products->count() > 0)
            @foreach ($products as $product)
                <div @if ($loop->last) id="last_record" @endif class="row g-3">
                    <!-- Single Weekly Product Card-->
                    <div class="col-12 col-md-6">
                        <div class="card weekly-product-card">
                            <div class="card-body d-flex align-items-center">
                                <div class="product-thumbnail-side"><a class="product-thumbnail d-block"
                                        href="{{ route('product-show', $product['id']) }}"><img
                                            src="{{ url('/images/' . $product->imageSearchBar()) }}" alt="{{ $product['name'] }} RetroGamingMarket"></a>
                                </div>
                                <div class="product-description"><a class="product-title d-block"
                                        href="{{ route('product-show', $product['id']) }}"> {{ $product['name'] }}</a>

                                    {{ $product['platform'] }} - {{ $product['region'] }}
                                    <div class="product-rating">
                                    </div>
                                    
                                    <a class="btn btn-success btn-sm add2cart-notify"
                                        href="/offer-auction-product/{{ $product['id'] + 1000 }}"><i
                                            class="mr-1 lni lni lni-euro"></i>Poner en oferta</a>
                                    <br>
                                    <div class="pt-1"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            @endforeach
            @if ($loadAmount >= $totalRecords)
                <p class="text-gray-800 font-bold text-2xl text-center my-10">No hay mas productos disponibles!</p>
            @endif
        @else
            <div class="text-center">
                <h4>- Sin Resultados -</h4>
            </div>
            <div class="text-center">
                <a href="{{ route('product_not_found') }}" class="btn btn-danger w-100 font-bold">NO
                    ENCUENTRO MI PRODUCTO</a>

            </div>
        @endif
    @endif
    <script>
        document.addEventListener('livewire:load', function() {
            $(function() {
                    $('[data-toggle="popover"]').popover()
                }),
                $('.select2').select2();
            $('.category2').select2();
            $('.region2').select2();
            $('.select2').on('change', function() {
                /*  alert(this.value) */
                @this.set('search_platform', this.value);
            });
            $('.category2').on('change', function() {
                /*  alert(this.value) */
                @this.set('search_category', this.value);
            });
            $('.region2').on('change', function() {
                /*  alert(this.value) */
                @this.set('search_region', this.value);
            });
        })
        document.addEventListener('DOMContentLoaded', function() {
            const lastRecord = document.getElementById('last_record');
            if (lastRecord) {
                const options = {
                    root: null,
                    threshold: 1,
                    rootMargin: '0px'
                };
                const observer = new IntersectionObserver((entries, observer) => {
                    entries.forEach(entry => {
                        if (entry.isIntersecting) {
                            @this.loadMore();
                        }
                    });
                }, options);
                observer.observe(lastRecord);
            }
        });
    </script>
</div>
