<div>
    <div class="wishlist-table-area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="my-6 bg-white rounded shadow-md">
                        <div class="my-6 bg-white rounded shadow-md">
                            <div class="tabs">
                                <div class="border-b tab">
                                    <div class="relative border-l-2 border-transparent">
                                        <input class="absolute z-10 w-full h-5 opacity-0 cursor-pointer top-6"
                                            type="checkbox" id="chck1">
                                        <header
                                            class="flex items-center justify-between p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                            for="chck1">
                                            <span class="text-xl font-thin text-grey-darkest">
                                                <h4 class="text-gray-900 retro"> {{ __('Filtrar por') . ':' }} </h4>
                                            </span>
                                            <div
                                                class="flex items-center justify-center border rounded-full border-grey w-7 h-7 test">
                                                <!-- icon by feathericons.com -->
                                                <svg aria-hidden="true" class="" data-reactid="266"
                                                    fill="none" height="24" stroke="#606F7B" stroke-linecap="round"
                                                    stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24"
                                                    width="24" xmlns="http://www.w3.org/2000/svg">
                                                    <polyline points="6 9 12 15 18 9">
                                                    </polyline>
                                                </svg>
                                            </div>
                                        </header>
                                        <div class="tab-content">
                                            <div class="col-md-12">
                                                <input wire:model="search"
                                                    class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                                    type="text" placeholder="@lang('messages.seller_inventory_search')">
                                                <div class="row">


                                                    <div class="col-md-4">
                                                        <label
                                                            class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase retro"
                                                            for="grid-state">
                                                            @lang('messages.box_condition')
                                                        </label>
                                                        <div class="relative">
                                                            <div class="nes-select">

                                                                <select wire:model="search_box" class="retro">
                                                                    <option value="" selected>@lang('messages.alls')
                                                                    </option>
                                                                    @foreach ($condition as $p)
                                                                        <option value="{{ $p->value_id }}">
                                                                            {{ __($p->value) }}</option>
                                                                    @endforeach

                                                                </select>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <label
                                                            class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase retro"
                                                            for="grid-state">
                                                            @lang('messages.state_condition')
                                                        </label>
                                                        <div class="relative">
                                                            <div class="nes-select">

                                                                <select wire:model="search_status"
                                                                    class="retro">
                                                                    <option value="" selected>@lang('messages.alls')
                                                                    </option>

                                                                    @foreach ($condition as $p)
                                                                        <option value="{{ $p->value_id }}">
                                                                            {{ __($p->value) }}</option>
                                                                    @endforeach

                                                                </select>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <label
                                                            class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase retro"
                                                            for="grid-state">
                                                            @lang('messages.extra_condition')
                                                        </label>
                                                        <div class="relative">
                                                            <div class="nes-select">

                                                                <select wire:model="search_extra"
                                                                    class="retro">
                                                                    <option value="" selected>@lang('messages.alls')
                                                                    </option>

                                                                    @foreach ($condition as $p)
                                                                        <option value="{{ $p->value_id }}">
                                                                            {{ __($p->value) }}</option>
                                                                    @endforeach

                                                                </select>
                                                            </div>

                                                        </div>
                                                    </div>





                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div
                                class="flex items-center justify-between px-4 py-3 bg-white border-t border-gray-200 sm:px-6">

                                <div class="nes-select">
                                    <select wire:model="perPage" class="text-gray-500 retro">
                                        <option value="5">5 @lang('messages.per_page')</option>
                                        <option value="10">10 @lang('messages.per_page')</option>
                                        <option value="15">15 @lang('messages.per_page')</option>
                                        <option value="25">25 @lang('messages.per_page')</option>
                                        <option value="50">50 @lang('messages.per_page')</option>
                                        <option value="10">100 @lang('messages.per_page')</option>
                                    </select>
                                </div>
                                <div class="block mt-1 ml-6 rounded-md shadow-sm form-input">
                                    @if ($search !== '')
                                        <button wire:click="clear"
                                            class="block mt-1 ml-6 rounded-md shadow-sm form-input">X</button>
                                    @endif
                                </div>
                                <div class="float-right">
                                    <a href="/site/guia-de-compra" target="_blank" class="nes-btn is-warning"><span
                                            class="text-right retro font-weight-bold color-primary small text-nowrap">@lang('messages.rgm_purchase')</span></a>
                                </div>
                            </div>
                            <div class="wishlist-table table-responsive">
                                <table class="w-full table-auto min-w-max">
                                    <thead>
                                        <tr class="text-sm leading-normal text-gray-100 uppercase bg-red-700">
                                            <th class="px-6 py-3 text-center">@lang('messages.seller_inventory')</th>
                                            <th class="px-6 py-3 text-center">@lang('messages.box')</th>
                                            <th class="px-6 py-3 text-center">@lang('messages.state_inventory')</th>
                                            <th class="px-6 py-3 text-center">Extra</th>
                                            <th class="px-6 py-3 text-center"><i class="fa fa-image"></i> </th>
                                            <th class="px-6 py-3 text-center"><i class="fa fa-comment"></i> </th>
                                            <th class="px-6 py-3 text-center">@lang('messages.price')</th>
                                            <th class="px-6 py-3 text-center">Stock</th>
                                            <th class="px-6 py-3 text-center">@lang('messages.available') <br><small>- @lang('messages.selects') -</small>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-sm font-light text-gray-600">
                                        @foreach ($seller as $s)
                                            <tr class="border-b border-gray-500 hover:bg-gray-200">
                                                <td data-filter="country" data-value="{{ $s->user->country->name }}"
                                                    class="flex items-center product-name">
                                                    <div class="text-left">
                                                        <span
                                                            class="inline-block px-2 py-1 mr-3 text-xs font-bold text-gray-600 bg-gray-100 rounded-full"
                                                            data-toggle="popover"
                                                            data-content="{{ App\AppOrgOrder::where('seller_user_id', $s->user->id)->whereIn('status', ['DD'])->count() }} Ventas | {{ $s->quantity }} Articulos disponibles"
                                                            data-placement="top"
                                                            data-trigger="hover">{{ App\AppOrgOrder::where('seller_user_id', $s->user->id)->whereIn('status', ['DD'])->count() }}</span>

                                                    </div>
                                                    &nbsp;&nbsp;
                                                    @include('partials.pais_products')

                                                    <?php foreach ($s->user->roles as $prueba) {
                                                        $level = $prueba->rol->name;
                                                    } ?>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    @if (App\SysUserRoles::where('user_id', $s->user->id)->whereIn('role_id', [2])->count() > 0)
                                                        <span class="text-base font-medium font-bold profesional"><a
                                                                href="{{ route('user-info', $s->user_name) }}"
                                                                target="_blank">{{ $s->user_name }}</a></span>
                                                    @else
                                                        <span class="font-mono text-base font-medium font-black"><a
                                                                href="{{ route('user-info', $s->user_name) }}"
                                                                target="_blank"
                                                                style="color: #000000">{{ $s->user_name }}</a></span>
                                                    @endif
                                                    @if (App\SysUserRoles::where('user_id', $s->user->id)->whereIn('role_id', [2])->count() > 0)
                                                        <img src="{{ asset('img/roles/ProfesionalSeller.png') }}"
                                                            width="25" data-toggle="popover"
                                                            data-content="@lang('messages.profesional_sell')" data-placement="top"
                                                            data-trigger="hover">
                                                    @endif
                                                </td>
                                                <td data-filter="box_condition" data-value="{{ $s->box_condition }}"
                                                    class="product-name">
                                                    <center>
                                                        <img width="15px" src="/{{ $s->box }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($s->box_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>
                                                </td>
                                                <td data-filter="game_condition"
                                                    data-value="{{ $s->game_condition }}">
                                                    <center>
                                                        <img width="15px" src="/{{ $s->game }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($s->game_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>
                                                </td>
                                                <td data-filter="extra_condition"
                                                    data-value="{{ $s->extra_condition }}">
                                                    <center>
                                                        <img width="15px" src="/{{ $s->extra }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($s->extra_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>
                                                </td>
                                                @if ($s->images->first())
                                                    <td class="text-center product-cart-img item">


                                                        <a href="{{ url('/uploads/inventory-images/' . $s->images->first()->image_path) }}"
                                                            class="fancybox" data-fancybox="{{ $s->id }}">
                                                            <img src="{{ url('/uploads/inventory-images/' . $s->images->first()->image_path) }}"
                                                                width="50px" height="50px" />
                                                        </a>


                                                        @foreach ($s->images as $p)

                                                            @if (!$loop->first)
                                                                <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                    data-fancybox="{{ $s->id }}">
                                                                    <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                        width="0px" height="0px"
                                                                        style="position:absolute;" />
                                                                </a>
                                                            @endif

                                                        @endforeach


                                                    </td>
                                                @else
                                                    <td class="product-price">
                                                        <font color="black">
                                                            <i class="fa fa-times" data-toggle="popover"
                                                                data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                                data-trigger="hover"></i>
                                                        </font>
                                                    </td>
                                                @endif
                                                <td class="product-stock-status">
                                                    @if ($s->comments == '')
                                                        <center>
                                                            <font color="black">
                                                                <i class="fa fa-times" data-toggle="popover"
                                                                    data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                                    data-trigger="hover"></i>
                                                            </font>
                                                        </center>
                                                    @else
                                                        <center>
                                                            <font color="black">
                                                                <i class="fa fa-comment" data-toggle="popover"
                                                                    data-content="{{ $s->comments }}"
                                                                    data-placement="top" data-trigger="hover"></i>
                                                            </font>
                                                        </center>

                                                    @endif
                                                </td>
                                                <td>
                                                    <font color="green"> <span
                                                            class="text-right retro font-weight-bold color-primary small text-nowrap">
                                                            {{ number_format($s->price, 2) }}
                                                            € </span> </font>
                                                </td>
                                                <td class="product-price">
                                                    <font color="black">
                                                        <span class="retro"><i>{{ $s->quantity }}</span>
                                                    </font>
                                                </td>
                                                <td class="flex items-center product-name md:justify-between">
                                                    @if ($s->user_id != Auth::id())
                                                        <div class="text-left">
                                                            <select class="text-gray-900 br-btn-product-qty">
                                                                @for ($i = 1; $i <= $s->quantity; $i++)
                                                                    <option value="{{ $i }}">
                                                                        {{ $i }}</option>
                                                                @endfor
                                                            </select>

                                                        </div>
                                                    @endif
                                                    @if (Auth::user())
                                                        @if ($s->user_id != Auth::id())
                                                            <button
                                                                style="color:rgb(3, 3, 3);  background:lighten(#292d48,65);"
                                                                class="br-btn-product-add-merchandising"
                                                                data-inventory-id="{{ $s->id + 1000 }}"
                                                                data-inventory-qty="{{ $s->quantity }}"> <a
                                                                    class="wishlist-btn" href="#"> <i
                                                                        class="fa fa-cart-plus"></i></a> </button>
                                                        @else


                                                            <div class="text-left">
                                                                <select
                                                                    class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none br-btn-product-qty focus:outline-none focus:bg-white focus:border-gray-500">
                                                                    @for ($i = 1; $i <= $s->quantity; $i++)
                                                                        <option value="{{ $i }}">
                                                                            {{ $i }}</option>
                                                                    @endfor
                                                                </select>

                                                            </div>
                                                            <button
                                                                style="color:rgb(3, 3, 3);  background:lighten(#292d48,65);"
                                                                data-href="{{ url('/account/inventory/update/' . ($s->id + 1000)) }}"
                                                                data-pro="{{ $s->id + 1000 }}"
                                                                class="wishlist-btn nk-btn-modify-inventory">
                                                                <font color="white"><i class="fa fa-pencil"></i>
                                                                </font>
                                                            </button>
                                                            <a wire:click="deleteId({{ $s->id }})"
                                                                class="wishlist-btn nes-btn" data-toggle="modal"
                                                                data-target="#merchadisingModal">
                                                                <font color="white"><i class="fa fa-trash"></i>
                                                                </font>
                                                            </a>

                                                        @endif
                                                    @else
                                                    <a href="#" class="nk-btn nk-btn-xs nk-btn-color-white showLogin"
                                                        data-toggle="popover"
                                                        data-content="@lang('messages.you_have_sing')"
                                                        data-placement="top" data-trigger="hover"><i
                                                            class="fa fa-cart-plus"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <!-- Modal -->
                                <div wire:ignore.self class="modal fade" id="merchadisingModal" tabindex="-1"
                                    role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <center>
                                                    <h5 class="modal-title retro" id="exampleModalLabel">@lang('messages.confirm_remove')
                                                    </h5>
                                                </center>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true close-btn">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <br>
                                                <h5 class="retro">
                                                    <center>@lang('messages.are_you_sure_product')</center>
                                                </h5>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="nes-btn is-warning retro close-btn"
                                                    data-dismiss="modal">@lang('messages.closes')</button>
                                                <button type="button" wire:click.prevent="delete()"
                                                    class="nes-btn retro is-error close-modal" data-dismiss="modal">@lang('messages.yes_closes')</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="flex flex-col items-center">
                                    @if ($seller->count())
                                        <div
                                            class="flex items-center justify-between px-4 py-3 border-t border-gray-200 bg-red sm:px-6">
                                            {{ $seller->links('pagination') }}
                                        </div>
                                    @else
                                        <div
                                            class="flex items-center justify-between px-4 py-3 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                                            @lang('messages.not_found_producto') "{{ $search }}" @lang('messages.per_page_two')
                                            {{ $page }}
                                            @lang('messages.see_page') {{ $perPage }}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener('livewire:load', function() {
        var $inventoryAdd = $('.br-btn-product-add-merchandising');
        $(function() {
                $('[data-toggle="popover"]').popover()
            }),
            $('.nk-btn-modify-inventory').click(function() {
                var url = $(this).data('href');
                var tr = $(this).closest('tr');
                var sel = $(this).closest('tr').find('select').prop('value');
                console.log(sel);
                //console.log(url);
                location.href = `${url}/${sel}`;
            }),
            $inventoryAdd.click(function() {
                var inventoryId = $(this).data('inventory-id');
                var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty').val();
                var stock = $(this).closest('tr').find('div.qty-new');
                var tr = $(this).closest('tr');
                var select = $(this).closest('tr').find('select.br-btn-product-qty');

                $.showBigOverlay({
                    message: '{{ __('Agregando producto a tu carro de compras') }}',
                    onLoad: function() {
                        $.ajax({
                            url: '{{ url('/cart/add_item') }}',
                            data: {
                                _token: $('meta[name="csrf-token"]').attr(
                                    'content'),
                                inventory_id: inventoryId,
                                inventory_qty: inventoryQty,
                            },
                            dataType: 'JSON',
                            type: 'POST',
                            success: function(data) {
                                if (data.error == 0) {
                                    $('#br-cart-items').text(data.items);
                                    stock.html(data.Qty);
                                    if (data.Qty == 0) {
                                        tr.remove();
                                    } else {
                                        select.html('');
                                        for (var i = 1; i < data.Qty +
                                            1; i++) {
                                            select.append('<option value=' +
                                                i + '>' + i +
                                                '</option>');
                                        }
                                    }
                                } else {

                                }
                                $.showBigOverlay('hide');
                            },
                            error: function(data) {
                                $.showBigOverlay('hide');
                            }
                        })
                    }
                });
            });
    })
</script>
