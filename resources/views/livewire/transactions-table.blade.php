<div>
    <div class="mt-20 wishlist-table-area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="my-6 bg-white rounded shadow-md">
                        <div class="mt-20 col-12">
                            <h4 class="text-gray-900 retro"> {{ __('Filtrar por') . ':' }} </h4>
                        </div>
                        <div class="col-lg-12">
                            <div
                                class="flex items-center justify-between px-4 py-3 bg-white border-t border-gray-200 sm:px-6">

                                <div class="nes-select">
                                    <select wire:model="perPage" class="text-gray-500 retro">
                                        <option value="5">5 @lang('messages.per_page')</option>
                                        <option value="10">10 @lang('messages.per_page')</option>
                                        <option value="15">15 @lang('messages.per_page')</option>
                                        <option value="25">25 @lang('messages.per_page')</option>
                                        <option value="50">50 @lang('messages.per_page')</option>
                                        <option value="10">100 @lang('messages.per_page')</option>
                                    </select>
                                </div>


                            </div>
                        </div>
                        <div class="wishlist-table table-responsive">
                            <table class="w-full table-auto min-w-max">
                                <thead>
                                    <tr class="text-sm leading-normal text-gray-100 uppercase bg-red-700">
                                        <th class="px-6 py-3 text-center">{{ __('Pedido') }} #</th>
                                        <th class="px-6 py-3 text-center">{{ __('Fecha') }}</th>
                                        <th class="px-6 py-3 text-center">{{ __('Valor') }}</th>
                                        <th class="px-6 py-3 text-center">{{ __('Decripción') }}</th>
                                    </tr>
                                </thead>
                                <tbody class="text-sm font-bold text-gray-700">
                                    @isset($trans)

                                        @foreach ($trans as $key)
                                            @php($stat = true)
                                            @if ($key->paid_out == 'N' || $key->paid_out == 'P')
                                                @php($stat = false)
                                            @else
                                                @php($stat = true)
                                            @endif
                                            @if ($stat)

                                                <tr class="border-b border-gray-500 hover:bg-gray-200">
                                                    <td>
                                                        @if (!in_array($key->status, ['EC', 'CE', 'AC', 'SC', 'PV', 'AS', 'YC', 'YV']))
                                                            <a target="_blank"
                                                                href="/account/{{ $key->tipoSeller ? 'sales' : ($key->status == 'FD' ? 'sales' : 'purchases') }}/view/{{ in_array($key->status, ['FD', 'CS', 'RE', 'RF']) ? $key->instructions : $key->order_identification }}">{{ $key->order_identification }}</a>
                                                        @else
                                                            {{ $key->order_identification }}
                                                        @endif

                                                    </td>
                                                    <td class="px-6 py-3 text-center">
                                                        <span class="font-extrabold text-base retro">
                                                            {{ $key->updated_at->format('d/m/Y H:i') }}
                                                        </span>
                                                    </td>
                                                    <td class="px-6 py-3 text-center">
                                                        <span class="font-extrabold text-base retro">
                                                            @if ($key->stats == 'Compra de Productos')
                                                                <div class="" style="color: #99121d">
                                                                    -
                                                                    {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                    {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                </div>
                                                            @elseif($key->stats == 'Venta de Productos')
                                                                <div class="" style="color: #276749">
                                                                    +
                                                                    {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                    {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                </div>
                                                            @elseif($key->stats == 'Saldo retirado por RetroGamingMarket')
                                                                <div class="" style="color: #99121d">
                                                                    {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                    {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                </div>
                                                            @elseif($key->stats == 'Saldo retirado')
                                                                <div class="" style="color: #99121d">
                                                                    {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                    {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                </div>
                                                            @elseif($key->stats == 'Comisión RetroGamingMarket')
                                                                <div class="" style="color: #99121d">
                                                                    {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                    {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                </div>
                                                            @elseif($key->stats == 'Reembolsos - ventas')
                                                                <div class="" style="color: #99121d">
                                                                    {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                    {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                </div>
                                                            @else
                                                                <div class="" style="color: #276749">
                                                                    +
                                                                    {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                                    {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                                </div>
                                                            @endif
                                                        </span>
                                                    </td>
                                                    <td class="px-6 py-3 font-bold text-base retro text-center">
                                                        {{ __($key->stats) }}
                                                        @if ($key->status == 'YV')
                                                            - <b>{{ $key->seller->user_name }}</b>
                                                        @elseif($key->status == 'YC')
                                                            - <b>{{ $key->buyer->user_name }}</b>
                                                        @endif
                                                    </td>
                                                </tr>


                                            @endif
                                        @endforeach

                                    @endisset
                                </tbody>
                            </table>
                            <div class="flex flex-col items-center bg-white">
                                @if ($trans->count())
                                    <div
                                        class="flex items-center justify-between px-4 py-3 border-t border-gray-200 bg-red sm:px-6">
                                        {{ $trans->links('pagination') }}
                                    </div>
                                @else
                                    <div
                                        class="flex items-center justify-between px-4 py-3 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                                        No hay resultados para la busqueda "{{ $search }}" en la página
                                        {{ $page }}
                                        al mostrar {{ $perPage }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            $(function() {
                $('[data-toggle="popover"]').popover()
            })

        });
    </script>
</div>
