<div>
    <div class="mt-20 wishlist-table-area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="my-6 bg-white rounded shadow-md">
                        <div class="mt-20 col-12">
                            <h4 class="text-gray-900 retro"> {{ __('Filtrar por') . ':' }} </h4>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input wire:model="search"
                                    class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                    type="text" placeholder="Buscar por Nombre...">


                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-12">
                                <select wire:model="search_platform" id="filter-extra-conditio"
                                    class="retro form-control br-filtering" data-filtering="search_platform"
                                    data-width="100%" title="">
                                    <option value="">{{ __('Plataforma') }}</option>
                                    @foreach ($platform as $p)
                                        <option value="{{ $p->value }}">{{ $p->value }}</option>
                                    @endforeach
                                </select>
                                <div class="input-group">
                                    <select wire:model="search_category" class="form-control br-filtering"
                                        data-width="100%" title="">
                                        <option value="">{{ __('Categoria') }}</option>
                                        @foreach ($category as $c)
                                            <option value="{{ $c->category_text }}">{{ $c->category_text }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <select wire:model="search_language" class="form-control br-filtering"
                                        data-width="100%" title="">
                                        <option value="">{{ __('Idioma Juego') }}</option>
                                        @foreach ($language as $l)
                                            <option value="{{ $l->value_id }}">{{ $l->value_id }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <select wire:model="search_box_language" class="form-control br-filtering"
                                        data-width="100%" title="">
                                        <option value="">{{ __('Idioma Caja') }}</option>
                                        @foreach ($language as $l)
                                            <option value="{{ $l->value_id }}">{{ $l->value_id }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <select wire:model="search_region" class="form-control br-filtering"
                                        data-width="100%" title="">
                                        <option value="">{{ __('Region') }}</option>
                                        @foreach ($region as $r)
                                            <option value="{{ $r->value_id }}">{{ $r->value_id }}
                                            </option>
                                        @endforeach
                                    </select>

                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-11">
                                <div
                                    class="flex items-center justify-between px-4 py-3 bg-white border-t border-gray-200 sm:px-6">

                                    <div class="nes-select">
                                        <select wire:model="perPage" class="text-gray-500 retro">
                                            <option value="5">5 Por pagina</option>
                                            <option value="10">10 Por pagina</option>
                                            <option value="15">15 Por pagina</option>
                                            <option value="25">25 Por pagina</option>
                                            <option value="50">50 Por pagina</option>
                                            <option value="100">100 Por pagina</option>
                                        </select>
                                    </div>


                                </div>
                            </div>
                            @if ($search !== '')
                                <div class="col-md-1">
                                    <button wire:click="clear" class="nes-btn is-error"><span
                                            class="text-right retro font-weight-bold color-primary small text-nowrap"><i
                                                class="nes-icon close is-small"></i> </span></button>
                                </div>
                            @endif
                        </div>
                        <div class="wishlist-table table-responsive">
                            <table class="w-full table-auto min-w-max">
                                <thead>
                                    <tr class="text-sm leading-normal text-gray-100 uppercase bg-red-700">
                                        <th class="px-6 py-3 text-center">#</th>
                                        <th class="px-6 py-3 text-center">Nombre</th>
                                        <th class="px-6 py-3 text-center">Categoria</th>
                                        <th class="px-6 py-3 text-center">Idioma Juego</th>
                                        <th class="px-6 py-3 text-center">Idioma Caja</th>
                                        <th class="px-6 py-3 text-center">Plataforma</th>
                                        <th class="px-6 py-3 text-center">Region</th>
                                        <th class="px-6 py-3 text-center">Precio Solo</th>
                                        <th class="px-6 py-3 text-center">Precio Usado</th>
                                        <th class="px-6 py-3 text-center">Precio Nuevo</th>
                                        <th class="px-6 py-3 text-center">Accion</th>
                                    </tr>
                                </thead>
                                <tbody class="text-sm font-light text-gray-600">
                                    @foreach ($product as $p)
                                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                                            <td class="px-6 py-3 text-center">
                                                <div class="flex items-center">
                                                    <div class="flex-shrink-0 w-10 h-10">
                                                        <a href="{{ route('product-show', $p->id) }}"
                                                            target="_blank">
                                                            @if ($p->images->first())
                                                                <img class="w-10 h-10 rounded-full"
                                                                    src="{{ count($p->images) > 0? url('/images/' . $p->images->first()->image_path): url('assets/images/art-not-found.jpg') }}">
                                                            @else
                                                                <img class="w-10 h-10 rounded-full"
                                                                    src="{{ url('assets/images/art-not-found.jpg') }}">
                                                            @endif
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="px-6 py-3 text-center">
                                                <span class="font-mono text-xs retro">

                                                    {{ $p->name }}

                                                </span>
                                            </td>
                                            <td class="px-6 py-3 text-center">
                                                <div class="text-sm text-gray-900">
                                                    @if ($p->categoryProd == '')
                                                        <b>No info</b>
                                                    @else
                                                        {{ $p->category->name }}
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="px-6 py-3 text-center">
                                                <div class="text-sm text-gray-900">
                                                    @if ($p->language == '')
                                                        <b>No info</b>
                                                    @elseif($p->language == 'ESP')
                                                        Castellano
                                                    @elseif($p->language == 'ESP')
                                                        Castellano
                                                    @elseif($p->language == 'ESP')
                                                        Castellano
                                                    @elseif($p->language == 'ESP')
                                                        Castellano
                                                    @else
                                                        {{ $p->language }}
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="px-6 py-3 text-center">
                                                <div class="text-sm text-gray-900">
                                                    @if ($p->box_language == '')
                                                        <b>No info</b>
                                                    @else
                                                        {{ $p->box_language }}
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="px-6 py-3 text-center">
                                                <div class="text-sm text-gray-900">{{ $p->platform }}</div>
                                            </td>
                                            <td class="px-6 py-3 text-center">
                                                <div class="text-sm text-gray-900">{{ $p->region }}</div>
                                            </td>
                                            <td class="px-6 py-3 text-center">
                                                <div class="text-sm text-gray-900">
                                                    @if ($p->price_solo == '')
                                                        <b>No info</b>
                                                    @else
                                                    {{ number_format($p->price_solo, 2) }} €
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="px-6 py-3 text-center">
                                                <div class="text-sm text-gray-900">
                                                    @if ($p->price_used == '')
                                                        <b>No info</b>
                                                    @else
                                                    {{ number_format($p->price_used, 2) }} € 
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="px-6 py-3 text-center">
                                                <div class="text-sm text-gray-900">
                                                    @if ($p->price_new == '')
                                                        <b>No info</b>
                                                    @else
                                                    {{ number_format($p->price_new, 2) }} € 
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="px-6 py-3 text-center">
                                                <div class="flex">
                                                    <a href="{{ route('edition_image_product', [$p->id]) }}"
                                                        class="btn btn-success btn-xs">
                                                        <font color="white">
                                                            <i class="fa-solid fa-images"></i>
                                                        </font>
                                                    </a> &nbsp;&nbsp;
                                                    <a wire:click="editId({{ $p->id }})" data-toggle="modal"
                                                        data-target="#exampleModal" class="btn btn-warning btn-xs">
                                                        <font color="white">
                                                            <i class="fa-solid fa-hand-holding-dollar"></i>
                                                        </font>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="flex flex-col items-center">
                                @if ($product->count())
                                    <div
                                        class="flex items-center justify-between px-4 py-3 border-t border-gray-200 sm:px-6">
                                        {{ $product->links('pagination') }}
                                    </div>
                                @else
                                    <div
                                        class="flex items-center justify-between px-4 py-3 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                                        No hay resultados para la busqueda "{{ $search }}" en la página
                                        {{ $page }}
                                        al mostrar {{ $perPage }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-header">
                    <center>
                        <h5 class="modal-title retro" id="exampleModalLabel">
                            Editar Precios
                        </h5>
                    </center>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group col-sm-12">

                        <label for="exampleFormControlInput1">Producto</label>
                        <input type="text" class="form-control" wire:model="name" id="exampleFormControlInput1"
                            placeholder="Enter Name">
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="mb-3">
                                <label for="" class="mb-2 form-label">Precio <font color="#A93226">Solo</font></label>
                                <input wire:model="price_solo" id="price_solo" placeholder="Precio en solo"
                                    class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="mb-3">
                                <label for="" class="mb-2 form-label">Precio <font color="#B7950B">Usado</font></label>
                                <input wire:model="price_used" id="price_used" type="number" placeholder="Precio Usado"
                                    class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="mb-3">
                                <label for="" class="mb-2 form-label">Precio <font color="#1E8449">Nuevo</font></label>
                                <input wire:model="price_new" id="price_new" type="number" placeholder="Precio Nuevo"
                                    class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="nes-btn is-error retro close-btn"
                        data-dismiss="modal">@lang('messages.closes')</button>
                    <button type="button" wire:click="update" class="nes-btn retro is-success close-modal"
                        data-dismiss="modal">Actualizar</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            $(function() {
                $('[data-toggle="popover"]').popover()
            })
        });
    </script>
</div>
