<div>
    <div class="row vertical-gap">
        @foreach ($ari as $u)
            <div class="col-md-4">
                <div class="nk-product-cat">
                    @if ($u->images->first())


                        <a class="nk-product-image"
                            href="{{ url('/uploads/inventory-images/' . $u->images->first()->image_path) }}"
                            class="fancybox" data-fancybox="RGMT{{ $u->id }}">
                            <img src="{{ url('/uploads/inventory-images/' . $u->images->first()->image_path) }}"
                                width="50px" height="50px" />
                        </a>

                        @foreach ($u->images as $p)

                            @if (!$loop->first)
                                <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                    data-fancybox="RGMT{{ $u->id }}">
                                    <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}" width="0px"
                                        height="0px" style="position:absolute;" />
                                </a>
                            @endif

                        @endforeach

                    @else
                        <a class="nk-product-image" href="{{ asset('assets/images/art-not-found.jpg') }}"
                            class="fancybox" data-fancybox="RGMT{{ $u->id }}">
                            <img src="{{ asset('assets/images/art-not-found.jpg') }}" width="50px" height="50px" />
                        </a>
                    @endif
                    <div class="nk-product-cont">
                        <h3 class="text-xs text-gray-900 retro nk-product-title h5"><a
                                href="{{ route('product-show', $u->product->id) }}"
                                target="_blank">{{ $u->product->name }}</a></h3>
                        <div class="nk-gap-1"></div>
                        <h5 class="text-xs text-gray-900 retro nk-product-title"><a
                                href="{{ route('product-show', $u->product->id) }}"
                                target="_blank">{{ $u->product->platform }} -
                                {{ $u->product->region }}</a></h5>
                        <div class="nk-gap-1"></div>
                        <span class="nk-product-rating">
                            <span class="nk-product-rating-front" style="width: 60%;">
                                <img width="15px" src="/{{ $u->game }}" data-toggle="popover"
                                    data-content="Estado - {{ App\AppOrgUserInventory::getCondicionName($u->game_condition) }}"
                                    data-placement="top" data-trigger="hover">
                                <img width="15px" src="/{{ $u->box }}" data-toggle="popover"
                                    data-content="Caja - {{ App\AppOrgUserInventory::getCondicionName($u->box_condition) }}"
                                    data-placement="top" data-trigger="hover">
                                <img width="15px" src="/{{ $u->extra }}" data-toggle="popover"
                                    data-content="Extra - {{ App\AppOrgUserInventory::getCondicionName($u->extra_condition) }}"
                                    data-placement="top" data-trigger="hover">
                                @if ($u->cover == '')
                                @else
                                    <img width="15px" src="/{{ $u->cover }}" data-toggle="popover"
                                        data-content="Caratula/Interior - {{ App\AppOrgUserInventory::getCondicionName($u->cover_condition) }}"
                                        data-placement="top" data-trigger="hover">
                                @endif
                                @if ($u->manual == '')
                                @else
                                    <img width="15px" src="/{{ $u->manual }}" data-toggle="popover"
                                        data-content="Manual - {{ App\AppOrgUserInventory::getCondicionName($u->manual_condition) }}"
                                        data-placement="top" data-trigger="hover">
                                @endif
                                @if ($u->inside == '')
                                @else
                                    <img width="15px" src="/{{ $u->inside }}" data-toggle="popover"
                                        data-content="{{ App\AppOrgUserInventory::getCondicionName($u->inside_condition) }}"
                                        data-placement="top" data-trigger="hover">
                                @endif
                                @if ($u->comments == '')
                                @else
                                    <font color="black">
                                        <i class="fa fa-comment" data-toggle="popover"
                                            data-content="{{ $u->comments }}" data-placement="top"
                                            data-trigger="hover"></i>
                                    </font>
                                @endif
                            </span>
                        </span>
                        <div class="nk-gap-1"></div>
                        <div class="nk-product-price">
                            <span class="text-base font-extrabold text-green-900 retro">{{ number_format($u->price, 2) }} €</span>
                        </div>
                        <div class="nk-gap-1"></div>
                        <div class="mt-20 wishlist-table-area mb-50">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div>
                                            <div class="">
                                                <table class="">
                                                    <tbody>

                                                                <tr>
                                                                    <td class="flex items-center product-name md:justify-between">
                                                                        @if ($u->user_id != Auth::id())
                                                                            <center>
                                                                                <font color="black">
                                                                                    <div class="text-left">
                                                                                        <select
                                                                                            class="block w-full mt-1 text-gray-900 retro form-select br-btn-product-qty d-inline">
                                                                                            @for ($i = 1; $i <= $u->quantity; $i++)
                                                                                                <option value="{{ $i }}">
                                                                                                    {{ $i }}</option>
                                                                                            @endfor
                                                                                        </select>
                    
                                                                                    </div>
                                                                                </font>
                    
                                                                        @endif
                    
                                                                        @if (Auth::user())
                                                                            @if ($u->user_id != Auth::id())
                                                                                <button
                                                                                    style="color:rgb(3, 3, 3);  background:lighten(#292d48,65);"
                                                                                    class="br-btn-product-add"
                                                                                    data-inventory-id="{{ $u->id + 1000 }}"
                                                                                    data-inventory-qty="{{ $u->quantity }}"> <a
                                                                                        class="wishlist-btn" href="#"> <i
                                                                                            class="fa fa-cart-plus"></i> - AÑADIR AL CARRITO</a> </button>
                    
                    
                                                                            @else
                    
                    
                                                                                <center>
                    
                                                                                    <div class="text-left">
                    
                                                                                        <select
                                                                                            class="block w-full mt-1 text-gray-900 retro form-select br-btn-product-qty d-inline">
                                                                                            @for ($i = 1; $i <= $u->quantity; $i++)
                                                                                                <option value="{{ $i }}">
                                                                                                    {{ $i }}</option>
                                                                                            @endfor
                                                                                        </select>
                    
                                                                                    </div>
                    
                                                                                    <button
                                                                                        data-href="{{ url('/account/inventory/update/' . ($u->id + 1000)) }}"
                                                                                        data-pro="{{ $u->id + 1000 }}"
                                                                                        class="wishlist-btn nk-btn-modify-inventory">
                                                                                        <font color="white"><i class="fa fa-pencil"></i>
                                                                                        </font>
                                                                                    </button>
                                                                                </center>
                    
                    
                                                                            @endif
                                                                        @else
                                                                            <a href="#"
                                                                                class="wishlist-btn showLogin"><i
                                                                                    class="fa fa-cart-plus"></i> - INICIA SESION</a>
                                                                        @endif
                    
                    
                                                                    </td>
                                                                </tr>

                                                    </tbody>
                                                </table>
                    
                                                <!-- Modal -->
                                                <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <center>
                                                                    <h5 class="modal-title retro" id="exampleModalLabel">Confirmacion de
                                                                        borrar
                                                                    </h5>
                                                                </center>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true close-btn">×</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <br>
                                                                <h5 class="retro">
                                                                    <center>Estas seguro de querer eliminar su producto?</center>
                                                                </h5>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="nes-btn is-warning retro close-btn"
                                                                    data-dismiss="modal">Cerrar</button>
                                                                <button type="button" wire:click.prevent="delete()"
                                                                    class="nes-btn retro is-error close-modal" data-dismiss="modal">Si,
                                                                    Eliminar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="flex flex-col items-center border-t border-gray-400">
        @if ($ari->count())
            <div
                class="flex items-center justify-between px-4 py-3 sm:px-6">
                {{ $ari->links('pagination') }}
            </div>
        @else
        <br><br>
            <div class="row">
                <div class="col-lg-12">
                    <blockquote class="nk-blockquote">
                        <div class="nk-blockquote-icon"></div>
                        <div class="text-center nk-blockquote-content h4 retro">
                            <font color="black">
                                {{ __('No hay destacados disponibles actualmente') }}
                            </font>
                        </div>
                        <div class="nk-gap"></div>
                        <div class="nk-blockquote-author"></div>
                    </blockquote>
                </div>
            </div>
        @endif
    </div>


</div>
<script>
    document.addEventListener('livewire:load', function() {
        var $inventoryAdd = $('.br-btn-product-add');
        $(function() {
                $('[data-toggle="popover"]').popover()
            }),
            $('.nk-btn-modify-inventory').click(function() {
                var url = $(this).data('href');
                var tr = $(this).closest('tr');
                var sel = $(this).closest('tr').find('select').prop('value');
                console.log(sel);
                //console.log(url);
                location.href = `${url}/${sel}`;
            }),
            $inventoryAdd.click(function() {
                var inventoryId = $(this).data('inventory-id');
                var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty').val();
                var stock = $(this).closest('tr').find('div.qty-new');
                var tr = $(this).closest('tr');
                var select = $(this).closest('tr').find('select.br-btn-product-qty');

                $.showBigOverlay({
                    message: '{{ __('Agregando producto a tu carro de compras') }}',
                    onLoad: function() {
                        $.ajax({
                            url: '{{ url('/cart/add_item') }}',
                            data: {
                                _token: $('meta[name="csrf-token"]').attr(
                                    'content'),
                                inventory_id: inventoryId,
                                inventory_qty: inventoryQty,
                            },
                            dataType: 'JSON',
                            type: 'POST',
                            success: function(data) {
                                if (data.error == 0) {
                                    $('#br-cart-items').text(data.items);
                                    stock.html(data.Qty);
                                    if (data.Qty == 0) {
                                        tr.remove();
                                    } else {
                                        select.html('');
                                        for (var i = 1; i < data.Qty +
                                            1; i++) {
                                            select.append('<option value=' +
                                                i + '>' + i +
                                                '</option>');
                                        }
                                    }
                                } else {

                                }
                                $.showBigOverlay('hide');
                            },
                            error: function(data) {
                                $.showBigOverlay('hide');
                            }
                        })
                    }
                });
            });
    })
</script>
