<div>

    <div class="mt-20 wishlist-table-area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="my-6 bg-white rounded shadow-md">
                        <div class="wishlist-table table-responsive">
                            <table class="w-full table-auto min-w-max">
                                <thead>
                                    <tr class="text-sm leading-normal text-gray-100 uppercase bg-red-700">
                                        <th class="px-6 py-3 text-center">@lang('messages.seller_rating')</th>
                                        <th class="px-6 py-3 text-center">@lang('messages.date_rating')</th>
                                        <th class="px-6 py-3 text-center">@lang('messages.comment_rating')</th>
                                        <th class="px-6 py-3 text-center">@lang('messages.speed_rating')</th>
                                        <th class="px-6 py-3 text-center">@lang('messages.package_seller')</th>
                                        <th class="px-6 py-3 text-center">@lang('messages.product_description')</th>
                                    </tr>
                                </thead>
                                <tbody class="text-sm font-light text-gray-600">
                                    @foreach ($rating_good as $key)
                                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                                            <td class="flex items-center product-name">
                                                <div class="text-left">
                                                    <span
                                                        class="inline-block px-2 py-1 mr-3 text-xs font-bold text-gray-600 bg-gray-100 rounded-full"
                                                        data-toggle="popover"
                                                        data-content="{{ App\AppOrgOrder::where('seller_user_id', $key->buyer->id)->whereIn('status', ['DD'])->count() }} @lang('messages.sale_profile_public')"
                                                        data-placement="top"
                                                        data-trigger="hover">{{ App\AppOrgOrder::where('seller_user_id', $key->buyer->id)->whereIn('status', ['DD'])->count() }}</span>

                                                </div>
                                                &nbsp;&nbsp;
                                                @include('partials.pais_rating')

                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                @if (App\SysUserRoles::where('user_id', $key->buyer->id)->whereIn('role_id', [2])->count() > 0)
                                                    <span class="text-base font-medium font-bold profesional"><a
                                                            href="{{ route('user-info', $key->buyer->user_name) }}"
                                                            target="_blank">{{ $key->buyer->user_name }}</a></span>
                                                @else
                                                    <span class="font-mono text-base font-medium font-black"><a
                                                            href="{{ route('user-info', $key->buyer->user_name) }}"
                                                            target="_blank"
                                                            style="color: #000000">{{ $key->buyer->user_name }}</a></span>
                                                @endif
                                                @if (App\SysUserRoles::where('user_id', $key->buyer->id)->whereIn('role_id', [2])->count() > 0)
                                                    <img src="{{ asset('img/roles/ProfesionalSeller.png') }}"
                                                        width="25" data-toggle="popover"
                                                        data-content="@lang('messages.profesional_sell')" data-placement="top"
                                                        data-trigger="hover">
                                                @endif
                                                @if ($key->buyer->is_online == true)
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                    <i class="fa fa-circle text-success online-icon"
                                                        data-toggle="popover" data-content="@lang('messages.online')"
                                                        data-placement="top" data-trigger="hover">
                                                    @else
                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <i class="fa fa-circle text-danger online-icon"
                                                            data-toggle="popover" data-content="Offline"
                                                            data-placement="top" data-trigger="hover">
                                                @endif
                                            </td>
                                            <td class="px-6 py-3 text-center">
                                                <span class="font-mono text-xs retro" style="color: #070707">
                                                    {{ date('Y-m-d', strtotime($key->created_at)) }}
                                                </span>
                                            </td>
                                            <td class="px-6 py-3 text-center">
                                                <b>
                                                    <span class="font-mono text-xs font-black">
                                                        {{ $key->description }}
                                                    </span>
                                                </b>
                                            </td>
                                            <td class="px-6 py-3 text-center">

                                                <center>
                                                    @if ($key->processig == 1)
                                                        <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                                            data-toggle="popover" data-content="Bueno"
                                                            data-placement="top" data-trigger="hover">
                                                    @elseif ($key->processig == 2)
                                                        <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                            data-toggle="popover" data-content="Neutral"
                                                            data-placement="top" data-trigger="hover">
                                                    @else
                                                        <img src="{{ asset('img/no-funciona.png') }}" width="17"
                                                            height="16" data-toggle="popover" data-content="Malo"
                                                            data-placement="top" data-trigger="hover">
                                                    @endif

                                                </center>

                                            </td>
                                            <td class="px-6 py-3 text-center">
                                                <center>
                                                    @if ($key->packaging == 1)
                                                        <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                                            data-toggle="popover" data-content="Bueno"
                                                            data-placement="top" data-trigger="hover">
                                                    @elseif ($key->packaging == 2)
                                                        <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                            data-toggle="popover" data-content="Neutral"
                                                            data-placement="top" data-trigger="hover">
                                                    @else
                                                        <img src="{{ asset('img/no-funciona.png') }}" width="17"
                                                            height="16" data-toggle="popover" data-content="Malo"
                                                            data-placement="top" data-trigger="hover">
                                                    @endif

                                                </center>
                                            </td>
                                            <td class="px-6 py-3 text-center">
                                                <center>
                                                    @if ($key->desc_prod == 1)
                                                        <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                                            data-toggle="popover" data-content="Bueno"
                                                            data-placement="top" data-trigger="hover">
                                                    @elseif ($key->desc_prod == 2)
                                                        <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                            data-toggle="popover" data-content="Neutral"
                                                            data-placement="top" data-trigger="hover">
                                                    @else
                                                        <img src="{{ asset('img/no-funciona.png') }}" width="17"
                                                            height="16" data-toggle="popover" data-content="Malo"
                                                            data-placement="top" data-trigger="hover">
                                                    @endif

                                                </center>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <div class="flex flex-col items-center">
                                @if ($rating_good->count())
                                    <div
                                        class="flex items-center justify-between px-4 py-3 border-t border-gray-200 sm:px-6">
                                        {{ $rating_good->links('pagination') }}
                                    </div>
                                @else
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <blockquote class="nk-blockquote">
                                                <div class="nk-blockquote-icon"></div>
                                                <div class="text-center nk-blockquote-content h4 retro">
                                                    <font color="black">
                                                        @lang('messages.not_evaluation_payment')
                                                    </font>
                                                </div>
                                                <div class="nk-gap"></div>
                                                <div class="nk-blockquote-author"></div>
                                            </blockquote>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
