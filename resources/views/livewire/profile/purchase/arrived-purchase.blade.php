<div>
    <div class="mt-20 wishlist-table-area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="my-6 bg-white rounded shadow-md">
                        <div class="mt-20 col-12">
                            <h4 class="text-gray-900 font-bold"> {{ __('Filtrar por') . ':' }} </h4>
                        </div>
                        <div class="row">
                            <div class="col-md-11">
                                <input wire:model="search"
                                    class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                    type="text" placeholder="@lang('messages.purcha_search')">


                            </div>
                            @if ($search !== '')

                                <div class="col-md-1">
                                    <button wire:click="clear" class="nes-btn is-error"><span
                                            class="text-right retro font-weight-bold color-primary small text-nowrap"><i
                                                class="nes-icon close is-small"></i> </span></button>
                                </div>
                            @endif
                        </div>
                        <div class="col-lg-12">
                            <div
                                class="flex items-center justify-between px-4 py-3 bg-white border-t border-gray-200 sm:px-6">

                                <div class="nes-select">
                                    <select wire:model="perPage" class="text-gray-500 retro">
                                        <option value="5">5 @lang('messages.per_page')</option>
                                        <option value="10">10 @lang('messages.per_page')</option>
                                        <option value="15">15 @lang('messages.per_page')</option>
                                        <option value="25">25 @lang('messages.per_page')</option>
                                        <option value="50">50 @lang('messages.per_page')</option>
                                        <option value="10">100 @lang('messages.per_page')</option>
                                    </select>
                                </div>


                            </div>
                        </div>
                        <div class="wishlist-table table-responsive">
                            <table class="w-full table-auto min-w-max">
                                <thead>
                                    <tr class="text-sm leading-normal text-gray-100 uppercase bg-red-700">
                                        <th class="px-6 py-3 text-center">{{ __('Transacción') }} #</th>
                                        <th class="px-6 py-3 text-center">{{ __('Vendedor') }}</th>
                                        <th class="px-6 py-3 text-center">{{ __('Status') }}</th>
                                        <th class="px-6 py-3 text-center">{{ __('Fecha de compra') }}</th>
                                        <th class="px-6 py-3 text-center">{{ __('Fecha de pago') }}</th>
                                        <th class="px-6 py-3 text-center">{{ __('Fecha de envío') }} </th>
                                        <th class="px-6 py-3 text-center">{{ __('Fecha de entrega') }} </th>
                                        <th class="px-6 py-3 text-center">{{ __('Calificación') }} </th>
                                        <th class="px-6 py-3 text-center">{{ __('Detalles') }} </th>
                                    </tr>
                                </thead>
                                <tbody class="text-sm font-light text-gray-600">
                                    @foreach ($arrived as $key)
                                        <tr class="border-b border-gray-500 hover:bg-gray-200">
                                            <td>
                                                <span
                                                    class="inline-block px-2 py-1 mr-3 font-bold text-gray-600 text-md">
                                                    {{ $key->order_identification }}
                                                </span>
                                            </td>
                                            <td>
                                                <span
                                                    class="inline-block px-2 py-1 mr-3 text-base font-bold text-gray-600 retro">
                                                    {{ $key->seller ? $key->seller->user_name : 'Ninguno' }}
                                                </span>
                                            </td>
                                            <td>
                                                @if ($key->status == 'DD')
                                                    <span
                                                        class="inline-block px-2 py-1 mr-3 text-base font-bold text-gray-900">
                                                        @lang('messages.send_sale_order_s') - {{ $key->status }}
                                                    </span>
                                                @else
                                                    <span
                                                        class="inline-block px-2 py-1 mr-3 text-base font-bold text-gray-900">
                                                        {{ $key->status }}
                                                    </span>
                                                @endif
                                            </td>
                                            <td>
                                                <span
                                                    class="inline-block px-2 py-1 mr-3 text-base font-bold text-gray-600 retro">
                                                    {{ date('d-m-Y', strtotime($key->created_on)) }}
                                                </span>
                                            </td>
                                            <td>
                                                <span
                                                    class="inline-block px-2 py-1 mr-3 text-base font-bold text-gray-600 retro">
                                                    @if ($key->payment)
                                                        {{ date('d-m-Y', strtotime($key->payment->created_at)) }}
                                                    @else
                                                        @lang('messages.not_paid') {{ __('') }}
                                                    @endif
                                                </span>
                                            </td>
                                            <td>
                                                <span
                                                    class="inline-block px-2 py-1 mr-3 text-base font-bold text-gray-600 retro">
                                                    {{ date('d-m-Y', strtotime($key->sent_on)) }}
                                                </span>
                                            </td>
                                            <td>
                                                <span
                                                    class="inline-block px-2 py-1 mr-3 text-base font-bold text-gray-600 retro">
                                                    {{ date('d-m-Y', strtotime($key->delivered_on)) }}
                                                </span>
                                            </td>
                                            <td class="text-center">
                                                @if ($key->rating)
                                                    @if ($key->rating->processig > 0)
                                                        <i class="nes-icon is-small star" data-toggle="popover"
                                                            data-content="{{ $key->rating->description }}"
                                                            data-placement="top" data-trigger="hover"></i>
                                                    @else
                                                        <i class="nes-icon is-small star is-empty" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    @endif
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ url('/account/purchases/view/' . $key->order_identification) }}"
                                                    target="_blank"
                                                    class="nk-btn nk-btn-xs nk-btn-rounded nes-btn is-error"> <i
                                                        class="fa fa-search"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="flex flex-col items-center">
                                @if ($arrived->count())
                                    <div
                                        class="flex items-center justify-between px-4 py-3 border-t border-gray-200 bg-red sm:px-6">
                                        {{ $arrived->links('pagination') }}
                                    </div>
                                @else
                                    <div
                                        class="flex items-center justify-between px-4 py-3 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                                        @lang('messages.not_found_producto') "{{ $search }}"
                                        @lang('messages.per_page_two')
                                        {{ $page }}
                                        @lang('messages.see_page') {{ $perPage }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded", () => {
        $(function() {
            $('[data-toggle="popover"]').popover()
        })
    });
</script>
