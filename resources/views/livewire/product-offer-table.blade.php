<div>
    <div class="flex items-center justify-center font-sans">

        <div class="w-full lg:w-5/6">

            <div class="bg-white rounded shadow-md my-9">
                <div class="flex items-center justify-between px-4 py-3 bg-white border-t border-gray-200 sm:px-6">
                    <input wire:model="search" class="block w-full mt-1 rounded-md shadow-sm form-input" type="text"
                        placeholder="Buscar...">
                    <div class="block mt-1 ml-6 rounded-md shadow-sm form-input">
                        <select wire:model="perPage" class="text-sm text-gray-500 outline-none">
                            <option value="5">5 Por página</option>
                            <option value="10">10 Por página</option>
                            <option value="15">15 Por página</option>
                            <option value="25">25 Por página</option>
                            <option value="50">50 Por página</option>
                            <option value="10">100 Por página</option>
                        </select>
                    </div>
                    <div class="block mt-1 ml-6 rounded-md shadow-sm form-input">
                        @if ($search !== '')
                            <button wire:click="clear" class="block mt-1 ml-6 rounded-md shadow-sm form-input">X</button>
                        @endif
                    </div>
                </div> 
                <table class="min-w-full divide-y divide-gray-200">
                    <thead>
                        <tr class="text-sm leading-normal text-gray-100 uppercase bg-red-700">
                            <th class="px-6 py-3 text-center">Vendedor</th>
                            <th class="px-6 py-3 text-center">Caja</th>
                            <th class="px-6 py-3 text-center">Caratula</th>
                            <th class="px-6 py-3 text-center">Manual</th>
                            <th class="px-6 py-3 text-center">Juego</th>
                            <th class="px-6 py-3 text-center">Extra</th>
                            <th class="px-6 py-3 text-center"><i class="fa fa-image"></i></th>
                            <th class="px-6 py-3 text-center"><i class="fa fa-commenting-o"></i></th>
                            <th class="px-6 py-3 text-center">Precio</th>
                            <th class="px-6 py-3 text-center">Stock</th>
                            <th class="px-6 py-3 text-center">Disponible</th>
                        </tr>
                    </thead>
                    <tbody class="text-sm font-light text-gray-500">
                        @foreach ($inventory as $i)
                            <tr class="bg-gray-800 border-b border-gray-100 hover:bg-gray-700">
                                <td class="px-6 py-3 text-left whitespace-nowrap">
                                    <div class="flex items-center md:justify-between">
                                         
                                        &nbsp;
                                        <span class="font-mono text-base retro" style="color: #ffffff">{{ $i->user_name }}</span>
                                        
                            
                                    </div>
                                    
                                </td>
                                <td class="px-6 py-3 text-left">
                                    <div class="flex items-center">
                                        <span class="font-mono text-base retro"
                                            style="color: #FA7F7F">{{ $i }}</span></b>
                                    </div>
                                </td>
                                <td class="px-6 py-3 text-center">
                                    <span class="font-mono text-base retro" style="color: #ffffff">
                                      
                                    </span>
                                </td>
                                <td class="px-6 py-3 text-center">
                                    <a href="#" target="_blank">
 
     
                                    
                                          <span class="font-mono text-base retro" style="color: #ffffff">Sin Articulos</span>
                                      
                                     </a>
                                </td>
                            </tr>
                            @endforeach
                        

                    </tbody>

                </table>
                <div class="flex flex-col items-center">
                    @if ($inventory->count())
                        <div
                            class="flex items-center justify-between px-4 py-3 border-t border-gray-200 bg-red sm:px-6">
                            {{ $inventory->links('pagination') }}
                        </div>
                    @else
                        <div
                            class="flex items-center justify-between px-4 py-3 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                            No hay resultados para la busqueda "{{ $search }}" en la página {{ $page }}
                            al mostrar {{ $perPage }}
                    @endif
                </div>
            </div>


        </div>

    </div>
</div>
