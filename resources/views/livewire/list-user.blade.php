<div>
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <div class="page-content-wrapper py-3">
            <div class="container">

                <div class="form-group searchbox-product">
                    <input type="text" class="form-control" placeholder="@lang('messages.user_search')" wire:model="search">
                </div>
                @foreach ($user as $u)
                    <ul class="listview image-listview media">
                        <li>
                            <a href="{{ route('user-info', $u->user_name) }}" class="item">
                                @if ($u->profile_picture)
                                    <div class="imageWrapper">
                                        <img src="{{ url($u->profile_picture) }}" alt="image" class="rounded-full h-100 w-100 border border-red-500">
                                    </div>
                                @else
                                    <div class="imageWrapper">
                                        <img src="{{ asset('img/profile-picture-not-found.png') }}" alt="image"
                                        class="rounded-full h-100 w-100 border border-red-500">
                                    </div>
                                @endif

                                <div class="in">
                                    <div>
                                        {{ $u->user_name }} <span
                                            class="flag-icon flag-icon-esp"></span>
                                        <div class="text-muted">Productos en venta : 
                                            @if (App\AppOrgOrder::where('seller_user_id', $u->id)->whereIn('status', ['DD'])->count() == 0)
                                                No info
                                            @else
                                                {{ App\AppOrgOrder::where('seller_user_id', $u->id)->whereIn('status', ['DD'])->count() }}
                                            @endif
                                        </div>
                                        <div class="text-muted">Miembro desde : {{ date_format($u->created_at, 'd/m/Y') }}</div>
                                        <div class="text-muted">Productos Vendidos : 
                                            <?php
                                            $duration = [];
                                            
                                            foreach ($u->inventory as $item) {
                                                $cantidad = $item->quantity;
                                                $duration[] = $cantidad;
                                            }
                                            
                                            $total = array_sum($duration);
                                            ?>
                                            @if ($total == 0)
                                                 No info 
                                            @else
                                                <span class="font-mono text-base retro"
                                                    style="color: #d82a2a">{{ $total }}</span>
                                            @endif
                                        </div>
                                       
                                    </div>
                                    <span class="text-muted">Ver</span>
                                </div>
                            </a>
                        </li>
                    </ul>
                @endforeach

            </div>

        </div>
    @else
        <div class="mt-20 wishlist-table-area mb-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="my-6 bg-white rounded shadow-md">
                            <div class="mt-20 col-12">
                                <h4 class="text-gray-900 retro"> {{ __('Filtrar por') . ':' }} </h4>
                            </div>
                            <div class="row">
                                <div class="col-md-11">
                                    <input wire:model="search"
                                        class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text" placeholder="@lang('messages.purcha_search')">


                                </div>
                                @if ($search !== '')
                                    <div class="col-md-1">
                                        <button wire:click="clear" class="nes-btn is-error"><span
                                                class="text-right retro font-weight-bold color-primary small text-nowrap"><i
                                                    class="nes-icon close is-small"></i> </span></button>
                                    </div>
                                @endif
                            </div>
                            <div class="col-lg-12">
                                <div
                                    class="flex items-center justify-between px-4 py-3 bg-white border-t border-gray-200 sm:px-6">

                                    <div class="nes-select">
                                        <select wire:model="perPage" class="text-gray-500 retro">
                                            <option value="5">5 @lang('messages.per_page')</option>
                                            <option value="10">10 @lang('messages.per_page')</option>
                                            <option value="15">15 @lang('messages.per_page')</option>
                                            <option value="25">25 @lang('messages.per_page')</option>
                                            <option value="50">50 @lang('messages.per_page')</option>
                                            <option value="10">100 @lang('messages.per_page')</option>
                                        </select>
                                    </div>


                                </div>
                            </div>
                            <div class="wishlist-table table-responsive">
                                <table class="w-full table-auto min-w-max">
                                    <thead>
                                        <tr class="text-sm leading-normal text-gray-100 uppercase bg-red-700">
                                            <th class="px-6 py-3 text-center">@lang('messages.user')</th>
                                            <th class="px-6 py-3 text-center">@lang('messages.member_date')</th>
                                            <th class="px-6 py-3 text-center">@lang('messages.sale_user')</th>
                                            <th class="px-6 py-3 text-center">@lang('messages.articles_inventory')</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-sm font-light text-gray-600">
                                        @foreach ($user as $u)
                                            <tr class="border-b border-gray-500 hover:bg-gray-200">
                                                <td class="px-6 py-3 text-left whitespace-nowrap">

                                                    <div class="flex items-center justify-between md:space-x-8">
                                                        @include('partials.pais_user')
                                                        &nbsp;
                                                        <?php foreach ($u->roles as $prueba) {
                                                            $prueba = $prueba->rol->name;
                                                        } ?>
                                                        <b><span class="font-mono text-lg font-medium"><a
                                                                    href="{{ route('user-info', $u->user_name) }}"
                                                                    target="_blank"
                                                                    style="color: #db0e0e">{{ ucfirst($u->user_name) }}</a></span></b>
                                                        @if ($prueba == 'Professional Seller')
                                                            <img src="{{ asset('img/roles/ProfesionalSeller.png') }}"
                                                                width="25" data-toggle="popover"
                                                                data-content="Vendedor Profesional"
                                                                data-placement="top" data-trigger="hover">
                                                        @else
                                                        @endif
                                                        &nbsp;
                                                        <img class="float-right" width="15px"
                                                            src="{{ asset($u->is_enabled == 'Y' ? '/img/new.png' : '/img/no-funciona.png') }}"
                                                            data-toggle="popover"
                                                            data-content="{{ $u->is_enabled == 'Y' ? 'Activo' : 'Inactivo' }}"
                                                            data-placement="top" data-trigger="hover" width="15px">

                                                    </div>


                                                </td>
                                                <td class="px-6 py-3 text-center">
                                                    <span class="font-mono text-base retro" style="color: #d44e4e">

                                                        {{ date_format($u->created_at, 'Y/m/d') }}

                                                    </span>
                                                </td>
                                                <td class="px-6 py-3 text-center">
                                                    <span class="font-mono text-base retro" style="color: #070707">
                                                        @if (App\AppOrgOrder::where('seller_user_id', $u->id)->whereIn('status', ['DD'])->count() == 0)
                                                            No info
                                                        @else
                                                            {{ App\AppOrgOrder::where('seller_user_id', $u->id)->whereIn('status', ['DD'])->count() }}
                                                        @endif
                                                    </span>
                                                </td>
                                                <td class="px-6 py-3 text-center">
                                                    <a href="{{ route('user-inventory', $u->user_name) }}"
                                                        target="_blank">
                                                        <?php
                                                        $duration = [];
                                                        
                                                        foreach ($u->inventory as $item) {
                                                            $cantidad = $item->quantity;
                                                            $duration[] = $cantidad;
                                                        }
                                                        
                                                        $total = array_sum($duration);
                                                        ?>
                                                        @if ($total == 0)
                                                            <span class="font-mono text-base retro"
                                                                style="color: #050505">@lang('messages.not_articles')</span>
                                                        @else
                                                            <span class="font-mono text-base retro"
                                                                style="color: #d82a2a">{{ $total }}</span>
                                                        @endif
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="flex flex-col items-center">
                                    @if ($user->count())
                                        <div
                                            class="flex items-center justify-between px-4 py-3 border-t border-gray-200 bg-red sm:px-6">
                                            {{ $user->links('pagination') }}
                                        </div>
                                    @else
                                        <div
                                            class="flex items-center justify-between px-4 py-3 text-gray-500 bg-white border-t border-gray-200 sm:px-6">
                                            @lang('messages.not_found_producto') "{{ $search }}" @lang('messages.per_page_two')
                                            {{ $page }}
                                            @lang('messages.see_page') {{ $perPage }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            document.addEventListener("DOMContentLoaded", () => {
                $(function() {
                    $('[data-toggle="popover"]').popover()
                })
            });
        </script>
    @endif



</div>
