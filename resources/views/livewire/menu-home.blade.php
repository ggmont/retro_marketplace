 <div @if (request()->is('account/messages')) class="item active"  @else class="item" @endif>
     <a href="/account/messages">
         <div class="col">
             <div wire:ignore>
                 <ion-icon name="chatbubbles-outline"></ion-icon>
             </div>
             <strong>Chat</strong>
             @isset($messages)
                 @if (count($messages) > 0)
                     <div @if (count($messages) > 0) @else @endif>
                         <span class="badge badge-danger">{{ count($messages) }}</span>
                     </div>
                 @endif
             @endisset
         </div>
     </a>
 </div>
 <div @if (request()->is('upload_select')) class="item active"  @else class="item" @endif>
     <a href="{{ route('upload_select') }}" class="item">
         <div class="col">
             <ion-icon name="add-circle-outline"></ion-icon>
             <strong>Vender</strong>
         </div>
     </a>
 </div>
 <div @if (request()->is('PriceList')) class="item active"  @else class="item" @endif>
     <a href="{{ route('price_list_index') }}" class="item">
         <div class="col">
             <ion-icon name="cash-outline"></ion-icon>
             <strong>@lang('messages.the_price')</strong>
         </div>
     </a>
 </div>
 <div @if (request()->is('ConfigUser') ||
         request()->is('ConfigUser/PublicProfile') ||
         request()->is('ConfigUser/PersonalData') ||
         request()->is('ConfigUser/ReferralsData') ||
         request()->is('ConfigUser/Balance') ||
         request()->is('accout/transactions')) class="item active"  @else class="item" @endif>
     <a href="{{ route('config_user') }}" class="item">
         <div class="col">
             <ion-icon name="person-outline"></ion-icon>
             <strong>Configuracion</strong>
         </div>
     </a>
 </div>
