<div>
    <div class="form-group searchbox-product">
        <input type="text" class="form-control" placeholder="@lang('messages.search_product')" wire:model.debounce.300ms="query">
    </div>
    <div class="form-group boxed">
        <div class="input-wrapper">
            <label class="form-label" for="city5">@lang('messages.category_two')</label>
            <div wire:ignore>
                <select wire:model="search_category" class="form-control category2 form-select" id="city5">
                    <option value="" selected>@lang('messages.alls')
                    </option>
                    <optgroup label="@lang('messages.category')">
                        <option value="Juegos">@lang('messages.games')</option>
                        <option value="Consolas">@lang('messages.consoles')</option>
                        <option value="Periféricos">@lang('messages.peripherals')</option>
                        <option value="Accesorios">@lang('messages.accesories')</option>
                        <option value="Merchandising">@lang('messages.merch')</option>
                    </optgroup>
                    <optgroup label="@lang('messages.sub_category')">
                        <option value="Mandos">@lang('messages.controls')</option>
                        <option value="Micrófonos">@lang('messages.microphones')</option>
                        <option value="Teclados">@lang('messages.keyboard')</option>
                        <option value="Fundas">@lang('messages.funded')</option>
                        <option value="Cables">@lang('messages.cable')</option>
                        <option value="Cargadores">@lang('messages.chargers')</option>
                        <option value="Merchandising -> Logos 3d">@lang('messages.3d_logo')</option>
                    </optgroup>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group boxed">
        <div class="input-wrapper">
            <label class="form-label" for="city5">@lang('messages.platform')</label>
            <div wire:ignore>
                <select wire:model="search_platform" class="form-control select2 form-select">
                    <option value="" selected> @lang('messages.alls')
                    </option>
                    @foreach ($platform as $p)
                        <option value="{{ $p->value }}">{{ $p->value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="form-group boxed">
        <div class="input-wrapper">
            <label class="form-label" for="city5">@lang('messages.the_region') - <svg width="20px" height="20px"
                    data-html="true" data-toggle="popover"
                    data-content="
               - Si la caja está en diferentes idiomas - PAL-EU <br />
               - Si la caja está solo en Español - PAL-ESP <br />
               - Si la caja está en Alemán - PAL-DE <br />
               - Si la caja está en Italiano - PAL-ITA <br />
               - Si la caja está en Francés - PAL-FRA <br />
               - Si el juego está en inglés y tiene etiqueta con una M en el frontal - es NTSC-U <br />
               - Si el juego tiene una etiqueta en el frontal marcada con USK es PAL-UK <br />
               - Si la portada e información del juego está en japonés es NTSC-J"
                    data-placement="top" data-trigger="hover" viewBox="0 0 76 76" xmlns="http://www.w3.org/2000/svg">
                    <path id="greyshadow" d="M8 8h68v68h-68z" fill="#BFBFBF" />
                    <path id="blackborder" d="M4 4h68v68h-68z" fill="#000" />
                    <path id="background" d="M4 4h64v64h-64z" fill="#FFC07C" />
                    <path id="borderlefttop" d="M4 0h64M0 4v64" stroke="#DE5917" stroke-width="8" />
                    <path id="rivets" d="M8 8h4v4h-4zM60 60h4v4h-4zM8 60h4v4h-4zM60 8h4v4h-4z" fill="#000" />
                    <path id="questionshadow" d="M24 20h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM36 52h8v8h-8z"
                        fill="#000" />
                    <path id="question" d="M20 16h4v-4h20v4h4v16h-8v8h-8v-8h4v-4h4v-12h-12v12h-8zM32 48h8v8h-8z"
                        fill="#DE5917" />
                </svg></label>
            <div wire:ignore>
                <select wire:model="search_region" class="form-control region2 form-select">
                    <option value="" selected>@lang('messages.alls')
                    </option>
                    @foreach ($region as $r)
                        <option value="{{ $r->value_id }}">{{ $r->value_id }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    @if (empty($search_platform) and empty($query) and empty($search_region) and empty($search_category))
        <br>



        <h4>@lang('messages.welcome_database')</h4>
        <p>
            @lang('messages.here_information')
        </p>
        <h4>@lang('messages.not_found_your_product')</h4>
        <p>
            @lang('messages.here_information_two')
        </p>
        <br>
        <div class="text-center">
            <a href="/contact-us" class="btn btn-danger w-100 font-bold">@lang('messages.forn_contact')</a>

        </div>
    @endif
    @if (!empty($query) || !empty($search_platform) || !empty($search_region) || !empty($search_category))
        <br>
        @if ($products->count() > 0)
            @foreach ($products as $product)
                <div @if ($loop->last) id="last_record" @endif class="row g-3">
                    <!-- Single Weekly Product Card-->
                    <div class="col-12 col-md-6">
                        <div class="card weekly-product-card">
                            <div class="card-body d-flex align-items-center">
                                <div class="product-thumbnail-side"><a class="product-thumbnail d-block"
                                        href="{{ route('product-show', $product['id']) }}"><img
                                            src="{{ url('/images/' . $product->imageSearchBar()) }}"
                                            alt="{{ $product['name'] }} RetroGammingMarket"></a>
                                </div>
                                <div class="product-description"><a class="product-title text-center d-block"
                                        href="{{ route('product-show', $product['id']) }}"> {{ $product['name'] }}</a>

                                        <div class="text-center">
                                            <span class="block">{{ $product['platform'] }} - {{ $product['region'] }}</span>
                                        </div>
                                    

                                    <?php
                                    $duration = [];
                                    $t = '';
                                    $total_numeros = '';
                                    $suma = '';
                                    $m = '';
                                    
                                    foreach ($product->inventory as $item) {
                                        $cantidad = $item->price;
                                        $duration[] = $cantidad;
                                        $total_numeros = count($duration);
                                        $total = max($duration);
                                        $t = min($duration);
                                        $suma = array_sum($duration);
                                        $m = $suma / $total_numeros;
                                    }
                                    
                                    ?>
                                    <div class="flex justify-between items-center pb-1">


                                        @if ($product['price_new'] == 0)
                                            <div class="total-result-of-ratings text-center pb-1">
                                                <span>@lang('messages.new_inventory')</span>
                                                <br>
                                                <span>No info</span>
                                            </div>
                                        @else
                                            <div class="total-result-of-ratings text-center pb-1">
                                                <span>@lang('messages.new_inventory')</span>
                                                <br>
                                                <span>{{ number_format($product['price_new'], 2) }}</span>
                                            </div>
                                        @endif

                                        <?php
                                        $duration = [];
                                        $t = '';
                                        $total_numeros = '';
                                        $suma = '';
                                        $m = '';
                                        
                                        foreach ($product->inventory as $item) {
                                            $cantidad = $item->price;
                                            $duration[] = $cantidad;
                                            $total_numeros = count($duration);
                                            $total = max($duration);
                                            $t = min($duration);
                                            $suma = array_sum($duration);
                                            $m = $suma / $total_numeros;
                                        }
                                        
                                        ?>

                                        @if ($product['price_used'] == 0)
                                            <div class="total-result-of-ratings-used text-center pb-1">
                                                <span>@lang('messages.used_inventory')</span>
                                                <br>
                                                <span>No info</span>
                                            </div>
                                        @else
                                            <div class="total-result-of-ratings-used text-center pb-1">
                                                <span>@lang('messages.used_inventory')</span>
                                                <br>
                                                <span>{{ number_format($product['price_used'], 2) }}</span>
                                            </div>
                                        @endif

                                        <?php
                                        $duration = [];
                                        $t = '';
                                        $total_numeros = '';
                                        $suma = '';
                                        $m = '';
                                        
                                        foreach ($product->inventory as $item) {
                                            $cantidad = $item->price;
                                            $duration[] = $cantidad;
                                            $total_numeros = count($duration);
                                            $total = max($duration);
                                            $t = min($duration);
                                            $suma = array_sum($duration);
                                            $m = $suma / $total_numeros;
                                        }
                                        
                                        ?>

                                        @if ($product['price_solo'] == 0)
                                            <div class="total-result-of-ratings-solo text-center pb-1">
                                                <span>@lang('messages.only_inventory')</span>
                                                <br>
                                                <span>No info</span>
                                            </div>
                                        @else
                                            <div class="total-result-of-ratings-solo text-center pb-1">
                                                <span>@lang('messages.only_inventory')</span>
                                                <br>
                                                <span>{{ number_format($product['price_solo'], 2) }}</span>
                                            </div>
                                        @endif


                                    </div>
                                    <div class="product-rating">
                                    </div><div class="flex justify-center">
                                        <a class="btn btn-success btn-sm add2cart-notify" href="{{ route('product-show', $product->id) }}">@lang('messages.more_information')</a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            @endforeach
            @if ($loadAmount >= $totalRecords)
                <p class="text-gray-800 font-bold text-2xl text-center my-10">@lang('messages.not_product')</p>
            @endif
        @else
            <div class="text-center">
                <h4>- @lang('messages.not_result') -</h4>
            </div>
            <div class="text-center">
                <a href="/contact-us" class="btn btn-danger w-100 font-bold">@lang('messages.forn_contact')</a>

            </div>
        @endif
    @endif
    <script>
        document.addEventListener('livewire:load', function() {
            $(function() {
                    $('[data-toggle="popover"]').popover()
                }),
                $('.select2').select2();
            $('.category2').select2();
            $('.region2').select2();
            $('.select2').on('change', function() {
                /*  alert(this.value) */
                @this.set('search_platform', this.value);
            });
            $('.category2').on('change', function() {
                /*  alert(this.value) */
                @this.set('search_category', this.value);
            });
            $('.region2').on('change', function() {
                /*  alert(this.value) */
                @this.set('search_region', this.value);
            });
        })
        document.addEventListener('DOMContentLoaded', function() {
            const lastRecord = document.getElementById('last_record');
            if (lastRecord) {
                const options = {
                    root: null,
                    threshold: 1,
                    rootMargin: '0px'
                };
                const observer = new IntersectionObserver((entries, observer) => {
                    entries.forEach(entry => {
                        if (entry.isIntersecting) {
                            @this.loadMore();
                        }
                    });
                }, options);
                observer.observe(lastRecord);
            }
        });
    </script>
</div>
