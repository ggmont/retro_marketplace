<div>


    <!-- Button trigger modal -->

    <div class="rating-review-content">
        <button type="button" class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed"
            data-toggle="modal" data-target="#exampleModal">
            <span class="fa-solid fa-filter"></span>
        </button>
        <div class="top-products-area clearfix py-3">
            <div class="container">



                <div class="shop-tab-menu">
                    <div class="row">
                        <!--List & Grid View Menu Start-->

                        <div class="col-lg-5 col-md-5 col-xl-6 col-12">
                            <div class="tab layout-options">
                                <ul class="nav">
                                    <li><a class="active" data-toggle="tab" href="#grid-view"><i
                                                class="lni lni-radio-button"></i></a></li>
                                    <li><a data-toggle="tab" href="#list-view"><i class="lni lni-grid-alt"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <!--List & Grid View Menu End-->
                        <!-- View Mode Start-->
                        <!-- View Mode End-->
                    </div>
                    <div class="tab-content">
                        <!--Grid View Start-->
                        <div class="section-heading d-flex align-items-center justify-content-between">

                        </div>
                        <div id="grid-view" class="tab-pane fade show active">
                            <br>




                            <div class="table article-table table-striped">
                                <div class="table-header d-none d-lg-flex">
                                    <div class="row no-gutters flex-nowrap">
                                        <div class="d-none col">#</div>
                                        <div class="col-sellerProductInfo col">
                                            <div class="row no-gutters h-100">
                                                <div class="col-seller col-12 col-lg-auto">Vendedor
                                                </div>
                                                <div class="col-product col-12 col-lg">Información
                                                    de producto + Oferta</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-body">
                                    @foreach ($seller as $s)
                                        <div id="articleRow1310616173" class="row no-gutters article-row">
                                            <div class="d-none col"></div>
                                            <div class="col-sellerProductInfo col">
                                                <div class="row no-gutters">
                                                    <div class="col-seller col-12 col-lg-auto"><span
                                                            class="seller-info d-flex align-items-center">
                                                            <span class="seller-name d-flex"><span
                                                                    class="icon d-flex has-content-centered mr-1"><span
                                                                        class="flag-icon flag-icon-esp"></span></span><span
                                                                    class="d-flex has-content-centered mr-1 font-extrabold"><a
                                                                        href="{{ route('user-info', $s->user_name) }}">{{ $s->user_name }}</a></span>
                                                                <span
                                                                    class="inline-block px-2 py-1 mr-3 text-xs font-bold text-gray-600 bg-gray-100 rounded-full"
                                                                    data-toggle="popover"
                                                                    data-content="{{ App\AppOrgOrder::where('seller_user_id', $s->user->id)->whereIn('status', ['DD'])->count() }} Ventas | {{ $s->quantity }} Articulos disponibles"
                                                                    data-placement="top"
                                                                    data-trigger="hover">{{ App\AppOrgOrder::where('seller_user_id', $s->user->id)->whereIn('status', ['DD'])->count() }}</span><span
                                                                    class="d-flex text-nowrap ml-lg-auto"></span></span></span>

                                                        @if ($s->user_id != Auth::id())
                                                            <select
                                                                class="d-inline br-btn-product-qty focus:outline-none focus:bg-white focus:border-gray-500">
                                                                @for ($i = 1; $i <= $s->quantity; $i++)
                                                                    {{ $i }}
                                                                    <option value="{{ $i }}">
                                                                        {{ $i }}</option>
                                                                @endfor
                                                            </select>
                                                        @endif &nbsp;&nbsp;
                                                        @if (Auth::user())
                                                            @if ($s->user_id != Auth::id())
                                                            @else
                                                                <button
                                                                    style="color:rgb(3, 3, 3);  background:lighten(#292d48,65);"
                                                                    data-href="{{ url('/account/inventory/update/' . ($s->id + 1000)) }}"
                                                                    data-pro="{{ $s->id + 1000 }}"
                                                                    class="btn-warning btn-xs nk-btn-modify-inventory">
                                                                    <i class="mr-1 lni lni-pencil-alt"></i></a>
                                                                </button>
                                                            @endif
                                                        @else
                                                            <button
                                                                style="color:rgb(3, 3, 3);  background:lighten(#292d48,65);"
                                                                class="br-btn-product-add-game"
                                                                data-inventory-id="{{ $s->id + 1000 }}"
                                                                data-inventory-qty="{{ $s->quantity }}"> <a
                                                                    class="btn-success btn-sm add2cart-notify"
                                                                    href="#">
                                                                    <i class="mr-1 ml-1 lni lni-cart"></i></a> </button>
                                                        @endif
                                                    </div>
                                                    <div class="col-product col-12 col-lg">
                                                        <div class="row no-gutters">
                                                            <div class="product-attributes col">

                                                                <img width="15px" src="/{{ $s->box }}"
                                                                    data-toggle="popover"
                                                                    data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($s->box_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px" src="/{{ $s->game }}"
                                                                    data-toggle="popover"
                                                                    data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($s->game_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                <img width="15px" src="/{{ $s->extra }}"
                                                                    data-toggle="popover"
                                                                    data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($s->extra_condition)) }}"
                                                                    data-placement="top" data-trigger="hover">
                                                                &nbsp;
                                                                @if ($s->comments == '')
                                                                    <span data-toggle="popover" data-html="true"
                                                                        data-content="@lang('messages.not_working_profile')"
                                                                        data-placement="top" data-trigger="hover"
                                                                        class="pl-1 pr-2 badge-inventories badge-info">

                                                                        <i
                                                                            class="fa-solid pl-1 fa-circle-xmark text-base"></i>

                                                                    </span>
                                                                @else
                                                                    <span data-toggle="popover" data-html="true"
                                                                        data-content="{{ $s->comments }}"
                                                                        data-placement="top" data-trigger="hover"
                                                                        class="pl-1 pr-2 badge-inventories badge-info">

                                                                        <i class="lni pl-1 lni-comments text-base"></i>

                                                                    </span>
                                                                @endif
                                                                &nbsp;
                                                                @if ($s->images->first())
                                                                    <a href="{{ url('/uploads/inventory-images/' . $s->images->first()->image_path) }}"
                                                                        class="fancybox"
                                                                        data-fancybox="{{ $s->id }}">
                                                                        <span
                                                                            class="pl-1 pr-2 badge-inventories badge-info">
                                                                            <i
                                                                                class="lni pl-1 lni-camera text-base"></i>
                                                                        </span>
                                                                    </a>
                                                                    @foreach ($s->images as $p)
                                                                        @if (!$loop->first)
                                                                            <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                                data-fancybox="{{ $s->id }}">
                                                                                <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                                    width="0px" height="0px"
                                                                                    style="position:absolute;" />
                                                                            </a>
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    <span data-toggle="popover" data-html="true"
                                                                        data-content="@lang('messages.not_working_profile')"
                                                                        data-placement="top" data-trigger="hover"
                                                                        class="pl-1 pr-2 badge-inventories badge-info">

                                                                        <i
                                                                            class="fa-solid pl-1 fa-circle-xmark text-base"></i>

                                                                    </span>
                                                                @endif
                                                                &nbsp;
                                                                <span data-toggle="popover" data-html="true"
                                                                    data-content="
                                                            <b>Estado :</b> <br />
                                                            Caja - {{ __(App\AppOrgUserInventory::getCondicionName($s->box_condition)) }} <br />
                                                            General - {{ __(App\AppOrgUserInventory::getCondicionName($s->game_condition)) }} <br />
                                                            Extra - {{ __(App\AppOrgUserInventory::getCondicionName($s->extra_condition)) }} <br />
                                                            "
                                                                    data-placement="top" data-trigger="hover"
                                                                    class="pr-2 pl-2 badge-inventories badge-info">Info</span>
                                                            </div>
                                                            <div
                                                                class="mobile-offer-container d-flex d-md-none justify-content-end col">
                                                                <span
                                                                    class="small item-count-mobile mr-3">{{ $s->quantity }}
                                                                    dispo.</span>
                                                                <div class="d-flex flex-column">
                                                                    <div
                                                                        class="d-flex align-items-center justify-content-end">
                                                                        <span
                                                                            class="font-weight-bold color-primary small text-right text-nowrap">
                                                                            {{ number_format($s->price, 2) }}
                                                                            €</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <!--Grid View End-->
                        <!--List View Start-->
                        <div id="list-view" class="tab-pane fade">
                            <div class="row g-3">
                                @foreach ($seller as $s)
                                    <!-- Single Weekly Product Card-->
                                    <div class="col-12 col-md-6">
                                        <div class="card weekly-product-card">
                                            <div class="card-body d-flex align-items-center">
                                                <div class="product-thumbnail-side"></span><a class="wishlist-btn"
                                                        href="#"></a>
                                                    @if ($s->images->first())
                                                        <a href="{{ url('/uploads/inventory-images/' . $s->images->first()->image_path) }}"
                                                            class="fancybox product-thumbnail d-block"
                                                            data-fancybox="RGM{{ $s->id }}">
                                                            <img src="{{ url('/uploads/inventory-images/' . $s->images->first()->image_path) }}"
                                                                alt="">
                                                        </a>
                                                        @foreach ($s->images as $p)
                                                            @if (!$loop->first)
                                                                <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                    data-fancybox="RGM{{ $s->id }}">
                                                                    <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                        width="0px" height="0px"
                                                                        style="position:absolute;" />
                                                                </a>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <a class="product-thumbnail d-block"><img
                                                                src="{{ asset('img/art-not-found.jpg') }}"
                                                                alt=""></a>
                                                    @endif

                                                </div>
                                                <div class="product-description">
                                                    <div class="product-title d-block"><span
                                                            class="flag-icon flag-icon-esp"></span> <a
                                                            href="{{ route('user-info', $s->user_name) }}">{{ $s->user_name }}</a>
                                                        <span
                                                            class="inline-block px-2 py-1 text-xs font-bold text-gray-600 bg-gray-100 rounded-full"
                                                            data-toggle="popover"
                                                            data-content="{{ App\AppOrgOrder::where('seller_user_id', $s->user->id)->whereIn('status', ['DD'])->count() }} Ventas | {{ $s->quantity }} Articulos disponibles"
                                                            data-placement="top"
                                                            data-trigger="hover">{{ App\AppOrgOrder::where('seller_user_id', $s->user->id)->whereIn('status', ['DD'])->count() }}</span><span
                                                            class="d-flex text-nowrap ml-lg-auto"></span>
                                                    </div>
                                                    <p class="sale-price">{{ number_format($s->price, 2) }}
                                                        €<span></span>
                                                    </p>
                                                    <div class="product-rating">
                                                        <img width="15px" src="/{{ $s->box }}"
                                                            data-toggle="popover"
                                                            data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($s->box_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                        &nbsp;
                                                        <img width="15px" src="/{{ $s->game }}"
                                                            data-toggle="popover"
                                                            data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($s->game_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                        &nbsp;
                                                        <img width="15px" src="/{{ $s->extra }}"
                                                            data-toggle="popover"
                                                            data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($s->extra_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                        &nbsp;

                                                    </div>

                                                    <div class="product-rating">

                                                        @if ($s->user_id != Auth::id())
                                                            <select
                                                                class="d-inline br-btn-product-qty focus:outline-none focus:bg-white focus:border-gray-500">
                                                                @for ($i = 1; $i <= $s->quantity; $i++)
                                                                    {{ $i }}
                                                                    <option value="{{ $i }}">
                                                                        {{ $i }}</option>
                                                                @endfor
                                                            </select>
                                                        @else
                                                        @endif
                                                        {{ $s->quantity }}
                                                        -
                                                        Disponible

                                                    </div>
                                                    @if (Auth::user())
                                                        @if ($s->user_id != Auth::id())
                                                            <button
                                                                style="color:rgb(3, 3, 3);  background:lighten(#292d48,65);"
                                                                class="br-btn-product-add-game"
                                                                data-inventory-id="{{ $s->id + 1000 }}"
                                                                data-inventory-qty="{{ $s->quantity }}"> <a
                                                                    class="wishlist-btn" href="#"> <i
                                                                        class="fa fa-cart-plus"></i></a> </button>
                                                        @else
                                                            <div class="text-left">
                                                                <select
                                                                    class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none br-btn-product-qty focus:outline-none focus:bg-white focus:border-gray-500">
                                                                    @for ($i = 1; $i <= $s->quantity; $i++)
                                                                        <option value="{{ $i }}">
                                                                            {{ $i }}</option>
                                                                    @endfor
                                                                </select>
                                                                <div
                                                                    class="absolute inset-y-0 right-0 flex items-center px-2 text-gray-700 pointer-events-none">
                                                                    <svg class="w-4 h-4 fill-current"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 20 20">
                                                                        <path
                                                                            d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                                                    </svg>
                                                                </div>
                                                            </div>
                                                            <button
                                                                style="color:rgb(3, 3, 3);  background:lighten(#292d48,65);"
                                                                data-href="{{ url('/account/inventory/update/' . ($s->id + 1000)) }}"
                                                                data-pro="{{ $s->id + 1000 }}"
                                                                class="wishlist-btn nk-btn-modify-inventory">
                                                                <font color="white"><i class="fa fa-pencil"></i>
                                                                </font>
                                                            </button>
                                                            <a wire:click="deleteId({{ $s->id }})"
                                                                data-toggle="modal" data-target="#exampleModal"
                                                                class="wishlist-btn">
                                                                <font color="white"><i class="fa fa-trash"></i>
                                                                </font>
                                                            </a>
                                                        @endif
                                                    @else
                                                        <a href="#"
                                                            class="nk-btn nk-btn-xs nk-btn-color-white showLogin"
                                                            data-toggle="popover" data-content="@lang('messages.you_have_sing')"
                                                            data-placement="top" data-trigger="hover"><i
                                                                class="fa fa-cart-plus"></i></a>
                                                    @endif


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>


                        <!--List View End-->
                    </div>
                </div>



            </div>
        </div>

    </div>

    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">
                        <center>Filtar por..</center>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="wide-block pb-1 pt-2">

                        <form>


                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label" for="city5">@lang('messages.country')</label>

                                    <select wire:model="search_country" class="form-control form-select"
                                        id="city5">
                                        <option value="" selected> @lang('messages.alls')
                                        </option>
                                        @foreach ($country as $c)
                                            <option value="{{ $c->name }}">
                                                {{ __($c->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label" for="city5">@lang('messages.box_condition')</label>
                                    <select wire:model="search_box" class="form-control form-select" id="city5">
                                        <option value="" selected>@lang('messages.alls')
                                        </option>
                                        @foreach ($condition as $p)
                                            <option value="{{ $p->value_id }}">
                                                {{ __($p->value) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label" for="city5">@lang('messages.game_condition')</label>
                                    <select wire:model="search_game" class="form-control form-select">
                                        <option value="" selected>@lang('messages.alls')
                                        </option>
                                        @foreach ($condition as $p)
                                            <option value="{{ $p->value_id }}">
                                                {{ __($p->value) }}
                                            </option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label" for="city5">@lang('messages.extra_condition')</label>
                                    <select wire:model="search_extra" class="form-control form-select">
                                        <option value="" selected>@lang('messages.alls')
                                        </option>
                                        @foreach ($condition as $p)
                                            <option value="{{ $p->value_id }}">
                                                {{ __($p->value) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

</div>
<script>
    document.addEventListener('livewire:load', function() {
        var $inventoryAdd = $('.br-btn-product-add-game');
        $(function() {
                $('[data-toggle="popover"]').popover()
            }),
            $('.nk-btn-modify-inventory').click(function() {
                var url = $(this).data('href');
                var tr = $(this).closest('tr');
                var sel = $(this).closest('tr').find('select').prop('value');
                console.log(sel);
                //console.log(url);
                location.href = `${url}/${sel}`;
            }),
            $inventoryAdd.click(function() {
                var inventoryId = $(this).data('inventory-id');
                var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty').val();
                var stock = $(this).closest('tr').find('div.qty-new');
                var tr = $(this).closest('tr');
                var select = $(this).closest('tr').find('select.br-btn-product-qty');

                $.showBigOverlay({
                    message: '{{ __('Agregando producto a tu carro de compras') }}',
                    onLoad: function() {
                        $.ajax({
                            url: '{{ url('/cart/add_item') }}',
                            data: {
                                _token: $('meta[name="csrf-token"]').attr(
                                    'content'),
                                inventory_id: inventoryId,
                                inventory_qty: inventoryQty,
                            },
                            dataType: 'JSON',
                            type: 'POST',
                            success: function(data) {
                                if (data.error == 0) {
                                    $('#br-cart-items').text(data.items);
                                    stock.html(data.Qty);
                                    if (data.Qty == 0) {
                                        tr.remove();
                                    } else {
                                        select.html('');
                                        for (var i = 1; i < data.Qty +
                                            1; i++) {
                                            select.append('<option value=' +
                                                i + '>' + i +
                                                '</option>');
                                        }
                                    }
                                } else {

                                }
                                $.showBigOverlay('hide');
                            },
                            error: function(data) {
                                $.showBigOverlay('hide');
                            }
                        })
                    }
                });
            });
    })
</script>
