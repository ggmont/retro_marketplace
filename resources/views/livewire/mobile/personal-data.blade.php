<div>
    <div class="page-content-wrapper py-3">
        <div class="container">
            <button type="button" id="actualizar" style="display:none" class="btn btn-secondary hidden me-05 mb-1"
                onclick="notification('notification-6' , 3000)">Auto Close (3s)</button>
            <button type="button" id="errorgm" style="display:none" class="btn btn-secondary hidden me-05 mb-1"
                onclick="notification('notification-7' , 3000)">Auto Close (3s)</button>
            <!-- User Information-->
            <div class="card user-info-card mb-3">

                <div class="card-body d-flex align-items-center">
                    <div class="user-profile me-3">
                        @if (auth()->user()->profile_picture)
                            <img src="{{ url(auth()->user()->profile_picture) }}" alt=""
                                class="rounded-full h-100 w-100 border border-red-500">
                            <form>
                                <input class="form-control edition"
                                    wire:click.prevent="editProfilePhoto({{ auth()->user()->id }})">
                            </form>
                        @else
                            <img src="{{ asset('img/profile-picture-not-found.png') }}" alt=""
                                class="rounded-full h-100 w-100 border border-red-500">
                            <form>
                                <input class="form-control edition"
                                    wire:click.prevent="editProfilePhoto({{ auth()->user()->id }})">
                            </form>
                        @endif
                    </div>


                    <div class="user-info">
                        <div class="d-flex align-items-center">
                            <h5 class="mb-1">{{ auth()->user()->user_name }}</h5>
                            @if (App\SysUserRoles::where('user_id', auth()->user()->id)->where('role_id', 2)->count() > 0)
                                <span class="badge bg-danger ms-2 rounded-pill">@lang('messages.profesional_sell')</span>
                            @else
                            @endif

                        </div>
                        <p class="mb-0">@lang('messages.member_date') -
                            {{ date_format(auth()->user()->created_at, 'Y') }}</p>
                        <p class="mb-0"> {{ auth()->user()->score[1] }}</p>
                    </div>
                </div>
            </div>


            <div class="listview-title mt-2">@lang('messages.personal_data')</div>
            <ul class="listview image-listview">
                <li>
                    <a wire:click.prevent="edit({{ auth()->user()->id }})" class="item">

                        <div class="in">
                            <div>
                                <header>@lang('messages.email_users')</header>
                                {{ auth()->user()->email }}
                            </div>
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a wire:click.prevent="editPass({{ auth()->user()->id }})" class="item">

                        <div class="in">
                            <div>
                                <header>@lang('messages.the_pass')</header>
                                ********
                            </div>
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a wire:click.prevent="editNam({{ auth()->user()->id }})" class="item">

                        <div class="in">
                            <div>
                                <header>@lang('messages.name_lastname')</header>
                                @if (auth()->user()->first_name)
                                    {{ auth()->user()->first_name }} {{ auth()->user()->last_name }}
                                @else
                                    -
                                @endif
                            </div>
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a wire:click.prevent="editDni({{ auth()->user()->id }})" class="item">

                        <div class="in">
                            <div>
                                <header>@lang('messages.dni')</header>
                                @if (auth()->user()->dni)
                                    {{ auth()->user()->dni }}
                                @else
                                    -
                                @endif
                            </div>
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a wire:click.prevent="editPhone({{ auth()->user()->id }})" class="item">

                        <div class="in">
                            <div>
                                <header>@lang('messages.the_phone')</header>
                                @if (auth()->user()->phone)
                                    {{ auth()->user()->phone }}
                                @else
                                    -
                                @endif
                            </div>
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a wire:click.prevent="editAddress({{ auth()->user()->id }})" class="item">

                        <div class="in">
                            <div>
                                <header>@lang('messages.the_direction')</header>
                                @if (auth()->user()->address)
                                    {{ auth()->user()->address }}
                                @else
                                    -
                                @endif
                            </div>
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a wire:click.prevent="editZipcode({{ auth()->user()->id }})" class="item">

                        <div class="in">
                            <div>
                                <header>@lang('messages.the_code')</header>
                                @if (auth()->user()->zipcode)
                                    {{ auth()->user()->zipcode }}
                                @else
                                    -
                                @endif
                            </div>
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a wire:click.prevent="editProvince({{ auth()->user()->id }})" class="item">

                        <div class="in">
                            <div>
                                <header>@lang('messages.province')</header>
                                @if (auth()->user()->province)
                                    {{ auth()->user()->province }}
                                @else
                                    -
                                @endif
                            </div>
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a wire:click.prevent="editCity({{ auth()->user()->id }})" class="item">

                        <div class="in">
                            <div>
                                <header>@lang('messages.city_company')</header>
                                @if (auth()->user()->city)
                                    {{ auth()->user()->city }}
                                @else
                                    -
                                @endif
                            </div>
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a wire:click.prevent="deleteUser({{ auth()->user()->id }})" class="item">

                        <div class="in">
                            <div>
                                <header>Eliminar cuenta</header>
                            </div>
                            <span class="text-muted">¿Estas seguro?</span>
                        </div>
                    </a>
                </li>
            </ul>
        </div>

    </div>

    <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.the_edit') @lang('messages.this_email')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="email5">@lang('messages.this_email')</label>
                                        <input type="text" wire:model="email"
                                            class="form-control @error('email') is-invalid @enderror" id="email"
                                            aria-describedby="emailHelp" placeholder="Email">
                                        @error('email')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" wire:click.prevent="update()" class="btn btn-danger font-bold w-100"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="deleteUser" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.the_edit') @lang('messages.the_phone')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>
                                ¿Estás seguro de que deseas eliminar tu cuenta? Esta acción es irreversible y todas tus
                                datos se perderán.
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="deleteUser()"
                            class="btn confirmclosed btn-danger font-bold w-50 close-modal" data-dismiss="modal">Sí,
                            estoy seguro</button>
                        <button type="button" class="btn btn-secondary font-bold w-50 close-modal"
                            data-dismiss="modal">No, espera</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="ModalPhone" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.the_edit') @lang('messages.the_phone')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="phone">@lang('messages.the_phone')</label>
                                        <input type="text" wire:model="phone"
                                            class="form-control @error('phone') is-invalid @enderror" id="phone"
                                            aria-describedby="phone" placeholder="@lang('messages.the_phone')">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updatePhone()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="ModalAddress" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.the_edit') @lang('messages.the_direction')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="address">@lang('messages.the_direction')</label>
                                        <input type="text" wire:model="address"
                                            class="form-control @error('address') is-invalid @enderror" id="address"
                                            aria-describedby="address" placeholder="@lang('messages.the_direction')">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updateAddress()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="ModalName" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.the_edit') @lang('messages.name_lastname')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="email5">@lang('messages.name')</label>
                                        <input type="text" wire:model="first_name"
                                            class="form-control @error('first_name') is-invalid @enderror"
                                            id="first_name" aria-describedby="emailHelp"
                                            placeholder="@lang('messages.name')">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="email5">@lang('messages.last_names')</label>
                                        <input type="text" wire:model="last_name"
                                            class="form-control @error('last_name') is-invalid @enderror"
                                            id="last_name" aria-describedby="emailHelp"
                                            placeholder="@lang('messages.last_names')">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updateName()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="ModalZipCode" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.the_edit') @lang('messages.the_code')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="zipcode">@lang('messages.the_code')</label>
                                        <input type="text" wire:model="zipcode"
                                            class="form-control @error('zipcode') is-invalid @enderror" id="zipcode"
                                            aria-describedby="zipcode" placeholder="@lang('messages.the_code')">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updateZipcode()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="ModalDni" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.the_edit') DNI</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="dni">DNI</label>
                                        <input type="text" wire:model="dni"
                                            class="form-control @error('dni') is-invalid @enderror" id="dni"
                                            aria-describedby="dni" minlength="9" maxlength="9"
                                            placeholder="12345678G">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updateDni()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="ModalProvince" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.the_edit') @lang('messages.province')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="province">@lang('messages.province')</label>
                                        <input type="text" wire:model="province"
                                            class="form-control @error('province') is-invalid @enderror"
                                            id="province" aria-describedby="province"
                                            placeholder="@lang('messages.province')">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updateProvince()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="ModalCity" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.the_edit') @lang('messages.city_company')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="city">@lang('messages.city_company')</label>
                                        <input type="text" wire:model="city"
                                            class="form-control @error('city') is-invalid @enderror" id="city"
                                            aria-describedby="city" placeholder="@lang('messages.city_company')">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updateCity()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="ModalDni" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.the_edit') DNI</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="dni">DNI</label>
                                        <input type="text" wire:model="dni"
                                            class="form-control @error('dni') is-invalid @enderror" id="dni"
                                            aria-describedby="dni" minlength="9" maxlength="9"
                                            placeholder="12345678G">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updateDni()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="ModalPass" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title">@lang('messages.the_edit') @lang('messages.pass_company')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>
                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="city">@lang('messages.pass_company')</label>
                                        <input type="text" wire:model="password" class="form-control"
                                            aria-describedby="password" placeholder="*****">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updatePass()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="photoprofileModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.edit_photoprofile')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        @if ($profile_picture)
                        @else
                            <div class="notification-area pt-3 pb-2">
                                <div class="list-group">
                                    <a class="list-group-item d-flex align-items-center" href="#"><span
                                            class="noti-icon"><i class="lni lni-warning"></i></span>
                                        <div class="noti-info">
                                            <h6 class="mb-0">@lang('messages.wait_momment')</h6>
                                        </div>
                                    </a>

                                </div>
                            </div>
                        @endif
                        <div wire:ignore class="custom-file-upload" id="fileUpload1">
                            <input wire:model="profile_picture" name="profile_picture" type="file"
                                id="fileuploadInput2" accept=".png, .jpg, .jpeg">
                            <label for="fileuploadInput2">
                                <span>
                                    <strong>
                                        <ion-icon name="cloud-upload-outline"></ion-icon>
                                        <i>@lang('messages.upload_touch')</i>
                                    </strong>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        @if ($profile_picture)
                            <button type="button" wire:click.prevent="SendFile()"
                                class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                                data-dismiss="modal">@lang('messages.updates')</button>
                        @else
                            <button type="button" disabled wire:click.prevent="SendFile()"
                                class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                                data-dismiss="modal">@lang('messages.updates')</button>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener('livewire:load', function() {
            $('.custom-file-upload').on('change', function() {
                //alert(this.value)
                @this.set('profile_picture', this.value);
            });
            $('.custom-file-profile').on('change', function() {
                //alert(this.value)
                @this.set('profile_picture', this.value);
            });
        })
    </script>
</div>
