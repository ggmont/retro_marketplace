<div>
    <div id="appCapsule">
        <div wire:loading.delay.class="opacity-50" class="section full">

            <div class="rating-and-review-wrapper bg-white mb-5">
                <button type="button" class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed"
                    data-toggle="modal" data-target="#exampleModal">
                    <span class="fa-solid fa-filter"></span>
                </button>

                <div class="accordion" id="accordionExample1">
                    @foreach ($user as $u)
                        <div @if ($loop->last) id="last_record" @endif>
                            <ul class="listview image-listview media accordion-item">
                                <li class="item">
                                    <h2 class="accordion-header">
                                        <button class="listview image-listview media accordion-button collapsed"
                                            type="button" data-bs-toggle="collapse"
                                            data-bs-target="#accordion{{ $u->id }}">
                                            @if ($u->profile_picture)
                                                <div class="imageWrapper">
                                                    <img src="{{ url($u->profile_picture) }}" alt="image"
                                                        class="imaged w48 h-100">
                                                </div>
                                            @else
                                                <div class="imageWrapper">
                                                    <img src="{{ asset('img/profile-picture-not-found.png') }}"
                                                        alt="image" class="imaged w48">
                                                </div>
                                            @endif
                                            {{ $u->user_name }} &nbsp; @include('partials.pais_user')
                                        </button>
                                    </h2>
                                    <div id="accordion{{ $u->id }}" class="accordion-collapse collapse"
                                        data-bs-parent="#accordionExample{{ $u->id }}">
                                        <div class="accordion-body">
                                            <div class="item-body-wrapper">
                                                <dl class="row evaluations-descriptionList mb-2">
                                                    <dt class="col-6 color-primary">Fecha</dt>
                                                    <dd class="col-6 text-right color-primary font-weight-bold">
                                                        {{ date_format($u->created_at, 'Y/m/d') }}</dd>
                                                    <dt class="col-6 color-primary">Productos en venta</dt>
                                                    <dd class="col-6 text-right color-primary font-weight-bold">
                                                        @if (App\AppOrgOrder::where('seller_user_id', $u->id)->whereIn('status', ['DD'])->count() == 0)
                                                            No info
                                                        @else
                                                            {{ App\AppOrgOrder::where('seller_user_id', $u->id)->whereIn('status', ['DD'])->count() }}
                                                        @endif
                                                    </dd>
                                                    <dt class="col-6 color-primary">Productos Vendidos</dt>
                                                    <dd class="col-6 text-right color-primary font-weight-bold">
                                                        <a class="color-primary"
                                                            href="{{ route('user-inventory', $u->user_name) }}">
                                                            <?php
                                                            $duration = [];
                                                            
                                                            foreach ($u->inventory as $item) {
                                                                $cantidad = $item->quantity;
                                                                $duration[] = $cantidad;
                                                            }
                                                            
                                                            $total = array_sum($duration);
                                                            ?>
                                                            @if ($total == 0)
                                                                @lang('messages.not_articles')
                                                            @else
                                                                {{ $total }}
                                                            @endif
                                                        </a>
                                                    </dd>
                                                    <dt class="col-6 color-primary">Evaluación de usuario</dt>
                                                    <dd class="col-6 text-right color-primary font-weight-bold">
                                                        Buen Vendedor
                                                    </dd>
                                                    <dt class="col-6 color-primary"> <a
                                                            href="{{ route('user-info', $u->user_name) }}"
                                                            class="btn btn-primary w-100 font-bold">Ver inventario</a>
                                                    </dt>
                                                    <dd class="col-6 text-right color-primary font-weight-bold">
                                                        <a href="#" data-toggle="modal"
                                                            data-target="#modalNewMessage{{ $u->id }}"
                                                            class="btn btn-success w-100 font-bold">Enviar
                                                            Mensaje</a>
                                                    </dd>
                                                </dl>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>

                            <div class="modal fade" id="modalNewMessage{{ $u->id }}" tabindex="-1"
                                role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <center>
                                                <h5 class="modal-title retro" id="exampleModalLabel">Enviar Mensaje a :
                                                    {{ $u->user_name }}
                                                </h5>
                                            </center>
                                            <button class="close-message btn btn-close p-1 ms-auto me-0" class="close"
                                                data-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" action="{{ route('newMessage') }} ">
                                                {{ csrf_field() }}
                                                <input type="hidden" autocomplete="off" class="form-control"
                                                    name="name" value="{{ $u->user_name }}" required>
                                                <div class="form-group boxed">
                                                    <div class="input-wrapper">
                                                        <textarea name="content" rows="2" placeholder="@lang('messages.write_msg')" class="form-control"></textarea>
                                                        <i class="clear-input">
                                                            <ion-icon name="close-circle"></ion-icon>
                                                        </i>
                                                    </div>
                                                </div>
                                                <br>
                                                <button type="submit"
                                                    class="btn confirmclosed btn-submit btn-primary w-100 text-lg font-extrabold">Enviar</button>
                                            </form>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>

        </div>

        <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.filter_by') :</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block pb-1 pt-2">

                                <form>


                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="name5">@lang('messages.the_user')</label>
                                            <input wire:model="search" type="text" class="form-control"
                                                placeholder="@lang('messages.the_user')" autocomplete="off">
                                            <i class="clear-input">
                                                <ion-icon name="close-circle"></ion-icon>
                                            </i>
                                        </div>
                                    </div>

                                    <div wire:ignore>
                                        <div class="form-group boxed">
                                            <div class="input-wrapper">
                                                <label class="form-label">Rol</label>

                                                <select wire:model="search_rol" style="width: 100%"
                                                    class="form-control region2 form-select">
                                                    <option value="" selected> @lang('messages.alls')
                                                    </option>
                                                    <option value="Profesional Seller">
                                                        Profesional Seller</option>
                                                    <option value="Company">
                                                        Company</option>
                                                    <option value="Customer">
                                                        Customer</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div wire:ignore>
                                        <div class="form-group boxed">
                                            <div class="input-wrapper">
                                                <label class="form-label" for="city5">@lang('messages.country')</label>

                                                <select wire:model="search_country" style="width: 100%"
                                                    class="form-control country2 form-select" id="city5">
                                                    <option value="" selected> @lang('messages.alls')
                                                    </option>
                                                    @foreach ($country as $c)
                                                        <option value="{{ $c->name }}">
                                                            {{ __($c->name) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>

            <script>
                document.addEventListener('livewire:load', function() {
                    $('.select2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.region2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.country2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.region2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_rol', this.value);
                    });
                    $('.country2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_country', this.value);
                    });
                })
                const lastRecord = document.getElementById('last_record');
                const options = {
                    root: null,
                    threshold: 1,
                    rootMargin: '0px'
                }
                const observer = new IntersectionObserver((entries, observer) => {
                    entries.forEach(entry => {
                        if (entry.isIntersecting) {
                            @this.loadMore()
                        }
                    });
                });
                observer.observe(lastRecord);
            </script>



        </div>
    </div>
</div>
