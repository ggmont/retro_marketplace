<div class="relative">

    <section class="flexbox">
        <div class="stretch">
            <input type="text" class="form-control" placeholder="@lang('messages.search_product')" wire:model="query"
                wire:keydown.escape="restart" wire:keydown.tab="restart" wire:keydown.ArrowUp="incrementHighlight"
                wire:keydown.ArrowDown="decrementHighlight" wire:keydown.enter="selectProduct">

            @if (!empty($query))
                <div class="absolute z-10 listen  bg-white rounded-t-none shadow-lg list-group">
                    <div class="list-item">
                        <font color="black">@lang('messages.searching')</font>
                    </div>
                </div>
                <div class="fixed top-0 left-0 right-0 botton-0" wire:click="reset"></div>
                <div class="absolute z-10 listen text-xs bg-white rounded-t-none shadow-lg list-group">
                    @if ($products->count() > 0)

                        @foreach ($products as $product)
                            <a href="{{ route('product-show', $product['id']) }}"
                                class="w-full-ultra list-item border list-none l list-group-item-action flex w-full-ultra items-center p-2 pl-2 border-transparent border-l-2 relative hover:border-teal-100{{ $highlightIndex === $product ? 'bg-blue-300' : '' }}">

                                <img class="w-16 h-16" src="{{ url('/images/' . $product->imageSearchBar()) }}"
                                    alt="{{ $product->imageSearchBar() ?? 'Prueba' }}">
                                &nbsp;
                                {{ $product['name'] }}
                                <br>
                                &nbsp;&nbsp;{{ $product['platform'] }} - {{ $product['region'] }}
                            </a>
                        @endforeach
                        <a href="https://www.retrogamingmarket.eu/MarketPlaceStore?search={{ $query }}#allproducts"
                            class="retro text-xs content-center ml-22 font-bold w-full list-item border list-none l list-group-item-action flex w-full items-center p-2 pl-2 border-transparent border-l-2 relative hover:border-teal-500{{ $highlightIndex === $product ? 'bg-blue-700' : '' }}">
                            <center> @lang('messages.show_all_results') </center>
                        </a>
                    @else
                        @if (Auth::user())
                            <div class="list-item content-center">
                                <font color="black"><span class="text-xs retro">@lang('messages.not_results')</span> <a
                                        href="{{ route('product_not_found') }}"
                                        class="text-xs retro nes-btn is-warning">@lang('messages.not_found_results')</a>
                                </font>
                            </div>
                        @else
                            <div class="list-item content-center">
                                <font color="black"><span class="text-xs retro">@lang('messages.not_results')</span>
                                </font>
                            </div>
                        @endif
                    @endif
                </div>
            @endif
        </div>
        <div class="normal">
            @if (Auth::user())
                <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" id="suhaNavbarToggler">
                    <span></span><span></span><span></span>
                </div>
            @else
                <a href="#" id="showLogin" class="showLogin" class="lni lni-user"
                    data-nav-toggle="#nk-nav-mobile">
                    <span class="lni lni-user  mt-3 d-flex flex-wrap">
                        <span></span><span></span><span></span>
                    </span>
                </a>
            @endif
        </div>
    </section>
</div>
