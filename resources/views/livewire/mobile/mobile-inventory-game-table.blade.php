<div>
    <div class="top-products-area clearfix">
        @if (
            $search_extra !== '' ||
                $search_cover !== '' ||
                $search_game !== '' ||
                $search_manual !== '' ||
                $search_box !== '' ||
                $search !== '' ||
                $search_country !== '' ||
                $search_category !== '' ||
                $search_region !== '')
            <button wire:click="clear" type="button"
                class="filterToggleDelete d-lg-none btn btn-lg btn-danger btn-rounded btn-absolute">
                <span class="fa-solid fa-trash"></span>
            </button>
        @endif

        <button type="button" class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed"
            data-toggle="modal" data-target="#filterModal">
            <span class="fa-solid fa-filter"></span>
        </button>

        <div class="rating-review-content">


            <div class="container">


                @if ($seller->isEmpty())
                    <p class="text-gray-800 font-bold text-2xl text-center my-10">No existe ningún
                        producto relacionado a
                        tu búsqueda</p>
                @endif

                <div class="shop-tab-menu">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table article-table table-striped">
                                <div class="table-header d-none d-lg-flex">
                                    <div class="row no-gutters flex-nowrap">
                                        <div class="d-none col">#</div>
                                        <div class="col-sellerProductInfo col">
                                            <div class="row no-gutters h-100">
                                                <div class="col-seller col-12 col-lg-auto">Vendedor
                                                </div>
                                                <div class="col-product col-12 col-lg">Información
                                                    de producto + Oferta</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-body">
                                    @foreach ($seller as $s)
                                        <div class="article-row">
                                            <div class="d-none col"></div>
                                            <div class="col-sellerProductInfo col">
                                                <div class="row no-gutters">

                                                    <table class="w-full table-auto min-w-max">
                                                        <tbody class="text-sm font-light text-gray-600">
                                                            <tr class="border-b">
                                                                <td>
                                                                    <div class="row no-gutters">
                                                                        <div class="col-seller col-12 col-lg-auto">
                                                                            <span
                                                                                class="seller-info d-flex align-items-center">
                                                                                <span class="seller-name d-flex">
                                                                                    <span
                                                                                        class="icon d-flex has-content-centered mr-1"><span
                                                                                            class="flag-icon flag-icon-esp"></span></span><span
                                                                                        class="d-flex has-content-centered mr-1 font-extrabold"><a
                                                                                            href="{{ route('user-info', $s->user_name) }}">{{ $s->user_name }}</a></span>
                                                                                    <span
                                                                                        class="inline-block px-2 text-xs font-bold text-gray-600 bg-gray-100 rounded-full"
                                                                                        data-toggle="popover"
                                                                                        data-content="{{ App\AppOrgOrder::where('seller_user_id', $s->user->id)->whereIn('status', ['DD'])->count() }} Ventas | {{ $s->quantity }} Articulos disponibles"
                                                                                        data-placement="top"
                                                                                        data-trigger="hover">{{ App\AppOrgOrder::where('seller_user_id', $s->user->id)->whereIn('status', ['DD'])->count() }}</span>
                                                                                    @if (App\SysUserRoles::where('user_id', $s->user->id)->where('role_id', 2)->count() > 0)
                                                                                        <span
                                                                                            class="badge-product bg-danger ms-2 rounded-pill">Vendedor
                                                                                            Profesional</span>
                                                                                    @else
                                                                                    @endif
                                                                                    <span
                                                                                        class="d-flex text-nowrap ml-lg-auto"></span>

                                                                                </span>
                                                                            </span>

                                                                            &nbsp;&nbsp;

                                                                            @if ($s->user_id != Auth::id())
                                                                                <select
                                                                                    class="pr-2 d-inline br-btn-product-qty focus:outline-none focus:bg-white focus:border-gray-500">
                                                                                    @for ($i = 1; $i <= $s->quantity; $i++)
                                                                                        {{ $i }}
                                                                                        <option
                                                                                            value="{{ $i }}">
                                                                                            {{ $i }}</option>
                                                                                    @endfor
                                                                                </select>
                                                                            @endif &nbsp;&nbsp;
                                                                            @if (Auth::user())
                                                                                @if ($s->user_id != Auth::id())
                                                                                    <button
                                                                                        class="br-btn-product-add-game"
                                                                                        data-user-id="{{ $s->user_id }}"
                                                                                        data-inventory-id="{{ $s->id + 1000 }}"
                                                                                        data-inventory-qty="{{ $s->quantity }}">
                                                                                        <a class="btn-success btn-sm"
                                                                                            href="#">
                                                                                            <i
                                                                                                class="mr-1 ml-1 lni lni-cart"></i></a>
                                                                                    </button>
                                                                                @else
                                                                                    <a wire:click="deleteId({{ $s->id }})"
                                                                                        data-toggle="modal"
                                                                                        data-target="#exampleModal"
                                                                                        class="btn-danger btn-xs">
                                                                                        <i
                                                                                            class="lni lni-trash-can"></i></a>
                                                                                    </a>
                                                                                    &nbsp;
                                                                                    <a href="{{ route('inventory_edit', $s->id) }}"
                                                                                        class="btn-warning btn-xs">
                                                                                        <i
                                                                                            class="lni text-white lni-pencil-alt"></i></a>
                                                                                @endif
                                                                            @else
                                                                            @endif
                                                                        </div>
                                                                        <div class="col-product col-12 col-lg">
                                                                            <div class="row no-gutters">
                                                                                <div class="product-attributes col">

                                                                                    <img width="15px"
                                                                                        src="/{{ $s->box }}"
                                                                                        data-toggle="popover"
                                                                                        data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($s->box_condition)) }}"
                                                                                        data-placement="top"
                                                                                        data-trigger="hover">
                                                                                    &nbsp;
                                                                                    <img width="15px"
                                                                                        src="/{{ $s->cover }}"
                                                                                        data-toggle="popover"
                                                                                        data-content="Caratula - {{ __(App\AppOrgUserInventory::getCondicionName($s->cover_condition)) }}"
                                                                                        data-placement="top"
                                                                                        data-trigger="hover">
                                                                                    &nbsp;
                                                                                    <img width="15px"
                                                                                        src="/{{ $s->manual }}"
                                                                                        data-toggle="popover"
                                                                                        data-content="Manual - {{ __(App\AppOrgUserInventory::getCondicionName($s->manual_condition)) }}"
                                                                                        data-placement="top"
                                                                                        data-trigger="hover">
                                                                                    &nbsp;
                                                                                    <img width="15px"
                                                                                        src="/{{ $s->game }}"
                                                                                        data-toggle="popover"
                                                                                        data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($s->game_condition)) }}"
                                                                                        data-placement="top"
                                                                                        data-trigger="hover">
                                                                                    &nbsp;
                                                                                    <img width="15px"
                                                                                        src="/{{ $s->extra }}"
                                                                                        data-toggle="popover"
                                                                                        data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($s->extra_condition)) }}"
                                                                                        data-placement="top"
                                                                                        data-trigger="hover">
                                                                                    &nbsp;
                                                                                    @if ($s->comments == '')
                                                                                        <span data-toggle="popover"
                                                                                            data-html="true"
                                                                                            data-content="@lang('messages.not_working_profile')"
                                                                                            data-placement="top"
                                                                                            data-trigger="hover"
                                                                                            class="pl-1 pr-2 badge-inventories badge-info">

                                                                                            <i
                                                                                                class="fa-solid pl-1 fa-circle-xmark text-base"></i>

                                                                                        </span>
                                                                                    @else
                                                                                        <span data-toggle="popover"
                                                                                            data-html="true"
                                                                                            data-content="{{ $s->comments }}"
                                                                                            data-placement="top"
                                                                                            data-trigger="hover"
                                                                                            class="pl-1 pr-2 badge-inventories badge-info">

                                                                                            <i
                                                                                                class="lni pl-1 lni-comments text-base"></i>

                                                                                        </span>
                                                                                    @endif
                                                                                    &nbsp;
                                                                                    @if ($s->images->first())
                                                                                        <a href="{{ url('/uploads/inventory-images/' . $s->images->first()->image_path) }}"
                                                                                            class="fancybox"
                                                                                            data-fancybox="{{ $s->id }}">
                                                                                            <span
                                                                                                class="pl-1 pr-2 badge-inventories badge-info">
                                                                                                <i
                                                                                                    class="lni pl-1 lni-camera text-base"></i>
                                                                                            </span>
                                                                                        </a>
                                                                                        @foreach ($s->images as $p)
                                                                                            @if (!$loop->first)
                                                                                                <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                                                    data-fancybox="{{ $s->id }}">
                                                                                                    <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                                                        width="0px"
                                                                                                        height="0px"
                                                                                                        style="position:absolute;" />
                                                                                                </a>
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @else
                                                                                        <span data-toggle="popover"
                                                                                            data-html="true"
                                                                                            data-content="@lang('messages.not_working_profile')"
                                                                                            data-placement="top"
                                                                                            data-trigger="hover"
                                                                                            class="pl-1 pr-2 badge-inventories badge-info">

                                                                                            <i
                                                                                                class="fa-solid pl-1 fa-circle-xmark text-base"></i>

                                                                                        </span>
                                                                                    @endif
                                                                                    &nbsp;

                                                                                    <span data-toggle="popover"
                                                                                        data-html="true"
                                                                                        data-content="
                                                                                    <b>Estado :</b> <br />
                                                                                Caja - {{ __(App\AppOrgUserInventory::getCondicionName($s->box_condition)) }} <br />
                                                                                Carátula - {{ __(App\AppOrgUserInventory::getCondicionName($s->cover_condition)) }} <br />
                                                                                Manual - {{ __(App\AppOrgUserInventory::getCondicionName($s->manual_condition)) }} <br />
                                                                                Juego - {{ __(App\AppOrgUserInventory::getCondicionName($s->game_condition)) }} <br />
                                                                                Extra - {{ __(App\AppOrgUserInventory::getCondicionName($s->extra_condition)) }} <br />
                                                                                "
                                                                                        data-placement="top"
                                                                                        data-trigger="hover"
                                                                                        class="pr-2 pl-2 badge-inventories badge-info">info</span>
                                                                                </div>
                                                                                <div
                                                                                    class="mobile-offer-container d-flex d-md-none justify-content-end col">





                                                                                    <span style="display:none"
                                                                                        data-toggle="popover"
                                                                                        data-html="true"
                                                                                        data-content="
                                                                                    - Estado de Caja : {{ __(App\AppOrgUserInventory::getCondicionName($s->box_condition)) }} <br />
                                                                                    - Estado de la Caratula: {{ __(App\AppOrgUserInventory::getCondicionName($s->cover_condition)) }} <br />
                                                                                    - Estado del Manual : {{ __(App\AppOrgUserInventory::getCondicionName($s->manual_condition)) }} <br />
                                                                                    - Estado del Juego : {{ __(App\AppOrgUserInventory::getCondicionName($s->game_condition)) }} <br />
                                                                                    - Estado Extra : {{ __(App\AppOrgUserInventory::getCondicionName($s->extra_condition)) }} <br />
                                                                                    "
                                                                                        data-placement="top"
                                                                                        data-trigger="hover"
                                                                                        class="pr-2 badge-product badge-info">Info</span>

                                                                                    <span
                                                                                        class="small item-count-mobile mr-3">{{ $s->quantity }}
                                                                                        Disp.</span>
                                                                                    <div class="d-flex flex-column">
                                                                                        <div
                                                                                            class="d-flex align-items-center justify-content-end">
                                                                                            <span
                                                                                                class="font-weight-bold color-primary small text-right text-nowrap">
                                                                                                {{ number_format($s->price, 2) }}
                                                                                                €</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                        </tbody>
                                                    </table>




                                                    <div wire:ignore.self class="modal fade" id="exampleModal"
                                                        tabindex="-1" role="dialog"
                                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content bg-white">
                                                                <div class="modal-content">
                                                                    <div class="modal-body">
                                                                        <div
                                                                            class="d-flex align-items-center justify-content-between mb-4">
                                                                            <h4 class="modal-title">@lang('messages.confirm_remove')
                                                                            </h4>
                                                                            <button
                                                                                class="btn btn-close p-1 ms-auto me-0"
                                                                                class="close" data-dismiss="modal"
                                                                                aria-label="Close"></button>
                                                                        </div>
                                                                        <div class="wide-block pb-1 pt-2">
                                                                            <span class="font-semibold text-sm">

                                                                                <center>
                                                                                    @lang('messages.are_you_sure_product')
                                                                                </center>
                                                                                <br>


                                                                                <div class="row-game">

                                                                                    <div class="col-6">


                                                                                        <button
                                                                                            class="pt-2 pb-2 btn-success close-modal btn-block"
                                                                                            type="button"
                                                                                            wire:click.prevent="delete()"
                                                                                            data-dismiss="modal">
                                                                                            <span
                                                                                                class="retro text-white font-extrabold">
                                                                                                @lang('messages.yes_closes')</span></button>

                                                                                    </div>
                                                                                    <div class="col-6">
                                                                                        <button
                                                                                            class="pt-2 pb-2 btn-danger close-modal btn-block"
                                                                                            data-dismiss="modal"
                                                                                            type="submit"><span
                                                                                                class="retro text-white font-extrabold">@lang('messages.closes')</span></button>

                                                                                    </div>
                                                                                </div>
                                                                            </span>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <script>
                document.addEventListener('livewire:load', function() {
                    var $inventoryAdd = $('.br-btn-product-add-game');
                    $(function() {
                            $('[data-toggle="popover"]').popover()
                        }),
                        $('.nk-btn-modify-inventory').click(function() {
                            var url = $(this).data('href');
                            var tr = $(this).closest('tr');
                            var sel = $(this).closest('tr').find('select').prop('value');
                            console.log(sel);
                            //console.log(url);
                            location.href = `${url}/${sel}`;
                        }),
                        $inventoryAdd.click(function() {
                            var userId = $(this).data('user-id');
                            var inventoryId = $(this).data('inventory-id');
                            var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty').val();
                            var stock = $(this).closest('tr').find('div.qty-new');
                            var tr = $(this).closest('tr');
                            var select = $(this).closest('tr').find('select.br-btn-product-qty');

                            $.showBigOverlay({
                                message: '{{ __('Agregando producto a tu carro de compras') }}',
                                onLoad: function() {
                                    $.ajax({
                                        url: '{{ url('/cart/add_item') }}',
                                        data: {
                                            _token: $('meta[name="csrf-token"]').attr(
                                                'content'),
                                            inventory_id: inventoryId,
                                            inventory_qty: inventoryQty,
                                            user_id: userId,
                                        },
                                        dataType: 'JSON',
                                        type: 'POST',
                                        success: function(data) {
                                            if (data.productCountForVendor >= 4) {
                                                Swal.fire({
                                                    icon: 'error',
                                                    title: 'Límite de productos alcanzado',
                                                    text: 'Solo puedes agregar 4 productos que pertenezcan al mismo vendedor',
                                                })
                                                $.showBigOverlay('hide');
                                            } else {
                                                if (data.error == 0) {
                                                    $('#br-cart-items').text(data.items);
                                                    stock.html(data.Qty);
                                                    if (data.Qty == 0) {
                                                        tr.remove();
                                                    } else {
                                                        select.html('');
                                                        for (var i = 1; i < data.Qty +
                                                            1; i++) {
                                                            select.append('<option value=' +
                                                                i + '>' + i +
                                                                '</option>');
                                                        }
                                                    }
                                                }
                                                $.showBigOverlay('hide');
                                                document.getElementById("cartrgm").click();
                                            }
                                        },
                                        error: function(data) {
                                            $.showBigOverlay('hide');
                                        }
                                    })
                                }
                            });



                        });
                })
            </script>
        </div>

        <div wire:ignore.self class="modal fade" id="filterModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title" id="addnewcontactlabel">Filtrar por :</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block pb-1 pt-2">

                                <form>


                                    <div wire:ignore>
                                        <div class="form-group boxed">
                                            <div class="input-wrapper">
                                                <label class="form-label" for="city5">@lang('messages.country')</label>

                                                <select wire:model="search_country" style="width: 100%"
                                                    class="form-control country2 form-select" id="city5">
                                                    <option value="" selected> @lang('messages.alls')
                                                    </option>
                                                    @foreach ($country as $c)
                                                        <option value="{{ $c->name }}">
                                                            {{ __($c->name) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">@lang('messages.box')</label>
                                            <div wire:ignore>
                                                <select wire:model="search_box" style="width: 100%"
                                                    class="form-control region2 form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    @foreach ($condition as $p)
                                                        <option value="{{ $p->value_id }}">
                                                            {{ __($p->value) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">@lang('messages.cover')</label>
                                            <div wire:ignore>
                                                <select wire:model="cover_condition" style="width: 100%"
                                                    class="form-control cover2 form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    @foreach ($condition as $p)
                                                        <option value="{{ $p->value_id }}">
                                                            {{ __($p->value) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">@lang('messages.the_manual')</label>
                                            <div wire:ignore>
                                                <select wire:model="search_manual" style="width: 100%"
                                                    class="form-control manual2 form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    @foreach ($condition as $p)
                                                        <option value="{{ $p->value_id }}">
                                                            {{ __($p->value) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Juego</label>
                                            <div wire:ignore>
                                                <select wire:model="search_game" style="width: 100%"
                                                    class="form-control game2 form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    @foreach ($condition as $p)
                                                        <option value="{{ $p->value_id }}">
                                                            {{ __($p->value) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">@lang('messages.the_extra')</label>
                                            <div wire:ignore>
                                                <select wire:model="search_extra" style="width: 100%"
                                                    class="form-control extra2 form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    @foreach ($condition as $p)
                                                        <option value="{{ $p->value_id }}">
                                                            {{ __($p->value) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>

            <script>
                document.addEventListener('livewire:load', function() {
                    $('.extra2').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.cover2').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.region2').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.country2').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.game2').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.extra2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_extra', this.value);
                    });
                    $('.cover2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_cover', this.value);
                    });
                    $('.region2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_box', this.value);
                    });
                    $('.country2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_country', this.value);
                    });
                    $('.game2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_game', this.value);
                    });
                })
                const lastRecord = document.getElementById('last_record');
                const options = {
                    root: null,
                    threshold: 1,
                    rootMargin: '0px'
                }
                const observer = new IntersectionObserver((entries, observer) => {
                    entries.forEach(entry => {
                        if (entry.isIntersecting) {
                            @this.loadMore()
                        }
                    });
                });
                observer.observe(lastRecord);
            </script>



        </div>
    </div>
</div>
