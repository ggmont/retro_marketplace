<div>
    <div class="page-content-wrapper py-3">
        <div class="container">
            <button type="button" id="actualizar" style="display:none" class="btn btn-secondary hidden me-05 mb-1"
                onclick="notification('notification-6' , 3000)">Auto Close (3s)</button>
            <button type="button" id="errorgm" style="display:none" class="btn btn-secondary hidden me-05 mb-1"
                onclick="notification('notification-7' , 3000)">Auto Close (3s)</button>
            <!-- android style -->

            <!-- User Information-->

            <div class="listview-title mt-2">@lang('messages.information_basic')</div>
            @error('cover_picture')
                <center>@lang('messages.not_image')</center>
            @enderror

            <ul class="listview image-listview media">
                <li>

                    <a wire:click.prevent="editCover({{ auth()->user()->id }})" class="item">
                        <div class="imageWrapper">
                            @if (auth()->user()->cover_picture)
                                <img src="{{ url(auth()->user()->cover_picture) }}" alt="image" class="imaged w64">
                            @else
                                <img src="{{ asset('uploads/carousels-images/carousel_image__1_6.jpg') }}"
                                    alt="image" class="imaged w64">
                            @endif
                        </div>
                        <div class="in">
                            <div>
                                @lang('messages.cover_optional')
                            </div>
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a wire:click.prevent="editTerms({{ auth()->user()->id }})" class="item">
                        <div class="in">
                            <div>
                                <header>@lang('messages.terms')</header>
                                {{ str_limit(auth()->user()->terms_and_conditions, 30) }}
                            </div>
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </div>
                    </a>
                </li>

            </ul>
            <ul class="listview link-listview">
                <li><a href="{{ route('user-info', auth()->user()->user_name) }}">@lang('messages.see_public')</a></li>
            </ul>
            <div class="listview-title mt-2">@lang('messages.social')</div>
            @if (count($social_user) == 0)
                <ul class="listview image-listview">
                    <li>
                        <a wire:click.prevent="editTwitchNew" class="item">

                            <div class="in">
                                <div>
                                    <header>Twitch</header>
                                    -
                                </div>
                                <span class="text-muted">@lang('messages.the_edit')</span>
                            </div>
                        </a>
                    </li>

                </ul>
                <ul class="listview image-listview">
                    <li>
                        <a wire:click.prevent="editYoutubeNew" class="item">

                            <div class="in">
                                <div>
                                    <header>Youtube</header>
                                    -
                                </div>
                                <span class="text-muted">@lang('messages.the_edit')</span>
                            </div>
                        </a>
                    </li>

                </ul>
                <ul class="listview image-listview">
                    <li>
                        <a wire:click.prevent="editTikTokNew" class="item">

                            <div class="in">
                                <div>
                                    <header>TikTok</header>
                                    -
                                </div>
                                <span class="text-muted">@lang('messages.the_edit')</span>
                            </div>
                        </a>
                    </li>

                </ul>
                <ul class="listview image-listview">
                    <li>
                        <a wire:click.prevent="editInstagramNew" class="item">

                            <div class="in">
                                <div>
                                    <header>Instagram</header>
                                    -
                                </div>
                                <span class="text-muted">@lang('messages.the_edit')</span>
                            </div>
                        </a>
                    </li>

                </ul>
            @else
                @foreach ($social_user as $d)
                    <ul class="listview image-listview">
                        <li>
                            <a wire:click.prevent="editTwitch" class="item">

                                <div class="in">
                                    <div>
                                        <header>Twitch</header>
                                        {{ str_limit($d->twitch, 30) }}
                                    </div>
                                    <span class="text-muted">@lang('messages.the_edit')</span>
                                </div>
                            </a>
                        </li>

                    </ul>
                    <ul class="listview image-listview">
                        <li>
                            <a wire:click.prevent="editYoutube({{ $d->id }})" class="item">

                                <div class="in">
                                    <div>
                                        <header>Youtube</header>
                                        @if($d->youtube)
                                        {{ str_limit($d->youtube, 30) }} 
                                        @else
                                        -
                                        @endif
                                    </div>
                                    <span class="text-muted">@lang('messages.the_edit')</span>
                                </div>
                            </a>
                        </li>

                    </ul>
                    <ul class="listview image-listview">
                        <li>
                            <a wire:click.prevent="editTikTok({{ $d->id }})" class="item">

                                <div class="in">
                                    <div>
                                        <header>TikTok</header>
                                        @if($d->tiktok)
                                        {{ str_limit($d->tiktok, 30) }} 
                                        @else
                                        -
                                        @endif
                                    </div>
                                    <span class="text-muted">@lang('messages.the_edit')</span>
                                </div>
                            </a>
                        </li>

                    </ul>
                    <ul class="listview image-listview">
                        <li>
                            <a wire:click.prevent="editInstagram({{ $d->id }})" class="item">

                                <div class="in">
                                    <div>
                                        <header>Instagram</header>
                                        @if($d->instagram)
                                        {{ str_limit($d->instagram, 30) }} 
                                        @else
                                        -
                                        @endif
                                    </div>
                                    <span class="text-muted">@lang('messages.the_edit')</span>
                                </div>
                            </a>
                        </li>

                    </ul>
                @endforeach
            @endif
        </div>
    </div>


    <div wire:ignore.self class="modal fade" id="NewtwitchModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">Usuario de Twitch</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="twitch">Twitch</label>
                                        <input type="text" wire:model="twitch"
                                            class="form-control @error('twitch') is-invalid @enderror" id="twitch"
                                            aria-describedby="twitch" placeholder="Nombre de usuario">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="NewTwitch()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">GUARDAR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="NewyoutubeModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">Usuario de Youtube</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="youtube">Youtube</label>
                                        <input type="text" wire:model="youtube"
                                            class="form-control @error('youtube') is-invalid @enderror" id="youtube"
                                            aria-describedby="youtube" placeholder="Nombre de usuario">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="NewYoutube()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">GUARDAR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="NewtiktokModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">Usuario de TikTok</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="tiktok">TikTok</label>
                                        <input type="text" wire:model="tiktok"
                                            class="form-control @error('tiktok') is-invalid @enderror" id="tiktok"
                                            aria-describedby="tiktok" placeholder="Usuario de TikTok">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="NewTikTok()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">GUARDAR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="NewinstagramModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">Usuario de instagram</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="instagram">Instagram</label>
                                        <input type="text" wire:model="instagram"
                                            class="form-control @error('instagram') is-invalid @enderror"
                                            id="instagram" aria-describedby="instagram"
                                            placeholder="Nombre de usuario">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="NewInstagram()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="termModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.the_edit') @lang('messages.terms')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="city5">@lang('messages.terms')</label>
                                        <textarea id="terms_and_conditions" wire:model="terms_and_conditions" rows="4" class="form-control"
                                            placeholder="Esta es mi manera de vender"></textarea>
                                        <br>
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updateTerms()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="youtubeModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">Usuario de youtube</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="youtube">Youtube</label>
                                        <input type="text" wire:model="youtube"
                                            class="form-control @error('youtube') is-invalid @enderror" id="youtube"
                                            aria-describedby="youtube" placeholder="https://www.youtube.com">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                        @error('email')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updateYoutube()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="tiktokModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">Usuario de TikTok</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="tiktok">TikTok</label>
                                        <input type="text" wire:model="tiktok"
                                            class="form-control @error('tiktok') is-invalid @enderror" id="tiktok"
                                            aria-describedby="tiktok" placeholder="Usuario de TikTok">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updateTikTok()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="instagramModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">Usuario de instagram</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="instagram">Instagram</label>
                                        <input type="text" wire:model="instagram"
                                            class="form-control @error('instagram') is-invalid @enderror"
                                            id="instagram" aria-describedby="instagram"
                                            placeholder="Nombre de usuario">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updateInstagram()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="facebookModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.facebook_link')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="discord">Facebook</label>
                                        <input type="text" wire:model="facebook"
                                            class="form-control @error('facebook') is-invalid @enderror"
                                            id="facebook" aria-describedby="facebook"
                                            placeholder="https://www.facebook.com">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updateFacebook()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="twitchModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">Usuario de Twitch</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="twitch">Twitch</label>
                                        <input type="text" wire:model="twitch"
                                            class="form-control @error('twitch') is-invalid @enderror" id="twitch"
                                            aria-describedby="twitch" placeholder="Nombre de usuario">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updateTwitch()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="profilesModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.edit_photoprofile')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        @if ($profile_picture)
                            {{ $profile_picture }}
                        @else
                            <div class="notification-area pt-3 pb-2">
                                <div class="list-group">
                                    <a class="list-group-item d-flex align-items-center"
                                        href="notification-details.html"><span class="noti-icon"><i
                                                class="lni lni-warning"></i></span>
                                        <div class="noti-info">
                                            <h6 class="mb-0">@lang('messages.wait_momment')</h6>
                                        </div>
                                    </a>

                                </div>
                            </div>
                        @endif
                        <div wire:ignore class="custom-file-profile" id="fileUpload2">
                            <input wire:model="profile_picture" name="profile_picture" type="file"
                                id="fileuploadInput" accept=".png, .jpg, .jpeg">
                            <label for="fileuploadInput">
                                <span>
                                    <strong>
                                        <ion-icon name="cloud-upload-outline"></ion-icon>
                                        <i>@lang('messages.upload_touch')</i>
                                    </strong>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        @if ($profile_picture)
                            <button type="button" wire:click.prevent="SendFile()"
                                class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                                data-dismiss="modal">@lang('messages.updates')</button>
                        @else
                            <button type="button" disabled wire:click.prevent="SendFile()"
                                class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                                data-dismiss="modal">@lang('messages.updates')</button>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>



    <div wire:ignore.self class="modal fade" id="photoModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.edit_cover')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        @if ($cover_picture)
                        @else
                            <div class="notification-area pt-3 pb-2">
                                <div class="list-group">
                                    <a class="list-group-item d-flex align-items-center"
                                        href="#"><span class="noti-icon"><i
                                                class="lni lni-warning"></i></span>
                                        <div class="noti-info">
                                            <h6 class="mb-0">@lang('messages.wait_momment')</h6>
                                        </div>
                                    </a>

                                </div>
                            </div>
                        @endif
                        <div wire:ignore class="custom-file-upload" id="fileUpload1">
                            <input wire:model="cover_picture" name="cover_picture" type="file"
                                id="fileuploadInput2" accept=".png, .jpg, .jpeg">
                            <label for="fileuploadInput2">
                                <span>
                                    <strong>
                                        <ion-icon name="cloud-upload-outline"></ion-icon>
                                        <i>@lang('messages.upload_touch')</i>
                                    </strong>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        @if ($cover_picture)
                            <button type="button" wire:click.prevent="SendFileCover()"
                                class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                                data-dismiss="modal">@lang('messages.updates')</button>
                        @else
                            <button type="button" disabled wire:click.prevent="SendFileCover()"
                                class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                                data-dismiss="modal">@lang('messages.updates')</button>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener('livewire:load', function() {
            $('.custom-file-upload').on('change', function() {
                //alert(this.value)
                @this.set('cover_picture', this.value);
            });
            $('.custom-file-profile').on('change', function() {
                //alert(this.value)
                @this.set('profile_picture', this.value);
            });
        })
    </script>
</div>
