<div>
    <div class="top-products-area clearfix py-16">


        <div class="container">

            <button type="button" id="actualizar" style="display:none" class="btn btn-secondary hidden me-05 mb-1"
                onclick="notification('notification-6' , 3000)">Auto Close (3s)</button>

            <div class="coupon-form">

                <input class="form-control" type="text" wire:model="search" placeholder="Buscar en mi colleción">

                <a class="btn btn-warning" href="{{ route('search_collection') }}">
                    <span class="text-white fa-solid fa-plus"></span>
                </a>


            </div>

        </div>


        <br>

        @if ($search_platform !== '' || $search_category !== '' || $search_region !== '' || $search_prueba !== '')
            <button wire:click="clear_all" type="button"
                class="filterToggleDelete d-lg-none btn btn-lg btn-danger btn-rounded btn-absolute">
                <span class="fa-solid fa-trash"></span>
            </button>
        @endif

        <button type="button" class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed"
            data-toggle="modal" data-target="#exampleModal">
            <span class="fa-solid fa-filter"></span>
        </button>
        <div class="container">


            <div wire:loading.delay.class="opacity-50" class="row g-3">
                <!-- Single Top Product Card-->
                @isset($seller)
                    @foreach ($seller as $i)
                        <div @if ($loop->last) id="last_record" @endif class="col-6 col-md-4 col-lg-3">
                            <div class="card top-product-card">
                                <div class="card-body">
                                    <a wire:click="deleteId({{ $i->id }})" data-toggle="modal"
                                        data-target="#DeleteModal">
                                        <span class="badge badge-danger">Borrar</span>
                                    </a>


                                    <a href="{{ route('inventory_edit', $i->id) }}"
                                        class="wishlist-btn-edit btn-edit  text-white text-xs btn-warning">
                                        <i class="fa-solid fa-pen-to-square"></i>
                                    </a>







                                    <a href="{{ route('product-inventory-show', $i->id) }}">

                                        <div class="figure">
                                            <a href="{{ route('product-inventory-show', $i->id) }}">
                                                @if ($i->images->first())
                                                    <img class="card-product-image-holder image-main"
                                                        src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                        alt="">
                                                @elseif ($i->product->images->first())
                                                    <img class=" card-product-image-holder image-main"
                                                        src="{{ url('/images/' . $i->product->images->first()->image_path) }}"
                                                        alt="">
                                                @else
                                                    <img loading="lazy" class="first-img"
                                                        src="{{ asset('assets/images/art-not-found.jpg') }}" alt="">
                                                @endif
                                            </a>
                                        </div>

                                        <span class="product-title ml-1">{{ str_limit($i->product->name, 16) }}
                                        </span>
                                        <br>
                                        <span class="progress-title">
                                            <div class="ml-1 pb-1">
                                                {{ $i->product->platform }} <br>
                                                {{ $i->product->region }}<br>
                                                Cantidad : {{ $i->quantity }}
                                            </div>
                                            @if ($i->product->categoryProd->parent_id == 1)
                                                <div class="flex ulti ml-1">
                                                    <img height="10px" width="15px" src="/{{ $i->box }}"
                                                        data-toggle="popover"
                                                        data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->cover }}" data-toggle="popover"
                                                        data-content="Caratula - {{ __(App\AppOrgUserInventory::getCondicionName($i->cover_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->manual }}" data-toggle="popover"
                                                        data-content="Manual - {{ __(App\AppOrgUserInventory::getCondicionName($i->manual_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->game }}" data-toggle="popover"
                                                        data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->extra }}" data-toggle="popover"
                                                        data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                </div>
                                                <div class="text-center pt-2">

                                                    @if ($i->comments == '')
                                                        <i class="fa-solid fa-circle-xmark" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    @else
                                                        <span data-toggle="popover" data-html="true"
                                                            data-content="
                                                         {{ $i->comments }}"
                                                            data-placement="top" data-trigger="hover"
                                                            class="badge-comment badge-info"><i
                                                                class="fa-solid fa-comment"></i></span>
                                                    @endif

                                                    <span data-toggle="popover" data-html="true"
                                                        data-content="
                                                        <b>Estado :</b> <br />
                                                        Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }} <br />
                                                        Carátula - {{ __(App\AppOrgUserInventory::getCondicionName($i->cover_condition)) }} <br />
                                                        Manual - {{ __(App\AppOrgUserInventory::getCondicionName($i->manual_condition)) }} <br />
                                                        Juego - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }} <br />
                                                        Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }} <br />
                                                    "
                                                        data-placement="top" data-trigger="hover"
                                                        class="badge-inventory badge-info">ESTADO</span>
                                                </div>
                                            @elseif ($i->product->categoryProd->parent_id == 2)
                                                <div class="flex ulti ml-1">
                                                    <img height="10px" width="15px" src="/{{ $i->box }}"
                                                        data-toggle="popover"
                                                        data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->cover }}" data-toggle="popover"
                                                        data-content="Interior - {{ __(App\AppOrgUserInventory::getCondicionName($i->cover_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->manual }}" data-toggle="popover"
                                                        data-content="Manual - {{ __(App\AppOrgUserInventory::getCondicionName($i->manual_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->game }}" data-toggle="popover"
                                                        data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->inside }}" data-toggle="popover"
                                                        data-content="Cables - {{ __(App\AppOrgUserInventory::getCondicionName($i->inside_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->extra }}" data-toggle="popover"
                                                        data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                </div>
                                                <div class="text-center pt-2">
                                                    @if ($i->comments == '')
                                                        <i class="fa-solid fa-circle-xmark" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    @else
                                                        <span data-toggle="popover" data-html="true"
                                                            data-content="
                                                           {{ $i->comments }}"
                                                            data-placement="top" data-trigger="hover"
                                                            class="badge-comment badge-info"><i
                                                                class="fa-solid fa-comment"></i></span>
                                                    @endif

                                                    <span data-toggle="popover" data-html="true"
                                                        data-content="
                                        <b>Estado :</b> <br />
                                        Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }} <br />
                                        Carátula - {{ __(App\AppOrgUserInventory::getCondicionName($i->cover_condition)) }} <br />
                                        Manual - {{ __(App\AppOrgUserInventory::getCondicionName($i->manual_condition)) }} <br />
                                        Estado - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }} <br />
                                        Cables - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }} <br />
                                        Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->inside_condition)) }} <br />
                                    "
                                                        data-placement="top" data-trigger="hover"
                                                        class="badge-inventory badge-info">ESTADO</span>
                                                </div>
                                            @else
                                                <div class="flex ulti ml-1">
                                                    <img height="10px" width="15px" src="/{{ $i->box }}"
                                                        data-toggle="popover"
                                                        data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->game }}" data-toggle="popover"
                                                        data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->extra }}" data-toggle="popover"
                                                        data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                </div>

                                                <div class="text-center pt-2">

                                                    @if ($i->comments == '')
                                                        <i class="fa-solid fa-circle-xmark" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    @else
                                                        <span data-toggle="popover" data-html="true"
                                                            data-content="
                                                     {{ $i->comments }}"
                                                            data-placement="top" data-trigger="hover"
                                                            class="badge-comment badge-info"><i
                                                                class="fa-solid fa-comment"></i></span>
                                                    @endif

                                                    <span data-toggle="popover" data-html="true"
                                                        data-content="
                                                    <b>Estado :</b> <br />
                                                    Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }} <br />
                                                    Juego - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }} <br />
                                                    Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }} <br />
                                                "
                                                        data-placement="top" data-trigger="hover"
                                                        class="badge-inventory badge-info">ESTADO</span>
                                                </div>
                                            @endif


                                            <hr style='margin-top:0.5em; margin-bottom:0.5em' />

                                            <div class="item-body-wrapper text-center">

                                                <dd class="col-12 color-primary font-weight-bold">
                                                    <a wire:click.prevent="PutOnSale({{ $i->id }})"
                                                        class="btn-ultra btn-danger"><span
                                                            class="retro font-extrabold text-white text-sm">Poner a la
                                                            venta</span></a>
                                                </dd>

                                            </div>




                                    </a>
                                </div>

                            </div>
                        </div>
                    @endforeach
                @endisset
            </div>
            @if ($loadAmount >= $totalRecords)
            @endif

        </div>

        <div wire:ignore.self class="modal fade" id="ModalPrice" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title" id="addnewcontactlabel">Poner a la venta</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block pb-1 pt-2">
                                <form>
                                    <div class="notification-area pt-3 pb-2">
                                        <!-- Notification Details-->
                                        <div class="list-group-item d-flex py-3"><span class="noti-icon"><i
                                                    class="lni lni-alarm"></i></span>
                                            <div class="noti-info">
                                                <h6>Aviso!</h6>
                                                <p>Una vez puesto el precio a la venta , el producto sera quitado de tu
                                                    colección</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="phone">Pon un precio </label>
                                            <input step="0.01" type="number" wire:model="price"
                                                class="form-control @error('price') is-invalid @enderror"
                                                id="price" aria-describedby="price" placeholder="Precio">
                                            <i class="clear-input">
                                                <ion-icon name="close-circle"></ion-icon>
                                            </i>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" wire:click.prevent="updatePutOnSale()"
                                class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                                data-dismiss="modal">@lang('messages.updates')</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div wire:ignore.self class="modal fade" id="DeleteModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title" id="addnewcontactlabel"> @lang('messages.confirm_remove')</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block pb-1 pt-2">
                                <h5>
                                    <center>@lang('messages.are_you_sure_product')</center>
                                </h5>
                            </div>

                            <br>

                            <dl class="row evaluations-descriptionList mb-0">
                                <dt class="col-6 color-primary">
                                    <button type="button" class="btn btn-warning w-100 close-btn font-bold"
                                        data-dismiss="modal">@lang('messages.closes')</button>
                                </dt>
                                <dd class="col-6 text-right color-primary font-weight-bold">
                                    <button type="button" wire:click.prevent="delete()"
                                        class="btn btn-success w-100 font-bold close-modal"
                                        data-dismiss="modal">@lang('messages.yes_closes')</button>
                                </dd>
                            </dl>
                        </div>



                    </div>
                </div>
            </div>
        </div>

        <div wire:ignore.self class="modal fade" id="favoriteModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title" id="addnewcontactlabel"> @lang('messages.favorite')</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block pb-1 pt-2">
                                <h5>
                                    <center>@lang('messages.favorite_question')</center>
                                </h5>
                            </div>
                            <br>

                            <dl class="row evaluations-descriptionList mb-0">
                                <dt class="col-6 color-primary">
                                    <button type="button" class="btn btn-warning w-100 close-btn font-bold"
                                        data-dismiss="modal">@lang('messages.closes')</button>
                                </dt>
                                <dd class="col-6 text-right color-primary font-weight-bold">
                                    <button type="button" wire:click.prevent="update()"
                                        class="btn btn-success w-100 font-bold close-modal"
                                        data-dismiss="modal">@lang('messages.yes_favorite')</button>
                                </dd>
                            </dl>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div wire:ignore.self class="modal fade" id="desfavoriteModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.quite_favorite')</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block pb-1 pt-2">
                                <h5>
                                    <center>@lang('messages.are_you_favorite')</center>
                                </h5>
                            </div>

                            <br>

                            <dl class="row evaluations-descriptionList mb-0">
                                <dt class="col-6 color-primary">
                                    <button type="button" class="btn btn-warning w-100 close-btn font-bold"
                                        data-dismiss="modal">@lang('messages.closes')</button>
                                </dt>
                                <dd class="col-6 text-right color-primary font-weight-bold">
                                    <button type="button" wire:click.prevent="update_desfavorite()"
                                        class="btn btn-success w-100 font-bold close-modal"
                                        data-dismiss="modal">@lang('messages.yes_closes_two')</button>
                                </dd>
                            </dl>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title" id="addnewcontactlabel">Filtrar por :</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block pb-1 pt-2">

                                <form>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Plataforma</label>
                                            <div wire:ignore>
                                                <select wire:model="search_platform" style="width: 100%"
                                                    class="form-control select2 form-select">
                                                    <option value="" selected> @lang('messages.alls')
                                                    </option>
                                                    @foreach ($platform as $p)
                                                        <option value="{{ $p->value }}">{{ $p->value }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Categoría</label>
                                            <div wire:ignore>
                                                <select wire:model="search_category" style="width: 100%"
                                                    class="form-control ultra2 form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    <optgroup label="@lang('messages.category')">
                                                        <option value="Juegos">@lang('messages.games')</option>
                                                        <option value="Consolas">@lang('messages.consoles')</option>
                                                        <option value="Periféricos">@lang('messages.peripherals')</option>
                                                        <option value="Accesorios">@lang('messages.accesories')</option>
                                                        <option value="Merchandising">Merchandising</option>
                                                    </optgroup>
                                                    <optgroup label="@lang('messages.sub_category')">
                                                        <option value="Mandos">@lang('messages.controls')</option>
                                                        <option value="Micrófonos">@lang('messages.microphones')</option>
                                                        <option value="Teclados">@lang('messages.keyboard')</option>
                                                        <option value="Fundas">@lang('messages.funded')</option>
                                                        <option value="Cables">@lang('messages.cable')</option>
                                                        <option value="Cargadores">@lang('messages.chargers')</option>
                                                        <option value="Merchandising -> Logos 3d">@lang('messages.3d_logo')
                                                        </option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Región</label>
                                            <div wire:ignore>
                                                <select wire:model="search_region" style="width: 100%"
                                                    class="form-control region2 form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    @foreach ($region as $r)
                                                        <option value="{{ $r->value_id }}">{{ $r->value_id }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>

            <script>
                document.addEventListener('livewire:load', function() {
                    $('.select2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.ultra2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.region2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.country2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.language2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.media2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.select2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_platform', this.value);
                    });
                    $('.ultra2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_category', this.value);
                    });
                    $('.region2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_region', this.value);
                    });
                    $('.country2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_country', this.value);
                    });
                    $('.language2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_language', this.value);
                    });
                    $('.media2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_media', this.value);
                    });
                })
                document.addEventListener('DOMContentLoaded', function() {
                    const lastRecord = document.getElementById('last_record');
                    if (lastRecord) {
                        const options = {
                            root: null,
                            threshold: 1,
                            rootMargin: '0px'
                        };
                        const observer = new IntersectionObserver((entries, observer) => {
                            entries.forEach(entry => {
                                if (entry.isIntersecting) {
                                    @this.loadMore();
                                }
                            });
                        }, options);
                        observer.observe(lastRecord);
                    }
                });
            </script>



        </div>
    </div>
</div>

<script>
    document.addEventListener('livewire:load', function() {
        var $inventoryAdd = $('.br-btn-product-add');
        $(function() {
                $('[data-toggle="popover"]').popover()
            }),
            $('.nk-btn-modify-inventory').click(function() {
                var url = $(this).data('href');
                var tr = $(this).closest('tr');
                var sel = $(this).closest('tr').find('select').prop('value');
                console.log(sel);
                //console.log(url);
                location.href = `${url}/${sel}`;
            }),
            $inventoryAdd.click(function() {
                var inventoryId = $(this).data('inventory-id');
                var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty').val();
                var stock = $(this).closest('tr').find('div.qty-new');
                var tr = $(this).closest('tr');
                var select = $(this).closest('tr').find('select.br-btn-product-qty');

                $.showBigOverlay({
                    message: '{{ __('Agregando producto a tu carro de compras') }}',
                    onLoad: function() {
                        $.ajax({
                            url: '{{ url('/cart/add_item') }}',
                            data: {
                                _token: $('meta[name="csrf-token"]').attr(
                                    'content'),
                                inventory_id: inventoryId,
                                inventory_qty: inventoryQty,
                            },
                            dataType: 'JSON',
                            type: 'POST',
                            success: function(data) {
                                if (data.error == 0) {
                                    $('#br-cart-items').text(data.items);
                                    stock.html(data.Qty);
                                    if (data.Qty == 0) {
                                        tr.remove();
                                    } else {
                                        select.html('');
                                        for (var i = 1; i < data.Qty +
                                            1; i++) {
                                            select.append('<option value=' +
                                                i + '>' + i +
                                                '</option>');
                                        }
                                    }
                                } else {

                                }
                                $.showBigOverlay('hide');
                            },
                            error: function(data) {
                                $.showBigOverlay('hide');
                            }
                        })
                    }
                });
            });
    })
</script>
