<div>
    <div class="header-area" id="headerArea">
        <div class="container h-100 mt-1 align-items-center justify-content-between">
            <!-- Logo Wrapper-->

            <div class="relative">

                <section class="flexbox">
                    <div class="stretch">
                        <input type="text" class="form-control" placeholder="@lang('messages.msg_chat')..." wire:model="search">
                    </div>
                    <div class="normal">
                        @if (Auth::user())
                            <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebarPanel">
                                <span></span><span></span><span></span>
                            </div>
                        @else
                            <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebarPanel">
                                <span></span><span></span><span></span>
                            </div>
                        @endif
                    </div>
                </section>
            </div>


        </div>

    </div>
    <div id="appCapsule">

        <ul class="ps-0 chat-user-list">
            <!-- Single Chat User -->
            @foreach ($beta as $s)
                @php
                    $isCurrentUserAliasUser = $s->prueba->user_name == auth()->user()->user_name;
                    $unreadMessages = ($isCurrentUserAliasUser && $s->seen_user_id === 'N') || (!$isCurrentUserAliasUser && $s->seen_contact_id === 'N');
                    $backgroundStyle = $unreadMessages ? 'background-color: #f3f3f3;' : '';
                @endphp

                <li class="p-3 chat-unread" style="{{ $backgroundStyle }}" wire:click="updateSeen({{ $s->id }})">
                    @if ($s->prueba->user_name == auth()->user()->user_name)
                        <a class="d-flex" href="/chatrgm/{{ $s->user->id }}">
                        @else
                            <a class="d-flex" href="/chatrgm/{{ $s->prueba->id }}">
                    @endif
                    <!-- Thumbnail -->
                    <div class="chat-user-thumbnail me-3 shadow">
                        @if ($s->prueba->user_name == auth()->user()->user_name)
                            @if ($s->user->profile_picture)
                                <div class="image-wrapper">
                                    <img src="{{ url($s->user->profile_picture) }}" class="imaged rounded">
                                </div>
                                @if ($s->user->is_online == true)
                                    <span class="active-status"></span>
                                @else
                                    <span class="inactive-status"></span>
                                @endif
                            @else
                                <img class="img-circle" src="{{ asset('img/profile-picture-not-found.png') }}"
                                    alt="">
                                @if ($s->user->is_online == true)
                                    <span class="active-status"></span>
                                @else
                                    <span class="inactive-status"></span>
                                @endif
                            @endif
                        @else
                            @if ($s->prueba->profile_picture)
                                <img class="img-circle" src="{{ url($s->prueba->profile_picture) }}" alt="">
                                @if ($s->prueba->is_online == true)
                                    <span class="active-status"></span>
                                @else
                                    <span class="inactive-status"></span>
                                @endif
                            @else
                                <img class="img-circle" src="{{ asset('img/profile-picture-not-found.png') }}"
                                    alt="">
                                @if ($s->prueba->is_online == true)
                                    <span class="active-status"></span>
                                @else
                                    <span class="inactive-status"></span>
                                @endif
                            @endif
                        @endif

                    </div>
                    <!-- Info -->
                    <div class="chat-user-info">
                        <h6 class="text-truncate mb-0">
                            @if ($s->prueba->user_name == auth()->user()->user_name)
                                {{ $s->user->user_name }}
                            @else
                                {{ $s->prueba->user_name }}
                            @endif
                        </h6>
                        <div class="last-chat">
                            <p class="mb-0 text-truncate">{!! mb_strimwidth($s->last_message, 0, 40, '...', 'UTF-8') !!}
                            </p>
                        </div>
                    </div>
                    </a>
                    <!-- Options -->
                    @if ($unreadMessages)
                        <span class="badge badge-pill badge-danger">1</span>
                    @endif
                </li>
            @endforeach

        </ul>


    </div>
    <button type="button" class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed"
        data-toggle="modal" data-target="#conversationplus">
        <span class="fa-solid fa-plus"></span>
    </button>
    <div wire:ignore.self class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <center>
                        <h5 class="modal-title retro" id="exampleModalLabel">@lang('messages.confirm_remove')
                        </h5>
                    </center>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <br>
                    <h5 class="retro">
                        <center>@lang('messages.areyousure_deleted') <br> (@lang('messages.you_cantrevocery'))</center>
                    </h5>
                </div>
                <div class="modal-footer">
                    <button type="button" wire:click.prevent="update()"
                        class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                        data-dismiss="modal">@lang('messages.yes_closes')</button>
                </div>
            </div>
        </div>
    </div>
</div>
