<div>
    <div class="page-content-wrapper py-3">
        <div class="container">
            <!-- User Information-->
            <div class="card user-info-card mb-3">
                <div class="card-body d-flex align-items-center">
                    <div class="user-profile me-3">
                        @if (auth()->user()->profile_picture)
                            <img src="{{ url(auth()->user()->profile_picture) }}" alt="">
                            <form wire:submit.prevent="SendFile">

                                <input class="form-control edition" wire:model="profile_picture" type="file"
                                    accept="image/*">
                            </form>
                        @else
                            <img src="{{ asset('img/profile-picture-not-found.png') }}" alt="">
                            <form wire:submit.prevent="SendFile">

                                <input class="form-control edition" wire:model="profile_picture" type="file"
                                    accept="image/*">
                            </form>
                        @endif


                    </div>
                    <div class="user-info">
                        <div class="d-flex align-items-center">
                            <h5 class="mb-1">{{ auth()->user()->user_name }}</h5><span
                                class="badge bg-danger ms-2 rounded-pill">Vendedor Profesional</span>
                        </div>
                        <p class="mb-0">@lang('messages.member_date') -
                            {{ date_format(auth()->user()->created_at, 'Y') }}</p>
                        <p class="mb-0"> {{ auth()->user()->score[1] }}</p>
                    </div>
                </div>
            </div>

            <div class="listview-title mt-2">Información</div>
            <ul class="listview link-listview">
                <li>
                    <a href="{{ route('config_personal_data') }}">
                        Datos Personales
                        <span class="text-muted">Editar</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('balance_data') }}">
                        Saldo Disponible
                        <span class="badge badge-success">{{ auth()->user()->cash }} €</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('referrals_index') }}">
                        Referidos
                        <span class="badge badge-danger">{{ count(auth()->user()->referidos) }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('public_profile_data') }}">
                        Perfil Público
                        <span class="text-muted">Editar</span>
                    </a>
                </li>
            </ul>
            <div class="listview-title mt-2">Panel de Control</div>
            <ul class="listview link-listview">
                <li>
                    <a href="#">
                        Compras
                        <span class="badge badge-primary">52</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        Ventas
                        <span class="badge badge-primary">52</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        Transacciones
                        <span class="text-muted">Editar</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        Inventario
                        <span class="text-muted">Editar</span>
                    </a>
                </li>
            </ul>
        </div>

    </div>

    @if ($profile_picture)
        <div wire:init="openModal" wire:ignore.self class="modal fade" id="photoModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <center>
                            <h5 class="modal-title retro" id="exampleModalLabel">Vista Previa
                            </h5>
                        </center>
                        <button wire:click="resetForm" class="confirmclosed btn btn-close p-1 ms-auto me-0"
                            class="close" data-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form wire:submit.prevent="SendFile">
                            <br>
                            <input id="file-input" name="profile_picture" wire:model="photo"
                                id="upload{{ $iteration }}" type="hidden" />
                            <img src="{{ $profile_picture->temporaryUrl() }}">
                            <br><br>
                            <button type="submit"
                                class="btn confirmclosed btn-submit btn-primary w-100 text-lg font-extrabold">Actualizar</button>
                            <br><br>
                            <button wire:click="resetForm"
                                class="btn confirmclosed btn-submit btn-danger w-100 text-lg font-extrabold">No ,
                                cancelar</button>
                        </form>
                    </div>



                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                window.livewire.emit('show');
            });



            window.livewire.on('show', () => {
                $('#photoModal').modal('show');
                $('.confirmclosed').click(function() { //Waring Modal Confirm Close Button
                    $('#photoModal').modal('hide'); //Hide Warning Modal
                });
            });
        </script>
    @endif
</div>
