<div>
    @foreach ($arrived as $key)
        @php($fecha = date('d-m-Y'))
        @php($Date = date('d-m-Y', strtotime($key->created_on)))
        @php($prueba = date('d-m-Y', strtotime($fecha . ' + 5 days')))
    @endforeach
    <div class="card-orders">

        <table class="table mb-0">
            <thead>
                <tr>
                    <th scope="col">Comprador</th>
                    <th scope="col">Estado del Pedido</th>
                    <th scope="col">Ver</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($arrived as $key)
                    <div @if ($loop->last) id="last_record" @endif>
                        <tr>
                            <td>
                                {{ str_limit($key->buyer ? $key->buyer->user_name : 'Ninguno', 10) }} <span
                                    class="flag-icon flag-icon-esp"></span> <br>
                                {{ date('d-m-Y', strtotime($key->created_on)) }}
                            </td>
                            <td>
                                <div class="product-description">
                                    @if ($key->status == 'DD')
                                        <div class="total-result-of-ratings"><span> @lang('messages.send_sale_order_s')
                                               </span>
                                        </div>
                                    @else
                                        <div class="total-result-of-ratings"><span> @lang('messages.not_paid')
                                                </span>
                                        </div>
                                    @endif
                                </div>
                            </td>
                            <td>
                                <a href="{{ url('/account/sales/view/' . $key->order_identification) }}"
                                    class="btn btn-icon btn-danger">
                                    
                                    <ion-icon wire:ignore name="eye-outline"></ion-icon>
                                </a>
                            </td>
                        </tr>
                    </div>
                @endforeach
            </tbody>
        </table>

    </div>
    <script>
        const lastRecord = document.getElementById('last_record');
        const options = {
            root: null,
            threshold: 1,
            rootMargin: '0px'
        }
        const observer = new IntersectionObserver((entries, observer) => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    @this.loadMore()
                }
            });
        });
        observer.observe(lastRecord);
    </script>
</div>
