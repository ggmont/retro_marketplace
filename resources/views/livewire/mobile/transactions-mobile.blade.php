<div>

    <div wire:loading.delay.class="opacity-50" class="section full">

        <div class="rating-and-review-wrapper bg-white mb-5">
            <button type="button" class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed"
                data-toggle="modal" data-target="#exampleModal">
                <span class="fa-solid fa-filter"></span>
            </button>

            <div class="accordion" id="accordionExample1">
                @foreach ($trans as $key)
                    @php($stat = true)
                    @if ($key->paid_out == 'N' || $key->paid_out == 'P')
                        @php($stat = false)
                    @else
                        @php($stat = true)
                    @endif
                    @if ($stat)
                        <div @if ($loop->last) id="last_record" @endif>
                            <ul class="listview image-listview media accordion-item">
                                <li class="item">
                                    <h2 class="accordion-header">
                                        <button class="listview image-listview media accordion-button collapsed"
                                            type="button" data-bs-toggle="collapse"
                                            data-bs-target="#accordion{{ $key->id }}">

                                            <dt class="col-6 text-sm">
                                                {{ __($key->stats) }}
                                                @if ($key->status == 'YV')
                                                    {{ $key->seller->user_name }}
                                                @elseif($key->status == 'YC')
                                                    {{ $key->buyer->user_name }}
                                                @endif
                                            </dt>
                                            <dd class="col-6 text-right text-sm font-weight-bold">
                                                @if ($key->stats == 'Compra de Productos')
                                                    <span class="badge badge-danger">
                                                        -
                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                    </span>
                                                @elseif($key->stats == 'Venta de Productos')
                                                    <span class="badge badge-success">
                                                        +
                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                    </span>
                                                @elseif($key->stats == 'Saldo retirado por RetroGamingMarket')
                                                    <span class="badge badge-danger">
                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                    </span>
                                                @elseif($key->stats == 'Saldo retirado')
                                                    <span class="badge badge-danger">
                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                    </span>
                                                @elseif($key->stats == 'Comisión RetroGamingMarket')
                                                    <span class="badge badge-danger">
                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                    </span>
                                                @elseif($key->stats == 'Reembolsos - ventas')
                                                    <span class="badge badge-danger">
                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                    </span>
                                                @elseif($key->stats == 'Pedido pendiente de envío')
                                                    <span class="badge badge-warning">
                                                        +
                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                    </span>
                                                @elseif($key->stats == 'Pagos extra - compras')
                                                    <span class="badge badge-danger">
                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                    </span>
                                                @else
                                                    <span class="badge badge-success">
                                                        +
                                                        {{ $key->status == 'CE' || $key->status == 'CP' || $key->status == 'RC' || $key->status == 'FD' || $key->status == 'TD' ? '- ' : '' }}
                                                        {{ in_array($key->status, ['RE', 'RF']) ? ($key->paid_out == 'Y' ? ($key->buyer_user_id == Auth::user()->id && $key->status == 'RE' ? '- ' : ($key->seller_user_id == Auth::user()->id && $key->status == 'RF' ? '- ' : '')) . number_format($key->total, 2) . ' €' : ($key->paid_out == 'N' ? 'Rechazado' : 'Pendiente de aprobación')) : number_format($key->total, 2) . ' €' }}
                                                    </span>
                                                @endif
                                            </dd>

                                        </button>
                                    </h2>
                                    <div id="accordion{{ $key->id }}" class="accordion-collapse collapse"
                                        data-bs-parent="#accordionExample{{ $key->id }}">
                                        <div class="accordion-body">
                                            <div class="item-body-wrapper">
                                                <dl class="row evaluations-descriptionList mb-2">
                                                    <dt class="col-6 color-primary">Fecha</dt>
                                                    <dd class="col-6 text-right color-primary font-weight-bold">
                                                        {{ $key->updated_at->format('d/m/Y H:i') }}</dd>
                                                    <dt class="col-6 color-primary">Número de transacción</dt>
                                                    <dd class="col-6 text-right color-primary font-weight-bold">

                                                        {{ $key->order_identification }}

                                                    </dd>
                                                    @if (!in_array($key->status, ['EC', 'CE', 'AC', 'SC', 'PV', 'AS', 'YC', 'YV']))
                                                        <dt class="col-12 color-primary"> <a
                                                                href="/account/{{ $key->tipoSeller ? 'sales' : ($key->status == 'FD' ? 'sales' : 'purchases') }}/view/{{ in_array($key->status, ['FD', 'CS', 'RE', 'RF']) ? $key->instructions : $key->order_identification }}"
                                                                class="btn btn-success w-100 font-bold">Ver
                                                                transaccion</a>
                                                        </dt>
                                                    @else
                                                    @endif
                                                </dl>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>

                            <div class="modal fade" id="modalNewMessage{{ $key->id }}" tabindex="-1"
                                role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <center>
                                                <h5 class="modal-title retro" id="exampleModalLabel">Enviar Mensaje
                                                    a :
                                                    {{ $key->id }}
                                                </h5>
                                            </center>
                                            <button class="close-message btn btn-close p-1 ms-auto me-0" class="close"
                                                data-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" action="{{ route('newMessage') }} ">
                                                {{ csrf_field() }}
                                                <input type="hidden" autocomplete="off" class="form-control"
                                                    name="name" value="{{ $key->id }}" required>
                                                <div class="form-group boxed">
                                                    <div class="input-wrapper">
                                                        <textarea name="content" rows="2" placeholder="@lang('messages.write_msg')" class="form-control"></textarea>
                                                        <i class="clear-input">
                                                            <ion-icon name="close-circle"></ion-icon>
                                                        </i>
                                                    </div>
                                                </div>
                                                <br>
                                                <button type="submit"
                                                    class="btn confirmclosed btn-submit btn-primary w-100 text-lg font-extrabold">Enviar</button>
                                            </form>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>

        </div>

    </div>

</div>
