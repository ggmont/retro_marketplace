<div>

    <!-- Header Area -->
    <div class="header-area">
        <div class="container">
            <!-- Header Content -->
            <div class="header-content position-relative d-flex align-items-center justify-content-between">
                <!-- Chat User Info -->
                <div class="chat-user--info d-flex align-items-center">
                    <!-- Back Button -->
                    <div class="back-button"><a href="/account/messages">
                            <svg class="bi bi-arrow-left-short" width="32" height="32" viewBox="0 0 16 16"
                                fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z">
                                </path>
                            </svg></a></div>
                    <!-- User Thumbnail & Name -->
                    <div class="user-thumbnail-name">
                        @if ($sender->profile_picture)
                            <img src="{{ url($sender->profile_picture) }}" alt="">
                        @else
                            <img src="{{ asset('img/profile-picture-not-found.png') }}" alt="">
                        @endif

                        <div class="info ms-1">
                            @if (App\SysUserRoles::where('user_id', $sender->id)->whereIn('role_id', [7])->count() > 0)
                                <p>Moderador</p>
                            @else
                                <p>{{ $sender->user_name }}</p>
                            @endif
                            @if ($sender->is_online == true)
                                <span class="active-status">Activo Ahora</span>
                            @else
                                <span class="active-status">
                                    <font color="#c30000">Inactivo Ahora</font>
                                </span>
                            @endif

                            <!-- span.offline-status.text-muted Last actived 27m ago-->
                        </div>
                    </div>
                </div>
                <!-- Call & Video Wrapper -->
                <div class="call-video-wrapper d-flex align-items-center">
                    <!-- Info Icon -->
                    <div class="info-icon"><a class="text-secondary" data-toggle="modal" data-target="#profile">
                            <svg class="bi bi-info-circle" xmlns="http://www.w3.org/2000/svg" width="18"
                                height="18" fill="currentColor" viewBox="0 0 16 16">
                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
                                <path
                                    d="M8.93 6.588l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z">
                                </path>
                            </svg></a></div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-content-wrapper py-3">
        <div class="chat-container">
            <div class="chat-content-wrap" wire:poll.1s="mountdata" id="chat-messages">
                @if (filled($allmessages))
                    @foreach ($allmessages as $mgs)
                        @if ($mgs->alias_from_id == auth()->id())
                            <div class="single-chat-item outgoing">
                                <!-- User Avatar -->
                                <div class="user-avatar mt-1">
                                    <span class="name-first-letter"></span>
                                    @if ($mgs->user->profile_picture)
                                        <img src="{{ url(auth()->user()->profile_picture) }}" alt="">
                                    @else
                                        <img src="{{ asset('img/profile-picture-not-found.png') }}" alt="">
                                    @endif

                                </div>
                                <!-- User Message -->
                                <div class="user-message">
                                    <div class="message-content">
                                        @if ($mgs->image)
                                            <div class="single-message">
                                                <a href="{{ url('storage/' . $mgs->image) }}" class="fancybox"
                                                    data-fancybox="{{ $mgs->user->user_name }}">
                                                    <img src="{{ asset('storage/' . $mgs->image) }}" alt=""></a>
                                            </div>
                                        @else
                                            <div class="single-message">
                                                <p>{{ $mgs->content }}</p>
                                            </div>
                                        @endif
                                        &nbsp;
                                        <div class="dropstart">
                                            @if ($mgs->user->user_name == auth()->user()->user_name)
                                                <a wire:click="deleteId({{ $mgs->id }})" data-toggle="modal"
                                                    data-target="#deleteModal">
                                                    <i class="bi bi-x"></i>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- Time and Status -->
                                    <div class="message-time-status">
                                        <div class="sent-time">{{ $mgs->created_at }}</div>
                                        <div class="sent-status seen"><i class="bi bi-check-lg" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="single-chat-item">
                                <!-- User Avatar -->
                                <div class="user-avatar mt-1">
                                    @if ($mgs->user->profile_picture)
                                        <img src="{{ url($mgs->user->profile_picture) }}" alt="">
                                    @else
                                        <img src="{{ asset('img/profile-picture-not-found.png') }}" alt="">
                                    @endif
                                </div>
                                <!-- User Message -->
                                <div class="user-message">
                                    <div class="message-content">
                                        @if ($mgs->image)
                                            <div class="single-message">



                                                <a href="{{ url('storage/' . $mgs->image) }}" class="fancybox"
                                                    data-fancybox="{{ $mgs->user->user_name }}">
                                                    <img src="{{ asset('storage/' . $mgs->image) }}"
                                                        alt=""></a>


                                            </div>
                                        @else
                                            <div class="single-message">
                                                <p>{{ $mgs->content }}</p>
                                            </div>
                                        @endif
                                        <!-- Options -->
                                        <div class="dropstart">
                                            &nbsp;
                                            @if ($mgs->user->user_name == auth()->user()->user_name)
                                                <a wire:click="deleteId({{ $mgs->id }})" data-toggle="modal"
                                                    data-target="#deleteModal">
                                                    <i class="bi bi-x"></i>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- Time and Status -->
                                    <div class="message-time-status">
                                        <div class="sent-time">{{ $mgs->created_at }}</div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div class="chat-footer">

        <div class="container h-100">
            <div class="chat-footer-content h-100 d-flex align-items-center">

                <button class="btn btn-submit" wire:click.prevent="SubmitPhoto" type="submit">
                    <font color="white">
                        <svg xmlns="http://www.w3.org/2000/svg" wwidth="19" height="19" fill="currentColor"
                            class="bi bi-camera pb-1" viewBox="0 0 16 16">
                            <path
                                d="M15 12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h1.172a3 3 0 0 0 2.12-.879l.83-.828A1 1 0 0 1 6.827 3h2.344a1 1 0 0 1 .707.293l.828.828A3 3 0 0 0 12.828 5H14a1 1 0 0 1 1 1v6zM2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4H2z" />
                            <path
                                d="M8 11a2.5 2.5 0 1 1 0-5 2.5 2.5 0 0 1 0 5zm0 1a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7zM3 6.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z" />
                        </svg>
                    </font>
                </button>
                <form wire:submit.prevent="SendFile">
                    @if ($photo)
                        <input wire:model="message" id="message" class="form-control"
                            placeholder="La imagen se ha cargado , enviar?" disabled>
                    @else
                        <input wire:model="message" id="message" class="form-control"
                            placeholder="@lang('messages.write_a_message')" required>
                    @endif

                    <button class="btn btn-submit" id="my_button" type="submit">
                        <svg class="bi bi-cursor" xmlns="http://www.w3.org/2000/svg" width="18" height="18"
                            fill="currentColor" viewBox="0 0 16 16">
                            <path
                                d="M14.082 2.182a.5.5 0 0 1 .103.557L8.528 15.467a.5.5 0 0 1-.917-.007L5.57 10.694.803 8.652a.5.5 0 0 1-.006-.916l12.728-5.657a.5.5 0 0 1 .556.103zM2.25 8.184l3.897 1.67a.5.5 0 0 1 .262.263l1.67 3.897L12.743 3.52 2.25 8.184z">
                            </path>
                        </svg>
                    </button>
                    <br>

                </form>
            </div>

        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <center>
                        <h5 class="modal-title retro" id="exampleModalLabel">@lang('messages.confirm_remove')
                        </h5>
                    </center>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <br>
                    <h5 class="retro">
                        <center>@lang('messages.msg_delete')</center>
                    </h5>
                </div>
                <div class="modal-footer">
                    <button type="button" wire:click.prevent="delete()"
                        class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                        data-dismiss="modal">@lang('messages.yes_closes')</button>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="chatModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">Enviar Imagen</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        @if ($photo)
                        @else
                            <div class="notification-area pt-3 pb-2">
                                <div class="list-group">
                                    <a class="list-group-item d-flex align-items-center" href="#"><span
                                            class="noti-icon"><i class="lni lni-warning"></i></span>
                                        <div class="noti-info">
                                            <h6 class="mb-0">@lang('messages.wait_momment')</h6>
                                        </div>
                                    </a>

                                </div>
                            </div>
                        @endif
                        <div wire:ignore class="custom-file-upload" id="fileUpload1">
                            <input wire:model="photo" name="photo" type="file" id="fileuploadInput2"
                                accept=".png, .jpg, .jpeg">
                            <label for="fileuploadInput2">
                                <span>
                                    <strong>
                                        <ion-icon name="cloud-upload-outline"></ion-icon>
                                        <i>@lang('messages.upload_touch')</i>
                                    </strong>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        @if ($photo)
                            <button type="button" wire:click.prevent="SendFile()"
                                class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                                data-dismiss="modal">Enviar</button>
                        @else
                            <button type="button" disabled wire:click.prevent="SendFile()"
                                class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                                data-dismiss="modal">Enviar</button>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('livewire:load', function () {
            window.livewire.on('scrollToBottom', () => {
                const chatMessages = document.getElementById('chat-messages');
                chatMessages.scrollTop = chatMessages.scrollHeight;
            });
        });
    </script>
    

    <script>
        document.addEventListener('livewire:load', function() {
            $('.custom-file-upload').on('change', function() {
                //alert(this.value)
                @this.set('photo', this.value);
            });
        })
    </script>

</div>
