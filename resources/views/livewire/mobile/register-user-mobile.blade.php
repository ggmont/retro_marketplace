<div>
    <div class="login-form">
        <form action="{{ url('/register-company-account') }}" method="POST">
            {{ csrf_field() }}
            <div class="listview-title mt-2">Datos Personales</div>
            <div class="section mt-2 mb-3">

                <div class="form-group boxed">
                    <label class="form-label" for="name5">Nombre</label>
                    <div class="input-wrapper">
                        <input type="text" class="form-control" name="first_name"
                            placeholder="Su nombre" reqauired>
                        <i class="clear-input">
                            <ion-icon name="close-circle"></ion-icon>
                        </i>
                    </div>
                    @error('first_name') <span class="text-xs text-red-700 error retro">{{ $message }}</span> @enderror
                </div>

                <div class="form-group boxed">
                    <label class="form-label" for="name5">Apellido</label>
                    <div class="input-wrapper">
                        <input type="text" class="form-control" name="last_name"
                            placeholder="Su apellido" required>
                        <i class="clear-input">
                            <ion-icon name="close-circle"></ion-icon>
                        </i>
                    </div>
                    @error('last_name') <span class="text-xs text-red-700 error retro">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="listview-title mt-2">Dirección</div>
            <div class="section mt-2 mb-3">

                <div class="form-group boxed">
                    <label class="form-label" for="name5">Su dirección</label>
                    <div class="input-wrapper">
                        <input type="text" class="form-control" name="address"
                            placeholder="Su dirección" required>
                        <i class="clear-input">
                            <ion-icon name="close-circle"></ion-icon>
                        </i>
                    </div>
                    @error('address') <span class="text-xs text-red-700 error retro">{{ $message }}</span> @enderror
                </div>

                <div class="form-group boxed">
                    <label class="form-label" for="name5">Su código postal</label>
                    <div class="input-wrapper">
                        <input type="text" class="form-control" name="zipcode"
                            placeholder="Codigo Postal" required>
                        <i class="clear-input">
                            <ion-icon name="close-circle"></ion-icon>
                        </i>
                    </div>
                    @error('zipcode') <span class="text-xs text-red-700 error retro">{{ $message }}</span> @enderror
                </div>

                <div class="form-group boxed">
                    <label class="form-label" for="name5">Ciudad</label>
                    <div class="input-wrapper">
                        <input type="text" class="form-control" name="city"
                            placeholder="Ciudad" required>
                        <i class="clear-input">
                            <ion-icon name="close-circle"></ion-icon>
                        </i>
                    </div>
                    @error('city') <span class="text-xs text-red-700 error retro">{{ $message }}</span> @enderror
                </div>

                <div class="form-group boxed">
                    <label class="form-label" for="name5">País</label>
                    <div class="input-wrapper">
                        <select id="br-register-co-country" class="form-control br-ajs-select2"
                            name="country_id" ng-model="brRegisterCountry" required
                            data-br-placeholder="{{ __('Su país') }}" data-br-resolve="Y">
                            <option style="display:none" value=""> @lang('messages.country_select')</option>
                            @foreach ($country as $c)
                                <option value="{{ $c->id }}">{{ $c->name }}</option>
                            @endforeach
                        </select>
                        @error('country_id') <span class="text-xs text-red-700 error retro">{{ $message }}</span> @enderror
                    </div>
                </div>

            </div>
            <div class="listview-title mt-2">Datos de la cuenta</div>
            <div class="section mt-2 mb-3">

                <div class="form-group boxed">
                    <label class="form-label" for="name5">Correo Electronico</label>
                    <div class="input-wrapper">
                        <input type="email" class="form-control" name="email"
                            placeholder="rgm@gmail.com" required>
                        <i class="clear-input">
                            <ion-icon name="close-circle"></ion-icon>
                        </i>
                    </div>
                    @error('email') <span class="text-xs text-red-700 error retro">{{ $message }}</span> @enderror
                </div>

                <div class="form-group boxed">
                    <label class="form-label" for="name5">Usuario</label>
                    <div class="input-wrapper">
                        <input type="email" class="form-control" name="user_name"
                            placeholder="usuario" required>
                        <i class="clear-input">
                            <ion-icon name="close-circle"></ion-icon>
                        </i>
                    </div>
                    @error('user_name') <span class="text-xs text-red-700 error retro">{{ $message }}</span> @enderror
                </div>


                <div class="form-group boxed">
                    <label class="form-label" for="name5">Contraseña - Max 8.</label>
                    <div class="input-wrapper">
                        <input type="password" class="form-control" name="password"
                            placeholder="*******" required>
                        <i class="clear-input">
                            <ion-icon name="close-circle"></ion-icon>
                        </i>
                    </div>
                    @error('password') <span class="text-xs text-red-700 error retro">{{ $message }}</span> @enderror
                </div>


                <div class="form-group boxed">
                    <label class="form-label" for="name5">Captcha</label>
                    <div class="captcha">
                        {!! NoCaptcha::renderJs() !!}
                        {!! NoCaptcha::display() !!}
                        @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block">
                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class=" mt-1 text-start">
                    <div class="form-check">
                        <input type="checkbox" name="brRegisterCoYearsOld" class="form-check-input">
                        <label class="form-check-label" for="customCheckb1">Tengo mas de 18
                            años</label>
                    </div>
                    @error('brRegisterCoYearsOld') <span class="text-xs text-red-700 error retro">{{ $message }}</span> @enderror

                </div>

                <div class=" mt-1 text-start">
                    <div class="form-check">
                        <input type="checkbox" name="brRegisterCoTerms" class="form-check-input">
                        <label class="form-check-label" for="customCheckb1"> He leído y acepto los <a
                                href="#">términos y condiciones & Política de
                                privacidad</a></label>
                    </div>
                    @error('brRegisterCoTerms') <span class="text-xs text-red-700 error retro">{{ $message }}</span> @enderror
                </div>
                <br>
                <button id="submitLogin" class="btn mb-3 btn-danger font-extrabold btn-lg w-100"
                    type="submit">Registrarse</button>
            </div>
        </form>
    </div>
</div>
