<div>
    <div class="top-products-area clearfix py-3">
        @if ($search !== '' || $search_platform !== '' || $search_category !== '' || $search_region !== '')
            <button wire:click="clear" type="button"
                class="filterToggleDelete d-lg-none btn btn-lg btn-danger btn-rounded btn-absolute">
                <span class="fa-solid fa-trash"></span>
            </button>
        @endif
        <button type="button" class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed"
            data-toggle="modal" data-target="#exampleModal">
            <span class="fa-solid fa-filter"></span>
        </button>
        <div class="container">
            <div wire:loading.delay.class="opacity-50" class="row g-3">
                <!-- Single Top Product Card-->
                @isset($profesional)
                    @foreach ($profesional as $i)
                        <div @if ($loop->last) id="last_record" @endif class="col-6 col-md-4 col-lg-3">
                            <div class="card top-product-card">
                                <div class="card-body"><a href="{{ route('product-inventory-show', $i->id) }}">

                                        <div class="figure">
                                            <a href="{{ route('product-inventory-show', $i->id) }}">
                                                @if ($i->images->first())
                                                    @if (str_starts_with($i->images->first()->image_path, 'https'))
                                                        <img class="card-product-image-holder image-main"
                                                            src="{{ $i->images->first()->image_path }}"
                                                            alt="{{ $i->title }} - RetroGamingMarket">
                                                    @elseif ($i->product_id > 0)
                                                        <img class="card-product-image-holder image-main"
                                                            src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                            alt="{{ $i->product->name }} - RetroGamingMarket">
                                                    @else
                                                        <img class="card-product-image-holder image-main"
                                                            src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                            alt="{{ $i->title }} - RetroGamingMarket">
                                                    @endif
                                                @elseif ($i->product->images->first())
                                                    <img class="card-product-image-holder image-main"
                                                        src="{{ url('/images/' . $i->product->images->first()->image_path) }}"
                                                        alt="{{ $i->product->name }} - RetroGamingMarket">
                                                @else
                                                    <img loading="lazy" class="card-product-image-holder image-main"
                                                        src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                        alt="{{ $i->product->name }} - RetroGamingMarket">
                                                @endif
                                            </a>
                                        </div>
                                        <span class="text-sm font-bold sicker">
                                            <a href="{{ route('user-info', $i->user->user_name) }}" target="_blank">
                                                <font color="white">
                                                    {{ str_limit($i->user->user_name, 10) }} -
                                                    @include('partials.pais_user_home')
                                                </font>
                                            </a>
                                        </span>

                                        @if ($i->title !== null)
                                            <span class="product-title ml-1">{{ str_limit($i->title, 16) }}</span>
                                            <br>
                                            <br>
                                            <br>
                                        @else
                                            <span class="product-title ml-1">{{ str_limit($i->product->name, 16) }}</span>
                                            <br>
                                            <span class="progress-title ml-1">{{ $i->product->platform }}</span>
                                            <br>
                                            <span class="progress-title ml-1">{{ $i->product->region }}</span>
                                            <br>
                                            Cantidad : {{ $i->quantity }}

                                            @if ($i->product->categoryProd->parent_id == 1)
                                                <div class="flex ulti ml-1">
                                                    <img height="10px" width="15px" src="/{{ $i->box }}"
                                                        data-toggle="popover"
                                                        data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->cover }}" data-toggle="popover"
                                                        data-content="Caratula - {{ __(App\AppOrgUserInventory::getCondicionName($i->cover_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->manual }}" data-toggle="popover"
                                                        data-content="Manual - {{ __(App\AppOrgUserInventory::getCondicionName($i->manual_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->game }}" data-toggle="popover"
                                                        data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->extra }}" data-toggle="popover"
                                                        data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                </div>
                                                <div class="text-center pt-2">



                                                    @if ($i->comments == '')
                                                        <span data-toggle="popover" data-html="true"
                                                            data-content="
                                                @lang('messages.not_working_profile')"
                                                            data-placement="top" data-trigger="hover"
                                                            class="badge-comment badge-info"><i
                                                                class="fa-solid fa-circle-xmark"></i></span>
                                                    @else
                                                        <span data-toggle="popover" data-html="true"
                                                            data-content="
                                            {{ $i->comments }}
                                    "
                                                            data-placement="top" data-trigger="hover"
                                                            class="badge-comment badge-info"><i
                                                                class="fa-solid fa-comment"></i></span>
                                                    @endif

                                                    <span data-toggle="popover" data-html="true"
                                                        data-content="
                                                    <b>Estado :</b> <br />
                                                    Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }} <br />
                                                    Carátula - {{ __(App\AppOrgUserInventory::getCondicionName($i->cover_condition)) }} <br />
                                                    Manual - {{ __(App\AppOrgUserInventory::getCondicionName($i->manual_condition)) }} <br />
                                                    Juego - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }} <br />
                                                    Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }} <br />
                                                "
                                                        data-placement="top" data-trigger="hover"
                                                        class="badge-inventory badge-info">ESTADO</span>
                                                </div>
                                            @elseif ($i->product->categoryProd->parent_id == 2)
                                                <div class="flex ulti ml-1">
                                                    <img height="10px" width="15px" src="/{{ $i->box }}"
                                                        data-toggle="popover"
                                                        data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->cover }}" data-toggle="popover"
                                                        data-content="Interior - {{ __(App\AppOrgUserInventory::getCondicionName($i->cover_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->manual }}" data-toggle="popover"
                                                        data-content="Manual - {{ __(App\AppOrgUserInventory::getCondicionName($i->manual_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->game }}" data-toggle="popover"
                                                        data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->inside }}" data-toggle="popover"
                                                        data-content="Cables - {{ __(App\AppOrgUserInventory::getCondicionName($i->inside_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->extra }}" data-toggle="popover"
                                                        data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                </div>
                                                <div class="text-center pt-2">
                                                    @if ($i->comments == '')
                                                        <i class="fa-solid fa-circle-xmark" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    @else
                                                        <span data-toggle="popover" data-html="true"
                                                            data-content="
                                        {{ $i->comments }}
                                "
                                                            data-placement="top" data-trigger="hover"
                                                            class="badge-comment badge-info"><i
                                                                class="fa-solid fa-comment"></i></span>
                                                    @endif

                                                    <span data-toggle="popover" data-html="true"
                                                        data-content="
                                    <b>Estado :</b> <br />
                                    Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }} <br />
                                    Carátula - {{ __(App\AppOrgUserInventory::getCondicionName($i->cover_condition)) }} <br />
                                    Manual - {{ __(App\AppOrgUserInventory::getCondicionName($i->manual_condition)) }} <br />
                                    Estado - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }} <br />
                                    Cables - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }} <br />
                                    Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->inside_condition)) }} <br />
                                "
                                                        data-placement="top" data-trigger="hover"
                                                        class="badge-inventory badge-info">ESTADO</span>
                                                </div>
                                            @else
                                                <div class="flex ulti ml-1">
                                                    <img height="10px" width="15px" src="/{{ $i->box }}"
                                                        data-toggle="popover"
                                                        data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->game }}" data-toggle="popover"
                                                        data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->extra }}" data-toggle="popover"
                                                        data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                </div>
                                                <div class="text-center pt-2">

                                                    @if ($i->comments == '')
                                                        <i class="fa-solid fa-circle-xmark" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    @else
                                                        <span data-toggle="popover" data-html="true"
                                                            data-content="
                                    {{ $i->comments }}
                            "
                                                            data-placement="top" data-trigger="hover"
                                                            class="badge-comment badge-info"><i
                                                                class="fa-solid fa-comment"></i></span>
                                                    @endif

                                                    <span data-toggle="popover" data-html="true"
                                                        data-content="
                                                <b>Estado :</b> <br />
                                                Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }} <br />
                                                Juego - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }} <br />
                                                Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }} <br />
                                            "
                                                        data-placement="top" data-trigger="hover"
                                                        class="badge-inventory badge-info">ESTADO</span>
                                                </div>
                                            @endif
                                        @endif
                                        <hr>
                                        <p class="sale-price ml-2">{{ number_format($i->price, 2) . ' €' }}</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endisset
            </div>
            @if ($loadAmount >= $totalRecords)
                <p class="text-gray-800 font-bold text-2xl text-center my-10">No hay mas productos disponibles!</p>
            @endif

        </div>
        <br><br><br><br><br>
        <!-- Modal -->
        <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title" id="addnewcontactlabel">Filtrar por :</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block pb-1 pt-2">

                                <form>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Tipo de ventas</label>

                                            <select wire:model="search_sales" style="width: 100%" id="search_sales"
                                                class="form-control form-select">
                                                <option value="" selected> @lang('messages.alls')
                                                </option>
                                                <option value="0">Precio fijo
                                                </option>
                                                <option value="1">Ofertas
                                                </option>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label">Nombre</label>
                                            <input wire:model="search" type="text" class="form-control"
                                                placeholder="Nombre del Producto" autocomplete="off">
                                            <i class="clear-input">
                                                <ion-icon name="close-circle"></ion-icon>
                                            </i>
                                        </div>
                                    </div>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Plataforma</label>
                                            <div wire:ignore>
                                                <select wire:model="search_platform" style="width: 100%"
                                                    class="form-control select2 form-select">
                                                    <option value="" selected> @lang('messages.alls')
                                                    </option>
                                                    @foreach ($platform as $p)
                                                        <option value="{{ $p->value }}">{{ $p->value }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Categoría</label>
                                            <div wire:ignore>
                                                <select wire:model="search_category" style="width: 100%"
                                                    class="form-control ultra2 form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    <optgroup label="@lang('messages.category')">
                                                        <option value="Juegos">@lang('messages.games')</option>
                                                        <option value="Consolas">@lang('messages.consoles')</option>
                                                        <option value="Periféricos">@lang('messages.peripherals')</option>
                                                        <option value="Accesorios">@lang('messages.accesories')</option>
                                                        <option value="Merchandising">Merchandising</option>
                                                    </optgroup>
                                                    <optgroup label="@lang('messages.sub_category')">
                                                        <option value="Mandos">@lang('messages.controls')</option>
                                                        <option value="Micrófonos">@lang('messages.microphones')</option>
                                                        <option value="Teclados">@lang('messages.keyboard')</option>
                                                        <option value="Fundas">@lang('messages.funded')</option>
                                                        <option value="Cables">@lang('messages.cable')</option>
                                                        <option value="Cargadores">@lang('messages.chargers')</option>
                                                        <option value="Merchandising -> Logos 3d">@lang('messages.3d_logo')
                                                        </option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Región</label>
                                            <div wire:ignore>
                                                <select wire:model="search_region" style="width: 100%"
                                                    class="form-control region2 form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    @foreach ($region as $r)
                                                        <option value="{{ $r->value_id }}">{{ $r->value_id }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>

            <script>
                document.addEventListener('livewire:load', function() {
                    $('.select2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.ultra2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.region2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.country2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.language2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.media2').select2({
                        dropdownParent: $('#exampleModal')
                    });
                    $('.select2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_platform', this.value);
                    });
                    $('.ultra2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_category', this.value);
                    });
                    $('.region2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_region', this.value);
                    });
                    $('.country2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_country', this.value);
                    });
                    $('.language2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_language', this.value);
                    });
                    $('.media2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_media', this.value);
                    });
                })



                const lastRecord = document.getElementById('last_record');
                const option = {
                    root: null,
                    threshold: 1,
                    rootMargin: '0px'
                }

                const observers = new IntersectionObserver((entries, observers) => {
                    entries.forEach(entry => {
                        if (entry.isIntersecting) {
                            @this.loadMore()
                        }
                    });
                });
                observers.observe(lastRecord);
            </script>



        </div>
    </div>
</div>
