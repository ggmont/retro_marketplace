<div>
    <div class="top-products-area clearfix py-3">
        @if ($search !== '' || $search_platform !== '' || $search_category !== '' || $search_region !== '')
            <button wire:click="clear_inventory" type="button"
                class="filterToggleDelete d-lg-none btn btn-lg btn-danger btn-rounded btn-absolute">
                <span class="fa-solid fa-trash"></span>
            </button>
        @endif
        <button type="button" class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed"
            data-toggle="modal" data-target="#collectionModal">
            <span class="fa-solid fa-filter"></span>
        </button>
        <div class="container">
            <div wire:loading.delay.class="opacity-50" class="row g-3">
                <!-- Single Top Product Card-->
                @isset($collection)
                    @foreach ($collection as $i)
                        <div @if ($loop->last) id="last_collection" @endif class="col-6 col-md-4 col-lg-3">
                            <div class="card top-product-card">
                                <div class="card-body"><a href="{{ route('product-inventory-show', $i->id) }}">

                                        <div class="figure">
                                            <a href="{{ route('product-inventory-show', $i->id) }}">
                                                @if ($i->images->first())
                                                    <img class="card-product-image-holder image-main"
                                                        src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                        alt="">
                                                @elseif ($i->product->images->first())
                                                    <img class=" card-product-image-holder image-main"
                                                        src="{{ url('/images/' . $i->product->images->first()->image_path) }}"
                                                        alt="">
                                                @else
                                                    <img loading="lazy" class="first-img"
                                                        src="{{ asset('assets/images/art-not-found.jpg') }}" alt="">
                                                @endif
                                            </a>
                                        </div>
                                        <span class="text-sm font-bold sicker">
                                            <a href="{{ route('user-info', $i->user->user_name) }}" target="_blank">
                                                <font color="white">
                                                    {{ str_limit($i->user->user_name, 10) }} -
                                                    @include('partials.pais_user_home')
                                                </font>
                                            </a>
                                        </span>
                                        <span class="product-title ml-1">{{ str_limit($i->product->name, 16) }}
                                        </span>
                                        <br>
                                        <span class="progress-title">
                                            <div class="ml-1 pb-1">
                                                {{ $i->product->platform }} <br>
                                                {{ $i->product->region }}<br>
                                                Cantidad : {{ $i->quantity }}
                                            </div>
                                            @if ($i->product->categoryProd->parent_id == 1)
                                                <div class="flex ulti ml-1">
                                                    <img height="10px" width="15px" src="/{{ $i->box }}"
                                                        data-toggle="popover"
                                                        data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->cover }}" data-toggle="popover"
                                                        data-content="Caratula - {{ __(App\AppOrgUserInventory::getCondicionName($i->cover_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->manual }}" data-toggle="popover"
                                                        data-content="Manual - {{ __(App\AppOrgUserInventory::getCondicionName($i->manual_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->game }}" data-toggle="popover"
                                                        data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->extra }}" data-toggle="popover"
                                                        data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                </div>
                                                <div class="text-center pt-2">



                                                    @if ($i->comments == '')
                                                        <span data-toggle="popover" data-html="true"
                                                            data-content="
                                                    @lang('messages.not_working_profile')"
                                                            data-placement="top" data-trigger="hover"
                                                            class="badge-comment badge-info"><i
                                                                class="fa-solid fa-circle-xmark"></i></span>
                                                    @else
                                                        <span data-toggle="popover" data-html="true"
                                                            data-content="
                                                {{ $i->comments }}
                                        "
                                                            data-placement="top" data-trigger="hover"
                                                            class="badge-comment badge-info"><i
                                                                class="fa-solid fa-comment"></i></span>
                                                    @endif

                                                    <span data-toggle="popover" data-html="true"
                                                        data-content="
                                                        <b>Estado :</b> <br />
                                                        Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }} <br />
                                                        Carátula - {{ __(App\AppOrgUserInventory::getCondicionName($i->cover_condition)) }} <br />
                                                        Manual - {{ __(App\AppOrgUserInventory::getCondicionName($i->manual_condition)) }} <br />
                                                        Juego - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }} <br />
                                                        Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }} <br />
                                                    "
                                                        data-placement="top" data-trigger="hover"
                                                        class="badge-inventory badge-info">ESTADO</span>
                                                </div>
                                            @elseif ($i->product->categoryProd->parent_id == 2)
                                                <div class="flex ulti ml-1">
                                                    <img height="10px" width="15px" src="/{{ $i->box }}"
                                                        data-toggle="popover"
                                                        data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->cover }}" data-toggle="popover"
                                                        data-content="Interior - {{ __(App\AppOrgUserInventory::getCondicionName($i->cover_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->manual }}" data-toggle="popover"
                                                        data-content="Manual - {{ __(App\AppOrgUserInventory::getCondicionName($i->manual_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->game }}" data-toggle="popover"
                                                        data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->inside }}" data-toggle="popover"
                                                        data-content="Cables - {{ __(App\AppOrgUserInventory::getCondicionName($i->inside_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->extra }}" data-toggle="popover"
                                                        data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                </div>
                                                <div class="text-center pt-2">
                                                    @if ($i->comments == '')
                                                        <i class="fa-solid fa-circle-xmark" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    @else
                                                        <span data-toggle="popover" data-html="true"
                                                            data-content="
                                            {{ $i->comments }}
                                    "
                                                            data-placement="top" data-trigger="hover"
                                                            class="badge-comment badge-info"><i
                                                                class="fa-solid fa-comment"></i></span>
                                                    @endif

                                                    <span data-toggle="popover" data-html="true"
                                                        data-content="
                                        <b>Estado :</b> <br />
                                        Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }} <br />
                                        Carátula - {{ __(App\AppOrgUserInventory::getCondicionName($i->cover_condition)) }} <br />
                                        Manual - {{ __(App\AppOrgUserInventory::getCondicionName($i->manual_condition)) }} <br />
                                        Estado - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }} <br />
                                        Cables - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }} <br />
                                        Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->inside_condition)) }} <br />
                                    "
                                                        data-placement="top" data-trigger="hover"
                                                        class="badge-inventory badge-info">ESTADO</span>
                                                </div>
                                            @else
                                                <div class="flex ulti ml-1">
                                                    <img height="10px" width="15px" src="/{{ $i->box }}"
                                                        data-toggle="popover"
                                                        data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->game }}" data-toggle="popover"
                                                        data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                    <img width="15px" src="/{{ $i->extra }}" data-toggle="popover"
                                                        data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }}"
                                                        data-placement="top" data-trigger="hover">
                                                    &nbsp;
                                                </div>
                                                <div class="text-center pt-2">

                                                    @if ($i->comments == '')
                                                        <i class="fa-solid fa-circle-xmark" data-toggle="popover"
                                                            data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                            data-trigger="hover"></i>
                                                    @else
                                                        <span data-toggle="popover" data-html="true"
                                                            data-content="
                                        {{ $i->comments }}
                                "
                                                            data-placement="top" data-trigger="hover"
                                                            class="badge-comment badge-info"><i
                                                                class="fa-solid fa-comment"></i></span>
                                                    @endif

                                                    <span data-toggle="popover" data-html="true"
                                                        data-content="
                                                    <b>Estado :</b> <br />
                                                    Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }} <br />
                                                    Juego - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }} <br />
                                                    Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }} <br />
                                                "
                                                        data-placement="top" data-trigger="hover"
                                                        class="badge-inventory badge-info">ESTADO</span>
                                                </div>
                                            @endif
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endisset
            </div>

            @if ($collection->isEmpty())
                <p class="text-gray-800 font-bold text-2xl text-center my-10">No existe ningún producto relacionado a
                    tu búsqueda</p>
            @endif

        </div>
        <!-- Modal -->
        <div wire:ignore.self class="modal fade" id="collectionModal" tabindex="-1" role="dialog"
            aria-labelledby="collectionModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title">Filtrar por :</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block pb-1 pt-2">

                                <form>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label">Nombre</label>
                                            <input wire:model="search" type="text" class="form-control"
                                                placeholder="Nombre del producto" autocomplete="off">
                                            <i class="clear-input">
                                                <ion-icon name="close-circle"></ion-icon>
                                            </i>
                                        </div>
                                    </div>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Plataforma</label>
                                            <div wire:ignore>
                                                <select wire:model="search_platform" style="width: 100%"
                                                    class="form-control select2platform form-select">
                                                    <option value="" selected> @lang('messages.alls')
                                                    </option>
                                                    @foreach ($platform as $p)
                                                        <option value="{{ $p->value }}">{{ $p->value }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Categoría</label>
                                            <div wire:ignore>
                                                <select wire:model="search_category" style="width: 100%"
                                                    class="form-control ultra3category form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    <optgroup label="@lang('messages.category')">
                                                        <option value="Juegos">@lang('messages.games')</option>
                                                        <option value="Consolas">@lang('messages.consoles')</option>
                                                        <option value="Periféricos">@lang('messages.peripherals')</option>
                                                        <option value="Accesorios">@lang('messages.accesories')</option>
                                                        <option value="Merchandising">Merchandising</option>
                                                    </optgroup>
                                                    <optgroup label="@lang('messages.sub_category')">
                                                        <option value="Mandos">@lang('messages.controls')</option>
                                                        <option value="Micrófonos">@lang('messages.microphones')</option>
                                                        <option value="Teclados">@lang('messages.keyboard')</option>
                                                        <option value="Fundas">@lang('messages.funded')</option>
                                                        <option value="Cables">@lang('messages.cable')</option>
                                                        <option value="Cargadores">@lang('messages.chargers')</option>
                                                        <option value="Merchandising -> Logos 3d">@lang('messages.3d_logo')
                                                        </option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Región</label>
                                            <div wire:ignore>
                                                <select wire:model="search_region" style="width: 100%"
                                                    class="form-control region2inventory form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    @foreach ($region as $r)
                                                        <option value="{{ $r->value_id }}">{{ $r->value_id }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>

            <script>
                document.addEventListener('livewire:load', function() {
                    $('.select2platform').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.ultra3category').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.region2inventory').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.country2inventory').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.language2').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.media2').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.select2platform').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_platform', this.value);
                    });
                    $('.ultra3category').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_category', this.value);
                    });
                    $('.region2inventory').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_region', this.value);
                    });
                    $('.country2inventory').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_country', this.value);
                    });
                    $('.language2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_language', this.value);
                    });
                    $('.media2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_media', this.value);
                    });
                })
                const lastCollection = document.getElementById('last_collection');
                const options = {
                    root: null,
                    threshold: 1,
                    rootMargin: '0px'
                }
                const observer = new IntersectionObserver((entries, observer) => {
                    entries.forEach(entry => {
                        if (entry.isIntersecting) {
                            @this.loadMoreCollection()
                        }
                    });
                });
                observer.observe(lastCollection);
            </script>



        </div>
    </div>
</div>
