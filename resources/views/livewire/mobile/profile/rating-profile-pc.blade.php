<div>

    <div class="page-content-wrapper-pro">
        <div class="container">

    <div class="d-md-none mb-4">
        <div class="row no-gutters align-items-center justify-content-end my-2" bis_skin_checked="1">
            <div class="col-12 pr-md-2 col-md text-center text-md-right" bis_skin_checked="1"><span
                    class="personalInfo-light">Evaluación general</span></div>
            <div class="p-title-price text-center">
                <div class="row">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="col">
                        <img src="{{ asset('img/new.png') }}" width="20px" data-toggle="popover"
                            data-content="@lang('messages.good')" data-placement="top" data-trigger="hover"><span
                            class="ml-1 mt-n1">
                            {{ number_format($sc1 * 100, 2) }} %</span>
                    </div>
                    <div class="col">
                        <img src="{{ asset('img/used.png') }}" width="20px" data-toggle="popover"
                            data-content="Neutral" data-placement="top" data-trigger="hover">
                        <span class="ml-1 mt-n1">{{ number_format($sc2 * 100, 2) }} %</span>
                    </div>
                    <div class="col">
                        <img src="{{ asset('img/no-funciona.png') }}" width="20px" data-toggle="popover"
                            data-content="@lang('messages.bad')" data-placement="top" data-trigger="hover"><span
                            class="ml-1 mt-n1">
                            {{ number_format($sc3 * 100, 2) }} %</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row no-gutters align-items-center justify-content-end my-2" bis_skin_checked="1">
            <div class="col-12 pr-md-2 col-md text-center text-md-right" bis_skin_checked="1"><span
                    class="personalInfo-light">Empaquetado</span></div>
            <div class="p-title-price text-center">
                <div class="row">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="col">
                        <img src="{{ asset('img/new.png') }}" width="20px" data-toggle="popover"
                            data-content="@lang('messages.good')" data-placement="top" data-trigger="hover"><span
                            class="ml-1 mt-n1">
                            {{ number_format($sc1 * 100, 2) }} %</span>
                    </div>
                    <div class="col">
                        <img src="{{ asset('img/used.png') }}" width="20px" data-toggle="popover"
                            data-content="Neutral" data-placement="top" data-trigger="hover">
                        <span class="ml-1 mt-n1">{{ number_format($sc2 * 100, 2) }} %</span>
                    </div>
                    <div class="col">
                        <img src="{{ asset('img/no-funciona.png') }}" width="20px" data-toggle="popover"
                            data-content="@lang('messages.bad')" data-placement="top" data-trigger="hover"><span
                            class="ml-1 mt-n1">
                            {{ number_format($sc3 * 100, 2) }} %</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row no-gutters align-items-center justify-content-end my-2" bis_skin_checked="1">
            <div class="col-12 pr-md-2 col-md text-center text-md-right" bis_skin_checked="1"><span
                    class="personalInfo-light">Descripción del Producto</span></div>
            <div class="p-title-price text-center">
                <div class="row">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="col">
                        <img src="{{ asset('img/new.png') }}" width="20px" data-toggle="popover"
                            data-content="@lang('messages.good')" data-placement="top" data-trigger="hover"><span
                            class="ml-1 mt-n1">
                            {{ number_format($sc1 * 100, 2) }} %</span>
                    </div>
                    <div class="col">
                        <img src="{{ asset('img/used.png') }}" width="20px" data-toggle="popover"
                            data-content="Neutral" data-placement="top" data-trigger="hover">
                        <span class="ml-1 mt-n1">{{ number_format($sc2 * 100, 2) }} %</span>
                    </div>
                    <div class="col">
                        <img src="{{ asset('img/no-funciona.png') }}" width="20px" data-toggle="popover"
                            data-content="@lang('messages.bad')" data-placement="top" data-trigger="hover"><span
                            class="ml-1 mt-n1">
                            {{ number_format($sc3 * 100, 2) }} %</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

 
        <div class="card-body">
            <table class="table mb-0">
                <thead>
                    <tr>
                        <th scope="col">Usuario</th>
                        <th scope="col">Nota General</th>
                        <th scope="col">Ver más</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rating as $key)
                        <tr>
                            <td>
                                @if (App\SysUserRoles::where('user_id', $key->buyer->id)->whereIn('role_id', [2])->count() > 0)
                                    <a href="{{ route('user-info', $key->buyer->user_name) }}" target="_blank"
                                        style="color: #000000">{{ $key->buyer->user_name }}</a>
                                @else
                                    <a href="{{ route('user-info', $key->buyer->user_name) }}" target="_blank"
                                        style="color: #000000">{{ $key->buyer->user_name }}</a>
                                    @endif <span class="flag-icon flag-icon-esp"></span> <br>
                                    {{ date('Y-m-d', strtotime($key->created_at)) }}
                            </td>
                            <td wire:ignore>

                                {{--
                                {{ $key->packaging }}

                                {{$key->processig}}

                                {{$key->desc_prod}}

                                --}}

                                {{-- CARA VERDE --}}

                                @if($key->packaging == 1  and $key->processig == 1 and $key->desc_prod == 1)
                                <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Bueno" data-placement="top"
                                data-trigger="hover">
                                @elseif($key->packaging == 2  and $key->processig == 1 and $key->desc_prod == 1)
                                <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Bueno" data-placement="top"
                                data-trigger="hover">
                                @elseif($key->packaging == 1  and $key->processig == 2 and $key->desc_prod == 1)
                                <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Bueno" data-placement="top"
                                data-trigger="hover">
                                @elseif($key->packaging == 1  and $key->processig == 1 and $key->desc_prod == 2)
                                <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Bueno" data-placement="top"
                                data-trigger="hover">
                                
                                {{-- CARA AMARILLA --}}

                                @elseif($key->packaging == 3  and $key->processig == 1 and $key->desc_prod == 1)
                                <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 1  and $key->processig == 3 and $key->desc_prod == 1)
                                <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 1  and $key->processig == 1 and $key->desc_prod == 3)
                                <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 2  and $key->processig == 2 and $key->desc_prod == 1)
                                <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 1  and $key->processig == 2 and $key->desc_prod == 2)
                                <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 2  and $key->processig == 1 and $key->desc_prod == 2)
                                <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 1  and $key->processig == 2 and $key->desc_prod == 3)
                                <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 2  and $key->processig == 1 and $key->desc_prod == 3)
                                <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 3  and $key->processig == 2 and $key->desc_prod == 1)
                                <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 1  and $key->processig == 3 and $key->desc_prod == 2)
                                <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 1  and $key->processig == 3 and $key->desc_prod == 2)
                                <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 2  and $key->processig == 2 and $key->desc_prod == 2)
                                <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 2  and $key->processig == 3 and $key->desc_prod == 2)
                                <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 3  and $key->processig == 2 and $key->desc_prod == 2)
                                <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 2  and $key->processig == 2 and $key->desc_prod == 3)
                                <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 3  and $key->processig == 2 and $key->desc_prod == 2)
                                <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                data-trigger="hover">

                                {{-- CARA ROJA --}}

                                @elseif($key->packaging == 3  and $key->processig == 3 and $key->desc_prod == 3)
                                <img src="{{ asset('img/no-funciona.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Malo" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 3  and $key->processig == 3 and $key->desc_prod == 2)
                                <img src="{{ asset('img/no-funciona.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Malo" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 3  and $key->processig == 2 and $key->desc_prod == 3)
                                <img src="{{ asset('img/no-funciona.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Malo" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 2  and $key->processig == 3 and $key->desc_prod == 3)
                                <img src="{{ asset('img/no-funciona.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Malo" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 1  and $key->processig == 3 and $key->desc_prod == 3)
                                <img src="{{ asset('img/no-funciona.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Malo" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 3  and $key->processig == 3 and $key->desc_prod == 1)
                                <img src="{{ asset('img/no-funciona.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Malo" data-placement="top"
                                data-trigger="hover">

                                @elseif($key->packaging == 3  and $key->processig == 1 and $key->desc_prod == 3)
                                <img src="{{ asset('img/no-funciona.png') }}" width="17" height="16"
                                data-toggle="popover" data-content="Malo" data-placement="top"
                                data-trigger="hover">


                                @endif
                               
                            </td>
                            <td>
                                <button type="button" class="btn btn-icon btn-danger"
                                    wire:click="viewDetails({{ $key->id }})">
                                    <ion-icon wire:ignore name="eye-outline"></ion-icon>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
 

        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="viewDetails" tabindex="-1" data-backdrop="static"
        data-keyboard="false" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Informacion detallada</h5>
                    <button class="btn btn-close p-1 ms-auto me-0" class="close" data-bs-dismiss="modal"
                    aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th>Comprador: </th>
                                <td>{{ $view_rating_name }} <span class="flag-icon flag-icon-esp"></span>
                                    <br>
                                    {{ date('Y-m-d', strtotime($view_rating_date)) }}
                                </td>
                            </tr>


                            <tr>
                                <th>Comentario: </th>
                                <td>{{ $view_rating_description }}</td>
                            </tr>


                            <tr>
                                <th>Velocidad de Procesamiento: </th>
                                <td>
                                    @if ($view_rating_processig == 1)
                                        <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                            data-toggle="popover" data-content="Bueno" data-placement="top"
                                            data-trigger="hover">
                                    @elseif ($view_rating_processig == 2)
                                        <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                            data-toggle="popover" data-content="Neutral" data-placement="top"
                                            data-trigger="hover">
                                    @else
                                        <img src="{{ asset('img/no-funciona.png') }}" width="17" height="16"
                                            data-toggle="popover" data-content="Malo" data-placement="top"
                                            data-trigger="hover">
                                    @endif
                                </td>
                            </tr>

                            <tr>
                                <th>Empaquetado: </th>
                                <td>
                                    @if ($view_rating_packaging == 1)
                                        <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                            data-toggle="popover" data-content="Bueno" data-placement="top"
                                            data-trigger="hover">
                                    @elseif ($view_rating_packaging == 2)
                                        <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                            data-toggle="popover" data-content="Neutral" data-placement="top"
                                            data-trigger="hover">
                                    @else
                                        <img src="{{ asset('img/no-funciona.png') }}" width="17" height="16"
                                            data-toggle="popover" data-content="Malo" data-placement="top"
                                            data-trigger="hover">
                                    @endif
                                </td>
                            </tr>

                            <tr>
                                <th>Descripcion del Producto: </th>
                                <td>
                                    @if ($view_rating_desc_prod == 1)
                                        <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                            data-toggle="popover" data-content="Bueno" data-placement="top"
                                            data-trigger="hover">
                                    @elseif ($view_rating_desc_prod == 2)
                                        <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                            data-toggle="popover" data-content="Neutral" data-placement="top"
                                            data-trigger="hover">
                                    @else
                                        <img src="{{ asset('img/no-funciona.png') }}" width="17" height="16"
                                            data-toggle="popover" data-content="Malo" data-placement="top"
                                            data-trigger="hover">
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
