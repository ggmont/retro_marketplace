<div>
    <div class="top-products-area clearfix py-3" style="margin-bottom: 20px;">
        @if ($query !== '' || $search_platform !== '' || $search_category !== '' || $search_region !== '')
            <button wire:click="clear_inventory" type="button"
                class="filterToggleDelete d-lg-none btn btn-lg btn-danger btn-rounded btn-absolute">
                <span class="fa-solid fa-trash"></span>
            </button>
        @endif
        <button type="button" class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed"
            data-toggle="modal" data-target="#filterModal">
            <span class="fa-solid fa-filter"></span>
        </button>
        <div class="container">
            <div wire:loading.delay.class="opacity-50" class="row g-3">
                <!-- Single Top Product Card-->
                @isset($seller)
                    @foreach ($seller as $i)
                        <div @if ($loop->last) id="last_record_inventory" @endif
                            class="col-6 col-md-4 col-lg-3">
                            <div class="card top-product-card">
                                <div class="card-body">
                                    <a href="{{ route('product-inventory-show', $i->id) }}">
                                        <div class="figure">
                                            <a href="{{ route('product-inventory-show', $i->id) }}">
                                                @if ($i && $i->images && $i->images->first())
                                                    @if (str_starts_with($i->images->first()->image_path, 'https'))
                                                        <img class="card-product-image-holder image-main"
                                                            src="{{ $i->images->first()->image_path }}"
                                                            alt="{{ $i->title }} - RetroGamingMarket">
                                                    @elseif ($i->product_id > 0)
                                                        <img class="card-product-image-holder image-main"
                                                            src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                            alt="{{ $i->product->name }} - RetroGamingMarket">
                                                    @else
                                                        <img class="card-product-image-holder image-main"
                                                            src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                            alt="{{ $i->title }} - RetroGamingMarket">
                                                    @endif
                                                @elseif ($i && $i->product && $i->product->images && $i->product->images->first())
                                                    <img class="card-product-image-holder image-main"
                                                        src="{{ url('/images/' . $i->product->images->first()->image_path) }}"
                                                        alt="{{ $i->product->name }} - RetroGamingMarket">
                                                @else
                                                    <img loading="lazy" class="card-product-image-holder image-main"
                                                        src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                        alt="Aqui esta el error RetroGamingMarket">
                                                @endif
                                            </a>
                                        </div>


                                        <span class="text-sm font-bold sicker">
                                            <a href="{{ route('user-info', $i->user->user_name) }}">
                                                <font color="white">
                                                    {{ str_limit($i->user->user_name, 10) }} - @include('partials.pais_user_home')
                                                </font>
                                            </a>
                                        </span>

                                        @if ($i->title !== null)
                                            <span class="product-title ml-1">{{ str_limit($i->title, 16) }}</span>
                                            <br>
                                            <br>
                                            <br>
                                        @else
                                            @if ($i->product && $i->product->name)
                                                <span
                                                    class="product-title ml-1">{{ str_limit($i->product->name, 16) }}</span>
                                                <br>
                                                <span
                                                    class="progress-title ml-1">{{ optional($i->product)->platform }}</span>
                                                <br>
                                                <span
                                                    class="progress-title ml-1">{{ optional($i->product)->region }}</span>
                                            @else
                                                <!-- Aquí puedes manejar el caso en el que $i->product o $i->product->name no existan -->
                                            @endif
                                        @endif
                                        <hr style='margin-top:0.5em; margin-bottom:0.5em' />
                                        @if ($i->auction_type > 0)
                                            <div wire:ignore class="flex items-center">
                                                <p
                                                    class="sale-price ml-1 bg-gradient-to-r from-red-500 to-red-700 text-white px-2 py-1 rounded">
                                                    {{ number_format($i->max_bid, 2) . ' €' }}
                                                </p>
                                                <div class="ml-auto mb-1 flex items-center">
                                                    <ion-icon size="small" class="mr-1 text-gray-900" data-toggle="popover"
                                                        data-content="{{ $i->countdown_hours }} horas" data-placement="auto" data-offset="0,10"
                                                        data-trigger="hover" name="timer-outline">
                                                    </ion-icon>
                                                    <!-- Icono de reloj de Ionicons con mensaje de 24 horas -->
                                                    <ion-icon size="small" class="mr-1 text-gray-900"
                                                        data-toggle="popover"
                                                        data-content="Envío certificado con seguro incluído desde 2,90€"
                                                        data-placement="auto" data-offset="0,10" data-trigger="hover"
                                                        name="paper-plane">
                                                    </ion-icon>
                                                    <!-- Icono de envío de Ionicons -->
                                                    <ion-icon size="small" class="mr-1 text-gray-900"
                                                        data-toggle="popover" data-html="true"
                                                        data-content="Pago seguro con Beseif. <a href='https://www.retrogamingmarket.eu/site/gastos-de-envio'>Más Información</a>"
                                                        data-placement="auto" data-offset="0,10" data-trigger="hover"
                                                        name="shield-checkmark">
                                                    </ion-icon>
                                                    <!-- Icono de seguridad de Font Awesome -->
                                                </div>
                                            </div>
                                        @else
                                            <div wire:ignore class="flex items-center">
                                                <p class="sale-price ml-1">{{ number_format($i->price, 2) . ' €' }}</p>
                                                <div class="ml-auto mb-1 flex items-center">
                                                    <ion-icon size="small" class="mr-1 text-gray-900"
                                                        data-toggle="popover"
                                                        data-content="Envío certificado con seguro incluído desde 2,90€"
                                                        data-placement="auto" data-offset="0,10" data-trigger="hover"
                                                        name="paper-plane">
                                                    </ion-icon>
                                                    <!-- Icono de envío de Ionicons -->
                                                    <ion-icon size="small" class="mr-1 text-gray-900"
                                                        data-toggle="popover" data-html="true"
                                                        data-content="Pago seguro con Beseif. <a href='https://www.retrogamingmarket.eu/site/gastos-de-envio'>Más Información</a>"
                                                        data-placement="auto" data-offset="0,10" data-trigger="hover"
                                                        name="shield-checkmark">
                                                    </ion-icon>
                                                    <!-- Icono de seguridad de Font Awesome -->
                                                </div>
                                            </div>
                                        @endif
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endisset
            </div>

            @if ($seller->isEmpty())
                <p class="text-gray-800 font-bold text-2xl text-center my-10">No existe ningún producto relacionado a
                    tu búsqueda</p>
            @endif

            @if ($query !== '' || $search_platform !== '' || $search_category !== '' || $search_region !== '')
                <p class="text-gray-800 font-bold text-2xl text-center my-10">No existe ningún producto relacionado a tu
                    búsqueda en este perfil</p>
            @endif

        </div>
        <br><br><br><br><br>
        <!-- Modal -->
        <div wire:ignore.self class="modal fade" id="filterModal" tabindex="-1" role="dialog"
            aria-labelledby="filterModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h4 class="modal-title">Filtrar por :</h4>
                                <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="wide-block pb-1 pt-2">

                                <form>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Tipo de ventas</label>

                                            <select wire:model="search_sales" style="width: 100%" id="search_sales"
                                                class="form-control form-select">
                                                <option value="" selected> @lang('messages.alls')
                                                </option>
                                                <option value="0">Precio fijo
                                                </option>
                                                <option value="1">Ofertas
                                                </option>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label">Nombre</label>
                                            <input wire:model.debounce.300ms="query" type="text"
                                                class="form-control" placeholder="Nombre del producto"
                                                autocomplete="off">
                                            <i class="clear-input">
                                                <ion-icon name="close-circle"></ion-icon>
                                            </i>
                                        </div>
                                    </div>

                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Plataforma</label>
                                            <div wire:ignore>
                                                <select wire:model="search_platform" style="width: 100%"
                                                    class="form-control select2platform form-select">
                                                    <option value="" selected> @lang('messages.alls')
                                                    </option>
                                                    @foreach ($platform as $p)
                                                        <option value="{{ $p->value }}">{{ $p->value }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Categoría</label>
                                            <div wire:ignore>
                                                <select wire:model="search_category" style="width: 100%"
                                                    class="form-control ultra3category form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    <optgroup label="@lang('messages.category')">
                                                        <option value="Juegos">@lang('messages.games')</option>
                                                        <option value="Consolas">@lang('messages.consoles')</option>
                                                        <option value="Periféricos">@lang('messages.peripherals')</option>
                                                        <option value="Accesorios">@lang('messages.accesories')</option>
                                                        <option value="Merchandising">Merchandising</option>
                                                    </optgroup>
                                                    <optgroup label="@lang('messages.sub_category')">
                                                        <option value="Mandos">@lang('messages.controls')</option>
                                                        <option value="Micrófonos">@lang('messages.microphones')</option>
                                                        <option value="Teclados">@lang('messages.keyboard')</option>
                                                        <option value="Fundas">@lang('messages.funded')</option>
                                                        <option value="Cables">@lang('messages.cable')</option>
                                                        <option value="Cargadores">@lang('messages.chargers')</option>
                                                        <option value="Merchandising -> Logos 3d">@lang('messages.3d_logo')
                                                        </option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="city5">Región</label>
                                            <div wire:ignore>
                                                <select wire:model="search_region" style="width: 100%"
                                                    class="form-control region2inventory form-select">
                                                    <option value="" selected>@lang('messages.alls')
                                                    </option>
                                                    @foreach ($region as $r)
                                                        <option value="{{ $r->value_id }}">{{ $r->value_id }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>

            <script>
                document.addEventListener('livewire:load', function() {
                    $('.select2platform').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.ultra3category').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.region2inventory').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.country2inventory').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.language2').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.media2').select2({
                        dropdownParent: $('#filterModal')
                    });
                    $('.select2platform').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_platform', this.value);
                    });
                    $('.ultra3category').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_category', this.value);
                    });
                    $('.region2inventory').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_region', this.value);
                    });
                    $('.country2inventory').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_country', this.value);
                    });
                    $('.language2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_language', this.value);
                    });
                    $('.media2').on('change', function() {
                        /*  alert(this.value) */
                        @this.set('search_media', this.value);
                    });
                })
                const lastRecordInventory = document.getElementById('last_record_inventory');
                const observerOptions = {
                    root: null,
                    threshold: 1,
                    rootMargin: '0px'
                }

                const observerInstance = new IntersectionObserver((entries, observerInstance) => {
                    entries.forEach(entry => {
                        if (entry.isIntersecting) {
                            @this.loadMoreInventory()
                        }
                    });
                }, observerOptions);

                observerInstance.observe(lastRecordInventory);
            </script>



        </div>
    </div>
</div>
