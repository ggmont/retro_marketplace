<div>
    <div class="page-content-wrapper py-3">
        <div class="container">
            <div class="listview-title mt-2">@lang('messages.information_basic')</div>

          

            <ul class="listview image-listview">
                <li>
                    <a wire:click.prevent="editCash({{ auth()->user()->id }})" class="item">

                        <div class="in">
                            <div>
                                <header>@lang('messages.cash_available')</header>
                                {{ auth()->user()->cash }} €
                            </div>
                            <span class="text-muted">@lang('messages.remove')</span>
                        </div>
                    </a>
                </li>


            </ul>
            <div class="listview-title mt-2">@lang('messages.data_bank')</div>
            <ul class="listview image-listview">
                <li>
                    <a wire:click.prevent="editBank({{ auth()->user()->id }})" class="item">

                        <div class="in">
                            <div>
                                <header>@lang('messages.titular_name')</header>
                                {{ auth()->user()->bank->beneficiary }}
                            </div>
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a wire:click.prevent="editIban({{ auth()->user()->id }})" class="item">

                        <div class="in">
                            <div>
                                <header>IBAN</header>
                                {{ auth()->user()->bank->iban_code }}
                            </div>
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a wire:click.prevent="editBicCode({{ auth()->user()->id }})" class="item">
                        <div class="in">
                            <div>
                                <header>BIC</header>
                                {{ auth()->user()->bank->bic_code }}
                            </div>
                            <span class="text-muted">@lang('messages.the_edit')</span>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="cashModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title">@lang('messages.withdraw_cash')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form action="{{ route('withdrawals_amount') }}" method="post">
                                {{ csrf_field() }}

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="cash">@lang('messages.sure_bank_correct')</label>
                                        <input type="text" name="credit" wire:model="cash"
                                            class="form-control @error('cash') is-invalid @enderror" id="cash"
                                            aria-describedby="cash" placeholder="100€">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>

                                </div>
                                &nbsp;&nbsp; <span class="font-bold">Max {{ auth()->user()->cash }} €</span>

                                <button type="submit" class="btn btn-danger font-bold w-100 close-modal"
                                    data-dismiss="modal">@lang('messages.remove')</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="bankModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.the_edit') @lang('messages.the_titular')</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="email5">@lang('messages.titular_name')</label>
                                        <input type="text" wire:model="beneficiary"
                                            class="form-control @error('beneficiary') is-invalid @enderror"
                                            id="beneficiary" aria-describedby="beneficiary" placeholder="beneficiary">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updateBank()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="ibanModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title" id="addnewcontactlabel">@lang('messages.the_edit') IBAN</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="iban_code">IBAN</label>
                                        <input type="text" wire:model="iban_code"
                                            class="form-control @error('iban_code') is-invalid @enderror" id="iban_code"
                                            aria-describedby="iban_code" placeholder="IBAN">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updateIban()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="bicModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <h4 class="modal-title">@lang('messages.the_edit') BIC</h4>
                            <button class="btn btn-close p-1 ms-auto me-0" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="wide-block pb-1 pt-2">
                            <form>

                                <div class="form-group boxed">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="bic_code">BIC</label>
                                        <input type="text" wire:model="bic_code"
                                            class="form-control @error('bic_code') is-invalid @enderror" id="bic_code"
                                            aria-describedby="bic_code" placeholder="BIC">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" wire:click.prevent="updateBicCode()"
                            class="btn confirmclosed btn-danger font-bold w-100 close-modal"
                            data-dismiss="modal">@lang('messages.updates')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
