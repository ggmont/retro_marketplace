@extends('brcode.front.layout.app')
@section('content-css-include')
    <style>
        .tab {
            overflow: hidden;
        }

        .tab-content {
            max-height: 0;
            transition: all 0.5s;
        }

        input:checked+.tab-label .test {
            background-color: #000;
        }

        input:checked+.tab-label .test svg {
            transform: rotate(180deg);
            stroke: #fff;
        }

        input:checked+.tab-label::after {
            transform: rotate(90deg);
        }

        input:checked~.tab-content {
            max-height: 100vh;
        }

        .demo {
            margin: 30px auto;
            max-width: 960px;
        }

        .demo>li {
            float: left;
        }

        .demo>li img {
            width: 220px;
            margin: 10px;
            cursor: pointer;
        }

        .item {
            transition: .5s ease-in-out;
        }

        .item:hover {
            filter: brightness(80%);
        }

        .retro {
            font-family: 'Jost', sans-serif;
            font-weight: 800;
        }

        .w-88 {
            width: 99% !important;
        }

    </style>
@endsection
@section('content')
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
    <div class="top-products-area clearfix py-3" style="margin-bottom: 47.2188px; padding-bottom: 5rem;">
            <!-- START: Breadcrumbs -->
            <div class="header-area" id="headerArea">
                <div class="container h-100 d-flex align-items-center justify-content-between">
                    <!-- Back Button-->
                    <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
                    <!-- Page Title-->
                    <div class="page-heading">
                        <h6 class="mb-0 font-extrabold">Detalles del Producto</h6>
                    </div>
                    <!-- Navbar Toggler-->

                    @if (Auth::user())
                    <div class="suha-navbar-toggler d-flex justify-content-between flex-wrap" id="sidenavWrapper">
                        <span></span><span></span><span></span>
                    </div>
                @else
                    <a href="#" id="showLogin" class="showLogin" class="lni lni-user d-flex flex-wrap"
                        data-nav-toggle="#nk-nav-mobile">
                        <span class="lni lni-user">
                            <span></span><span></span><span></span>
                        </span>
                    </a>
                @endif
                </div>
            </div>
            <!-- END: Breadcrumbs -->
            <div class="product-slides owl-carousel">

                @if (count($viewData['product_images']) > 0)
                    <img class="single-product-slide" loading="lazy"
                        src="/images/{{ $viewData['product_images'][0]->image_path }}" alt="">
                    @if (count($viewData['product_images']) > 1)
                        @foreach ($viewData['product_images'] as $key)
                            @if ($viewData['product_images'][0]->id != $key->id)
                                <img class="single-product-slide" loading="lazy" src="/images/{{ $key->image_path }}"
                                    alt="">
                            @endif
                        @endforeach
                    @endif
                @else
                    <img class="single-product-slide" loading="lazy" src="{{ asset('assets/images/art-not-found.jpg') }}"
                        alt="">
                @endif




            </div>



            <div class="product-description">
 
                <!-- Product Specification-->
                <div class="p-specification bg-white mb-3 py-3">
                    <div class="container">
                        <div class="p-title-price text-center">
                            <h5 class="mb-1 font-extrabold">{{ $producto->name }}</h5>
                            <br>

                            <h5 class="mb-1 font-extrabold">- Precios Generales -</h5>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <div class="total-result-of-ratings"><span>-</span><span>Nuevo</span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="total-result-of-ratings-used"><span>-</span><span>Usado</span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="total-result-of-ratings-solo"><span>-</span><span>Solo</span>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                        <h6>Detalles</h6>
                        <div class="nk-product-meta">
                            <div class="lists">
                                <ul class="nes-list is-disc">
                                    @if ($producto->comments == '')
                                    @else
                                        <li>
                                            {{ $producto->comments }}
                                        </li>
                                    @endif
                                    <li>
                                        {{ __('Plataforma') }}:
                                        {{ $producto->platform }}
                                    </li>
                                    <li>
                                        {{ __('Región') }}:
                                        {{ $producto->region }}
                                    </li>
                                    @if ($producto->media)
                                        <li>
                                            {{ __('Media') }}:
                                            {{ $producto->media }}
                                        </li>
                                    @endif
                                    @if ($producto->language == '')
                                    @else
                                        <li>
                                            {{ __('Idioma') }}:
                                            {{ $producto->language }}
                                        </li>
                                    @endif
                                    @if ($producto->box_language == '')
                                    @else
                                        <li>
                                            {{ __('Idioma de Caja') }}:
                                            {{ $producto->box_language }}
                                        </li>
                                    @endif
                                </ul>
                            </div>


                            <div class="nk-gap"></div>
                            <a class="btn btn-danger ms-3" href="/offerproduct/{{ $producto->id + 1000 }}"><span
                                    class="retro font-extrabold">@lang('messages.sell_similar')</span></a>
                        </div>
                    </div>
                </div>


  
  
 

            </div>
            
            <div class="rating-and-review-wrapper bg-white py-3 mb-3">
                <!-- Button trigger modal -->
    
                @if (count($viewData['inventory']) > 0)
                   @if ($producto->catid == 1)
                   @livewire('mobile.mobile-inventory-game-table',['producto' => $producto])
                   @elseif ($producto->catid == 2)
                   @livewire('mobile.mobile-inventory-console',['producto' => $producto])
                   @else
                   @livewire('mobile.mobile-inventory-accesories',['producto' => $producto])
                   @endif
                @else
                <div class="row">
                    <div class="col-lg-12">
                        <blockquote class="nk-blockquote">
                            <div class="nk-blockquote-icon"></div>
                            <div class="text-center nk-blockquote-content h4">
                                <font color="black">
                                    {{ __('No hay inventario disponible para este producto') }}
                                </font>
                            </div>
                            <div class="nk-gap"></div>
                            <div class="nk-blockquote-author"></div>
                        </blockquote>
                    </div>
                </div>
                @endif
            </div>


        </div>
        

         
    @else
        <div class="nk-main">
            <!-- START: Breadcrumbs -->
            <div class="nk-gap-1"></div>
            <div class="container">
                <ul class="nk-breadcrumbs">
                    <li>

                        <div class="section-title1">
                            <h3 class="retro">@lang('messages.information_product')</h3>
                        </div>

                    </li>
                </ul>
            </div>
            <div class="nk-gap-3"></div>

            <!-- END: Breadcrumbs -->
            <div class="container bg-white">
                <div class="row vertical-gap">
                    <div class="col-lg-7">
                        <div class="nk-store-product">
                            <div class="row vertical-gap">
                                <div class="col-md-4 item">
                                    @if (count($viewData['product_images']) > 0)
                                        <a href="/images/{{ $viewData['product_images'][0]->image_path }}"
                                            class="fancybox" data-fancybox="RGM1">
                                            <img src="/images/{{ $viewData['product_images'][0]->image_path }}"
                                                width="100%" height="100%" />
                                        </a>
                                        @if (count($viewData['product_images']) > 1)
                                            <div class="nk-gap-1"></div>
                                            <div class="row vertical-gap sm-gap">
                                                @foreach ($viewData['product_images'] as $key)
                                                    @if ($viewData['product_images'][0]->id != $key->id)
                                                        <div class="col-6 col-md-4">
                                                            <a href="/images/{{ $key->image_path }}"
                                                                class="fancybox" data-fancybox="RGM1">
                                                                <img src="/images/{{ $key->image_path }}" width="100%"
                                                                    height="100%" />
                                                            </a>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        @endif
                                    @else
                                        <a href="{{ url('assets/images/art-not-found.jpg') }}" class="fancybox"
                                            data-fancybox="RGM1">
                                            <img src="{{ url('assets/images/art-not-found.jpg') }}" width="100%"
                                                height="100%" />
                                        </a>
                                    @endif
                                </div>
                                <div class="col-md-6">

                                    <h2 class="nk-product-title h3 retro">
                                        <font color="black">{{ $producto->name_en }}</font>
                                    </h2>
                                    <div class="nk-product-meta">
                                        <div class="lists">
                                            <ul class="nes-list is-disc">
                                                @if ($producto->comments == '')
                                                @else
                                                    <li><b>
                                                            <font color="black">
                                                                {{ $producto->comments }}</font>
                                                        </b></li>
                                                @endif
                                                <li><b>
                                                        <font color="black">{{ __('Fecha de lanzamiento') }}:
                                                            {{ $producto->release_date }}</font>
                                                    </b></li>
                                                <li><b>
                                                        <font color="black">{{ __('Plataforma') }}:
                                                            {{ $producto->platform }}</font>
                                                    </b></li>
                                                <li><b>
                                                        <font color="black">{{ __('Región') }}:
                                                            {{ $producto->region }}</font>
                                                    </b></li>
                                                @if ($producto->volume)
                                                    <li><b>
                                                            <font color="black">{{ __('Volumen') }}:
                                                                {{ $producto->volume }}</font>
                                                        </b></li>
                                                @endif
                                                @if ($producto->weight)
                                                    <li><b>
                                                            <font color="black">{{ __('Peso') }}:
                                                                {{ $producto->weight }}</font>
                                                        </b></li>
                                                @endif
                                                @if ($producto->media)
                                                    <li><b>
                                                            <font color="black">{{ __('Media') }}:
                                                                {{ $producto->media }}</font>
                                                        </b></li>
                                                @endif
                                                @if ($producto->language == '')
                                                @else
                                                    <li><b>
                                                            <font color="black">{{ __('Idioma') }}:
                                                                {{ $producto->language }}</font>
                                                        </b></li>
                                                @endif
                                                @if ($producto->box_language == '')
                                                @else
                                                    <li><b>
                                                            <font color="black">{{ __('Idioma de Caja') }}:
                                                                {{ $producto->box_language }}</font>
                                                        </b></li>
                                                @endif
                                            </ul>
                                        </div>


                                        <div class="nk-gap"></div>
                                        <a class="nes-btn is-error" href="/offerproduct/{{ $producto->id + 1000 }}"><span
                                                class="retro">@lang('messages.sell_similar')</span></a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <aside class="nk-sidebar nk-sidebar-right nk-sidebar-sticky">
                        @if (Auth::user())
                            <?php
                            $l = $producto->price_solo + $producto->price_used + $producto->price_new / 3;
                            ?>
                            @if (count($chart_sold) > 0)
                                <center><small class="retro"> - @lang('messages.selects') -</small></center>
                                @livewire('product-price',['producto' => $producto])
                            @elseif($l > 0)
                                <div class="nes-container with-title is-centered">
                                    <p class="title retro text-gray-900">GENERAL</p>
                                    <p class="text-center text-lg retro">{{ number_format($l, 2) }} €</p>
                                </div>
                                <div class="col-md-12">
                                    @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                        <div class="mt-10">
                                        @else
                                            <div class="d-flex justify-content-between mt-10">
                                    @endif
                                    <button class="nk-btn nk-btn-outline nk-btn-color-danger retro">Solo <br> <span
                                            class="muyusado">
                                            {{ number_format($producto->price_solo, 2) }} €
                                        </span></button>
                                    <button class="nk-btn nk-btn-outline nk-btn-color-warning retro">Usado <br> <span
                                            class="usado">
                                            {{ number_format($producto->price_used, 2) }}
                                        </span></button>

                                    <button class="nk-btn nk-btn-outline nk-btn-color-success retro">Nuevo <br> <span
                                            class="nuevo">
                                            {{ number_format($producto->price_new, 2) }}
                                        </span></button>

                                </div>
                </div>
    @endif
    @endif
    <div class="nk-widget">
        <div class="nk-widget-content">
            @if (Auth::user())
                <br>
                <div class="social-share">
                    @if ((new \Jenssegers\Agent\Agent())->isMobile())
                    @else
                        <h4 class="text-md retro">
                            <font color="black">
                                &nbsp;&nbsp;&nbsp;@lang('messages.share_this_product')&nbsp;&nbsp;&nbsp;&nbsp;
                            </font>
                        </h4>
                    @endif
                    <center>
                        <ul class="socil-icon2">
                            <li><a href="#" id="facebook-btn" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" id="twitter-btn" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" id="whatsapp-btn" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                            <li><a href="#" id="linkedin-btn" target="_blank"><i class="fa fa-linkedin-square"></i></a>
                            </li>
                            <li><a href="#" id="gmail-btn" target="_blank"><i class="fa fa-envelope-o"></i></a></li>
                        </ul>
                    </center>
                </div>
            @endif

        </div>
        <div class="nk-gap-3"></div>
    </div>
    </aside>
    </div>
    </div>

    </div>





    <div class="container">

        <!-- Content Header (Page header) -->
        @if (count($viewData['inventory']) > 0)
            <div class="table-responsive-md">
                <table class="nk-table">

                    <tbody>

                        @php( $sellers = [])
                        @php( $title_game = [
                            'NEW' => "Nuevo",
                            'USED-NEW' => "Usado como nuevo",
                            'USED' => "Usado",
                            'USED-VERY' => "Muy usado" ,
                            'NOT-WORK' => "No funciona",
                            'NOT-PRES' => "No aplica"
                        ])
    
                        @php( $country = [
                            'Spain' => "España",
                            'Andorra' => "Andorra",
                             'Åland Islands' => "Åland Islands",
                             'Albania' => "Albania",
                             'Austria' => "Austria",
                             'Belarus' => "Belarus",
                             'Belgium' => "Belgium",
                             'Bosnia and Herzegovina' => "Bosnia and Herzegovina",
                             'Bulgaria' => "Bulgaria",
                             'Croatia' => "Croatia",
                             'Czech Republic' => "Czech Republic",
                             'Denmark' => "Denmark",
                             'Estonia' => "Estonia",
                             'Faroe Islands' => "Faroe Islands",
                              'Finland' => "Finland",
                              'France' => "France",
                              'Germany' => "Germany",
                              'Gibraltar' => "Gibraltar",
                              'Guernsey' => "Guernsey",
                              'Holy See' => "Holy See",
                              'Hungary' => "Hungary",
                              'Iceland' => "Iceland",
                              'Ireland' => "Ireland",
                              'Isle of Man' => "Isle of Man",
                              'Italy' => "Italy",
                              'Jersey' => "Jersey",
                              'Latvia' => "Latvia",
                              'Liechtenstein' => "Liechtenstein",
                              'Lithuania' => "Lithuania",
                              'Luxembourg' => "Luxembourg",
                              'Macedonia (the former Yugoslav Republic of)' => "Macedonia (the former Yugoslav Republic of)",
                              'Malta' => "Malta",
                              'Moldova (Republic of)' => "Moldova (Republic of)",
                              'Monaco' => "Monaco",
                              'Montenegro' => "Montenegro",
                              'Netherlands' => "Netherlands",
                              'Norway' => "Norway",
                              'Poland' => "Poland",
                              'Portugal' => "Portugal",
                              'Romania' => "Romania",
                              'Russian Federation' => "Russian Federation",
                              'San Marino' => "San Marino",
                              'Serbia' => "Serbia",
                              'Slovakia' => "Slovakia",
                              'Slovenia' => "Slovenia",
                              'Svalbard and Jan Mayen' => "Svalbard and Jan Mayen",
                              'Sweden' => "Sweden",
                              'Switzerland' => "Switzerland",
                              'Ukraine' => "Ukraine",
                              'United Kingdom of Great Britain and Northern Ireland' => "United Kingdom of Great Britain and Northern Ireland"
                        ])
    
                        @php( $game_conditions = [])
                        @php( $box_conditions = [])
                        @php( $countrys = [])
                        @php( $manual_conditions = [])
                        @php( $extra_conditions = [])
                        @php( $cover_conditions = [])
                        @php( $inside_conditions = [])
                        @if(count($viewData['inventory']) > 0)
                        @foreach($viewData['inventory'] as $row)
    
                        @php ( ! in_array(['id' => $row->user_name, 'name' => $row->user_name], $sellers) ? array_push($sellers,['id' => $row->user_name, 'name' => $row->user_name]) : ''  )
                        @php ( ! in_array(['id' => $row->game_condition, 'name' => __($title_game[$row->game_condition])], $game_conditions) ? array_push($game_conditions,['id' => $row->game_condition, 'name' => __($title_game[$row->game_condition])]) : ''  )
                        @php ( ! in_array(['id' => $row->box_condition, 'name' => __($title_game[$row->box_condition])], $box_conditions) ? array_push($box_conditions,['id' => $row->box_condition, 'name' => __($title_game[$row->box_condition])]) : ''  )
    
                        @php ( ! in_array(['id' => $row->user->country->name, 'name' => __($country[$row->user->country->name])], $countrys) ? array_push($countrys,['id' => $row->user->country->name, 'name' => __($country[$row->user->country->name])]) : ''  )
    
                        @php ( ! in_array(['id' => $row->extra_condition, 'name' => __($title_game[$row->extra_condition])], $extra_conditions) ? array_push($extra_conditions,['id' => $row->extra_condition, 'name' => __($title_game[$row->extra_condition])]) : ''  )
    
    
                        @if($producto->catid == 2 || $producto->catid == 1)
                            @php ( ! in_array(['id' => $row->manual_condition, 'name' => __($title_game[$row->manual_condition])], $manual_conditions) ? array_push($manual_conditions,['id' => $row->manual_condition, 'name' => __($title_game[$row->manual_condition])]) : ''  )
                            @php ( ! in_array(['id' => $row->cover_condition, 'name' => __($title_game[$row->cover_condition])], $cover_conditions) ? array_push($cover_conditions,['id' => $row->cover_condition, 'name' => __($title_game[$row->cover_condition])]) : ''  )
                        @endif
                        @if($producto->catid == 2)
                            @php ( ! in_array(['id' => $row->inside_condition, 'name' => __($title_game[$row->inside_condition])], $inside_conditions) ? array_push($inside_conditions,['id' => $row->inside_condition, 'name' => __($title_game[$row->inside_condition])]) : ''  )
                        @endif
    
                        @endforeach
                        @else
                        <tr>
                            <td colspan="11">{{ __('No hay inventario disponible para este producto') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if ($producto->catid == 1)
                @livewire('product.inventory-game-table',['producto' => $producto])
            @elseif($producto->catid == 2)
                @livewire('product.inventory-console-table',['producto' => $producto])
            @elseif($producto->catid == 3)
                @livewire('product.inventory-periferico-table',['producto' => $producto])
            @elseif($producto->catid == 4)
                @livewire('product.inventory-accesorio-table',['producto' => $producto])
            @elseif($producto->catid == 172)
                @livewire('product.inventory-merchadising-table',['producto' => $producto])
            @endif
        @else
            <div class="container">
                <div class="nk-gap-3"></div>
                <div class="row">
                    <div class="col-lg-12">
                        <blockquote class="nk-blockquote">
                            <div class="nk-blockquote-icon"></div>
                            <div class="text-center nk-blockquote-content h4 retro">
                                <font color="black">
                                    {{ __('No hay inventario disponible para este producto') }}
                                </font>
                            </div>
                            <div class="nk-gap"></div>
                            <div class="nk-blockquote-author"></div>
                        </blockquote>
                    </div>
                </div>
            </div>
        @endif

        @endif

    @endsection

    @section('content-script-include')
        <script src="{{ asset('brcode/js/pty.components.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>


        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

        <script src="{{ asset('assets/noty/packaged/jquery.noty.packaged.min.js') }}"></script>

        <!-- date-range-picker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.4.1/chart.min.js"
                integrity="sha512-5vwN8yor2fFT9pgPS9p9R7AszYaNn0LkQElTXIsZFCL7ucT8zDCAqlQXDdaqgA1mZP47hdvztBMsIoFxq/FyyQ=="
                crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    @endsection
    @section('content-script')
        @livewireScripts

        <script>
            Livewire.on('game', function() {


                var $inventoryAdd = $('.br-btn-product-add-game');
                $(function() {
                    $('[data-toggle="popover"]').popover()
                })
                $('.nk-btn-modify-inventory').click(function() {
                        var url = $(this).data('href');
                        var tr = $(this).closest('tr');
                        var sel = $(this).closest('tr').find('select').prop('value');
                        console.log(sel);
                        //console.log(url);
                        location.href = `${url}/${sel}`;
                    }),
                    $inventoryAdd.click(function() {
                        var inventoryId = $(this).data('inventory-id');
                        var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                            .val();
                        var stock = $(this).closest('tr').find('div.qty-new');
                        var tr = $(this).closest('tr');
                        var select = $(this).closest('tr').find('select.br-btn-product-qty');

                        $.showBigOverlay({
                            message: '{{ __('Agregando producto a tu carro de compras') }}',
                            onLoad: function() {
                                $.ajax({
                                    url: '{{ url('/cart/add_item') }}',
                                    data: {
                                        _token: $('meta[name="csrf-token"]').attr(
                                            'content'),
                                        inventory_id: inventoryId,
                                        inventory_qty: inventoryQty,
                                    },
                                    dataType: 'JSON',
                                    type: 'POST',
                                    success: function(data) {
                                        if (data.error == 0) {
                                            $('#br-cart-items').text(data
                                                .items);
                                            stock.html(data.Qty);
                                            if (data.Qty == 0) {
                                                tr.remove();
                                            } else {
                                                select.html('');
                                                for (var i = 1; i < data.Qty +
                                                    1; i++) {
                                                    select.append(
                                                        '<option value=' +
                                                        i + '>' + i +
                                                        '</option>');
                                                }
                                            }
                                        } else {

                                        }
                                        $.showBigOverlay('hide');
                                    },
                                    error: function(data) {
                                        $.showBigOverlay('hide');
                                    }
                                })
                            }
                        });
                    });
            })
        </script>

        <script>
            Livewire.on('console', function() {


                var $inventoryAdd = $('.br-btn-product-add-console');
                $(function() {
                    $('[data-toggle="popover"]').popover()
                })
                $('.nk-btn-modify-inventory').click(function() {
                        var url = $(this).data('href');
                        var tr = $(this).closest('tr');
                        var sel = $(this).closest('tr').find('select').prop('value');
                        console.log(sel);
                        //console.log(url);
                        location.href = `${url}/${sel}`;
                    }),
                    $inventoryAdd.click(function() {
                        var inventoryId = $(this).data('inventory-id');
                        var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                            .val();
                        var stock = $(this).closest('tr').find('div.qty-new');
                        var tr = $(this).closest('tr');
                        var select = $(this).closest('tr').find('select.br-btn-product-qty');

                        $.showBigOverlay({
                            message: '{{ __('Agregando producto a tu carro de compras') }}',
                            onLoad: function() {
                                $.ajax({
                                    url: '{{ url('/cart/add_item') }}',
                                    data: {
                                        _token: $('meta[name="csrf-token"]').attr(
                                            'content'),
                                        inventory_id: inventoryId,
                                        inventory_qty: inventoryQty,
                                    },
                                    dataType: 'JSON',
                                    type: 'POST',
                                    success: function(data) {
                                        if (data.error == 0) {
                                            $('#br-cart-items').text(data
                                                .items);
                                            stock.html(data.Qty);
                                            if (data.Qty == 0) {
                                                tr.remove();
                                            } else {
                                                select.html('');
                                                for (var i = 1; i < data.Qty +
                                                    1; i++) {
                                                    select.append(
                                                        '<option value=' +
                                                        i + '>' + i +
                                                        '</option>');
                                                }
                                            }
                                        } else {

                                        }
                                        $.showBigOverlay('hide');
                                    },
                                    error: function(data) {
                                        $.showBigOverlay('hide');
                                    }
                                })
                            }
                        });
                    });
            })
        </script>

        <script>
            Livewire.on('periferico', function() {


                var $inventoryAdd = $('.br-btn-product-add-periferico');
                $(function() {
                    $('[data-toggle="popover"]').popover()
                })
                $('.nk-btn-modify-inventory').click(function() {
                        var url = $(this).data('href');
                        var tr = $(this).closest('tr');
                        var sel = $(this).closest('tr').find('select').prop('value');
                        console.log(sel);
                        //console.log(url);
                        location.href = `${url}/${sel}`;
                    }),
                    $inventoryAdd.click(function() {
                        var inventoryId = $(this).data('inventory-id');
                        var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                            .val();
                        var stock = $(this).closest('tr').find('div.qty-new');
                        var tr = $(this).closest('tr');
                        var select = $(this).closest('tr').find('select.br-btn-product-qty');

                        $.showBigOverlay({
                            message: '{{ __('Agregando producto a tu carro de compras') }}',
                            onLoad: function() {
                                $.ajax({
                                    url: '{{ url('/cart/add_item') }}',
                                    data: {
                                        _token: $('meta[name="csrf-token"]').attr(
                                            'content'),
                                        inventory_id: inventoryId,
                                        inventory_qty: inventoryQty,
                                    },
                                    dataType: 'JSON',
                                    type: 'POST',
                                    success: function(data) {
                                        if (data.error == 0) {
                                            $('#br-cart-items').text(data
                                                .items);
                                            stock.html(data.Qty);
                                            if (data.Qty == 0) {
                                                tr.remove();
                                            } else {
                                                select.html('');
                                                for (var i = 1; i < data.Qty +
                                                    1; i++) {
                                                    select.append(
                                                        '<option value=' +
                                                        i + '>' + i +
                                                        '</option>');
                                                }
                                            }
                                        } else {

                                        }
                                        $.showBigOverlay('hide');
                                    },
                                    error: function(data) {
                                        $.showBigOverlay('hide');
                                    }
                                })
                            }
                        });
                    });
            })
        </script>

        <script>
            Livewire.on('accesorio', function() {


                var $inventoryAdd = $('.br-btn-product-add-accesorio');
                $(function() {
                    $('[data-toggle="popover"]').popover()
                })
                $('.nk-btn-modify-inventory').click(function() {
                        var url = $(this).data('href');
                        var tr = $(this).closest('tr');
                        var sel = $(this).closest('tr').find('select').prop('value');
                        console.log(sel);
                        //console.log(url);
                        location.href = `${url}/${sel}`;
                    }),
                    $inventoryAdd.click(function() {
                        var inventoryId = $(this).data('inventory-id');
                        var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                            .val();
                        var stock = $(this).closest('tr').find('div.qty-new');
                        var tr = $(this).closest('tr');
                        var select = $(this).closest('tr').find('select.br-btn-product-qty');

                        $.showBigOverlay({
                            message: '{{ __('Agregando producto a tu carro de compras') }}',
                            onLoad: function() {
                                $.ajax({
                                    url: '{{ url('/cart/add_item') }}',
                                    data: {
                                        _token: $('meta[name="csrf-token"]').attr(
                                            'content'),
                                        inventory_id: inventoryId,
                                        inventory_qty: inventoryQty,
                                    },
                                    dataType: 'JSON',
                                    type: 'POST',
                                    success: function(data) {
                                        if (data.error == 0) {
                                            $('#br-cart-items').text(data
                                                .items);
                                            stock.html(data.Qty);
                                            if (data.Qty == 0) {
                                                tr.remove();
                                            } else {
                                                select.html('');
                                                for (var i = 1; i < data.Qty +
                                                    1; i++) {
                                                    select.append(
                                                        '<option value=' +
                                                        i + '>' + i +
                                                        '</option>');
                                                }
                                            }
                                        } else {

                                        }
                                        $.showBigOverlay('hide');
                                    },
                                    error: function(data) {
                                        $.showBigOverlay('hide');
                                    }
                                })
                            }
                        });
                    });
            })
        </script>

        <script>
            Livewire.on('merchandising', function() {


                var $inventoryAdd = $('.br-btn-product-add-merchandising');
                $(function() {
                    $('[data-toggle="popover"]').popover()
                })
                $('.nk-btn-modify-inventory').click(function() {
                        var url = $(this).data('href');
                        var tr = $(this).closest('tr');
                        var sel = $(this).closest('tr').find('select').prop('value');
                        console.log(sel);
                        //console.log(url);
                        location.href = `${url}/${sel}`;
                    }),
                    $inventoryAdd.click(function() {
                        var inventoryId = $(this).data('inventory-id');
                        var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty')
                            .val();
                        var stock = $(this).closest('tr').find('div.qty-new');
                        var tr = $(this).closest('tr');
                        var select = $(this).closest('tr').find('select.br-btn-product-qty');

                        $.showBigOverlay({
                            message: '{{ __('Agregando producto a tu carro de compras') }}',
                            onLoad: function() {
                                $.ajax({
                                    url: '{{ url('/cart/add_item') }}',
                                    data: {
                                        _token: $('meta[name="csrf-token"]').attr(
                                            'content'),
                                        inventory_id: inventoryId,
                                        inventory_qty: inventoryQty,
                                    },
                                    dataType: 'JSON',
                                    type: 'POST',
                                    success: function(data) {
                                        if (data.error == 0) {
                                            $('#br-cart-items').text(data
                                                .items);
                                            stock.html(data.Qty);
                                            if (data.Qty == 0) {
                                                tr.remove();
                                            } else {
                                                select.html('');
                                                for (var i = 1; i < data.Qty +
                                                    1; i++) {
                                                    select.append(
                                                        '<option value=' +
                                                        i + '>' + i +
                                                        '</option>');
                                                }
                                            }
                                        } else {

                                        }
                                        $.showBigOverlay('hide');
                                    },
                                    error: function(data) {
                                        $.showBigOverlay('hide');
                                    }
                                })
                            }
                        });
                    });
            })
        </script>

        <script>
            Livewire.on('alert', function() {
                Swal.fire(
                    'Listo!',
                    '<span class="retro">Tu Producto fue eliminado</span>',
                    'success'
                )
            })
        </script>

        <script>
            // Social Share links.
            const gmailBtn = document.getElementById('gmail-btn');
            const facebookBtn = document.getElementById('facebook-btn');
            const gplusBtn = document.getElementById('gplus-btn');
            const linkedinBtn = document.getElementById('linkedin-btn');
            const twitterBtn = document.getElementById('twitter-btn');
            const whatsappBtn = document.getElementById('whatsapp-btn');
            const socialLinks = document.getElementById('social-links');

            // producturl posttitle
            let productUrl = encodeURI(document.location.href);
            let productTitle = encodeURI('{{ $producto->name }}');
            facebookBtn.setAttribute("href", `https://www.facebook.com/sharer.php?u=${productUrl}`);
            twitterBtn.setAttribute("href", `https://twitter.com/share?url=${productUrl}&text=${productTitle}`);
            whatsappBtn.setAttribute("href", `https://wa.me/?text=${productTitle} ${productUrl}`);
            gmailBtn.setAttribute("href", `https://mail.google.com/mail/?view=cm&su=${productTitle}&body=${productUrl}`);
            linkedinBtn.setAttribute("href", `https://www.linkedin.com/shareArticle?url=${productUrl}&title=${productTitle}`);
        </script>


        <script>
            function setup() {
                return {
                    activeTab: 0,
                    tabs: [
                        "Nuevo",
                        "Solo",
                        "Usado",
                    ]
                };
            };
        </script>
        @stack('scripts')
    @endsection
