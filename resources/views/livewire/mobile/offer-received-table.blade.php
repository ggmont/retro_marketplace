<div>
    <div class="card-orders">

        <table class="table mb-0">
            <thead>
                <tr>
                    <th scope="col">Producto</th>
                    <th scope="col">Comprador</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Tiempo</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($offersReceived as $s)
                    <tr>
                        @if ($s->inventory)
                            <td class="text-left">
                                <a href="{{ route('product-inventory-show', $s->inventory->id) }}">
                                    @if ($s->inventory->title !== null)
                                        <div class="product-description font-bold">
                                            {{ str_limit($s->inventory->title, 16) }}
                                        </div>
                                    @else
                                        <div class="product-description font-bold">
                                            {{ str_limit($s->inventory->product->name, 16) }}
                                        </div>
                                    @endif
                                </a>
                            </td>
                        @else
                            <td>
                                <div class="product-description font-bold">
                                    -----
                                </div>
                            </td>
                        @endif


                        <td>
                            <div class="product-description font-bold">
                                {{ $s->userbid->user_name }}
                            </div>
                        </td>
                        <td>
                            <div class="font-bold text-green-700 text-lg">
                                {{ number_format(optional($s->inventory)->max_bid, 2) . ' €' }}
                            </div>
                        </td>
                        <td>
                            @if ($s->inventory)
                                @if ($s->inventory->countdown_hours > 0)
                                    {{ $s->inventory->countdown_hours }} Horas
                                @else
                                    Se acabó el tiempo
                                @endif
                            @else
                                Este producto fue eliminado por su usuario
                            @endif

                            <br>
                            @if ($s->inventory)
                                @if ($s->status == 1)
                                    <div class="text-blue-500 font-bold">En Espera</div>
                                @elseif ($s->status == 2)
                                    <div class="text-red-500 font-bold">Superado</div>
                                @elseif ($s->status == 3)
                                    <div class="text-green-500 font-bold">Aceptado</div>
                                @elseif ($s->status == 4)
                                    <div class="text-red-500 font-bold">Denegado</div>
                                @endif
                            @else
                                <div class="text-red-500 font-bold">-</div>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
