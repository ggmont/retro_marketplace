<div>
   <h1>Lista de paises (Pruebas a tiempo real)</h1>
   <form action="" wire:submit.prevent="agregar">
   <input type="text" placeholder="Escribir Nuevo Pais" wire:model="pais" wire:keydown.enter="agregar">
   <p></p>
   <button type="submit">Enviar</button>
   </form>
   <ul>
   	@foreach ($paises as $pais)
   	<li> {{$pais}} </li>
   	@endforeach
   </ul>
</div>
