<div class="col-lg-6 nk-mchimp nk-form nk-form-style-1 validate">
    <label class="text-lg text-gray-700">¿Tienes un Codigo Promocional? Introducelo
        para recibir descuentos especiales! </label>
    <div class="nes-field is-inline">
        <input wire:model="code" type="text" id="code" class="nes-input" placeholder="RGM">
        @error('category_text') <span class="error retro text-xs text-red-700">{{ $message }}</span> @enderror
        <button for="inline_field" button wire:click="check" class="nes-btn is-error retro text-xs">COMPROBAR</button>
    </div>
</div>
