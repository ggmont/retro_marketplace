<div>
    <?php
    $duration = [];
    $duration_not_press = [];
    $duration_used = [];
    $duration_new = [];
    $duration_general = [];
    $t = '';
    $total_numeros = '';
    $total_numeros_not_press = '';
    $total_numeros_used = '';
    $total_numeros_new = '';
    $total_numeros_general = '';
    $suma = '';
    $suma_not_press = '';
    $suma_used = '';
    $suma_new = '';
    $suma_general = '';
    $m = '';
    $promedio_not_press = '';
    $promedio_used = '';
    $promedio_new = '';
    $promedio_general = '';
    $prueba = '';
    $fecha = '';
    $precio = '';
    
    foreach ($product->inventory as $item) {
        $cantidad = $item->price;
        $duration[] = $cantidad;
        $total_numeros = count($duration);
        $total = max($duration);
        $t = min($duration);
        $suma = array_sum($duration);
        $m = $suma / $total_numeros;
        $prueba = $item->created_at;
    }
    
    //PROMEDIO GENERAL
    foreach ($seller_sold_general as $p) {
        $cantidad_general = $p->price;
        $duration_general[] = $cantidad_general;
        $total_numeros_general = count($duration_general);
        $suma_general = array_sum($duration_general);
        $promedio_general = $suma_general / $total_numeros_general;
    }
    
    //PROMEDIO PARA LAS CONDICIONES TIPO "NOT PRESS - NO TIENE" POKEMON GOLD ES EL QUE NO TIENE ASTERIX Y OBELIS TAMPOCO TIENEN
    foreach ($seller_sold_not_pres as $p) {
        $cantidad_press = $p->price;
        $duration_not_press[] = $cantidad_press;
        $total_numeros_not_press = count($duration_not_press);
        $suma_not_press = array_sum($duration_not_press);
        $promedio_not_press = $suma_not_press / $total_numeros_not_press;
    }
    
    //PROMEDIO PARA LAS CONDICIONES TIPO "USED" WOODY ES EL QUE ESTA USADO
    foreach ($seller_sold_used as $p_used) {
        $cantidad_used = $p_used->price;
        $duration_used[] = $cantidad_used;
        $total_numeros_used = count($duration_used);
        $suma_used = array_sum($duration_used);
        $promedio_used = $suma_used / $total_numeros_used;
    }
    
    //PROMEDIO PARA LAS CONDICIONES TIPO "NEW" XG2 ES EL QUE TIENE EL ESTADO NUEVO
    foreach ($seller_sold_new as $p_new) {
        $cantidad_new = $p_new->price;
        $duration_new[] = $cantidad_new;
        $total_numeros_new = count($duration_new);
        $suma_new = array_sum($duration_new);
        $promedio_new = $suma_new / $total_numeros_new;
    }
    
    ?>


    <!--actual component start-->
    <div x-data="setup()">
        <ul class="flex items-center justify-center my-3 retro">
            <template x-for="(tab, index) in tabs" :key="index">
                <li class="px-4 py-2 text-gray-500 border-b-8 cursor-pointer"
                    :class="activeTab===index ? 'text-red-500 border-red-500' : ''" @click="activeTab = index"
                    x-text="tab"></li>
            </template>
        </ul>
        <div class="nes-container with-title is-centered">
            <p class="title retro text-gray-900">GENERAL</p>
            <p class="text-center text-lg retro">{{ number_format($promedio_general, 2) }} €</p>
        </div>
        @if ((new \Jenssegers\Agent\Agent())->isMobile())
            <div class="text-center bg-white border w-88">
            @else
                <div class="text-center bg-white border w-80">
        @endif
        <div x-show="activeTab===0">
            <div class="col-md-12">
                @if ((new \Jenssegers\Agent\Agent())->isMobile())
                    <div class="mt-10">
                    @else
                        <div class="d-flex justify-content-between mt-10">
                @endif
                <button class="nk-btn nk-btn-outline nk-btn-color-success retro">Nuevo <br> <span
                        class="nuevo">
                        @if ($promedio_new == 0)
                            No Info
                        @else

                            {{ number_format($promedio_new, 2) }} €
                        @endif
                    </span></button>
                <button class="nk-btn nk-btn-outline nk-btn-color-danger retro">Solo <br> <span class="muyusado">
                        @if ($promedio_not_press == 0)
                            No Info
                        @else
                            {{ number_format($promedio_not_press, 2) }} €
                        @endif
                    </span></button>
                <button class="nk-btn nk-btn-outline nk-btn-color-warning retro">Usado <br> <span
                        class="usado">
                        @if ($promedio_used == 0)
                            No Info
                        @else
                            {{ number_format($promedio_used, 2) }} €
                        @endif
                    </span></button>
            </div>
        </div>
        <div class="relative">
            <canvas id="myChart" width="386" height="250"></canvas>
        </div>
    </div>
    <div x-show="activeTab===1">
        <div class="col-md-12">
            @if ((new \Jenssegers\Agent\Agent())->isMobile())
                <div class="mt-10">
                @else
                    <div class="d-flex justify-content-between mt-10">
            @endif
            <button class="nk-btn nk-btn-outline nk-btn-color-success retro">Nuevo <br> <span class="nuevo">
                    @if ($promedio_new == 0)
                        No Info
                    @else

                        {{ number_format($promedio_new, 2) }} €
                    @endif
                </span></button>
            <button class="nk-btn nk-btn-outline nk-btn-color-danger retro">Solo <br> <span class="muyusado">
                    @if ($promedio_not_press == 0)
                        No Info
                    @else
                        {{ number_format($promedio_not_press, 2) }} €
                    @endif
                </span></button>
            <button class="nk-btn nk-btn-outline nk-btn-color-warning retro">Usado <br> <span class="usado">
                    @if ($promedio_used == 0)
                        No Info
                    @else
                        {{ number_format($promedio_used, 2) }} €
                    @endif
                </span></button>
        </div>
    </div>
    <div class="relative">
        <canvas id="myChart2" width="386" height="250"></canvas>
    </div>
</div>
<div x-show="activeTab===2">
    <div class="col-md-12">
        @if ((new \Jenssegers\Agent\Agent())->isMobile())
            <div class="mt-10">
            @else
                <div class="d-flex justify-content-between mt-10">
        @endif
        <button class="nk-btn nk-btn-outline nk-btn-color-success retro">Nuevo <br> <span class="nuevo">
                @if ($promedio_new == 0)
                    No Info
                @else

                    {{ number_format($promedio_new, 2) }} €
                @endif
            </span></button>
        <button class="nk-btn nk-btn-outline nk-btn-color-danger retro">Solo <br> <span class="muyusado">
                @if ($promedio_not_press == 0)
                    No Info
                @else
                    {{ number_format($promedio_not_press, 2) }} €
                @endif
            </span></button>
        <button class="nk-btn nk-btn-outline nk-btn-color-warning retro">Usado <br> <span class="usado">
                @if ($promedio_used == 0)
                    No Info
                @else
                    {{ number_format($promedio_used, 2) }} €
                @endif
            </span></button>
    </div>
</div>
<div class="relative">
    <canvas id="myChart3" width="386" height="250"></canvas>
</div>
</div>
</div>



</div>
<!--actual component end-->



</div>
@push('scripts')
    <script>
        var ctx = document.getElementById('myChart');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    <?php foreach ($date_new as $l) {?>[<?php echo "'" . $l->my_date . "'"; ?>],
                    <?php }?>
                ],
                datasets: [{
                        label: 'Precio medio de venta - Nuevo (€)',

                        data: [

                            <?php
                            foreach ($fecha_general as $l) {
                                foreach ($date_new as $u) {
                                    if ($l->my_date == $u->my_date) {
                                        echo "'" . $u->price . "', "; // you can also use $row if you don't use keys
                                    }
                                }
                            } ?>

                        ],
                        backgroundColor: [
                            'rgba(10, 178, 43)',
                        ],
                        borderColor: [
                            'rgba(10, 178, 43)',
                        ],
                        borderWidth: 1
                    },


                ],

            },
            options: {
                responsive: true,
                interaction: {
                    mode: 'index',
                    intersect: false,
                },
                stacked: false,
                plugins: {
                    title: {
                        display: true,

                    }
                },
                scales: {
                    y: {
                        type: 'linear',
                        display: true,
                        position: 'left',
                        tick: {

                            values: [{{ number_format($t, 2) }}, {{ number_format($m, 2) }},
                                {{ number_format($total, 2) }}
                            ]
                        }
                    },
                    y1: {
                        type: 'linear',
                        display: true,
                        position: 'right',

                        // grid line settings
                        grid: {
                            drawOnChartArea: false, // only want the grid lines for one axis to show up
                        },
                    },
                }
            }
        });
    </script>
    <script>
        var ctx = document.getElementById('myChart2');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    <?php foreach ($date_not_pres as $l) {?>[<?php echo "'" . $l->my_date . "'"; ?>],
                    <?php }?>
                ],
                datasets: [{
                        label: 'Precio medio de venta - Solo el producto (€)',

                        data: [

                            <?php
                            foreach ($fecha_general as $l) {
                                foreach ($date_not_pres as $u) {
                                    if ($l->my_date == $u->my_date) {
                                        echo "'" . $u->price . "', "; // you can also use $row if you don't use keys
                                    }
                                }
                            } ?>

                        ],
                        backgroundColor: [
                            'rgba(188, 20, 4)',
                        ],
                        borderColor: [
                            'rgba(188, 20, 4)',
                        ],
                        borderWidth: 1
                    },


                ],

            },
            options: {
                responsive: true,
                interaction: {
                    mode: 'index',
                    intersect: false,
                },
                stacked: false,
                plugins: {
                    title: {
                        display: true,

                    }
                },
                scales: {
                    y: {
                        type: 'linear',
                        display: true,
                        position: 'left',
                        tick: {

                            values: [{{ number_format($t, 2) }}, {{ number_format($m, 2) }},
                                {{ number_format($total, 2) }}
                            ]
                        }
                    },
                    y1: {
                        type: 'linear',
                        display: true,
                        position: 'right',

                        // grid line settings
                        grid: {
                            drawOnChartArea: false, // only want the grid lines for one axis to show up
                        },
                    },
                }
            }
        });
    </script>
    <script>
        var ctx = document.getElementById('myChart3');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    <?php foreach ($date_used as $l) {?>[<?php echo "'" . $l->my_date . "'"; ?>],
                    <?php }?>
                ],
                datasets: [{
                        label: 'Precio medio de venta - Usado (€)',

                        data: [

                            <?php
                            foreach ($fecha_general as $l) {
                                foreach ($date_used as $u) {
                                    if ($l->my_date == $u->my_date) {
                                        echo "'" . $u->price . "', "; // you can also use $row if you don't use keys
                                    }
                                }
                            } ?>

                        ],
                        backgroundColor: [
                            'rgba(241, 254, 39)',
                        ],
                        borderColor: [
                            'rgba(241, 254, 39)',
                        ],
                        borderWidth: 1
                    },


                ],

            },
            options: {
                responsive: true,
                interaction: {
                    mode: 'index',
                    intersect: false,
                },
                stacked: false,
                plugins: {
                    title: {
                        display: true,

                    }
                },
                scales: {
                    y: {
                        type: 'linear',
                        display: true,
                        position: 'left',
                        tick: {

                            values: [{{ number_format($t, 2) }}, {{ number_format($m, 2) }},
                                {{ number_format($total, 2) }}
                            ]
                        }
                    },
                    y1: {
                        type: 'linear',
                        display: true,
                        position: 'right',

                        // grid line settings
                        grid: {
                            drawOnChartArea: false, // only want the grid lines for one axis to show up
                        },
                    },
                }
            }
        });
    </script>
@endpush
