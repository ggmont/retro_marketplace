<div class="relative">
    <input type="text" class="form-input" placeholder="@lang('messages.search_product')" wire:model="query"
        wire:keydown.escape="restart" wire:keydown.tab="restart" wire:keydown.ArrowUp="incrementHighlight"
        wire:keydown.ArrowDown="decrementHighlight" wire:keydown.enter="selectProduct">



    @if (!empty($query))
        <div class="absolute z-10 w-full bg-white rounded-t-none shadow-lg list-group">
            <div class="list-item">
                <font color="black">@lang('messages.searching')</font>
            </div>
        </div>
        <div class="fixed top-0 left-0 right-0 botton-0" wire:click="reset"></div>
        <div class="absolute z-10 w-full bg-white rounded-t-none shadow-lg list-group">
            @if ($products->count() > 0)

                @foreach ($products as $product)

                    <a href="{{ route('product-show', $product['id']) }}"
                        class="w-full list-item border list-none l list-group-item-action flex w-full items-center p-2 pl-2 border-transparent border-l-2 relative hover:border-teal-100{{ $highlightIndex === $product ? 'bg-blue-300' : '' }}">

                        <img class="w-16 h-16" src="{{ url('/images/' . $product->imageSearchBar()) }}"
                            alt="{{ $product->imageSearchBar() ?? 'Prueba' }}">
                        &nbsp;
                        {{ $product['name'] }}
                        <br>
                        &nbsp;&nbsp;{{ $product['platform'] }} - {{ $product['region'] }}
                    </a>
                @endforeach
                <a href="https://www.retrogamingmarket.eu/MarketPlaceStore?search={{$query}}#allproducts"
                    class="retro text-xs font-bold w-full list-item border list-none l list-group-item-action flex w-full items-center p-2 pl-2 border-transparent border-l-2 relative hover:border-teal-500{{ $highlightIndex === $product ? 'bg-blue-700' : '' }}">
                    <center> &nbsp; &nbsp; &nbsp; @lang('messages.show_all_results')  </center>
                </a>
            @else
                @if (Auth::user())
                    <div class="list-item">
                        <font color="black"><span class="text-xs retro">@lang('messages.not_results')</span> <a
                                href="{{ route('product_not_found') }}" class="text-xs retro nes-btn is-warning">@lang('messages.not_found_results')</a></font>
                    </div>
                @else
                    <div class="list-item">
                        <font color="black"><span class="text-xs retro">@lang('messages.not_results')</span></font>
                    </div>
                @endif
            @endif
        </div>
    @endif
</div>
