<div>
    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <button type="button" class="float-right nes-btn is-error nk-btn-rounded nk-btn nk-btn-xs"
                            data-toggle="modal" data-target="#NewMessage">@lang('messages.msg_new')</button>
                        <br>
                        <center>
                            <font color="white">
                                <h5 class="retro font-extrabold">@lang('messages.users_list')</h5>
                            </font>
                        </center>
                        <br>
                    </div>

                    <div class="p-0 md:flex card-body chatbox">
                        <div class="w-full p-4">
                            <div class="relative"> <input wire:model="search" type="text"
                                    class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                    placeholder="@lang('messages.msg_chat')..."></div>
                            <ul class="hover:bg-gray-300">
                                @foreach ($beta as $s)
                                    @if ($s->alias_user_id !== auth()->id())
                                        @php
                                            $not_seen =
                                                App\AppMessage::where('alias_from_id', $s->prueba->id)
                                                    ->where('alias_to_id', auth()->id())
                                                    ->where('read', false)
                                                    ->get() ?? null;
                                            
                                        @endphp

                                        @if ($s->prueba->user_name == auth()->user()->user_name)

                                            <a wire:click="getUser({{ $s->user->id }})" class="text-dark link">
                                            @else

                                                <a wire:click="getUser({{ $s->prueba->id }})" class="text-dark link">
                                        @endif
                                        <li
                                            class="flex items-center justify-between p-2 mt-2 transition bg-white rounded cursor-pointer hover:shadow-lg">
                                            <div class="flex ml-2">
                                                @if ($s->prueba->user_name == auth()->user()->user_name)
                                                    @if ($s->user->profile_picture)
                                                        <img class="rounded-full img-fluid avatar"
                                                            src="{{ url($s->user->profile_picture) }}">
                                                    @else
                                                        <img class="rounded-full img-fluid avatar"
                                                            src="{{ asset('img/profile-picture-not-found.png') }}">
                                                    @endif
                                                @else
                                                    @if ($s->prueba->profile_picture)
                                                        <img class="rounded-full img-fluid avatar"
                                                            src="{{ url($s->prueba->profile_picture) }}">
                                                    @else
                                                        <img class="rounded-full img-fluid avatar"
                                                            src="{{ asset('img/profile-picture-not-found.png') }}">
                                                    @endif
                                                @endif
                                                <div class="flex flex-col ml-2"> <span class="font-medium text-black">
                                                        @if ($s->prueba->user_name == auth()->user()->user_name)
                                                            {{ $s->user->user_name }}
                                                        @else
                                                            {{ $s->prueba->user_name }}
                                                        @endif
                                                        -
                                                        @if ($s->prueba->user_name == auth()->user()->user_name)
                                                            @if ($s->user->is_online == true)
                                                                <i class="fa fa-circle text-success online-icon"></i>
                                                            @else
                                                                <i class="fa fa-circle text-danger online-icon"></i>
                                                            @endif
                                                        @else
                                                            @if ($s->prueba->is_online == true)
                                                                <i class="fa fa-circle text-success online-icon"></i>
                                                            @else
                                                                <i class="fa fa-circle text-danger online-icon"></i>
                                                            @endif
                                                        @endif

                                                        @if (filled($not_seen))
                                                            @php
                                                                $boton = true;
                                                                $sonido = 'newmessage.mp3';
                                                            @endphp
                                                            <audio autoplay
                                                                src="{{ asset('sonidos/' . $sonido) }}"></audio>
                                                            -
                                                            <div class="rounded badge badge-success">
                                                                {{ $not_seen->count() }}
                                                            </div>
                                                        @endif
                                                    </span> <span class="w-32 text-sm text-gray-500 truncate">

                                                        {{ substr($s->last_message, 0, 30) . '.' }}

                                                    </span>
                                                </div>
                                            </div>
                                            <div class="flex flex-col items-center"> <span class="text-red-300">
                                                    {{ $s->updated_at->format('d/m/Y') }}<br>
                                                    {{ $s->updated_at->format('H:m:s') }}</span>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>


                    </a>
                </div>
            </div>

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <center>
                            <label class="block mb-2 text-lg font-extrabold tracking-wide text-white uppercase">
                                @if (isset($sender)) <a
                                        href="{{ route('user-info', $sender->user_name) }}" style="color: #ffffff"
                                        target="_blank">
                                        {{ $sender->user_name }}
                                </a> @else
                                    CHAT RGM
                                @endif
                            </label>
                        </center>
                    </div>
                    <div class="card-body message-box" wire:poll="mountdata">
                        @if (filled($allmessages))
                            @foreach ($allmessages as $mgs)
                                <div class="single-message @if ($mgs->alias_from_id == auth()->id()) sent @else received @endif">

                                    <p class="my-0 text-base text-yellow-700 retro font-weight-bolder">

                                        {{ $mgs->user->user_name }}

                                        @if ($mgs->user->user_name == auth()->user()->user_name)
                                            <a wire:click="deleteId({{ $mgs->id }})" data-toggle="modal"
                                                data-target="#exampleModal">
                                                <i class="nes-icon close is-small"></i>
                                            </a>
                                        @endif
                                    </p>
                                    @if ($mgs->image)
                                        <a href="{{ url('storage/' . $mgs->image) }}" class="fancybox"
                                            data-fancybox="{{ $mgs->user->user_name }}">
                                            <img class="w-40" src="{{ asset('storage/' . $mgs->image) }}">
                                        </a>
                                    @else
                                    <span class="text-sm font-bold">{{ $mgs->content }}</span>
                                        
                                    @endif

                                    <br><small class="text-muted w-100">@lang('messages.msg_sends')
                                        <em>{{ $mgs->created_at }}</em></small>
                                </div>
                            @endforeach
                        @else
                            @if ((new \Jenssegers\Agent\Agent())->isMobile())
                                <div class="retro nes-container with-title is-centered">
                                    <p class="title"><img class="w-20"
                                        src="{{ asset('img/RGM.png') }}"></p>
                                        <p class="font-extrabold text-lg">@lang('messages.msg_message') <br><span class="text-red-500">"@lang('messages.msg_new')"
                                        </span> @lang('messages.msg_search_user') &nbsp;
                                            <span class="text-red-700">"@lang('messages.msg_public')"</span>.
                                        </p>
                                </div>
                            @else
                                <div class="relative">
                                    <div class="p-90">
                                        <div class="nes-container with-title is-centered ">
                                            <p class="title"><img class="w-20"
                                                    src="{{ asset('img/RGM.png') }}"></p>
                                            <p class="font-extrabold text-lg">@lang('messages.msg_message') <br><span class="text-red-500">"@lang('messages.msg_new')"
                                            </span> @lang('messages.msg_search_user') &nbsp;
                                                <span class="text-red-700">"@lang('messages.msg_public')"</span>.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endif

                    </div>
                    @if (filled($allmessages))
                        <div class="card-footer">
                            <form wire:submit.prevent="SendMessage">
                                <div class="row">
                                    <div class="col-md-6">
                                        @if ($photo)
                                            <input wire:model="message" id="message"
                                                class="shadow-none form-control nes-input input w-100 d-inline-block"
                                                placeholder="@lang('messages.write_a_message')" disabled>
                                        @else
                                            <input wire:model="message" id="message"
                                                class="shadow-none form-control nes-input input w-100 d-inline-block"
                                                placeholder="@lang('messages.write_a_message')" required>
                                        @endif
                                    </div>

                                    <div class="col-md-4">
                                        <button type="submit"
                                            class="font-semibold nes-btn is-primary d-inline-block w-100"><i
                                                class="fa fa-paper-plane"></i> @lang('messages.sends')</button>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="text-xs nes-btn col-md-12 retro">
                                            <i class="fa fa-image fa-2x"></i>
                                            <input type="file" name="photo" wire:model="photo"
                                                id="upload{{ $iteration }}">
                                        </label>
                                    </div>
                                    @error('message') <span class="error">{{ $message }}</span>
                                    @enderror
                                    @error('photo') <span class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                                <br>
                                <div wire:loading wire:target="photo"
                                    class="relative px-4 py-3 text-red-700 bg-blue-100 border border-blue-400 rounded col-md-12"
                                    role="alert">
                                    <strong class="text-xs font-extrabold retro">@lang('messages.msg_image_load')</strong>
                                    <span class="block font-extrabold sm:inline">@lang('messages.msg_image_wait')</span>
                                </div>
                                @if ($photo)
                                    <img src="{{ $photo->temporaryUrl() }}">
                                @endif
                            </form>
                        </div>

                </div>
            @else
                <div class="card-footer">
                    <form wire:submit.prevent="SendMessage">
                        <div class="row">
                            <span class="retro font-extrabold">@lang('messages.msg_image_wait_two')...</span>
                        </div>
                    </form>
                </div>
                @endif

            </div>
        </div>

        <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <center>
                            <h5 class="modal-title retro" id="exampleModalLabel">@lang('messages.confirm_remove')
                            </h5>
                        </center>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <br>
                        <h5 class="retro">
                            <center>@lang('messages.msg_delete')</center>
                        </h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="nes-btn is-warning retro close-btn"
                            data-dismiss="modal">@lang('messages.closes')</button>
                        <button type="button" wire:click.prevent="delete()" class="nes-btn retro is-error close-modal"
                            data-dismiss="modal">@lang('messages.yes_closes')</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
