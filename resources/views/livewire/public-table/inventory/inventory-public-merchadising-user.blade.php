<div>

    <div class="mt-20 wishlist-table-area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="my-6 bg-white rounded shadow-md">
                        <div class="tabs">
                            <div class="border-b tabs">
                                <div class="relative border-l-2 border-transparent">
                                    <input class="absolute z-10 w-full h-5 opacity-0 cursor-pointer top-6"
                                        type="checkbox" id="chck2">
                                    <header
                                        class="flex items-center justify-between p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                        for="chck2">
                                        <span class="text-xl font-thin text-grey-darkest">
                                            <h4 class="text-gray-900 retro-sell"> {{ __('Filtrar por') . ':' }} </h4>
                                        </span>
                                        <div
                                            class="flex items-center justify-center border rounded-full border-grey w-7 h-7 test">
                                            <!-- icon by feathericons.com -->
                                            <svg aria-hidden="true" class="" data-reactid="266"
                                                fill="none" height="24" stroke="#606F7B" stroke-linecap="round"
                                                stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <polyline points="6 9 12 15 18 9">
                                                </polyline>
                                            </svg>
                                        </div>
                                    </header>
                                    <div class="tabs-content">
                                        <div class="col-md-12">
                                            <input wire:model="search"
                                                class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
                                                type="text" placeholder="@lang('messages.search_by_name')">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label
                                                        class="block mb-2 text-xs font-extrabold tracking-wide text-gray-700 uppercase retro"
                                                        for="grid-state">
                                                        @lang('messages.platform')
                                                    </label>
                                                    <div class="relative">
                                                        <div class="nes-select">

                                                            <select wire:model="search_platform" class="retro">
                                                                <option value="" selected>@lang('messages.alls')
                                                                </option>
                                                                @foreach ($platform as $p)
                                                                    <option value="{{ $p->value }}">
                                                                        {{ __($p->value) }}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <label
                                                        class="block mb-2 text-xs font-extrabold tracking-wide text-gray-700 uppercase retro"
                                                        for="grid-state">
                                                        @lang('messages.box_condition')
                                                    </label>
                                                    <div class="relative">
                                                        <div class="nes-select">

                                                            <select wire:model="search_box" class="retro">
                                                                <option value="" selected>@lang('messages.alls')
                                                                </option>
                                                                @foreach ($condition as $p)
                                                                    <option value="{{ $p->value_id }}">
                                                                        {{ __($p->value) }}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <label
                                                        class="block mb-2 text-xs font-extrabold tracking-wide text-gray-700 uppercase retro"
                                                        for="grid-state">
                                                        @lang('messages.state_condition')
                                                    </label>
                                                    <div class="relative">
                                                        <div class="nes-select">

                                                            <select wire:model="search_status" class="retro">
                                                                <option value="" selected>@lang('messages.alls')
                                                                </option>

                                                                @foreach ($condition as $p)
                                                                    <option value="{{ $p->value_id }}">
                                                                        {{ __($p->value) }}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <label
                                                        class="block mb-2 text-xs font-extrabold tracking-wide text-gray-700 uppercase retro"
                                                        for="grid-state">
                                                        @lang('messages.state_condition')
                                                    </label>
                                                    <div class="relative">
                                                        <div class="nes-select">

                                                            <select wire:model="search_extra" class="retro">
                                                                <option value="" selected>@lang('messages.alls')
                                                                </option>

                                                                @foreach ($condition as $p)
                                                                    <option value="{{ $p->value_id }}">
                                                                        {{ __($p->value) }}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>

                                                    </div>
                                                </div>





                                            </div>

                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wishlist-table table-responsive">
                            <table class="w-full table-auto min-w-max">
                                <thead>
                                    <tr class="text-sm leading-normal text-gray-100 uppercase bg-red-700">
                                        @if ($user->id == Auth::id())
                                            <th class="px-6 py-3 text-center"></th>
                                        @endif
                                        <th class="px-6 py-3 text-center"><i class="fa fa-image"></i></th>
                                        <th class="px-6 py-3 text-center">@lang('messages.products')</th>
                                        <th class="px-6 py-3 text-center">@lang('messages.box')</th>
                                        <th class="px-6 py-3 text-center">@lang('messages.state_inventory')</th>
                                        <th class="px-6 py-3 text-center">Extra</th>
                                        <th class="px-6 py-3 text-center"><i class="fa fa-image"></i></th>
                                        <th class="px-6 py-3 text-center"><i class="fa fa-comment"></i></th>
                                        <th class="px-6 py-3 text-center">@lang('messages.price')</th>
                                        <th class="px-6 py-3 text-center">Stock</th>
                                    </tr>
                                </thead>
                                <tbody class="text-sm font-light text-gray-600">
                                    @foreach ($ari as $u)
                                        @if ($u->quantity > 0)
                                            <tr class="border-b border-gray-500 hover:bg-gray-200">
                                                @if ($user->id == Auth::id())
                                                    <td class="product-remove">
                                                        <a wire:click="deleteId({{ $u->id }})"
                                                            data-toggle="modal" data-target="#merchadisingModal">
                                                            ×
                                                        </a>
                                                    </td>
                                                @endif
                                                <td>

                                                    <center>
                                                        <a href="{{ route('product-show', $u->product->id) }}"
                                                            target="_blank">
                                                            @if ($u->product->images->first())
                                                                <img width="100px" height="100px"
                                                                    src="{{ count($u->product->images) > 0 ? url('/images/' . $u->product->images->first()->image_path) : url('assets/images/art-not-found.jpg') }}"
                                                                    alt="{{ $u->product->name }}">
                                                            @else
                                                                <img width="100px" height="100px"
                                                                    src="{{ url('assets/images/art-not-found.jpg') }}">
                                                            @endif
                                                        </a>
                                                    </center>
                                                </td>
                                                <td class="text-left">
                                                    <a href="{{ route('product-show', $u->product->id) }}"
                                                        target="_blank">
                                                        <span
                                                            class="text-sm retro font-bold color-primary">
                                                            <font color="black"><b>{{ $u->product->name }}
                                                                    @if ($u->product->name_en)
                                                                        <br><small>{{ $u->product->name_en }}</small>
                                                                    @endif
                                                                </b>
                                                            </font>
                                                        </span>
                                                    </a>


                                                    <br><span class="h7 text-secondary">{{ $u->product->platform }} -
                                                        {{ $u->product->region }}</span>

                                                </td>
                                                <td>
                                                    <center>
                                                        <img width="15px" src="/{{ $u->box }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($u->box_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>
                                                </td>
                                                <td>
                                                    <center>
                                                        <img width="15px" src="/{{ $u->game }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($u->game_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>
                                                </td>
                                                <td>
                                                    <center>
                                                        <img width="15px" src="/{{ $u->extra }}"
                                                            data-toggle="popover"
                                                            data-content="{{ __(App\AppOrgUserInventory::getCondicionName($u->extra_condition)) }}"
                                                            data-placement="top" data-trigger="hover">
                                                    </center>
                                                </td>
                                                @if ($u->images->first())
                                                    <td class="text-center product-cart-img item">

                                                        <a href="{{ url('/uploads/inventory-images/' . $u->images->first()->image_path) }}"
                                                            class="fancybox" data-fancybox="{{ $u->id }}">
                                                            <img src="{{ url('/uploads/inventory-images/' . $u->images->first()->image_path) }}"
                                                                width="50px" height="50px" />
                                                        </a>


                                                        @foreach ($u->images as $p)

                                                            @if (!$loop->first)
                                                                <a href="{{ '/uploads/inventory-images/' . $p->image_path }}"
                                                                    data-fancybox="{{ $u->id }}">
                                                                    <img src="{{ url('/uploads/inventory-images/' . $p->image_path) }}"
                                                                        width="0px" height="0px"
                                                                        style="position:absolute;" />
                                                                </a>
                                                            @endif


                                                        @endforeach

                                                    </td>
                                                @else
                                                    <td class="product-price">
                                                        <font color="black">
                                                            <i class="fa fa-times" data-toggle="popover"
                                                                data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                                data-trigger="hover"></i>
                                                        </font>
                                                    </td>
                                                @endif
                                                <td>
                                                    @if ($u->comments == '')
                                                        <center>
                                                            <font color="black">
                                                                <i class="fa fa-times" data-toggle="popover"
                                                                    data-content="@lang('messages.not_working_profile')" data-placement="top"
                                                                    data-trigger="hover"></i>
                                                            </font>
                                                        </center>
                                                    @else
                                                        <center>
                                                            <font color="black">
                                                                <i class="fa fa-comment" data-toggle="popover"
                                                                    data-content="{{ $u->comments }}"
                                                                    data-placement="top" data-trigger="hover"></i>
                                                            </font>
                                                        </center>

                                                    @endif
                                                </td>
                                                <td>
                                                    <font color="green"> <span
                                                        class="text-base text-right retro font-extrabold color-primary text-nowrap">
                                                        {{ number_format($u->price, 2) }}
                                                        € </span> </font>
                                                </td>
                                                <td class="flex items-center product-name md:justify-between">
                                                    @if ($u->user_id != Auth::id())
                                                        <center>
                                                            <font color="black">
                                                                <div class="text-left">
                                                                    <select
                                                                        class="block w-full mt-1 text-gray-900 retro form-select br-btn-product-qty d-inline">
                                                                        @for ($i = 1; $i <= $u->quantity; $i++)
                                                                            <option value="{{ $i }}">
                                                                                {{ $i }}</option>
                                                                        @endfor
                                                                    </select>

                                                                </div>
                                                            </font>
                                                        </center>
                                                    @endif

                                                    @if (Auth::user())
                                                        @if ($u->user_id != Auth::id())
                                                            <button
                                                                style="color:rgb(3, 3, 3);  background:lighten(#292d48,65);"
                                                                class="br-btn-product-add-merchandising"
                                                                data-inventory-id="{{ $u->id + 1000 }}"
                                                                data-inventory-qty="{{ $u->quantity }}"> <a
                                                                    class="wishlist-btn" href="#"> <i
                                                                        class="fa fa-cart-plus"></i></a> </button>


                                                        @else


                                                            <center>

                                                                <div class="text-left">

                                                                    <select
                                                                        class="block w-full mt-1 text-gray-900 retro form-select br-btn-product-qty d-inline">
                                                                        @for ($i = 1; $i <= $u->quantity; $i++)
                                                                            <option value="{{ $i }}">
                                                                                {{ $i }}</option>
                                                                        @endfor
                                                                    </select>

                                                                </div>

                                                                <button
                                                                    data-href="{{ url('/account/inventory/update/' . ($u->id + 1000)) }}"
                                                                    data-pro="{{ $u->id + 1000 }}"
                                                                    class="wishlist-btn nk-btn-modify-inventory">
                                                                    <font color="white"><i class="fa fa-pencil"></i>
                                                                    </font>
                                                                </button>
                                                            </center>


                                                        @endif
                                                    @else
                                                        <a href="#"
                                                            class="nk-btn nk-btn-xs nk-btn-color-white showLogin"><i
                                                                class="fa fa-cart-plus"></i></a>
                                                    @endif


                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>

                            <!-- Modal -->
                            <div wire:ignore.self class="modal fade" id="merchadisingModal" tabindex="-1"
                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <center>
                                                <h5 class="modal-title retro-sell" id="exampleModalLabel">@lang('messages.confirm_remove')
                                                </h5>
                                            </center>
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                                <span aria-hidden="true close-btn">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <br>
                                            <h5 class="retro-sell text-white">
                                                <center>@lang('messages.are_you_sure_product')</center>
                                            </h5>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="nes-btn is-warning retro-sell close-btn"
                                                data-dismiss="modal">@lang('messages.closes')</button>
                                            <button type="button" wire:click.prevent="delete()"
                                                class="nes-btn retro-sell is-error close-modal" data-dismiss="modal">@lang('messages.yes_closes')</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="flex flex-col items-center">
                                @if ($ari->count())
                                    <div
                                        class="flex items-center justify-between px-4 py-3 border-t border-gray-200 bg-red sm:px-6">
                                        {{ $ari->links('pagination') }}
                                    </div>
                                @else
                                <div class="row">
                                    <div class="col-lg-12">
                                        <blockquote class="nk-blockquote">
                                            <div class="nk-blockquote-icon"></div>
                                            <div class="text-center nk-blockquote-content h4 retro-not-inventory">
                                                <font color="black">
                                                    @lang('messages.not_available')
                                                </font>
                                            </div>
                                            <div class="nk-gap"></div>
                                            <div class="nk-blockquote-author"></div>
                                        </blockquote>
                                    </div>
                                </div>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener('livewire:load', function() {
            var $inventoryAdd = $('.br-btn-product-add-merchandising');
            $(function() {
                    $('[data-toggle="popover"]').popover()
                }),
                $('.nk-btn-modify-inventory').click(function() {
                    var url = $(this).data('href');
                    var tr = $(this).closest('tr');
                    var sel = $(this).closest('tr').find('select').prop('value');
                    console.log(sel);
                    //console.log(url);
                    location.href = `${url}/${sel}`;
                }),
                $inventoryAdd.click(function() {
                    var inventoryId = $(this).data('inventory-id');
                    var inventoryQty = $(this).closest('tr').find('select.br-btn-product-qty').val();
                    var stock = $(this).closest('tr').find('div.qty-new');
                    var tr = $(this).closest('tr');
                    var select = $(this).closest('tr').find('select.br-btn-product-qty');

                    $.showBigOverlay({
                        message: '{{ __('Agregando producto a tu carro de compras') }}',
                        onLoad: function() {
                            $.ajax({
                                url: '{{ url('/cart/add_item') }}',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr(
                                        'content'),
                                    inventory_id: inventoryId,
                                    inventory_qty: inventoryQty,
                                },
                                dataType: 'JSON',
                                type: 'POST',
                                success: function(data) {
                                    if (data.error == 0) {
                                        $('#br-cart-items').text(data.items);
                                        stock.html(data.Qty);
                                        if (data.Qty == 0) {
                                            tr.remove();
                                        } else {
                                            select.html('');
                                            for (var i = 1; i < data.Qty +
                                                1; i++) {
                                                select.append('<option value=' +
                                                    i + '>' + i +
                                                    '</option>');
                                            }
                                        }
                                    } else {

                                    }
                                    $.showBigOverlay('hide');
                                },
                                error: function(data) {
                                    $.showBigOverlay('hide');
                                }
                            })
                        }
                    });
                });
        })
    </script>
</div>
