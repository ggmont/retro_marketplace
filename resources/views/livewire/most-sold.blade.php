<div>
    <section class="bestseller-product mb-55">

        <div class="row">
            <div class="col-lg-12">
                <!--Section Title1 Start-->
                <div class="section-title1-border">
                    <div class="section-title1">
                        <font color="#ffd3b1">
                            <h3 class="retro">Productos mas vendidos <i class="nes-logo"></i></h3>
                        </font>
                    </div>
                </div>
                <!--Section Title1 End-->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!--Product Tab Menu Start-->
                <div class="product-tab-menu-area">
                    <div class="product-tab">
                        <ul class="nav">
                            <li><a class="active" data-toggle="tab" href="#amply">
                                    <h3 class="retro"><b>TOP
                                            10</b></h3>
                                </a></li>
                        </ul>
                    </div>
                </div>
                <!--Product Tab Menu End-->
            </div>
        </div>
        <!--Product Tab Start-->

        <div class="tab-content">
            <div id="amply" class="tab-pane fade show active">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="nk-carousel nk-carousel-x4" data-autoplay="5000" data-dots="false"
                            data-cell-align="left" data-arrows="true">
                            <div class="nk-carousel-inner">

                                @isset($inventory_most_sold)
                                    @foreach ($inventory_most_sold as $cItem)
                                        @if ($cItem->product_id > 0)

                                            <div>
                                                <div class="col-lg-12 item-col">
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="single-product.html">
                                                                <img class="first-img"
                                                                    src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                                    alt="">
                                                                <img class="hover-img"
                                                                    src="{{ asset('assets/images/art-not-found.jpg') }}"
                                                                    alt="">
                                                            </a>
                                                            <ul class="product-action">
                                                                <li><a href="#" data-toggle="modal" title="Quick View"
                                                                        data-target="#myModal"><i
                                                                            class="ion-android-expand"></i></a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-content">
                                                            <h2 class="retro"><a
                                                                    href="single-product.html">{{ str_limit($cItem->product->name, 25) }}</a>
                                                            </h2>


                                                            <div class="product-price">
                                                                <span
                                                                    class=""><b>{{ $cItem->product->platform . ' - ' . $cItem->product->region }}</b></span>
                                                                <span
                                                                    class="new-price">{{ number_format($cItem->price, 2) . '€' }}</span>
                                                                <a class="button add-btn" href="#" data-toggle="tooltip"
                                                                    title="Add to Cart">add to cart</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                @endisset


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--Product Tab End-->
</div>

<div class="offer-area mb-100">

    <div class="row">
        <div class="col-lg-12">
            <div class="single-offer">
                <div class="offer-img img-full">
                    <a href="#">
                        <img src="{{ asset('images/3.jpg') }}" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>

</div>


<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <!--Section Title1 Start-->
            <div class="section-title1-border">
                <div class="section-title1">
                    <h3 class="retro">CATEGORIAS</h3>
                </div>
            </div>
            <!--Section Title1 End-->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!--Product Tab Menu Start-->
            <div class="product-tab-menu-area">
                <div class="product-tab">
                    <ul class="nav">
                        <li><button type="button" class="retro nes-btn is-error">
                                <font color="#ffd3b1">VER TODO</font>
                            </button></li>
                    </ul>
                </div>
            </div>
            <!--Product Tab Menu End-->
        </div>
    </div>
    <!--Hot Categories Start-->
    <div class="row hot-categories">
        <!--Single Categories Start-->
        <div class="col-lg-6 col-md-6">
            <div class="single-categories">
                <div class="categories-img img-full">
                    <a href="#"><img src="{{ asset('img/productos/juegos.png') }}" alt=""></a>
                </div><br>
                <div class="categories-content">
                    <center>
                        <h3 class="retro"><a href="#">JUEGOS</a></h3>
                    </center>
                </div>
            </div>
        </div>
        <!--Single Categories End-->
        <!--Single Categories Start-->
        <div class="col-lg-6 col-md-6">
            <div class="single-categories">
                <div class="categories-img img-full">
                    <a href="#"><img src="{{ asset('img/productos/consolas.png') }}" alt=""></a>
                </div><br>
                <div class="categories-content">
                    <center>
                        <h3 class="retro"><a href="#">CONSOLAS</a></h3>
                        <br>
                    </center>
                </div>
            </div>
        </div>
        <!--Single Categories End-->
        <!--Single Categories Start-->
        <div class="col-lg-4 col-md-6">
            <div class="single-categories">
                <div class="categories-img img-full">
                    <a href="#"><img src="{{ asset('img/productos/perifericos.png') }}" alt=""></a>
                </div><br>
                <div class="categories-content">
                    <center>
                        <h3 class="retro"><a href="#">perifericos</a></h3>
                    </center>
                </div>
            </div>
        </div>
        <!--Single Categories End-->
        <!--Single Categories Start-->
        <div class="col-lg-4 col-md-6">
            <div class="single-categories">
                <div class="categories-img img-full">
                    <a href="#"><img src="{{ asset('img/productos/accesorios.png') }}" alt=""></a>
                </div><br>
                <div class="categories-content">
                    <center>
                        <h3 class="retro"><a href="#">Accesorios</a></h3>
                    </center>
                </div>
            </div>
        </div>
        <!--Single Categories End-->
        <!--Single Categories Start-->
        <div class="col-lg-4 col-md-6">
            <div class="single-categories">
                <div class="categories-img img-full">
                    <a href="#"><img src="{{ asset('img/productos/merchandising.png') }}" alt=""></a>
                </div><br>
                <div class="categories-content">
                    <center>
                        <h3 class="retro"><a href="#">Merchadising</a></h3>
                    </center>
                </div>
            </div>
        </div>
        <!--Single Categories End-->
    </div>
    <!--Hot Categories End-->
</div>
</section>
</div>
