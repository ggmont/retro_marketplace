<div>

    <div class="nk-gap"></div>
    <div class="col-md-12">
        <input wire:model="search"
            class="block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500"
            type="text" placeholder="@lang('messages.search_by_name')">
        <div class="row">
            <div class="col-md-6">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase retro" for="grid-state">
                    @lang('messages.category_two')
                </label>
                <div class="relative">
                    <div class="nes-select">

                        <select wire:model="search_category" class="retro">
                            <option value="" selected>@lang('messages.alls')
                                <optgroup label="@lang('messages.category')">
                                    <option value="Juegos">@lang('messages.games')</option>
                                    <option value="Consolas">@lang('messages.consoles')</option>
                                    <option value="Periféricos">@lang('messages.peripherals')</option>
                                    <option value="Accesorios">@lang('messages.accesories')</option>
                                    <option value="Merchandising">Merchandising</option>
                                </optgroup>
                                <optgroup label="@lang('messages.sub_category')">
                                    <option value="Mandos">@lang('messages.controls')</option>
                                    <option value="Micrófonos">@lang('messages.microphones')</option>
                                    <option value="Teclados">@lang('messages.keyboard')</option>
                                    <option value="Fundas">@lang('messages.funded')</option>
                                    <option value="Cables">@lang('messages.cable')</option>
                                    <option value="Cargadores">@lang('messages.chargers')</option>
                                    <option value="Merchandising -> Logos 3d">@lang('messages.3d_logo')</option>
                                </optgroup>
                        </select>

                    </div>

                </div>
            </div>

            <div class="col-md-6">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase retro"
                    for="grid-state">
                    @lang('messages.platform')
                </label>
                <div class="relative">
                    <div class="nes-select">

                        <select wire:model="search_platform" class="retro">
                            <option value="" selected>@lang('messages.alls')
                            </option>
                            @foreach ($platform as $p)
                                <option value="{{ $p->value }}">{{ $p->value }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
            </div>

            <div class="col-md-4">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase retro"
                    for="grid-state">
                    Region
                </label>
                <div class="relative">
                    <div class="nes-select">

                        <select wire:model="search_region" class="retro">
                            <option value="" selected>@lang('messages.alls')
                            </option>
                            @foreach ($region as $r)
                                <option value="{{ $r->value_id }}">{{ $r->value_id }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                </div>
            </div>

            <div class="col-md-4">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase retro"
                    for="grid-state">
                    @lang('messages.language_advanced')
                </label>
                <div class="relative">
                    <div class="nes-select">

                        <select wire:model="search_language" class="retro">
                            <option value="" selected>@lang('messages.alls')
                            </option>
                            @foreach ($language as $l)
                                <option value="{{ $l->value }}">{{ $l->value }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
            </div>

            <div class="col-md-4">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase retro"
                    for="grid-state">
                    Media
                </label>
                <div class="relative">
                    <div class="nes-select">

                        <select wire:model="search_media" class="retro">
                            <option value="" selected>@lang('messages.alls')
                            </option>
                            @foreach ($media as $m)
                                <option value="{{ $m->value }}">{{ $m->value }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
            </div>




        </div>

    </div>
    <div class="mt-20 wishlist-table-area mb-50">
        <div class="col-lg-12">
            <div class="my-6 bg-white rounded shadow-md">
                <div class="wishlist-table table-responsive">
                    <table class="w-full table-auto min-w-max">
                        <thead>
                            <tr class="text-sm leading-normal text-gray-100 uppercase bg-red-700">
                                <th class="px-6 py-3 text-center"><i class="fa fa-image"></i></th>
                                <th class="px-6 py-3 text-center">@lang('messages.name')</th>
                                <th class="px-6 py-3 text-center">@lang('messages.platform')</th>
                                <th class="px-6 py-3 text-center">@lang('messages.language_advanced')</th>
                                <th class="px-6 py-3 text-center">Region</th>
                                <th class="px-6 py-3 text-center">@lang('messages.available')</th>
                                <th class="px-6 py-3 text-center">@lang('messages.price_prod')</th>
                            </tr>
                        </thead>
                        <tbody class="text-sm font-light text-gray-600">
                            @foreach ($product as $p)
                                <tr class="border-b border-gray-500 hover:bg-gray-200">
                                    <td class="px-6 py-3 text-center">
                                        <center>
                                            <a href="{{ route('product-show', $p->id) }}" target="_blank">
                                                @if ($p->images->first())
                                                    <img width="80px" height="80px"
                                                        src="{{ count($p->images) > 0 ? url('/images/' . $p->images->first()->image_path) : url('assets/images/art-not-found.jpg') }}">
                                                @else
                                                    <img width="80px" height="80px"
                                                        src="{{ url('assets/images/art-not-found.jpg') }}">
                                                @endif
                                            </a>
                                        </center>

                                    </td>
                                    <td class="px-6 py-3 text-left">
                                        <a href="{{ route('product-show', $p->id) }}" target="_blank">
                                            <span class="text-sm retro font-bold color-primary">
                                                <font color="black"><b>{{ $p->name }}
                                                        @if ($p->name_en)
                                                            <br><small>{{ $p->name_en }}</small>
                                                        @endif
                                                    </b>
                                                </font>
                                            </span>
                                        </a>
                                    </td>
                                    <td class="px-6 py-3 text-center">
                                        <span class="text-sm font-weight-bold color-primary">
                                            <font color="black">{{ $p->platform }}
                                            </font>
                                        </span>
                                    </td>
                                    <td class="px-6 py-3 text-center">
                                        <span class="text-sm font-weight-bold color-primary">
                                            @if ($p->language == '')
                                                <font color="black">No info</font>
                                            @endif
                                            <font color="black">{{ $p->language }}
                                            </font>
                                        </span>
                                    </td>
                                    <td class="px-6 py-3 text-center">
                                        <span class="text-sm font-weight-bold color-primary">
                                            <font color="black">{{ $p->region }}
                                            </font>
                                        </span>
                                    </td>
                                    <td class="px-6 py-3 text-center">
                                        <?php
                                        $duration = [];
                                        
                                        foreach ($p->inventory as $item) {
                                            $cantidad = $item->quantity;
                                            $duration[] = $cantidad;
                                        }
                                        
                                        $total = array_sum($duration);
                                        ?>
                                        <span class="text-sm font-weight-bold color-primary">
                                            @if ($total == 0)
                                                <font color="black">No info</font>
                                            @else
                                                <font color="black">{{ $total }}
                                                </font>
                                            @endif
                                        </span>
                                    </td>

                                    <td class="px-6 py-3 text-center">
                                        <?php
                                        $duration = [];
                                        $t = '';
                                        $total_numeros = '';
                                        $suma = '';
                                        $m = '';
                                        
                                        foreach ($p->inventory as $item) {
                                            $cantidad = $item->price;
                                            $duration[] = $cantidad;
                                            $total_numeros = count($duration);
                                            $total = max($duration);
                                            $t = min($duration);
                                            $suma = array_sum($duration);
                                            $m = $suma / $total_numeros;
                                        }
                                        
                                        ?>
                                        <span class="text-base retro font-extrabold color-primary">
                                            @if ($total_numeros == 0)
                                                <font color="black">No info</font>
                                            @else
                                                <font color="green">{{ number_format($m, 2) }}
                                                    €
                                                </font>
                                            @endif
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="flex flex-col items-center bg-white">
                        @if ($product->count())
                            <div
                                class="flex items-center justify-between px-4 py-3 border-t border-gray-200 bg-red sm:px-6">
                                {{ $product->links('pagination') }}
                            </div>
                        @else
                            <div class="row">
                                <div class="col-lg-12">
                                    <blockquote class="nk-blockquote">
                                        <div class="nk-blockquote-icon"></div>
                                        <div class="text-center nk-blockquote-content h4 retro">
                                            <font color="black">
                                                {{ __('No hay inventario disponible para esta seccion') }}
                                            </font>
                                        </div>
                                        <div class="nk-gap"></div>
                                        <div class="nk-blockquote-author"></div>
                                    </blockquote>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
