<div>

    <div class="page-content-wrapper-pro">
        <div class="container">

            <div class="mb-4">
                <div class="row no-gutters align-items-center justify-content-end my-2">
                    <div class="col-12 col-md-auto text-center text-md-right pr-md-2 mb-2 mb-md-0">
                        <span class="personalInfo-light font-extrabold">Evaluación general</span>
                    </div>
                    <div class="col-4 col-md">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <img src="{{ asset('img/new.png') }}" width="20px" data-toggle="popover"
                                    data-content="@lang('messages.good')" data-placement="top" data-trigger="hover">
                            </div>
                            <div class="col-auto">
                                <span class="ml-1 mt-n1">
                                    {{ number_format($sc1 * 100, 2) }} %
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 col-md">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <img src="{{ asset('img/used.png') }}" width="20px" data-toggle="popover"
                                    data-content="Neutral" data-placement="top" data-trigger="hover">
                            </div>
                            <div class="col-auto">
                                <span class="ml-1 mt-n1">
                                    {{ number_format($sc2 * 100, 2) }} %
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 col-md">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <img src="{{ asset('img/no-funciona.png') }}" width="20px" data-toggle="popover"
                                    data-content="@lang('messages.bad')" data-placement="top" data-trigger="hover">
                            </div>
                            <div class="col-auto">
                                <span class="ml-1 mt-n1">
                                    {{ number_format($sc3 * 100, 2) }} %
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <br>


                <div class="row no-gutters align-items-center justify-content-end my-2">
                    <div class="col-12 col-md-auto text-center text-md-right pr-md-2 mb-2 mb-md-0">
                        <span class="personalInfo-light font-extrabold">Evaluación general</span>
                    </div>
                    <div class="col-4 col-md">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <img src="{{ asset('img/new.png') }}" width="20px" data-toggle="popover"
                                    data-content="@lang('messages.good')" data-placement="top" data-trigger="hover">
                            </div>
                            <div class="col-auto">
                                <span class="ml-1 mt-n1">
                                    {{ number_format($sc1 * 100, 2) }} %
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 col-md">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <img src="{{ asset('img/used.png') }}" width="20px" data-toggle="popover"
                                    data-content="Neutral" data-placement="top" data-trigger="hover">
                            </div>
                            <div class="col-auto">
                                <span class="ml-1 mt-n1">
                                    {{ number_format($sc2 * 100, 2) }} %
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 col-md">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <img src="{{ asset('img/no-funciona.png') }}" width="20px" data-toggle="popover"
                                    data-content="@lang('messages.bad')" data-placement="top" data-trigger="hover">
                            </div>
                            <div class="col-auto">
                                <span class="ml-1 mt-n1">
                                    {{ number_format($sc3 * 100, 2) }} %
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <br>

                <div class="row no-gutters align-items-center justify-content-end my-2">
                    <div class="col-12 col-md-auto text-center text-md-right pr-md-2 mb-2 mb-md-0">
                       <span class="personalInfo-light font-extrabold">Evaluación general</span>
                    </div>
                    <div class="col-4 col-md">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <img src="{{ asset('img/new.png') }}" width="20px" data-toggle="popover"
                                    data-content="@lang('messages.good')" data-placement="top" data-trigger="hover">
                            </div>
                            <div class="col-auto">
                                <span class="ml-1 mt-n1">
                                    {{ number_format($sc1 * 100, 2) }} %
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 col-md">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <img src="{{ asset('img/used.png') }}" width="20px" data-toggle="popover"
                                    data-content="Neutral" data-placement="top" data-trigger="hover">
                            </div>
                            <div class="col-auto">
                                <span class="ml-1 mt-n1">
                                    {{ number_format($sc2 * 100, 2) }} %
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 col-md">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <img src="{{ asset('img/no-funciona.png') }}" width="20px" data-toggle="popover"
                                    data-content="@lang('messages.bad')" data-placement="top" data-trigger="hover">
                            </div>
                            <div class="col-auto">
                                <span class="ml-1 mt-n1">
                                    {{ number_format($sc3 * 100, 2) }} %
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Repite la estructura anterior para las siguientes filas -->
            </div>




            <div class="card-body">
                <div class="table-responsive">
                    <table class="table mb-0 mx-auto">
                        <thead>
                            <tr>
                                <th scope="col">Usuario</th>
                                <th scope="col">Comentario</th>
                                <th scope="col">Velocidad de Procesamiento</th>
                                <th scope="col">Empaquetado</th>
                                <th scope="col">Descripción del Producto</th>
                                <th scope="col">Nota General</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($rating as $key)
                                <tr class="text-center shadow-sm">
                                    <td class="text-center">
                                        @if (App\SysUserRoles::where('user_id', $key->buyer->id)->whereIn('role_id', [2])->count() > 0)
                                            <a href="{{ route('user-info', $key->buyer->user_name) }}"
                                                target="_blank"
                                                style="color: #000000">{{ $key->buyer->user_name }}</a>
                                        @else
                                            <a href="{{ route('user-info', $key->buyer->user_name) }}"
                                                target="_blank"
                                                style="color: #000000">{{ $key->buyer->user_name }}</a>
                                        @endif <span class="flag-icon flag-icon-esp"></span> <br>
                                        {{ date('Y-m-d', strtotime($key->created_at)) }}
                                    </td>
                                    <td class="text-center">
                                        @if ($view_rating_description)
                                            {{ $view_rating_description }}
                                        @else
                                            Sin comentarios
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if ($view_rating_processig == 1)
                                            <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Bueno" data-placement="top"
                                                data-trigger="hover">
                                        @elseif ($view_rating_processig == 2)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @else
                                            <img src="{{ asset('img/no-funciona.png') }}" width="17"
                                                height="16" data-toggle="popover" data-content="Malo"
                                                data-placement="top" data-trigger="hover">
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if ($view_rating_packaging == 1)
                                            <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Bueno" data-placement="top"
                                                data-trigger="hover">
                                        @elseif ($view_rating_packaging == 2)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @else
                                            <img src="{{ asset('img/no-funciona.png') }}" width="17"
                                                height="16" data-toggle="popover" data-content="Malo"
                                                data-placement="top" data-trigger="hover">
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if ($view_rating_desc_prod == 1)
                                            <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Bueno" data-placement="top"
                                                data-trigger="hover">
                                        @elseif ($view_rating_desc_prod == 2)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @else
                                            <img src="{{ asset('img/no-funciona.png') }}" width="17"
                                                height="16" data-toggle="popover" data-content="Malo"
                                                data-placement="top" data-trigger="hover">
                                        @endif
                                    </td>
                                    <td>
                                        @if ($key->packaging == 1 and $key->processig == 1 and $key->desc_prod == 1)
                                            <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Bueno" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 2 and $key->processig == 1 and $key->desc_prod == 1)
                                            <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Bueno" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 1 and $key->processig == 2 and $key->desc_prod == 1)
                                            <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Bueno" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 1 and $key->processig == 1 and $key->desc_prod == 2)
                                            <img src="{{ asset('img/new.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Bueno" data-placement="top"
                                                data-trigger="hover">

                                            {{-- CARA AMARILLA --}}
                                        @elseif($key->packaging == 3 and $key->processig == 1 and $key->desc_prod == 1)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 1 and $key->processig == 3 and $key->desc_prod == 1)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 1 and $key->processig == 1 and $key->desc_prod == 3)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 2 and $key->processig == 2 and $key->desc_prod == 1)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 1 and $key->processig == 2 and $key->desc_prod == 2)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 2 and $key->processig == 1 and $key->desc_prod == 2)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 1 and $key->processig == 2 and $key->desc_prod == 3)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 2 and $key->processig == 1 and $key->desc_prod == 3)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 3 and $key->processig == 2 and $key->desc_prod == 1)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 1 and $key->processig == 3 and $key->desc_prod == 2)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 1 and $key->processig == 3 and $key->desc_prod == 2)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 2 and $key->processig == 2 and $key->desc_prod == 2)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 2 and $key->processig == 3 and $key->desc_prod == 2)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 3 and $key->processig == 2 and $key->desc_prod == 2)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 2 and $key->processig == 2 and $key->desc_prod == 3)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">
                                        @elseif($key->packaging == 3 and $key->processig == 2 and $key->desc_prod == 2)
                                            <img src="{{ asset('img/used.png') }}" width="17" height="16"
                                                data-toggle="popover" data-content="Neutral" data-placement="top"
                                                data-trigger="hover">

                                            {{-- CARA ROJA --}}
                                        @elseif($key->packaging == 3 and $key->processig == 3 and $key->desc_prod == 3)
                                            <img src="{{ asset('img/no-funciona.png') }}" width="17"
                                                height="16" data-toggle="popover" data-content="Malo"
                                                data-placement="top" data-trigger="hover">
                                        @elseif($key->packaging == 3 and $key->processig == 3 and $key->desc_prod == 2)
                                            <img src="{{ asset('img/no-funciona.png') }}" width="17"
                                                height="16" data-toggle="popover" data-content="Malo"
                                                data-placement="top" data-trigger="hover">
                                        @elseif($key->packaging == 3 and $key->processig == 2 and $key->desc_prod == 3)
                                            <img src="{{ asset('img/no-funciona.png') }}" width="17"
                                                height="16" data-toggle="popover" data-content="Malo"
                                                data-placement="top" data-trigger="hover">
                                        @elseif($key->packaging == 2 and $key->processig == 3 and $key->desc_prod == 3)
                                            <img src="{{ asset('img/no-funciona.png') }}" width="17"
                                                height="16" data-toggle="popover" data-content="Malo"
                                                data-placement="top" data-trigger="hover">
                                        @elseif($key->packaging == 1 and $key->processig == 3 and $key->desc_prod == 3)
                                            <img src="{{ asset('img/no-funciona.png') }}" width="17"
                                                height="16" data-toggle="popover" data-content="Malo"
                                                data-placement="top" data-trigger="hover">
                                        @elseif($key->packaging == 3 and $key->processig == 3 and $key->desc_prod == 1)
                                            <img src="{{ asset('img/no-funciona.png') }}" width="17"
                                                height="16" data-toggle="popover" data-content="Malo"
                                                data-placement="top" data-trigger="hover">
                                        @elseif($key->packaging == 3 and $key->processig == 1 and $key->desc_prod == 3)
                                            <img src="{{ asset('img/no-funciona.png') }}" width="17"
                                                height="16" data-toggle="popover" data-content="Malo"
                                                data-placement="top" data-trigger="hover">
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>

</div>
