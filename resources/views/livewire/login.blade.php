<div>
    <form wire:submit.prevent="doLogin" class="form-horizontal">
        @csrf

        @error('user_name')
            <span class="error">{!! $message !!}</span>
        @enderror


        <div class="form-group boxed relative">
            <div class="input-wrapper">
                <input type="text" wire:model.lazy="user_name" id="user_name" name="user_name" class="form-control"
                    placeholder="Usuario">
                <i class="clear-input" wire:click="$set('user_name', '')">
                    <ion-icon name="close-circle"></ion-icon>
                </i>
            </div>
        </div>

        <div class="form-group boxed relative">
            <div class="input-wrapper">
                <input type="password" wire:model.lazy="password" id="password" name="password" class="form-control"
                    placeholder="Contraseña">
                <!-- Botón para mostrar/ocultar la contraseña -->
                <button type="button" class="password-toggle absolute right-0 top-0 mt-3 mr-2">
                    <i
                        class="{{ $passwordFieldType === 'password' ? 'fas fa-eye' : 'fas fa-eye-slash' }} text-gray-400 hover:text-gray-500"></i>
                </button>
            </div>
            @error('password')
                <span class="error">{{ $message }}</span>
            @enderror
        </div>

        <div class="flex justify-end">
            <a class="forgot-password"  href="{{ route('account_problem') }}">
              Tengo problemas con mi cuenta  
            </a>
        </div>
        

        <div class="mt-4 text-start">
            <div class="form-check">
                <input type="checkbox" wire:model.lazy="rememberMe" name="remember_me" class="form-check-input"
                    id="customCheckb1" checked>
                <label class="form-check-label font-bold" for="customCheckb1">{{ __('Recordarme') }}</label>
            </div>
        </div>

        <br>

        <button class="btn btn-danger font-extrabold btn-lg w-100" type="submit" wire:loading.attr="disabled">
            <span wire:loading.remove>Iniciar Sesión</span>
            <span wire:loading>
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                <span class="sr-only">Loading...</span>
            </span>
        </button>
    </form>
</div>
