<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> {{ config('brcode.app_name_public') }} - 2.0</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="description" content="Retro Gaming Market - Portal games store">
    <meta name="keywords" content="game, gaming, Retro Gaming, Market, Retro">
    <meta name="author" content="RetroGamingMarket">
    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
    <link rel="icon" type="image/png" href="{{ asset('img/RGM.png') }}">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <style>
            .sicker {
                position: absolute;
                left: 50%;
                top: 10%;
                width: 137px;
                transform: translate(-50%, -50%);
                background: #00000094;
                color: #fff;
                line-height: 20px;
                border-radius: 2px;
                text-align: center;
                font-size: 12px;
                z-index: 991;
            }

            /*
      Rollover Image
     */
            .figure {
                position: relative;
                width: 360px;
                /* can be omitted for a regular non-lazy image */
                max-width: 100%;
            }

            .figure img.image-hover {
                position: absolute;
                top: 0;
                right: 0;
                left: 0;
                bottom: 0;
                object-fit: contain;
                opacity: 0;
                transition: opacity .2s;
            }

            .figure:hover img.image-hover {
                opacity: 1;
            }

            .modal-header button.close {
                position: absolute;
                right: 25px;
                opacity: 1;
                -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
                width: 30px;
                height: 30px;
                border: 1px solid #808080;
                border-radius: 5px;
                font-size: 16px;
                line-height: 25px;
                -webkit-transition: all 0.3s ease-in-out;
                transition: all 0.3s ease-in-out;
                z-index: 99;
                color: #808080;
                padding: 0;
            }

            .Buttons__login-btn {
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                position: relative;
                border: none;
                text-align: center;
                line-height: 34px;
                white-space: nowrap;
                border-radius: 21px;
                font-size: 1rem;
                color: #fff;
                width: 100%;
                height: 42px;
                display: -webkit-flex;
                display: -ms-flexbox;
                display: flex;
                -webkit-align-items: center;
                -ms-flex-align: center;
                align-items: center;
                -webkit-justify-content: center;
                -ms-flex-pack: center;
                justify-content: center;
                padding: 0;
                cursor: pointer;
            }

            .Buttons__login-btn--google {
                background: #dd4b39;
            }

            .prueba {
                position: relative;
                width: 100%;
                min-height: 100vh;
                z-index: 10;
                overflow-y: auto;
                padding-top: 1rem;
                padding-bottom: 1rem;
                overflow-x: hidden;
            }

            .w-full-ultra {
                width: 92%
            }


            .btn-ultra {
                /* height: 40px; */
                padding: 3px 18px;
                font-size: 13px;
                line-height: 1.2em;
                font-weight: 500;
                box-shadow: none !important;
                display: inline-flex;
                align-items: center;
                justify-content: center;
                transition: none;
                text-decoration: none !important;
                border-radius: 6px;
                border-width: 2px;
            }

            .form-control-ultra {
                display: block;
                width: 120%;
                padding: 0.375rem 0.75rem;
                font-size: 1rem;
                font-weight: 400;
                line-height: 1.5;
                color: #212529;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                border-radius: 0.25rem;
                -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            }
        </style>

        <!-- Dropzone css -->
        <link href="/assets/dropzone-master/dist/min/dropzone.min.css" rel="stylesheet" type="text/css" />
        <style>
            .dropzone .dz-preview .dz-error-message {
                top: 175px !important;
            }

            .searchbox .form-control-chat {
                height: 36px;
                border-radius: 6px;
                border: 1px solid #E1E1E1 !important;
                padding: 0 16px 0 36px;
                font-size: 15px;
                box-shadow: none !important;
                color: #141515;
            }

            .searchbox .form-control-chat:focus {
                border-color: #c8c8c8 !important;
            }

            .searchbox .form-control-chat:focus~.input-icon {
                color: #141515;
            }

            .form-control-chat {
                background-clip: padding-box;
                background-image: linear-gradient(transparent, transparent);
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
            }

            .form-group.basic .form-control-chat,
            .form-group.basic .custom-select {
                background: transparent;
                border: none;
                border-bottom: 1px solid #E1E1E1;
                padding: 0 30px 0 0;
                border-radius: 0;
                height: 40px;
                color: #141515;
                font-size: 15px;
            }

            .form-group.basic .form-control-chat:focus,
            .form-group.basic .custom-select:focus {
                border-bottom-color: #1E74FD;
                box-shadow: inset 0 -1px 0 0 #1E74FD;
            }

            .form-group.basic textarea.form-control-chat {
                height: auto;
                padding: 7px 40px 7px 0;
            }

            .form-group.boxed .form-control-chat.form-select,
            .form-group.basic .form-control-chat.form-select {
                background-image: url("data:image/svg+xml,%0A%3Csvg width='13px' height='8px' viewBox='0 0 13 8' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'%3E%3Cg id='Page-1' stroke='none' stroke-width='1' fill='none' fill-rule='evenodd' stroke-linecap='round' stroke-linejoin='round'%3E%3Cpolyline id='Path' stroke='%23A9ABAD' stroke-width='2' points='1.59326172 1.79663086 6.59326172 6.79663086 11.5932617 1.79663086'%3E%3C/polyline%3E%3C/g%3E%3C/svg%3E") !important;
                background-repeat: no-repeat !important;
                background-position: right center !important;
            }

            .form-group.boxed .form-control-chat.form-select {
                background-position: right 12px center !important;
            }

            .chatFooter .form-group .form-control-chat {
                font-size: 13px;
                border-radius: 300px;
                height: 40px;
            }

            .searchbox .form-control-chat {
                height: 36px;
                border-radius: 6px;
                border: 1px solid #E1E1E1 !important;
                padding: 0 16px 0 36px;
                font-size: 15px;
                box-shadow: none !important;
                color: #141515;
            }

            .searchbox .form-control-chat:focus {
                border-color: #c8c8c8 !important;
            }

            .searchbox .form-control-chat:focus~.input-icon {
                color: #141515;
            }

            #search .searchbox .form-control-chat {
                box-shadow: none !important;
                border: 0 !important;
                border-radius: 0;
                height: 56px;
                padding: 0 56px 0 56px;
                background: transparent;
                font-size: 17px;
                color: #141515;
                width: 100%;
            }

            #search .searchbox .form-control-chat:focus {
                border-color: #bbbbbb;
            }

            #search .searchbox .form-control-chat:focus~.input-icon {
                color: #141515;
            }

            .form-control-chat.is-valid,
            .was-validated .form-control-chat:valid {
                border-color: #34C759;
                box-shadow: 0 !important;
                background-image: none !important;
            }

            .form-control-chat.is-valid:focus,
            .was-validated .form-control-chat:valid:focus {
                border-color: #34C759;
                box-shadow: none !important;
            }

            .form-control-chat.is-invalid,
            .was-validated .form-control-chat:invalid {
                border-color: #EC4433;
                background-image: none !important;
            }

            .form-control-chat.is-invalid:focus,
            .was-validated .form-control-chat:invalid:focus {
                border-color: #EC4433;
                box-shadow: none !important;
            }

            .form-control-chat {
                display: block;
                width: 90%;
                padding: 0.375rem 0.75rem;
                font-size: 1rem;
                font-weight: 400;
                line-height: 1.5;
                color: #212529;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                border-radius: 0.25rem;
                -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            }

            img {
                max-width: 100%;
                height: auto;
            }

            .card-product-image-holder .card-product-image {
                object-fit: cover;
                height: 100%;
                width: 100%;
                -webkit-border-radius: 6px;
                border-radius: 6px;
            }

            .card-product-image-holder {
                width: 100%;
                max-width: 400px;
                height: 250px;
                border-radius: 5px;
                overflow: hidden;
                position: relative;
            }

            .element {
                display: inline-flex;
                align-items: center;
            }

            svg.bi bi-camera {
                margin: 10px;
                cursor: pointer;
                font-size: 30px;
            }

            svg:hover {
                opacity: 0.6;
            }
        </style>
        <link rel="stylesheet" href="{{ asset('css/mobile/core.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/affan.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/mobilekit.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/splide.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/animate.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
            integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
            crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/select2/select2.min.css') }}">
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <meta name="google-site-verification" content="uOvlTBBjltn6jz3EfQV11zsAEEHusnIZ9h-Buylgk6s" />
    @else
        <link rel="stylesheet" href="{{ asset('css/menu.css') }}">

        <!-- NES CSS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/nes/nes_prueba.css') }}">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="{{ asset('assets/css/desk.min.css') }}">

        <!-- FontAwesome -->
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}">

        <!-- IonIcons -->
        <link rel="stylesheet" href="{{ asset('assets/vendor/ionicons/css/ionicons.min.css') }}">

        <!-- Seiyria Bootstrap Slider -->
        <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-slider/dist/css/bootstrap-slider.min.css') }}">

        <!-- GoodGames -->
        <link rel="stylesheet" href="{{ asset('assets/css/goodgames.css') }}">

        <!-- Custom Styles -->
        <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">

        <link rel="stylesheet" href="{{ asset('assets/imageSelect/css/chosen.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/imageSelect/css/ImageSelect.css') }}">

        <link rel="stylesheet" href="{{ asset('assets/select2/select2.min.css') }}">
        @yield('content-css-include')
        <!-- END: Styles -->
        <link rel="stylesheet" href="{{ asset('assets/cookie/cookiealert.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/flag-icon/css/flag-icon.min.css') }}">
        <!-- Prueba -->
        <!-- Animate CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
        <!-- UI CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.min.css') }}">
        <!-- Chosen CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/chosen.css') }}">
        <!-- Meanmenu CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/meanmenu.min.css') }}">
        <!-- Normalize CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}">
        <!-- Nivo Slider CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/nivo-slider.css') }}">
        <!-- Owl Carousel CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
        <!-- EasyZoom CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/easyzoom.css') }}">
        <!-- Slick CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/slick.css') }}">
        <!-- Default CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/default.css') }}">
        <!-- Style CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <meta name="google-site-verification" content="uOvlTBBjltn6jz3EfQV11zsAEEHusnIZ9h-Buylgk6s" />
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
        <!-- jQuery -->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('headroom/headroom.min.js') }}"></script>
        <script src="{{ asset('js/menu.js') }}"></script>
    @endif
</head>

@if ((new \Jenssegers\Agent\Agent())->isMobile())
@include('partials.flash')
    <div class="header-area" id="headerArea">
        <div class="container h-100 d-flex align-items-center justify-content-between">
            <!-- Back Button-->
            <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
            <!-- Page Title-->
            <div class="page-heading">
                <h6 class="mb-0 font-extrabold">Registrar Usuario</h6>
            </div>
            <!-- Navbar Toggler-->

            @if (Auth::user())
                <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                    data-bs-target="#sidebarPanel">
                    <span></span><span></span><span></span>
                </div>
            @else
                <div class="suha-navbar-toggler mt-1 d-flex flex-wrap" data-bs-toggle="offcanvas"
                    data-bs-target="#sidebarPanel">
                    <span></span><span></span><span></span>
                </div>
            @endif
        </div>
    </div>

    <body class="bg-white">

        <div class="login-form">
            <div class="page-content-wrapper py-3">
                <div class="container">
                    <form action="{{ route('register-user-start') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="listview-title mt-2">Datos Personales</div>
                        <div class="section mt-2 mb-3">

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Nombre</label>
                                <div class="input-wrapper">
                                    <input type="text" class="form-control" name="register_first_name" placeholder="Su nombre"
                                        reqauired>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Apellido</label>
                                <div class="input-wrapper">
                                    <input type="text" class="form-control" name="register_last_name" placeholder="Su apellido"
                                        required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Teléfono (Opcional)</label>
                                <div class="input-wrapper">
                                    <input type="number" class="form-control" name="register_phone" placeholder="0000"
                                        reqauired>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                        </div>
                        <div class="listview-title mt-2">Dirección</div>
                        <div class="section mt-2 mb-3">

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Su dirección</label>
                                <div class="input-wrapper">
                                    <input type="text" class="form-control" name="register_address" placeholder="Su dirección"
                                        required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Su código postal</label>
                                <div class="input-wrapper">
                                    <input type="number" class="form-control" name="register_zipcode" placeholder="Codigo Postal"
                                        required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Ciudad</label>
                                <div class="input-wrapper">
                                    <input type="text" class="form-control" name="register_city" placeholder="Ciudad" required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">País</label>
                                <div class="input-wrapper">
                                    <select id="br-register-co-country" class="form-control br-ajs-select2"
                                        name="register_country" ng-model="brRegisterCountry" required
                                        data-br-placeholder="{{ __('Su país') }}" data-br-resolve="Y">
                                        <option style="display:none" value=""> @lang('messages.country_select')</option>
                                        @foreach ($viewData['countries'] as $country)
                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="listview-title mt-2">Datos de la cuenta</div>
                        <div class="section mt-2 mb-3">

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Correo Electronico</label>
                                <div class="input-wrapper">
                                    <input type="email" class="form-control" name="register_email" placeholder="rgm@gmail.com"
                                        required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Usuario</label>
                                <div class="input-wrapper">
                                    <input type="text" class="form-control" name="register_user_name" placeholder="usuario"
                                        required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Usuario de Twitch (Opcional)</label>
                                <div class="input-wrapper">
                                    <input type="text" class="form-control" name="register_twitch_user" placeholder="Usuario de twitch"
                                        reqauired>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>


                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Contraseña - Max 8.</label>
                                <div class="input-wrapper">
                                    <input type="password" class="form-control" name="register_password" placeholder="*******"
                                        required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>


                            <div class="form-group boxed">
                                <label class="form-label" for="name5">Captcha</label>
                                <div class="captcha">
                                    {!! NoCaptcha::renderJs() !!}
                                    {!! NoCaptcha::display() !!}
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class=" mt-1 text-start">
                                <div class="form-check">
                                    <input type="checkbox" name="brRegisterCoYearsOld" class="form-check-input" required
                                        id="brRegisterCoYearsOld">
                                    <label class="form-check-label" for="brRegisterCoYearsOld">Tengo mas de 18
                                        años</label>
                                </div>
                            </div>

                           

                            <div class="mt-3 text-start">
                                <div class="form-check">
                                    <input type="checkbox" name="brRegisterCoTerms" class="form-check-input" required
                                        id="brRegisterCoTerms">
                                    <label class="form-check-label" for="brRegisterCoTerms"> He leído y acepto los <a
                                            href="/site/terminos-y-condiciones">términos y condiciones & Política de
                                            privacidad</a></label>
                                </div>
                            </div>
                            <br>
                            <button id="submitLogin" class="btn mb-3 btn-danger font-extrabold btn-lg w-100"
                                type="submit">Registrarse</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </body>
@else

    <body>
        <!-- Create By Joker Banny -->
        <div class="min-h-screen bg-center bg-no-repeat bg-cover" style="background-image: url('/img/LOGIN.png')">
            <div class="flex justify-end">
                <div class="flex items-center justify-center w-1/2 min-h-screen bg-white">
                    <div>

                        <form action="{{ url('/TobeContinued/login') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div>
                                <span class="text-sm text-gray-900">Bienvenido</span>
                                <h1 class="text-2xl font-bold">Iniciar Sesion</h1>
                            </div>
                            <div class="mt-5">
                                <label class="block mb-2 text-md" for="password">Usuario</label>
                                <input class="w-full px-4 py-2 text-sm border-2 rounded-md outline-none"
                                    type="text" name="user_name" placeholder="RGM">
                            </div>
                            <div class="my-3">
                                <label class="block mb-2 text-md" for="email">Contraseña</label>
                                <input class="w-full px-4 py-2 text-sm border-2 rounded-md outline-none"
                                    type="password" name="password" placeholder="******">
                            </div>
                            <div class="flex justify-between">
                                <div>
                                    <label>
                                        <input type="radio" class="nes-radio" name="remember_me" />
                                        <span>{{ __('Recordarme') }}</span>
                                    </label>

                                </div>
                            </div>
                            <div class="">
                                <button type="submit"
                                    class="w-full py-2 mt-4 mb-3 text-white transition duration-100 bg-green-500 rounded-md nes-btn is-error hover:bg-green-400">Iniciar
                                    Sesion</button>

                            </div>
                        </form>
                        <p class="mt-8"> Modo Developer Temporal <span class="text-sm text-blue-600 cursor-pointer">
                                (Version de prueba)</span></p>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </body>
@endif

@if ((new \Jenssegers\Agent\Agent())->isMobile())
    <script src="{{ asset('js/mobile/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/mobile/waypoints.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/mobile/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/mobile/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('js/mobile/default/jquery.passwordstrength.js') }}"></script>
    <script src="{{ asset('js/mobile/wow.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jarallax.min.js') }}"></script>
    <script src="{{ asset('js/mobile/jarallax-video.min.js') }}"></script>
    <script src="{{ asset('js/mobile/default/dark-mode-switch.js') }}"></script>
    <script src="{{ asset('js/mobile/default/active.js') }}"></script>
    <script src="{{ asset('js/mobile/base.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://scripts.sirv.com/sirvjs/v3/sirv.js"></script>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    <script>
        
    </script>
    <script>
        // Trigger welcome notification after 5 seconds
        setTimeout(() => {
            notification('notification-welcome', 10000);
        }, 2000);
    </script>
        <script>
            // Trigger welcome notification after 5 seconds
            setTimeout(() => {
                notification('cookies-box', 10000);
            }, 2000);
        </script>
    <script>
        window.setMobileTable = function(selector) {
            // if (window.innerWidth > 600) return false;
            const tableEl = document.querySelector(selector);
            const thEls = tableEl.querySelectorAll('thead th');
            const tdLabels = Array.from(thEls).map(el => el.innerText);
            tableEl.querySelectorAll('tbody tr').forEach(tr => {
                Array.from(tr.children).forEach(
                    (td, ndx) => td.setAttribute('label', tdLabels[ndx])
                );
            });
        }

        $(function() {
            $('[data-toggle="popover"]').popover()
        })
    </script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="{{ asset('assets/jquery-number/jquery.number.min.js') }}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/load-image.all.min.js') }}"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/canvas-to-blob.min.js') }}"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{ asset('assets/jQuery-FileUpload/js/jquery.fileupload-image.js') }}"></script>

    <script src="{{ asset('assets/dropzone-master/dist/min/dropzone.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/quagga/0.12.1/quagga.js"></script>
    <script src="{{ asset('vendor/sweetalert2/sweetalert2.all.min.js') }}"></script>
@else
    <!-- GSAP -->
    <script src="{{ asset('assets/vendor/gsap/src/minified/TweenMax.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/gsap/src/minified/plugins/ScrollToPlugin.min.js') }}"></script>

    <!-- Popper -->
    <script src="{{ asset('assets/vendor/popper.js/dist/umd/popper.min.js') }}"></script>

    <!-- Bootstrap -->
    <script src="{{ asset('assets/bootstrap-4.4/js/bootstrap.min.js') }}"></script>


    <script src="{{ asset('assets/js/goodgames.min.js') }}"></script>

    <!-- Seiyria Bootstrap Slider -->
    <script src="{{ asset('assets/vendor/bootstrap-slider/dist/bootstrap-slider.min.js') }}"></script>

    @yield('content-script-include')

    <!-- GoodGames -->
    <script src="{{ asset('assets/js/goodgames-init.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-notify.min.js') }}"></script>


    <!--All Js JAVENIST Here-->
    <!--Jquery 1.12.4 ESTO HACE QUE EL CARRITO NO FUNCIONE-->



    <!--Popper-->
    <script src="{{ asset('js/javenist/popper.min.js') }}"></script>

    <!--Bootstrap-->
    <script src="{{ asset('js/javenist/bootstrap.min.js') }}"></script>

    <!--Imagesloaded-->
    <script src="{{ asset('js/javenist/imagesloaded.pkgd.min.js') }}"></script>

    <!--Isotope-->
    <script src="{{ asset('js/javenist/isotope.pkgd.min.js') }}"></script>
    <!--Ui js-->
    <script src="{{ asset('js/javenist/jquery-ui.min.js') }}"></script>

    <!--Countdown-->
    <script src="{{ asset('js/javenist/jquery.countdown.min.js') }}"></script>
    <!--Counterup-->
    <script src="{{ asset('js/javenist/jquery.counterup.min.js') }}"></script>

    <!--ScrollUp-->
    <script src="{{ asset('js/javenist/jquery.scrollUp.min.js') }}"></script>

    <!--Chosen js-->
    <script src="{{ asset('js/javenist/chosen.jquery.js') }}"></script>

    <!--Meanmenu js-->
    <script src="{{ asset('js/javenist/jquery.meanmenu.min.js') }}"></script>

    <!--Instafeed-->
    <script src="{{ asset('js/javenist/instafeed.min.js') }}"></script>

    <!--EasyZoom-->
    <script src="{{ asset('js/javenist/easyzoom.min.js') }}"></script>


    <!--Nivo Slider-->
    <script src="{{ asset('js/javenist/jquery.nivo.slider.js') }}"></script>

    <!--Waypoints-->
    <script src="{{ asset('js/javenist/waypoints.min.js') }}"></script>

    <!--Carousel-->
    <script src="{{ asset('js/javenist/owl.carousel.min.js') }}"></script>

    <!--Slick-->
    <script src="{{ asset('js/javenist/slick.min.js') }}"></script>

    <!--Wow-->
    <script src="{{ asset('js/javenist/wow.min.js') }}"></script>

    <!--Plugins-->
    <script src="{{ asset('js/javenist/plugins.js') }}"></script>

    <!--Main Js-->
    <script src="{{ asset('js/javenist/main.js') }}"></script>
@endif

<script src="{{ asset('assets/noty/packaged/jquery.noty.packaged.min.js') }}"></script>
<script src="{{ asset('assets/select2/select2.min.js') }}"></script>

<!-- Angular JS -->
<script src="{{ asset('assets/angularjs/angular.min.js') }}"></script>
<script src="{{ asset('assets/angularjs/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('assets/angularjs/ui-bootstrap-tpls-2.5.0.min.js') }}"></script>
<script src="{{ asset('brcode/js/app.public.angjs-module.js') }}"></script>
<script src="{{ asset('brcode/js/app.public.angjs-topsearch.js') }}"></script>
<script src="{{ asset('brcode/js/app.public.angjs-register.js') }}"></script>

<script src="{{ asset('assets/cookie/cookiealert.js') }}"></script>

<script src="{{ asset('assets/imageSelect/js/chosen.jquery.js') }}"></script>
<script src="{{ asset('assets/imageSelect/js/ImageSelect.jquery.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<script>
    $(".nk-lang-change").chosen({
        width: '100%'
    });
</script>
<script type="text/javascript">
    $('#reload').click(function() {
        $.ajax({
            type: 'GET',
            url: '/reload-captcha',
            success: function(data) {
                $(".captcha span").html(data.captcha);
            }
        });
    });
</script>
@if (Auth::user())
    <script>
        $('.nk-cart-toggle').click(function() {
            location.href = "/cart";
        });
    </script>
@endif
<script></script>
<script>
    $(document).ready(function() {


        // $('#registerForm').modal('show');
        // $('.bropdown-back > a').click();
        if ('<?= session('
            message_confirm ') ?>' != '') {
            $.notify({
                message: '<?= session('
                message_confirm ') ?>'
            }, {
                // settings
                type: '<?= session('
                type_msg ') ?>',
                placement: {
                    from: "bottom",
                    align: "right"
                },
            });
        }

    });
</script>

<script>
    $(function() {
        var globalMsg = '<?= session('
        message ') ?>';
        var xhr = null;
        if (globalMsg.length > 0) {
            var globalMsgType = '<?= session('
            messageType ') ?>';
            var buttons = [];

            if (globalMsgType == 'warning') {
                buttons = [{
                    addClass: 'btn btn-primary btn-sm text-center col-md-12',
                    text: 'OK',
                    onClick: function($noty) {
                        $noty.close();
                    }
                }, ]
            }
            presentNotyMessage(globalMsg, globalMsgType, buttons);

        }

    });

    function presentNotyMessage(message, type, buttons, max) {

        buttons = typeof buttons == undefined ? [] : buttons;
        max = typeof max == undefined ? 5 : max;

        notySettings = {
            layout: 'topCenter',
            text: message,
            theme: 'relax',
            type: type,
            maxVisible: max,
            timeout: buttons.length == 0 ? 4000 : false,
        }

        if (buttons.length > 0) {
            notySettings.buttons = buttons;
        }

        var n = noty(notySettings);
    }
</script>
@if (Auth::user())
    <script>
        function salir() {
            if ($('#br-cart-items').text() > 0) {
                $('#ModalSingOut').modal('show');
            } else {
                location.href = '{{ url('/account/logout') }}';
            }
        }
    </script>

    <script>
        // $('#vender').click();
        $(".select2-ajax").select2({
            minimumInputLength: 3,
            width: '100%',
            // tags: [],
            ajax: {
                url: "{{ route('ajaxProduct') }}",
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function(term) {
                    return {
                        term: term
                    };
                },
                processResults: function(data) {
                    console.log(data);
                    return {
                        results: $.map(data, function(item) {

                            return {
                                text: `${item.name} - ${item.platform}  - ${item.region} `,
                                id: item.id,
                            }
                        })
                    };
                }
            }
        });

        $(".select2-ajax").change(function() {
            if ($(this).val()) {
                $('#data-sub').html(
                    `<iframe src="https://www.retrogamingmarket.eu/account/inventory/add/${$(this).val()}?side=1" height="900px" width="100%" frameBorder="0"></iframe>`
                );
            }
        });
    </script>
@endif

<script>
    function lang_change() {
        location.href = $('.nk-lang-change').val();
    }
</script>
