<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> {{ config('brcode.app_name_public') }} - 2.0</title>
    <meta name="viewport"
    content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="description" content="Retro Gaming Market - Portal games store">
    <meta name="keywords" content="game, gaming, Retro Gaming, Market, Retro">
    <meta name="author" content="RetroGamingMarket">
    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
    <link rel="icon" type="image/png" href="{{ asset('img/RGM.png') }}">
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }}">
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <style>
            .sicker {
                position: absolute;
                left: 50%;
                top: 10%;
                width: 137px;
                transform: translate(-50%, -50%);
                background: #00000094;
                color: #fff;
                line-height: 20px;
                border-radius: 2px;
                text-align: center;
                font-size: 12px;
                z-index: 991;
            }

            /*
      Rollover Image
     */
            .figure {
                position: relative;
                width: 360px;
                /* can be omitted for a regular non-lazy image */
                max-width: 100%;
            }

            .figure img.image-hover {
                position: absolute;
                top: 0;
                right: 0;
                left: 0;
                bottom: 0;
                object-fit: contain;
                opacity: 0;
                transition: opacity .2s;
            }

            .figure:hover img.image-hover {
                opacity: 1;
            }

            .modal-header button.close {
                position: absolute;
                right: 25px;
                opacity: 1;
                -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
                width: 30px;
                height: 30px;
                border: 1px solid #808080;
                border-radius: 5px;
                font-size: 16px;
                line-height: 25px;
                -webkit-transition: all 0.3s ease-in-out;
                transition: all 0.3s ease-in-out;
                z-index: 99;
                color: #808080;
                padding: 0;
            }

            .Buttons__login-btn {
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                position: relative;
                border: none;
                text-align: center;
                line-height: 34px;
                white-space: nowrap;
                border-radius: 21px;
                font-size: 1rem;
                color: #fff;
                width: 100%;
                height: 42px;
                display: -webkit-flex;
                display: -ms-flexbox;
                display: flex;
                -webkit-align-items: center;
                -ms-flex-align: center;
                align-items: center;
                -webkit-justify-content: center;
                -ms-flex-pack: center;
                justify-content: center;
                padding: 0;
                cursor: pointer;
            }

            .Buttons__login-btn--google {
                background: #dd4b39;
            }

            .prueba {
                position: relative;
                width: 100%;
                min-height: 100vh;
                z-index: 10;
                overflow-y: auto;
                padding-top: 1rem;
                padding-bottom: 1rem;
                overflow-x: hidden;
            }

            .w-full-ultra {
                width: 92%
            }


            .btn-ultra {
                /* height: 40px; */
                padding: 3px 18px;
                font-size: 13px;
                line-height: 1.2em;
                font-weight: 500;
                box-shadow: none !important;
                display: inline-flex;
                align-items: center;
                justify-content: center;
                transition: none;
                text-decoration: none !important;
                border-radius: 6px;
                border-width: 2px;
            }

            .form-control-ultra {
                display: block;
                width: 120%;
                padding: 0.375rem 0.75rem;
                font-size: 1rem;
                font-weight: 400;
                line-height: 1.5;
                color: #212529;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                border-radius: 0.25rem;
                -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            }
        </style>

        <!-- Dropzone css -->
        <link href="/assets/dropzone-master/dist/min/dropzone.min.css" rel="stylesheet" type="text/css" />
        <style>
            .dropzone .dz-preview .dz-error-message {
                top: 175px !important;
            }

            .searchbox .form-control-chat {
                height: 36px;
                border-radius: 6px;
                border: 1px solid #E1E1E1 !important;
                padding: 0 16px 0 36px;
                font-size: 15px;
                box-shadow: none !important;
                color: #141515;
            }

            .searchbox .form-control-chat:focus {
                border-color: #c8c8c8 !important;
            }

            .searchbox .form-control-chat:focus~.input-icon {
                color: #141515;
            }

            .form-control-chat {
                background-clip: padding-box;
                background-image: linear-gradient(transparent, transparent);
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
            }

            .form-group.basic .form-control-chat,
            .form-group.basic .custom-select {
                background: transparent;
                border: none;
                border-bottom: 1px solid #E1E1E1;
                padding: 0 30px 0 0;
                border-radius: 0;
                height: 40px;
                color: #141515;
                font-size: 15px;
            }

            .form-group.basic .form-control-chat:focus,
            .form-group.basic .custom-select:focus {
                border-bottom-color: #1E74FD;
                box-shadow: inset 0 -1px 0 0 #1E74FD;
            }

            .form-group.basic textarea.form-control-chat {
                height: auto;
                padding: 7px 40px 7px 0;
            }

            .form-group.boxed .form-control-chat.form-select,
            .form-group.basic .form-control-chat.form-select {
                background-image: url("data:image/svg+xml,%0A%3Csvg width='13px' height='8px' viewBox='0 0 13 8' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'%3E%3Cg id='Page-1' stroke='none' stroke-width='1' fill='none' fill-rule='evenodd' stroke-linecap='round' stroke-linejoin='round'%3E%3Cpolyline id='Path' stroke='%23A9ABAD' stroke-width='2' points='1.59326172 1.79663086 6.59326172 6.79663086 11.5932617 1.79663086'%3E%3C/polyline%3E%3C/g%3E%3C/svg%3E") !important;
                background-repeat: no-repeat !important;
                background-position: right center !important;
            }

            .form-group.boxed .form-control-chat.form-select {
                background-position: right 12px center !important;
            }

            .chatFooter .form-group .form-control-chat {
                font-size: 13px;
                border-radius: 300px;
                height: 40px;
            }

            .searchbox .form-control-chat {
                height: 36px;
                border-radius: 6px;
                border: 1px solid #E1E1E1 !important;
                padding: 0 16px 0 36px;
                font-size: 15px;
                box-shadow: none !important;
                color: #141515;
            }

            .searchbox .form-control-chat:focus {
                border-color: #c8c8c8 !important;
            }

            .searchbox .form-control-chat:focus~.input-icon {
                color: #141515;
            }

            #search .searchbox .form-control-chat {
                box-shadow: none !important;
                border: 0 !important;
                border-radius: 0;
                height: 56px;
                padding: 0 56px 0 56px;
                background: transparent;
                font-size: 17px;
                color: #141515;
                width: 100%;
            }

            #search .searchbox .form-control-chat:focus {
                border-color: #bbbbbb;
            }

            #search .searchbox .form-control-chat:focus~.input-icon {
                color: #141515;
            }

            .form-control-chat.is-valid,
            .was-validated .form-control-chat:valid {
                border-color: #34C759;
                box-shadow: 0 !important;
                background-image: none !important;
            }

            .form-control-chat.is-valid:focus,
            .was-validated .form-control-chat:valid:focus {
                border-color: #34C759;
                box-shadow: none !important;
            }

            .form-control-chat.is-invalid,
            .was-validated .form-control-chat:invalid {
                border-color: #EC4433;
                background-image: none !important;
            }

            .form-control-chat.is-invalid:focus,
            .was-validated .form-control-chat:invalid:focus {
                border-color: #EC4433;
                box-shadow: none !important;
            }

            .form-control-chat {
                display: block;
                width: 90%;
                padding: 0.375rem 0.75rem;
                font-size: 1rem;
                font-weight: 400;
                line-height: 1.5;
                color: #212529;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                border-radius: 0.25rem;
                -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
                transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            }

            img {
                max-width: 100%;
                height: auto;
            }

            .card-product-image-holder .card-product-image {
                object-fit: cover;
                height: 100%;
                width: 100%;
                -webkit-border-radius: 6px;
                border-radius: 6px;
            }

            .card-product-image-holder {
                width: 100%;
                max-width: 400px;
                height: 250px;
                border-radius: 5px;
                overflow: hidden;
                position: relative;
            }

            .element {
                display: inline-flex;
                align-items: center;
            }

            svg.bi bi-camera {
                margin: 10px;
                cursor: pointer;
                font-size: 30px;
            }

            svg:hover {
                opacity: 0.6;
            }
        </style>
        <link rel="stylesheet" href="{{ asset('css/mobile/core.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/affan.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/mobilekit.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/splide.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/ultra.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/animate.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
            integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
            crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="{{ asset('css/mobile/prueba.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/select2/select2.min.css') }}">
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <meta name="google-site-verification" content="uOvlTBBjltn6jz3EfQV11zsAEEHusnIZ9h-Buylgk6s" />
    @else
        <link rel="stylesheet" href="{{ asset('css/menu.css') }}">

        <!-- NES CSS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/nes/nes_prueba.css') }}">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="{{ asset('assets/css/desk.min.css') }}">

        <!-- FontAwesome -->
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}">

        <!-- IonIcons -->
        <link rel="stylesheet" href="{{ asset('assets/vendor/ionicons/css/ionicons.min.css') }}">

        <!-- Seiyria Bootstrap Slider -->
        <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-slider/dist/css/bootstrap-slider.min.css') }}">

        <!-- GoodGames -->
        <link rel="stylesheet" href="{{ asset('assets/css/goodgames.css') }}">

        <!-- Custom Styles -->
        <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">

        <link rel="stylesheet" href="{{ asset('assets/imageSelect/css/chosen.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/imageSelect/css/ImageSelect.css') }}">

        <link rel="stylesheet" href="{{ asset('assets/select2/select2.min.css') }}">
        @yield('content-css-include')
        <!-- END: Styles -->
        <link rel="stylesheet" href="{{ asset('assets/cookie/cookiealert.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/flag-icon/css/flag-icon.min.css') }}">
        <!-- Prueba -->
        <!-- Animate CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
        <!-- UI CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.min.css') }}">
        <!-- Chosen CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/chosen.css') }}">
        <!-- Meanmenu CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/meanmenu.min.css') }}">
        <!-- Normalize CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}">
        <!-- Nivo Slider CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/nivo-slider.css') }}">
        <!-- Owl Carousel CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
        <!-- EasyZoom CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/easyzoom.css') }}">
        <!-- Slick CSS-->
        <link rel="stylesheet" href="{{ asset('assets/css/slick.css') }}">
        <!-- Default CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/default.css') }}">
        <!-- Style CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <meta name="google-site-verification" content="uOvlTBBjltn6jz3EfQV11zsAEEHusnIZ9h-Buylgk6s" />
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
        <!-- jQuery -->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('headroom/headroom.min.js') }}"></script>
        <script src="{{ asset('js/menu.js') }}"></script>
    @endif
</head>
 
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
    <div class="header-area" id="headerArea">
        <div class="container h-100 d-flex align-items-center justify-content-between">
            <!-- Back Button-->
            <div class="back-button"><a href="/"><i class="lni lni-arrow-left"></i></a></div>
            <!-- Page Title-->
            <div class="page-heading">
                <h6 class="mb-0 font-extrabold">INICIAR SESIÓN</h6>
            </div>
            <!-- Navbar Toggler-->

            @if (Auth::user())
                <div class="suha-navbar-toggler mt-1 d-flex flex-wrap">
                    <span></span><span> - </span><span></span>
                </div>
            @else
                <div class="suha-navbar-toggler mt-1 d-flex flex-wrap">
                    <span></span><span> - </span><span></span>
                </div>
            @endif
        </div>
    </div>
    <body class="bg-white">

        <div class="login-form">
            <div class="page-content-wrapper">
                <div class="container">
                    <div class="login-wrapper d-flex align-items-center justify-content-center text-center">
                        <div class="col-12 col-sm-9 col-md-7 col-lg-6 col-xl-5"><img class="big-logo" width="78px"
                                height="111px" src="{{ asset('img/RGM.png') }}" alt=""><br><br><br><br>
                            <div class="container">
                                <div class="row justify-content-center">

                                    <!-- Register Form-->
                                    <form action="{{ url('/TobeContinued/login') }}" class="form-horizontal" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group boxed">
                                            <div class="input-wrapper">
                                                <input type="text" id="user_name" name="user_name"
                                                    class="form-control" placeholder="Usuario">
                                                <i class="clear-input">
                                                    <ion-icon name="close-circle"></ion-icon>
                                                </i>
                                            </div>
                                        </div>

                                        <div class="form-group boxed">
                                            <div class="input-wrapper">
                                                <input type="password" value="" id="password" name="password"
                                                    class="form-control" placeholder="Contraseña">
                                                <i class="clear-input">
                                                    <ion-icon name="close-circle"></ion-icon>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="mt-4 text-start">
                                            <div class="form-check">
                                                <input type="checkbox" name="remember_me" class="form-check-input">
                                                <label class="form-check-label font-bold"
                                                    for="customCheckb1">{{ __('Recordarme') }}</label>
                                            </div>

                                        </div>

                                        <div class="login-meta-data text-left">
                                        
                                            <p class="mb-0 mt-2">@lang('messages.forget_account')<a
                                                class="ml-1" href="{{ route('register-start') }}"> <font color="#fe0000">@lang('messages.registers')</font></a>
                                        </p>
                                    </div>
                                        <br>
                                        <button id="submitLogin" class="btn btn-danger font-extrabold btn-lg w-100"
                                            type="submit">Iniciar
                                            Sesion</button>
                                            <br><br> <br><br> <br><br> <br>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    
    </body>
    @else

    <body>
        <!-- Create By Joker Banny -->
        <div class="min-h-screen bg-center bg-no-repeat bg-cover" style="background-image: url('/img/LOGIN.png')">
            <div class="flex justify-end">
                <div class="flex items-center justify-center w-1/2 min-h-screen bg-white">
                    <div>
    
                        <form action="{{ url('/TobeContinued/login') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div>
                                <span class="text-sm text-gray-900">Bienvenido</span>
                                <h1 class="text-2xl font-bold">Iniciar Sesion</h1>
                            </div>
                            <div class="mt-5">
                                <label class="block mb-2 text-md" for="password">Usuario</label>
                                <input class="w-full px-4 py-2 text-sm border-2 rounded-md outline-none" type="text"
                                    name="user_name" placeholder="RGM">
                            </div>
                            <div class="my-3">
                                <label class="block mb-2 text-md" for="email">Contraseña</label>
                                <input class="w-full px-4 py-2 text-sm border-2 rounded-md outline-none" type="password"
                                    name="password" placeholder="******">
                            </div>
                            <div class="flex justify-between">
                                <div>
                                    <label>
                                        <input type="radio" class="nes-radio" name="remember_me" />
                                        <span>{{ __('Recordarme') }}</span>
                                    </label>
    
                                </div>
                            </div>
                            <div class="">
                                <button type="submit"
                                    class="w-full py-2 mt-4 mb-3 text-white transition duration-100 bg-green-500 rounded-md nes-btn is-error hover:bg-green-400">Iniciar
                                    Sesion</button>
    
                            </div>
                        </form>
                        <p class="mt-8"> Modo Developer Temporal <span class="text-sm text-blue-600 cursor-pointer">
                                (Version de prueba)</span></p>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </body>
    @endif
 

