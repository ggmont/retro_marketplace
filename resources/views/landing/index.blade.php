<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> {{ config('brcode.app_name_public') }} - 2.0</title>

    <meta name="description" content="Retro Gaming Market - Portal games store">
    <meta name="keywords" content="game, gaming, Retro Gaming, Market, Retro">
    <meta name="author" content="RetroGamingMarket">

    <link rel="icon" type="image/png" href="{{ asset('img/RGM.png') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nes/nes_prueba.css') }}"> 
    
    <style>
        html {
            width: 100%;
            height: 100%;
        }

        body {
            width: 100%;
            background-image: url("img/RGMTOBECONTINUED.png");
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }

        audio {
            display: none;
        }

    </style>
</head>
@php
$boton = true;
$sonido = 'waiting.mp3';
@endphp

<body>
    <audio controls loop autoplay height="" width="">
        <source src="{{ asset('sonidos/' . $sonido) }}" type="audio/mp3" />
    </audio>
</body>
