<div class="top-products-area clearfix py-3">
    <div class=" container section-heading d-flex align-items-center justify-content-between">
        <h5>Categorías</h5>
        <button class="btn btn-danger btn-sm"><span
            class="font-extrabold">Ver Todo</span></button>
    </div>
    <div wire:ignore  class="flash-sale-slide owl-carousel">

        <div class="owl-item active" style="width: 154px; margin-right: 18px;  padding: 0px 10px;">
            <div class="card collection-card"><a  href="https://www.retrogamingmarket.eu/?search_category=Juegos"><img src="{{ asset('img/juegos.png') }}"
                        alt=""></a>
                <div class="collection-title"><span>Juegos</span><span>{{$inventories_game}} Disponibles</span></div>
            </div>
        </div>

        <div class="owl-item active" style="width: 154px; margin-right: 18px;  padding: 0px 10px;">
            <div class="card collection-card"><a href="https://www.retrogamingmarket.eu/?search_category=Consolas"><img width="1280" height="720"
                        src="{{ asset('img/consolas.png') }}" alt=""></a>
                <div class="collection-title"><span>Consolas</span><span>{{$inventories_consoles}} Disponibles</span>
                </div>
            </div>
        </div>
        <div class="owl-item active" style="width: 154px; margin-right: 18px;  padding: 0px 10px;">
            <div class="card collection-card"><a   href="https://www.retrogamingmarket.eu/?search_category=Periféricos"><img width="1280" height="720"
                        src="{{ asset('img/perifericos.png') }}" alt=""></a>
                <div class="collection-title"><span>Periféricos</span><span>{{$inventories_perifericos}} Disponibles</span>
                </div>
            </div>
        </div>
        <div class="owl-item active" style="width: 154px; margin-right: 18px;  padding: 0px 10px;">
            <div class="card collection-card"><a  href="https://www.retrogamingmarket.eu/?search_category=Accesorios"><img width="1280" height="720"
                        src="{{ asset('img/accesorios.png') }}" alt=""></a>
                <div class="collection-title"><span>Accesorios</span><span>{{$inventories_accesorios}} Disponibles</span>
                </div>
            </div>
        </div>
        <div class="owl-item active" style="width: 154px; margin-right: 18px;  padding: 0px 10px;">
            <div class="card collection-card"><a href="https://www.retrogamingmarket.eu/?search_category=Merchandising"><img width="1280" height="720"
                        src="{{ asset('img/productos/merchandising.png') }}" alt=""></a>
                <div class="collection-title"><span>Merch</span><span>{{$inventories_merch}} Disponibles</span>
                </div>
            </div>
        </div>



    </div>
    <br>
    <br>

    <button type="button" class="filterToggle d-lg-none btn btn-lg btn-danger btn-rounded btn-fixed" data-toggle="modal"
        data-target="#exampleModal">
        <span class="fa-solid fa-filter"></span>
    </button>
    <div class="container">
        <div class="section-heading d-flex align-items-center justify-content-between">
            <h5 class="ml-1">Últimos productos</h5>
        </div>
      
        <div wire:loading.delay.class="opacity-50" class="row g-3">
            <!-- Single Top Product Card-->
            @isset($seller)
                @foreach ($seller as $i)
                    <div @if ($loop->last) id="last_record" @endif class="col-6 col-md-4 col-lg-3">
                        <div class="card top-product-card">
                            <div class="card-body"><a href="single-product.html">
                                    <div class="figure">
                                        <a href="{{ route('product-show', $i->product->id) }}">
                                            @if ($i->images->first())
                                                <img class="Sirv image-main"
                                                    src="{{ url('/uploads/inventory-images/' . $i->images->first()->image_path) }}"
                                                    alt="">
                                                @if ($i->images->get(1))
                                                    <img class="Sirv image-hover"
                                                        src="{{ url('/uploads/inventory-images/' . $i->images->get(1)->image_path) }}"
                                                        alt="">
                                                @endif
                                            @elseif ($i->product->images->first())
                                                <img class="Sirv image-main"
                                                    src="{{ url('/images/' . $i->product->images->first()->image_path) }}"
                                                    alt="">
                                                @if ($i->product->images->get(1))
                                                    <img class="Sirv image-hover"
                                                        src="{{ url('/images/' . $i->product->images->get(1)->image_path) }}"
                                                        alt="">
                                                @endif
                                            @else
                                                <img loading="lazy" class="first-img"
                                                    src="{{ asset('assets/images/art-not-found.jpg') }}" alt="">
                                            @endif
                                        </a>
                                    </div>
                                    <span class="text-sm font-bold sicker">
                                        <a href="{{ route('user-info', $i->user->user_name) }}" target="_blank">
                                            <font color="white">
                                                {{ str_limit($i->user->user_name, 10) }} -
                                                @include('partials.pais_user_home')
                                            </font>
                                        </a>
                                    </span>
                                    <span class="product-title">{{ str_limit($i->product->name, 45) }}
                                    </span>
                                    <span class="progress-title">
                                        {{ $i->product->platform . ' - ' . $i->product->region }}</span>
                                    <p class="sale-price">{{ number_format($i->price, 2) . ' €' }}</p>
                                    <hr>
                                    @if ($i->product->categoryProd->parent_id == 1)
                                    <div class="flex ulti">
                                        <img height="10px" width="15px" src="/{{ $i->box }}"
                                            data-toggle="popover"
                                            data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        &nbsp;
                                        <img width="15px" src="/{{ $i->cover }}" data-toggle="popover"
                                            data-content="Caratula - {{ __(App\AppOrgUserInventory::getCondicionName($i->cover_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        &nbsp;
                                        <img width="15px" src="/{{ $i->manual }}" data-toggle="popover"
                                        data-content="Manual - {{ __(App\AppOrgUserInventory::getCondicionName($i->manual_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                        &nbsp;
                                        <img width="15px" src="/{{ $i->game }}" data-toggle="popover"
                                            data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        &nbsp;
                                        <img width="15px" src="/{{ $i->extra }}" data-toggle="popover"
                                            data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        &nbsp; 
                                        @if ($i->comments == '')
                                            <i class="fa-solid fa-circle-xmark" data-toggle="popover"
                                                data-content="@lang('messages.not_working_profile')"
                                                data-placement="top" data-trigger="hover"></i>
                                        @else
                                            <i class="fa-solid fa-comment" data-toggle="popover"
                                                data-content="{{ $i->comments }}" data-placement="top"
                                                data-trigger="hover"></i>
                                        @endif
                                    </div>
                                    @elseif ($i->product->categoryProd->parent_id == 2)
                                    <div class="flex ultri">
                                        <img height="10px" width="15px" src="/{{ $i->box }}"
                                            data-toggle="popover"
                                            data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        &nbsp;
                                        <img width="15px" src="/{{ $i->cover }}" data-toggle="popover"
                                            data-content="Caratula - {{ __(App\AppOrgUserInventory::getCondicionName($i->cover_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        &nbsp;
                                        <img width="15px" src="/{{ $i->manual }}" data-toggle="popover"
                                        data-content="Manual - {{ __(App\AppOrgUserInventory::getCondicionName($i->manual_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                        &nbsp;
                                        <img width="15px" src="/{{ $i->game }}" data-toggle="popover"
                                            data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        &nbsp;
                                        <img width="15px" src="/{{ $i->extra }}" data-toggle="popover"
                                            data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        &nbsp; 
                                        <img width="15px" src="/{{ $i->extra }}" data-toggle="popover"
                                        data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                        &nbsp;
                                        <img width="15px" src="/{{ $i->inside }}" data-toggle="popover"
                                        data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->inside_condition)) }}"
                                        data-placement="top" data-trigger="hover">
                                        &nbsp;
                                        @if ($i->comments == '')
                                            <i class="fa-solid fa-circle-xmark" data-toggle="popover"
                                                data-content="@lang('messages.not_working_profile')"
                                                data-placement="top" data-trigger="hover"></i>
                                        @else
                                            <i class="fa-solid fa-comment" data-toggle="popover"
                                                data-content="{{ $i->comments }}" data-placement="top"
                                                data-trigger="hover"></i>
                                        @endif
                                    </div>
                                    @else
                                    <div class="flex ulti">
                                        <img height="10px" width="15px" src="/{{ $i->box }}"
                                            data-toggle="popover"
                                            data-content="Caja - {{ __(App\AppOrgUserInventory::getCondicionName($i->box_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        &nbsp;
                                        <img width="15px" src="/{{ $i->game }}" data-toggle="popover"
                                            data-content="Estado - {{ __(App\AppOrgUserInventory::getCondicionName($i->game_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        &nbsp;
                                        <img width="15px" src="/{{ $i->extra }}" data-toggle="popover"
                                            data-content="Extra - {{ __(App\AppOrgUserInventory::getCondicionName($i->extra_condition)) }}"
                                            data-placement="top" data-trigger="hover">
                                        &nbsp; 
                                        @if ($i->comments == '')
                                            <i class="fa-solid fa-circle-xmark" data-toggle="popover"
                                                data-content="@lang('messages.not_working_profile')"
                                                data-placement="top" data-trigger="hover"></i>
                                        @else
                                            <i class="fa-solid fa-comment" data-toggle="popover"
                                                data-content="{{ $i->comments }}" data-placement="top"
                                                data-trigger="hover"></i>
                                        @endif
                                    </div>
                                    @endif
                                </a></div>
                        </div>
                    </div>
                @endforeach
            @endisset
        </div>
        @if ($loadAmount >= $totalRecords)
            <p class="text-gray-800 font-bold text-2xl text-center my-10">No hay mas productos disponibles!</p>
        @endif

    </div>
    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-white">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">
                        <center>Filtar por..</center>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="wide-block pb-1 pt-2">

                        <form>

                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label" for="name5">Nombre</label>
                                    <input wire:model="search" type="text" class="form-control" id="name5"
                                        placeholder="Buscar por nombre" autocomplete="off">
                                    <i class="clear-input">
                                        <ion-icon name="close-circle" role="img" class="md hydrated"
                                            aria-label="close circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label" for="city5">@lang('messages.country')</label>

                                    <select wire:model="search_country" class="form-control form-select" id="city5">
                                        <option value="" selected> @lang('messages.alls')
                                        </option>
                                        @foreach ($country as $c)
                                            <option value="{{ $c->name }}">
                                                {{ __($c->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label" for="city5">Plataforma</label>

                                    <select wire:model="search_platform" class="form-control form-select">
                                        <option value="" selected> @lang('messages.alls')
                                        </option>
                                        @foreach ($platform as $p)
                                            <option value="{{ $p->value }}">{{ $p->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label" for="city5">Categoria</label>
                                    <select wire:model="search_category" class="form-control form-select" id="city5">
                                        <option value="" selected>@lang('messages.alls')
                                        </option>
                                        <optgroup label="@lang('messages.category')">
                                            <option value="Juegos">@lang('messages.games')</option>
                                            <option value="Consolas">@lang('messages.consoles')</option>
                                            <option value="Periféricos">@lang('messages.peripherals')</option>
                                            <option value="Accesorios">@lang('messages.accesories')</option>
                                            <option value="Merchandising">Merchandising</option>
                                        </optgroup>
                                        <optgroup label="@lang('messages.sub_category')">
                                            <option value="Mandos">@lang('messages.controls')</option>
                                            <option value="Micrófonos">@lang('messages.microphones')</option>
                                            <option value="Teclados">@lang('messages.keyboard')</option>
                                            <option value="Fundas">@lang('messages.funded')</option>
                                            <option value="Cables">@lang('messages.cable')</option>
                                            <option value="Cargadores">@lang('messages.chargers')</option>
                                            <option value="Merchandising -> Logos 3d">@lang('messages.3d_logo')</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label" for="city5">Region</label>
                                    <select wire:model="search_region" class="form-control form-select">
                                        <option value="" selected>@lang('messages.alls')
                                        </option>
                                        @foreach ($region as $r)
                                            <option value="{{ $r->value_id }}">{{ $r->value_id }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label">Idioma</label>
                                    <select wire:model="search_language" class="form-control form-select">
                                        <option value="" selected>@lang('messages.alls')
                                        </option>

                                        @foreach ($language as $l)
                                            <option value="{{ $l->value }}">{{ $l->value }}</option>
                                        @endforeach

                                    </select>

                                </div>
                            </div>
                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="form-label">Media</label>
                                    <select wire:model="search_media" class="form-control form-select">
                                        <option value="" selected>@lang('messages.alls')
                                        </option>
                                        @foreach ($media as $m)
                                            <option value="{{ $m->value }}">{{ $m->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
    <script>
        const lastRecord = document.getElementById('last_record');
        const options = {
            root: null,
            threshold: 1,
            rootMargin: '0px'
        }
        const observer = new IntersectionObserver((entries, observer) => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    @this.loadMore()
                }
            });
        });
        observer.observe(lastRecord);
    </script>



</div>
