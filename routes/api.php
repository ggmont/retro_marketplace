<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserInventoryImageController;
use Illuminate\Support\Facades\Route;

Route::post('login', [LoginController::class, 'loginApp']);
Route::post('logout', [LoginController::class, 'logoutApp'])->middleware('auth:sanctum');

Route::post('sell/uploadInventoryImage', [UserInventoryImageController::class, 'store'])->middleware('auth:sanctum');
