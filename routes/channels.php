<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.SysUser.{user_name}', function ($user, $user_name) {
    return (string) $user->user_name === (string) $user_name;
});

Broadcast::channel('users.{user_name}', function ($user, $user_name) {
    return (string) $user->username === (string) $user_name;
});

Broadcast::channel('messenger', function ($user) {
    return [
    	'id' => $user->user_name
    ];
});

Broadcast::channel('notification', function () {
    return true;
});

