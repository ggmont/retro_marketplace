<?php

namespace App\Helpers;

use DB;
use App\AppOrgUserEvent;

class PtyCommons
{

  public static function setUserEvent($userId, $messageCode, $messageValues = []) {
    $event = new AppOrgUserEvent;
    $event->user_id = $userId;
    $event->message_code = $messageCode;
    $event->message_values = json_encode($messageValues);
    $event->save();
  }

}
