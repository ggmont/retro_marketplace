<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppOrgFooterLink extends Model {
  use SoftDeletes;
  protected $table = 'appOrgFooterLinks';
  
}
