<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppOrgUserHistorialBidInventory extends Model
{
    use SoftDeletes;
    protected $table = 'appOrgUserBidHistorialInventories';

    protected $fillable = ['user_id', 'user_bid', 'inventory_id', 'status'];

    private $title_game = [
        'NEW' => "Nuevo",
        'USED-NEW' => "Usado como nuevo",
        'USED' => "Usado",
        'USED-VERY' => "Muy usado",
        'NOT-WORK' => "No funciona",
        'NOT-PRES' => "No aplica"
    ];

    public function inventory()
    {
        return $this->hasOne('App\AppOrgUserInventory', 'id', 'inventory_id');
    }

    public function user()
    {
        return $this->hasOne('App\SysUser', 'id', 'user_id');
    }

    public function userbid()
    {
        return $this->hasOne('App\SysUser', 'id', 'user_bid');
    }
}
