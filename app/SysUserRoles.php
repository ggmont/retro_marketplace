<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SysUserRoles extends Model
{
    use SoftDeletes;
    
	protected $dates = ['deleted_at'];
	
	protected $table = 'sysUserRoles';
	
	public function scopeOfUserAndPrivilege($query, $user_id, $privilege = '') {
		$query
			->join('sysRolePrivileges','sysUserRoles.role_id','=','sysRolePrivileges.role_id')
			->join('sysPrivileges','sysPrivileges.id','=','sysRolePrivileges.privilege_id')
			->where('sysUserRoles.user_id',$user_id)
			->where('sysPrivileges.is_enabled','Y');
		
		if(strlen($privilege) > 0) {
			$query->where('sysPrivileges.code',$privilege);
		}
		
		return $query;
	}
	
	public static function markOtherAsDeleted($user_id, $ids) {
		
		DB::table('sysUserRoles')
            ->where('user_id', $user_id)
            ->whereNotIn('id', $ids)
            ->delete();
		
	}	

	public function rol(){
		return $this->hasOne('App\SysRoles', 'id', 'role_id');
	}
}
