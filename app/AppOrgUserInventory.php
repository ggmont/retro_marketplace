<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppOrgUserInventory extends Model {
  use SoftDeletes;
  protected $table = 'appOrgUserInventories';

  protected $fillable = ['user_id', 'product_id', 'box_condition', 'manual_condition', 'cover_condition','in_collection','auction_type','max_bid','user_bid',
  'game_condition', 'extra_condition','countdown_hours','countdown_cancel' ,'location','status','title','platform','region','size','kg','offer_type', 'category', 'quantity','quantity_sold','price','favorite','comments','created_at','error_id'];

  private $title_game = [
    'NEW' => "Nuevo",
    'USED-NEW' => "Usado como nuevo",
    'USED' => "Usado",
    'USED-VERY' => "Muy usado" ,
    'NOT-WORK' => "No funciona",
    'NOT-PRES' => "No aplica"
  ];

  private $country = [
    'Spain' => "España",
    'Andorra' => "Andorra",
    'Åland Islands' => "Åland Islands",
    'Albania' => "Albania",
    'Austria' => "Austria",
    'Belarus' => "Belarus",
    'Belgium' => "Belgica",
    'Bosnia and Herzegovina' => "Bosnia and Herzegovina",
    'Bulgaria' => "Bulgaria",
    'Croatia' => "Croacia",
    'Czech Republic' => "Czech Republic",
    'Denmark' => "Dinamarca",
    'Estonia' => "Estonia",
    'Faroe Islands' => "Faroe Islands",
    'Finland' => "Finlandia",
    'France' => "Francia",
    'Germany' => "Alemania",
    'Gibraltar' => "Gibraltar",
    'Guernsey' => "Guernsey",
    'Holy See' => "Holy See",
    'Hungary' => "Hungria",
    'Iceland' => "Islandia",
    'Ireland' => "Irlanda",
    'Isle of Man' => "Isle of Man",
    'Italy' => "Italia",
    'Jersey' => "Jersey",
    'Latvia' => "Latvia",
    'Liechtenstein' => "Liechtenstein",
    'Lithuania' => "Lithuania",
    'Luxembourg' => "Luxembourg",
    'Macedonia (the former Yugoslav Republic of)' => "Macedonia (the former Yugoslav Republic of)",
    'Malta' => "Malta",
    'Moldova (Republic of)' => "Moldova (Republic of)",
    'Monaco' => "Monaco",
    'Montenegro' => "Montenegro",
    'Netherlands' => "Paises Bajos",
    'Norway' => "Norway",
    'Poland' => "Poland",
    'Portugal' => "Portugal",
    'Romania' => "Romania",
    'Russian Federation' => "Russian Federation",
    'San Marino' => "San Marino",
    'Serbia' => "Serbia",
    'Slovakia' => "Slovakia",
    'Slovenia' => "Slovenia",
    'Svalbard and Jan Mayen' => "Svalbard and Jan Mayen",
    'Sweden' => "Sweden",
    'Switzerland' => "Switzerland",
    'Ukraine' => "Ukraine",
    'United Kingdom of Great Britain and Northern Ireland' => "Reino Unido"
  ];

  public function images() {
    return $this->hasMany('App\AppOrgUserInventoryImage','inventory_id','id');
  }

  public function imagesean() {
    return $this->hasMany('App\AppOrgUserInventoryEan','inventory_id','id');
  }

  public function product() {
    return $this->hasOne('App\AppOrgProduct', 'id', 'product_id');
  }

  public function user() {
    return $this->hasOne('App\SysUser', 'id', 'user_id');
  }


  public function userbid() {
    return $this->hasOne('App\SysUser', 'id', 'user_bid');
  }

 

  public function getCountryAttribute(){
    $value = '';
    if($this->user->country->name == 'Spain'){
      $value = 'img/spain.png';
    }
    if($this->user->country->name == 'Andorra'){
      $value = 'img/andorra.png';
    }
    return $value;
  }

  public function getBoxAttribute(){
    $value = '';
    if($this->box_condition == 'NEW'){
      $value = 'img/new.png';
    }
    if($this->box_condition == 'USED-NEW'){
      $value = 'img/used-new.png';
    }
    if($this->box_condition == 'USED'){
      $value = 'img/used.png';
    }
    if($this->box_condition == 'USED-VERY'){
      $value = 'img/used-very.png';
    }
    if($this->box_condition == 'NOT-WORK'){
      $value = 'img/no-funciona.png';
    }
    if($this->box_condition == 'NOT-PRES'){
      $value = 'img/not.png';
    }
    return $value;
  }
  public function getCoverAttribute(){
    $value = '';
    if($this->cover_condition == 'NEW'){
      $value = 'img/new.png';
    }
    if($this->cover_condition == 'USED-NEW'){
      $value = 'img/used-new.png';
    }
    if($this->cover_condition == 'USED'){
      $value = 'img/used.png';
    }
    if($this->cover_condition == 'USED-VERY'){
      $value = 'img/used-very.png';
    }
    if($this->cover_condition == 'NOT-WORK'){
      $value = 'img/no-funciona.png';
    }
    if($this->cover_condition == 'NOT-PRES'){
      $value = 'img/not.png';
    }
    return $value;
  }

  public function getManualAttribute(){
    $value = '';
    if($this->manual_condition == 'NEW'){
      $value = 'img/new.png';
    }
    if($this->manual_condition == 'USED-NEW'){
      $value = 'img/used-new.png';
    }
    if($this->manual_condition == 'USED'){
      $value = 'img/used.png';
    }
    if($this->manual_condition == 'USED-VERY'){
      $value = 'img/used-very.png';
    }
    if($this->manual_condition == 'NOT-WORK'){
      $value = 'img/no-funciona.png';
    }
    if($this->manual_condition == 'NOT-PRES'){
      $value = 'img/not.png';
    }
    return $value;
  }

  public function getGameAttribute(){
    $value = '';
    if($this->game_condition == 'NEW'){
      $value = 'img/new.png';
    }
    if($this->game_condition == 'USED-NEW'){
      $value = 'img/used-new.png';
    }
    if($this->game_condition == 'USED'){
      $value = 'img/used.png';
    }
    if($this->game_condition == 'USED-VERY'){
      $value = 'img/used-very.png';
    }
    if($this->game_condition == 'NOT-WORK'){
      $value = 'img/no-funciona.png';
    }
    if($this->game_condition == 'NOT-PRES'){
      $value = 'img/not.png';
    }
    return $value;
  }

  public function getExtraAttribute(){
    $value = '';
    if($this->extra_condition == 'NEW'){
      $value = 'img/new.png';
    }
    if($this->extra_condition == 'USED-NEW'){
      $value = 'img/used-new.png';
    }
    if($this->extra_condition == 'USED'){
      $value = 'img/used.png';
    }
    if($this->extra_condition == 'USED-VERY'){
      $value = 'img/used-very.png';
    }
    if($this->extra_condition == 'NOT-WORK'){
      $value = 'img/no-funciona.png';
    }
    if($this->extra_condition == 'NOT-PRES'){
      $value = 'img/not.png';
    }
    return $value;
    
  }

  public function getInsideAttribute(){
    $value = '';
    if($this->inside_condition == 'NEW'){
      $value = 'img/new.png';
    }
    if($this->inside_condition == 'USED-NEW'){
      $value = 'img/used-new.png';
    }
    if($this->inside_condition == 'USED'){
      $value = 'img/used.png';
    }
    if($this->inside_condition == 'USED-VERY'){
      $value = 'img/used-very.png';
    }
    if($this->inside_condition == 'NOT-WORK'){
      $value = 'img/no-funciona.png';
    }
    if($this->inside_condition == 'NOT-PRES'){
      $value = 'img/not.png';
    }
    return $value;
    
  }

  

  // public function getGameConditionAttribute(){
  //   return $this->title_game[$this->game_condition];
  // }
  // public function getBoxConditionAttribute(){
  //   return $this->title_game[$this->box_condition];
  // }
  // public function getManualConditionAttribute(){
  //   return $this->title_game[$this->manual_condition];
  // }
  // public function getExtraConditionAttribute(){
  //   return $this->title_game[$this->extra_condition];
  // }
  // public function getCoverConditionAttribute(){
  //   return $this->title_game[$this->cover_condition];
  // }
  // public function getInsideConditionAttribute(){
  //   return $this->title_game[$this->inside_condition];
  // }

  public function scopeOfSeller($query) {
    //DB::raw("IF(`buyer_user_id`='$userId', CONCAT('C', `status`), CONCAT('V', `status`)) as status_type")
    //DB::raw("false as favorito"),
    $query->select(['u.user_name', 'user_id', 'appOrgUserInventories.id',
      'price', 'tax',  'quantity', 'quantity_sold',
      'extra_condition', 'game_condition','box_condition', 'cover_condition', 'manual_condition', 'inside_condition',
      'comments'
    ])
    ->join('sysUsers AS u','u.id','=','appOrgUserInventories.user_id')
    ;

    return $query;
  }

  public static function getCondicionName($id)
  {
    $title_game = [
      'NEW' => "Nuevo",
      'USED-NEW' => "Usado como nuevo",
      'USED' => "Usado",
      'USED-VERY' => "Muy usado" ,
      'NOT-WORK' => "No funciona",
      'NOT-PRES' => "No tiene"
    ];
    return $title_game[$id];
  }

    public static function getCountryName($id)
  {
    
    $country = [
    'Spain' => "España",
    'Andorra' => "Andorra",
    'Åland Islands' => "Åland Islands",
    'Albania' => "Albania",
    'Austria' => "Austria",
    'Belarus' => "Belarus",
    'Belgium' => "Belgica",
    'Bosnia and Herzegovina' => "Bosnia and Herzegovina",
    'Bulgaria' => "Bulgaria",
    'Croatia' => "Croacia",
    'Czech Republic' => "Czech Republic",
    'Denmark' => "Dinamarca",
    'Estonia' => "Estonia",
    'Faroe Islands' => "Faroe Islands",
    'Finland' => "Finlandia",
    'France' => "Francia",
    'Germany' => "Alemania",
    'Gibraltar' => "Gibraltar",
    'Guernsey' => "Guernsey",
    'Holy See' => "Holy See",
    'Hungary' => "Hungria",
    'Iceland' => "Islandia",
    'Ireland' => "Irlanda",
    'Isle of Man' => "Isle of Man",
    'Italy' => "Italia",
    'Jersey' => "Jersey",
    'Latvia' => "Latvia",
    'Liechtenstein' => "Liechtenstein",
    'Lithuania' => "Lithuania",
    'Luxembourg' => "Luxembourg",
    'Macedonia (the former Yugoslav Republic of)' => "Macedonia (the former Yugoslav Republic of)",
    'Malta' => "Malta",
    'Moldova (Republic of)' => "Moldova (Republic of)",
    'Monaco' => "Monaco",
    'Montenegro' => "Montenegro",
    'Netherlands' => "Paises Bajos",
    'Norway' => "Norway",
    'Poland' => "Poland",
    'Portugal' => "Portugal",
    'Romania' => "Romania",
    'Russian Federation' => "Russian Federation",
    'San Marino' => "San Marino",
    'Serbia' => "Serbia",
    'Slovakia' => "Slovakia",
    'Slovenia' => "Slovenia",
    'Svalbard and Jan Mayen' => "Svalbard and Jan Mayen",
    'Sweden' => "Sweden",
    'Switzerland' => "Switzerland",
    'Ukraine' => "Ukraine",
    'United Kingdom of Great Britain and Northern Ireland' => "Reino Unido"
    ];

    return $country[$id];
  }

}
