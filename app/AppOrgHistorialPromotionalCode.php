<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppOrgHistorialPromotionalCode extends Model
{
    protected $table = 'historial_promotional_code';
	
    protected $fillable = [
        'user_id','code_id','code', 'is_enabled','total','type','prueba_id','status','order_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\SysUser', 'user_id');
    }

    public function order()
    {
        return $this->belongsTo('App\AppOrgOrder', 'order_id');
    } 

    public function promotion()
    {
        return $this->belongsTo('App\AppOrgPromotionalCode', 'code_id');
    }

    public function cart()
    {
        return $this->belongsTo(AppOrgCartUser::class);
    }
}
