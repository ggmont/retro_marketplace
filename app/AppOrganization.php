<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppOrganization extends Model
{
    protected $table = 'appOrganizations';
}
