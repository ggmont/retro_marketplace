<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SysPrivileges extends Model
{
    use SoftDeletes;
    
	protected $dates = ['deleted_at'];
	
	protected $table = 'sysPrivileges';

	protected $fillable = [
        'name', 'description', 'code',
	];
	
}
