<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppOrgCarousel extends Model {
  use SoftDeletes;
  protected $table = 'appOrgCarousels';

  protected $fillable = ['name','location','is_enabled'];

  public function images() {
    return $this->hasMany('App\AppOrgCarouselItem','carousel_id','id','lang');
  }
}
