<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysUserSocial extends Model
{
    protected $table = 'sys_user_social';
	
    protected $fillable = [
        'user_id','youtube', 'tiktok', 'instagram','twitch','facebook'
    ];

    public function social()
    {
        return $this->hasMany('App\SysUser', 'user_id');
    }
}
