<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysUserAccountActivation extends Model
{
    protected $table = 'sysUserAccountActivations';
}
