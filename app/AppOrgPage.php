<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppOrgPage extends Model {

  use SoftDeletes;

  protected $table = 'appOrgPages';

  protected $fillable = ['title','url','content','show_title','is_enabled','lang'];

}
