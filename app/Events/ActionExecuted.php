<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ActionExecuted extends Event
{
    use SerializesModels;

	public $action;

	public $table_name;

	public $object_id;

	public $ip;

	public $additional_info;

    /**
     * Create a new event instance.
     *
     * @return void
     */
	public function __construct($action, $table_name, $object_id, $ip, $additional_info = null)
	{
		$this->action       = $action;
		$this->table_name   = $table_name;
		$this->object_id 	  = $object_id;
		$this->ip           = $ip;
		$this->add_info     = $additional_info;
	}

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
