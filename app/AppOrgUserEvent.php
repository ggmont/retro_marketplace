<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppOrgUserEvent extends Model {

  protected $table = 'appOrgUserEvents';

  public function getMessageValuesAttribute($value) {
    return (array) json_decode($value);
  }

  public function user(){
    return $this->hasOne('App\SysUser', 'id', 'user_id');
  }

}
