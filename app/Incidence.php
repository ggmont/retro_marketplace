<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incidence extends Model
{
    protected $table = 'incidence';
	
    protected $fillable = [
        'user_id','order_id', 'problem','status','id_identification'
    ];

    public function user()
    {
        return $this->belongsTo('App\SysUser', 'user_id');
    }

    public function order()
    {
        return $this->belongsTo('App\AppOrgOrder', 'order_id');
    }
}
