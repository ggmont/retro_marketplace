<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SysShipping extends Model
{
    use SoftDeletes;

    protected $table = 'sysShippings';

    public static $message=[
        'name.required' => 'Nombre Obligatorio',
        'name.min' => 'Minimo 3 caracteres',
        'certified.required' => 'Elija el tipo de certificado',
        'from_id.required' => 'From country required',
        'to_id.required' => 'To country required',

        'large.required' => 'Large required',
        'weight.required' => 'Weight required',
        'hight.required' => 'hight required',

        'large.numeric' => 'Large only numbers',
        'weight.numeric' => 'Weight only numbers',
        'hight.numeric' => 'hight only numbers',

        'large.min' => 'Large minium value 1',
        'weight.min' => 'Weight minium value 1',
        'hight.min' => 'hight minium value 1',

        'max_weight.numeric' => 'Max weight only numbers',
        'max_value.numeric' => 'Max value only numbers',
        'stamp_price.numeric' => 'Stamp price only numbers',
        'price.numeric' => 'Price minium only numbers',

        'max_weight.required' => 'Max weight required',
        'max_value.required' => 'Max value required',
        'stamp_price.required' => 'Stamp price required',
        'price.required' => 'Price required',

        'max_weight.min' => 'Max weight minium value 0',
        'max_value.min' => 'Max value minium value 0',
        'stamp_price.min' => 'Stamp price minium value 0',
        'price.min' => 'Price minium value 0',
    ];
    public static $rules=[
        'name' => 'required|min:3',
        'certified' => 'required',
        'from_id' => 'required',
        'to_id' => 'required',
        'large' => 'required|numeric|min:1',
        'weight' => 'required|numeric|min:1',
        'hight' => 'required|numeric|min:1',
        'max_weight' => 'required|numeric|min:0',
        'max_value' => 'required|numeric|min:0',
        'stamp_price' => 'required|numeric|min:0',
        'price' => 'required|numeric|min:0',
        'type' => 'required',
    ];

    protected $fillable = ['name', 'certified','from_id', 'to_id' ,'large', 'width','hight', 'max_weight','max_value','stamp_price', 'type'];

    private $type = [
        'PA'  =>  'Paquete',
        'CA'  =>  'Carta'
    ];

    private $certified = [
        'Yes'  =>  'Carta Certificada',
        'No'  =>  'Carta Ordinaria'
    ];

    public function country_from() {
        return $this->hasMany('App\SysCountry', 'id', 'from_id');
    }
    
    public function country_to() {
        return $this->hasMany('App\SysCountry', 'id', 'to_id');
    }

    public function getTypeAttribute($value) {
        return isset($this->type[$value]) ? $this->type[$value] : $value;
    }

    public function getTypeDesAttribute($value) {
        return $this->type;
    }

}
