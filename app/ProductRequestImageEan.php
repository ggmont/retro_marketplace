<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class ProductRequestImageEan extends Model
{
    protected $table = 'product_request_image_ean';

    protected $fillable = [
        'inventory_id', 'image_ean', 'created_by', 'modified_by'
    ];

    public function product()
    {
        return $this->hasMany('App\ProductRequest', 'inventory_id');
    }

    public static function markOtherAsDeleted($inventory_id, $ids) {

		DB::table('product_request_image_ean')
            ->where('inventory_id', $inventory_id)
            ->whereNotIn('id', $ids)
            ->delete();

	}
}