<?php

namespace App\Observers;

use App\AppMessage;
use App\AppConversation;
use App\Events\MessageSent;

class MessageObserver
{
    /**
     * Listen to the Message created event.
     *
     * @param  \App\Message  $message
     * @return void
     */
    public function created(AppMessage $message)
    {
        

        $conversation = AppConversation::where('alias_user_id', $message->alias_from_id)
        ->where('alias_contact_id', $message->alias_to_id)->first();

        if ($conversation) {
        $conversation->last_message = "Tú: $message->content"; 
        $conversation->last_time = $message->created_at;
        $conversation->save();
        }

        $conversation = AppConversation::where('alias_contact_id', $message->alias_from_id)
                    ->where('alias_user_id', $message->alias_to_id)->first();

        if ($conversation) {
        $conversation->last_message = "$conversation->contact_name: $message->content"; 
        $conversation->last_time = $message->created_at;
        $conversation->save();
        }
        
        event(new MessageSent($message));
    }
}