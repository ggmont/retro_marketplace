<?php

namespace App;
use App\AppOrgUserInventory;
use Illuminate\Database\Eloquent\Model;

class AppOrgCartDetailUser extends Model
{
    protected $table = 'appOrgCartDetailUsers';

    public function inventory()
    {
        return $this->hasOne('App\AppOrgUserInventory','id','inventory_id');
    }

    public function inventories()
    {
        return $this->hasMany('App\AppOrgUserInventory','inventory_id');
    }


}
