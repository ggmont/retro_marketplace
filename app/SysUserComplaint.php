<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysUserComplaint extends Model
{
    protected $table = 'sys_user_complaint';
	
    protected $fillable = [
        'user_id','product_id', 'category', 'message','chat_id','type'
    ];

    public function user()
    {
        return $this->hasMany('App\SysUser', 'user_id');
    }

    public function chat()
    {
        return $this->belongsTo('App\SysUser', 'chat_id');
    }

    public function inventory() {
		return $this->belongsTo('App\AppOrgUserInventory', 'product_id');
    }

    public function product() {
		return $this->belongsTo('App\AppOrgProduct', 'product_id');
    }
}
