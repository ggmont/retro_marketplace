<?php

namespace App;
use App\SysUser;

use Illuminate\Database\Eloquent\Model;

class AppOrgUserRating extends Model
{
    protected $table = 'appOrgUserRatings';

    protected $fillable = ['id', 'seller_user_id', 'buyer_user_id', 'order_id', 'description',
  'processig', 'packaging', 'des_prod', 'status','score','created_at'];

    public function seller() {
        return $this->hasOne('App\SysUser' ,'id', 'seller_user_id');
    }
    
    public function buyer() {
        return $this->hasOne('App\SysUser' ,'id', 'buyer_user_id')->withTrashed();

    }

    public function getRatingAttribute(){
        return $this->score;
    }

    public function getRatingTypeAttribute(){
        $tipo = '';
        if($this->score < 0.5)
            $tipo = 'danger';
        
        if($this->score == 0.5)
            $tipo = 'warning';

        if($this->score > 0.5)
            $tipo = 'success';
        
        return $tipo;
    }

    public function getRatingIconAttribute(){
        $tipo = '';
        if($this->score < 0.5)
            $tipo = 'ion-close-round';
        
        if($this->score == 0.5)
            $tipo = 'ion-information';

        if($this->score > 0.5)
            $tipo = 'ion-checkmark-round';
        
        return $tipo;
    }

    public function getRatingStatusAttribute(){
        $tipo = '';
        if($this->score < 0.5)
            $tipo = 'Bad';
        
        if($this->score == 0.5)
            $tipo = 'Neutral';

        if($this->score > 0.5)
            $tipo = 'Good';
        
        return $tipo;
    }

    public function getProAttribute(){
        $tipo = '';
        if($this->processig == 3)
            $tipo = 'Bad';
        
        if($this->processig == 2)
            $tipo = 'Neutral';

        if($this->processig == 1)
            $tipo = 'Good';
        
        return $tipo;
    }

    public function getPacAttribute(){
        $tipo = '';
        if($this->packaging == 3)
            $tipo = 'Bad';
        
        if($this->packaging == 2)
            $tipo = 'Neutral';

        if($this->packaging == 1)
            $tipo = 'Good';
        
        return $tipo;
    }

    public function getDesAttribute(){
        $tipo = '';
        if($this->desc_prod == 3)
            $tipo = 'Bad';
        
        if($this->desc_prod == 2)
            $tipo = 'Neutral';

        if($this->desc_prod == 1)
            $tipo = 'Good';
        return $tipo;
    }


}
