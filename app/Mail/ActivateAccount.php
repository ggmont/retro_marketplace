<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ActivateAccount extends Mailable
{
    use Queueable, SerializesModels;

    public $activationToken;
    public $userName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($activationToken, $userName)
    {
        $this->activationToken = $activationToken;
        $this->userName = $userName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('brcode.emails.activate_account')
                    ->with([
                        'activationToken' => $this->activationToken,
                        'userName' => $this->userName,
                    ])
                    ->subject('Activación de Cuenta');
    }
}
