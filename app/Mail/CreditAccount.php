<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreditAccount extends Mailable
{
    use Queueable, SerializesModels;

    public $ben;
    public $iban;
    public $bic;
    public $concept;
    public $amount;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($ben, $iban, $bic, $concept, $amount)
    {
        $this->ben = $ben;
        $this->iban = $iban;
        $this->bic = $bic;
        $this->concept = $concept;
        $this->amount = $amount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('brcode.emails.petition_credit');
    }
}
