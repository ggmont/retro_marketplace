<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\AppOrgOrder;

class OrderCancellationNotification48HoursBuyer extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     *
     * @param  App\AppOrgOrder  $order
     * @return void
     */
    public function __construct(AppOrgOrder $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Aviso de cancelación del pedido - 48 horas";
        
        return $this->subject($subject)
                    ->view('brcode.emails.order_cancellation_48_hours_buyer');
    }
}

