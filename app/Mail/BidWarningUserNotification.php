<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\AppOrgUserInventory; // Asegúrate de importar el modelo necesario

class BidWarningUserNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $inventory;

    public function __construct(AppOrgUserInventory $inventory)
    {
        $this->inventory = $inventory;
    }

    public function build()
    {
        return $this->subject('¡Atención! Oferta en tu producto')
            ->view('brcode.emails.bid-warning-user-notification');
    }
}
