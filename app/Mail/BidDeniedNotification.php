<?php

namespace App\Mail;

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BidDeniedNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $inventory;

    public function __construct($inventory)
    {
        $this->inventory = $inventory;
    }

    public function build()
    {
        return $this->subject('Oferta denegada')
                    ->view('brcode.emails.bid-denied-notification');
    }
}
