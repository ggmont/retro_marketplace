<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\AppOrgUserInventory; // Asegúrate de importar el modelo necesario

class BidSuccessBuyerNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $inventory;

    public function __construct(AppOrgUserInventory $inventory)
    {
        $this->inventory = $inventory;
    }

    public function build()
    {
        return $this->subject('Se ha acabado el tiempo de la oferta , pero todavia puedes tener una oportunidad')
            ->view('brcode.emails.bid-success-buyer-notification');
    }
}
