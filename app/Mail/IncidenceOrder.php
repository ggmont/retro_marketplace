<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class IncidenceOrder extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $order;
    public $incidence;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $order,$incidence)
    {
        $this->user = $user;
        $this->order = $order;
        $this->incidence = $incidence;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject('Id identificador de la incidencia')->view('brcode.emails.incidence');
    }
}
