<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\AppOrgUserInventory; // Asegúrate de importar el modelo necesario

class HalfwayNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $inventory;

    public function __construct(AppOrgUserInventory $inventory)
    {
        $this->inventory = $inventory;
    }

    public function build()
    {
        return $this->subject('Tu oferta está a la mitad')
            ->view('brcode.emails.halfway-notification');
    }
}

