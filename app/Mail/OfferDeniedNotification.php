<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\AppOrgUserInventory;

class OfferDeniedNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $inventory;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AppOrgUserInventory $inventory)
    {
        $this->inventory = $inventory;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Oferta Rechazada')
            ->markdown('brcode.emails.offer_denied_notification'); // Asegúrate de crear esta vista en resources/views/emails/offer_denied_notification.blade.php
    }
}
