<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReplacedBidNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $inventory;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inventory)
    {
        $this->inventory = $inventory;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Tu oferta ha sido reemplazada')
        ->view('brcode.emails.replaced_bid_notification');
    }
}


