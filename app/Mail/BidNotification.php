<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\AppOrgUserInventory; // Asegúrate de importar el modelo necesario

class BidNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $inventory;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AppOrgUserInventory $inventory)
    {
        $this->inventory = $inventory;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Nueva oferta en tu producto')
            ->view('brcode.emails.bid_notification'); // Asegúrate de crear esta vista en resources/views/emails/bid_notification.blade.php
    }
}

