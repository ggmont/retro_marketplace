<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestForm extends Mailable
{
    use Queueable, SerializesModels;

    public $emailData = [];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($emailData)
    {
      $this->emailData = $emailData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        $subject = 'Nueva solicitud de registro de producto';
        $productData = [
            'product' => $this->emailData['product'],
            'platform' => $this->emailData['platform'],
            'region' => $this->emailData['region'],
            'quantity' => $this->emailData['quantity'],
            'comments' => $this->emailData['comments'],
            'box_condition' => $this->emailData['box_condition'],
            'manual_condition' => $this->emailData['manual_condition'],
            'cover_condition' => $this->emailData['cover_condition'],
            'game_condition' => $this->emailData['game_condition'],
            'extra_condition' => $this->emailData['extra_condition'],
            'in_collection' => $this->emailData['in_collection'],
        ];
        return $this->subject($subject)->view('brcode.emails.request_form', compact('productData'));

    }
    
    
    
    
    
}