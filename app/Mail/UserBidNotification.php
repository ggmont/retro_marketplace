<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserBidNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $orderIdentification;
    public $username;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($orderIdentification, $username)
    {
        $this->orderIdentification = $orderIdentification;
        $this->username = $username;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Oferta aceptada - Inicio del proceso de compra')
                    ->view('brcode.emails.user_bid_notification')
                    ->with([
                        'username' => $this->username,
                        'orderIdentification' => $this->orderIdentification,
                    ]);
    }
}
