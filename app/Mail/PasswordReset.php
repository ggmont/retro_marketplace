<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordReset extends Mailable
{
    use Queueable, SerializesModels;

    public $resetToken;
    public $username;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($resetToken, $username)
    {
        $this->resetToken = $resetToken;
        $this->username = $username;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('brcode.emails.password_reset')
                    ->with([
                        'resetToken' => $this->resetToken,
                        'username' => $this->username,
                    ])
                    ->subject('Restablecimiento de Contraseña');
    }
}
