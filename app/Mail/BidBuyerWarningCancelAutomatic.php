<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\AppOrgUserInventory; // Asegúrate de importar el modelo necesario

class BidBuyerWarningCancelAutomatic extends Mailable
{
    use Queueable, SerializesModels;

    public $inventory;

    public function __construct(AppOrgUserInventory $inventory)
    {
        $this->inventory = $inventory;
    }

    public function build()
    {
        return $this->subject('Oferta finalizada , pero..')
            ->view('brcode.emails.bid-warning-buyer-cancel-notificacion');
    }
}
