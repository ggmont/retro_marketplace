<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestProductInventory extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $inventory;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $inventory)
    {
        $this->user = $user;
        $this->inventory = $inventory;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('brcode.emails.request-product-inventory')
                    ->subject('Producto Aprobado y Disponible')
                    ->with([
                        'user' => $this->user,
                        'inventory' => $this->inventory,
                    ]);
    }
}

