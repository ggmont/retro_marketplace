<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class AppConversation extends Model
{    
    protected $table = 'appConversations';

    protected $fillable = ['alias_user_id', 'alias_contact_id','last_message','status_deleted_one','status_deleted_two','seen_user_id','seen_contact_id'];

    protected $appends = ['contact_name', 'contact_image'];

    protected $dates = ['last_time'];

    public function user(){
    	return $this->belongsTo('App\SysUser', 'alias_user_id');
    }
 

    public function prueba(){
    	return $this->belongsTo('App\SysUser', 'alias_contact_id');
    }

    public function getContactImageAttribute()
    {
    	return $this->contact()->first(['profile_picture']);
    }


    public function userName()
    {
    	return $this->hasOne('App\SysUser', 'user_name', 'alias_user_id');
    }

    public function conName()
    {
    	return $this->hasOne('App\SysUser', 'user_name', 'alias_contact_id');
    }

    public function contact() 
    {	
        // Conversation N   1 User (contact)
        return $this->hasOne('App\SysUser', 'user_name', 'alias_contact_id')->withTrashed();
    }
}
