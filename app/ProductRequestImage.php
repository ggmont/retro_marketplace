<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class ProductRequestImage extends Model
{
    protected $table = 'product_request_image';

    protected $fillable = [
        'inventory_id', 'image_path', 'created_by', 'modified_by'
    ];

    public function product()
    {
        return $this->hasMany('App\ProductRequest', 'inventory_id');
    }

    public static function markOtherAsDeleted($inventory_id, $ids) {

		DB::table('product_request_image')
            ->where('inventory_id', $inventory_id)
            ->whereNotIn('id', $ids)
            ->delete();

	}
}
