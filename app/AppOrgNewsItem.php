<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppOrgNewsItem extends Model {
 use SoftDeletes;
 protected $table = 'appOrgNewsItems';
 protected $fillable = ['title','content','is_enabled','source_url','image_path'];
}
