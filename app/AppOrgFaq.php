<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppOrgFaq extends Model {

  use SoftDeletes;
  protected $table = 'appOrgFaqs';
  
  protected $fillable = ['question','answer','is_enabled'];
}
