<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class AppMessage extends Model
{
    protected $table = 'appMessages';
    
    protected $fillable = ['alias_from_id', 'alias_to_id','alias_from_del','alias_to_del','read','content','image'
    ,'delete_message','status_deleted_one','status_deleted_two'];

    public function user(){
    	return $this->belongsTo('App\SysUser', 'alias_from_id');
    }

    public function probando(){
    	return $this->belongsTo('App\SysUser', 'alias_to_id');
    }
    
}
