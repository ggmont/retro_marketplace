<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysUserCash extends Model
{
    	
    protected $table = 'sysUserCashs';
    
    protected $fillable = [
        'user_id', 'user_cash', 'user_especial'
    ];

}
