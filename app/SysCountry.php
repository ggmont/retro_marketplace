<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysCountry extends Model
{
    protected $table = 'sysCountries';

    protected $fillable = ['name', 'region', 'sub_region'];

    private $country = [
        'Spain' => "España",
        'Andorra' => "Andorra",
        'Åland Islands' => "Åland Islands",
        'Albania' => "Albania",
        'Austria' => "Austria",
        'Belarus' => "Belarus",
        'Belgium' => "Belgica",
        'Bosnia and Herzegovina' => "Bosnia and Herzegovina",
        'Bulgaria' => "Bulgaria",
        'Croatia' => "Croacia",
        'Czech Republic' => "Czech Republic",
        'Denmark' => "Dinamarca",
        'Estonia' => "Estonia",
        'Faroe Islands' => "Faroe Islands",
        'Finland' => "Finlandia",
        'France' => "Francia",
        'Germany' => "Alemania",
        'Gibraltar' => "Gibraltar",
        'Guernsey' => "Guernsey",
        'Holy See' => "Holy See",
        'Hungary' => "Hungria",
        'Iceland' => "Islandia",
        'Ireland' => "Irlanda",
        'Isle of Man' => "Isle of Man",
        'Italy' => "Italia",
        'Jersey' => "Jersey",
        'Latvia' => "Latvia",
        'Liechtenstein' => "Liechtenstein",
        'Lithuania' => "Lithuania",
        'Luxembourg' => "Luxembourg",
        'Macedonia (the former Yugoslav Republic of)' => "Macedonia (the former Yugoslav Republic of)",
        'Malta' => "Malta",
        'Moldova (Republic of)' => "Moldova (Republic of)",
        'Monaco' => "Monaco",
        'Montenegro' => "Montenegro",
        'Netherlands' => "Paises Bajos",
        'Norway' => "Norway",
        'Poland' => "Poland",
        'Portugal' => "Portugal",
        'Romania' => "Romania",
        'Russian Federation' => "Russian Federation",
        'San Marino' => "San Marino",
        'Serbia' => "Serbia",
        'Slovakia' => "Slovakia",
        'Slovenia' => "Slovenia",
        'Svalbard and Jan Mayen' => "Svalbard and Jan Mayen",
        'Sweden' => "Sweden",
        'Switzerland' => "Switzerland",
        'Ukraine' => "Ukraine",
        'United Kingdom of Great Britain and Northern Ireland' => "Reino Unido"
      ];

    public static function getCountryName($id)
    {
      
      $country = [
      'Spain' => "España",
      'Andorra' => "Andorra",
      'Åland Islands' => "Åland Islands",
      'Albania' => "Albania",
      'Austria' => "Austria",
      'Belarus' => "Belarus",
      'Belgium' => "Belgica",
      'Bosnia and Herzegovina' => "Bosnia and Herzegovina",
      'Bulgaria' => "Bulgaria",
      'Croatia' => "Croacia",
      'Czech Republic' => "Czech Republic",
      'Denmark' => "Dinamarca",
      'Estonia' => "Estonia",
      'Faroe Islands' => "Faroe Islands",
      'Finland' => "Finlandia",
      'France' => "Francia",
      'Germany' => "Alemania",
      'Gibraltar' => "Gibraltar",
      'Guernsey' => "Guernsey",
      'Holy See' => "Holy See",
      'Hungary' => "Hungria",
      'Iceland' => "Islandia",
      'Ireland' => "Irlanda",
      'Isle of Man' => "Isle of Man",
      'Italy' => "Italia",
      'Jersey' => "Jersey",
      'Latvia' => "Latvia",
      'Liechtenstein' => "Liechtenstein",
      'Lithuania' => "Lithuania",
      'Luxembourg' => "Luxembourg",
      'Macedonia (the former Yugoslav Republic of)' => "Macedonia (the former Yugoslav Republic of)",
      'Malta' => "Malta",
      'Moldova (Republic of)' => "Moldova (Republic of)",
      'Monaco' => "Monaco",
      'Montenegro' => "Montenegro",
      'Netherlands' => "Paises Bajos",
      'Norway' => "Norway",
      'Poland' => "Poland",
      'Portugal' => "Portugal",
      'Romania' => "Romania",
      'Russian Federation' => "Russian Federation",
      'San Marino' => "San Marino",
      'Serbia' => "Serbia",
      'Slovakia' => "Slovakia",
      'Slovenia' => "Slovenia",
      'Svalbard and Jan Mayen' => "Svalbard and Jan Mayen",
      'Sweden' => "Sweden",
      'Switzerland' => "Switzerland",
      'Ukraine' => "Ukraine",
      'United Kingdom of Great Britain and Northern Ireland' => "Reino Unido",
      "Greece" => "Grecia"
      ];
  
      return $country[$id];
    }
}
