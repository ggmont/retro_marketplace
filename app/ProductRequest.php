<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class ProductRequest extends Model
{
    protected $table = 'product_request';

    protected $fillable = [
        'category', 'user_id','email', 'product','product_id', 'platform', 'region', 'quantity', 'price',
        'comments', 'box_condition', 'manual_condition', 'cover_condition', 'game_condition', 'extra_condition', 'inside_condition','status'
        ,'aprobed_by','in_collection','auction'
    ];

    private $title_game = [
        'NEW' => "Nuevo",
        'USED-NEW' => "Usado como nuevo",
        'USED' => "Usado",
        'USED-VERY' => "Muy usado",
        'NOT-WORK' => "No funciona",
        'NOT-PRES' => "No aplica"
    ];

    public function images() {
        return $this->hasMany('App\ProductRequestImage','inventory_id','id');
    }
    

    public function user(){
    	return $this->belongsTo('App\SysUser', 'user_id');
    }

    public function getBoxAttribute()
    {
        $value = '';
        if ($this->box_condition == 'NEW') {
            $value = 'img/new.png';
        }
        if ($this->box_condition == 'USED-NEW') {
            $value = 'img/used-new.png';
        }
        if ($this->box_condition == 'USED') {
            $value = 'img/used.png';
        }
        if ($this->box_condition == 'USED-VERY') {
            $value = 'img/used-very.png';
        }
        if ($this->box_condition == 'NOT-WORK') {
            $value = 'img/no-funciona.png';
        }
        if ($this->box_condition == 'NOT-PRES') {
            $value = 'img/not.png';
        }
        return $value;
    }
    public function getCoverAttribute()
    {
        $value = '';
        if ($this->cover_condition == 'NEW') {
            $value = 'img/new.png';
        }
        if ($this->cover_condition == 'USED-NEW') {
            $value = 'img/used-new.png';
        }
        if ($this->cover_condition == 'USED') {
            $value = 'img/used.png';
        }
        if ($this->cover_condition == 'USED-VERY') {
            $value = 'img/used-very.png';
        }
        if ($this->cover_condition == 'NOT-WORK') {
            $value = 'img/no-funciona.png';
        }
        if ($this->cover_condition == 'NOT-PRES') {
            $value = 'img/not.png';
        }
        return $value;
    }

    public function getManualAttribute()
    {
        $value = '';
        if ($this->manual_condition == 'NEW') {
            $value = 'img/new.png';
        }
        if ($this->manual_condition == 'USED-NEW') {
            $value = 'img/used-new.png';
        }
        if ($this->manual_condition == 'USED') {
            $value = 'img/used.png';
        }
        if ($this->manual_condition == 'USED-VERY') {
            $value = 'img/used-very.png';
        }
        if ($this->manual_condition == 'NOT-WORK') {
            $value = 'img/no-funciona.png';
        }
        if ($this->manual_condition == 'NOT-PRES') {
            $value = 'img/not.png';
        }
        return $value;
    }

    public function getGameAttribute()
    {
        $value = '';
        if ($this->game_condition == 'NEW') {
            $value = 'img/new.png';
        }
        if ($this->game_condition == 'USED-NEW') {
            $value = 'img/used-new.png';
        }
        if ($this->game_condition == 'USED') {
            $value = 'img/used.png';
        }
        if ($this->game_condition == 'USED-VERY') {
            $value = 'img/used-very.png';
        }
        if ($this->game_condition == 'NOT-WORK') {
            $value = 'img/no-funciona.png';
        }
        if ($this->game_condition == 'NOT-PRES') {
            $value = 'img/not.png';
        }
        return $value;
    }

    public function getExtraAttribute()
    {
        $value = '';
        if ($this->extra_condition == 'NEW') {
            $value = 'img/new.png';
        }
        if ($this->extra_condition == 'USED-NEW') {
            $value = 'img/used-new.png';
        }
        if ($this->extra_condition == 'USED') {
            $value = 'img/used.png';
        }
        if ($this->extra_condition == 'USED-VERY') {
            $value = 'img/used-very.png';
        }
        if ($this->extra_condition == 'NOT-WORK') {
            $value = 'img/no-funciona.png';
        }
        if ($this->extra_condition == 'NOT-PRES') {
            $value = 'img/not.png';
        }
        return $value;
    }

    public function getInsideAttribute()
    {
        $value = '';
        if ($this->inside_condition == 'NEW') {
            $value = 'img/new.png';
        }
        if ($this->inside_condition == 'USED-NEW') {
            $value = 'img/used-new.png';
        }
        if ($this->inside_condition == 'USED') {
            $value = 'img/used.png';
        }
        if ($this->inside_condition == 'USED-VERY') {
            $value = 'img/used-very.png';
        }
        if ($this->inside_condition == 'NOT-WORK') {
            $value = 'img/no-funciona.png';
        }
        if ($this->inside_condition == 'NOT-PRES') {
            $value = 'img/not.png';
        }
        return $value;
    }

    public static function getCondicionName($id)
    {
        $title_game = [
            'NEW' => "Nuevo",
            'USED-NEW' => "Usado como nuevo",
            'USED' => "Usado",
            'USED-VERY' => "Muy usado",
            'NOT-WORK' => "No funciona",
            'NOT-PRES' => "No tiene"
        ];
        return $title_game[$id];
    }
}
