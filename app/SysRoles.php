<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SysRoles extends Model
{
    use SoftDeletes;
    
	protected $dates = ['deleted_at'];
	
	protected $table = 'sysRoles';
	
    protected $fillable = [
        'name', 'description', 'is_enabled',
    ];
	
	public function privileges() {
		return $this->hasMany('App\SysRolePrivileges','role_id');
	}
}
