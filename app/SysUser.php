<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\AppOrgCartUser;
use App\AppOrgUserRating;
use App\AppOrgOrder;
use App\SysUserRoles;
use Awssat\Visits\Models\Visit;

use Auth;
use Laravel\Sanctum\HasApiTokens;

class SysUser extends Authenticatable
{
    use HasApiTokens, SoftDeletes;
    
    protected $appends = ['cash'];
    
    protected $dates = ['deleted_at'];
	
	protected $table = 'sysUsers';
	
    protected $fillable = [
        'first_name', 'last_name','twitch_user','email', 'password', 'user_name','comision','phone','address','zipcode','city'
        ,'terms_and_conditions','profile_picture','cover_picture','fcm_token','dni','province'
    ];
    

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function visitsCounter()
    {
        return visits($this);
    }

    public function visits()
    {
        return visits($this)->relation();
    }
    
    public function getVentasAttribute() {
        $i = 0;
        foreach(AppOrgOrder::where('seller_user_id', $this->id)->get() as $key){
            $key->detail;
            foreach($key->details AS $detail){
                $i += $detail->quantity;
            }
            
        }

        return $i;
    }
    
	public function getSalesAttribute() {
		return AppOrgOrder::where('seller_user_id', $this->id)->count();
    }
    
    public function inventory() {
		return $this->hasMany('App\AppOrgUserInventory', 'user_id');
    }

 

    public function prueba() {
		return $this->hasMany('App\AppMessages', 'user_id');
    }

	public function roles() {
		return $this->hasMany('App\SysUserRoles','user_id');
    }

	public function getFrontRoleAttribute(){
        if(SysUserRoles::where('user_id', Auth::id())->where('role_id', 5)->first()){
            return true;
        } else {
            return false;
        }
    }
	public function getAdminRoleAttribute(){
        if(SysUserRoles::where('user_id', Auth::id())->where('role_id', 1)->first()){
            return true;
        } else {
            return false;
        }
    }
    
    public function country() {
        return $this->hasOne('App\SysCountry', 'id', 'country_id');
    }

    public function carts() {
		return $this->hasMany('App\AppOrgCartUser','user_id');
    }
      
    public function bank() {
		return $this->hasOne('App\SysAccountBank','user_id');
    }

    public function getScoreAttribute() {
        //AppOrgUserRating
        $score = 0;
        $valores = array('user-o', 'No rating', 'default', 0);
        if(AppOrgUserRating::where('seller_user_id', $this->id)->where('processig', '>', 0)->get()->count() > 0){
            $rating = AppOrgUserRating::where('seller_user_id', $this->id)->where('processig', '>', 0)->get();
            foreach($rating as $key){
                $score += $key->score;
            }
            $total = 0;
            if($rating->count() > 0){
                if($score > 0)
                    $score = $score / $rating->count();
                
                if($score > 0.60){
                    $valores = array('check', 'Good Seller', 'success', $rating->count());
                }
                    
                if($score < 0.40){
                    $valores = array('exclamation-circle', 'Bad Seller', 'danger', $rating->count());
                }
                    
                if($score >= 0.40 && $score <= 0.60){
                    $valores = array('minus', 'Neutral Seller', 'warning', $rating->count());
                }
                return $valores ;
            }

                
        }else{
            return $valores;  
        }
    }

    public function getCartAttribute(){
        $cart = $this->carts()->where('status','Active')->first();
        if($cart)
            return $cart;
        $cart = new AppOrgCartUser();
        $cart->status = 'Active';
        $cart->user_id = $this->id;
        $cart->save();
        return $cart;
    }

    public function getLifetimeCartAttribute(){
        if($this->carts()->where('status','active')->first()){
            $cart = $this->carts()->where('status','active')->first();
            if(strtotime($cart->updated_at) < strtotime($cart->created_at . "+6hours")){
                $cart->updated_at = date('Y-m-d H:i:s');
                $cart->save();
            }
        }
        
    }

    public function getReferidosAttribute() {
        return SysUser::whereIn('id', SysUserReferred::where('master_id', $this->id)->pluck('son_id'))->select('first_name', 'last_name', 'email', 'user_name')->get();
    }

    public function getCollaboratorAttribute() {
        return SysUser::whereIn('id', SysUserReferred::where('son_id', $this->id)->pluck('master_id'))->get();
    }

    public function getReferenciaIdAttribute() {
        return SysUserReferred::where('son_id', $this->id)->first() ? SysUserReferred::where('son_id', $this->id)->first()->master_id : 0;
    }

    public function setCashAttribute($value)
    {
        if(SysUserCash::where('user_id', $this->id)->first()) {
            $cash = SysUserCash::where('user_id', $this->id)->first();
            $cash->user_cash = $value;
            $cash->save();
        } else {
            SysUserCash::create([
                'user_id' => $this->id,
                'user_cash' => $this->amountEURO + $value,
                'user_especial' => 0
            ]);
        }
    }

    public function getCashAttribute() {
        if(!SysUserCash::where('user_id', $this->id)->first()){
            SysUserCash::create([
                'user_id' => $this->id,
                'user_cash' => $this->amountEURO ? $this->amountEURO : 0,
                'user_especial' => 0
            ]);
        }
        return (SysUserCash::where('user_id', $this->id)->first() ? SysUserCash::where('user_id', $this->id)->first()->user_cash : 0);
    }

    public function setSpecialCashAttribute($value)
    {
        if(SysUserCash::where('user_id', $this->id)->first()) {
            $cash = SysUserCash::where('user_id', $this->id)->first();
            $cash->user_especial = $value;
            $cash->save();
        } else {
            SysUserCash::create([
                'user_id' => $this->id,
                'user_cash' => $this->amountEURO + $value,
                'user_especial' => 0
            ]);
        }
    }

    public function getSpecialCashAttribute() {
        if(!SysUserCash::where('user_id', $this->id)->first()){
            SysUserCash::create([
                'user_id' => $this->id,
                'user_cash' => $this->amountEURO ? $this->amountEURO : 0,
                'user_especial' => 0
            ]);
        }
        return (SysUserCash::where('user_id', $this->id)->first() ? SysUserCash::where('user_id', $this->id)->first()->user_especial : 0);
    }

    public function adminlte_image()
    {
        if(auth()->user()->profile_picture) {
            return url(auth()->user()->profile_picture);
        } else {
             return asset('img/profile-picture-not-found.png');
        }
       
    }

        public function adminlte_desc()
    {
        return auth()->user()->user_name ;
    }

        public function adminlte_profile_url()
    {
        return url('/account/profile');
    }

    

}
