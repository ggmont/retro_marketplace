<?php

namespace App;

use App\AppOrgCartShipping;
use App\SysShipping;
use App\AppOrgCartUser;
use App\SysUser;
use Illuminate\Database\Eloquent\Model;


class AppOrgCartShipping extends Model
{
    protected $table = 'appOrgCartShippings';

    public function cart()
    {
        return $this->belongsTo(AppOrgCartUser::class);
    }

    public function user()
    {
        return $this->belongsTo(SysUser::class);
    }

    public function shipping()
    {
        return $this->belongsTo(SysShipping::class);
    }

}
