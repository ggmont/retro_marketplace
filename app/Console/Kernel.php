<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\UpdateOrderStatus;
use App\Console\Commands\UpdateCountdownHours;
use App\Console\Commands\UpdateCountdownCancel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Images::class,
        Commands\UserInventory::class,
        Commands\BackupImage::class,
        Commands\HelloWorld::class,
        UpdateOrderStatus::class, // Agregar esta línea
        UpdateCountdownHours::class, // Agregar esta línea
        UpdateCountdownCancel::class, // Agregar esta línea
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('actualizar:cancel')->everyMinute();
    }
}
