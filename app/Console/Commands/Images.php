<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Log;
use File;
use App\AppOrgProduct;
use App\AppOrgProductImage;
use App\SysPrivileges;
use App\SysRolePrivileges;

class Images extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'image:new';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $prod = AppOrgProduct::where('modified_by', 1000)->get();
        // $prodID = AppOrgProduct::where('modified_by', 1000)->pluck('id');
        // $images = AppOrgProductImage::whereIn('product_id', $prodID)->delete();
        // Log::info($images);
        // $correccion = AppOrgProductImage::where('created_by', 0)->get();

        foreach ($prod as $key) {
            // $prod = AppOrgProduct::whereNotNull('id_imagen')->get();
            $name =  strtolower(str_replace(" ","_", $key->platform)) . '_' . date('Ymd') . $key->id . date('His') . '.JPG';
            $fail = true;

            error_log(public_path() . '/img4/' . $key->id_imagen  . '.JPG');
            error_log(public_path() . '/images/'. $name);
            try {
                File::copy(public_path() . '/img4/' . $key->id_imagen  . '.JPG' , public_path() . '/images/'. $name);
                $fail = true;
                error_log('true');
                
            }  catch (\Exception $ex) {
                error_log('false');
                $fail = false;
                // error_log('Error on ' . $key->id_imagen . ' - new name ' . $name);
            }
            if(!$fail){
                try {
                    File::copy(public_path() . '/img4/' . $key->id_imagen  . '.jpg' , public_path() . '/images/'. $name);
                    $fail = true;
                    error_log('true');
                    
                }  catch (\Exception $ex) {
                    error_log('false');
                    $fail = false;
                    // error_log('Error on ' . $key->id_imagen . ' - new name ' . $name);
                }
            }

            if($fail){
                $imgProd = new AppOrgProductImage();
                $imgProd->product_id = $key->id;
                $imgProd->image_path = $name;
                $imgProd->save();
            }
         
        }

        AppOrgProduct::where('modified_by', 1000)->update(['modified_by' => 900]);

        error_log('success');

    }
}
