<?php

namespace App\Console\Commands;
use App\AppOrgUserInventory;
use Illuminate\Console\Command;

class HelloWorld extends Command
{
    protected $signature = 'hello:world';

    protected $description = 'Saluda al mundo';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        AppOrgUserInventory::where('countdown_hours', '>', 0)->decrement('countdown_hours');
        $this->info('Contador actualizado correctamente.');
    }
    
}
