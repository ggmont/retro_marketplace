<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\AppOrgUserInventory;
use App\AppOrgUserHistorialBidInventory;
use App\Mail\BidBuyerWarningCancelAutomatic;
use Illuminate\Support\Facades\Mail;
use App\Mail\HalfwayNotification;
use App\Mail\BidWarningUserNotification;
use App\Mail\TimeOut;
use App\Mail\BidWarningCancelAutomatic;

class UpdateCountdownCancel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'actualizar:cancel'; // Define el nombre del comando
    protected $description = 'Actualiza el contador de un producto que tiene la ultima oferta';

    /**
     * The console command description.
     *
     * @var string
     */


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */

    public function handle()
    {
        // Obtener registros donde countdown_hours es mayor que 0
        $inventoriesToUpdate = AppOrgUserInventory::where('countdown_cancel', '>', 0)->get();

        foreach ($inventoriesToUpdate as $inventory) {
            $inventory->decrement('countdown_cancel');


            // Verificar si countdown_hours llegó a 0 y actualizar auction_type
            if ($inventory->countdown_cancel === 0) {

                // Verificar si hay ofertas (user_bid) y enviar notificación por correo
                if (!empty($inventory->user_bid)) {

                    $historialRecords = AppOrgUserHistorialBidInventory::where('user_bid', $inventory->user_bid)
                    ->where('inventory_id', $inventory->id)
                    ->get();

                    foreach ($historialRecords as $historialBidder) {
                        $historialBidder->update(['status' => 4]);
                    }

                    $inventory->update([
                        'user_bid' => null
                    ]);

            
 

                    Mail::to($inventory->user->email)->send(new BidWarningCancelAutomatic($inventory));
                    Mail::to($inventory->userbid->email)->send(new BidBuyerWarningCancelAutomatic($inventory));
                } else {

                }
            }
        }

        $this->info('Contador de ofertas actualizado correctamente.');
    }
}
