<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\AppOrgOrder;
use App\AppOrgOrderCancel;
use App\AppOrgOrderDetail;
use App\AppOrgUserInventory;
use App\SysUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderCancellationNotification24HoursBuyer;
use App\Mail\OrderCancellationNotification24HoursSeller;
use App\Mail\OrderCancellationNotification48HoursBuyer;
use App\Mail\OrderCancellationNotification48HoursSeller;

class UpdateOrderStatus extends Command
{
    protected $signature = 'orders:update_status';

    protected $description = 'Update order status after 48 hours - Actualizar el estado del pedido después de 48 horas';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        
        $orders = AppOrgOrder::where('status', 'RP')
        ->where(function ($query) {
            $query->where('created_at', '>=', Carbon::now()->subHours(24))
                ->orWhere('created_at', '>=', Carbon::now()->subHours(48));
        })
        ->get();    
        foreach ($orders as $order) {
            // Enviar correo electrónico de aviso después de 24 horas
            if ($order->created_at <= Carbon::now()->subHours(24) && $order->email_sent === 'N') {
                Mail::to($order->buyer->email)->send(new OrderCancellationNotification24HoursBuyer($order));
                Mail::to($order->seller->email)->send(new OrderCancellationNotification24HoursSeller($order));
                $order->email_sent = 'Y';
                $order->save();
            }            
            // Enviar correo electrónico de aviso después de 48 horas
            if ($order->created_at <= Carbon::now()->subHours(48)) {
                $order->status = 'CN';
                $order->save();

                $orderCancel = new AppOrgOrderCancel;
                $orderCancel->order_id = $order->id;
                $orderCancel->seller_user_id = $order->seller_user_id;
                $orderCancel->buyer_user_id =$order->buyer_user_id;
                $orderCancel->cause = 'Limite de 48 horas';
                $orderCancel->solicited_by = 'B';
                $orderCancel->save();
    

                $detail = AppOrgOrderDetail::where('order_id', $order->id)->get();
              
                foreach($detail as $key){
                  $inventory = AppOrgUserInventory::find($key->inventory_id);
                  $inventory->quantity += $key->quantity;
                  $inventory->save();
                }

                Mail::to($order->buyer->email)->send(new OrderCancellationNotification48HoursBuyer($order));
                Mail::to($order->seller->email)->send(new OrderCancellationNotification48HoursSeller($order));
            }
        }
    }
}
