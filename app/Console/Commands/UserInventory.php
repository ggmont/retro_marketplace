<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\AppOrgOrder;
use App\AppOrgOrderDetail;
use App\AppOrgUserInventory;
use App\AppOrgProductFeedback;


class UserInventory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inventory:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para testear graficas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // $inventory = AppOrgUserInventory::all();
        AppOrgProductFeedback::truncate();
        foreach (AppOrgOrderDetail::all() as $detail) {

            
            $key = AppOrgUserInventory::where('id', $detail->inventory_id)->first();
            if($key){

                $feedback = new AppOrgProductFeedback();
                $feedback->box_condition = $key->box_condition;
                $feedback->manual_condition = $key->manual_condition;
                $feedback->cover_condition = $key->cover_condition;
                $feedback->game_condition = $key->game_condition;
                $feedback->extra_condition = $key->extra_condition;
                $feedback->inside_condition = $key->inside_condition;
                $feedback->ean = $key->ean;
                $feedback->inventory_id = $key->id;

                $feedback->seller_user_id = $key->user_id; //vendedor inventario

                $order = AppOrgOrder::where('id', $detail->order_id)->first();
                
                $feedback->buyer_user_id = $order->buyer_user_id; //comprador

                $feedback->order_id = $detail->order_id; //vendedor
    
                $feedback->product_id = $key->product_id;
                $feedback->price = $key->price;
                $feedback->created_at = $detail->updated_at;
                $feedback->updated_at = $detail->updated_at;
                $feedback->save();
            }

            # code...
        }

        error_log('SUCCESS');
    }
}
