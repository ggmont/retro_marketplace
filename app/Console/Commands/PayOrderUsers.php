<?php

namespace App\Console\Commands;

use App\AppOrgOrder;
use App\AppOrgCartUser;
use App\AppOrgOrderDetail;
use App\AppOrgUserInventory;
use App\SysUser;
use Auth;
use Log;
use File;
use App\AppOrgProduct;
use App\AppOrgProductImage;

use Illuminate\Console\Command;

class PayOrderUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:complete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command check payment orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $order = AppOrgOrder::whereIn('status', ['CR', 'CW', 'RP'])->get();
        foreach($order as $key) {

            if($key->paid_out == 'N'){

                if($key->max_pay_out < date('Y-m-d H:i:s') && $key->status == 'RP'){
                    $key->status = 'CN';
                    $key->save();
                    $detail = AppOrgOrderDetail::where('order_id', $key->id)->get();
                    foreach($detail as $keyDetail) {
                        $keyDetail->status = 'CN';
                        $keyDetail->updated_at = date('Y-m-d H:i:s');
                        $keyDetail->save();
                        $inventory = AppOrgUserInventory::find($keyDetail->inventory_id);
                        $inventory->quantity += $keyDetail->quantity;
                        $inventory->save();
                    }
                }

            }

            elseif($key->paid_out == 'Y'){
                //$ship = SysShipping::find($key->shipping_id);
                if($key->max_pay_out < date('Y-m-d H:i:s') && $key->status == 'CR'){

                    $key->status = 'CN';
                    $key->save();

                    $user_buyer = SysUser::find($key->buyer_user_id);
                    $user_buyer->cash = $key->total;
                    $user_buyer->save();

                    $detail = AppOrgOrderDetail::where('order_id', $key->id)->get();
                    foreach($detail as $keyDetail) {
                        $keyDetail->status = 'CN';
                        $keyDetail->updated_at = date('Y-m-d H:i:s');
                        $keyDetail->save();
                        $inventory = AppOrgUserInventory::withTrashed()->find($keyDetail->inventory_id);
                        $inventory->quantity += $keyDetail->quantity;
                        $inventory->save();
                    }
                }

            }
            
        }
        
        Log::info('Pay End ' . date('Y-m-d H:i:s'));

        if(AppOrgCartUser::where('status','Active')->get()){
            foreach(AppOrgCartUser::where('status','Active')->get() as $key){

                if(strtotime(date('Y-m-d H:i:s')) > strtotime($key->updated_at . "+2hours")){
                    if($key->details->count() > 0){
                        foreach($key->details as $key_detail){
                            $inventory = AppOrgUserInventory::find($key_detail->inventory_id);
                            $inventory->quantity += $key_detail->quantity;
                            $inventory->save();
                        }
                    }
                    $key->status = 'Complete';
                    $key->save();
                }
            }
        }

        Log::info('Cart End ' . date('Y-m-d H:i:s'));
    }
}
