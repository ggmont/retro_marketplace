<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Log;
use File;
use App\AppOrgProduct;
use App\AppOrgProductImage;

class BackupImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'image:back';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $i = 0;
        // AppOrgProductImage::where('modified_by', 1000)->count()
        // $ids = AppOrgProductImage::pluck('product_id');

        // error_log(AppOrgProduct::whereIn('id', $ids)->where('modified_by', 1000)->count());

        foreach(AppOrgProductImage::all() as $img){
            try {
                File::copy(public_path() . '/img-bkup/' . $img->image_path, public_path() . '/images/' . $img->image_path);
                // $fail = true;
            }  catch (\Exception $ex) {
                // $fail = false;
                $i++;
                error_log('Error on ' . $i);
                error_log('Error on ' . $img->image_path);
            }
        }

    }
}
