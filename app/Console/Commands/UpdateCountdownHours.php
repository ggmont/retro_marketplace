<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\AppOrgUserInventory;
use App\AppOrgOrder;
use App\AppOrgOrderDetail;
use Illuminate\Support\Facades\Mail;
use App\Mail\HalfwayNotification;
use App\Mail\BidWarningUserNotification;
use App\Mail\TimeOut;
use App\Mail\BidSuccessBuyerNotification;


class UpdateCountdownHours extends Command
{
    protected $signature = 'actualizar:contador'; // Define el nombre del comando
    protected $description = 'Actualizar el contador de tiempo restante';

    public function __construct()
    {
        parent::__construct();
    }



    public function handle()
    {
        // Obtener registros donde countdown_hours es mayor que 0
        $inventoriesToUpdate = AppOrgUserInventory::where('countdown_hours', '>', 0)->get();

        foreach ($inventoriesToUpdate as $inventory) {
            $inventory->decrement('countdown_hours');

            // Verificar si countdown_hours llegó a 12 y enviar notificación por correo
            if ($inventory->countdown_hours === 12) {
                Mail::to($inventory->user->email)->send(new HalfwayNotification($inventory));
            }

            // Verificar si countdown_hours llegó a 0 y actualizar auction_type
            if ($inventory->countdown_hours === 0) {

                // Verificar si hay ofertas (user_bid) y enviar notificación por correo
                if (!empty($inventory->user_bid)) {
                    $inventory->update([
                        'auction_type' => 2,
                        'offer_type' => 2,
                        'countdown_cancel' => 24
                    ]);
                    
                    Mail::to($inventory->user->email)->send(new BidWarningUserNotification($inventory));
                    Mail::to($inventory->userbid->email)->send(new BidSuccessBuyerNotification($inventory));
                    
                } else {
                    $inventory->update(['auction_type' => 2]);
                    $inventory->update(['offer_type' => 2]);
                    Mail::to($inventory->user->email)->send(new TimeOut($inventory));
                }
            }
        }

        $this->info('Contador actualizado correctamente.');
    }
}
