<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppOrgCategory extends Model {

  protected $table = 'appOrgCategories';

  protected $fillable = ['category_text','parent_id','name','hierarchy','is_enabled'];

  public function subCategories() {
    return $this->hasMany('App\AppOrgCategory', 'parent_id', 'id')->orderBy('order_by');
  }

  public function scopeWithAll($query) {
    $query->where('is_enabled', 'Y')->where('parent_id', 0)->with('subCategories');
  }

  public function getNewchildrenAttribute(){
    
    return AppOrgCategory::where('parent_id', $this->id)->count();
  }
}
