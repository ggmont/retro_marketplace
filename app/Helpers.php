<?php

if (!function_exists('__trans_choice')) {
    /**
     * Translates the given message based on a count from json key.
     *
     * @param $key
     * @param $number
     * @param array $replace
     * @param null $locale
     * @return string
     */
    function __trans_choice($key, $number, array $replace = [], $locale = null)
    {
        return trans_choice(__($key), $number, $replace, $locale);
    }
}

	function saveModel($currentId = 0, &$model, $modelName, &$subModelExists, &$input, &$colPrefix, &$new) {

		$new = true;
		$subModelExists = false;

		if($currentId > 0) {
			$model = $modelName::find($currentId);
			$new = false;
		}
		else {
			$model = new $modelName();
		}

		foreach($input as $key => $value) {
			
			if(strpos($key,$colPrefix) === 0) {
				if(str_replace($colPrefix,'',$key) == 'platform'){
					$dat = $value;
					$sysD = App\SysDictionary::whereIn('id', $value)->pluck('value')->toArray();
					$model[str_replace($colPrefix,'',$key)] = implode( ", ", $sysD );
				}else{
					$model[str_replace($colPrefix,'',$key)] = $value;
				}
			}

			else if (strpos($key,'add_') === 0) {
				$model[str_replace('add_','',$key)] = $value;
			}

			// if(strpos($key,'sub'..'_' . $colPrefix) === 0)
			// 	$subModelExists = true;
			if(preg_match("/^sub\d_".$colPrefix."/", $key)) {
				$subModelExists = true;
			}
			else {

			}

		}

		if($model->id > 0) {
			$model->modified_by = Auth::id();
		}
		else {
			$model->created_by = Auth::id();
		}

		$model->save();

		if(isset($dat)){
			$model->sysDictionaries()->sync($dat);
		}

		return $model->id;
	}

	/**
	 * Save a Sub Model
	 *
	 * param $new						boolean | if the record is new
	 * param $baseId				integer | id of the parent object
	 * param $baseName			string 	| name of the parent column in the child object
	 * param $baseModel			object 	| parent object
	 * param $subFunction		string 	| name of the function in the parent object to get child objects
	 * param $subName				string 	| name of the column to be save in the child object
	 * param $subModel			object 	| child object
	 * param $colPrefix			string 	| prefix for the input names
	 * param $input					array 	| array with the input and values
	 * param $subModelKey	  integer | key of the child object to be saved (when saving several childs)
	 * param $otherVals	  	array 	| array of the additional static columns to be saved
	 * param $otherInputs	  array 	| array of the additional dynamic form input to be saved
	 * param $imageTag	  	string 	| string for the image if there is a file name image_path
	 * param $imagePath	  	string 	| string for the folder of the image if there is a file name image_path
	 *
	 * @return string
	 */
	function saveSubModel($new, $baseId, $baseName, $baseModel, $subFunction, $subName, $subModel,
									 &$colPrefix, &$input, $subModelKey = '0', $otherVals = [], $otherInputs = [], $imageTag = '', $imagePath = '') {

		$subCol = isset($input['sub'.$subModelKey.'_' . $colPrefix . $subName]) ? $input['sub'.$subModelKey.'_' . $colPrefix . $subName] : array();
		$sub_ids = '';

		if(count($subCol) > 0) {

			if( ! $new) {
				$modelSubArray = $baseModel::find($baseId)->$subFunction->toArray();
			}

			foreach($subCol as $key => $value) {
				if(strlen($value) === 0)
					continue;
				if($new) {

					$sub_model = new $subModel();

					if($subName) {
						$sub_model[$subName] = $value;
					}

					$sub_model[$baseName] = $baseId;

					foreach($otherVals as $col => $val) {
						$sub_model[$col] = $val;
					}

					foreach($otherInputs as $col => $val) {
						$sub_model[$otherInputs[$col]] = isset($input['sub'.$subModelKey.'_' . $colPrefix . $otherInputs[$col]]) ?
																						 $input['sub'.$subModelKey.'_' . $colPrefix . $otherInputs[$col]][$key] : null;
					}

					$sub_model['created_by'] = Auth::id();

					$sub_model->save();

					$sub_ids .= (strlen($sub_ids)>0 ? ',' : '') . $sub_model->id;

				}
				else {
					$pos = array_search($value, array_column($modelSubArray, $subName));

					if($pos !== false) {

						$sub_model = $subModel::find($modelSubArray[$pos]['id']);
						$sub_model['modified_by'] = Auth::id();

						if($sub_model['deleted_at'] != null) {
							$sub_model['deleted_at'] = null;
							$sub_model->save();
						}

						$sub_ids .= (strlen($sub_ids)>0 ? ',' : '') . $sub_model->id;

					}
					else {

						$sub_model = new $subModel();
						$sub_model['created_by'] = Auth::id();

						if($subName) {
							$sub_model[$subName] = $value;
						}
						$sub_model[$baseName] = $baseId;

						foreach($otherVals as $col => $val) {
							$sub_model[$col] = $val;
						}

						foreach($otherInputs as $col => $val) {
							$sub_model[$otherInputs[$col]] = isset($input['sub'.$subModelKey.'_' . $colPrefix . $otherInputs[$col]]) ?
																							 $input['sub'.$subModelKey.'_' . $colPrefix . $otherInputs[$col]][$key] : null;
						}

						$sub_model->save();

						$sub_ids .= (strlen($sub_ids)>0 ? ',' : '') . $sub_model->id;

					}

				}

				// Save the image

				if(strpos($key,'image_path') !== false || $subName == 'image_path') {

					$image = $value;
					// echo 'image = ' . $image . '<br/>';
					// echo 'imageTag = ' . $imageTag;
					// die(0);
					if(strlen($image) > 0 && strpos($image, $imageTag) === false) {

						$arr = explode('/',$image);
						$dir = '';
						if(count($arr) > 1) {
							array_pop($arr);
							$dir = implode('/',$arr);
						}
						// dd($dir);
						$ext = File::extension(public_path() . '/uploads/' . $imagePath . '/' . $image);

						$newFileName = $imageTag . '_' . $baseId . '_' . $sub_model->id . '.' . $ext;

						File::move(public_path() . '/uploads/'  . $imagePath . '/' . $image,
						public_path() . '/uploads/'  . $imagePath . '/' . $dir . '/' . $newFileName);

						$sub_model->image_path = $dir . '/' . $newFileName;
						$sub_model->save();

					}
				}
			}
		}

		if( ! $new )
			$subModel::markOtherAsDeleted($baseId, explode(',',$sub_ids));

		return $sub_ids;
	}

	function saveSubModelNew($new, $baseId, $baseName, $baseModel, $subFunction, $subName, $subModel,
									 &$colPrefix, &$input, $subModelKey = '0', $otherVals = [], $otherInputs = [], $imageTag = '', $imagePath = '') {

		$subCol = isset($input['sub'.$subModelKey.'_' . $colPrefix . $subName]) ? $input['sub'.$subModelKey.'_' . $colPrefix . $subName] : array();
		$sub_ids = '';
				
		if(count($subCol) > 0) {

			if( ! $new) {
				$modelSubArray = $baseModel::find($baseId)->$subFunction->toArray();
			}

			foreach($subCol as $key => $value) {
				if(strlen($value) === 0)
					continue;

				if($new) {

					$sub_model = new $subModel();

					if($subName) {
						$sub_model[$subName] = $value;
					}

					$sub_model[$baseName] = $baseId;

					foreach($otherVals as $col => $val) {
						$sub_model[$col] = $val;
					}

					foreach($otherInputs as $col => $val) {
						$sub_model[$otherInputs[$col]] = isset($input['sub'.$subModelKey.'_' . $colPrefix . $otherInputs[$col]]) ?
																						 $input['sub'.$subModelKey.'_' . $colPrefix . $otherInputs[$col]][$key] : null;
					}

					$sub_model['created_by'] = Auth::id();

					$sub_model->save();

					$sub_ids .= (strlen($sub_ids)>0 ? ',' : '') . $sub_model->id;

				}
				else {

					$pos = array_search($value, array_column($modelSubArray, $subName));

					if($pos !== false) {

						$sub_model = $subModel::find($modelSubArray[$pos]['id']);
						$sub_model['modified_by'] = Auth::id();

						if($sub_model['deleted_at'] != null) {
							$sub_model['deleted_at'] = null;
							$sub_model->save();
						}

						$sub_ids .= (strlen($sub_ids)>0 ? ',' : '') . $sub_model->id;

					}
					else {

						$sub_model = new $subModel();
						$sub_model['created_by'] = Auth::id();

						if($subName) {
							$sub_model[$subName] = $value;
						}
						$sub_model[$baseName] = $baseId;

						foreach($otherVals as $col => $val) {
							$sub_model[$col] = $val;
						}

						foreach($otherInputs as $col => $val) {
							$sub_model[$otherInputs[$col]] = isset($input['sub'.$subModelKey.'_' . $colPrefix . $otherInputs[$col]]) ?
																							 $input['sub'.$subModelKey.'_' . $colPrefix . $otherInputs[$col]][$key] : null;
						}

						$sub_model->save();

						$sub_ids .= (strlen($sub_ids)>0 ? ',' : '') . $sub_model->id;

					}

				}

				// Save the image

				if(strpos($key,'image_path') !== false || $subName == 'image_path') {
					
					$image = $value;
					// echo 'image = ' . $image . '<br/>';
					// echo 'imageTag = ' . $imageTag;
					// die(0);
					if(strlen($image) > 0 && strpos($image,$imageTag) === false) {

						$arr = explode('/',$image);
						$dir = '';
						if(count($arr) > 1) {
							array_pop($arr);
							$dir = implode('/',$arr);
						}

						$ext = File::extension(public_path() . '/images/' . $imagePath . '/' . $image);
						
						$num_rand = rand(1000, 9999);
						//$newFileName = $imageTag . '_' . $baseId . '_' . $sub_model->id . '.' . $ext;
						$newFileName = $baseId . '_' . $num_rand . '.' . $ext;
						
						//here
						$mb = $baseModel::find($baseId)->first();

						$search = explode(",","ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,e,i,ø,u");
						$replace = explode(",","c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,e,i,o,u");
						$urlTitle = str_replace($search, $replace, $mb->categoryProd->name);
						$img = strtolower(str_replace(' ', '_', $urlTitle));
						File::move(public_path() . '/images/' . $image,
							public_path() . '/images/' . $img . '_'. $newFileName);
						

						$sub_model->image_path = $img . '_'. $newFileName;
						
						$sub_model->save();
						
		      }
				}


			}

		}

		if( ! $new )
			$subModel::markOtherAsDeleted($baseId, explode(',',$sub_ids));

		return $sub_ids;
	}

	function createMenu($currentItem, $organization_id = 0) {
		$returnArray = [];
		$returnArray[0] = '';
		$returnArray[1] = false;
		$returnHtml = '';

		$privileges = Session::get('user_privileges');

		if( ! is_array($currentItem)) {

			if(function_exists($currentItem)) {
				return $currentItem($organization_id);
			}
			else {
				return array('',false);
			}
		}

		foreach($currentItem as $key => $value) {

			if($value['type'] == 'ROOT') {

				if(isset($value['children']) && count($value['children']) > 0) {

						if(! is_array($value['children'])) {

							if(Auth::id() == 1)
								$organizations = DB::table('sysOrganizations')
								->select(['sysOrganizations.id','sysOrganizations.name','appOrganizations.company_name'])
								->leftJoin('appOrganizations','appOrganizations.sys_org_id','=','sysOrganizations.id')
								->whereNull('deleted_at')
								->get();
							else
								$organizations = DB::table('sysOrganizations')
								->select(['sysOrganizations.id','sysOrganizations.name','appOrganizations.company_name'])
								->leftJoin('appOrganizations','appOrganizations.sys_org_id','=','sysOrganizations.id')
								->where('user_id',Auth::id())
								->whereNull('deleted_at')
								->get();

							$mainShowActive = false;
							$subReturnHtml = '';
							foreach($organizations as $k => $v) {

								$retArray 	= createMenu($value['children'], $v->id);

								$retRootArr = menuRootItem($retArray[0],$retArray[1],strlen($v->company_name) > 0 ? $v->company_name : $v->name,$key, $value['icon']);

								$subReturnHtml .= $retRootArr[0];
								if($retRootArr[1]) {
									$mainShowActive = true;
									$returnArray[1] = $retRootArr[1];
								}


							}

							if(Auth::id() == 1) {
								$returnHtml .= '
									<li class="treeview '.($mainShowActive?'active menu-open':'').'">
										<a href="#">
										<i class="fa fa-industry"></i>
										<span>'.trans('app.stores').'</span>
										<i class="fa fa-angle-left pull-right"></i>
										</a>
										<ul class="treeview-menu " '.($mainShowActive?'style="display:block;"':'').'>
										'.$subReturnHtml.'
										</ul>
									 </li>';
							}
							else {
								$returnHtml .= $subReturnHtml;
							}

						}
						else {

							$retArray 	= createMenu($value['children'], null);

							$retRootArr = menuRootItem($retArray[0],$retArray[1],'',$key, $value['icon']);

							$returnHtml .= $retRootArr[0];

							if($retRootArr[1])
								$returnArray[1] = $retRootArr[1];

						}
				}
				else {
					$returnHtml .= '
						<li class="treeview ">
							<a href="#">
							<i class="fa fa-'.$value['icon'].'"></i>
							<span>'.trans('app.' . $key).'</span>
							<i class="fa fa-angle-left pull-right"></i>
							</a>';
					$returnHtml .= '</li>';
				}

			}
			else {

				if( ( ! isset($value['access']) || count($value['access']) == 0)
					 || (count($value['access']) > 0 && count(array_intersect($value['access'], $privileges)) > 0)) {

					$url = substr($value['url'],1,strlen($value['url']) - 1 );

					$showActive = Request::is($url) || Route::current()->getPrefix() == $url;

					$returnArray[1] = ! $returnArray[1]  ? ($showActive) : $returnArray[1];

					$returnHtml .= '
						<li class="treeview '.($showActive?'active':'').'">
							<a href="'.url($value['url']).'">
							<i class="fa fa-'.$value['icon'].'"></i>
							<span>'.trans('app.' . $key).'</span>
							</a>
						 </li>';
				}


			}

		}

		$returnArray[0] = $returnHtml;

		return $returnArray;
	}

	function menuRootItem($html, $active, $label, $key, $icon) {

		$return = [];
		$returnHtml = '';
		$returnActive = false;

		if(strlen($html) > 0) {

			if(strlen($label) == 0)
				$label = trans('app.' . $key);

			$returnHtml .= '
			<li class="treeview '.($active?'menu-open active':'').'">
				<a href="#">
				<i class="fa fa-'.$icon.'"></i>
				<span>'.$label.'</span>
				<i class="fa fa-angle-left pull-right"></i>
				</a>';
			$returnHtml .= '<ul class="treeview-menu " '.($active?'style="display:block;"':'').'>';
			$returnHtml .= $html;
			$returnHtml .= '</ul></li>';

			if($active)
				$returnActive = true;
		}
		$return[0] = $returnHtml;
		$return[1] = $returnActive;
		return $return;
	}

	function orgMenu($user_id) {

		$returnArray = [];
		$returnArray[0] = '';
		$returnArray[1] = false;
		$returnHtml = '';

		$customMenu = [
			'my_store' 					=> ['url'=>'/admin/my_store/'.$user_id.'/configuration', 'icon'=>'home', 'as' => 'my_store/configuration', 'access' => ['UMYCONF']],
			'my_messages' 			=> ['url'=>'/admin/my_store/'.$user_id.'/messages', 'icon'=>'comment', 'as' => 'my_store/messages', 'access' => ['CONFMESSAGE'] ],
			'my_inventory' 			=> ['url'=>'/admin/my_store/'.$user_id.'/inventory', 'icon'=>'list-ol', 'as' => 'my_store/inventory', 'access' => ['UMYINVE']],
			'my_orders' 				=> ['url'=>'/admin/my_store/'.$user_id.'/orders', 'icon'=>'check-square', 'as' => 'my_store/orders', 'access' => ['UMYORDR']],
			'my_invoices' 			=> ['url'=>'/admin/my_store/'.$user_id.'/invoices', 'icon'=>'file-text-o', 'as' => 'my_store/invoices', 'access' => ['UMYINVO'] ],
		];

		$privileges = Session::get('user_privileges');

		foreach($customMenu as $key => $value) {

			if(Auth::id() != 1 && isset($value['access']) && count(array_intersect($value['access'], $privileges)) == 0) {
				continue;
			}

			$url = substr($value['url'],1,strlen($value['url']) - 1 );

			$showActive = Request::is($url) ;

			$uriParts = explode('/',Request::path());

			$uriOrgId = isset($uriParts[1]) ? $uriParts[1] : 0;

			$showActive = ! $showActive ? Route::current()->getName() == $value['as'] && $uriOrgId == $user_id : $showActive ;

			$returnArray[1] = ! $returnArray[1]  ? ($showActive) : $returnArray[1];
			$returnHtml .= '
				<li class="treeview '.($showActive?'active':'').'">
					<a href="'.url($value['url']).'">
					<i class="fa fa-'.$value['icon'].'"></i>
					<span>'.trans('org.' . $key).'</span>
					</a>
				 </li>';
		}

		$returnArray[0] = $returnHtml;

		return $returnArray;
	}



	function getRandomToken($length) {

	    $token = "";
	    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
	    $codeAlphabet.= "0123456789";
	    $max = strlen($codeAlphabet); // edited

	    for ($i=0; $i < $length; $i++) {
	        $token .= $codeAlphabet[cryptoRandSecure(0, $max-1)];
	    }

	    return $token;
	}

	function cryptoRandSecure($min, $max) {
    $range = $max - $min;
    if ($range < 1) return $min; // not so random...
    $log = ceil(log($range, 2));
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd > $range);
    return $min + $rnd;
	}

	function menuGetChildren($row) {
		$html = '
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'.__($row->name).' <span class="caret"></span></a>
					<ul class="dropdown-menu">';

		foreach($row->subCategories as $subRow) {

			if($subRow->children == 0) {
				$html .= '<li ><a href="/search?cat='.$subRow->name.'&cati='.$subRow->id.'">'.__($subRow->name).'</a></li>';
			}
			else {
				$html .= menuGetChildren($subRow);
			}

		}

		$html .= '
					</ul>
			</li>';

		return $html;

	}

	function menuGetChildrenNew($row) {
		$html = '
			<li class="nk-drop-item">
				<a href="#" role="button" aria-haspopup="true" aria-expanded="false">'.__($row->name).' <i class="fa fa-chevron-down"></i></a>
					<ul class="dropdown">';

						foreach($row->subCategories as $subRow) {
							if($subRow->newchildren == 0) {
								$html .= '<li ><a href="/search?cat=' . $row->name . '&scat='.$subRow->name.'">'.__($subRow->name).'</a></li>';
							}
							else {
								// dd(menuGetChildren($subRow));
								$html .= menuGetChildrenSubNew($subRow, $row->name);
							}

						}

		$html .= '
					</ul>
			</li>';

		return $html;

	}

	function menuGetChildrenSubNew($row, $name) {
		$html = '
			<li class="nk-drop-item">
				<a href="#" role="button" aria-haspopup="true" aria-expanded="false">'.__($row->name).' <i class="fa fa-chevron-down"></i></a>
					<ul class="dropdown">';

						foreach($row->subCategories as $subRow) {
							if($subRow->newchildren == 0) {
								$html .= '<li ><a href="/search?cat=' . $name . '&scat='.$row->name.'&plt='.$subRow->name.'">'.__($subRow->name).'</a></li>';
							}
							// else {
							// 	// dd(menuGetChildren($subRow));
							// 	$html .= menuGetChildrenNew($subRow);
							// }

						}

		$html .= '
					</ul>
			</li>';

		return $html;

	}

	function searchGetChildren($row, $categoryIds = []) {

		$html = '<ul class="list-unstyled">';
		$isSelected = false;
		
		foreach($row->subCategories as $subRow) {
			
			$checked = in_array($subRow->id, $categoryIds) ? true : false;
			if($checked && !$isSelected) $isSelected = true;
			$html .= '<li><label data-cat-name="'.__($subRow->name).'" data-cat-id="'.$subRow->id.'"> <input type="checkbox" '.($checked ? 'CHECKED' : '') .' /> '.__($subRow->name) .'</label></li>';
		}

		$html .= '</ul>';

		return '<div class="br-search-category title '.($isSelected ? 'br-show-children' : '').'" >'.__($row->name). $html . '</div>';
	}

	function searchGetChildrenNew($row, $categoryIds = []) {
		/*
		<div id="accordion-1-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accordion-1-1-heading" style="">
			<p></p>
			<p></p>
		</div>
		*/
		$html = '<div id="accordion-'.__($row->name).'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accordion-'.$row->name.'-heading" style="">';
		$isSelected = false;
		
		foreach($row->subCategories as $subRow) {
			
			$checked = in_array($subRow->id, $categoryIds) ? true : false;
			if($checked && !$isSelected) $isSelected = true;
			$html .= '<li style="list-style-type: none;"><label data-cat-name="'.__($subRow->name).'" data-cat-id="'.$subRow->id.'"> <input data-type="'.__($row->name).'" type="checkbox" '.($checked ? 'CHECKED' : '') .' /> '.__($subRow->name) .'</label></li>';
		}

		$html .= '</div>';

		/*
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="accordion-1-1-heading">
				<a class="collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#accordion-1-1" aria-expanded="false" aria-controls="accordion-1-1">
					Collapsible 1 <span class="panel-heading-arrow fa fa-angle-down"></span> 
				</a>
			</div>
			
			<div id="accordion-1-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accordion-1-1-heading" style="">
			</div>
		</div>
		*/

		return '<div class="panel panel-default">'.
		'<div class="panel-heading" role="tab" id="accordion-'.__($row->name).'-heading">
			<a class="collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#accordion-'.__($row->name).'" aria-expanded="false" aria-controls="accordion-'.__($row->name).'">
			'.__($row->name).' <span class="panel-heading-arrow fa fa-angle-down"></span> 
			</a>
		</div>'.
		$html 
		. '</div>';
		//return '<div class="br-search-category title '.($isSelected ? 'br-show-children' : '').'" >'.__($row->name). $html . '</div>';
	}
