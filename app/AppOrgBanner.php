<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppOrgBanner extends Model {
  use SoftDeletes;
  protected $table = 'appOrgBanners';

  protected $fillable = ['name','image_path','is_enabled','created_by','category','link','lang'];
}
