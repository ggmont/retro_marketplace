<?php

namespace App\Http\Controllers;
 

use Illuminate\Pagination\LengthAwarePaginator;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\SysUserPasswordReset;
use App\Events\ActionExecuted;
use App\Mail\PasswordReset;
use App\ProductRequestImage;
use Log;
use App\AppOrgCategory;
use App\AppOrgFooterLink;
use App\AppOrgSocialNetwork;
use App\AppOrgCarousel;
use App\AppOrgNewsItem;
use App\AppOrgBanner;
use App\AppOrgProduct;
use App\AppOrgProductImage;
use App\AppOrgUserInventory;
use App\AppOrgOrder;
use App\AppOrgOrderCancel;
use App\AppOrgOrderDetail;
use App\AppOrgOrderPayment;
use App\AppOrgCartDetailUser;
use App\AppOrgUserEvent;
Use App\AppOrgCartShipping;
use App\AppMessage;
use App\SysSettings;
use App\AppOrgProductFeedback;

use App\SysUser;
use App\ProductRequest;
use App\SysUserRoles;
use App\SysCountry;
use App\SysAccountBank;
use App\SysShipping;
use App\AppOrgOrderShipping;
use App\AppOrgOrderNotReceived;
use App\AppOrgCartUser;
use App\AppOrgUserRating;
use App\SysUserReferred;

use Illuminate\Http\Request as BaseRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\SysDictionary;
use App\SysUserAccountActivation;
use App\Mail\ActivateAccount;
use App\Mail\UserRecovery;
use App\Mail\OrderWithoutPay;
use App\Mail\OrderWithoutPayBuyer;
use App\Mail\OrderPaymentComplete;
use App\Mail\OrderPaymentBuyer;
use App\Mail\CreditAccount;
use App\Mail\ContactForm;
use App\Helpers\PtyCommons;

use Mpdf\Mpdf;

use App;
use App\AppOrgCarouselItem;
use App\AppOrgPage;
use App\SysUserSocial;
use Auth;
use Carbon\Carbon;
use Config;
use Cookie;
use Datatables;
use DB;
use Hash;
use File;
use Mail;
use Redirect;
use Request;
use Response;
use Route;
use Session;
use Validator;
use Image;
use Intervention\Image\Facades\Storagsse;
use Illuminate\Support\Facades\Http;


use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Payment;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Api\ItemList;

class FrontController extends Controller {

  /*
	|--------------------------------------------------------------------------
	| Front Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "Public pages" for the application and
	| is configured to allow anonymous and registered users.
	|
	*/


  private $viewData = [];
  private $user_checkout_incomplete = '';
  /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {   

    $payPalConfig = Config::get('paypal');
    //dd($payPalConfig);

    $this->apiContext = new ApiContext(
        new OAuthTokenCredential(
            $payPalConfig['client_id'],
            $payPalConfig['secret']
        )
    );

    $this->apiContext->setConfig($payPalConfig['settings']);

    //dd($payPalConfig);

    $this->viewData['col_prefix'] = Config::get('brcode.column_prefix');
    // Product's Category
    $this->viewData['categories'] = AppOrgCategory::withAll()
      // ->where('is_enabled','Y')
      // ->orderBy(DB::raw('(case when parent_id = 0 THEN concat(id,"-",parent_id) ELSE concat(parent_id,"-",id) END)'))
    ->get();

    $locale = App::getLocale();

    $this->viewData['page_information'] = AppOrgPage::where('is_enabled','Y')->where('type','informacion')->get();
    $this->viewData['page_guide'] = AppOrgPage::where('is_enabled','Y')->where('type','guia')->get();
    $this->viewData['translations'] = File::exists(base_path() . '/resources/lang/' . $locale . '_ang.json') ? File::get(base_path() . '/resources/lang/' . $locale . '_ang.json') : '{}';
    $this->viewData['footer_links'] = AppOrgFooterLink::orderBy('column')->get();
    $this->viewData['footer_pages'] = App\AppOrgPage::all();
    $this->viewData['footer_faqs'] = App\AppOrgFaq::all();
    $this->viewData['footer_cols'] = SysDictionary::whereIn('code', ['FOOTER_COL'])->get()->toArray();
    $this->viewData['header_newsletter'] = App\AppOrgNewsItem::orderBy('created_at', 'DESC')->take(5)->get();
  }
  
  function getUserIP() {
    if( array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')>0) {
            $addr = explode(",",$_SERVER['HTTP_X_FORWARDED_FOR']);
            return trim($addr[0]);
        } else {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
    }
    else {
        return $_SERVER['REMOTE_ADDR'];
    }
  }
  


  public function showPasswordReset(){
    $this->loadCart();
    return view('brcode.front.public.recovery')->with('viewData', $this->viewData);
  }

  public function showUserReset(){
    $this->loadCart();
    return view('brcode.front.public.recovery_user')->with('viewData', $this->viewData);
  }

  public function userReset() {
		$input = Request::all();

		$rules = [
			'email' 	=> 'required|email',
		];

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {
			$email = $input['email'];
			$user = SysUser::where('email',$email)->first();
      //dd($user);

			if($user) {

				if($user->is_activated == 'N') {
          return redirect('/show_user_reset')->with([
            'flash_class'   => 'alert-warning',
            'flash_message' => 'Su cuenta se encuentra desactivada.',
          ]);
        }

				Mail::to($user->email)->queue(new UserRecovery($user->user_name));

        return redirect('/show_user_reset')->with([
          'flash_class'   => 'alert-success',
          'flash_message' => 'Se a enviado a su correo el nombre de usuario que a olvidado.',
        ]);

			}
			else {

				event(new ActionExecuted('events.user_password_reset_unknown',null,null, Request::ip(),array('user_name'=> $email )));

        return redirect('/show_user_reset')->with([
          'flash_class'   => 'alert-danger',
          'flash_message' => 'Su correo no se encuentra en nuestra base de datos.',
        ]);

			}

		}

  }

  public function show_resend_activation(){
    $this->loadCart();
    return view('brcode.front.public.recovery_activation')->with('viewData', $this->viewData);
  }

  public function resendActivation() {
		$input = Request::all();

		$rules = [
			'email' 	=> 'required|email',
		];

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {
			$email = $input['email'];
			$user = SysUser::where('email',$email)->first();
      //dd($user);

			if($user) {

				if($user->is_activated == 'N') {
					event(new ActionExecuted('events.user_pwd_r',null,null, Request::ip(),array('user_name'=> $user->email )));
          $actRequest = SysUserAccountActivation::where('email',$email)->first();
  
          if( ! $actRequest) {
            $actRequest = new SysUserPasswordReset();
          }

          $actRequest->email = $email;
          $actRequest->token = str_random(100);
          $actRequest->save();

          Mail::to($actRequest->email)->queue(new ActivateAccount($actRequest->token, $user->first_name . ' ' . $user->last_name));
          return redirect('/show_resend_activation')->with([
            'flash_class'   => 'alert-success',
            'flash_message' => 'Se a reenviado su correo de activacion correctamente.',
          ]);
        } else {
          return redirect('/show_resend_activation')->with([
            'flash_class'   => 'alert-warning',
            'flash_message' => 'Su cuenta ya se encuentra activada.',
          ]);
        }

			}
			else {

				event(new ActionExecuted('events.user_password_reset_unknown',null,null, Request::ip(),array('user_name'=> $email )));

        return redirect('/show_resend_activation')->with([
          'flash_class'   => 'alert-danger',
          'flash_message' => 'Su correo no se encuentra en nuestra base de datos.',
        ]);

			}

		}

  }

  public function showCreateNewPassword($token){
    $this->loadCart();

    if( ! Config::get('brcode.app_allow_pass_recover')) {
			abort(404);
		}

		$passReset = SysUserPasswordReset::where('token',$token)->first();

		$this->viewData['token'] = '';

		if($passReset) {
			$this->viewData['token'] = $passReset->token;
		}
    return view('brcode.front.public.create_new_password')->with('viewData', $this->viewData);
  }

  public function passwordReset() {
		$input = Request::all();

		$rules = [
			'email' 	=> 'required|email',
		];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {
			$email = $input['email'];
			$user = SysUser::where('email',$email)->first();

			if($user) {

				if($user->is_activated == 'N') {
					event(new ActionExecuted('events.user_pwd_r',null,null, Request::ip(),array('user_name'=> $user->email )));

          return redirect('/show_password_reset')->with([
            'flash_class'   => 'alert-warning',
            'flash_message' => 'Su cuenta se encuentra desactivada.',
          ]);
        }

				$passReset = SysUserPasswordReset::where('email',$email)->first();

				if( ! $passReset) {
					$passReset = new SysUserPasswordReset();
				}

				$passReset->email = $email;
				$passReset->token = str_random(100);
				$passReset->save();

				Mail::to($user->email)->queue(new PasswordReset($passReset->token, $user->first_name . ' ' . $user->last_name));

				event(new ActionExecuted('events.user_password_reset_snt',null,null, Request::ip(),array('user_name'=> $email )));

        return redirect('/show_password_reset')->with([
					'flash_class'   => 'alert-success',
					'flash_message' => 'Su correo de restauracion de contraseña se a enviado correctamente.',
				  ]);

			}
			else {

				event(new ActionExecuted('events.user_password_reset_unknown',null,null, Request::ip(),array('user_name'=> $email )));

				return redirect('/show_password_reset')->with([
					'flash_class'   => 'alert-danger',
					'flash_message' => 'Su correo no se encuentra en nuestra base de datos.',
				  ]);

			}

		}

  }

  /**
	 * Save the new password of the user
	 *
	 * @return redirect(back if fails | login if success)
	 */
	public function createNewPassword() {
		$input = Request::all();

		$rules = [
			'email' 	=> 'required|email',
			'password' 		=> 'required|min:8|max:15|confirmed',
		];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {

			$token = $input['_resetToken'];
			$email = $input['email'];

			$passReset = SysUserPasswordReset::where('token',$token)->where('email', $email)->first();

			if($passReset) {
				$user = SysUser::where('email',$passReset->email)->first();

				if($user) {
					$user->password = Hash::make($input['password']);
					$user->save();

					$passReset->delete();

					event(new ActionExecuted('events.user_password_reset_snt',null,null, Request::ip(),array('user_name'=> $passReset->email )));

          return redirect('/')->with('message_confirm', trans('login.pwd_changed_successfully'))->with('type_msg','success');
				}
				else {
					return Redirect::back()
            ->withErrors($v->errors())
            ->with('message_confirm', trans('app.error_unknown'))->with('type_msg','warning');
				}

			}
			else {

				return Redirect::back()
          ->withErrors($v->errors())
          ->with('message_confirm', trans('login.email_pwd_reset_unrelated'))->with('type_msg','warning');
			}
		}
	}
  
  private function loadCart() {

    $cart   = [];
    $ship   = [];

    $hasta = date('Y-m-d 23:59:59');
    $desde = date('Y-m-d 00:00:00', strtotime($hasta."- 15 days"));

    $from = date('Y-m-d 23:59:59');
    $to = date('Y-m-d 00:00:00', strtotime($hasta."- 3 days"));

    //// error_log($user_ip);

    $userId = Auth::id();
    $items_total = 0;
    $locale = App::getLocale();
    $k = 0;
    $r = 0;

    if(! Cookie::get('LG')){
      Cookie::queue('LG', 'es', 2628000);
      session(['locale' => Cookie::get('LG')]);
    }else{
      if(! session('locale')){
        session(['locale' => Cookie::get('LG')]);
      }else if(session('locale') != Cookie::get('LG')){
        session(['locale' => Cookie::get('LG')]);
      }
    }

    if(Auth::user()){
      foreach(Auth::user()->cart->details as $detail){
        $items_total += $detail->quantity;
      }
    }
    
    $this->viewData['translations'] = File::exists(base_path() . '/resources/lang/' . $locale . '_ang.json') ? File::get(base_path() . '/resources/lang/' . $locale . '_ang.json') : '{}';
    $this->viewData['header_pay_pending'] = AppOrgOrder::where('buyer_user_id', Auth::id())->where('status', 'RP')->whereBetween('updated_at',[$desde, $hasta])->get();
    $this->viewData['header_send_pending'] = AppOrgOrder::where('seller_user_id', Auth::id())->where('status', 'CR')->whereBetween('updated_at',[$desde, $hasta])->get();

    $this->viewData['product_request'] = AppOrgProduct::where('request_by', Auth::id())->whereBetween('created_at',[$from, $to])->get();
  
    $this->viewData['header_notifications'] = AppOrgOrder::
    where(function ($query) use ($userId) {
      $query->where('buyer_user_id', $userId)->where(function ($query2){
        $query2->where('status', 'ST')->orWhere('status', 'RP');
      });
    })->orWhere(function ($query) use ($userId) {
      $query->where('seller_user_id', $userId)->where(function ($query2){
        $query2->where('status', 'DD')->orWhere('status', 'RP')->orWhere('status', 'CR')->orWhere('status', 'CW')->orWhere('status', 'PC');
      });
    })
    ->whereBetween('updated_at',[$desde, $hasta])
    ->orderBy('updated_at', 'desc')->get();
    
    foreach($this->viewData['header_notifications'] as $key){
      $k += $key->seller_user_id == Auth::id() ? ( $key->seller_read == 'N' ? 1 : 0 ) : ( $key->buyer_read == 'N' ? 1 : 0 );
    }

   $this->viewData['product_request'] = ProductRequest::where('user_id',Auth::id())->where('status','A')->where('user_read','N')->get();

    foreach($this->viewData['product_request'] as $key){
      $r += $key->user_id == Auth::id() ? ( $key->user_read == 'N' ? 1 : 0 ) : ( $key->user_read == 'N' ? 1 : 0 );
    }
    
    if(Auth::user()){
      $this->viewData['header_messages'] = AppMessage::where('alias_to_id', Auth::user()->id)->where('read', false)->get()->unique('alias_from_id');
    }
    
    $this->viewData['c_h_n']           = $k;
    $this->viewData['c_r_n']           = $r;
    $this->viewData['cart']            = $cart;
    $this->viewData['ship']            = $ship;
    $this->viewData['products_count']  = count($cart);
    $this->viewData['items_count']     = $items_total;
    $this->viewData['countries']       = SysCountry::all();

  }

  /*
	|--------------------------------------------------------------------------
	| Home and other public url addresses
	|--------------------------------------------------------------------------
	|
	*/

    /*
	|--------------------------------------------------------------------------
	| Front Controller 2019
	|--------------------------------------------------------------------------
  |
  */


  public function showProfile(){
    $this->loadCart();
    $social = count(SysUserSocial::where('user_id',  Auth::id())->get());
    $social_user =  SysUserSocial::where('user_id',  Auth::id())->get();
    return view('brcode.front.private.profile')->with('viewData', $this->viewData)->with('social',$social)->with('social_user',$social_user);
  }
  
  public function showSales(){
    $this->loadCart();
    $this->viewData['unpaid'] = AppOrgOrder::where('seller_user_id', Auth::id())->whereIn('status', ['RP', 'CW'])->orderBy('id','desc')->get(); //Unpaid
    $this->viewData['paid'] = AppOrgOrder::where('seller_user_id', Auth::id())->whereIn('status', ['CR', 'PC'])->orderBy('id','desc')->get();  //Paid
    $this->viewData['sent'] = AppOrgOrder::where('seller_user_id', Auth::id())->where('status', 'ST')->orderBy('id','desc')->get(); // sent
    $this->viewData['arrived'] = AppOrgOrder::where('seller_user_id', Auth::id())->where('status', 'DD')->orderBy('id','desc')->get(); // arrived
    $this->viewData['notArrived'] = AppOrgOrder::where('seller_user_id', Auth::id())->where('status', 'ON')->orderBy('id','desc')->get(); // Not arrived
    $this->viewData['cancelled'] = AppOrgOrder::where('seller_user_id', Auth::id())->where('status', 'CN')->orderBy('id','desc')->get(); //cancelled
    
    return view('brcode.front.private.sales')->with('viewData', $this->viewData);
  }

  public function showPurchases(){
    $this->loadCart();
    
    $this->viewData['unpaid'] = AppOrgOrder::where('buyer_user_id', Auth::id())->whereIn('status', ['RP', 'CW'])->orderBy('id','desc')->get(); //Unpaid
    $this->viewData['paid'] = AppOrgOrder::where('buyer_user_id', Auth::id())->whereIn('status', ['CR', 'PC'])->orderBy('id','desc')->get();  //Paid
    $this->viewData['sent'] = AppOrgOrder::where('buyer_user_id', Auth::id())->where('status', 'ST')->orderBy('id','desc')->get(); // sent
    $this->viewData['arrived'] = AppOrgOrder::where('buyer_user_id', Auth::id())->where('status', 'DD')->orderBy('id','desc')->get(); // arrived
    $this->viewData['notArrived'] = AppOrgOrder::where('buyer_user_id', Auth::id())->where('status', 'ON')->orderBy('id','desc')->get(); // Not arrived
    $this->viewData['cancelled'] = AppOrgOrder::where('buyer_user_id', Auth::id())->where('status', 'CN')->orderBy('id','desc')->get(); //cancelled
    return view('brcode.front.private.purchases')->with('viewData', $this->viewData);
  }
  
  public function showInventory(){
    $this->loadCart();
    $this->viewData['inventories']  = AppOrgUserInventory::where('user_id', Auth::id())->where('quantity', '>', 0)->whereNull('deleted_at')->orderBy('id','desc')->get();
    $this->viewData['platform'] = SysDictionary::where('code', 'GAME_PLATFORM')->get(['value_id', 'value', 'parent_id']);
    return view('brcode.front.private.inventory')->with('viewData', $this->viewData);
  }

  public function ProductRequestIndex()
  {
     if(!Auth::user()){
      $this->loadCart();
      return view('brcode.front.public.user_list_register')->with('viewData',$this->viewData);
      }

      $this->loadCart();
      $categories = AppOrgCategory::all();
      $genero = SysDictionary::where("code", "GAME_CATEGORY")->get();
      $location = SysDictionary::where("code", "GAME_LOCATION")->get();
      $generation = SysDictionary::where("code", "GAME_GENERATION")->get();
      $language = SysDictionary::where("code", "GAME_LANGUAGE")->get();
      $media = SysDictionary::where("code", "GAME_SUPPORT")->get();
      $platform = SysDictionary::where("code", "GAME_PLATFORM")->get();
      $region = SysDictionary::where("code", "GAME_REGION")->get();
      
      return view('brcode.front.public.product_request', compact(
      'categories',
      'genero',
      'location',
      'generation',
      'language',
      'media',
      'platform',
      'region'
      ))->with('viewData', $this->viewData);
  }

  public function ProductRequestSendToAdmin(BaseRequest $request)
  {

    $input = Request::all();


     if ($request->hasFile('image_path')) {
      $data   =   ProductRequest::create([
          'category' => $request->category,
          'user_id' => \Auth::user()->id,
          'email' => \Auth::user()->email,
          'product' => $request->product,
          'platform' => $request->platform,
          'region' => $request->region,
          'quantity' => $request->quantity,
          'price' => $request->price,
          'comments' => $request->comments,
          'box_condition' => $request->box_condition,
          'manual_condition' => $request->manual_condition,
          'cover_condition' => $request->cover_condition,
          'game_condition' => $request->game_condition,
          'extra_condition' => $request->extra_condition,
          'inside_condition' => $request->inside_condition,
          'status' => 'P',
          ]);

          if ($data->save()) {


                  foreach ($request->file('image_path') as $image) {
                    if ($image->isValid()) {
        
                        $img = new ProductRequestImage();
                        $image_name = 'request' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $image->getClientOriginalExtension();
                        $destinationPath = public_path() . '/uploads/inventory-images/';
                        $dateFolder = date('Y') . '/' . date('m') . '/' . date('d') . '/';
                        $directory = $destinationPath . $dateFolder;
                        if ($directory)
                        {
                             @mkdir($directory);
                        }
                        $imgx = Image::make($image->getRealPath());
                        $imgx->resize(600, 750, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($directory.'/'.$image_name);
                        $upload = $dateFolder . $image_name;
                        $img->inventory_id = $data->id;
                        $img->image_path = $upload;
                        $img->created_by = \Auth::user()->id;
                        $img->save();
                    }
                }


                return redirect("/ProductRequest")->with([
                    'flash_message' => 'Tu solicitud esta pendiente de revision ,
                    en lo que se apruebe tu inventario estara actualizado (Recibiras una Notificacion de ello).',
                    'flash_class'   => 'alert-success',
                ]);

          } else {

              return back()->with([
                  'flash_message'   => 'Ha ocurrido un error.',
                  'flash_class'     => 'alert-danger',
                  'flash_important' => true,
              ]);

          }
      } else {
      $data   =   ProductRequest::create([
        'category' => $request->category,
        'user_id' => \Auth::user()->id,
        'email' => \Auth::user()->email,
        'product' => $request->product,
        'platform' => $request->platform,
        'region' => $request->region,
        'quantity' => $request->quantity,
        'price' => $request->price,
        'comments' => $request->comments,
        'box_condition' => $request->box_condition,
        'manual_condition' => $request->manual_condition,
        'cover_condition' => $request->cover_condition,
        'game_condition' => $request->game_condition,
        'extra_condition' => $request->extra_condition,
        'inside_condition' => $request->inside_condition,
        'status' => 'P',
          ]);

          if ($data->save()) {

            return redirect("/ProductRequest")->with([
              'flash_message' => 'Tu solicitud esta pendiente de revision ,
              en lo que se apruebe tu inventario estara actualizado (Recibiras una Notificacion de ello).',
              'flash_class'   => 'alert-success',
          ]);

          } else {

              return back()->with([
                  'flash_message'   => 'Ha ocurrido un error.',
                  'flash_class'     => 'alert-danger',
                  'flash_important' => true,
              ]);

          }
      }
  }

  public function showMessages(){
    $this->loadCart();
    return view('brcode.front.private.messages')->with('viewData', $this->viewData)->with('user_m', SysUser::where('id', Auth::id())->select('id', 'user_name', 'last_name', 'first_name')->first());
  }

  public function getUsersConversation() {
    if(Request::ajax()) {
      $values = Request::input();
      return SysUser::select('user_name', 'profile_picture')->where('user_name', 'LIKE', '%'.$values['name'].'%')->whereNotIn('id', [1, 2, Auth::id()])->get();
    }
    /*
		if(Request::ajax()) {
      $data = AppOrgUserInventory::where('user_id', Auth::id())->orderBy('id','desc')->get();
      //i.id','p.name','i.created_at','i.quantity','i.quantity_sold'
      $datos = [];
      foreach($data as $key){
				$mapArray = (object) array(
					"d0" => '<a href="' . url('/account/inventory/modify/' . ($key->id + 1000) ).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>',
					"d1" => $key->product->name,
					"d2" => $key->quantity,
					"d3" => $key->quantity_sold,
					"d4" => date('Y-m-d H:i:s', strtotime($key->created_at)),
				);
				array_push($datos, $mapArray);
			}
			return response()->json([
				'data' => $datos, 
			]);
		}*/
  }


  /**
	 * Show the view for the home page of the application
	 *
	 * @return view
	 */


  public function showHome() {
    $this->loadCart();
    $filter_user = SysUserRoles::where('role_id', '!=', 1)->pluck('user_id');
    $usuario = SysUser::wherein('id', $filter_user)->groupBy('id')->orderBy('id','asc')->get();

    /*
    $rating = AppOrgUserRating::where('score', '<', 0.5)->where('processig', '>', 0)->where('seller_user_id', $usuario->id)->orderBy('created_at','desc')->get();
    */
    $this->viewData['usuario'] = $usuario;
    // Load Image Carousel - Cargar Imagenes del Carrusel
    $this->viewData['main_carousel'] = AppOrgCarousel::where('location','CRS_MAIN')->first();
    $this->viewData['main_carousel_items'] = AppOrgCarouselItem::where('is_enabled','Y')->get();

    //Imagenes Secundarias
    $this->viewData['main_carousel_secundary_one'] = AppOrgCarousel::where('location','CRS_MAIN_ONE')->first();
    $this->viewData['main_carousel_items_secundary_one'] = $this->viewData['main_carousel_secundary_one']->images->toArray();
    $this->viewData['main_carousel_secundary_two'] = AppOrgCarousel::where('location','CRS_MAIN_TWO')->first();
    $this->viewData['main_carousel_items_secundary_two'] = $this->viewData['main_carousel_secundary_two']->images->toArray();

    //$main_carousel_items_secundary_two = $this->viewData['main_carousel_secundary_two']->images->toArray();
    //dd($main_carousel_items_secundary_two);

    // Load banners
    $this->viewData['main_banner_1'] = AppOrgBanner::where('location','BNR_MAIN_1')->where('is_enabled','Y')->get();
    $this->viewData['main_banner_2'] = AppOrgBanner::where('location','BNR_MAIN_2')->where('is_enabled','Y')->get();

    

    // Load Image Banner (Left)
    //$images = AppOrgProductImage::where('image_path')->first();
    // Load New Products
    $sites = AppOrgBanner::where('location','BNR_MAIN_3')->where('is_enabled','Y')->get();
    //dd($sites);
    $magazine = AppOrgBanner::where('location','IMAGE_MAIN_MAGAZINE')->where('is_enabled','Y')->get();
    $inventory_latest_product  = AppOrgProduct::whereNull('deleted_at')->orderBy('created_at','desc')->take(10)->get();
    //dd($inventory_latest_product);
    $imgs = AppOrgProductImage::groupBy('product_id')->pluck('product_id');
    
    $this->viewData['inventory_random_product']  = AppOrgProduct::whereIn('id', $imgs)
    ->whereNull('deleted_at')  
    ->inRandomOrder()
    ->take(10)
    ->get();

    $inventory_random_product_sale  = AppOrgUserInventory::where('quantity', '>', 0)
    ->whereNull('deleted_at')  
    ->inRandomOrder()
    ->take(10)
    ->get();

    //dd($inventory_random_product_sale);
    
    $this->viewData['inventory_latest_admin']  = AppOrgUserInventory::whereNull('deleted_at')->where('user_id', 2)->orderBy('id','desc')->take(10)->get();
    $inventory_latest_users  = AppOrgUserInventory::where('quantity', '>', 0)->whereNull('deleted_at')->orderBy('id','desc')->take(10)->get();
    //dd($inventory_latest_users);
    
    //CONSULTAS DE EL INVENTARIO PARA JUEGOS RETRO 
    //NES
    $inventory_retro_nes  = AppOrgUserInventory::where('quantity', '>', 0)->whereNull('deleted_at')
    ->whereHas('product', function ($q) {
      $q->where('platform', 'NES');
     })
    ->inRandomOrder()
    ->take(10)
    ->get();
    //SNES
    $inventory_retro_snes  = AppOrgUserInventory::where('quantity', '>', 0)->whereNull('deleted_at')
        ->whereHas('product', function ($q) {
          $q->where('platform', 'Super Nintendo');
         })
        ->inRandomOrder()
        ->take(10)
        ->get();
    //SEGA MEGADRIVE
    $inventory_retro_sega_megadrive  = AppOrgUserInventory::where('quantity', '>', 0)->whereNull('deleted_at')
        ->whereHas('product', function ($q) {
          $q->where('platform', 'Sega Mega Drive');
         })
        ->inRandomOrder()
        ->take(10)
        ->get();
    //DREAMCAST
    $inventory_retro_dreamcast  = AppOrgUserInventory::where('quantity', '>', 0)->whereNull('deleted_at')
    ->whereHas('product', function ($q) {
      $q->where('platform', 'Game Gear');
     })
    ->inRandomOrder()
    ->take(10)
    ->get();
    //FIN CONSULTAS RETRO 
    //CONSULTAS DE EL INVENTARIO PARA JUEGOS RETRO 
    //PS4
    $inventory_new_ps4  = AppOrgUserInventory::where('quantity', '>', 0)->whereNull('deleted_at')
    ->whereHas('product', function ($q) {
      $q->where('platform', 'PlayStation 4');
      $q->whereHas('images', function ($q) {
        $q->where('image_path','>', 0);
    });
     })
    ->inRandomOrder()
    ->take(10)
    ->get();
    //XBOX ONE
   $inventory_new_xbox  = AppOrgUserInventory::where('quantity', '>', 0)->whereNull('deleted_at')
        ->whereHas('product', function ($q) {
          $q->where('platform', 'Xbox One');
         })
        ->orderBy('id','desc')
        ->inRandomOrder()
        ->get();
      //XBOX ONE
    $inventory_new_switch  = AppOrgUserInventory::where('quantity', '>', 0)->whereNull('deleted_at')
            ->whereHas('product', function ($q) {
              $q->where('platform', 'Nintendo DS');
             })
            ->orderBy('id','desc')
            ->inRandomOrder()
            ->get();
    //dd($prueba);

    // Load Most sold
    $inventory_most_sold    = AppOrgOrderDetail::OfMostSold(11)->get();

    //Best Seller - Mejores Vendedores
 

    return view('brcode.front.public.home', compact('inventory_most_sold',
    'inventory_latest_product',
    'inventory_latest_users',
    'inventory_retro_nes',
    'inventory_retro_snes',
    'inventory_retro_sega_megadrive',
    'inventory_retro_dreamcast',
    'inventory_new_ps4',
    'inventory_new_xbox',
    'inventory_new_switch',
    'inventory_random_product_sale',
    'sites',
    'magazine'
    ))->with('viewData',$this->viewData);
  }

  public function ajaxUser(){
    $params = Request::input();
    $name = $params['term']['term'];
    return SysUser::where('user_name', 'like', '%' . $name . '%')->take(10)->get();
  }

  public function ajaxProduct(){
    $params = Request::input();
    $name = $params['term']['term'];
    return AppOrgProduct::where('name', 'like', '%' . $name . '%')->take(10)->get([ DB::raw('id + 1000 as id'), 'name', 'platform', 'region']);
  }

  public function getData(BaseRequest $request){

    // $bajo = AppOrgOrderDetail::where('appOrgUserInventories.product_id', $product)
    // ->whereIn('appOrgUserInventories.game_condition', ['USED-VERY', 'NOT-WORK'])
    // ->whereBetween('appOrgOrders.updated_at', [$desde, $hasta])
    // ->leftJoin('appOrgUserInventories', 'appOrgUserInventories.id', '=', 'appOrgOrderDetails.inventory_id')
    // ->leftJoin('appOrgProducts', 'appOrgOrderDetails.product_id', '=', 'appOrgProducts.id')
    // ->leftJoin('appOrgOrders', 'appOrgOrderDetails.order_id', '=', 'appOrgOrders.id')
    // ->orderBy('appOrgOrders.updated_at')
    // ->get([
    //   // 'appOrgProducts.name as name', 
    //   'appOrgOrderDetails.price as y', 
    //   DB::raw('DATE_FORMAT(appOrgOrders.updated_at, "%d-%b-%Y") as x'),
    //   // 'appOrgOrders.updated_at as x',
    //   // 'appOrgUserInventories.box_condition as box_condition',
    //   // 'appOrgUserInventories.manual_condition as manual_condition',
    //   // 'appOrgUserInventories.cover_condition as cover_condition',
    //   // 'appOrgUserInventories.game_condition as game_condition',
    //   // 'appOrgUserInventories.extra_condition as extra_condition',
    //   // 'appOrgUserInventories.inside_condition as inside_condition',
      
    //   ]);

    $product = $request->product ? $request->product - 1000 : 0;
    $d = explode(' - ', $request->date);
    $desde = date('Y-m-d', strtotime($d[0])) . ' 00:00:00' ;
    $hasta =  date('Y-m-d', strtotime($d[1])) . ' 23:59:59' ;
    $categoria = false;
    $categoria_id = 0;

    if(AppOrgProduct::where('id', $product)->first()){
      if(AppOrgCategory::where('id', AppOrgProduct::where('id', $product)->first()->prod_category_id)->first()){
        if(AppOrgCategory::where('id', AppOrgProduct::where('id', $product)->first()->prod_category_id)->first()->parent_id == 0){
          $categoria_id = AppOrgCategory::where('id', AppOrgProduct::where('id', $product)->first()->prod_category_id)->first()->id;
        }else{
          $categoria_id = AppOrgCategory::where('id', AppOrgProduct::where('id', $product)->first()->prod_category_id)->first()->parent_id;
        }
      }
    }
    if($categoria_id == 1){

      $nuevo = AppOrgProductFeedback::where('game_condition', 'NEW')
      ->where('box_condition', 'NEW')
      ->where('manual_condition', 'NEW')
      ->where('cover_condition', 'NEW')
      ->where('extra_condition', 'NEW')
      ->where('product_id', $product)
      ->whereBetween('updated_at', [$desde, $hasta])
      ->get(['price as y', DB::raw('DATE_FORMAT(updated_at, "%d-%b-%Y %H:%i") as x')]);

      $nuevos_id = AppOrgProductFeedback::where('game_condition', 'NEW')
      ->where('box_condition', 'NEW')
      ->where('manual_condition', 'NEW')
      ->where('cover_condition', 'NEW')
      ->where('extra_condition', 'NEW')
      ->where('product_id', $product)
      ->whereBetween('updated_at', [$desde, $hasta])->pluck('id');
      
      $medio = AppOrgProductFeedback::whereNotIn('id', $nuevos_id)
      ->where('product_id', $product)
      ->whereBetween('updated_at', [$desde, $hasta])
      ->get(['price as y', DB::raw('DATE_FORMAT(updated_at, "%d-%b-%Y %H:%i") as x')]);
      // error_log("nuevo");
      // error_log($nuevo);
      // error_log("medio");
      // error_log($medio);
      // error_log("values");
      $bajo = AppOrgProductFeedback::where('product_id', $product)
      ->whereBetween('updated_at', [$desde, $hasta])->get(['price as y', DB::raw('DATE_FORMAT(updated_at, "%d-%b-%Y %H:%i") as x')]);
      
      $pricenew  = 0;
      if( AppOrgProductFeedback::where('game_condition', 'NEW')
      ->where('box_condition', 'NEW')
      ->where('manual_condition', 'NEW')
      ->where('cover_condition', 'NEW')
      ->where('extra_condition', 'NEW')
      ->where('product_id', $product)
      ->whereBetween('updated_at', [$desde, $hasta])->count() > 0){
        $pricenew  = AppOrgProductFeedback::where('game_condition', 'NEW')
        ->where('box_condition', 'NEW')
        ->where('manual_condition', 'NEW')
        ->where('cover_condition', 'NEW')
        ->where('extra_condition', 'NEW')
        ->where('product_id', $product)
        ->whereBetween('updated_at', [$desde, $hasta])->sum('price') / AppOrgProductFeedback::where('game_condition', 'NEW')
        ->where('box_condition', 'NEW')
        ->where('manual_condition', 'NEW')
        ->where('cover_condition', 'NEW')
        ->where('extra_condition', 'NEW')
        ->where('product_id', $product)
        ->whereBetween('updated_at', [$desde, $hasta])->count();
      }

      $priceused = 0;
      if(AppOrgProductFeedback::whereNotIn('id', $nuevos_id)
      ->where('product_id', $product)
      ->whereBetween('updated_at', [$desde, $hasta])->count() > 0){
        // error_log(AppOrgProductFeedback::whereNotIn('id', $nuevos_id)
        // ->where('product_id', $product)
        // ->whereBetween('updated_at', [$desde, $hasta])->get());
        $priceused = AppOrgProductFeedback::whereNotIn('id', $nuevos_id)
        ->where('product_id', $product)
        ->whereBetween('updated_at', [$desde, $hasta])->sum('price') / AppOrgProductFeedback::whereNotIn('id', $nuevos_id)
        ->where('product_id', $product)
        ->whereBetween('updated_at', [$desde, $hasta])->count();
      }
      
      $pricevery = 0;
      if(AppOrgProductFeedback::where('product_id', $product)
      ->whereBetween('updated_at', [$desde, $hasta])->count() > 0 ){
        $pricevery = AppOrgProductFeedback::where('product_id', $product)
        ->whereBetween('updated_at', [$desde, $hasta])->sum('price') / AppOrgProductFeedback::where('product_id', $product)
        ->whereBetween('updated_at', [$desde, $hasta])->count();

      }
    }

    ///error_nog
    return ['nuevo' => $nuevo, 'medio' => $medio, 'bajo' => $bajo, 'pricenew' => $pricenew, 'priceused' => $priceused, 'pricevery' => $pricevery];  
  }

  public function showImage() {
    $prod = AppOrgProduct::whereNotNull('id_imagen')->get();
    foreach ($prod as $key) {
      
        $name =  strtolower(str_replace(" ","_", $key->platform)) . '_' . date('YmdHis') . '.JPG';
        
        $fail = true;

        try {
          File::copy(public_path() . '/img2/' . $key->id_imagen  . '.JPG' , public_path() . '/img3/'.$name);
          $fail = true;
        }  catch (\Exception $ex) {
          $fail = false;
        }

        if($fail){
          $imgProd = new AppOrgProductImage();
          $imgProd->product_id = $key->id;
          $imgProd->image_path = $name;
          $imgProd->save();
        }
    }
    return redirect('/');
  }



  public function PendinPays() {
    // error_log('hola');    
  }

  /**
	 * Show the view for the search result
	 *
	 * @return view
	 */

  public function showSearchProduct() {

    $this->loadCart();

    $params = Request::input();
    $catIds  = [];
    $categoria = 0;
    $cat_unit   = isset($params['cat']) ? $params['cat'] : '';
    if($cat_unit != ""){
      if(AppOrgCategory::where('name', $cat_unit)->first()){
        $categoria = AppOrgCategory::where('name', $cat_unit)->first()->id;
      }else{
        return abort(404);
      }
    }

    $sub_cat_unit   = isset($params['scat']) ? $params['scat'] : '';
    if($sub_cat_unit != ""){
      if(AppOrgCategory::where('name', $sub_cat_unit)->first()){
        array_push($catIds, AppOrgCategory::where('name', $sub_cat_unit)->first()->id);
      }
    }

    $text     = isset($params['text']) ? $params['text'] : '';

    $this->viewData['category_ids'] = $catIds;

    $this->viewData['platform'] = SysDictionary::where('code', 'GAME_PLATFORM')->get(['value_id', 'value']);
    
    $this->viewData['result'] = AppOrgProduct::whereNull('deleted_at');

    // $categories     = isset($input['categories']) ? $input['categories'] : [];

    if(count($this->viewData['category_ids']) > 0) {
      $this->viewData['result']->where('prod_category_id', $this->viewData['category_ids'][0]);
    }
    
    // else if (count($this->viewData['category_ids']) > 1 ) {
    //   $this->viewData['result']->whereIn('prod_category_id',$this->viewData['category_ids']);
    // }

    if ( strlen($text) > 0 ) {
      $this->viewData['result']->where('name', 'like', '%'.$text.'%')->where('platform', 'like', '%'.$text.'%');
    }
    // $all = $this->viewData['result']->get();
     //dd($this->viewData);
    $this->viewData['search_text']    = $text;
    // $this->viewData['result']         = $all;
    $inventory_latest_users  = AppOrgUserInventory::where('quantity', '>', 0)->whereNull('deleted_at')->orderBy('id','desc')->take(10)->get();

    return view('brcode.front.public.search', compact('inventory_latest_users'))->with('viewData',$this->viewData);

  }

  public function Marketplace() {

    $this->loadCart();
    $inventory_latest_users  = AppOrgUserInventory::where('quantity', '>', 0)->whereNull('deleted_at')->orderBy('id','desc')->take(10)->get();

    return view('brcode.front.public.store.market', compact('inventory_latest_users'))->with('viewData',$this->viewData);

  }

  /**
   * Get products
   *
   * @return json
   */
  public function getProducts() {
    $input = Request::input();
    $text           = isset($input['text']) ? $input['text'] : '';
    $categories     = isset($input['categories']) ? $input['categories'] : [];
    $sortColumn     = isset($input['sortColumn']) ? $input['sortColumn'] : '';
    $sortDir        = isset($input['sortDir']) ? $input['sortDir'] : '';
    $filterPro        = isset($input['filter']) ? $input['filter'] : false;
    
    $products_id = AppOrgUserInventory::where('quantity', '>', 0)->whereNull('deleted_at')->pluck('product_id');

    $products       = AppOrgProduct::withAll();
    //dd($products);
    

    if($filterPro) {
      $products->whereIn('id', $products_id);
    }

    if( count($categories) > 0 ) {
      
      $products->whereIn('prod_category_id', $categories);
    }
    if(str_contains($text , '_platform_sort_')){
      $txt_search = substr($text,0, strpos($text, '_platform_sort_'));
      $plt_search = str_replace("_platform_sort_","", stristr($text, '_platform_sort'));

      if(strlen($txt_search) > 0){
        $products->where('name', 'like', '%'. $txt_search .'%');
      }
      if(strlen($plt_search) > 0){
        $products->where('platform', $plt_search);
      }
    }else{
      if(strlen($text) > 0){
        $products->where('name', 'like', '%'.$text.'%')->orWhere('platform', 'like', '%'.$text.'%');
      }
    }

    if( strlen($sortColumn) > 0 && strlen($sortDir) > 0 ) {
      $products->orderBy($sortColumn, $sortDir);
    }

    return response()->json([
      'error'   => 0,
      'length'  => $products->count(),
      'records' => $products->skip( ($input['currentPage'] - 1) * $input['numPerPage'] )->take( $input['numPerPage'] )->orderBy('id', 'desc')->get(),
    ]);
  }
  
  public function testImage() {
    $images = App\AppOrgProductImage::where('created_by', 0)->paginate(1000);
    return view('brcode.front.public.test')->with('images', $images);

  }

  public function showSearchNewProduct() {
    
    if(Request::ajax()) {

      $params = Request::input();
      // error_log($params['name']);
      $data = AppOrgProduct::whereNull('deleted_at')->where('name', 'like', '%'.$params['name'].'%')->orWhere('platform', 'like', '%'.$params['name'].'%')
      ->with('images')
      ->orderBy('name')
      ->get();
      $total = count($data);
      $searchData = [];
      // error_log($data->count());
      if($total > 0){
        foreach($data->take(10) as $key){
          $d = array(
            'code' => '/product/'. ($key->id + 1000),
            'name' => $key->name,
            'platform' => $key->platform . ' - ' . $key->region,
            'image' => $key->images->count() > 0 ? '/images/'.$key->images->first()->image_path : '/assets/images/art-not-found.jpg',
          );
          array_push($searchData, $d);
        }
      }
      /*
      foreach($searchData as $key){
        // error_log($key[0]);
      }*/

      return [$searchData, $total];
    }
  }
  /**
	 * Show the view for the product page
	 *
	 * @return view
	 */

  public function showProduct($id) {

    $this->loadCart();

    //$respuesta_champion = Http::get('https://www.pricecharting.com/api/offers?t=YOUR_TOKEN&seller=7cgdyu5ynzos3mxwhfh46xj3f4&status=sold');
    
    //$champion = $respuesta_champion->json();
    //dd($champion); 

    $product = AppOrgProduct::findOrFail($id);

    $product_request = ProductRequest::where('product_id', $product->id)->get();
    //dd($product_request);

    foreach($product_request as $p) {
      if($p->user_read == 'N'){
        $p->user_read = 'Y';
        $p->save();
      }
    }
    //dd($product) 
    //dd($product);
    //$prueba = AppOrgProduct::find($productId)->toArray(); 
    //$result = array_intersect($champion, $prueba);
    //dd($prueba,$result); 

     

    if($product) {
      $producto = $product;

      $this->viewData['inventory'] =  AppOrgUserInventory::ofSeller()->where('product_id', $id )
        ->where('quantity', '>', 0)->orderBy('price','ASC')->get();
     

        $chart_sold = AppOrgUserInventory::where('product_id', $id)
        ->where('quantity_sold', '>', 0)
        ->orderBy('id', 'DESC')->get();

        //dd($chart_sold);
        //dd($seller);
      // error_log(count($this->viewData['inventory']));
      $this->viewData['product_images']   = $product->images;
      $this->viewData['product_lang']     = $product->lang;
    }

    return view('brcode.front.public.product', compact('producto','chart_sold'))->with('viewData',$this->viewData);
  }

  /**
	 * Show the view for offer product page
	 *
	 * @return view
	 */
  public function showOfferProduct($id = 0) {
    if($id <= 0 ) {
      return 'Invalid product';
    }


    if(Auth::id() > 0) {
      return redirect('/account/inventory/add/' . $id);
    }
    else {
      $this->loadCart();
      return view('brcode.front.public.offer_register')->with('viewData',$this->viewData);
    }
  }
  
  /**
	 * AJAX function to upload an image
	 *
	 * @return json
	 */
  public function deleteInventoryItem(){
    if(Request::ajax()){
      $input = Request::all();
      $qty = 0;
      if(is_numeric($input['qty']) && is_numeric($input['pro'] ) ){
        if($input['qty'] > 0 && ($input['pro'] - 1000) > 0){
          $inventory = AppOrgUserInventory::find($input['pro'] - 1000);
          if($inventory->quantity <= $input['qty']){
            $inventory->delete();
          }else{
            $inventory->quantity -= $input['qty'];
            $inventory->save();
            $qty = $inventory->quantity;
          }
        }
      }
      return Response::json(array('error' => 0, 'message' => 'success', 'qty' => $qty), 200);
    }else{
      return Response::json(array('message' => 'danger'), 400);
    }
  }


  /**
	 * AJAX function to upload an image
	 *
	 * @return json
	 */
  public function uploadInventoryImage(BaseRequest $r) {
    
    
    if(isset($r->dropzone)){
      $inputName = 'file';
      $file = Request::file($inputName);
       

      if($file) {

          $destinationPath = public_path() . '/uploads/inventory-images/';

          $extension = $file->getClientOriginalExtension();
          $filename = md5(microtime()) . '.' . $extension;

          // Prepare dir
          $dateFolder = date('Y') . '/' . date('m') . '/' . date('d') . '/';

          $directory = $destinationPath . $dateFolder;

          $filenameean  = 'inv_img_' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $file->getClientOriginalExtension();
          //$filename = 'inv_img_' . Auth::id() . '_'.microtime(true).'.' . $extension;
          //$filenameean = 'inv_img_' . Auth::id() . '_'.microtime(true).'.jpg';
          
          $upload_success = Request::file($inputName)->move($directory, $filename);

          //dd($upload_success);

          $webp  = (string) Image::make($directory . $filename)->resize(600, 750, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
          })->encode('jpg', 75);
          
     
                
          file_put_contents($directory . $filenameean, $webp);

          unlink($directory . $filename);
 

          if ($upload_success) {
              // resizing an uploaded file
              //Image::make($destinationPath . $filename)->resize(200, 200)->save($destinationPath . $filename200);
              //return Response::json(array('error' => 1, 'message' => 'error'), 400);
              //dd($upload_success);
               return Response::json(array('error' => 0, 'message' => 'success', 'name' => $dateFolder . $filenameean), 200);
          } else {
              return Response::json(array('error' => 1, 'message' => 'error'), 400);
          }
      }
    }

    $inputName = 'files';
		$file = Request::file($inputName);

		if($file) {

        $destinationPath = public_path() . '/uploads/inventory-images/';

				$extension = $file[0]->getClientOriginalExtension();
				$filename = md5(microtime()) . '.' . $extension;

        // Prepare dir
        $dateFolder = date('Y') . '/' . date('m') . '/' . date('d') . '/';

        $directory = $destinationPath . $dateFolder;

				$filename200 = 'inv_img_' . Auth::id() . '_'.microtime(true).'.' . $extension;
        $upload_success = Request::file($inputName)[0]->move($directory, $filename);

        if ($upload_success) {
            // resizing an uploaded file
            //Image::make($destinationPath . $filename)->resize(200, 200)->save($destinationPath . $filename200);
            //return Response::json(array('error' => 1, 'message' => 'error'), 400);
            return Response::json(array('error' => 0, 'message' => 'success', 'name' => $dateFolder . $filename), 200);
        } else {
            return Response::json(array('error' => 1, 'message' => 'error'), 400);
        }
    }
  }

  public function uploadInventoryImageAdmin(BaseRequest $r,$id) {
    $data =  AppOrgProduct::findOrFail($id);  
    $imagen = AppOrgProductImage::where('product_id', $id)->get();
    //dd($imagen);
    $inputName = 'file';
    $file = Request::file($inputName);
    $image = $file;
    //dd($image);
    $imagename =  time() . $image->getClientOriginalName();
    $image->move(public_path('images'),$imagename);

    $imageUpload = new AppOrgProductImage();
    $imageUpload->product_id = $data->id;
    //dd($imageUpload->product_id);
    $imageUpload->image_path = $imagename;
    $imageUpload->save(); 

    return response()->json(['success' => $imagename]);
  }

  public function removeInventoryImageAdmin(BaseRequest $r,$id) {
    $filename =  $r->get('filename');//gives orginal file name eg:abc.jpg
    AppOrgProductImage::where('image_path',$filename)->delete();
    return response()->json(['success' => 'Borrado']);
  }


  /**
	 * AJAX function to upload an image
	 *
	 * @return json
	 */
  public function uploadInventoryImageEa(BaseRequest $r) {
    
    
    if(isset($r->dropzone)){
      $inputName = 'file';
      $file = Request::file($inputName);

      if($file) {

          $destinationPath = public_path() . '/uploads/inventory-images/';

          $extension = $file->getClientOriginalExtension();
          $filename = md5(microtime()) . '.' . $extension;

          // Prepare dir
          $dateFolder = date('Y') . '/' . date('m') . '/' . date('d') . '/';

          $directory = $destinationPath . $dateFolder;

          $filename200 = 'inv_img_' . Auth::id() . '_'.microtime(true).'.' . $extension;
          $upload_success = Request::file($inputName)->move($directory, $filename);

          if ($upload_success) {
              // resizing an uploaded file
              //Image::make($destinationPath . $filename)->resize(200, 200)->save($destinationPath . $filename200);
              //return Response::json(array('error' => 1, 'message' => 'error'), 400);
              return Response::json(array('error' => 0, 'message' => 'success', 'name' => $dateFolder . $filename), 200);
          } else {
              return Response::json(array('error' => 1, 'message' => 'error'), 400);
          }
      }
    }

    $inputName = 'files';
		$file = Request::file($inputName);

		if($file) {

        $destinationPath = public_path() . '/uploads/inventory-images/';

				$extension = $file[0]->getClientOriginalExtension();
				$filename = md5(microtime()) . '.' . $extension;

        // Prepare dir
        $dateFolder = date('Y') . '/' . date('m') . '/' . date('d') . '/';

        $directory = $destinationPath . $dateFolder;

				$filename200 = 'inv_img_' . Auth::id() . '_'.microtime(true).'.' . $extension;
        $upload_success = Request::file($inputName)[0]->move($directory, $filename);

        if ($upload_success) {
            // resizing an uploaded file
            //Image::make($destinationPath . $filename)->resize(200, 200)->save($destinationPath . $filename200);
            //return Response::json(array('error' => 1, 'message' => 'error'), 400);
            return Response::json(array('error' => 0, 'message' => 'success', 'name' => $dateFolder . $filename), 200);
        } else {
            return Response::json(array('error' => 1, 'message' => 'error'), 400);
        }
    }
  }

  /**
	 * AJAX function to remove an image
	 *
	 * @return json
	 */


  public function removeProductImage(BaseRequest $r) {
    $path = $r->file;
    $fileName = str_replace(date('Y') . '/' . date('m') . '/' . date('d') . '/', "", $path);

    AppOrgProductImage::where('image_path', $path)->delete();

    if(file_exists(public_path() . '/uploads/inventory-images/' . $path)) {
      unlink(public_path() . '/uploads/inventory-images/' . $path);
      return Response::json(array('message' => 'success'), 200);
    }

    return Response::json(array('message' => 'error', 'reason' => 'Unknown file'), 200);

  }

    /**
	 * Show the view for offering a product
	 *
	 * @return view
	 */
  public function showInventoryItem($id = 0, $qty = 0) {

    if($id <= 0 ) {
      return redirect('/account/inventory')->with('message_confirm', 'Invalid product')->with('type_msg', 'danger');
    }
    if(Auth::id() > 0) {
      $id = $id - 1000;
      $this->viewData['model_id'] = 0;
      $this->viewData['model_data'] = [];
      $this->viewData['qty_pro'] = $qty;
      $product = false;
      if(Request::is('account/inventory/modify/*')) {
        $model = AppOrgUserInventory::find($id);
        $prueba = 0;
        if($model) {
          $product = AppOrgProduct::find($model->product_id);
          $this->viewData['model_id'] = $model->id;
          $this->viewData['inventory_images'] = $model->images;
          $this->viewData['inventory_images_ean'] = $model->imagesean;
          $this->viewData['model_data'] = $model->toArray();
        }
      }
      else if(Request::is('account/inventory/add/*')) {
        $product = AppOrgProduct::find($id);
        $prueba = 0;
      }
      else if(Request::is('account/inventory/update/*/*')){
        $model = AppOrgUserInventory::find($id);
      
        if($model) {
          $product = AppOrgProduct::find($model->product_id);
          $prueba = $model->images->count();
          //dd($prueba);
          $this->viewData['model_id'] = $model->id;
          $this->viewData['qty_pro'] = $qty;
          $this->viewData['type_dv'] = true;
          $this->viewData['inventory_images'] = $model->images;
          $this->viewData['inventory_images_ean'] = $model->imagesean;
          $this->viewData['model_data'] = $model->toArray();
        }
        
      }

      if($product) {

        $this->viewData['product'] = $product;
        $producto = $product;

        $dictionaryModel = SysDictionary::whereIn('code',['GAME_STATE','GAME_LOCATION','GAME_LANGUAGE','GAME_SUPPORT'])
          ->orderBy('code')->orderByRaw(' IFNULL(order_by,0) ')->get();
        $gameStates   = [];
        $gameLocation = [];
        $gameLanguage = [];
        $gameSupport  = [];

        foreach($dictionaryModel as $key => $row) {
          switch($row->code) {
            case 'GAME_STATE':
              array_push($gameStates, ['id' => $row->value_id, 'value' => $row->value]);
              break;
            case 'GAME_LOCATION':
              array_push($gameLocation, ['id' => $row->value_id, 'value' => $row->value]);
              break;
            case 'GAME_LANGUAGE':
              array_push($gameLanguage, ['id' => $row->value_id, 'value' => $row->value]);
              break;
            case 'GAME_SUPPORT':
              array_push($gameSupport, ['id' => $row->value_id, 'value' => $row->value]);
              break;
          }
        }
        $this->viewData['product_images']   = $product->images;
        $this->viewData['game_location']    = $gameLocation;
        $this->viewData['game_language']    = $gameLanguage;
        $this->viewData['game_states']      = $gameStates;
        $this->viewData['game_support']     = $gameSupport;

        $this->viewData['product_id']       = $product->id;
        
        $cat = substr(AppOrgCategory::find($product->prod_category_id)->category_text, 0, strpos(AppOrgCategory::find($product->prod_category_id)->category_text, ' -> '));
        
        $cat = AppOrgCategory::where('parent_id', 0)->where('name', 'like', $cat)->first();
        
        $this->loadCart();
        if($cat->id == 2){
          return view('brcode.front.private.offerSub', compact('producto','prueba'))->with('viewData',$this->viewData);
        } elseif($cat->id == 1) {
          return view('brcode.front.private.offer', compact('producto','prueba'))->with('viewData',$this->viewData);
        } else {
          return view('brcode.front.private.offerOther', compact('producto','prueba'))->with('viewData',$this->viewData);
        }

      }
      else {
        return back();
      }

    }

  }

  public function showInventoryItemSide($id = 0, $qty = 0) {

    if($id <= 0 ) {
      return redirect('/account/inventory')->with('message_confirm', 'Invalid product')->with('type_msg', 'danger');
    }
    if(Auth::id() > 0) {
      $id = $id - 1000;
      $this->viewData['model_id'] = 0;
      $this->viewData['model_data'] = [];
      $this->viewData['qty_pro'] = $qty;
      $product = false;
      if(Request::is('account/inventory/modify/*')) {
        $model = AppOrgUserInventory::find($id);
        if($model) {
          $product = AppOrgProduct::find($model->product_id);
          $this->viewData['model_id'] = $model->id;
          $this->viewData['inventory_images'] = $model->images;
          $this->viewData['model_data'] = $model->toArray();
        }
      }
      else if(Request::is('account/inventory/add/*')) {
        $product = AppOrgProduct::find($id);
      }
      else if(Request::is('account/inventory/update/*/*')){
        $model = AppOrgUserInventory::find($id);

        if($model) {
          $product = AppOrgProduct::find($model->product_id);
          $this->viewData['model_id'] = $model->id;
          $this->viewData['qty_pro'] = $qty;
          $this->viewData['type_dv'] = true;
          $this->viewData['inventory_images'] = $model->images;
          $this->viewData['model_data'] = $model->toArray();
        }
      }

      if($product) {

        $this->viewData['product'] = $product;

        $dictionaryModel = SysDictionary::whereIn('code',['GAME_STATE','GAME_LOCATION','GAME_LANGUAGE','GAME_SUPPORT'])
          ->orderBy('code')->orderByRaw(' IFNULL(order_by,0) ')->get();
        $gameStates   = [];
        $gameLocation = [];
        $gameLanguage = [];
        $gameSupport  = [];

        foreach($dictionaryModel as $key => $row) {
          switch($row->code) {
            case 'GAME_STATE':
              array_push($gameStates, ['id' => $row->value_id, 'value' => $row->value]);
              break;
            case 'GAME_LOCATION':
              array_push($gameLocation, ['id' => $row->value_id, 'value' => $row->value]);
              break;
            case 'GAME_LANGUAGE':
              array_push($gameLanguage, ['id' => $row->value_id, 'value' => $row->value]);
              break;
            case 'GAME_SUPPORT':
              array_push($gameSupport, ['id' => $row->value_id, 'value' => $row->value]);
              break;
          }
        }
        $this->viewData['product_images']   = $product->images;
        $this->viewData['game_location']    = $gameLocation;
        $this->viewData['game_language']    = $gameLanguage;
        $this->viewData['game_states']      = $gameStates;
        $this->viewData['game_support']     = $gameSupport;

        $this->viewData['product_id']       = $product->id;
        
        $cat = substr(AppOrgCategory::find($product->prod_category_id)->category_text, 0, strpos(AppOrgCategory::find($product->prod_category_id)->category_text, ' -> '));
        
        $cat = AppOrgCategory::where('parent_id', 0)->where('name', 'like', $cat)->first();
        
        $this->loadCart();
        if($cat->id == 2){
          return view('brcode.front.private.offerSub')->with('viewData',$this->viewData);
        } elseif($cat->id == 1) {
          return view('brcode.front.private.offer')->with('viewData',$this->viewData);
        } else {
          return view('brcode.front.private.offerOther')->with('viewData',$this->viewData);
        }

      }
      else {
        return back();
      }

    }

  }

  /**
	 * Save the inventory item and redirect
	 *
	 * @return redirect (back | account/inventory)
	 */
  public function saveInventoryItem() {
    
    $colPrefix = $this->viewData['col_prefix'];
    $input = Request::input();

    //dd($input);

    $id = $input['model_id'];

    $rules = [
			$colPrefix.'product_id'          => 'required',
			$colPrefix.'quantity'            => 'required',
			$colPrefix.'price'               => 'required',
			$colPrefix.'game_condition'      => 'required',
			$colPrefix.'box_condition'       => 'required',
			//$colPrefix.'cover_condition'     => 'required',
			//$colPrefix.'manual_condition'    => 'required',
			$colPrefix.'extra_condition'     => 'required',
		];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {
      
      if($input['type_dv'] == 1){
        $inv = AppOrgUserInventory::find($id);
        if(($inv->quantity - $input['br_c_quantity']) > 0){
          $inv->quantity -= $input['br_c_quantity'];
          $inv->save();
          $id = 0;
        }
      }
      
      $input['add_user_id'] = Auth::id();

      $id = saveModel($id, $model, 'App\\AppOrgUserInventory',$subModelExists, $input, $colPrefix, $new);
      
      $OrgInv = AppOrgUserInventory::find($id);

      $product = AppOrgProduct::find($OrgInv->product_id);

      $cat = substr(AppOrgCategory::find($product->prod_category_id)->category_text, 0, strpos(AppOrgCategory::find($product->prod_category_id)->category_text, ' -> '));   
      $cat = AppOrgCategory::where('parent_id', 0)->where('name', 'like', $cat)->first();

      $OrgInv->location = $cat ? $cat->id : 0;
      $OrgInv->save();
      
      saveSubModel($new, $id, 'inventory_id', 'App\\AppOrgUserInventory', 'images', 'image_path', 'App\\AppOrgUserInventoryImage'
        ,$colPrefix, $input,'0',[],[],'inv_img','inventory-images');


      saveSubModel($new, $id, 'inventory_id', 'App\\AppOrgUserInventory', 'images', 'image_path', 'App\\AppOrgUserInventoryEan'
        ,$colPrefix, $input,'1',[],[],'ean_img','ean-images');


      return redirect("/account/inventory")->with([
          'flash_message' => 'Inventario Actualizado',
          'flash_class'   => 'alert-success',
      ]);
    }

  }

  /*
	|--------------------------------------------------------------------------
	| User Account
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view for the home page of the application
	 *
	 * @return view
	 */
  public function showAccount() {

    //$this->loadBasicInfo();
    $this->loadCart();

    $this->viewData['inventories']  = AppOrgUserInventory::where('user_id', Auth::id())->orderBy('id','desc')->take(10)->get();
    $this->viewData['events']       = AppOrgUserEvent::where('user_id', Auth::id())->orderBy('id','desc')->take(10)->get();
    $this->viewData['sales']        = AppOrgOrder::where('seller_user_id', Auth::id())->where('created_on','>=', date('Y-m-d', strtotime('-30 days')))->orderBy('id','desc')->count();
    $this->viewData['purchases']    = AppOrgOrder::where('buyer_user_id', Auth::id())->where('created_on','>=', date('Y-m-d', strtotime('-30 days')))->orderBy('id','desc')->count();

    return view('brcode.front.private.account')->with('viewData',$this->viewData);

  }
  public function cancelAccountPurchases() {
    $input = Request::all();
    //dd($input);
    $message = "Order canceled successfully";
    
    if(AppOrgOrder::where('buyer_user_id', Auth::id())->where('order_identification', $input['model_id'])->first() || AppOrgOrder::where('seller_user_id', Auth::id() )->where('order_identification', $input['model_id'])->first()){
      $order = AppOrgOrder::where('order_identification', $input['model_id'])->first();
      $tipo = AppOrgOrder::where('buyer_user_id', Auth::id())->where('order_identification', $input['model_id'])->first() ? 'purchases' : 'sales';
      if($order->status == 'CN'){
        $message = 'Error, contact the administrator';
        return Redirect('/account/'. $tipo)->with('message',$message)->with('messageType','warning');
      }
      if($order->status == 'RP' || $order->status == 'CW'){
        try{
          $detail = AppOrgOrderDetail::where('order_id', $order->id)->get();
          $order->status = 'CN';
          $order->updated_at = date('Y-m-d H:i:s');
          $order->save();

          foreach($detail as $key){
            $inventory = AppOrgUserInventory::find($key->inventory_id);
            $inventory->quantity += $key->quantity;
            $inventory->save();
          }
         
          return Redirect('/account/'.$tipo)->with('message',$message)->with([
          'flash_message' => 'Cancelada Correctamente',
          'flash_class'   => 'alert-success',
          ]);
        } catch (Exception $ex) {
          $message = 'Error contact the administrator';
          return Redirect('/account/'.$tipo)->with('message',$message)->with([
            'flash_message' => 'Cancelada Correctamente',
            'flash_class'   => 'alert-success',
            ]);
        }
      }

    }
    return Redirect('/account/'.$tipo)->with('message',$message)->with([
      'flash_message' => 'Cancelada Correctamente',
      'flash_class'   => 'alert-success',
      ]);
  }

  /*
	|--------------------------------------------------------------------------
	| User Amount
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view for the home page of the application
	 *
	 * @return view
	 */
  public function CheckAmount() {
    $input = Request::all();
    $id = Auth::user()->id;
    $bank = SysAccountBank::where('user_id', $id)->first();
    $credit = (float) $input['credit'];
    $complete="Revise que su información bancaria sea correcta y vuelva a intentar, si el error persiste, contáctese con el administrador.";
    $user = Auth::user();
    $tab = '';
    if($credit > 0 && $bank->iban_code != "" && $bank->bic_code!="" && $user->cash >= $credit){
      $createdOn = date('Y-m-d H:i:s');
      $order = new AppOrgOrder;
      $order->buyer_user_id = Auth::id();
      $order->seller_user_id = 1;
      $order->created_on = $createdOn;
      $order->max_pay_out = date('Y-m-d H:i:s', strtotime('+14 days'));
      $order->status = 'CP';
      $order->save();
      $orderTotal = $credit;
      $orderQty = 1;

      $orderDetail = new AppOrgOrderDetail([
        'type'               =>  'CU',
        'seller_user_id'     =>  $order->seller_user_id,
        'product_id'         =>  0,
        'inventory_id'       =>  0,
        'quantity'           =>  1,
        'price'              =>  0,
        'total'              =>  number_format($credit,2,'.',''),
        'status'             =>  'CP',
        'created_on'         =>  $createdOn,
      ]);

      $order->details()->save($orderDetail);

      $order->total = $orderTotal;
      $order->quantity  = $orderQty;
      $order->order_identification = Auth::id() . 'P' . getRandomToken(15);
      $order->paid_out = 'N';
      $order->save();

      $user->cash -= floatval($credit);
      $complete="Se ha notificado al administrador su solicitud";
      
      $type_msg = 'success';
    }else{
      $tab = 'fail';
      $type_msg = 'danger';
      return redirect('/account/profile')->with('viewData',$this->viewData)->with([
        'flash_class'     => 'alert-warning',
        'flash_message'   => 'Ha ocurrido un error/ Verifique si realmente tiene los campos de la cuenta bancaria llenos',
        'flash_important' => true,
      ]);
    }

    $message = $complete;
    //$this->loadBasicInfo();
    $this->loadCart();
    $this->viewData['inventories']  = AppOrgUserInventory::where('user_id', Auth::id())->orderBy('id','desc')->take(10)->get();
    $this->viewData['events']       = AppOrgUserEvent::where('user_id', Auth::id())->orderBy('id','desc')->take(10)->get();
    $this->viewData['sales']        = AppOrgOrder::where('seller_user_id', Auth::id())->where('created_on','>=', date('Y-m-d', strtotime('-30 days')))->orderBy('id','desc')->count();
    $this->viewData['purchases']    = AppOrgOrder::where('buyer_user_id', Auth::id())->where('created_on','>=', date('Y-m-d', strtotime('-30 days')))->orderBy('id','desc')->count();
    //$message = trans('app.user') . ' ' . trans('app.addCredit');

    return redirect('/account/profile')->with('viewData',$this->viewData)->with([
      'flash_class'     => 'alert-success',
      'flash_message'   => 'Tu solicitud ha sido procesada, recibirás el dinero en un plazo de 24/48hrs',
      'flash_important' => true,
    ]);

  }
  
  public function changeLangRetro(BaseRequest $request){

    if($request->input('locale') == 'en' || $request->input('locale') == 'es'){
      // error_log($request->input('locale'));
      $this->_putLangCookie($request->input('locale'));
      // error_log('$this->_getLangCookie()');
      // error_log($this->_getLangCookie());
      App::setLocale($request->input('locale'));
    }
   
    return redirect('/account/profile');
  }

  public function updateAccountEmail(BaseRequest $request){

    $message=[
        'email.required' => 'Email Obligatorio',
        'email.email' => 'Ingrese un email valido',
    ];
    $rules=[
        'email' => 'required|email|unique:sysUsers,email',
    ];
    //updateAccountBank
    $this->validate($request, $rules, $message);

    $user = Auth::user();
    $user->email = $request->input('email');
    $user->save();
    return redirect('/account/profile');
  }

  public function updateAccountInformation(BaseRequest $request){
    
    $rules=[
        'name' => 'required|min:3',
        'lastname' => 'required|min:3',
        'phone' => 'required|numeric',
        'address' => 'required|min:3',
        'postal' => 'required|min:3',
        'city' => 'required|min:3',
        'country' => 'required|numeric',
    ];

    //$this->validate($request, $rules, $message);
    $this->validate($request, $rules);

    $user = Auth::user();
    $user->first_name = $request->input('name');
    $user->last_name = $request->input('lastname');
    $user->phone = $request->input('phone');
    $user->address = $request->input('address');
    $user->zipcode = $request->input('postal');
    $user->city = $request->input('city');
    $user->country_id = $request->input('country');
    $user->save();
    
    return redirect('/account/profile')->with([
      'flash_class'   => 'alert-warning',
      'flash_message' => 'Informacion Actualizada Con Exito.',
      ]);

  }

  /*
  public function sendContactUs(Request $request) {

    $rules = [
			'captcha'                        => 'required|captcha',
			'contact_name'                   => 'required',
			'contact_email'                  => 'required',
			'contact_subject'                => 'required',
			'contact_message'                => 'required',
		];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {

      Mail::to(env('CONTACT_MAIL'))->queue(new ContactForm($input));

			return redirect('/contact-us')->with('contact_form_message',__('Mensaje enviado satisfactoriamente'));
    }
  }
  */

  public function updateAccountPass(Request $request){
    $message = 'Contraseña actualizada correctamente';
    $type_msg = 'success';
    $rules=[
        'lastpass' => 'required',
        'password' => 'required|min:8|alpha_num|confirmed',
    ];
    $input = Request::all();

    $v = Validator::make($input, $rules);

    if($v->fails()) {
      $message = 'Hubo un error al cambiar la contraseña, intentelo nuevamente';
      $type_msg = 'warning';
      return Redirect::back()
        ->withErrors($v->errors()) // send back all errors to the login form
        ->withInput(Request::all())->with('message_confirm', $message)->with('type_msg', $type_msg);
    }
    else {      
      if(Hash::check($input['lastpass'], Auth::user()->password)){
        $user = Auth::user();
        $user->password = bcrypt($input['password']);
        $user->save();
      }else{
        $message = 'Contraseña antigua incorrecta';
        $type_msg = 'danger';
      }

    }
    return redirect('/account/profile')->with('message_confirm', $message)->with('type_msg', $type_msg);

  }


  /*
	|--------------------------------------------------------------------------
	| User Amount
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view for the home page of the application
	 *
	 * @return view
	 */
  public function showPaymentAccount() {

    //$this->loadBasicInfo();
    $this->loadCart();
    $total = 0;
    foreach(Auth::user()->cart->details as $key){
      //// error_log($key);
      $total += $key->inventory->price * $key->quantity;
      //// error_log($total);
    }
    foreach(Auth::user()->cart->shipping as $key){
     
      $total += $key->shipping->price;
      //// error_log($total);
    }
    
    $orders = AppOrgOrder::where('buyer_user_id', Auth::id())->where('status', 'RP')->get();
    foreach($orders as $key){
      $total += $key->total;
    }
    //$message = trans('app.user') . ' ' . trans('app.addCredit');
    return view('brcode.front.private.payment')->with('viewData',$this->viewData)->with('total', $total);

  }




  /*
	|--------------------------------------------------------------------------
	| User Account Profile Public
	|--------------------------------------------------------------------------
	|
	*/

  public function showPublicProfile($user) {
    //$this->loadBasicInfo();
    $this->loadCart();
    if(!SysUser::where('user_name', $user)->first()){
      return abort(404);
    }
    $usuario = SysUser::where('user_name', $user)->first();
    //dd($usuario->id);
    $rating = AppOrgUserRating::where('seller_user_id', $usuario->id)->where('processig', '>', 0)->orderBy('created_at','desc')->get();
    $social = SysUserSocial::where('user_id', $usuario->id)->get();
    $this->viewData['ratingsCount'] = AppOrgUserRating::where('seller_user_id', Auth::id())->where('processig', '>', 0)->count();
  

    $fp1 = $rating->where('processig', 1)->count();
    $fp2 = $rating->where('processig', 2)->count();
    $fp3 = $rating->where('processig', 3)->count();

    $fa1 = $rating->where('packaging', 1)->count();
    $fa2 = $rating->where('packaging', 2)->count();
    $fa3 = $rating->where('packaging', 3)->count();
    
    $fd1 = $rating->where('desc_prod', 1)->count();
    $fd2 = $rating->where('desc_prod', 2)->count();
    $fd3 = $rating->where('desc_prod', 3)->count();
    $imgs = AppOrgProductImage::groupBy('product_id')->pluck('product_id');
    $this->viewData['inventory_random_product']  = AppOrgProduct::whereIn('id', $imgs)
    ->whereNull('deleted_at')  
    ->inRandomOrder()
    ->take(10)
    ->get();

    $profesional = SysUserRoles::where('user_id', $usuario->id)->where('role_id', 2)->get();
 
    $this->viewData['ratings'] = $rating;

    $this->viewData['sc1'] = $rating->count() > 0 ? (($fp1 / $rating->count()) + ($fa1 / $rating->count()) + ($fd1 / $rating->count())) /3 : 0;
    $this->viewData['sc2'] = $rating->count() > 0 ? (($fp2 / $rating->count()) + ($fa2 / $rating->count()) + ($fd2 / $rating->count())) /3 : 0;
    $this->viewData['sc3'] = $rating->count() > 0 ? (($fp3 / $rating->count()) + ($fa3 / $rating->count()) + ($fd3 / $rating->count())) /3 : 0;

    if(SysUserRoles::where('user_id', $usuario->id)->where('role_id', 2)->count() > 0){
      return view('brcode.front.public.profile_profesional')->with('viewData',$this->viewData)->with('user',$user)->with('usuario',$usuario)->with('social',$social);
     } else {
      return view('brcode.front.public.profile')->with('viewData',$this->viewData)->with('user',$user)->with('usuario',$usuario)->with('social',$social);
     }
    
  }

  public function showPublicUserInventory(BaseRequest $request, $user){
    $this->loadCart();
    
    $user = $user;
    $this->viewData['platform'] = SysDictionary::where('code', 'GAME_PLATFORM')->get(['value_id', 'value', 'parent_id']);
    $this->viewData['categoria_id'] = 1;
    $usuario = SysUser::where('user_name', $user)->first();
    $ari = AppOrgUserInventory::where('user_id', $usuario->id);
    //dd($ari);
   
    $viewData = $this->viewData;
    
    return view('brcode.front.public.inventory_public', compact('user', 'viewData', 'usuario','ari'));
  }
  
  public function showPublicProfilePositive($user) {
    //$this->loadBasicInfo();
    $this->loadCart();
    
    $usuario = SysUser::where('user_name', $user)->first();
    $rating = AppOrgUserRating::where('score', '>', 0.5)->where('processig', '>', 0)->where('seller_user_id', $usuario->id)->orderBy('created_at','desc')->get();

    $this->viewData['ratings'] = $rating;
    //$this->viewData['user'] = $user;
    return view('brcode.front.public.profile')->with('viewData',$this->viewData)->with('user',$user);
    
  }
  
  public function showPublicProfileNeutral($user) {
    //$this->loadBasicInfo();
    $this->loadCart();
    $usuario = SysUser::where('user_name', $user)->first();
    $rating = AppOrgUserRating::where('score', 0.5)->where('processig', '>', 0)->where('seller_user_id', $usuario->id)->orderBy('created_at','desc')->get();

    $this->viewData['ratings'] = $rating;
    //$this->viewData['user'] = $user;
    return view('brcode.front.public.profile')->with('viewData',$this->viewData)->with('user',$user);
    
  }
  
  public function showPublicProfileNegative($user) {
    //$this->loadBasicInfo();
    $this->loadCart();
    $usuario = SysUser::where('user_name', $user)->first();
    $rating = AppOrgUserRating::where('score', '<', 0.5)->where('processig', '>', 0)->where('seller_user_id', $usuario->id)->orderBy('created_at','desc')->get();

    $this->viewData['ratings'] = $rating;
    //$this->viewData['user'] = $user;
    return view('brcode.front.public.profile')->with('viewData',$this->viewData)->with('user',$user);
    
  }

  public function showPublicProfileList(BaseRequest $request) {
    if(!Auth::user()){
      $this->loadCart();
      return view('brcode.front.public.user_list_register')->with('viewData',$this->viewData);
    }
    $this->loadCart();
    $usuario = SysUser::all();

   /* if(SysUserRoles::where('user_id', Auth::id())->whereIn('role_id', [3])->first()){
      dd('prueba');
  } else {
      dd('falso');
  }
  */
    
    /*
    $rating = AppOrgUserRating::where('score', '<', 0.5)->where('processig', '>', 0)->where('seller_user_id', $usuario->id)->orderBy('created_at','desc')->get();
    */
    $this->viewData['usuario'] = $usuario;
    $viewData = $this->viewData;
    return view('brcode.front.public.listUser', compact('viewData','usuario'));
    
  }
  
  /*
	|--------------------------------------------------------------------------
	| User Account Profile
	|--------------------------------------------------------------------------
	|
	*/

  public function showAccountProfile() {
    //$this->loadBasicInfo();
    $this->loadCart();

    $this->viewData['ratings']      = AppOrgUserRating::where('seller_user_id', Auth::id())->where('processig', '>', 0)->orderBy('created_at','desc')->get();

    

    return view('brcode.front.private.profile')->with('viewData',$this->viewData);
  }

  public function showAccountProfilePositive() {
    //$this->loadBasicInfo();
    $this->loadCart();

    $this->viewData['ratings']      = AppOrgUserRating::where('score', '>', 0.5)->where('processig', '>', 0)->where('seller_user_id', Auth::id())->orderBy('created_at','desc')->get();
     
    return view('brcode.front.private.profile')->with('viewData',$this->viewData);
  }

  public function showAccountProfileNeutral() {
    //$this->loadBasicInfo();
    $this->loadCart();

    $this->viewData['ratings']      = AppOrgUserRating::where('score', 0.50)->where('processig', '>', 0)->where('seller_user_id', Auth::id())->orderBy('created_at','desc')->get();
     
    return view('brcode.front.private.profile')->with('viewData',$this->viewData);
  }

  public function showAccountProfileNegative() {
    //$this->loadBasicInfo();
    $this->loadCart();

    $this->viewData['ratings']      = AppOrgUserRating::where('score', '<', 0.5)->where('processig', '>', 0)->where('seller_user_id', Auth::id())->orderBy('created_at','desc')->get();
     
    return view('brcode.front.private.profile')->with('viewData',$this->viewData);
  }


  public function showListInventory() {
    
		if(Request::ajax()) {
      $data = AppOrgUserInventory::where('user_id', Auth::id())->orderBy('id','desc')->get();
      //i.id','p.name','i.created_at','i.quantity','i.quantity_sold'
      $datos = [];
      foreach($data as $key){
				$mapArray = (object) array(
					"d0" => '<a href="' . url('/account/inventory/modify/' . ($key->id + 1000) ).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>',
					"d1" => $key->product->name,
					"d2" => $key->quantity,
					"d3" => $key->quantity_sold,
					"d4" => date('Y-m-d H:i:s', strtotime($key->created_at)),
				);
				array_push($datos, $mapArray);
			}
			return response()->json([
				'data' => $datos, 
			]);
		}
  }



  



  /*
	|--------------------------------------------------------------------------
	| User Account Inventory
	|--------------------------------------------------------------------------
	|
	*/

  public function showAccountInventory() {
    $this->loadCart();
    $this->viewData['form_url_add'] = url('/account/inventory/add');
		$this->viewData['form_url_dt'] =  url('/account/inventory/list-dt');
		$this->viewData['table_cols'] = ['Id', 'Nombre', 'Cantidad', 'Vendidos','Creado el'];
    $this->viewData['form_dt_cols'] = json_encode([
      ["data" => 'id', "name" => 'i.id'],
      ["data" => 'name', "name" => 'p.name'],
      ["data" => 'quantity', "name" => 'i.quantity'],
      ["data" => 'quantity_sold', "name" => 'i.quantity_sold'],
      ["data" => 'created_at', "name" => 'i.created_at'],
      ["data" => 'action', "searchable" => false]
    ]);
    
    return view('brcode.front.private.inventory')->with('viewData',$this->viewData);
  }

  public function dtAccountInventory() {
    $inventory = DB::table('appOrgUserInventories AS i')
    ->select(['i.id','p.name','i.created_at','i.quantity','i.quantity_sold'])
    ->join('appOrgProducts AS p','p.id','=','i.product_id')
    ->where('i.user_id', Auth::id())
    ->whereNull('i.deleted_at')
    ;
    return Datatables::of($inventory)
			->addColumn('action', function ($inventory) {
                return '<a href="' . url('/account/inventory/modify/' . ($inventory->id + 1000) ).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
            })
			->make(true);
  }

  /*
	|--------------------------------------------------------------------------
	| User Account Purchases
	|--------------------------------------------------------------------------
	|
	*/
  public function showAccountPurchases() {
    $this->loadCart();
    $this->viewData['form_url_add'] = url('/account/purchases/add');
		$this->viewData['form_url_dt'] =  url('/account/purchases/list-dt');
		$this->viewData['table_cols'] = ['Creado el', 'Tipo/Transacción', 'Payment/Shipping', 'Artículos comprados', 'Total'];
    $this->viewData['form_dt_cols'] = json_encode([
      ["data" => 'order_identification', "name" => 'o.order_identification'],
      ["data" => 'created_on', "name" => 'o.created_on'],
      ["data" => 'quantity', "name" => 'o.quantity'],
      ["data" => 'paid_out', "name" => 'o.paid_out'],
      ["data" => 'total', "name" => 'o.total'],
      ["data" => 'action', "searchable" => false]
      
    ]);
    $this->viewData['order_user'] = AppOrgOrder::where('buyer_user_id', Auth::id())->get();
    return view('brcode.front.private.purchases')->with('viewData',$this->viewData);
  }

  public function dtAccountPurchases() {
    $purchase = DB::table('appOrgOrders AS o')
    ->select(['o.id','o.order_identification','o.paid_out','o.created_on','o.quantity',DB::raw('FORMAT(o.total,2) total')])
    ->where('o.buyer_user_id', Auth::id())
    ->whereNull('o.deleted_at')
    ;
    return Datatables::of($purchase)
			->addColumn('action', function ($purchase) {
                return '<a href="' . url('/account/purchases/view/' . $purchase->order_identification ).'" class="btn btn-xs btn-primary"><i class="fa fa-search"></i> '.trans('app.view').'</a> '.' <button data-url="' . url('/account/purchases/cancel/' . $purchase->order_identification ).'" class="btn btn-xs btn-danger paid-status-' . $purchase->paid_out . '" onclick="dis(this)"><i class="fa fa-ban"></i> '.trans('app.cancel').'</button>';
            })
			->make(true);
  }

  public function showAccountPurchaseDetails($orderIdentification) {
    if(strlen($orderIdentification) == 0)
      return Redirect('/account/purchases');

    $this->loadCart();

    $order = AppOrgOrder::where('order_identification',$orderIdentification)->where('buyer_user_id', Auth::id())->first();
    $shipping = AppOrgOrderShipping::where('order_id', $order->id)->first();
    $order_shipping = SysShipping::find($order->shipping_id);
    if($order == null)
      return abort(404);
    
    $ttotal = 0;
    $details = $order->details()->get();
    $ttotal = $order->total;
    $ttotal = $ttotal;
    if($ttotal < 0){
      $ttotal = 0;
    }else{
      $ttotal = $ttotal + (($ttotal * 0.034) + 0.35); 
    }
    //117,98
    
    //// error_log($ttotal);
    $this->viewData['order_header']           = $order;
    $this->viewData['rating']           = $order->rating;
    $this->viewData['total_paypal']           = $ttotal;
    $this->viewData['order_details']          = $details;
    $this->viewData['shipping_detail']          = $shipping;
    $this->viewData['order_shipping']         = $order_shipping;
    $this->viewData['paid_out']          = $order->paid_out;
    $this->viewData['order_status']          = $order->status;
    return view('brcode.front.private.purchase_view')->with('viewData', $this->viewData);
  }

  public function showAccountSales() {
    $this->loadCart();
    $this->viewData['form_url_add'] = url('/account/sales/add');
		$this->viewData['form_url_dt'] =  url('/account/sales/list-dt');
		$this->viewData['table_cols'] = ['Creado el', 'Tipo/Transacción', 'Artículos comprados', 'Total'];
    $this->viewData['form_dt_cols'] = json_encode([
      ["data" => 'order_identification', "name" => 'o.order_identification'],
      ["data" => 'created_on', "name" => 'o.created_on'],
      ["data" => 'quantity', "name" => 'o.quantity'],
      ["data" => 'total', "name" => 'o.total'],
      ["data" => 'action', "searchable" => false]
    ]);

    return view('brcode.front.private.sales')->with('viewData',$this->viewData);
  }

  public function listSales() {
    if(Request::ajax()) {
      
      $data = AppOrgOrder::where('seller_user_id', Auth::id() )->get();

			$datos = [];
			foreach($data as $key){
				$mapArray = (object) array(
					"d0" => $key->created_on,
					"d1" => $key->order_identification,
          "d2" => $key->quantity,
					"d3" => $key->total,
					"d4" => $key->statusDes,
        );
        array_push($datos, $mapArray);
      }
			return response()->json([
				'data' => $datos, 
			]);
		}
  }

  public function showOrderRating($orderIdentification) {
    if(strlen($orderIdentification) == 0)
      return Redirect('/account/purchases');

    $this->loadCart();

    $order = AppOrgOrder::where('order_identification',$orderIdentification)->where('buyer_user_id', Auth::id())->first();
    
    if(!$order->rating)
      return Redirect('/account/purchases/view/'.$orderIdentification);
    
    if($order->rating->status == true)
      return Redirect('/account/purchases/view/'.$orderIdentification);
    
    if($order == null)
      return abort(404);

    $this->viewData['order_header']           = $order;
    $this->viewData['rating']           = $order->rating;
    return view('brcode.front.private.purchase_rating')->with('viewData', $this->viewData);
  }

  public function saveOrderRating(BaseRequest $request, $orderIdentification) {
    $this->loadCart();
    if(strlen($orderIdentification) == 0)
      return Redirect('/account/purchases');
    
    $order = AppOrgOrder::where('order_identification',$orderIdentification)->where('buyer_user_id', Auth::id())->first();
    if(!$order->rating)
      return Redirect('/account/purchases');

    if($request['optionsProcess'] < 1 && $request['optionsProcess'] > 3)
      return Redirect('/account/purchases/view/'.$orderIdentification);
      
    if($request['optionsPackage'] < 1 && $request['optionsPackage'] > 3)
      return Redirect('/account/purchases/view/'.$orderIdentification);

    if($request['optionsDescription'] < 1 && $request['optionsDescription'] > 3)
      return Redirect('/account/purchases/view/'.$orderIdentification);

    $total = 0;

    if ($request['optionsProcess'] == 1){
      $total += 1;
    }
    if ($request['optionsProcess'] == 2){
      $total += 0.5;
    }
    if ($request['optionsProcess'] == 3){
      $total += 0;
    }
    if ($request['optionsPackage'] == 1){
      $total += 1;
    }
    if ($request['optionsPackage'] == 2){
      $total += 0.5;
    }
    if ($request['optionsPackage'] == 3){
      $total += 0;
    }
    if ($request['optionsDescription'] == 1){
      $total += 1;
    }
    if ($request['optionsDescription'] == 2){
      $total += 0.5;
    }
    if ($request['optionsDescription'] == 3){
      $total += 0;
    }


    if($total != 0)
      $total = $total/3;

    $rating = AppOrgUserRating::where('order_id',$order->id)->where('buyer_user_id', Auth::id())->first();
    $rating->score = $total;
    $rating->processig = $request['optionsProcess'];
    $rating->packaging = $request['optionsPackage'];
    $rating->desc_prod = $request['optionsDescription'];
    $rating->description = $request['description'];
    $rating->status = true;
    $rating->save();

    //// error_log($order->rating);
    return Redirect('/account/purchases/view/'.$orderIdentification);

  }

  public function dtAccountSales() {

    $_GET = $this->changeAlias($_GET, 'o.', 'appOrgOrders.');

    $sales = AppOrgOrder::whereHas('details', function($query) {
      $query->where('seller_user_id','=',Auth::id());
    });
    return Datatables::of($sales)
			->addColumn('action', function ($sales) {
                return '<a href="' . url('/account/sales/view/' . $sales->order_identification ).'" class="btn btn-xs btn-primary"><i class="fa fa-search"></i> '.trans('app.view').'</a>';
            })
			->make(true);
  }

  public function changeAlias($get, $aliasFrom, $aliasTo) {

    foreach($get['columns'] as $key => $item) {
      $get['columns'][$key]['name'] = str_replace($aliasFrom, $aliasTo, $get['columns'][$key]['name']);
    }

    return $get;
  }

  public function showAccountSaleDetails($orderIdentification) {
    if(strlen($orderIdentification) == 0)
      return Redirect('/account/purchases');

    $this->loadCart();

    $order = AppOrgOrder::where('order_identification',$orderIdentification)->where('seller_user_id', Auth::id())->first();
    

    $shipping = SysShipping::find($order->shipping_id);

    if($order == null)
      return abort(404);
    $shippingDetail = AppOrgOrderShipping::where('order_id', $order->id)->first();
    

    $details = $order->details()->get();
    $tp = $order->total;
    $ttotal = $tp + ($tp * 0.034) + 0.35; 

    $this->viewData['order_header']           = $order;
    $this->viewData['total_paypal']           = $ttotal;
    $this->viewData['order_details']          = $details;
    
    $this->viewData['order_shipping']         = $shipping;


    $this->viewData['user_shipping']         = SysUser::withTrashed()->find($order->buyer_user_id);
    $this->viewData['shipping_detail']        = $shippingDetail;
    $this->viewData['paid_out']          = $order->paid_out;
    $this->viewData['order_status']          = $order->status;
    return view('brcode.front.private.sale_view')->with('viewData', $this->viewData);

  }

  public function updateShippingSales(BaseRequest $request){

    $message = '';
    $type_msg = 'success';

    $rules=[
        'tracking_number' => 'required|min:6',
    ];
    $input = Request::all();

    $v = Validator::make($input, $rules);
    
    $order = AppOrgOrder::where('order_identification', $request['id'])->first();

    if($v->fails() && SysShipping::find($order->shipping_id)->certified == 'Yes') {
      $message = 'Complete los campos obligatorios';
      $type_msg = 'danger';
      return Redirect::back()
        ->withErrors($v->errors()) // send back all errors to the login form
        ->withInput(Request::all())->with('message_confirm', $message)->with('type_msg', $type_msg);
    }

    if(isset($order->cancelOrder)){
      if($order->cancelOrder->count() > 0 && $order->cancelOrder->ANSWER != ''){
        $message = 'El pedido fue cancelado, no se puede realizar esta acción';
        return redirect('/account/sales/view/'.$request['id'])->with('message_confirm',$message)->with('type_msg','danger');
      }
    }

    if(SysShipping::find($order->shipping_id)->certified == 'Yes'){
      $order->tracking_number = $request['tracking_number'];
    }

    $order->sent_on = date('Y-m-d H:i:s');
    $order->status = 'ST';
    $order->buyer_read = 'N';
    $order->seller_read = 'N';
    $order->save();

    if($order->shippingSelect->certified == 'No'){

      $user_seller = SysUser::find($order->seller_user_id);
      $total_seller = $order->total - $order->shipping_price;
      $user_seller->cash += $total_seller - (($total_seller * $user_seller->comision) / 100);
      $user_seller->cash += $order->shipping_price;
      $user_seller->save();

      $createdOn = date('Y-m-d H:i:s');

      $comisionRMG = 0;
      $comisionMaster = 0;
      if($user_seller->comision > 1){
        $comisionRMG = $user_seller->comision - 1;
        $comisionMaster = 1;
      } elseif($user_seller->comision > 0) {
        $comisionRMG = $user_seller->comision;
        $comisionMaster = 0;
      }

      if($comisionRMG > 0){
        $bankUser = SysUser::find(1);
        $bankUser->cash += (($total_seller * $comisionRMG) / 100);
        $bankUser->save();
        $orderC = new AppOrgOrder;
        $orderC->buyer_user_id = $user_seller->id;
        $orderC->seller_user_id = 1;
        $orderC->created_on = $createdOn;
        $orderC->max_pay_out = date('Y-m-d H:i:s', strtotime('+14 days'));
        $orderC->status = 'FD';
        $orderC->total = (($total_seller * $comisionRMG) / 100);
        $orderC->tax = 0;
        $orderC->quantity = 1;
        $orderC->order_identification = Auth::id() . 'C' . getRandomToken(15);
        $orderC->paid_out = 'Y';
        $orderC->instructions = $order->order_identification;
        $orderC->save();
      }

      if($comisionMaster > 0){
        
        $referred = SysUser::find($user_seller->referencia_id);
        if($referred){
          $referred->special_cash += (($total_seller * $comisionMaster) / 100);
          $referred->save();
          
          $orderC = new AppOrgOrder;
          $orderC->buyer_user_id = $user_seller->referencia_id;
          $orderC->seller_user_id = $user_seller->id;
          $orderC->created_on = $createdOn;
          $orderC->max_pay_out = date('Y-m-d H:i:s', strtotime('+14 days'));
          $orderC->status = 'EC';
          $orderC->total = (($total_seller * $comisionMaster) / 100);
          $orderC->tax = 0;
          $orderC->quantity = 1;
          $orderC->order_identification = Auth::id() . 'EC' . getRandomToken(15);
          $orderC->paid_out = 'Y';
          $orderC->instructions = $order->order_identification;
          $orderC->save();
        }
      }

      $this->payAllOrders($order->seller_user_id);
    }

    $message = 'El pedido fue marcado como enviado';
    return redirect('/account/sales/view/'.$request['id'])->with('message_confirm',$message)->with('type_msg','success');

  }

  public function updateShippingStatus(BaseRequest $request){
    $message = 'El pedido fue marcado como no recibido';

    
    $order = AppOrgOrder::where('order_identification', $request->id)->first();
    
    if($order->buyer_user_id == Auth::id()){
      if(SysShipping::find($order->shipping_id)->certified == 'No'){

        $order->shipping_status = 'No';
        $order->status = 'ON';
        $order->save();
        $user = Auth::user();
        $user->orders_no_received += 1;
        $user->save();

      }elseif(SysShipping::find($order->shipping_id)->certified == 'Yes'){
        
        $order->shipping_status = 'No';
        $order->status = 'ON';
        $order->save();
        $user = Auth::user();
        $user->orders_no_received += 1;
        $user->save();
        $ship = new AppOrgOrderNotReceived();
        $ship->order_id = $order->order_identification;
        $ship->buyer_username = $order->buyer->user_name;
        $ship->seller_username = $order->seller->user_name;
        $ship->import = $order->total;
        $ship->dateSend = $order->sent_on;
        $ship->tracking_number = $order->tracking_number;
        $ship->save();

      }
    }else{
      $message = 'No puedes realizar esta acción';
      return back()->with('message_confirm',$message)->with('type_msg','success');
    }
    return back()->with('message_confirm',$message)->with('type_msg','success');
  }

  // Pay order to seller
  public function completeAccountPurchases(BaseRequest $request){
    if(Hash::check($request['pass'], Auth::user()->password)){
      if(AppOrgOrder::where('order_identification',  $request['id'])->first()){
        $order = AppOrgOrder::where('order_identification',  $request['id'])->first();
        //dd($order->quantity);  
        if($order->status != 'DD'){
        
          $order->status = 'DD';
          $order->buyer_read = 'N';
          $order->seller_read = 'N';
          $order->delivered_on = date('Y-m-d H:i:s');
          if($order->shipping_status){
            $order->shipping_status = 'Yes';
          }
          $order->save();
          //dd($order->shippingSelect);
          if($order->shippingSelect->certified == 'Yes'){
            //dd('eee');
            $user_seller = SysUser::find($order->seller_user_id);
            $total_seller = $order->total - $order->shipping_price;

            $user_seller->cash += $total_seller - (($total_seller * $user_seller->comision) / 100);
            $user_seller->cash += $order->shipping_price;
            $user_seller->save();

            $createdOn = date('Y-m-d H:i:s');
            $comisionRMG = 0;
            $comisionMaster = 0;
            if($user_seller->comision > 1){
              $comisionRMG = $user_seller->comision - 1;
              $comisionMaster = 1;
            } elseif($user_seller->comision > 0) {
              $comisionRMG = $user_seller->comision;
              $comisionMaster = 0;
            }

            if($comisionRMG > 0){
              $bankUser = SysUser::find(1);
              $bankUser->cash += (($total_seller * $comisionRMG) / 100);
              $bankUser->save();
              $orderC = new AppOrgOrder;
              $orderC->buyer_user_id = $user_seller->id;
              $orderC->seller_user_id = 1;
              $orderC->created_on = $createdOn;
              $orderC->max_pay_out = date('Y-m-d H:i:s', strtotime('+14 days'));
              $orderC->status = 'FD';
              $orderC->total = (($total_seller * $comisionRMG) / 100);
              $orderC->tax = 0;
              $orderC->quantity = 1;
              $orderC->order_identification = Auth::id() . 'C' . getRandomToken(15);
              $orderC->paid_out = 'Y';
              $orderC->instructions = $order->order_identification;
              $orderC->save();
            }

            if($comisionMaster > 0){
              
              $referred = SysUser::find($user_seller->referencia_id);
              if($referred){
                $referred->special_cash += (($total_seller * $comisionMaster) / 100);
                // $referred->save();
                
                $orderC = new AppOrgOrder;
                $orderC->buyer_user_id = $user_seller->referencia_id;
                $orderC->seller_user_id = $user_seller->id;
                $orderC->created_on = $createdOn;
                $orderC->max_pay_out = date('Y-m-d H:i:s', strtotime('+14 days'));
                $orderC->status = 'EC';
                $orderC->total = (($total_seller *$comisionMaster) / 100);
                $orderC->tax = 0;
                $orderC->quantity = 1;
                $orderC->order_identification = Auth::id() . 'EC' . getRandomToken(15);
                $orderC->paid_out = 'Y';
                $orderC->instructions = $order->order_identification;
                $orderC->save();
              }


            }

            $this->payAllOrders($order->seller_user_id);
          }
          //dd('No');
          $user_seller = SysUser::find($order->seller_user_id);
          //dd($user_seller);
          $total_seller = $order->total - $order->shipping_price;
          //dd($total_seller);
          $user = Auth::user();
          
          if(Auth::user()->collaborator->count() > 0) {
            $evaluacion = $total_seller  * 20 / 100;
            //dd($evaluacion);
            $evaluacion_collaborator = $total_seller  * 10 / 100;
            //dd($evaluacion_collaborator);
            $month = count(AppOrgOrder::where('buyer_user_id', Auth::id())->whereIn('status', ['DD'])->whereMonth('created_at', '=', (date('m')) )->get());
            //dd($month);
            $createdOn = date('Y-m-d H:i:s');
            
            if($month == 1) {
              $especial = new AppOrgOrder;
              $especial->buyer_user_id = $user->id;
              $especial->seller_user_id = $user_seller->id;
              $especial->created_on = $createdOn;
              $especial->max_pay_out = $createdOn;
              $especial->status = 'AS';
              $especial->total = $evaluacion;
              $especial->tax = 0;
              $especial->quantity = $order->quantity;
              $especial->order_identification = Auth::id() . 'C' . getRandomToken(15);
              $especial->paid_out = 'Y';
              $especial->instructions = $order->order_identification;
              $especial->save();
            }
              //dd('Zelda Ocarina');
  
            foreach(Auth::user()->collaborator as $collaborator_user){
                //dd($collaborator_user);
                $collaborator_user->special_cash = $collaborator_user->special_cash + $evaluacion_collaborator;
                //dd($probando->special_cash);
                $collaborator_user->save();
                $especial_collaborator = new AppOrgOrder;
                $especial_collaborator->buyer_user_id = $user->id;
                $especial_collaborator->seller_user_id = $collaborator_user->id;
                $especial_collaborator->created_on = $createdOn;
                $especial_collaborator->max_pay_out = $createdOn;
                $especial_collaborator->status = 'YC';
                $especial_collaborator->total = $evaluacion;
                $especial_collaborator->tax = 0;
                $especial_collaborator->quantity = $order->quantity;
                $especial_collaborator->order_identification = Auth::id() . 'C' . getRandomToken(15);
                $especial_collaborator->paid_out = 'Y';
                $especial_collaborator->instructions = $order->order_identification;
                $especial_collaborator->save();
            }
          } 

          if($user_seller->collaborator->count() > 0) {
            //dd($user_seller->collaborator);
            $evaluacion = $total_seller  * 20 / 100;
            //dd($evaluacion);
            $evaluacion_collaborator = $total_seller  * 10 / 100;
            //dd($evaluacion_collaborator);
            $months = count(AppOrgOrder::where('seller_user_id', $user_seller->id)->whereIn('status', ['DD'])->whereMonth('created_at', '=', (date('m')) )->get());
            //dd($months);
            $createdOn = date('Y-m-d H:i:s');
            
            if($months == 1) {
              $especial = new AppOrgOrder;
              $especial->buyer_user_id = 1;
              $especial->seller_user_id = $user_seller->id;
              $especial->created_on = $createdOn;
              $especial->max_pay_out = $createdOn;
              $especial->status = 'AS';
              $especial->total = $evaluacion;
              $especial->tax = 0;
              $especial->quantity = $order->quantity;
              $especial->order_identification = Auth::id() . 'C' . getRandomToken(15);
              $especial->paid_out = 'Y';
              $especial->instructions = $order->order_identification;
              $especial->save();
            }
              //dd('Zelda Ocarina');
  
            foreach($user_seller->collaborator as $collaborator_user_seller){
                //dd($user->id);
                //dd($collaborator_user_seller->id);
                $collaborator_user_seller->special_cash = $collaborator_user_seller->special_cash + $evaluacion_collaborator;
                //dd($collaborator_user_seller->special_cash);
                $collaborator_user_seller->save();
                $especial_collaborator = new AppOrgOrder;
                $especial_collaborator->buyer_user_id = $user->id;
                $especial_collaborator->seller_user_id = $user_seller->id;
                $especial_collaborator->created_on = $createdOn;
                $especial_collaborator->max_pay_out = $createdOn;
                $especial_collaborator->status = 'YV';
                $especial_collaborator->total = $evaluacion_collaborator;
                $especial_collaborator->tax = 0;
                $especial_collaborator->quantity = $order->quantity;
                $especial_collaborator->order_identification = Auth::id() . 'C' . getRandomToken(15);
                $especial_collaborator->paid_out = 'Y';
                $especial_collaborator->instructions = $order->order_identification;
                $especial_collaborator->save();
            }
          }

          $rating = new AppOrgUserRating;
          $rating->seller_user_id = $order->seller_user_id;
          $rating->buyer_user_id = $order->buyer_user_id;
          $rating->order_id = $order->id;
          $rating->save();


        }


        return redirect('/account/purchases/view/'.$request['id'])->with([
          'flash_class'     => 'alert-success',
          'flash_message'   => 'Pedido entregado satisfactoriamente.',
          'flash_important' => true,
         ]);
      }
      
      $message = 'Ah ocurrido un error, póngase en contacto con el administrador';
      return redirect('/account/purchases/view/'.$request['id'])->with('message',$message)->with('messageType','danger');
    }
    return redirect('/account/purchases/view/'.$request['id'])->with([
      'flash_class'     => 'alert-danger',
      'flash_message'   => 'Contraseña incorrecta.',
      'flash_important' => true,
     ]);
  }

  public function showAccountSettings() {
    $this->loadCart();

    $this->viewData['user_data'] = SysUser::find(Auth::id());

    return view('brcode.front.private.settings')->with('viewData',$this->viewData);
  }

  /*
	|--------------------------------------------------------------------------
	| AccountBank
	|--------------------------------------------------------------------------
	|
  */
  public function showAccountBank() {
    $this->loadCart();

    $this->viewData['bank_data'] = SysAccountBank::where('user_id', Auth::id())->first();;

    return view('brcode.front.private.banks')->with('viewData',$this->viewData);
  }

  /**
   * Process the register account
   *
   * @return json
   */
  public function updateAccountBank(BaseRequest $request) {
    $this->loadCart();
    $rules = [
      'beneficiary'          => 'required',
      'iban_code'            => 'required',
      'bic_code'             => 'required',
      'bank_address'         => 'required',
    ];

    $input = Request::all();

    $this->validate($request, $rules);

    $bankAccount = SysAccountBank::where('user_id', Auth::id())->first();
    $bankAccount->beneficiary = $input['beneficiary'];
    $bankAccount->iban_code = $input['iban_code'];
    $bankAccount->bic_code = $input['bic_code'];
    $bankAccount->bank_address = $input['bank_address'];
    $bankAccount->save();
    $this->viewData['bank_data'] = $bankAccount;
    return redirect('/account/profile')->with([
      'flash_class'     => 'alert-success',
      'flash_message'   => 'Informacion bancaria actualizada correctamente.',
      'flash_important' => true,
  ]);
    //return view('brcode.front.private.banks')->with('viewData',$this->viewData);

    
  }
  
  /*
	|--------------------------------------------------------------------------
	| Cart
	|--------------------------------------------------------------------------
	|
  */

  public function showCart() {
    
    if(!Auth::user()){
      return Redirect('/');
    }
    $this->loadCart();

    $productsInCart = [];

    foreach(Auth::user()->cart->details as $key){
      $inventory = AppOrgUserInventory::find($key->inventory_id);
      $inventory->qty = $key->quantity;
      $inventory->inventory_images = $inventory->images;
      $inventory->product_info = AppOrgProduct::find($inventory->product_id);
      $inventory->product_info->product_images = $inventory->product_info->images;
      array_push($productsInCart, $inventory);
    }

    $users = [];
    $users_orders = [];
    foreach(Auth::user()->cart->inventory as $key_i){
      array_push( $users, $key_i->user->user_name);
      array_push( $users_orders, $key_i->user);
    }

    $ship_total = 0;

    $u = array_unique($users);
    sort($u);

    $cart_new = Auth::user()->cart;
    
    $this->viewData['cart_new'] = $cart_new;
    $this->viewData['products_in_cart'] = $productsInCart;
    $this->viewData['us'] = $u;
    $this->viewData['shipping_total'] = $ship_total;
    $this->viewData['shipping_values'] = Auth::user()->cart->shipping;
    $this->viewData['shipping'] = SysShipping::orderBy('price')->get();
    $this->viewData['user_order'] = array_unique($users_orders);

    $month = count(AppOrgOrder::where('buyer_user_id', Auth::id())->whereIn('status', ['DD'])->whereMonth('created_at', '=', (date('m')) )->get());
    //dd($month);

    return view('brcode.front.public.cart', compact('month'))->with('viewData', $this->viewData)->with('us', $u);
  }

  /**
	 * Add item to cart
	 *
	 * @return json
	 */

  public function cartAddItem() {
    if(Request::ajax()) {
      if(Auth::user()){

        $input  = Request::input();
        $ship   = $this->_getShip();
        $inventoryId  = $input['inventory_id'];
        $inventoryQty = $input['inventory_qty'];

        $stock = 0;
        $error_cart = 1;
        if(is_numeric($input['inventory_id']) && is_numeric($input['inventory_qty'])){

          $inventory_cart = AppOrgUserInventory::find($input['inventory_id'] - 1000);
          
          if($inventory_cart->user_id != Auth::id()){

            if(AppOrgCartDetailUser::where('inventory_id', $inventory_cart->id)->where('cart_id', Auth::user()->cart->id )->first()){
              $detail = AppOrgCartDetailUser::where('inventory_id', $inventory_cart->id)->where('cart_id', Auth::user()->cart->id )->first();
              if($input['inventory_qty'] > 0){
                if($inventory_cart->quantity - $input['inventory_qty'] >= 0){
                  //Inventory exist
                  if(isset($input['inventory_typ'])){
                    //Change Cart view
                    $inventory_cart->quantity += $detail->quantity;
                    $inventory_cart->save();
                    $detail->quantity = $input['inventory_qty'];
                    $detail->save();
                    $inventory_cart->quantity -= $input['inventory_qty'];
                    $inventory_cart->save();
                  }else{
                    //More items from produts
                    $detail->quantity += $input['inventory_qty'];
                    $detail->save();
                    $inventory_cart->quantity -= $input['inventory_qty'];
                    $inventory_cart->save();
                  }
                  
                }
              }else{
                //Delete product form cart
                $inventory_cart->quantity += $detail->quantity;
                $inventory_cart->save();
                $detail->delete();
              }
            }else{
              if($inventory_cart->quantity - $input['inventory_qty'] >= 0){
                //New Item Cart
                $detail = new AppOrgCartDetailUser;
                $detail->cart_id = Auth::user()->cart->id;
                $detail->inventory_id = $inventory_cart->id;
                $detail->quantity = $input['inventory_qty'];
                $detail->save();
                $inventory_cart->quantity -= $input['inventory_qty'];
                $inventory_cart->save();

              }
            }


            $stock = $inventory_cart->quantity;

            $items_total = 0;
            foreach(Auth::user()->cart->details as $detail){
              $items_total += $detail->quantity;
            }
            $error_cart = 0;
          }
        }

      }
      

      return response()->json([
        'error' => $error_cart, 
        'items' => $items_total,
        'Qty' => $stock,
      ]);
    }
  }
  
  public function cartAddShipping() {
    if(Request::ajax()) {
      if(Auth::user()){
        $input  = Request::input();
        $user = SysUser::where('user_name', $input['d1'])->first();
        $shipping_selected = SysShipping::find($input['d2']);
        
        if(AppOrgCartShipping::where('cart_id', Auth::user()->cart->id)->where('user_id', $user->id)->first()){
          $shipping = AppOrgCartShipping::where('cart_id', Auth::user()->cart->id)->where('user_id', $user->id)->first();
          $shipping->shipping_id = $shipping_selected->id;
          $shipping->save();
          // error_log('hola_shipping_old');
          // error_log($shipping);
        } else {
          // error_log('hola_shipping_new');
          $shipping = new AppOrgCartShipping();
          $shipping->user_id = $user->id;
          $shipping->cart_id = Auth::user()->cart->id;
          $shipping->shipping_id = $shipping_selected->id;
          $shipping->save();
        }

      }
      return response()->json([
        'error' => 0, 
      ]);

    }
  }

  public function _getLangCookie() {
    return Session::get('lang.retro');
  }

  public function _putLangCookie($lang) {
    Session::put('lang.retro', $lang);
    Cookie::queue(Cookie::make('lang_retro', $lang, 267840));
  }

  private function _getCart() {
    return Session::get('user.cart');
  }

  private function _getShip() {
    return Session::get('user.ship');
  }

  private function _putCart($cart) {
    Session::put('user.cart', $cart);
    Cookie::queue(Cookie::make('user_cart', json_encode($cart), 10080));
  }
  private function _putShip($ship) {
    Session::put('user.ship', $ship);
    Cookie::queue(Cookie::make('user_ship', json_encode($ship), 10080));
  }

  private function _clearCart() {
    Session::forget('user.cart');
    Cookie::queue(
        Cookie::forget('user_cart')
    );
    Session::forget('user.ship');
    Cookie::queue(
        Cookie::forget('user_ship')
    );
  }

  /*
	|--------------------------------------------------------------------------
	| Checkout
	|--------------------------------------------------------------------------
	|
	*/
  public function showCheckout() {

    $this->loadCart();
    $cart = $this->viewData['cart'];
    if( count($cart) == 0) {
      return Redirect('/cart');
    }

    $updateCart = false;
    $productsInCart = [];
    foreach($cart as $key => $qty) {

      $inventory = AppOrgUserInventory::find($key);

      $inventory->qty = $qty;
      $inventory->inventory_images = $inventory->images;
      $inventory->product_info = AppOrgProduct::find($inventory->product_id);
      $inventory->product_info->product_images = $inventory->product_info->images;
      $user = SysUser::find($inventory->user_id);
      $inventory->seller_name = strlen($user->user_name) > 0 ? $user->user_name : $user->user_name;
      array_push($productsInCart, $inventory);

      if($inventory->quantity < $inventory->qty) {
        unset($cart[$key]);
        $updateCart = true;
      }

    }

    if($updateCart)
      $this->_putCart($cart);

    $this->viewData['products_in_cart'] = $productsInCart;

    return view('brcode.front.private.checkout')->with('viewData', $this->viewData);
  }

  /*
	|--------------------------------------------------------------------------
	| Checkout Avalon cash
	|--------------------------------------------------------------------------
	|
	*/

  private function getCartPaypalItemsNew($currency, $orderIdentification) {

    $items = [];
    $itemsLow = [];
    $total = 0;

    $order = AppOrgOrder::where('order_identification', $orderIdentification)->first();
 
    
    $varTotalPagar = number_format($order->total,2,'.','');
    
    if(Auth::user()->special_cash > 0){
      $varTotalPagar -= number_format(Auth::user()->special_cash,2,'.','');
    }
    
    if(Auth::user()->cash > 0){
      $varTotalPagar -= number_format(Auth::user()->cash,2,'.','');
    }

    $varTotal = $varTotalPagar + (($varTotalPagar * 0.034) + 0.35); 
    
    if($varTotal < 0){
      return abort(500);
    }

    $item = new \PayPal\Api\Item();
    $item->setName('Pago Paypal Retro Gaming Market / RGM');
    $item->setQuantity(1);
    $item->setPrice($varTotal);
    $item->setCurrency($currency);
    array_push($items, $item);
    
    // error_log(number_format($varTotal,2,'.',''));
    return [
      'total'       =>  number_format($varTotal,2,'.',''),
      'items'       =>  $items,
      'items_low'   =>  $itemsLow,
    ];

  }

  public function payWithPayPal($orderIdentification)
  {
      $sys = SysSettings::where('type', 'PayPal')->first();

      $payer = new Payer();
      $payer->setPaymentMethod('paypal');
      $currency = config('brcode.paypal_currency');

      $itemDetails = $this->getCartPaypalItemsNew($currency, $orderIdentification);

      if(count($itemDetails['items_low']) > 0)
      return response()->json(['error' => 1, 'message' => 'low_quantity', 'ids' => $itemDetails['items_low']]);

      $itemList = new ItemList();
      $itemList->setItems($itemDetails['items']);

      $amount = new Amount();
      $amount->setTotal($itemDetails['total']);
      $amount->setCurrency($currency);

      $transaction = new Transaction();
      $transaction->setAmount($amount);
      $transaction->setDescription('See your IQ resultados');
      $transaction->setItemList($itemList);

      $callbackUrl = url('/success_checkout_new/'.$orderIdentification);

      $redirectUrls = new RedirectUrls();
      $redirectUrls->setReturnUrl($callbackUrl)
          ->setCancelUrl($callbackUrl);

      $payment = new Payment();
      $payment->setIntent('sale')
              ->setPayer($payer)
              ->setTransactions(array($transaction))
              ->setRedirectUrls($redirectUrls);

    try {
    $payment->create($this->apiContext);
    return response()->json(['error' => 0, 'payment' => $payment, 'redirect_to' => $payment->getApprovalLink()]);
        }
    catch (\PayPal\Exception\PayPalConnectionException $ex) {
    return response()->json(['error' => 1, 'message' => trans('app.paypal_error'), 'paypal' => $ex->getData()]);
       }

  }

  public function processCheckoutNew($orderIdentification) {
    //// error_log($orderIdentification);
    $sys = SysSettings::where('type', 'PayPal')->first();

    $payer = new Payer();
    $payer->setPaymentMethod('paypal');
    $currency = config('brcode.paypal_currency');

    $itemDetails = $this->getCartPaypalItemsNew($currency, $orderIdentification);
    
    if(count($itemDetails['items_low']) > 0)
    return response()->json(['error' => 1, 'message' => 'low_quantity', 'ids' => $itemDetails['items_low']]);

    $itemList = new ItemList();

    $itemList->setItems($itemDetails['items']);

    //dd($itemList->setItems);

    $amount = new Amount();
    $amount->setTotal($itemDetails['total']);
    $amount->setCurrency($currency);
    

    $transaction = new Transaction();
    $transaction->setAmount($amount);
    $transaction->setItemList($itemList);
    

    $redirectUrls = new RedirectUrls();
    $redirectUrls->setReturnUrl(url('/success_checkout_new/'.$orderIdentification ))->setCancelUrl(url('/checkout'));

    $payment = new Payment();
    $payment->setIntent('sale')
        ->setPayer($payer)
        ->setTransactions(array($transaction))
        ->setRedirectUrls($redirectUrls);

    // After Step 3
    try {
        $payment->create($this->apiContext);
        return redirect()->away($payment->getApprovalLink());
    }
    catch (\PayPal\Exception\PayPalConnectionException $ex) {
        return response()->json(['error' => 1, 'message' => trans('app.paypal_error'), 'paypal' => $ex->getData()]);

    }

  }


  public function showSuccessCheckoutNew($orderIdentification) {
    
    $input = Request::input();
    $createdOn = date('Y-m-d H:i:s');
    $message = 'Ah ocurrido un error, póngase en contacto con el administrador';
    if (isset($input['paymentId']) && $input['paymentId'] != '') {

      $sys = SysSettings::where('type', 'PayPal')->first();
 
      
      $error = false;
      $paymentId = $input['paymentId'];
      $payment = Payment::get($paymentId, $this->apiContext);
      
     


      $execution = new PaymentExecution();
      $execution->setPayerId($input['PayerID']); 
      $currency = config('brcode.paypal_currency');
      $result = $payment->execute($execution, $this->apiContext);
      $itemDetails = $this->getCartPaypalItemsNew($currency, $orderIdentification);
      
      if(count($itemDetails['items_low']) > 0){
        return Redirect('/checkout');
      }

      $order = AppOrgOrder::where('order_identification', $orderIdentification)->first();
      //dd($order);
    
      /** */
      $us = Auth::user();
      $varTotal = 0;
      // $varTotal = $order->total;
      $totalpayment = floatval($order->total);

      if($us->special_cash > 0) {
        if($us->special_cash > $totalpayment) {
          $us->special_cash -= $totalpayment;
          $order->special_cash = $totalpayment;
          $totalpayment = 0;
        } else {
          $totalpayment -= $us->special_cash;
          $order->special_cash = $us->special_cash;
          $us->special_cash = 0;
        }
      }
      
      if($totalpayment > 0 && $us->cash > 0) {
        if($us->cash > $totalpayment){
          $us->cash -= $totalpayment;
          $totalpayment = 0;            
        }else{
          $us->cash = 0;
        }
      } 
      $us->save();
      // }

      $orderTotal = $order->total;
      $orderShipping = $order->shipping_price;

      
      $order_ide = $order->order_identification;
      $user_seller = SysUser::find($order->seller_user_id);
      $total_seller = $orderTotal - $orderShipping;
      
      $orderToken = $order->order_identification;

      $order->paid_out = 'Y';
      $order->buyer_read = 'N';
      $order->seller_read = 'N';
      $order->status = 'CR'; // saveordercr
      $order->total = $orderTotal;
      $order->max_pay_out = date('Y-m-d H:i:s', strtotime('+14 days'));
      $order->save();


      $detail = AppOrgOrderDetail::where('order_id', $order->id)->get();
      foreach($detail as $key) {
        $key->status = 'PD';
        $key->save();
      }

      $this->loadCart();

      try {
        $payment = Payment::get($paymentId, $this->apiContext);
        $orderPayment = new AppOrgOrderPayment([
          'payment_result' => $payment
        ]);
        $order->payment()->save($orderPayment);
      } catch (Exception $ex) {
        return redirect('/account/purchases/view/'.$order_ide )->with('message_confirm',$message)->with('type_msg','danger');
      }
      
      PtyCommons::setUserEvent(Auth::id(), 'Pago via paypal :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP(), 'order_id' => $orderToken]);

      foreach (AppOrgOrderDetail::where('order_id', $order->id)->get() as $detail) {
        $key = AppOrgUserInventory::where('id', $detail->inventory_id)->first();
        $feedback = new AppOrgProductFeedback();
        $feedback->box_condition = $key->box_condition;
        $feedback->manual_condition = $key->manual_condition;
        $feedback->cover_condition = $key->cover_condition;
        $feedback->game_condition = $key->game_condition;
        $feedback->extra_condition = $key->extra_condition;
        $feedback->inside_condition = $key->inside_condition;
        $feedback->ean = $key->ean;
        $feedback->inventory_id = $key->id;

        $feedback->seller_user_id = $key->user_id; //vendedor inventario

        $feedback->buyer_user_id = $order->buyer_user_id; //comprador

        $feedback->order_id = $detail->order_id; //vendedor

        $feedback->product_id = $key->product_id;
        $feedback->price = $key->price;
        $feedback->created_at = $detail->updated_at;
        $feedback->updated_at = $detail->updated_at;
        $feedback->save();
      }

      try {
        //OrderPaymentComplete
        Mail::to($order->seller->email)->queue(new OrderPaymentComplete(Auth::user(), $order));
        Mail::to($order->buyer->email)->queue(new OrderPaymentBuyer(Auth::user(), $order));    
      } catch (\Exception $ex) {
        return redirect('/account/purchases/view/'.$order_ide )->with('message_confirm',$message)->with('type_msg','danger');
      }

      $this->viewData['error'] = $error;

      if ($result->getState() === 'approved') {
        $message = 'El pago se realizó con éxito';
             return redirect('/account/purchases/view/'.$order_ide)->with([
        'flash_class'   => 'alert-success',
        'flash_message' => 'El pago se realizó con éxito.',
        ]);
      }

  
      return redirect('/account/purchases/view/'.$order_ide)->with([
     'flash_class'   => 'alert-danger',
     'flash_message' => 'A ocurrido un error.',
     ]);

     

    

    }
    else {
      abort(404);
    }

  }

  public function showSuccessOrder($val) {
    $this->loadCart();
    if($val == 'success'){
      $this->viewData['error'] = false;
    }else{
      $this->viewData['error'] = true;
    }

    
    return view('brcode.front.private.order_complete')->with('viewData', $this->viewData);
  }

  public function requestRefund(BaseRequest $request) {

    $message = 'Saldo insuficiente';

    

    $input = Request::all();

    $order = AppOrgOrder::where('order_identification', $request->id)->first();

    if($order->buyer_user_id == Auth::id()){
      $rules=[
        'cash' => 'required||numeric|between:0, 5',
      ];

      $v = Validator::make($input, $rules);

      if($v->fails()) {
        $message = 'Recuerde la cantidad maxima son 5€';
        $type_msg = 'danger';
        return Redirect::back()
          ->withErrors($v->errors()) // send back all errors to the login form
          ->withInput(Request::all())->with('message_confirm', $message)->with('type_msg', $type_msg);
      }
      
      if(Auth::user()->cash <= $request->cash)
        return back()->with('message_confirm',$message)->with('type_msg','warning');
      
      $user = Auth::user();
      $user->cash -= floatval($request->cash);
      // $user->save();
      
      $refund = new AppOrgOrder();
      $refund->buyer_user_id = Auth::id();
      $refund->seller_user_id = $order->seller_user_id;
      $refund->order_identification = Auth::id() . 'RE' . getRandomToken(15);
      $refund->instructions = $order->order_identification;
      $refund->seller_read = 'N';
      $refund->buyer_read = 'N';
      $refund->status = 'RE';
      $refund->paid_out = 'P';
      $refund->total = $request->cash;
      $refund->save();
      
      $message = 'Solicitud de pago extra enviada';
      return back()->with('message_confirm',$message)->with('type_msg','success');

    }elseif($order->seller_user_id == Auth::id()){
      $rules=[
        'cash' => 'required||numeric|between:0, 100',
      ];

      $v = Validator::make($input, $rules);

      if($v->fails()) {
        $message = 'Recuerde la cantidad maxima son 100€';
        $type_msg = 'danger';
        return Redirect::back()
          ->withErrors($v->errors()) // send back all errors to the login form
          ->withInput(Request::all())->with('message_confirm', $message)->with('type_msg', $type_msg);
      }
      //  seller
      if(Auth::user()->cash <= $request->cash)
        return back()->with('message_confirm',$message)->with('type_msg','warning');
      
      $user = Auth::user();
      $user->cash -= floatval($request->cash);
      
      $refund = new AppOrgOrder();
      $refund->buyer_user_id = $order->buyer_user_id;
      $refund->seller_user_id = Auth::id();
      $refund->order_identification = Auth::id() . 'RD' . getRandomToken(15);
      $refund->instructions = $order->order_identification;
      $refund->seller_read = 'N';
      $refund->buyer_read = 'N';
      $refund->status = 'RF';
      $refund->paid_out = 'P';
      $refund->total = $request->cash;
      $refund->save();

      $message = 'Solicitud de reembolso enviada';
      return back()->with('message_confirm',$message)->with('type_msg','success');
    }
    
    $message = 'Cuidado, no puedes realizar esta acción!!';
    return back()->with('message_confirm',$message)->with('type_msg','success');
  }

  public function payAllOrders($x){
    //$u = Auth::user();updateShippingStatus
    foreach(AppOrgOrder::where('buyer_user_id', $x)->where('status', 'RP')->where('paid_out', 'N')->orderBy('created_at')->get() as $order){
      $us = SysUser::where('id', $order->buyer_user_id)->first();
      if($us->cash + $us->special_cash >= $order->total){

        $totalpayment = floatval($order->total);

        if($us->special_cash > 0) {
          if($us->special_cash > $totalpayment) {
            $us->special_cash -= $totalpayment;
            $order->special_cash = $totalpayment;
            $totalpayment = 0;
          } else {
            $totalpayment -= $us->special_cash;
            $order->special_cash = $us->special_cash;
            $us->special_cash = 0;
          }
        }
        
        if($totalpayment > 0) {
          $us->cash -= $totalpayment;
          $totalpayment = 0;            
        }


        $orderToken = $order->order_identification;
        $order->paid_out = 'Y';
        $order->buyer_read = 'N';
        $order->seller_read = 'N';
        $order->status = 'CR'; // saveordercr
        $order->save();

        $this->loadCart();
        try {
          $payment = json_encode(array('date' => $order->created_on, 'order_id' => $order->order_identification, 'total' => $order->total, 'cantidad' => $order->quantity, 'type_payment' => 'Cash'));
          $orderPayment = new AppOrgOrderPayment([
            'payment_result' => $payment,
          ]);
          $order->payment()->save($orderPayment);
        } catch (Exception $ex) {
            $error = true;
        }
  
        $detail = AppOrgOrderDetail::where('order_id', $order->id)->get();
        foreach($detail as $key) {
          $key->status = 'PD';
          $key->save();
        }
    
        PtyCommons::setUserEvent(
          $order->buyer_user_id, 
          'Pago automático del sistema al terminar una compra', 
          [ 
            'date' => Carbon::now()->toDateTimeString(), 
            'ip_user' => '0.0.0.0', 
            'order_id' => $order->order_identification 
          ]
        );

        foreach (AppOrgOrderDetail::where('order_id', $order->id)->get() as $detail) {
          $key = AppOrgUserInventory::where('id', $detail->inventory_id)->first();
          $feedback = new AppOrgProductFeedback();
          $feedback->box_condition = $key->box_condition;
          $feedback->manual_condition = $key->manual_condition;
          $feedback->cover_condition = $key->cover_condition;
          $feedback->game_condition = $key->game_condition;
          $feedback->extra_condition = $key->extra_condition;
          $feedback->inside_condition = $key->inside_condition;
          $feedback->ean = $key->ean;
          $feedback->inventory_id = $key->id;

          $feedback->seller_user_id = $key->user_id; //vendedor inventario

          $feedback->buyer_user_id = $order->buyer_user_id; //comprador

          $feedback->order_id = $detail->order_id; //vendedor

          $feedback->product_id = $key->product_id;
          $feedback->price = $key->price;
          $feedback->created_at = $detail->updated_at;
          $feedback->updated_at = $detail->updated_at;
          $feedback->save();
        }

      }
    }

    return true;
  }

  public function responseRefund(BaseRequest $request){
    $orderSeller =  AppOrgOrder::where('status', 'RE')
              ->where('instructions', $request->order)
              ->where('seller_user_id', Auth::id())
              ->first();

    $orderBuyer =  AppOrgOrder::where('status','RF')
              ->where('instructions', $request->order)
              ->where('buyer_user_id', Auth::id())
              ->first();

    if($orderSeller){
      if($orderSeller->paid_out != 'P'){
        return back();
      }
      
      if($request->response == 'Y'){
        
        $user = Auth::user();

        $user->cash += $orderSeller->total;
        

        $orderSeller->paid_out = 'Y';
        $orderSeller->save();

        PtyCommons::setUserEvent(Auth::id(), 'Reembolso: Pago saldo personal en cuenta RGM el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP(), 'order_id' => $orderSeller->order_identification]);
          
        $message = 'Aceptado';
        return back()->with('message_confirm',$message)->with('type_msg','success');
      }elseif($request->response == 'N'){
        
        $user = SysUser::find($orderSeller->buyer_user_id);
        $user->cash += $orderSeller->total;

        $orderSeller->paid_out = 'N';
        $orderSeller->save();
        
        $message = 'Denegado';
        return back()->with('message_confirm',$message)->with('type_msg','danger');
      }
    }
    if($orderBuyer){
      if($orderBuyer->paid_out != 'P'){
        return back();
      }
      
      if($request->response == 'Y'){
        
        $user = Auth::user();
        $user->cash += $orderBuyer->total;
        $orderBuyer->paid_out = 'Y';
        $orderBuyer->save();

        PtyCommons::setUserEvent(Auth::id(), 'Pago extra: Saldo personal en cuenta RGM el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP(), 'order_id' => $orderBuyer->order_identification]);
          
        $message = 'Aceptado';
        return back()->with('message_confirm',$message)->with('type_msg','success');
      } elseif ($request->response == 'N'){
        
        $user = SysUser::find($orderBuyer->seller_user_id);
        $user->cash += $orderBuyer->total;
        $user->save();

        $orderBuyer->paid_out = 'N';
        $orderBuyer->save();

        $message = 'Denegado';
        return back()->with('message_confirm',$message)->with('type_msg','danger');
      }
    }
    
    

    $message = 'Cuidado, esta acción puede banear tu cuenta';
    return back()->with('message_confirm',$message)->with('type_msg','success');
  }

/*
	|--------------------------------------------------------------------------
	| Checkout Money
	|--------------------------------------------------------------------------
	|
	*/

  public function storeCheckout() {

    $input = Request::input();
    $pass = $input['pass'];
    if(!Hash::check($pass, Auth::user()->password)){
      return redirect('/cart')->with([
        'flash_class'     => 'alert-danger',
        'flash_message'   => 'Contraseña Incorrecta.',
        'flash_important' => true,
      ]);
    }
    
    $cartTotal = Auth::user()->cart->totalCart;
    $shippingTotal = Auth::user()->cart->totalShipping;
    $orderTotal = Auth::user()->cart->totalCart + Auth::user()->cart->totalShipping;

    $cart_id = Auth::user()->cart->id;
    $us = Auth::user();

    $error = false;

    if((Auth::user()->cash + Auth::user()->special_cash) >= $orderTotal){
      foreach(Auth::user()->cart->seller as $keyUser) {
        $shippin_user = SysUser::find($keyUser);

          $shipping = AppOrgCartShipping::where('cart_id', $cart_id)->where('user_id', $keyUser)->first();
     

          $createdOn = date('Y-m-d H:i:s');
          $max_pay_out = date('Y-m-d H:i:s', strtotime('+14 days'));
          $order = new AppOrgOrder;
          $order->buyer_user_id = Auth::id();
          $order->seller_user_id = $keyUser;

          $order->shipping_id = $shipping->shipping->id;
          $order->shipping_price = $shipping->shipping->price;

          $order->created_on = $createdOn;
          $order->max_pay_out = date('Y-m-d H:i:s', strtotime('+14 days'));
          $order->status = 'CR'; //saveordercr
          $order->save();
          $orderTotal = 0;
          $orderQty = 0;

          foreach(Auth::user()->cart->details as $key){

            $qty = $key->quantity;

            $inventory = AppOrgUserInventory::find($key->inventory_id);


            if($inventory->user_id == $keyUser){
              
              $inventory->quantity_sold = $inventory->quantity_sold + $qty;

              $inventory->save();

              $orderDetail = new AppOrgOrderDetail([
                'type'               =>  'PR',
                'seller_user_id'     =>  $inventory->user_id,
                'product_id'         =>  $inventory->product_id,
                'inventory_id'       =>  $inventory->id,
                'quantity'           =>  $qty,
                'price'              =>  $inventory->price,
                'total'              =>  number_format($inventory->price * $qty,2,'.',''),
                'status'             =>  'PD',
                'created_on'         =>  $createdOn,
              ]);
              
              $orderQty += $qty;
              $orderTotal += $inventory->price * $qty;
              $order->details()->save($orderDetail);

            }
          }
          
          $user_seller = SysUser::find($inventory->user_id);
          
          // $special_cash = $us->special_cash;
          
          $totalpayment = floatval($orderTotal + $order->shipping_price);

          if($us->special_cash > 0) {
            if($us->special_cash > $totalpayment) {
              $us->special_cash -= $totalpayment;
              $order->special_cash = $totalpayment;
              $totalpayment = 0;
            } else {
              $totalpayment -= $us->special_cash;
              $order->special_cash = $us->special_cash;
              $us->special_cash = 0;
            }
          }
          
          if($totalpayment > 0) {
            $us->cash -= $totalpayment;
            $totalpayment = 0;            
          }
          $order->total = ($orderTotal + $order->shipping_price);
          $order->quantity  = $orderQty;
          $order->order_identification = Auth::id() . 'P' . getRandomToken(15);
          $order->paid_out = 'Y';
          $order->save();

          $orderToken = $order->order_identification;
          

          $this->viewData['items_count'] = 0;

          try {
            $payment = json_encode(array('date' => $order->created_on, 'order_id' => $order->order_identification, 'total' => ($orderTotal + $order->shipping_price), 'cantidad' => $orderQty, 'type_payment' => 'Cash'));
            $orderPayment = new AppOrgOrderPayment([
              'payment_result' => $payment
            ]);
            $order->payment()->save($orderPayment);
          } catch (\Exception $ex) {
              $error = true;
              $this->loadCart();
          }

          foreach (AppOrgOrderDetail::where('order_id', $order->id)->get() as $detail) {
            $key = AppOrgUserInventory::where('id', $detail->inventory_id)->first();
            $feedback = new AppOrgProductFeedback();
            $feedback->box_condition = $key->box_condition;
            $feedback->manual_condition = $key->manual_condition;
            $feedback->cover_condition = $key->cover_condition;
            $feedback->game_condition = $key->game_condition;
            $feedback->extra_condition = $key->extra_condition;
            $feedback->inside_condition = $key->inside_condition;
            $feedback->ean = $key->ean;
            $feedback->inventory_id = $key->id;
  
            $feedback->seller_user_id = $key->user_id; //vendedor inventario
  
            $feedback->buyer_user_id = $order->buyer_user_id; //comprador
  
            $feedback->order_id = $detail->order_id; //vendedor
  
            $feedback->product_id = $key->product_id;
            $feedback->price = $key->price;
            $feedback->created_at = $detail->updated_at;
            $feedback->updated_at = $detail->updated_at;
            $feedback->save();
          }
          PtyCommons::setUserEvent(Auth::id(), 'Pago saldo personal en cuenta RGM el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP(), 'order_id' => $orderToken]);
          //OrderPaymentComplete
          try {
            Mail::to($order->seller->email)->queue(new OrderPaymentComplete(Auth::user(), $order));
            Mail::to($order->buyer->email)->queue(new OrderPaymentBuyer(Auth::user(), $order));   
          } catch(\Exception $e){
            $error = true;
            $this->loadCart();
          }
        

      }

         

      $cart = AppOrgCartUser::find(Auth::user()->cart->id);
      $cart->status = "Complete";
      $cart->save();

      $this->viewData['error'] = $error;
      $this->loadCart();
      $message = 'Purchased products';
      return Redirect('/account/purchases/view/'.$order->order_identification)->with('message_confirm',$message)->with('type_msg','success');

    }else{

      foreach(Auth::user()->cart->seller as $keyUser) {
        
       
          $shipping = AppOrgCartShipping::where('cart_id', $cart_id)->where('user_id', $keyUser)->first();

          $createdOn = date('Y-m-d H:i:s');
          $max_pay_out = date('Y-m-d H:i:s', strtotime('+7 days'));

          $order = new AppOrgOrder;
          $order->buyer_user_id = Auth::id();
          $order->seller_user_id = $keyUser;

          $order->shipping_id = $shipping->shipping->id;
          $order->shipping_price = $shipping->shipping->price;

          $order->created_on = $createdOn;
          $order->max_pay_out = date('Y-m-d H:i:s', strtotime('+14 days'));
          $order->status = 'RP';
          $order->save();
          $orderTotal = 0;
          $orderQty = 0;

          foreach(Auth::user()->cart->details as $key){

            $qty = $key->quantity;

            $inventory = AppOrgUserInventory::find($key->inventory_id);

            if($inventory->user_id == $keyUser){

              $inventory->quantity_sold = $inventory->quantity_sold + $qty;
              $inventory->save();
              $orderDetail = new AppOrgOrderDetail([
                'type'               =>  'PR',
                'seller_user_id'     =>  $inventory->user_id,
                'product_id'         =>  $inventory->product_id,
                'inventory_id'       =>  $inventory->id,
                'quantity'           =>  $qty,
                'price'              =>  $inventory->price,
                'total'              =>  number_format($inventory->price * $qty,2,'.',''),
                'status'             =>  'RP',
                'created_on'         =>  $createdOn,
              ]);
              
              $orderQty += $qty;
              $orderTotal += $inventory->price * $qty;
              $order->details()->save($orderDetail);
            }
          }

          $order->total = ($orderTotal + $order->shipping_price);
          $order->quantity  = $orderQty;
          $order->order_identification = Auth::id() . 'P' . getRandomToken(15);
          $order->paid_out = 'N';
          $order->save();
          
          $this->viewData['items_count'] = 0;
          try {
            Mail::to($order->seller->email)->queue(new OrderWithoutPay(Auth::user(), $order));
            Mail::to($order->buyer->email)->queue(new OrderWithoutPayBuyer(Auth::user(), $order));  
          } catch(\Exception $e){
            $error = true;
            $this->loadCart();
          }
      }

      $cart = AppOrgCartUser::find(Auth::user()->cart->id);
      $cart->status = "Complete";
      $cart->save();

      $message = 'Pedido pendiente de pago';
      return Redirect('/account/purchases/view/'.$order->order_identification)->with('message_confirm',$message)->with('type_msg','success');

    }

    $message = 'Ocurrio un error, contactese con el administrador';
    return Redirect('/account/purchases/view/'.$order->order_identification)->with('message_confirm',$message)->with('type_msg','success');

  }



  public function storeCompleteCheckout() {
    $input = Request::all();
    //dd($input);
    
    $order = AppOrgOrder::where('order_identification', $input['id_order'])->first();
    $createdOn = date('Y-m-d H:i:s');
    $us=Auth::user();
    $seller_id = $order->seller_user_id;
    $orderTotal = $order->total;
    $orderShipping = $order->shipping_price;
    $subT = $orderTotal - $orderShipping;

    $error = false;

    if($us->cash + $us->special_cash >= $orderTotal){
      

        $totalpayment = floatval($orderTotal);

         

        if($us->special_cash > 0) {
          if($us->special_cash > $totalpayment) {
            $us->special_cash -= $totalpayment;
            $order->special_cash = $totalpayment;
            $totalpayment = 0;
          } else {
            $totalpayment -= $us->special_cash;
            $order->special_cash = $us->special_cash;
            $us->special_cash = 0;
          }
        }
        
        if($totalpayment > 0) {
          $us->cash -= $totalpayment;
          $totalpayment = 0;            
        }

        $orderToken = $order->order_identification;
        $order->paid_out = 'Y';
        $order->buyer_read = 'N';
        $order->seller_read = 'N';
        $order->status = 'CR'; //saveordercr
        $order->save();
        $this->loadCart();
        try {
          $payment = json_encode(array('date' => $order->created_on, 'order_id' => $order->order_identification, 'total' => $order->total, 'cantidad' => $order->quantity, 'type_payment' => 'Cash'));
          $orderPayment = new AppOrgOrderPayment([
            'payment_result' => $payment,
          ]);
          $order->payment()->save($orderPayment);
        } catch (Exception $ex) {
            $error = true;
        }

        $detail = AppOrgOrderDetail::where('order_id', $order->id)->get();
        foreach($detail as $key) {
          $key->status = 'PD';
          $key->save();
        }

        PtyCommons::setUserEvent(Auth::id(), 'Pago saldo personal en cuenta RGM el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP(), 'order_id' => $orderToken]);
        
        foreach (AppOrgOrderDetail::where('order_id', $order->id)->get() as $detail) {
          $key = AppOrgUserInventory::where('id', $detail->inventory_id)->first();
          $feedback = new AppOrgProductFeedback();
          $feedback->box_condition = $key->box_condition;
          $feedback->manual_condition = $key->manual_condition;
          $feedback->cover_condition = $key->cover_condition;
          $feedback->game_condition = $key->game_condition;
          $feedback->extra_condition = $key->extra_condition;
          $feedback->inside_condition = $key->inside_condition;
          $feedback->ean = $key->ean;
          $feedback->inventory_id = $key->id;

          $feedback->seller_user_id = $key->user_id; //vendedor inventario

          $feedback->buyer_user_id = $order->buyer_user_id; //comprador

          $feedback->order_id = $detail->order_id; //vendedor

          $feedback->product_id = $key->product_id;
          $feedback->price = $key->price;
          $feedback->created_at = $detail->updated_at;
          $feedback->updated_at = $detail->updated_at;
          $feedback->save();
        }
      try {
        //OrderPaymentComplete
        Mail::to($order->seller->email)->queue(new OrderPaymentComplete(Auth::user(), $order));
        Mail::to($order->buyer->email)->queue(new OrderPaymentBuyer(Auth::user(), $order));
      } catch (\Exception $ex) {
        $error = true;
      }

      return redirect('/account/purchases/view/'.$order->order_identification )->with('type_msg','danger')->with([
        'flash_class'     => 'alert-success',
        'flash_message'   => 'El pago se realizo con éxito.',
        'flash_important' => true,
    ]);
    }

 
    return redirect('/account/purchases/view/'.$order->order_identification )->with('type_msg','danger')->with([
      'flash_class'     => 'alert-danger',
      'flash_message'   => 'Saldo insuficiente.',
      'flash_important' => true,
  ]);

  }

  /**
   * Show the view for the contact us page
   *
   * @return view
   */
  public function showContactUs() {
    $this->loadCart();
    return view('brcode.front.public.contact')->with('viewData',$this->viewData);
  }

  /**
   * Process the contact us form
   *
   * @return json
   */
  public function sendContactUs(Request $request) {

    $rules = [
			'captcha'                        => 'required|captcha',
			'contact_name'                   => 'required',
			'contact_email'                  => 'required',
			'contact_subject'                => 'required',
			'contact_message'                => 'required',
		];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {
      // error_log(env('CONTACT_MAIL'));
      Mail::to('marketplace790@gmail.com')->queue(new ContactForm($input));

			return redirect('/contact-us')->with('contact_form_message',__('Mensaje enviado satisfactoriamente'));
    }
  }

  

  /*
	|--------------------------------------------------------------------------
	| Personal and company registration
	|--------------------------------------------------------------------------
	|
	*/
  /**
   * Show the view for the company registration
   *
   * @return view
   */
  public function showRegisterCompany() {

    if( Auth::id() > 0 )
      return redirect('/');

    $this->loadCart();

    return view('brcode.front.public.register_company')->with('viewData', $this->viewData);

  }

  /**
   * Process the register account
   *
   * @return json
   */
  public function registerAccount(Request $request) {
    $referred = false;
    $referred_user = 0;
    $random = (string) random_int(10000, 99999);
    $input = Request::all();

    
    $rules = [
      'captcha'                        =>  'required|captcha',
      'register_email'                  => 'required|unique:sysUsers,email',
      'register_first_name'             => 'required|min:3',
      'register_last_name'              => 'required|min:3',
      'register_user_name'              => 'required|unique:sysUsers,user_name|alpha_num|min:4|max:10',
      'register_address'                => 'required',
      'register_zipcode'                => 'required',
      'register_city'                   => 'required',
      'register_country'                => 'required',
      'register_password'               => 'required|min:8',
    ];

    $messages = array(
      'register_email.required' => 'Email Obligatorio',
      'register_first_name.required' => 'Nombre Obligatorio',
      'register_last_name.required' => 'Apellido Obligatorio',
      'register_user_name.required' => 'Nombre de Usuario Obligatorio',
      'register_address.required' => 'Dirección Obligatoria',
      'register_zipcode.required' => 'Código postal  Obligatorio',
      'register_city.required' => 'Ciudad Obligatorio',
      'register_country.required' => 'País Obligatorio',
      'register_password.required' => 'Email Obligatorio',

      'register_email.unique' => 'El correo electrónico ya existe en nuestra base de datos',
      'register_user_name.unique' => 'El email ya existe en nuestra base de datos',
      'register_user_name.alpha_num' => 'El nombre de usuario solo puede contener letras y números',
      'register_user_name.min' => 'El nombre de usuario debe contener min. 4 caracteres',
      'register_user_name.max' => 'El nombre de usuario debe contener max. 10 caracteres',
      'register_password.min' => 'La contraseña debe contener min. 8 caracteres',
      'register_first_name.min' => 'El nombre debe contener min. 3 caracteres',
      'register_last_name.min' => 'El apellido debe contener min. 3 caracteres',
    );

    $v = Validator::make($input, $rules, $messages);

    

    if(isset($input["register_referred"])){
      // error_log($input["register_referred"]);
      if(SysUser::where('user_name', $input["register_referred"])->first()){
        $referred_user = SysUser::where('user_name', $input["register_referred"])->first()->id;
      } else {
        $referred = true;
      }
    }
    
    if($v->fails() || $referred) {
      if($v->fails()){
        return response()->json(['error' => 1, 'errors' => $v->errors()]);
      }elseif($referred){
        return response()->json(['error' => 1, 'errors' => ['user' => ['El nombre de usuario del recomendado no fue encontrado']]]);
      }
    } else {

      $country = SysCountry::find($input['register_country']);
      $userName = $input['register_user_name'];
      $newUser = new SysUser;
      $newUser->first_name    = $input['register_first_name'];
      $newUser->last_name     = $input['register_last_name'];
      $newUser->user_name     = $userName;
      $newUser->email         = $input['register_email'];
      $newUser->password      = Hash::make($input['register_password']);
      $newUser->country_id    = $input['register_country'];
      $newUser->country_code  = $country->alpha_2;
      $newUser->city          = $input['register_city'];
      $newUser->zipcode       = $input['register_zipcode'];
      $newUser->address       = $input['register_address'];
      $newUser->concept       = $userName . '-' . $random;
      $newUser->save();

      $newUserRol = new SysUserRoles;
      $newUserRol->user_id    = $newUser->id;
      $newUserRol->role_id    = 4;
      $newUserRol->save();

      if($referred_user > 0){
        $newUserReferred = new SysUserReferred;
        $newUserReferred->master_id    = $referred_user;
        $newUserReferred->son_id    = $newUser->id;
        $newUserReferred->save();
      }

      $newAccountBank = new SysAccountBank;
      $newAccountBank->user_id = $newUser->id;
      $newAccountBank->beneficiary= $newUser->first_name . ' ' . $newUser->last_name ;
      $newAccountBank->save();
      
      $actRequest = new SysUserAccountActivation();

      $actRequest->email = $newUser->email;
      $actRequest->token = str_random(100);
      $actRequest->save();

      PtyCommons::setUserEvent($newUser->id, 'Cuenta creada el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP()]);
      try {
        //code...
        Mail::to($newUser->email)->queue(new ActivateAccount($actRequest->token, $newUser->first_name . ' ' . $newUser->last_name));
      } catch (\Exception  $th) {
        Log::info($th);
        //throw $th;
      }

      return response()->json(['error' => 0]);

    }
  }

  public function reloadCaptcha()
  {
      return response()->json(['captcha'=> captcha_img()]);
  }
  /**
   * Process the company register account
   *
   * @return json
   */
  public function registerCompanyAccount(Request $request) {

    $rules = [
      'register_email'                  => 'required|unique:sysUsers,email',
      'register_first_name'             => 'required',
      'register_last_name'              => 'required',
      'register_user_name'              => 'required|unique:sysUsers,user_name',
      'register_address'                => 'required',
      'register_zipcode'                => 'required',
      'register_city'                   => 'required',
      'register_country'                => 'required',
      'register_password'               => 'required|min:8',
      'register_vat'                    => 'required',
      'register_name'                   => 'required',
      'register_phone'                  => 'required',
      'captcha'                         => 'required|captcha',
    ];

    $input = Request::all();

    $v = Validator::make($input, $rules);

    if($v->fails()) {
      return redirect('/register-company')->with([
        'flash_class'     => 'alert-danger',
        'flash_message'   => $v->errors(),
        'flash_important' => true,
    ]);
    }
    else {

      $country = SysCountry::find($input['register_country']);

      $newUser = new SysUser;
      $newUser->first_name    = $input['register_first_name'];
      $newUser->last_name     = $input['register_last_name'];
      $newUser->user_name     = $input['register_user_name'];
      $newUser->email         = $input['register_email'];
      $newUser->password      = Hash::make($input['register_password']);
      $newUser->country_id    = $input['register_country'];
      $newUser->country_code  = $country->alpha_2;
      $newUser->city          = $input['register_city'];
      $newUser->zipcode       = $input['register_zipcode'];
      $newUser->address       = $input['register_address'];
      $newUser->is_company    = 'Y';
      $newUser->is_enabled    = 'N';
      $newUser->company_name  = $input['register_name'];
      $newUser->company_vat   = $input['register_vat'];
      $newUser->phone         = $input['register_phone'];
      
      $newUser->save();

      $newUserRol = new SysUserRoles;
      $newUserRol->user_id = $newUser->id;
      $newUserRol->role_id = 3;

      $newUserRol->save();

      $actRequest = new SysUserAccountActivation();

      $actRequest->email = $newUser->email;
      $actRequest->token = str_random(100);
      $actRequest->save();

      PtyCommons::setUserEvent($newUser->id, 'Cuenta creada el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP()]);

      Mail::to($newUser->email)->queue(new ActivateAccount($actRequest->token, $newUser->first_name . ' ' . $newUser->last_name));

      return redirect('/register-company')->with([
        'flash_class'     => 'alert-success',
        'flash_message'   => 'Cuenta registrada satisfactoriamente.<br/>Ahora es necesario que revises tu correo (El que registraste) para completar la activación de tu cuenta.',
        'flash_important' => true,
    ]);

    }
  }

  public function showAccountActivated() {

    if( ! session()->pull('allow_account_activated') ) {
      abort(404);
    }
    return back()->with('message_confirm','Cuenta activada!')->with('type_msg','success');

    $this->loadCart();
    return view('brcode.front.public.account_activated')->with('viewData', $this->viewData);
  }

  public function showTransactions(){
    $this->loadCart();
    $userId = Auth::id();
    $dates = AppOrgOrder::where('buyer_user_id', Auth::id())->orWhere('seller_user_id', Auth::id())->orderBy('updated_at')
    ->get([
        DB::raw('DATE_FORMAT(updated_at, "%m / %Y" ) AS `Dates`'),
    ]);
    $this->viewData['dates'] = $dates->unique('Dates');

    $this->viewData['tipos'] = AppOrgOrder::where('status', '!=', 'CS')->select(DB::raw("IF(`buyer_user_id`='$userId', CONCAT('C', `status`), CONCAT('V', `status`)) as status_type"), "status")->where('buyer_user_id', Auth::id())->orWhere('seller_user_id', Auth::id())->get()->unique('status_type');
    
  $this->viewData['trans'] = AppOrgOrder::where('paid_out', 'Y')->where(function ($query) {
     $query->where('buyer_user_id', Auth::id())
     ->orWhere('seller_user_id', Auth::id());
  })->whereIn('status', ['DD','RE','RF','FD','CE','AC','RC','CR','ST','EC','SC','PV'])
  ->select("*", DB::raw("IF(`buyer_user_id`='$userId', CONCAT('C', `status`), CONCAT('V', `status`)) as status_type"))
  ->having('status_type', '!=', "VEC")
  ->orderBy('updated_at', 'desc')->get();  
    
    return view('brcode.front.private.transactions_view')->with('viewData', $this->viewData);
  }

  public function getTransactions(BaseRequest $request){
    $this->loadCart();
    
    $userId = Auth::id();
    $dates = AppOrgOrder::where('buyer_user_id', Auth::id())->orWhere('seller_user_id', Auth::id())->orderBy('updated_at')
    ->get([
        DB::raw('DATE_FORMAT(updated_at, "%m / %Y" ) AS `Dates`'),
    ]);
    $this->viewData['dates'] = $dates->unique('Dates');

    $this->viewData['tipos'] = AppOrgOrder::select(DB::raw("IF(`buyer_user_id`='$userId', CONCAT('C', `status`), CONCAT('V', `status`)) as status_type"), "status")->where('buyer_user_id', Auth::id())->orWhere('seller_user_id', Auth::id())->get()->unique('status_type');

    $year = substr($request['d1'], 3, 7);
    $month = substr($request['d1'], 0, 2);
    
    $type_des = substr($request['d2'], 0, 1);
    $type = substr($request['d2'], 1, 2);
    if($request['d3']){
      if($request['d1'] && $request['d2']){
        $this->viewData['resumen'] = AppOrgOrder::
        where(function ($query) use ($userId, $month, $year, $type, $type_des) {
          if($type_des == 'C' && $type == 'DD')
            $query->where('paid_out', 'Y')->where('buyer_user_id', $userId)->whereYear('updated_at', '=', $year)->whereMonth('updated_at', '=', $month)->whereIn('status', ['DD', 'ST', 'CR']);
          if($type_des == 'C' && $type != 'DD')
            $query->where('paid_out', 'Y')->where('buyer_user_id', $userId)->whereYear('updated_at', '=', $year)->whereMonth('updated_at', '=', $month)->where('status', '=', $type);
          
        })->orWhere(function ($query) use ($userId, $month, $year, $type, $type_des) {
          
          if($type_des == 'V' && $type == 'DD')
            $query->where('paid_out', 'Y')->where('seller_user_id', $userId)->whereYear('updated_at', '=', $year)->whereMonth('updated_at', '=', $month)->whereIn('status', ['DD', 'ST', 'CR']);
          if($type_des == 'V' && $type != 'DD')
            $query->where('paid_out', 'Y')->where('seller_user_id', $userId)->whereYear('updated_at', '=', $year)->whereMonth('updated_at', '=', $month)->where('status', '=', $type);
            
          
        })
        ->select(DB::raw("IF(`buyer_user_id`='$userId', CONCAT('C', `status`), CONCAT('V', `status`)) as status_type"), "status", "total", "buyer_user_id", "seller_user_id","updated_at")
        ->groupBy('status_type')
        ->selectRaw('sum(total) as sum')
        ->get();
        
      }elseif($request['d1']){
       
        $this->viewData['resumen'] = AppOrgOrder::where('paid_out', 'Y')->where(function ($query) use ($userId, $month, $year, $type, $type_des) {
          $query->where('buyer_user_id', Auth::id())->whereYear('updated_at', '=', $year)->whereMonth('updated_at', '=', $month)
                ->orWhere('seller_user_id', Auth::id())->whereYear('updated_at', '=', $year)->whereMonth('updated_at', '=', $month);
        })->whereIn('status', ['DD','RE','RF','FD','CE','AC','RC','CR','ST','EC'])
        ->select("*", DB::raw("IF(`buyer_user_id`='$userId', CONCAT('C', `status`), CONCAT('V', `status`)) as status_type"))
        ->get();

      }elseif($request['d2']){
        $this->viewData['resumen'] = AppOrgOrder::
        select(DB::raw("IF(`buyer_user_id`='$userId', CONCAT('C', `status`), CONCAT('V', `status`)) as status_type"), "status", "total", "buyer_user_id", "seller_user_id","updated_at")
        ->where(function ($query) use ($userId, $month, $year, $type, $type_des) {
          if($type_des == 'C' && $type == 'DD') 
            $query->where('paid_out', 'Y')->where('buyer_user_id', $userId)->whereIn('status', ['DD', 'ST', 'CR']);
          if($type_des == 'C' && $type != 'DD')
            $query->where('paid_out', 'Y')->where('buyer_user_id', $userId)->where('status', '=', $type);
        })->orWhere(function ($query) use ($userId, $month, $year, $type, $type_des) {
          if($type_des == 'V'  && $type == 'DD' )
            $query->where('paid_out', 'Y')->where('seller_user_id', $userId)->whereIn('status', ['DD', 'ST', 'CR']);  
          if($type_des == 'V' && $type != 'DD')
            $query->where('paid_out', 'Y')->where('seller_user_id', $userId)->where('status', '=', $type);
        })
        ->groupBy('status_type')
        ->selectRaw('sum(total) as sum')
        ->get();
        
      }else{
        $this->viewData['resumen'] = AppOrgOrder::where('paid_out', 'Y')
        ->whereIn('status', ['DD','RE','RF','FD','CE','AC','RC','CR','ST'])
        ->select(DB::raw("IF(`buyer_user_id`='$userId', CONCAT('C', `status`), CONCAT('V', `status`)) as status_type"), "status", "total", "buyer_user_id", "seller_user_id")
        ->where(function ($query) {
          $query->where('buyer_user_id', Auth::id())
                ->orWhere('seller_user_id', Auth::id());
         })
        ->groupBy('status_type')
        ->selectRaw('sum(total) as sum')
        ->get();
      }
      $this->viewData['d3'] = true;

    }else{
      if($request['d1'] && $request['d2'] ){
          $this->viewData['trans'] = AppOrgOrder::
          where(function ($query) use ($userId, $month, $year, $type, $type_des) {
            if($type_des == 'C'){
              if($type == 'DD'){
                $query->where('paid_out', 'Y')->where('buyer_user_id', $userId)->whereYear('updated_at', '=', $year)->whereMonth('updated_at', '=', $month)->whereIn('status', ['DD', 'ST', 'CR']);
              }else{
                $query->where('paid_out', 'Y')->where('buyer_user_id', $userId)->whereYear('updated_at', '=', $year)->whereMonth('updated_at', '=', $month)->where('status', '=', $type);
              }
            }
          })->orWhere(function ($query) use ($userId, $month, $year, $type, $type_des) {
            if($type_des == 'V'){
              if($type == 'DD'){
                $query->where('paid_out', 'Y')->where('seller_user_id', $userId)->whereYear('updated_at', '=', $year)->whereMonth('updated_at', '=', $month)->whereIn('status', ['DD', 'ST', 'CR']);
                //$query->where('paid_out', 'Y')->where('seller_user_id', $userId)->whereIn('status', '=', ['DD', 'ST', 'CR']);  
              }else{
                $query->where('paid_out', 'Y')->where('seller_user_id', $userId)->whereYear('updated_at', '=', $year)->whereMonth('updated_at', '=', $month)->where('status', '=', $type);
                //$query->where('paid_out', 'Y')->where('seller_user_id', $userId)->where('status', '=', $type);
              }
            }
          })
          ->select("*", DB::raw("IF(`buyer_user_id`='$userId', CONCAT('C', `status`), CONCAT('V', `status`)) as status_type"))
          ->get();
      }elseif($request['d1']){
        $this->viewData['trans'] = AppOrgOrder::where('paid_out', 'Y')->where(function ($query) use ($userId, $month, $year, $type, $type_des) {
          $query->where('buyer_user_id', Auth::id())->whereYear('updated_at', '=', $year)->whereMonth('updated_at', '=', $month)
                ->orWhere('seller_user_id', Auth::id())->whereYear('updated_at', '=', $year)->whereMonth('updated_at', '=', $month);
        })->whereIn('status', ['DD','RE','RF','FD','CE','AC','RC','CR','ST','EC'])
        ->select("*", DB::raw("IF(`buyer_user_id`='$userId', CONCAT('C', `status`), CONCAT('V', `status`)) as status_type"))
        ->get();
      }elseif($request['d2']){
        $this->viewData['trans'] = AppOrgOrder::
        where(function ($query) use ($userId, $month, $year, $type, $type_des) {
          if($type_des == 'C' && $type == 'DD') 
            $query->where('paid_out', 'Y')->where('buyer_user_id', $userId)->whereIn('status', ['DD', 'ST', 'CR']);
          if($type_des == 'C' && $type != 'DD')
            $query->where('paid_out', 'Y')->where('buyer_user_id', $userId)->where('status', '=', $type);

        })->orWhere(function ($query) use ($userId, $month, $year, $type, $type_des) {
          if($type_des == 'V'  && $type == 'DD' )
            $query->where('paid_out', 'Y')->where('seller_user_id', $userId)->whereIn('status', ['DD', 'ST', 'CR']);  
          if($type_des == 'V' && $type != 'DD')
            $query->where('paid_out', 'Y')->where('seller_user_id', $userId)->where('status', '=', $type);          
        })
        ->select("*", DB::raw("IF(`buyer_user_id`='$userId', CONCAT('C', `status`), CONCAT('V', `status`)) as status_type"))
        ->get();
      }else{
        return redirect('/account/transactions')->with([
          'flash_class'     => 'alert-warning',
          'flash_message'   => 'No has seleccionado ninguna de las opciones',
          'flash_important' => true,
       ]);
      }
      $this->viewData['d3'] = false;
    }

    $this->viewData['d1'] = $request['d1'];
    $this->viewData['d2'] = $request['d2'];
    return view('brcode.front.private.transactions_view')->with('viewData', $this->viewData);
  }

  public function generatePDF($id = '') { 
    //// error_log($id);
    //// error_log(Request::root() . '/assets/images/favicon.png');

    $year = substr($id, 3, 7);
    $month = substr($id, 0, 2);

    // error_log($year);
    // error_log($month);
    $userId = Auth::id();
    $resumen = AppOrgOrder::where(function ($query) use ($userId, $month, $year) {
      $query->where('buyer_user_id', $userId)->where('status', '=', 'FD')->whereYear('updated_at', '=', $year)->whereMonth('updated_at', '=', $month);
    })->get();

    dd($resumen);

    $total = AppOrgOrder::where(function ($query) use ($userId, $month, $year) {
      $query->where('buyer_user_id', $userId)->where('status', '=', 'FD')->whereYear('updated_at', '=', $year)->whereMonth('updated_at', '=', $month);
    })->sum('total');

    $data = [
      'title' => 'Welcome to HDTuto.com',
      'url' => Request::root(),
      'resumen' => $resumen,
      'total' => $total,
      'date' => $month . '/'.$year,
    
    ];
    $html = view('PDF',$data)->render();
    $namefile = 'boleta_de_venta_'.time().'.pdf';
    $mpdf = new Mpdf([
    
      'default_font' => 'Verdana, Arial, sans-serif',
      "format" => "A4",
    ]);
  
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->WriteHTML($html);
    $mpdf->Output($namefile,"D");
    //$pdf = PDF::loadView('PDF', $data);

    //return $pdf->download('bill' . $id . '.pdf');
  }

  public function showOrderStatus($order_id = 0) {
    if($order_id <= 0 ) {
      return redirect('/');
    }
    //return redirect('/account/inventory/add/' . $id);
    $order = AppOrgOrder::where('order_identification', $order_id)->first();
    if($order->seller->id == Auth::id()){
      if($order->seller_read == 'N'){
        $order->seller_read = 'Y';
        $order->save();
      }
      return redirect('/account/sales/view/'.$order_id);
    }
    if($order->buyer->id == Auth::id()){
      if($order->buyer_read == 'N'){
        $order->buyer_read = 'Y';
        $order->save();
      }
      return redirect('/account/purchases/view/'.$order_id);
    }
    return redirect('/');
  }

  public function showPublicFaqs($link_page) {
    $this->loadCart();
    // error_log($link_page);

    $this->viewData['category_page_info'] = $link_page;
    
 

    return view('brcode.front.public.faqs_view')->with('viewData', $this->viewData);
  }

  public function showOrderCancel($id = 0) {
    $this->loadCart();
    // error_log($id);

    //$this->viewData['category_page_info'] = $link_page;

    return view('brcode.front.private.purchase_cancel')->with('viewData', $this->viewData);
  }

  public function saveOrderCancel($id = 0, BaseRequest $request) {
    $this->loadCart();

    if($id == 0){
      return redirect('/account/profile');
    }

    $order = AppOrgOrder::where('order_identification', $id)->first();
    if($order->buyer_user_id == Auth::id() || $order->seller_user_id == Auth::id()){
      if(in_array($order->status, ['CR', 'RP'])){
        if($order->seller_user_id == Auth::id()){
          $order->status = 'CW';
          //dd($order->status);
          $order->save();
          $orderCancel = new AppOrgOrderCancel;
          $orderCancel->order_id = $order->id;
          $orderCancel->seller_user_id = Auth::id();
          $orderCancel->buyer_user_id = $order->buyer_user_id;
          $orderCancel->cause = $request['reason'];
          $orderCancel->solicited_by = 'S';
          $orderCancel->save();
          return redirect('/account/sales/view/'.$order->order_identification);
        }elseif($order->status == 'CR'){
          $order->status = 'PC';
          //dd($order->status);
          
          $order->save();
          //dd($order);
          $orderCancel = new AppOrgOrderCancel;
          $orderCancel->order_id = $order->id;
          $orderCancel->seller_user_id = $order->seller_user_id;
          $orderCancel->buyer_user_id = Auth::id();
          $orderCancel->cause = $request['reason'];
          $orderCancel->solicited_by = 'B';
          $orderCancel->save();

          return redirect('/account/purchases/view/'.$order->order_identification)->with([
            'flash_class'   => 'alert-warning',
            'flash_message' => 'Solicitud de cancelacion enviada.',
           ]);

        }elseif($order->status == 'RP'){
          $order->status = 'CW';
          //dd($order->status);
          $order->save();
          $orderCancel = new AppOrgOrderCancel;
          $orderCancel->order_id = $order->id;
          $orderCancel->seller_user_id = $order->seller_user_id;
          $orderCancel->buyer_user_id = Auth::id();
          $orderCancel->cause = $request['reason'];
          $orderCancel->solicited_by = 'B';
          $orderCancel->save();
          return redirect('/account/purchases/view/'.$order->order_identification)->with([
            'flash_class'   => 'alert-warning',
            'flash_message' => 'Solicitud de cancelacion enviada.',
           ]);
        }
      }elseif(in_array($order->status, ['CW', 'PC']))
      {
        //dd($order->status);
        if($order->cancelOrder->solicited_by == 'S'){
          // error_log('Seller petition');
          $rules=[
            'optradio' => 'required',
            'reason' => 'required|max:350',
          ];
          //updateAccountBank
          $this->validate($request, $rules);
          if($request['optradio'] == 'Y' || $request['optradio'] == 'N'){
            $orderCan = AppOrgOrderCancel::where('order_id', $order->id)->first();
            $orderCan->answer = $request['optradio'];
            $orderCan->reason = $request['reason'];
            $orderCan->save();
            if($request['optradio'] == 'Y'){
              $order->status = 'CN';
              $order->save();
              
              $order->status = 'CN';

              if($order->paid_out == 'Y'){
                $buyer = SysUser::where('id', $order->buyer_user_id)->first();
                $auxTotal = floatval($order->total);
                if($order->special_cash > 0){
                  $auxTotal -= $order->special_cash;
                  $buyer->special_cash += $order->special_cash;
                }
                $buyer->cash += floatval($auxTotal);
              }

              $detail = AppOrgOrderDetail::where('order_id', $order->id)->get();
              
              foreach($detail as $key){
                $inventory = AppOrgUserInventory::find($key->inventory_id);
                $inventory->quantity += $key->quantity;
                $inventory->save();
              }
              
            }elseif($request['optradio'] == 'N'){
              $order->status = 'CR';
              $order->save();
            }
          }
          return redirect('/account/purchases/view/'.$order->order_identification);

        }elseif($order->cancelOrder->solicited_by == 'B'){

          // error_log('Buyer petition');
          $rules=[
              'optradio' => 'required',
              'reason' => 'required|max:350',
          ];
          //updateAccountBank
          $this->validate($request, $rules);
          if($request['optradio'] == 'Y' || $request['optradio'] == 'N'){
            $orderCan = AppOrgOrderCancel::where('order_id', $order->id)->first();
            $orderCan->answer = $request['optradio'];
            $orderCan->reason = $request['reason'];
            $orderCan->save();
            if($request['optradio'] == 'Y'){
              $order->status = 'CN';
              $order->save();

              if($order->paid_out == 'Y'){

                $buyer = SysUser::where('id', $order->buyer_user_id)->first();
                $auxTotal = floatval($order->total);
                if($order->special_cash > 0){
                  $auxTotal -= $order->special_cash;
                  $buyer->special_cash += $order->special_cash;
                }
                $buyer->cash += floatval($auxTotal);
                
                // $buyer->save();
              }

              $detail = AppOrgOrderDetail::where('order_id', $order->id)->get();
              
              foreach($detail as $key){
                $inventory = AppOrgUserInventory::find($key->inventory_id);
                $inventory->quantity += $key->quantity;
                $inventory->save();
              }
              
            }elseif($request['optradio'] == 'N'){
              $order->status = 'CR';
              //dd($order->status);
              $order->save();
            }

          }
          return redirect('/account/sales/view/'.$order->order_identification);
        }

      }
        
    }else{
      return redirect('/account/profile');
    }
    //return view('brcode.front.private.purchase_cancel')->with('viewData', $this->viewData);
  }

  public function fixProduct() {

    /*
    $this->viewData['category_ids'] = strlen($catIds) > 0 ? explode(',',$catIds) : [];

    $this->viewData['platform'] = SysDictionary::where('code', 'GAME_PLATFORM')->get(['value_id', 'value']);
    
    $this->viewData['result'] = AppOrgProduct::whereNull('deleted_at');
    */
    foreach(AppOrgProduct::whereNull('deleted_at')->get() as $key){
      // error_log($key->platform);
      $dic = SysDictionary::where('value_id', $key->platform)->first();
      if($dic){
        $sysD = App\SysDictionary::where('id', $dic->id)->pluck('value')->toArray();
        // error_log(implode( ", ", $sysD ));

        $key->platform = implode( ", ", $sysD );
        $key->sysDictionaries()->sync([$dic->id]);
        $key->save();
        //// error_log($dic);

      }
    }

    return redirect('/account/profile');

  }

  public function showBlogs(){
    $this->loadCart();
    $blogs = App\AppOrgBlog::where('is_enabled', 'Y')->orderBy('created_at', 'DESC')->paginate(10);
    $topblogs = App\AppOrgBlog::where('is_enabled', 'Y')->where('is_top', 'Y')->orderBy('created_at', 'DESC')->take(5)->get();
    $imgs = AppOrgProductImage::groupBy('product_id')->pluck('product_id');
    $random = AppOrgProduct::whereIn('id', $imgs)
    ->whereNull('deleted_at')  
    ->inRandomOrder()
    ->take(5)
    ->get();
    
    return view('brcode.front.public.blogs_view', compact('blogs','topblogs', 'random'))->with('viewData', $this->viewData);
  }
  public function SingleBlog(BaseRequest $request){
    $this->loadCart();
    $blog = $request->blog;
    if(!App\AppOrgBlog::where('id', $blog - 2000)->first()){
      abort(404);
    }

    $topblogs = App\AppOrgBlog::where('is_enabled', 'Y')->where('is_top', 'Y')->orderBy('created_at', 'DESC')->take(5)->get();
    $blog = App\AppOrgBlog::where('id', $blog - 2000)->first();
    $imgs = AppOrgProductImage::groupBy('product_id')->pluck('product_id');
    $random = AppOrgProduct::whereIn('id', $imgs)
    ->whereNull('deleted_at')  
    ->inRandomOrder()
    ->take(5)
    ->get();
    return view('brcode.front.public.blog_view', compact('blog','topblogs','random'))->with('viewData', $this->viewData);
  }

}
