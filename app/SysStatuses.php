<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysStatuses extends Model
{
    //
	protected $table = 'sysStatuses';

	public function scopeStatusFor($query, $status_for) {
		return $query->where('status_for','INB_IBL');
	}
}
