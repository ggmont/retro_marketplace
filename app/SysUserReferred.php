<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysUserReferred extends Model
{
    
    protected $table = 'sysUserReferreds';
    
    protected $fillable = [
        'master_id', 'son_id', 'user_especial'
    ];

}
