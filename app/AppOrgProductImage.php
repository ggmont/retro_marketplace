<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class AppOrgProductImage extends Model {
  protected $table = 'appOrgProductImages';

  protected $fillable = ['id', 'product_id', 'image_path', 'created_by', 'modified_by'];

  protected $hidden = ['created_at', 'created_by', 'updated_at', 'modified_by'];

  public static function markOtherAsDeleted($product_id, $ids) {

		DB::table('appOrgProductImages')
            ->where('product_id', $product_id)
            ->whereNotIn('id', $ids)
            ->delete();

  }

  public function img()
  {
      return $this->belongsTo('App\AppOrgProduct', 'product_id');
  }

}
