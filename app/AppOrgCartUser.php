<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\AppOrgCartShipping;
use App\SysShipping;
use App\AppOrgProduct;
use Auth;

class AppOrgCartUser extends Model
{
    protected $table = 'appOrgCartUsers';

    public function details()
    {
        return $this->hasMany('App\AppOrgCartDetailUser', 'cart_id');
    }

    public function shippings()
    {
        return $this->hasMany(AppOrgCartShipping::class);
    }

    public function getInventoryAttribute()
    {
        $inventories = [];
        foreach ($this->details as $detail) {
            array_push($inventories, AppOrgUserInventory::find($detail->inventory_id));
        }
        return $inventories;
    }

    public function getCartItemsAttribute()
    {
        $productsInCart = [];
        foreach (Auth::user()->cart->details as $key) {
            $inventory = AppOrgUserInventory::find($key->inventory_id);
            $inventory->qty = $key->quantity;
            $inventory->inventory_images = $inventory->images;
            $inventory->product_info = AppOrgProduct::find($inventory->product_id);
            $inventory->product_info->product_images = $inventory->product_info->images;
            array_push($productsInCart, $inventory);
        }
        return $productsInCart;
    }

    public function getShippingAttribute()
    {
        $shippings = [];

        $shippings_id = [];

        $users_orders = [];
        foreach (Auth::user()->cart->inventory as $key_i) {
            array_push($users_orders, $key_i->user);
        }

        $users = array_unique($users_orders);

        foreach ($users as $key) {
            $total_user = 0;
            $total_wei = 0;
            $total_vol = 0;

            foreach ($this->details as $detail) {
                $inventory = AppOrgUserInventory::find($detail->inventory_id);

                //dd($inventory);
                if ($key->user_name == $inventory->user->user_name) {
                    $total_user += ($inventory->price * $detail->quantity);

                    if($inventory->title !== null) {
                        $total_wei += $detail->quantity * $inventory->kg;
                        $total_vol += $detail->quantity * $inventory->size;
                    } else {
                        $total_wei += $detail->quantity * $inventory->product->weight;
                        $total_vol += $detail->quantity * $inventory->product->volume;
                    }
                }
            }

            $sys_shipping = SysShipping::orderBy('price')
                ->where('from_id', $key->country_id)
                ->where('to_id', Auth::user()->country_id)
                ->where('max_value', '>=', $total_user)
                ->where('max_weight', '>=', $total_wei)
                ->first();

            if ($sys_shipping) {

                //dd($sys_shipping);
                if (AppOrgCartShipping::where('cart_id', Auth::user()->cart->id)->where('user_id', $key->id)->first()) {
                    $shipping = AppOrgCartShipping::where('cart_id', Auth::user()->cart->id)->where('user_id', $key->id)->first();
                    //dd($shipping);
                    if ($shipping) {

                        $shipping->shipping_id = $sys_shipping->id;
                        $shipping->save();
                    }
                    array_push($shippings_id, $shipping->id);
                } else {

                    //dd('a');
                    if ($sys_shipping) {
                        $shipping = new AppOrgCartShipping;
                        $shipping->cart_id = Auth::user()->cart->id;
                        $shipping->user_id = $key->id;
                        $shipping->shipping_id = $sys_shipping->id;
                        $shipping->save();
                        array_push($shippings_id, $shipping->id);
                    } else {
                        $shipping = new AppOrgCartShipping;
                        $shipping->cart_id = Auth::user()->cart->id;
                        $shipping->user_id = $key->id;
                        $shipping->shipping_id = 46;
                        $shipping->save();
                        array_push($shippings_id, $shipping->id);
                    }
                }
            } else {
                $shipping = new AppOrgCartShipping;
                $shipping->cart_id = Auth::user()->cart->id;
                $shipping->user_id = $key->id;
                $shipping->shipping_id = 46;
                $shipping->save();
                array_push($shippings_id, $shipping->id);
            }



            // Calculo del sistema

            //error_log($shipping->id);
        }
        //dd($shippings_id);

        if (AppOrgCartShipping::where('cart_id', Auth::user()->cart->id)->whereNotIn('id', $shippings_id)->get()) {
            foreach (AppOrgCartShipping::where('cart_id', Auth::user()->cart->id)->whereNotIn('id', $shippings_id)->get() as $Shippings) {
                $Shippings->delete();
            }
        }

        ///dd($shippings_id);
        return AppOrgCartShipping::whereIn('id', $shippings_id)->get();
    }

    public function getTotalShippingAttribute()
    {

        $total = 0;
        //error_log(Auth::user()->cart->shipping);
        foreach ($this->shipping as $key) {
            //error_log($key->shipping->price);
            $total += $key->shipping->price;
        }
        return $total;
    }

    public function getTotalIvaAttribute()
    {
        $total = 0;
    
        foreach ($this->details as $detail) {
            $inventory = AppOrgUserInventory::find($detail->inventory_id);
            $total += $detail->quantity * $inventory->price;
        }
    
        $iva = $total * 0.04; // Calcula el 4% de IVA
    
        return $iva;
    }

    public function getTotalCartAttribute()
    {
        $total = 0;
        foreach ($this->details as $detail){
            $inventory = AppOrgUserInventory::find($detail->inventory_id);
            $total += $detail->quantity * $inventory->price;
        }
        return $total;
    }
    

    public function getTotalSendAttribute()
    {
        $total = 0;
        $totalWeight = 0;
        $totalSize = 0;
    
        foreach ($this->details as $detail) {
            $inventory = AppOrgUserInventory::find($detail->inventory_id);
            $total += $detail->quantity * $inventory->price;
            $totalWeight += $detail->quantity * ($inventory->product->weight / 1000); // Convertir de gramos a kilogramos
            $totalSize += $detail->quantity * ($inventory->product->width + $inventory->product->large + $inventory->product->high);
        }
    
        // Validación del costo de envío según el peso y las dimensiones del paquete
        if ($totalWeight >= 0 && $totalWeight <= 1 && $totalSize >= 0 && $totalSize <= 50) {
            return 2.90;
        } elseif ($totalWeight == 5 && $totalSize == 95) {
            return 4.90;
        } elseif ($totalWeight == 10 && $totalSize == 120) {
            return 6.90;
        } elseif ($totalWeight == 20 && $totalSize == 150) {
            return 10.90;
        } else {
            // Costo de envío por defecto si no se cumple ninguna de las validaciones anteriores
            return 0.00;
        }
    }
    

    public function getSellerAttribute()
    {
        $users_orders = [];
        foreach (Auth::user()->cart->inventory as $key_i) {
            array_push($users_orders, $key_i->user->id);
        }
        $users = array_unique($users_orders);

        return $users;
    }
}
