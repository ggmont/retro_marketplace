<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class AppOrgOrder extends Model
{
  use SoftDeletes;

  protected $table = 'appOrgOrders';

  protected $fillable = [
    'order_identification', 'seller_user_id', 'buyer_user_id', 'shipping_id', 'quantity',
    'status', 'is_enabled', 'special_promotion', 'paid_out','email_sent'
  ];

  protected $casts = [
    'status_type' => 'string',
    'sum' => 'float'
  ];

  private $orderStatus = [

    'CP'  =>  'Saldo pendiente por transferir', // saldo por enviar al usuario not
    'CE'  =>  'Saldo retirado', // salida -
    'AC'  =>  'Saldo agregado por el administrador', //Saldo ingresado por admin ingreso +
    'RC'  =>  'Saldo retirado por el administrador',
    'SC'  =>  'Saldo especial agregado por el administrador',
    'PV'  =>  'Saldo Promocional RGM',
    'AS'  =>  'Abono de saldo especial por mes del 20%',
    'YC'  =>  'Abono de saldo especial por compra del 10%',
    'YV'  =>  'Abono de saldo especial por venta del 10%',

    'RE'  =>  'Pagos extra', //buyer
    'RF'  =>  'Reembolsos',  // seller

    'AE'  =>  'Pagos extra', //buyer
    'AF'  =>  'Reembolsos',  // seller

    'CS'  =>  'Gastos de envió', // buyer -
    'CR'  =>  'Pedido pendiente de envío', // venta seller + buyer -
    'DD'  =>  'Entregado', // venta seller + buyer -
    'PD'  =>  'Pendiente de envío', // venta seller + buyer -
    'ST'  =>  'Enviado', // venta seller + buyer -

    'FD'  =>  'Comisión RetroGamingMarket',
    'EC'  =>  'Saldo especial programa de referidos',
    'PV'  =>  'Promoción 5€ en la primera venta', // venta seller + buyer -

    'OL'  =>  'Pedido perdido', // not
    'ON'  =>  'Pedido no recibido', // not

    'CN'  =>  'Pedido cancelado', // not
    'CW'  =>  'Solicitud de cancelación de pedido', // not
    'PC'  =>  'Solicitud de cancelación de un pedido pagado', // venta seller + buyer -
    'RT'  =>  'Pedido devuelto', // not
    'RP'  =>  'Pedido sin pagar', // not
  ];


  /*

    'CP'  =>  'Saldo pendiente por transferir', // saldo por enviar al usuario not
    'CE'  =>  'Saldo retirado', // salida -
    'AC'  =>  'Saldo agregado por el administrador', //Saldo ingresado por admin ingreso +
    'RC'  =>  'Saldo retirado por el administrador',

    'CS'  =>  'Gastos de envió', // buyer -
    'CR'  =>  'Pedido pendiente de envío', // venta seller + buyer -
    'PD'  =>  'Pendiente de envío', // venta seller + buyer -
    'ST'  =>  'Enviado', // venta seller + buyer -

    'FD'  =>  'Comisión RetroGamingMarket',

    'LT'  =>  'Pedido perdido', // not
    'CN'  =>  'Pedido cancelado', // not
    'CW'  =>  'Solicitud de cancelación de pedido', // not
    'RT'  =>  'Pedido devuelto', // not
    'RP'  =>  'Pedido sin pagar', // not
    'DD'  =>  'Pedido Entregado'
    
   */

  private $status_resumen = [
    'CDD' => 'Compra de Productos',
    'VDD' => 'Venta de Productos',
    'CST' => 'Compra de Productos',
    'VST' => 'Venta de Productos',
    'CCR' => 'Compra de Productos',
    'VCR' => 'Pedido pendiente de envío',
    'CRE' => 'Pagos extra - compras',
    'VRE' => 'Pagos extras - ventas',
    'CRF' => 'Reembolsos - compras',
    'VRF' => 'Reembolsos - ventas',
    'CFD' => 'Comisión RetroGamingMarket',
    'CEC' => 'Saldo especial programa de referidos',
    'VEC' => 'Saldo especial programa de referidos',
    'CCE' => 'Saldo retirado',
    'VCN' => 'Pedido cancelado - vendedor',
    'CCN' => 'Pedido cancelado - comprador',
    'CAC' => 'Saldo agregado por RetroGamingMarket',
    'CAS' => 'Abono de saldo especial por mes del 20%',
    'VYC' =>  'Abono de saldo especial de 10% por compra del referido hijo',
    'CYV'  => 'Abono de saldo especial de 10% por venta del referido hijo',
    'CSC' => 'Saldo especial agregado por RetroGamingMarket',
    'CRC' => 'Saldo retirado por RetroGamingMarket',
    'VAS' => 'Abono de saldo especial por mes del 20%',
    'CYC' =>  'Abono de saldo especial de 10% por compra del referido hijo',
    'VYV'  => 'Abono de saldo especial de 10% por venta del referido hijo'
  ];

  private $orderStatus2 = [
    'CCP'  =>  'Saldo pendiente por transferir', // saldo por enviar al usuario not
    'CCE'  =>  'Saldo retirado', // salida -
    'CAC'  =>  'Saldo agregado por el administrador', //Saldo ingresado por admin ingreso +
    'CRC'  =>  'Saldo retirado por el administrador',
    'CCS'  =>  'Gastos de envió', // buyer -
    'CCR'  =>  'Comprador: Pedido pendiente de envío', // venta seller + buyer -
    'CPD'  =>  'Pendiente de envío', // venta seller + buyer -
    'CST'  =>  'Vendedor envio el pedido', // venta seller + buyer -
    'CDD'  =>  'Compras de Productos', // venta seller + buyer -
    'CTD'  =>  'Tax pagado',
    'CFD'  =>  'Comisión RetroGamingMarket',
    'CEC' => 'Saldo especial programa de referidos',
    'COL'  =>  'Pedido perdido', // not
    'CON'  =>  'Pedido no recibido', // not
    'CCN'  =>  'Pedido cancelado', // not
    'CCW'  =>  'Solicitud de cancelación de pedido', // not
    'CRT'  =>  'Pedido devuelto', // not
    'CRP'  =>  'Pedido sin pagar', // not
    'CRE'  =>  'Pagos extra en compras', //buyer
    'CRF'  =>  'Reembolsos en compras',  // seller
    'VRE'  =>  'Pagos extras en ventas', //buyer
    'VRF'  =>  'Reembolsos en ventas',  // seller
    'VCP'  =>  'Saldo pendiente por transferir', // saldo por enviar al usuario not
    'VCE'  =>  'Saldo retirado', // salida -
    'VAC'  =>  'Saldo agregado por el administrador', //Saldo ingresado por admin ingreso +
    'VRC'  =>  'Saldo retirado por el administrador',
    'VCS'  =>  'Gastos de envió', // buyer -
    'VCR'  =>  'Ventas: Pedido pendiente de envío', // venta seller + buyer -
    'VPD'  =>  'Pendiente de envío', // venta seller + buyer -
    'VST'  =>  'Pedidos enviados', // venta seller + buyer -
    'VDD'  =>  'Ventas de Productos', // venta seller + buyer -
    'VTD'  =>  'Tax pagado',
    'VFD'  =>  'Comisión RetroGamingMarket',
    'VOL'  =>  'Pedido perdido', // not
    'VON'  =>  'Pedido no recibido', // not
    'VCN'  =>  'Pedido cancelado', // not
    'VCW'  =>  'Solicitud de cancelación de pedido', // not
    'VRT'  =>  'Pedido devuelto', // not
    'VRP'  =>  'Pedido sin pagar' // not
  ];

  /*
   
   
   'CFD'  =>  'Comisión RetroGamingMarket',
   'CCE'  =>  'Saldo retirado', // salida -
   
   'VRF'  =>  'Rembolsos en ventas',  // seller
   'CRF'  =>  'Rembolsos en compras',  // seller
   
   'VRE'  =>  'Pagos extras en ventas', //buyer
   'CRE'  =>  'Pagos extra en compras', //buyer
   
   'VDD'  =>  'Ventas de Productos', // venta seller + buyer -
   'CDD'  =>  'Compras de Productos', // venta seller + buyer -

   'AC'  =>  
   'RC'  => 
   
   */

  private $orderSeller = [
    'CR'  =>  'Venta pendiente de envío', // venta seller + buyer -
    'PD'  =>  'Venta Pendiente de envío', // venta seller + buyer -
    'ST'  =>  'Venta enviada', // venta seller + buyer -
    'DD'  =>  'Venta de Productos', // venta seller + buyer -
  ];

  private $orderBuyer = [
    'CR'  =>  'Compra pendiente de envío', // venta seller + buyer -
    'PD'  =>  'Compra pendiente de envío', // venta seller + buyer -
    'ST'  =>  'Compra enviada', // venta seller + buyer -
    'DD'  =>  'Compra de Productos', // venta seller + buyer -
  ];

  public function details()
  {
    return $this->hasMany('App\AppOrgOrderDetail', 'order_id', 'id');
  }

  public function seller()
  {
    return $this->hasOne('App\SysUser', 'id', 'seller_user_id')->withTrashed();
  }

  public function buyer()
  {
    return $this->hasOne('App\SysUser', 'id', 'buyer_user_id')->withTrashed();
  }

  public function payment()
  {
    return $this->hasOne('App\AppOrgOrderPayment', 'order_id', 'id');
  }

  public function shippingSelect()
  {
    return $this->hasOne('App\SysShipping', 'id', 'shipping_id');
  }

  public function shippings()
  {
    return $this->hasOne('App\AppOrgOrderShipping', 'order_id', 'id');
  }

  public function country()
  {
    return $this->hasOne('App\SysCountry', 'country_id', 'id');
  }

  public function rating()
  {
    return $this->hasOne('App\AppOrgUserRating', 'order_id', 'id');
  }

  public function cancelOrder()
  {
    return $this->hasOne('App\AppOrgOrderCancel', 'order_id', 'id');
  }

  public function getRefundAttribute()
  {
    return AppOrgOrder::whereIn('status', ['RF'])->where('instructions', $this->order_identification)->first();
  }

  public function getExtraAttribute()
  {
    return AppOrgOrder::whereIn('status', ['RE'])->where('instructions', $this->order_identification)->first();
  }


  public function getStatusDesAttribute()
  {
    $value = $this->status;
    return isset($this->orderStatus[$value]) ? $this->orderStatus[$value] : $value;
  }

  public function getStatsAttribute()
  {
    $value = $this->status_type;
    return isset($this->status_resumen[$value]) ? $this->status_resumen[$value] : $value;
  }

  public function getTipoSellerAttribute()
  {
    if (Auth::id() == $this->seller_user_id)
      return true;
    else
      return false;
  }

  public function getDescriptionStatusAttribute()
  {
    $value = $this->status;
    if (Auth::id() == $this->seller_user_id)
      return isset($this->orderSeller[$value]) ? $this->orderSeller[$value] : (isset($this->orderStatus[$value]) ? $this->orderStatus[$value] : $value);
    else
      return isset($this->orderBuyer[$value]) ? $this->orderBuyer[$value] : (isset($this->orderStatus[$value]) ? $this->orderStatus[$value] : $value);
  }


  public function getStatusTipoAttribute()
  {
    return $this->status;
  }
}
