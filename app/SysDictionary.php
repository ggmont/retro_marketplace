<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\AppOrgProduct;
class SysDictionary extends Model {

  use SoftDeletes;

  protected $dates = ['deleted_at'];
  protected $table = 'sysDictionaries';

  protected $fillable = ['code','value_id','value','parent_id','is_enabled'];

  public function scopeOfCode($query, $code) {

    $query->where('code',$code);

    return $query;

  }

  public function appOrgProducts() {
    return $this->belongsToMany(AppOrgProduct::class, 'appOrgProduct_sysDictionary', 'dictionary_id', 'product_id');
  }


}
