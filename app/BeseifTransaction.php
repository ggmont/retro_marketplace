<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeseifTransaction extends Model
{
    protected $table = 'beseif_transaction';

    protected $fillable = ['id', 'action', 'id_transaccion', 'email_comprador', 'id_anuncio', 'signature'];
}
