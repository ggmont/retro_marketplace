<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysUserPasswordReset extends Model
{
    protected $table = 'sysUserPasswordResets';
}
