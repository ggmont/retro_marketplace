<?php

namespace App;

use DB;

use Illuminate\Database\Eloquent\Model;

class AppOrgUserInventoryEan extends Model
{
    protected $table = 'appOrgUserInventoryEans';

    public static function markOtherAsDeleted($inventory_id, $ids) {

		DB::table('appOrgUserInventoryEans')
            ->where('inventory_id', $inventory_id)
            ->whereNotIn('id', $ids)
            ->delete();

	}

  public function inventory() {
    return $this->hasOne('App\AppOrgUserInventory', 'id', 'inventory_id');
  }
    
}
