<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

class AppOrgOrderDetail extends Model {
  use SoftDeletes;

  private $orderProductStatus = [
    'CR'  =>  'Pendiente',
    'PD'  =>  'Pendiente',
    'ST'  =>  'Enviado',
    'DD'  =>  'Entregado',
    'LT'  =>  'Perdido',
    'CN'  =>  'Cancelado',
    'RT'  =>  'Devuelto',
  ];

  protected $fillable = ['type', 'seller_user_id', 'product_id', 'inventory_id', 'quantity',
    'price', 'tax', 'total', 'status', 'created_on'
  ];

  protected $table = 'appOrgOrderDetails';

  
  public function inventory() {
    return $this->hasOne('App\AppOrgUserInventory','id', 'inventory_id')->withTrashed();
  }

  public function order() {
    return $this->hasOne('App\AppOrgOrder','id', 'order_id');
  }

  public function user() {
    return $this->hasOne('App\SysUser', 'id', 'seller_user_id');
  }

  public function seller() {
    return $this->hasOne('App\SysUser','id', 'seller_user_id')->withTrashed();
  }

  public function buyer() {
    return $this->hasOne('App\SysUser','id', 'buyer_user_id')->withTrashed();
  }

  public function product() {
    return $this->hasOne('App\AppOrgProduct','id', 'product_id')->withTrashed();
  }

  public function getStatusAttribute($value) {
    return isset($this->orderProductStatus[$value]) ? $this->orderProductStatus[$value] : $value;
  }

  public function scopeOfMostSold($query, $limit) {

    return $query->select(['product_id','price','seller_user_id', DB::raw('sum(quantity) sum_quantity '), DB::raw('u.user_name')])
    ->join('sysUsers AS u','u.id','=','seller_user_id')
    ->take($limit)
    ->orderBy('sum_quantity', 'desc')
    ->groupBy('seller_user_id')
    ->where('type', '!=', 'CU');
  }

  public function scopeOfMostSoldPro($query, $limit) {
    //$val = $query->where('type', '!=', 'CU')->groupBy('product_id')->pluck('product_id');
    return $query->select("product_id",'price','seller_user_id', DB::Raw("SUM(quantity) AS quantity"))
    ->groupBy('product_id')
    ->orderBy('quantity', 'desc')
    ->take($limit);
  }

}
