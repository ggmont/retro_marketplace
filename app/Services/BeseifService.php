<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Traits\ConsumesExternalServices;

class BeseifService
{
    use ConsumesExternalServices;

    protected $baseUri;

    protected $key;

    protected $secret;

    protected $access_token;

    public function __construct()
    {
        //dd('prueba');
        $this->baseUri = config('services.beseif.base_uri');
        $this->key = config('services.beseif.client_id');
        $this->secret = config('services.beseif.client_secret');
        $this->access_token = config('services.beseif.access_token');

        //dd($this->secret);
        
    }

    public function resolveAuthorization(&$queryParams, &$formParams, &$headers)
    {
        //dd('prueba 2');
        $queryParams['codigo'] = 'F5bmBx55BjH7tq';



    }

    public function decodeResponse($response)
    {
        //dd('prueba 3');
        return json_decode($response);
    }

    public function resolveAccessToken()
    {
        //dd('prueba 4');
        return $this->access_token;
    }

    public function resolveAccessTokenTwo()
    {
        //dd('prueba 4');
        return "Bearer {$this->access_token}";
    }

    public function handlePayment(Request $request)
    {
        //dd('prueba 5');
        //dd('OOOH');

        $payment = $this->createPayment(
            $request->email_vendedor,
            $request->email_comprador,
            $request->email_comprador,
        );

        

 
            return redirect()
                ->route('home')
                ->withSuccess(['payment' => "Thanks."]);
   
 
    }

    public function handleApproval()
    {
        //
    }

    public function createPayment($value, $email,$token, $installments = 1)
    {
        //dd('prueba 6');

        return $this->makeRequest(
            'POST',
            '/marketplace/pre_checkout',
            [
                
            ],
            [

            ],
            [

            ],
            //$isJsonRequest = true,
        );

    }

    public function resolveFactor($currency)
    {
        dd('prueba 7');
        return $this->converter
            ->convertCurrency($currency, $this->baseCurrency);
    }
}