<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppOrgProductSysDictionary extends Model
{
    //appOrgProduct_sysDictionaries
    protected $table = 'appOrgProduct_sysDictionary';
}
