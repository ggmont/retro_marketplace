<?php

namespace App\Listeners;

use Auth;
use App\SystemLog;
use App\Events\ActionExecuted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ActionExecutedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ActionExecuted  $event
     * @return void
     */
    public function handle(ActionExecuted $event)
    {
		$message 	= 	'';
		$trans		=	'';
		
		$log 					= New SystemLog;
			
		$message =  str_replace('_IP_ADDRESS_',$event->ip, 
			str_replace('_USER_',$event->add_info['user_name'], trans($event->action))
			);
		
		if(isset($event->add_info['removed_ids'])) {
			$message .= trans('events.affected_ids') . $event->add_info['removed_ids'];
			$log->object_id			= -1;
		}
		else
			$log->object_id			= strlen($event->object_id)>0?$event->object_id:null;
		
		
		$log->action			= $event->action;
		$log->table_name		= strlen($event->table_name)>0?$event->table_name:null;
		
		$log->user_id			= strlen(Auth::id())>0?Auth::id():0;
		$log->ip_address		= strlen($event->ip)>0?$event->ip:null;
		$log->additional_info	= $message;
		
		$log->save();
    }
}
