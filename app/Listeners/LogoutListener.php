<?php

namespace App\Listeners;

use App\Events\SessionTimeoutEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Http\Controllers\LoginController;

class LogoutListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SessionTimeoutEvent  $event
     * @return void
     */
    public function handle(SessionTimeoutEvent $event)
    {
        $loginController = new LoginController();
        $loginController->doLogout();
    }
}
