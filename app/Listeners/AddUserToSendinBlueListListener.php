<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use App\Http\Controllers\SendinBlueController;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AddUserToSendinBlueListListener implements ShouldQueue
{
    use InteractsWithQueue;

    protected $sendinblueController;

    public function __construct(SendinBlueController $sendinblueController)
    {
        $this->sendinblueController = $sendinblueController;
    }

    public function handle(UserRegistered $event)
    {
        $user = $event->user;
        $email = $user->email;
        $listId = 'your_list_id'; // Reemplaza con el ID de tu lista en SendinBlue

        $this->sendinblueController->addContactToList($email, $listId);
    }
}
