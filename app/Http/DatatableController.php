<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AppOrgUserInventory;
use Auth;
class DatatableController extends Controller
{
    public function inventory(){
        $inventories  = AppOrgUserInventory::where('user_id', Auth::id())->where('quantity', '>', 0)->whereNull('deleted_at')->orderBy('id','desc')->select('id','product_id','box_condition','manual_condition','cover_condition','game_condition','extra_condition')->get();
        return $inventories;
    }
}
