<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\BarCodeController;
use App\Http\Controllers\Admin\ProductRequestController;
use App\Http\Controllers\Admin\InventoryController;
use App\Http\Controllers\Admin\ShippingController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\RolController;
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\CarrouselController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\FooterLinksController;
use App\Http\Controllers\Admin\NewsController;
use App\Http\Controllers\Admin\PageController;
use App\Http\Controllers\Admin\ConfigurationController;
use App\Http\Controllers\Admin\GestionController;
use App\Http\Controllers\Admin\MessageController;
use App\Http\Controllers\Admin\WebNotificationController;
use App\Http\Controllers\Admin\IncidenceMessageController;
use App\Http\Controllers\Admin\IncidenceController;
use App\Http\Controllers\Admin\UploadController;
use App\Http\Controllers\Admin\PromotionalController;
use App\Http\Controllers\Admin\HistorialPromotionalController;
use App\Http\Controllers\Admin\HistorialProductController;
use App\Http\Controllers\Admin\UserMessageController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\Select2SearchController;
use App\Http\Controllers\Admin\SendController;
use App\Http\Controllers\Admin\ImportController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;

Route::group(['middleware' => ['admin']], function () {
    //Dashboard - Principal - Index
    Route::get('', [HomeController::class, 'index']);

    //Estadisticas - Withdrawal
    Route::get('/withdrawal', [HomeController::class, 'showWithdrawal']);
    Route::get('/showwiths', [HomeController::class, 'showCredit']);
    Route::get('/showwithsped', [HomeController::class, 'showPed']);
    Route::get('/showfees', [HomeController::class, 'showFees']);
    Route::get('/showpur', [HomeController::class, 'showPur']);
    Route::post('/confirm_withdrawal', 'UserController@storeWithDrawal')->name('confirm_withdrawal');


    //Notificaciones - Notifications
    Route::prefix('notifications')->group(function () {

        Route::get('/', [WebNotificationController::class, 'index']);

        Route::post('sendNotificationPro', [WebNotificationController::class, 'sendAll'])->name('send.all');
        Route::put('/send-web-notification/{id}', [WebNotificationController::class, 'sendNotificationrToUser'])->name('send.web-notification');
        Route::post('sendNotification', [WebNotificationController::class, 'sendNotification'])->name('send.notification');
        Route::get('/send-web-notification', [WebNotificationController::class, 'test']);

        Route::patch('/fcm-token', [WebNotificationController::class, 'updateToken'])->name('fcmToken');
        Route::post('/send-notification', [WebNotificationController::class, 'notification'])->name('notification');
    });

    //Notificaciones - Notifications
    Route::prefix('send')->group(function () {

        Route::get('/', [SendController::class, 'index']);
        Route::post('/beseif', [SendController::class, 'pay'])->name('beseif');
    });

    //Categorias - Categories
    Route::prefix('categories')->group(function () {

        Route::get('/', [CategoryController::class, 'index']);
    });

    //Productos - Products
    Route::prefix('csv')->group(function () {

        Route::get('/', [ImportController::class, 'index']);
        Route::post('import', [ImportController::class, 'import'])->name('import');

    });

    //Productos - Products
    Route::prefix('product')->group(function () {

        Route::get('/', [ProductController::class, 'index']);
        Route::get('/create', [ProductController::class, 'create'])->name("product_create");
        Route::post('/store', [ProductController::class, 'store'])->name('product_store');
        Route::get('/images/{id}', [ProductController::class, 'imageProduct']);
        Route::post('/images', [ProductController::class, 'saveImagesProduct']);
        Route::post('/upload_file/{id}', [FrontController::class, 'uploadInventoryImageAdmin']);
        Route::post('/delete_file/{id}', [FrontController::class, 'removeInventoryImageAdmin']);
        Route::get('/edit/{id}', [ProductController::class, 'edit'])->name("edit_product");
        Route::put('/update/{id}', [ProductController::class, 'update'])->name("update_product");
    });

    Route::prefix('historial')->group(function () {

        Route::get('/product', [HistorialProductController::class, 'index'])->name("historial_product");
        Route::get('/user', [HistorialUserController::class, 'index']);
    });



    //Inventario - Inventory
    Route::prefix('inventory')->group(function () {

        Route::get('/', [InventoryController::class, 'index'])->name('inventory_admin');
    });

    //Categorias - Categories
    Route::prefix('promotional')->group(function () {

        Route::get('/', [PromotionalController::class, 'index']);
        Route::get('/create', [PromotionalController::class, 'create'])->name("code_create");
        Route::get('/historial', [HistorialPromotionalController::class, 'index']);
        Route::get('/historial/show/{id}', [HistorialPromotionalController::class, 'show'])->name("historial_view");
    });

    //Solicitudes de Productos - Product Request
    Route::prefix('product-request')->group(function () {

        Route::get('/', [ProductRequestController::class, 'index'])->name('product_request_index');
        Route::get('/edit/{id}', [ProductRequestController::class, 'edit'])->name("edit_product_request");
        Route::post('/update/{id}', [ProductRequestController::class, 'updateGame'])->name("update_product_request");
        Route::post('/update_auction/{id}', [ProductRequestController::class, 'updateAuction'])->name("update_product_request_auction");
        Route::post('/update-collection/{id}', [ProductRequestController::class, 'updateCollection'])->name("update_collection_request");
        Route::delete('/delete/{id}', [ProductRequestController::class, 'ProductRequestDestroy'])->name("delete_product_request");
        Route::post('/update_exist/{id}', [ProductRequestController::class, 'updateProductExist'])->name("update_product_request_exist");
        Route::post('/update_exist_auction/{id}', [ProductRequestController::class, 'updateProductExistAuction'])->name("update_product_request_exist_auction");
        Route::post('/update_exist_collection/{id}', [ProductRequestController::class, 'updateProductCollectionExist'])->name("update_exist_collection");
    });

    //Compras - Shipping
    Route::prefix('shipping')->group(function () {

        Route::get('/', [ShippingController::class, 'index']);
        Route::get('/create', [ShippingController::class, 'create'])->name("new_shipping");
        Route::post('/store', [ShippingController::class, 'store'])->name('shipping.store');
    });

    //Pedidos - Orders
    Route::prefix('orders')->group(function () {

        Route::get('/', [OrderController::class, 'index'])->name("orders");
        Route::get('/view/{id}', [OrderController::class, 'view'])->name("view_order");
    });

    //Banners
    Route::prefix('banners')->group(function () {

        Route::get('/', [BannerController::class, 'index']);
        Route::get('/create', [BannerController::class, 'create'])->name("new_banner");
        Route::post('/store', [BannerController::class, 'store'])->name('banner_store');
        Route::get('/edit/{id}', [BannerController::class, 'edit'])->name("edit_banner");
        Route::post('/update/{id}', [BannerController::class, 'updateImage'])->name("banner_update");
        Route::post('/update-category/{id}', [BannerController::class, 'updateImageCategory'])->name("banner_update_category");
        Route::post('/update-magazine/{id}', [BannerController::class, 'updateImageMagazine'])->name("banner_update_magazine");
    });

    //Blogs
    Route::prefix('blogs')->group(function () {

        Route::get('/', [BlogController::class, 'index']);
    });

    //Carruseles - Carousels
    Route::prefix('carrousel')->group(function () {

        Route::get('/', [CarrouselController::class, 'index']);
        Route::get('/create', [CarrouselController::class, 'create'])->name("new_carrousel");
        Route::post('/store', [CarrouselController::class, 'store'])->name('carrousel_store');
        Route::post('/store_secundary_one', [CarrouselController::class, 'storeImageSecundaryOne'])->name('carrousel_store_secundary_one');
        Route::get('/edit/{id}', [CarrouselController::class, 'edit'])->name("edit_carousel");
        Route::post('/update-principal/{id}', [CarrouselController::class, 'updateImage'])->name("carrousel_update_image");
        Route::post('/update-secundary-one/{id}', [CarrouselController::class, 'updateImageSecundaryOne'])->name("carrousel_update_image_secundary_one");
        Route::post('/update-secundary-two/{id}', [CarrouselController::class, 'updateImageSecundaryTwo'])->name("carrousel_update_image_secundary_two");
    });

    //Preguntas Frecuentes - Faq
    Route::prefix('faqs')->group(function () {
        Route::get('/', [FaqController::class, 'index']);
        Route::get('/edit/{id}', [FaqController::class, 'edit'])->name("edit_faq");
        Route::post('/modify', [FaqController::class, 'updateFaq'])->name('update_faq');
    });

    //Enlaces de pie de Pagina - Footer Links
    Route::prefix('footer_links')->group(function () {

        Route::get('/', [FooterLinksController::class, 'index']);
    });

    //Noticias - News
    Route::prefix('news')->group(function () {

        Route::get('/', [NewsController::class, 'index']);
        Route::get('/edit/{id}', [NewsController::class, 'edit'])->name("edit_new");
        Route::post('/modify', [NewsController::class, 'updateNew'])->name('update_new');
    });

    //Paginas - Pages
    Route::prefix('pages')->group(function () {

        Route::get('/', [PageController::class, 'index']);
        Route::get('/create', [PageController::class, 'create'])->name("page_create");
        Route::get('/edit/{id}', [PageController::class, 'edit'])->name("edit_page");
        Route::post('/store', [PageController::class, 'savePage'])->name('save_page');
        Route::post('/upload', [UploadController::class, 'upload']);
        Route::post('/modify', [PageController::class, 'updatePage'])->name('update_page');
    });

    //Configuracion - Configuration
    Route::prefix('configuration')->group(function () {

        Route::get('/', [ConfigurationController::class, 'index']);
    });

    //Gestion de Usuarios - User Management
    Route::prefix('management')->group(function () {
        Route::get('/', [GestionController::class, 'index']);
        Route::get('/complaint', [GestionController::class, 'complaint_index']);
        Route::get('/UserMessages', [GestionController::class, 'index']);
        Route::get('UserMessages/{userId}/{MsgId}', [UserMessageController::class, 'showMessagesUser'])->name("message_user");
        Route::get('/incidences', [IncidenceController::class, 'index']);
        Route::get('/incidences/{id}', [IncidenceController::class, 'view'])->name("view_incidence");
        Route::post('/SendMessageNewModerator', [IncidenceMessageController::class, 'message_incidence'])->name("newMessageModerator");
        Route::post('/ConfirmIncidence', [IncidenceMessageController::class, 'confirm_incidence'])->name("ConfirmIncidence");
        Route::post('/complaint/{id}', [GestionController::class, 'updateComplaint'])->name("complaint_update");
        Route::get('/roles', [GestionController::class, 'rol_index']);
        Route::post('/roles/{id}', [RolController::class, 'updateRol'])->name("rol_update");
        Route::get('/users', [GestionController::class, 'user_index']);
        Route::get('/visitors', [GestionController::class, 'visitor_index']);
        Route::get('/historial/show/{id}', [GestionController::class, 'historial'])->name("historial_user");
        Route::post('/update-user/{id}', [GestionController::class, 'updateUser'])->name("user_update");
        Route::get('/edit/{id}', [GestionController::class, 'edit'])->name("edit_user");
        Route::post('/modify', [GestionController::class, 'updateUser'])->name('user_update_all');
        Route::post('/AddCredit', [GestionController::class, 'storeCreditUser'])->name('user_credit');
        Route::post('/RestCredit', [GestionController::class, 'minusCreditUser'])->name('user_minus_credit');
        Route::post('/AddCreditSpecial', [GestionController::class, 'storeCreditUserSpecial'])->name('user_credit_special');
        Route::post('/FilterMonth', [GestionController::class, 'filtroMes'])->name('filter_month');
    });

    //Mensajes (Chat de Admin) - Messages (Chat of Admin)
    Route::prefix('messages')->group(function () {

        Route::get('/', [MessageController::class, 'index']);
    });

    //Codigo de Barras para los productos - BarCode for the products
    Route::prefix('barcode')->group(function () {

        Route::get('/', [BarCodeController::class, 'index']);
    });
});
