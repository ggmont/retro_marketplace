<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Special\HomeController;
use App\Http\Controllers\Special\ImagesProductController;
use App\Http\Controllers\Special\ImagesProductUserController;
use App\Http\Controllers\FrontController;


Route::group(['middleware' => ['special']], function () {
    Route::get('', [HomeController::class, 'index']);

    Route::prefix('edition')->group(function () {
        Route::get('/images', [ImagesProductController::class, 'index'])->name('images_product_edition');
        Route::post('/upload_file/{id}', [FrontController::class, 'uploadInventoryImageAdmin']);
        Route::post('/delete_file/{id}', [FrontController::class, 'removeInventoryImageAdmin']);
        Route::get('/edit/{id}', [ImagesProductController::class, 'edit'])->name("edition_image_product");
        Route::get('/special_images', [ImagesProductUserController::class, 'index'])->name('images_trend');
    });
});
