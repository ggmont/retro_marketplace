<?php

namespace App\Http\Middleware;

use Closure;
use App\SysUserRoles;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(Auth::user()->roles->first()->rol->name);
        if (Auth::user() == '') {
            return redirect('/');
        }
        if (SysUserRoles::where('user_id', Auth::id())->whereIn('role_id', [1])->count() > 0) {
            return $next($request)
                //Url a la que se le dará acceso en las peticiones
                ->header("Access-Control-Allow-Origin", "*")
                //Métodos que a los que se da acceso
                ->header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
                //Headers de la petición
                ->header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, X-Token-Auth, Authorization");
        } else {
            return redirect('/');
        }
    }
}
