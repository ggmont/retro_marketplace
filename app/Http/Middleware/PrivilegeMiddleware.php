<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Request;
use Session;
use App\SysUserRoles;

class PrivilegeMiddleware
{
  const PRIVILEGE_ADMIN     = 1;
  const PRIVILEGE_FRONT_END = 2;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
      $hasAdmin = false;
      $hasFrontEnd = false;

      if(Session::get('user_id') == null) {
        Session::put('user_name', Auth::user()->user_name);
        Session::put('user_id', Auth::id());

        $privileges = SysUserRoles::ofUserAndPrivilege(Auth::id())->get();
        $privArray = [];

        foreach($privileges as $key => $value) {
          if( ! in_array($value['code'], $privArray)) {
            array_push($privArray, $value['code']);
            if($value['id'] == self::PRIVILEGE_ADMIN ) {
              $hasAdmin = true;
            }
            elseif ($value['id'] == self::PRIVILEGE_FRONT_END) {
              $hasFrontEnd = true;
            }
          }
        }

        Session::put('user_privileges',$privArray);
      }
      else {
        $hasAdmin = Session::get('user_has_admin');
        $hasFrontEnd = Session::get('user_has_front_end');
      }

      ///dd($hasFrontEnd);

      $routeName = Request::route()->getName();
      //dd(Request::path());
      
      $uri = $routeName !== null ? $routeName : Request::path();

      

      //dd($uri);
      //$url = substr(route('statusShippingReceived'), stripos(route('statusShippingReceived'),"/", 10));

      //dd([substr(substr(route('statusShippingReceived'), stripos(route('statusShippingReceived'),"/", 10)), 1), $url, route('statusShippingReceived')]);

      // Check if user is trying to access the admin URL and has the requested privileges
      if(Auth::user()->frontRole){

      } elseif (explode('/',$uri)[0] == '11w5Cj9WDAjnzlg0' && ! $hasAdmin) {
        return redirect('/')->with('message',trans('app.you_have_no_access'))->with('messageType','warning')->send();
      }
      // if(() || !) {
      //   error_log('aqui');
      //   //http://127.0.0.1:8000/11w5Cj9WDAjnzlg0

      // }

  		if (Auth::check() && ! ($uri == 'home' || $uri == 'logout')) {
        //error_log(config('accesses.'.$uri) );
  			$requestedAccess = config('accesses.'.$uri) ? config('accesses.'.$uri) : '';
  			if(strlen($requestedAccess) > 0) {
  				if($requestedAccess != 'ALL') {
  					$privileges = SysUserRoles::OfUserAndPrivilege(Auth::id(),$requestedAccess)->get();
  					if(count($privileges) === 0) {
              die('1. Wrong URL');
  						return redirect(Request::server('HTTP_REFERER'))->with('message',trans('app.you_have_no_access'))->with('messageType','warning')->send();
  					}
  				}
  			}
  			else {
          die('2. Wrong URL');
  				return redirect('/11w5Cj9WDAjnzlg0/dashboard')->with('message',trans('app.you_have_no_access'))->with('messageType','warning')->send();
  			}


  		}

  		return $next($request);

    }
}
