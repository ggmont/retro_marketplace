<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Session\Store;

class SessionTimeoutMiddleware
{
    protected $session;
    protected $timeout = 120; // en minutos, tiempo de vida útil de la sesión

    public function __construct(Store $session)
    {
        $this->session = $session;
    }

    public function handle($request, Closure $next)
    {
        // Verificar si el usuario está autenticado
        if (Auth::check()) {
            // Obtener el tiempo de la última actividad del usuario
            $lastActivity = $this->session->get('last_activity');

            // Verificar si ha pasado el tiempo de vida útil de la sesión
            if (time() - $lastActivity > $this->timeout * 60) {
                // Cerrar la sesión y redirigir a la página de inicio de sesión
                Auth::logout();
                $this->session->flush();
                return redirect('/login')->with('flash_message', 'Tu sesión ha expirado. Por favor, inicia sesión nuevamente.');
            }

            // Actualizar el tiempo de la última actividad del usuario
            $this->session->put('last_activity', time());
        }

        return $next($request);
    }
}
