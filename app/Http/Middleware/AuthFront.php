<?php

namespace App\Http\Middleware;

use Closure;
use App\SysUserRoles;
use Illuminate\Support\Facades\Auth;

class AuthFront
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd('AAAAAA');
        
        if(SysUserRoles::where('user_id', Auth::id())->whereIn('role_id', [5,2,1,4])->count() > 0){
            return $next($request);
        } else {
            return redirect('/');
        }
        
    }
}
