<?php

namespace App\Http\Middleware;

use Closure;
use App\SysUserRoles;
use Illuminate\Support\Facades\Auth;

class SeeYouSoon
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(Auth::user()->roles->first()->rol->name);
        if (Auth::user() == '') {
            return redirect('/landing');
        }
        if (SysUserRoles::where('user_id', Auth::id())->whereIn('role_id', [1,5])->first()) {
            return $next($request);
        } else {
            return redirect('/landing');
        }
    }
}
