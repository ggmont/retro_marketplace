<?php

namespace App\Http\Middleware;

use Closure;
use App\SysUserRoles;
use Illuminate\Support\Facades\Auth;

class Special
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(Auth::user()->roles->first()->rol->name);
        if (Auth::user() == '') {
            return redirect('/');
        }
        if(SysUserRoles::where('user_id', Auth::id())->whereIn('role_id', [5,2])->count() > 0){
            return $next($request);
        } else {
            return redirect('/');
        }
    }
}
