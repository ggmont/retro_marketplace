<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//Route::post('webhook', 'WebhookController@beseprueba');


Route::post('webhook/{id}', 'FrontController@besepruebatwo');
Route::post('tracking', 'FrontController@besetracking');

Route::post('/sendinblue/add-contact', 'SendinBlueController@addContactToList');
Route::post('/sendinblue/send-email', 'SendinBlueController@sendEmail');
Route::get('/sendinblue/import-contacts', 'SendinBlueController@importContacts');

Route::post('/update-seller-read', 'FrontController@markAsRead');
Route::post('/registrar-token', 'FrontController@registrarToken');


if ((new \Jenssegers\Agent\Agent())->isMobile()) {



	Route::get('/create_new_password/{token?}', array('uses' => 'UserController@showCreateNewPassword'));

	Route::prefix('/google')->group(function () {
		Route::get('/redirect', 'LoginGoogleController@redirectToProvider');
		Route::get('/callback', 'LoginGoogleController@handleProviderCallback');
	});

	Route::get('setlocale/{locale}', [\App\Http\Controllers\LocaleController::class, 'setLocale']);

	//Route::post('/response', 'WebhookController@handle')->name('omega');

	Route::get('datatable/inventories', 'DatatableController@inventory')->name('datatable.inventories');
	Route::post('/FilterMonth', 'UserController@filtroMes')->name('fil');
	Route::get('/product/ajax', ['uses' => 'FrontController@ajaxProduct'])->name('ajaxProduct');
	Route::get('/user/ajax', ['uses' => 'FrontController@ajaxUser'])->name('ajaxUser');


	Route::get('/', ['uses' => 'FrontController@showHome']);

	Route::prefix('/Information')->group(function () {
		Route::get('/', ['uses' => 'FrontController@InformationIndex'])->name('information_index');
	});



	Route::group(['middleware' => ['authfrontend']], function () {

		Route::prefix('/PriceList')->group(function () {
			Route::get('/', ['uses' => 'FrontController@PriceListIndex'])->name('price_list_index');
		});

		Route::prefix('/Auction')->group(function () {
			Route::get('/Select', ['uses' => 'FrontController@AuctionSelect'])->name('auction_select');
		});

		Route::prefix('/FixedPrice')->group(function () {
			Route::get('/Select', ['uses' => 'FrontController@FixedPriceSelect'])->name('fixed_price_select');
		});

		Route::prefix('/Upload')->group(function () {
			Route::get('/', ['uses' => 'FrontController@UploadRequestIndex'])->name('upload_product');
			Route::get('/Auction', ['uses' => 'FrontController@AuctionploadRequestIndex'])->name('auction_upload_product');
		});

		Route::prefix('/NormalUpload')->group(function () {
			Route::get('/', ['uses' => 'FrontController@NormalUploadIndex'])->name('normal_upload');
			Route::post('/Full', ['uses' => 'FrontController@NormalUploadFull'])->name('normal_upload_full');
			Route::get('/Auction', ['uses' => 'FrontController@AuctionNormalUploadIndex'])->name('auction_normal_upload');
			Route::post('/AuctionFull', ['uses' => 'FrontController@AuctionNormalUploadFull'])->name('auction_normal_upload_full');
		});

		Route::prefix('/UploadSelect')->group(function () {
			Route::get('/', ['uses' => 'FrontController@UploadSelectIndex'])->name('upload_select');
		});

		Route::prefix('/Offer')->group(function () {
			Route::get('/', ['uses' => 'FrontController@OfferIndex'])->name('offer_index');
		});

		Route::prefix('/Collection')->group(function () {
			Route::get('/', ['uses' => 'FrontController@CollectionIndex'])->name('collection_index');
			Route::get('/Search', ['uses' => 'FrontController@AddSearchCollectionIndex'])->name('search_collection');
			Route::get('/edit/{id}', ['uses' => 'FrontController@editCollection'])->name('collection_edit');
			Route::get('/add/{product_id?}', ['uses' => 'FrontController@AddCollectionItem'])->name('add_collection');
		});

		Route::post('/store-token', ['uses' => 'FrontController@storeToken'])->name('store.token');
		Route::post('/send-web-notification', ['uses' => 'FrontController@sendWebNotification'])->name('send.web-notification');
		Route::post('/fcm/send', ['uses' => 'FrontController@sendAll']);


		Route::prefix('/ConfigUser')->group(function () {
			Route::get('/', ['uses' => 'FrontController@ConfigUserIndex'])->name('config_user');
			Route::get('/PersonalData', ['uses' => 'FrontController@ConfigUserPersonal'])->name('config_personal_data');
			Route::get('/Balance', ['uses' => 'FrontController@BalanceUserPersonal'])->name('balance_data');
			Route::get('/PublicProfile', ['uses' => 'FrontController@PublicProfilePersonal'])->name('public_profile_data');
			Route::get('/ReferralsData', ['uses' => 'FrontController@ReferralsDataIndex'])->name('referrals_index');
			Route::get('/AddCash', ['uses' => 'FrontController@AddCashIndex'])->name('add_cash');

			//Panel de Control - Control Panel
			Route::get('/Purchases', ['uses' => 'FrontController@PurchasesDataIndex'])->name('purchases_data');
			Route::get('/ReferralsData', ['uses' => 'FrontController@ReferralsDataIndex'])->name('referrals_index');


			Route::get('/PurchaseReserveDataProfile', ['uses' => 'FrontController@ReserveDataIndex'])->name('reserves_index');
			Route::get('/PurchasePaidDataProfile', ['uses' => 'FrontController@PaidDataIndex'])->name('paid_index');
			Route::get('/PurchaseSendDataProfile', ['uses' => 'FrontController@SendDataIndex'])->name('send_index');
			Route::get('/PurchaseDeliveredDataProfile', ['uses' => 'FrontController@DeliveredDataIndex'])->name('delivered_index');
			Route::get('/PurchaseNotDeliveredDataProfile', ['uses' => 'FrontController@NotDeliveredDataIndex'])->name('not_delivered_index');
			Route::get('/PurchaseCancelDataProfile', ['uses' => 'FrontController@CancelDataIndex'])->name('cancel_index');

			Route::get('/PurchaseInternationalReserveDataProfile', ['uses' => 'FrontController@ReserveInternationalDataIndex'])->name('reserves_international_index');
			Route::get('/PurchaseInternationalDeliveredDataProfile', ['uses' => 'FrontController@DeliveredInternationalDataIndex'])->name('delivered_international_index');
			Route::get('/PurchaseInternationalCancelDataProfile', ['uses' => 'FrontController@CancelInternationalDataIndex'])->name('cancel_international_index');
			//Ventas
			Route::get('/Sales', ['uses' => 'FrontController@SalesDataIndex'])->name('sales_data');

			Route::get('/SaleReserveDataProfile', ['uses' => 'FrontController@SaleReserveDataIndex'])->name('sale_reserves_index');
			Route::get('/SalePaidDataProfile', ['uses' => 'FrontController@SalePaidDataIndex'])->name('sale_paid_index');
			Route::get('/SaleSendDataProfile', ['uses' => 'FrontController@SaleSendDataIndex'])->name('sale_send_index');
			Route::get('/SaleDeliveredDataProfile', ['uses' => 'FrontController@SaleDeliveredDataIndex'])->name('sale_delivered_index');
			Route::get('/SaleNotDeliveredDataProfile', ['uses' => 'FrontController@SaleNotDeliveredDataIndex'])->name('sale_not_delivered_index');
			Route::get('/SaleCancelDataProfile', ['uses' => 'FrontController@SaleCancelDataIndex'])->name('sale_cancel_index');

			Route::get('/SaleInternationalReserveDataProfile', ['uses' => 'FrontController@SaleInternationalReserveDataIndex'])->name('reserves_sale_international_index');
			Route::get('/SaleInternationalDeliveredDataProfile', ['uses' => 'FrontController@SaleInternationalDeliveredDataIndex'])->name('delivered_sale_international_index');
			Route::get('/SaleInternationalCancelDataProfile', ['uses' => 'FrontController@SaleInternationalCancelDataIndex'])->name('cancel_sale_international_index');
		});

		Route::prefix('/Complaint')->group(function () {
			Route::post('/complaint_update/{id}', ['uses' => 'FrontController@ComplaintUserInventory'])->name('complaint_user_inventory');
			Route::post('/complaint_product_update/{id}', ['uses' => 'FrontController@ComplaintProductInventory'])->name('complaint_product');
			Route::post('/complaint_user/{id}', ['uses' => 'FrontController@ComplaintUser'])->name('complaint_user');
		});

		Route::prefix('/ProductRequest')->group(function () {
			Route::get('/', ['uses' => 'FrontController@ProductRequestIndex'])->name('product_request');
			Route::get('/NotFound', ['uses' => 'FrontController@ProductNotFound'])->name('product_not_found');
			Route::get('/NotFoundAuction', ['uses' => 'FrontController@ProductAuctionNotFound'])->name('product_auction_not_found');
			Route::get('/NotFoundCollection', ['uses' => 'FrontController@ProductNotFoundCollection'])->name('product_not_found_collection');
			Route::get('/NotFoundAdmin', ['uses' => 'FrontController@ProductNotFoundAdmin'])->name('product_not_found_admin');
			Route::post('/SendToAdmin', ['uses' => 'FrontController@ProductRequestSendToAdmin'])->name('send_product_request');
			Route::post('/SendToAdminRequest', ['uses' => 'FrontController@ProductRequestSendToAdminRequest'])->name('send_product_request_admin');
			Route::post('/SendToCollectionAdminRequest', ['uses' => 'FrontController@ProductCollectionRequestSendToAdminRequest'])->name('send_product_request_collection');
			Route::post('/Exist', ['uses' => 'FrontController@ProductRequestSendToInventory'])->name('send_product_request_exist');
			Route::post('/ajax-autocomplete-search', ['uses' => 'Select2SearchController@selectSearchProduct'])->name('SearchbyProduct');
			Route::post('/ajax-autocomplete-search-game', ['uses' => 'Select2SearchController@selectSearchGame'])->name('SearchbyGame');
			Route::post('/ajax-autocomplete-search-console', ['uses' => 'Select2SearchController@selectSearchConsole'])->name('SearchbyConsole');
			Route::post('/ajax-autocomplete-search-periferico', ['uses' => 'Select2SearchController@selectSearchPeriferico'])->name('SearchbyPeriferico');
			Route::post('/ajax-autocomplete-search-accesorio', ['uses' => 'Select2SearchController@selectSearchAccesorio'])->name('SearchbyAccesorio');
			Route::post('/ajax-autocomplete-search-merchandising', ['uses' => 'Select2SearchController@selectSearchMerchandising'])->name('SearchbyMerchandising');
		});

		Route::prefix('/PromotionCode')->group(function () {
			Route::post('/Check', ['uses' => 'FrontController@CheckPromotionCode'])->name('check_promotional_code');
		});
	});



	//Subida de Imagenes para el perfil

	Route::post('/profile_upload_file/{id}', ['uses' => 'ProfileController@UpdateProfileImage'])->name('upload_profile_image');
	Route::post('/profile_cover_upload_file/{id}', ['uses' => 'ProfileController@UpdateCoverImage'])->name('upload_cover_image');
	Route::post('/profile_social_update/{id}', ['uses' => 'ProfileController@SocialNetworkStore'])->name('social_store');
	Route::post('/profile_social_network_update/{id}', ['uses' => 'ProfileController@SocialNetworkUpdate'])->name('social_network_update');
	//Paypal
	Route::get('/paypal/pay', 'PaymentController@payWithPayPal');
	Route::get('/paypal/status', 'PaymentController@payPalStatus');

	Route::get('/image', ['uses' => 'FrontController@showImage']);

	Route::get('/search/{text?}', ['uses' => 'FrontController@showSearchProduct']);

	Route::get('/MarketPlaceStore', ['uses' => 'FrontController@Marketplace'])->name('marketplace');

	Route::get('/testImage', ['uses' => 'FrontController@testImage']);

	Route::get('/fix/products', ['uses' => 'FrontController@fixProduct']);

	Route::post('/lang', ['uses' => 'FrontController@changeLangRetro']);

	Route::get('/chat', ['uses' => 'FrontController@chat_index'])->name('chat');
	Route::post('/send-message', ['uses' => 'FrontController@sendMessage'])->name('send-message');

	Route::get('/product/search', ['uses' => 'FrontController@showSearchNewProduct']);

	//Route::get('/searchNew',['uses' => 'FrontController@showSearchNewProduct']);
	Route::get('/product/{id?}', ['uses' => 'FrontController@showProduct'])->name("product-show");
	Route::get('/product/inventory/{id?}', ['uses' => 'FrontController@showInventoryProduct'])->name("product-inventory-show");
	Route::get('/item/{id?}', ['uses' => 'FrontController@showProductItem']);
	Route::get('/contact-us', ['uses' => 'FrontController@showContactUs']);
	Route::post('/contact-us', ['uses' => 'FrontController@sendContactUs']);
	Route::get('/offerproduct/{text?}', ['uses' => 'FrontController@showOfferProduct']);
	Route::get('/offer-auction-product/{text?}', ['uses' => 'FrontController@showAuctionOfferProduct']);
	Route::post('/get-products', ['uses' => 'FrontController@getProducts']);


	Route::get('/register-company', ['uses' => 'FrontController@showRegisterCompany']);
	Route::get('/register-user', ['uses' => 'FrontController@showRegisterUser']);
	Route::post('/ajax-search-refered', ['uses' => 'Select2SearchController@selectSearchRefered'])->name('UserRefered');
	Route::post('/register-account', ['uses' => 'FrontController@registerAccount']);
	Route::post('/register-company-account', ['uses' => 'FrontController@registerCompanyAccount']);
	Route::post('/register-user', ['uses' => 'FrontController@registerUser'])->name('registerUser');
	Route::get('/reload-captcha', ['uses' => 'FrontController@reloadCaptcha']);
	Route::get('/account-activated/{token?}', ['uses' => 'FrontController@showAccountActivated']);

	Route::prefix('/AccountProblem')->group(function () {
		Route::get('/', ['uses' => 'FrontController@AccountProblem'])->name('account_problem');

		Route::post('/re_password_reset', array('uses' => 'FrontController@passwordReset'))->name('passResetzero');
		Route::post('/user_reset', array('uses' => 'FrontController@userReset'))->name('resetUser');
		Route::post('/resend_activation', array('uses' => 'FrontController@resendActivation'))->name('resendActivation');
		Route::post('/create_new_password', array('uses' => 'FrontController@createNewPassword'));
	});


	//faqs & pages

	Route::get('/site/{link_page}', ['uses' => 'FrontController@showPublicFaqs']);

	Route::get('/user/{user}/invetory', ['uses' => 'FrontController@showPublicUserInventory'])->name('user-inventory');
	Route::post('/user/{user}/invetory', ['uses' => 'FrontController@showPublicUserInventory']);
	Route::get('/user/{user}', ['uses' => 'FrontController@showPublicProfile'])->name('user-info');
	Route::get('/user/{user}/positive', ['uses' => 'FrontController@showPublicProfilePositive']);
	Route::get('/user/{user}/neutral', ['uses' => 'FrontController@showPublicProfileNeutral']);
	Route::get('/user/{user}/negative', ['uses' => 'FrontController@showPublicProfileNegative']);

	Route::get('/listing/users', ['uses' => 'FrontController@showPublicProfileList'])->name('listUsers');
	Route::post('/listing/users', ['uses' => 'FrontController@showPublicProfileList']);
	Route::post('/filtroUsuario', ['uses' => 'FrontController@filtroUsuario'])->name('filtroUsuario');

	Route::prefix('/notifications')->group(function () {
		Route::get('/', ['uses' => 'FrontController@showNotifications']);
	});

	Route::prefix('/alerts')->group(function () {
		Route::get('/', ['uses' => 'FrontController@showAlerts']);
	});

	Route::prefix('/cart')->group(function () {
		Route::get('/', ['uses' => 'FrontController@showCart']);
		Route::post('/add_item', ['uses' => 'FrontController@cartAddItem']);
		Route::post('/delete_item', ['uses' => 'FrontController@deleteItem']);
		Route::post('/add_shipping', ['uses' => 'FrontController@cartAddShipping']);
	});

	Route::prefix('/blog')->group(function () {
		Route::get('/', ['uses' => 'FrontController@showBlogs'])->name('blogview');
		Route::get('/view', ['uses' => 'FrontController@SingleBlog'])->name('singleblogview');
	});


	Route::prefix('account')->group(function () {

		Route::post('/login', array('uses' => 'LoginController@doLogin'));
		Route::get('/logout', array('uses' => 'LoginController@doLogOut'));

		Route::get('/new', ['uses' => 'FrontController@getAllProducts']);

		Route::group(['middleware' => ['auth', 'frontend']], function () {



			Route::get('/get/user', ['uses' => 'FrontController@getUsersConversation']);
			Route::post('/ajax-autocomplete-search', ['uses' => 'Select2SearchController@selectSearch'])->name('newUserMessage');
			Route::post('/SendMessageNewChat', 'ConversationController@storeProfileChat')->name('newMessageChat');
			Route::post('/SendMessageNew', 'ConversationController@storeProfile')->name('newMessage');
			Route::get('/api/messages', 'MessageController@index');
			Route::post('/api/messages', 'MessageController@store');
			Route::post('/api/messages/del', 'MessageController@removeSMS');
			Route::post('/api/messages/all', 'MessageController@removeAll');
			//Route::get('/',['uses' => 'FrontController@showAccount']);
			Route::post('/', ['uses' => 'FrontController@CheckAmount'])->name('withdrawals_amount');
			Route::get('/payment', ['uses' => 'FrontController@showPaymentAccount']);
			Route::get('/messages', ['uses' => 'FrontController@showMessages'])->name('messages_index');
			Route::get('/messages/user/{id?}', ['uses' => 'FrontController@showMessagesUser'])->name("chat_index");

			Route::get('/order/{order_id}', ['uses' => 'FrontController@showOrderStatus']);

			Route::prefix('/transactions')->group(function () {
				Route::get('/', ['uses' => 'FrontController@showTransactions'])->name('transaction_index');
				Route::post('/', ['uses' => 'FrontController@getTransactions']);
				Route::get('/generate-pdf/{id}', 'FrontController@generatePDF');
			});


			Route::prefix('/prueba')->group(function () {
				Route::get('/', ['uses' => 'FrontController@prueba'])->name('prueba_index');
			});


			Route::prefix('/profile')->group(function () {
				Route::get('/', ['uses' => 'FrontController@showProfile']);
				Route::get('/positive', ['uses' => 'FrontController@showAccountProfilePositive']);
				Route::get('/neutral', ['uses' => 'FrontController@showAccountProfileNeutral']);
				Route::get('/negative', ['uses' => 'FrontController@showAccountProfileNegative']);
				Route::get('/list-inv', ['uses' => 'FrontController@showListInventory']);
				//Route::get('/{user}',['uses' => 'FrontController@showAccountProfileNegative']);
			});

			Route::prefix('/inventory')->group(function () {
				Route::get('/', ['uses' => 'FrontController@showInventory']);
				Route::get('/list-dt', ['uses' => 'FrontController@dtAccountInventory']);

				//Subastas

				Route::get('/add-auction/{product_id?}', ['uses' => 'FrontController@showAuctionInventoryItem']);
				Route::post('/add-auction/{product_id?}', ['uses' => 'FrontController@saveAuctionInventoryItem']);

				Route::put('/MakeAuction/{id}', ['uses' => 'FrontController@MakeAuction'])->name('MakeAuction');
				Route::put('/AcceptOffer/{id}', ['uses' => 'FrontController@AcceptOffer'])->name('AcceptOffer');
				Route::put('/DenyOffer/{id}', ['uses' => 'FrontController@DenyOffer'])->name('DenyOffer');


				//Normal
				Route::get('/add/{product_id?}', ['uses' => 'FrontController@showInventoryItem']);
				Route::post('/add/{product_id?}', ['uses' => 'FrontController@saveInventoryItem']);
				Route::post('/collection/{product_id?}', ['uses' => 'FrontController@saveInventoryCollectionItem']);
				Route::get('/modify/{id?}', ['uses' => 'FrontController@showInventoryItem']);
				Route::get('/edit/{id}', ['uses' => 'FrontController@editInventory'])->name('inventory_edit');
				Route::get('/edit_normal/{id}', ['uses' => 'FrontController@editNormalInventory'])->name('normal_inventory_edit');

				Route::put('/update/{id}', ['uses' => 'FrontController@updateInventoryItem'])->name("update_inventory");

				Route::put('/normal_update/{id}', ['uses' => 'FrontController@NormalUpdateInventoryItem'])->name("normal_update_inventory");

				Route::put('/collection-update/{id}', ['uses' => 'FrontController@updateInventoryItem'])->name("update_collection");

				Route::post('/upload_file/{id}', ['uses' => 'FrontController@uploadInventoryImageUser']);
				Route::post('/delete_file/{id}', ['uses' => 'FrontController@removeInventoryImageUser']);

				Route::post('/upload_file_ean/{id}', ['uses' => 'FrontController@uploadInventoryImageEanUser']);
				Route::post('/delete_file_ean/{id}', ['uses' => 'FrontController@removeInventoryImageEanUser']);

				// CONSOLES
				/*
					Route::get('/add/{product_id?}',['uses' => 'FrontController@showInventoryItem']);
					Route::post('/add/{product_id?}',['uses' => 'FrontController@saveInventoryItem']);
					Route::get('/modify/{id?}',['uses' => 'FrontController@showInventoryItem']);
					Route::get('/update/{id?}/{qty?}',['uses' => 'FrontController@showInventoryItem']);
					*/
				Route::post('/delete', ['uses' => 'FrontController@deleteInventoryItem']);

				Route::post('/upload_file', ['uses' => 'FrontController@uploadInventoryImage']);

				Route::post('/upload_file_request', ['uses' => 'FrontController@uploadInventoryImageRequest']);

				Route::post('/remove_file', ['uses' => 'FrontController@removeInventoryImage']);

				Route::post('/upload_file_ean', ['uses' => 'FrontController@uploadInventoryImageEan']);
			});

			Route::prefix('/sales')->group(function () {
				Route::get('/', ['uses' => 'FrontController@showSales']);

				Route::get('/list-dt', ['uses' => 'FrontController@dtAccountSales']);
				Route::get('/list', ['uses' => 'FrontController@listSales']);
				Route::get('/view/{id}', ['uses' => 'FrontController@showAccountSaleDetails']);
				Route::post('/automatic/{id}', ['uses' => 'FrontController@saveOrderCancelAutomatic']);
				Route::post('/cancel/{id}', ['uses' => 'FrontController@CancelSale']);

				Route::post('/update', ['uses' => 'FrontController@updateShippingSales']);
				Route::post('/status', ['uses' => 'FrontController@updateShippingStatus'])->name('statusShippingReceived');
				Route::post('/status-send', ['uses' => 'FrontController@SendUpdate'])->name('SendStatus');
				Route::post('/status-received', ['uses' => 'FrontController@updateShippingStatus'])->name('ReceivedStatus');
				Route::post('/status-received-yes', ['uses' => 'FrontController@ReceivedUpdate'])->name('ReceivedStatusTwo');
				Route::post('/view/{id}', ['uses' => 'FrontController@saveOrderCancel']);
			});

			Route::prefix('/purchases')->group(function () {
				Route::get('/', ['uses' => 'FrontController@showPurchases']);

				Route::get('/list-dt', ['uses' => 'FrontController@dtAccountPurchases']);
				Route::get('/view/{id}', ['uses' => 'FrontController@showAccountPurchaseDetails']);

				Route::post('/view/{id}', ['uses' => 'FrontController@saveOrderCancel']);

				Route::post('/automatic/{id}', ['uses' => 'FrontController@saveOrderCancelAutomatic']);

				Route::post('/cancel/{id}', ['uses' => 'FrontController@CancelPurchase']);

				Route::get('/view/{id}/score', ['uses' => 'FrontController@showOrderRating']);
				Route::post('/view/{id}/score', ['uses' => 'FrontController@saveOrderRating']);

				Route::get('/view/{id}/cancel', ['uses' => 'FrontController@showOrderCancel']);


				Route::post('/cancel', ['uses' => 'FrontController@cancelAccountPurchases']);
				Route::post('/complete', ['uses' => 'FrontController@completeAccountPurchases']);
			});

			Route::get('/banks', ['uses' => 'FrontController@showAccountBank']);
			Route::post('/banks', ['uses' => 'FrontController@updateAccountBank']);

			Route::get('/settings', ['uses' => 'FrontController@showAccountSettings']);
		});
	});

	Route::group(['middleware' => ['auth', 'frontend']], function () {

		Route::get('/product/ajax', ['uses' => 'FrontController@ajaxProduct'])->name('ajaxProduct');

		Route::get('/get/data', ['uses' => 'FrontController@getData'])->name('getData');


		Route::post('/change/email', ['uses' => 'FrontController@updateAccountEmail']);
		Route::post('/change/information', ['uses' => 'FrontController@updateAccountInformation']);
		Route::post('/change/pass', ['uses' => 'FrontController@updateAccountPass']);
		Route::post('/change/info', ['uses' => 'FrontController@updateAccountBank']);
		Route::get('/checkout', ['uses' => 'FrontController@showCheckout']);
		Route::post('/cartComplete', ['uses' => 'FrontController@storeCheckout']);


		Route::post('/tracking', ['uses' => 'FrontController@tracking'])->name('url_tracking');


		Route::post('/cartCompleteCheck', ['uses' => 'FrontController@storeCompleteCheckout']);
		Route::post('/process_checkout', ['uses' => 'FrontController@processCheckoutNew']);
		Route::post('/process_checkout_new/{id}', ['uses' => 'FrontController@processCheckoutNew']);
		Route::get('/success_checkout', ['uses' => 'FrontController@showSuccessCheckoutNew']);
		Route::get('/success_checkout_new/{id}', ['uses' => 'FrontController@showSuccessCheckoutNew']);
		Route::get('/order_complete/{id}', ['uses' => 'FrontController@showSuccessOrder']);

		Route::post('/problem_order', ['uses' => 'FrontController@problemOrderSend'])->name('problem-order');



		Route::post('/refund', ['uses' => 'FrontController@requestRefund'])->name('requestRefund');
		Route::post('/refund/response', ['uses' => 'FrontController@responseRefund'])->name('responseRefund');
	});

	Route::get('/activate_account/{token?}', array('uses' => 'UserController@activateAccount'));


	// Admin routes
	Route::prefix('11w5Cj9WDAjnzlg0')->group(function () {


		// Routes for Login Screen
		Route::get('/', array('uses' => 'LoginController@showLogin'));

		Route::post('/login', array('uses' => 'LoginController@doLogin'));

		Route::get('/password_reset', array('uses' => 'UserController@showPasswordReset'));

		Route::post('/password_reset', array('uses' => 'UserController@passwordReset'));

		Route::get('/create_new_password/{token?}', array('uses' => 'UserController@showCreateNewPassword'));

		Route::post('/create_new_password', array('uses' => 'UserController@createNewPassword'));

		Route::get('/register', array('uses' => 'UserController@showNewUserRegistration'));

		Route::post('/register', array('uses' => 'UserController@newUserRegistration'));

		Route::get('/show_resend_activation', array('uses' => 'UserController@showResendActivation'));

		Route::post('/resend_activation', array('uses' => 'UserController@resendActivation'));

		Route::post('/gestion', ['uses' => 'MarketController@requestGestion'])->name('requestGestion');
		Route::post('/solucion', ['uses' => 'MarketController@requestSolucion'])->name('requestSolucion');

		Route::group(['middleware' => ['auth', 'privilege']], function () {

			// Routes for application

			Route::get('/dashboard', 'HomeController@showDashboard');
			Route::get('/withdrawal', 'HomeController@showWithdrawal');

			Route::get('/showwiths', ['uses' => 'HomeController@showCredit']);
			Route::get('/showwithsped', ['uses' => 'HomeController@showPed']);
			Route::get('/showfees', ['uses' => 'HomeController@showFees']);
			Route::get('/showpur', ['uses' => 'HomeController@showPur']);

			Route::get('/list', 'HomeController@showList');

			Route::get('/configuration/lists', 'SystemController@showLists');
			Route::get('/configuration/lists-dt', 'SystemController@dtLists');
			Route::get('/configuration/list/{id?}', ['as' => '11w5Cj9WDAjnzlg0/configuration/list', 'uses' => 'SystemController@showList']);
			Route::post('/configuration/save_list', 'SystemController@saveList');
			Route::post('/configuration/delete_list', 'SystemController@deleteList');

			Route::get('/messages', 'MessageController@showMessages');
			Route::post('/messagesUs', 'MessageController@showUserMessages');
			// User management
			Route::prefix('users')->group(function () {

				Route::get('/', 'UserController@showUsers');
				Route::get('/info/{id}', ['as' => '11w5Cj9WDAjnzlg0/users/info', 'uses' => 'UserController@showInfoUsers']);
				Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/users', 'uses' => 'UserController@dtUsers']);
				Route::get('/add', 'UserController@showUser');
				Route::get('/modify/{id}', ['uses' => 'UserController@showUser']);
				Route::post('/delete', 'UserController@deleteUser');
				Route::get('/logout', array('uses' => 'LoginController@doLogOut'));
				Route::post('/AddCredit', 'UserController@storeCreditUser');
				Route::prefix('privileges')->group(function () {
					Route::get('/', 'UserController@showPrivileges');
					Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/users/privileges', 'uses' => 'UserController@dtPrivileges']);
					Route::get('/modify/{id?}', ['uses' => 'UserController@showPrivilege']);
					Route::post('/modify', 'UserController@savePrivilege');
				});

				Route::prefix('roles')->group(function () {
					Route::get('/', 'UserController@showRoles');
					Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/users/roles', 'uses' => 'UserController@dtRoles']);
					Route::get('/add', 'UserController@showRole');
					Route::post('/save', ['as' => '11w5Cj9WDAjnzlg0/users/roles/add', 'uses' => 'UserController@saveRole']);
					Route::get('/modify/{id}', ['uses' => 'UserController@showRole']);
					Route::post('/modify', 'UserController@saveRole');
					Route::post('/delete', 'UserController@deleteRole');
				});

				Route::prefix('profile')->group(function () {
					Route::get('/{id?}', 'UserController@showProfile');
					Route::post('/save_profile', 'UserController@saveProfile');
					Route::post('/upload_image', 'UserController@saveProfilePicture');
				});
			});

			Route::get('/utils/sidebar_collapse', 'UtilController@changeSideBar');

			Route::prefix('marketplace')->group(function () {

				//Route::get('/orders',['as' => '11w5Cj9WDAjnzlg0/marketplace/orders', 'uses' => 'MarketController@showOrders'] );

				Route::prefix('orders')->group(function () {
					Route::get('/', 'MarketController@showOrders');
					Route::get('/not', 'MarketController@showOrdersNot');
					Route::post('/', 'MarketController@showOrders');
				});

				// Market Place Products
				Route::prefix('products')->group(function () {
					Route::get('/', 'MarketController@showProducts');
					Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/products', 'uses' => 'MarketController@dtProducts']);

					Route::get('/list-search/{id}', ['as' => '11w5Cj9WDAjnzlg0/marketplace/products/search', 'uses' => 'MarketController@searchProducts']);

					Route::get('/add', 'MarketController@showProduct');
					Route::post('/save', ['as' => '11w5Cj9WDAjnzlg0/marketplace/products/add', 'uses' => 'MarketController@saveProduct']);
					Route::get('/modify/{id}', ['as' => '11w5Cj9WDAjnzlg0/marketplace/products/modify', 'uses' => 'MarketController@showProduct']);

					Route::post('/modify', 'MarketController@saveProduct');
					Route::post('/delete', 'MarketController@deleteProduct');

					Route::get('/images/{id}', ['uses' => 'MarketController@imageProduct']);
					Route::post('/images', ['uses' => 'MarketController@saveImagesProduct']);
					Route::post('/upload_file', ['uses' => 'MarketController@uploadProductImage']);
					Route::post('/remove_file', ['uses' => 'MarketController@removeProductImage']);
				});
				// Market Place Shipping
				Route::prefix('shipping')->group(function () {
					Route::get('/', 'MarketController@showShipping');
					Route::get('/list-dt', ['uses' => 'MarketController@listShipping']);
					Route::get('/add', ['uses' => 'MarketController@AddShowShipping']);
					Route::post('/add', 'MarketController@storeShipping');
					Route::get('/modify/{id}', ['as' => '11w5Cj9WDAjnzlg0/marketplace/shipping/modify', 'uses' => 'MarketController@updateShipping']);
					Route::post('/modify/{id}', ['uses' => 'MarketController@saveShipping']);
					Route::post('/delete', 'MarketController@deleteShipping');
				});
				// Market Place Inventory
				Route::prefix('inventory')->group(function () {
					Route::get('/', 'MarketController@showInventory');

					Route::get('/ean', 'MarketController@showInventoryEan');

					Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/inventory', 'uses' => 'MarketController@dtInventory']);
				});


				// Market Place Categories
				Route::prefix('categories')->group(function () {
					Route::get('/', 'MarketController@showCategories');
					Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/categories', 'uses' => 'MarketController@dtCategories']);
					Route::get('/add', 'MarketController@showCategory');
					Route::post('/save', ['as' => '11w5Cj9WDAjnzlg0/marketplace/categories/add', 'uses' => 'MarketController@saveCategory']);
					Route::get('/modify/{id}', ['as' => '11w5Cj9WDAjnzlg0/marketplace/categories/modify', 'uses' => 'MarketController@showCategory']);
					Route::post('/modify', 'MarketController@saveCategory');
					Route::post('/delete', 'MarketController@deleteCategory');
					Route::post('/upload_image', ['uses' => 'MarketController@saveCategoryImage']);
				});
				// Market Place Pages
				Route::prefix('pages')->group(function () {
					Route::get('/', 'MarketController@showPages');
					Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/pages', 'uses' => 'MarketController@dtPages']);
					Route::get('/add', ['uses' => 'MarketController@showPage']);
					Route::post('/save', ['as' => '11w5Cj9WDAjnzlg0/marketplace/pages/add', 'uses' => 'MarketController@savePage']);
					Route::get('/modify/{id}', ['as' => '11w5Cj9WDAjnzlg0/marketplace/pages/modify', 'uses' => 'MarketController@showPage']);
					Route::post('/modify', 'MarketController@savePage');
					Route::post('/delete', 'MarketController@deletePage');
				});
				// Market Place Footer Links
				Route::prefix('links')->group(function () {
					Route::get('/', 'MarketController@showLinks');
					Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/links', 'uses' => 'MarketController@dtLinks']);
					Route::get('/add', 'MarketController@showLink');
					Route::post('/save', ['uses' => 'MarketController@saveLink']);
					Route::get('/modify/{id}', ['uses' => 'MarketController@showLink']);
					Route::post('/modify', 'MarketController@saveLink');
					Route::post('/delete', 'MarketController@deleteLink');
				});
				// Market Place Footer Faqs
				Route::prefix('faqs')->group(function () {
					Route::get('/', 'MarketController@showFaqs');
					Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/faqs', 'uses' => 'MarketController@dtFaqs']);
					Route::get('/add', 'MarketController@showFaq');
					Route::post('/save', ['as' => '11w5Cj9WDAjnzlg0/marketplace/faqs/add', 'uses' => 'MarketController@saveFaq']);
					Route::get('/modify/{id}', ['as' => '11w5Cj9WDAjnzlg0/marketplace/faqs/modify', 'uses' => 'MarketController@showFaq']);
					Route::post('/modify', 'MarketController@saveFaq');
					Route::post('/delete', 'MarketController@deleteFaq');
				});
				// Market Place Carousel
				Route::prefix('carousels')->group(function () {
					Route::get('/', 'MarketController@showCarousels');
					Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/carousels', 'uses' => 'MarketController@dtCarousels']);
					Route::get('/add', 'MarketController@showCarousel');
					Route::post('/save', ['as' => '11w5Cj9WDAjnzlg0/marketplace/carousels/add', 'uses' => 'MarketController@saveCarousel']);
					Route::get('/modify/{id}', ['as' => '11w5Cj9WDAjnzlg0/marketplace/carousels/modify', 'uses' => 'MarketController@showCarousel']);
					Route::post('/modify', 'MarketController@saveCarousel');
					Route::post('/delete', 'MarketController@deleteCarousel');
					Route::post('/upload_image', ['uses' => 'MarketController@saveCarouselImage']);
				});
				// Market Place Banners
				Route::prefix('banners')->group(function () {
					Route::get('/', 'MarketController@showBanners');
					Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/banners', 'uses' => 'MarketController@dtBanners']);
					Route::get('/add', 'MarketController@showBanner');
					Route::post('/save', ['as' => '11w5Cj9WDAjnzlg0/marketplace/banners/add', 'uses' => 'MarketController@saveBanner']);
					Route::get('/modify/{id}', ['as' => '11w5Cj9WDAjnzlg0/marketplace/banners/modify', 'uses' => 'MarketController@showBanner']);
					Route::post('/modify', 'MarketController@saveBanner');
					Route::post('/delete', 'MarketController@deleteBanner');
					Route::post('/upload_image', ['uses' => 'MarketController@saveBannerImage']);
				});
				// Market Place News
				Route::prefix('news')->group(function () {
					Route::get('/', 'MarketController@showNews');
					Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/news', 'uses' => 'MarketController@dtNews']);
					Route::get('/add', 'MarketController@showNewsItem');
					Route::post('/save', ['as' => '11w5Cj9WDAjnzlg0/marketplace/news/add', 'uses' => 'MarketController@saveNewsItem']);
					Route::get('/modify/{id}', ['uses' => 'MarketController@showNewsItem']);
					Route::post('/modify', 'MarketController@saveNewsItem');
					Route::post('/delete', 'MarketController@deleteNewsItem');
					Route::post('/upload_image', ['uses' => 'MarketController@saveNewsItemImage']);
				});

				// Market Place News
				Route::prefix('blogs')->group(function () {
					Route::get('/', 'MarketController@showBlogs');
					Route::get('/list-dt', ['uses' => 'MarketController@dtBlogs']);
					Route::get('/add', 'MarketController@showBlogsItem');
					Route::post('/save', ['uses' => 'MarketController@saveBlogsItem']);
					Route::get('/modify/{id}', ['uses' => 'MarketController@showBlogsItem']);
					Route::post('/modify', 'MarketController@saveBlogsItem');
					Route::post('/delete', 'MarketController@deleteBlogsItem');
					Route::post('/upload_image', ['uses' => 'MarketController@saveBlogsItemImage']);
				});
			});

			// Organizations configuration

			// Organizations for individuals

			Route::prefix('configuration')->group(function () {
				Route::prefix('organizations')->group(function () {
					Route::get('/', 'SystemController@showOrganizations');
				});
			});

			Route::get('/organizations/{org_id}/info', ['as' => '11w5Cj9WDAjnzlg0/organizations/info', 'uses' => 'OrganizationController@showOrganizationInfo']);
			Route::post('/organizations/{org_id}/save_info', ['as' => '11w5Cj9WDAjnzlg0/organizations/save_info', 'uses' => 'OrganizationController@saveOrganizationInfo']);


			Route::get('/my_store/{user_id}/configuration', ['as' => '11w5Cj9WDAjnzlg0/my_store/configuration', 'uses' => 'MyStoreController@showConfiguration']);
			Route::post('/my_store/{user_id}/save_configuration', ['uses' => 'MyStoreController@saveConfiguration']);

			Route::get('/my_store/{user_id}/inventory', ['as' => '11w5Cj9WDAjnzlg0/my_store/inventory', 'uses' => 'MyStoreController@showInventories']);
			Route::get('/my_store/{user_id}/inventory/list-dt', ['uses' => 'MyStoreController@dtInventories']);
			Route::get('/my_store/{user_id}/add_inventory', ['uses' => 'MyStoreController@showInventory']);
			Route::post('/my_store/{user_id}/save_inventory', ['uses' => 'MyStoreController@saveInventory']);
			Route::get('/my_store/{user_id}/modify_inventory/{id}', ['uses' => 'MyStoreController@showInventory']);
			Route::post('/my_store/{user_id}/modify_inventory', ['uses' => 'MyStoreController@saveInventory']);
			Route::post('/my_store/{user_id}/delete_inventory', ['uses' => 'MyStoreController@deleteInventory']);
			Route::post('/my_store/inventory/upload_file', ['uses' => 'MyStoreController@uploadInventoryImages']);
			Route::post('/my_store/inventory/remove_file', ['uses' => 'MyStoreController@removeInventoryImages']);

			Route::get('/my_store/{user_id}/orders', ['as' => '11w5Cj9WDAjnzlg0/my_store/orders', 'uses' => 'MyStoreController@showOrders']);
			Route::get('/my_store/{user_id}/orders/list-dt', ['uses' => 'MyStoreController@dtOrders']);
			Route::get('/my_store/{user_id}/modify_order/{id}', ['uses' => 'MyStoreController@showOrder']);
			Route::post('/my_store/{user_id}/modify_order', ['uses' => 'MyStoreController@saveOrder']);
		});
	});
} else {


	 

		Route::prefix('/google')->group(function () {
			Route::get('/redirect', 'LoginGoogleController@redirectToProvider');
			Route::get('/callback', 'LoginGoogleController@handleProviderCallback');
		});

		Route::get('/offline', 'OfflineController@index');

		Route::prefix('/Wait')->group(function () {
			Route::get('/', ['uses' => 'FrontController@WaitIndex'])->name('waiting_index');
		});

		Route::get('setlocale/{locale}', [\App\Http\Controllers\LocaleController::class, 'setLocale']);

		Route::get('datatable/inventories', 'DatatableController@inventory')->name('datatable.inventories');
		Route::post('/FilterMonth', 'UserController@filtroMes')->name('fil');
		Route::get('/product/ajax', ['uses' => 'FrontController@ajaxProduct'])->name('ajaxProduct');
		Route::get('/user/ajax', ['uses' => 'FrontController@ajaxUser'])->name('ajaxUser');


		Route::get('/', ['uses' => 'FrontController@showHome']);

		Route::group(['middleware' => ['authfrontend']], function () {

			Route::prefix('/PriceList')->group(function () {
				Route::get('/', ['uses' => 'FrontController@PriceListIndex'])->name('price_list_index');
			});


			Route::prefix('/Auction')->group(function () {
				Route::get('/Select', ['uses' => 'FrontController@AuctionSelect'])->name('auction_select');
			});

			Route::prefix('/FixedPrice')->group(function () {
				Route::get('/Select', ['uses' => 'FrontController@FixedPriceSelect'])->name('fixed_price_select');
			});

			Route::prefix('/Upload')->group(function () {
				Route::get('/', ['uses' => 'FrontController@UploadRequestIndex'])->name('upload_product');
			});

			Route::prefix('/NormalUpload')->group(function () {
				Route::get('/', ['uses' => 'FrontController@NormalUploadIndex'])->name('normal_upload');
				Route::post('/Full', ['uses' => 'FrontController@NormalUploadFull'])->name('normal_upload_full');
			});

			Route::prefix('/UploadSelect')->group(function () {
				Route::get('/', ['uses' => 'FrontController@UploadSelectIndex'])->name('upload_select');
			});

			Route::prefix('/Collection')->group(function () {
				Route::get('/', ['uses' => 'FrontController@CollectionIndex'])->name('collection_index');
				Route::get('/Search', ['uses' => 'FrontController@AddSearchCollectionIndex'])->name('search_collection');
				Route::get('/edit/{id}', ['uses' => 'FrontController@editCollection'])->name('collection_edit');
				Route::get('/add/{product_id?}', ['uses' => 'FrontController@AddCollectionItem'])->name('add_collection');
			});

			Route::post('/store-token', ['uses' => 'FrontController@storeToken'])->name('store.token');
			Route::post('/send-web-notification', ['uses' => 'FrontController@sendWebNotification'])->name('send.web-notification');
			Route::post('/fcm/send', ['uses' => 'FrontController@sendAll']);


			Route::prefix('/ConfigUser')->group(function () {
				Route::get('/', ['uses' => 'FrontController@ConfigUserIndex'])->name('config_user');
				Route::get('/PersonalData', ['uses' => 'FrontController@ConfigUserPersonal'])->name('config_personal_data');
				Route::get('/Balance', ['uses' => 'FrontController@BalanceUserPersonal'])->name('balance_data');
				Route::get('/PublicProfile', ['uses' => 'FrontController@PublicProfilePersonal'])->name('public_profile_data');
				Route::get('/ReferralsData', ['uses' => 'FrontController@ReferralsDataIndex'])->name('referrals_index');
				Route::get('/AddCash', ['uses' => 'FrontController@AddCashIndex'])->name('add_cash');

				//Panel de Control - Control Panel
				Route::get('/Purchases', ['uses' => 'FrontController@PurchasesDataIndex'])->name('purchases_data');
				Route::get('/ReferralsData', ['uses' => 'FrontController@ReferralsDataIndex'])->name('referrals_index');


				Route::get('/PurchaseReserveDataProfile', ['uses' => 'FrontController@ReserveDataIndex'])->name('reserves_index');
				Route::get('/PurchasePaidDataProfile', ['uses' => 'FrontController@PaidDataIndex'])->name('paid_index');
				Route::get('/PurchaseSendDataProfile', ['uses' => 'FrontController@SendDataIndex'])->name('send_index');
				Route::get('/PurchaseDeliveredDataProfile', ['uses' => 'FrontController@DeliveredDataIndex'])->name('delivered_index');
				Route::get('/PurchaseNotDeliveredDataProfile', ['uses' => 'FrontController@NotDeliveredDataIndex'])->name('not_delivered_index');
				Route::get('/PurchaseCancelDataProfile', ['uses' => 'FrontController@CancelDataIndex'])->name('cancel_index');

				Route::get('/PurchaseInternationalReserveDataProfile', ['uses' => 'FrontController@ReserveInternationalDataIndex'])->name('reserves_international_index');
				Route::get('/PurchaseInternationalDeliveredDataProfile', ['uses' => 'FrontController@DeliveredInternationalDataIndex'])->name('delivered_international_index');
				Route::get('/PurchaseInternationalCancelDataProfile', ['uses' => 'FrontController@CancelInternationalDataIndex'])->name('cancel_international_index');
				//Ventas
				Route::get('/Sales', ['uses' => 'FrontController@SalesDataIndex'])->name('sales_data');

				Route::get('/SaleReserveDataProfile', ['uses' => 'FrontController@SaleReserveDataIndex'])->name('sale_reserves_index');
				Route::get('/SalePaidDataProfile', ['uses' => 'FrontController@SalePaidDataIndex'])->name('sale_paid_index');
				Route::get('/SaleSendDataProfile', ['uses' => 'FrontController@SaleSendDataIndex'])->name('sale_send_index');
				Route::get('/SaleDeliveredDataProfile', ['uses' => 'FrontController@SaleDeliveredDataIndex'])->name('sale_delivered_index');
				Route::get('/SaleNotDeliveredDataProfile', ['uses' => 'FrontController@SaleNotDeliveredDataIndex'])->name('sale_not_delivered_index');
				Route::get('/SaleCancelDataProfile', ['uses' => 'FrontController@SaleCancelDataIndex'])->name('sale_cancel_index');

				Route::get('/SaleInternationalReserveDataProfile', ['uses' => 'FrontController@SaleInternationalReserveDataIndex'])->name('reserves_sale_international_index');
				Route::get('/SaleInternationalDeliveredDataProfile', ['uses' => 'FrontController@SaleInternationalDeliveredDataIndex'])->name('delivered_sale_international_index');
				Route::get('/SaleInternationalCancelDataProfile', ['uses' => 'FrontController@SaleInternationalCancelDataIndex'])->name('cancel_sale_international_index');
			});

			Route::prefix('/Complaint')->group(function () {
				Route::post('/complaint_update/{id}', ['uses' => 'FrontController@ComplaintUserInventory'])->name('complaint_user_inventory');
				Route::post('/complaint_product_update/{id}', ['uses' => 'FrontController@ComplaintProductInventory'])->name('complaint_product');
				Route::post('/complaint_user/{id}', ['uses' => 'FrontController@ComplaintUser'])->name('complaint_user');
			});

			Route::prefix('/ProductRequest')->group(function () {
				Route::get('/', ['uses' => 'FrontController@ProductRequestIndex'])->name('product_request');
				Route::get('/NotFound', ['uses' => 'FrontController@ProductNotFound'])->name('product_not_found');
				Route::get('/NotFoundAuction', ['uses' => 'FrontController@ProductAuctionNotFound'])->name('product_auction_not_found');
				Route::get('/NotFoundCollection', ['uses' => 'FrontController@ProductNotFoundCollection'])->name('product_not_found_collection');
				Route::get('/NotFoundAdmin', ['uses' => 'FrontController@ProductNotFoundAdmin'])->name('product_not_found_admin');
				Route::post('/SendToAdmin', ['uses' => 'FrontController@ProductRequestSendToAdmin'])->name('send_product_request');
				Route::post('/SendToAdminRequest', ['uses' => 'FrontController@ProductRequestSendToAdminRequest'])->name('send_product_request_admin');
				Route::post('/SendToCollectionAdminRequest', ['uses' => 'FrontController@ProductCollectionRequestSendToAdminRequest'])->name('send_product_request_collection');
				Route::post('/Exist', ['uses' => 'FrontController@ProductRequestSendToInventory'])->name('send_product_request_exist');
				Route::post('/ajax-autocomplete-search', ['uses' => 'Select2SearchController@selectSearchProduct'])->name('SearchbyProduct');
				Route::post('/ajax-autocomplete-search-game', ['uses' => 'Select2SearchController@selectSearchGame'])->name('SearchbyGame');
				Route::post('/ajax-autocomplete-search-console', ['uses' => 'Select2SearchController@selectSearchConsole'])->name('SearchbyConsole');
				Route::post('/ajax-autocomplete-search-periferico', ['uses' => 'Select2SearchController@selectSearchPeriferico'])->name('SearchbyPeriferico');
				Route::post('/ajax-autocomplete-search-accesorio', ['uses' => 'Select2SearchController@selectSearchAccesorio'])->name('SearchbyAccesorio');
				Route::post('/ajax-autocomplete-search-merchandising', ['uses' => 'Select2SearchController@selectSearchMerchandising'])->name('SearchbyMerchandising');
			});

			Route::prefix('/PromotionCode')->group(function () {
				Route::post('/Check', ['uses' => 'FrontController@CheckPromotionCode'])->name('check_promotional_code');
			});
		});

		Route::group(['middleware' => ['authfrontend']], function () {

			Route::prefix('/Information')->group(function () {
				Route::get('/', ['uses' => 'FrontController@InformationIndex'])->name('information_index');
			});

			Route::prefix('/Upload')->group(function () {
				Route::get('/', ['uses' => 'FrontController@UploadRequestIndex'])->name('upload_product');
			});

			Route::prefix('/UploadSelect')->group(function () {
				Route::get('/', ['uses' => 'FrontController@UploadSelectIndex'])->name('upload_select');
			});

			Route::prefix('/PriceList')->group(function () {
				Route::get('/', ['uses' => 'FrontController@PriceListIndex'])->name('price_list_index');
			});

			Route::prefix('/ConfigUser')->group(function () {
				Route::get('/', ['uses' => 'FrontController@ConfigUserIndex'])->name('config_user');
				Route::get('/PersonalData', ['uses' => 'FrontController@ConfigUserPersonal'])->name('config_personal_data');
				Route::get('/Balance', ['uses' => 'FrontController@BalanceUserPersonal'])->name('balance_data');
				Route::get('/PublicProfile', ['uses' => 'FrontController@PublicProfilePersonal'])->name('public_profile_data');
				Route::get('/ReferralsData', ['uses' => 'FrontController@ReferralsDataIndex'])->name('referrals_index');
				Route::get('/AddCash', ['uses' => 'FrontController@AddCashIndex'])->name('add_cash');

				//Panel de Control - Control Panel
				Route::get('/Purchases', ['uses' => 'FrontController@PurchasesDataIndex'])->name('purchases_data');
				Route::get('/ReferralsData', ['uses' => 'FrontController@ReferralsDataIndex'])->name('referrals_index');


				Route::get('/PurchaseReserveDataProfile', ['uses' => 'FrontController@ReserveDataIndex'])->name('reserves_index');
				Route::get('/PurchasePaidDataProfile', ['uses' => 'FrontController@PaidDataIndex'])->name('paid_index');
				Route::get('/PurchaseSendDataProfile', ['uses' => 'FrontController@SendDataIndex'])->name('send_index');
				Route::get('/PurchaseDeliveredDataProfile', ['uses' => 'FrontController@DeliveredDataIndex'])->name('delivered_index');
				Route::get('/PurchaseNotDeliveredDataProfile', ['uses' => 'FrontController@NotDeliveredDataIndex'])->name('not_delivered_index');
				Route::get('/PurchaseCancelDataProfile', ['uses' => 'FrontController@CancelDataIndex'])->name('cancel_index');

				//Ventas
				Route::get('/Sales', ['uses' => 'FrontController@SalesDataIndex'])->name('sales_data');

				Route::get('/SaleReserveDataProfile', ['uses' => 'FrontController@SaleReserveDataIndex'])->name('sale_reserves_index');
				Route::get('/SalePaidDataProfile', ['uses' => 'FrontController@SalePaidDataIndex'])->name('sale_paid_index');
				Route::get('/SaleSendDataProfile', ['uses' => 'FrontController@SaleSendDataIndex'])->name('sale_send_index');
				Route::get('/SaleDeliveredDataProfile', ['uses' => 'FrontController@SaleDeliveredDataIndex'])->name('sale_delivered_index');
				Route::get('/SaleNotDeliveredDataProfile', ['uses' => 'FrontController@SaleNotDeliveredDataIndex'])->name('sale_not_delivered_index');
				Route::get('/SaleCancelDataProfile', ['uses' => 'FrontController@SaleCancelDataIndex'])->name('sale_cancel_index');
			});

			Route::prefix('/Complaint')->group(function () {
				Route::post('/complaint_update/{id}', ['uses' => 'FrontController@ComplaintUserInventory'])->name('complaint_user_inventory');
				Route::post('/complaint_product_update/{id}', ['uses' => 'FrontController@ComplaintProductInventory'])->name('complaint_product');
				Route::post('/complaint_user/{id}', ['uses' => 'FrontController@ComplaintUser'])->name('complaint_user');
			});

			Route::prefix('/ProductRequest')->group(function () {
				Route::get('/', ['uses' => 'FrontController@ProductRequestIndex'])->name('product_request');
				Route::get('/NotFound', ['uses' => 'FrontController@ProductNotFound'])->name('product_not_found');
				Route::get('/NotFoundAuction', ['uses' => 'FrontController@ProductAuctionNotFound'])->name('product_auction_not_found');
				Route::get('/NotFoundCollection', ['uses' => 'FrontController@ProductNotFoundCollection'])->name('product_not_found_collection');
				Route::get('/NotFoundAdmin', ['uses' => 'FrontController@ProductNotFoundAdmmin']);
				Route::post('/upload-image-via-ajax', 'FrontController@uploadImageViaAjax')->name('uploadImageViaAjax');
				Route::post('/SendToAdmin', ['uses' => 'FrontController@ProductRequestSendToAdmin'])->name('send_product_request');
				Route::post('/Exist', ['uses' => 'FrontController@ProductRequestSendToInventory'])->name('send_product_request_exist');
				Route::post('/ajax-autocomplete-search', ['uses' => 'Select2SearchController@selectSearchProduct'])->name('SearchbyProduct');
				Route::post('/ajax-autocomplete-search-game', ['uses' => 'Select2SearchController@selectSearchGame'])->name('SearchbyGame');
				Route::post('/ajax-autocomplete-search-console', ['uses' => 'Select2SearchController@selectSearchConsole'])->name('SearchbyConsole');
				Route::post('/ajax-autocomplete-search-periferico', ['uses' => 'Select2SearchController@selectSearchPeriferico'])->name('SearchbyPeriferico');
				Route::post('/ajax-autocomplete-search-accesorio', ['uses' => 'Select2SearchController@selectSearchAccesorio'])->name('SearchbyAccesorio');
				Route::post('/ajax-autocomplete-search-merchandising', ['uses' => 'Select2SearchController@selectSearchMerchandising'])->name('SearchbyMerchandising');
			});

			Route::prefix('/PromotionCode')->group(function () {
				Route::post('/Check', ['uses' => 'FrontController@CheckPromotionCode'])->name('check_promotional_code');
			});
		});

		//Subida de Imagenes para el perfil

		Route::post('/profile_upload_file/{id}', ['uses' => 'ProfileController@UpdateProfileImage'])->name('upload_profile_image');
		Route::post('/profile_cover_upload_file/{id}', ['uses' => 'ProfileController@UpdateCoverImage'])->name('upload_cover_image');
		Route::post('/profile_social_update/{id}', ['uses' => 'ProfileController@SocialNetworkStore'])->name('social_store');
		Route::post('/profile_social_network_update/{id}', ['uses' => 'ProfileController@SocialNetworkUpdate'])->name('social_network_update');
		//Paypal
		Route::get('/paypal/pay', 'PaymentController@payWithPayPal');
		Route::get('/paypal/status', 'PaymentController@payPalStatus');

		Route::get('/image', ['uses' => 'FrontController@showImage']);

		Route::get('/search/{text?}', ['uses' => 'FrontController@showSearchProduct']);

		Route::get('/MarketPlaceStore', ['uses' => 'FrontController@Marketplace'])->name('marketplace');

		Route::get('/testImage', ['uses' => 'FrontController@testImage']);

		Route::get('/fix/products', ['uses' => 'FrontController@fixProduct']);

		Route::post('/lang', ['uses' => 'FrontController@changeLangRetro']);

		Route::get('/product/search', ['uses' => 'FrontController@showSearchNewProduct']);

		//Route::get('/searchNew',['uses' => 'FrontController@showSearchNewProduct']);
		Route::get('/product/{id?}', ['uses' => 'FrontController@showProduct'])->name("product-show");
		Route::get('/product/inventory/{id?}', ['uses' => 'FrontController@showInventoryProduct'])->name("product-inventory-show");
		Route::get('/item/{id?}', ['uses' => 'FrontController@showProductItem']);
		Route::get('/contact-us', ['uses' => 'FrontController@showContactUs']);
		Route::post('/contact-us', ['uses' => 'FrontController@sendContactUs']);
		Route::get('/offerproduct/{text?}', ['uses' => 'FrontController@showOfferProduct']);
		Route::post('/get-products', ['uses' => 'FrontController@getProducts']);


		Route::get('/register-company', ['uses' => 'FrontController@showRegisterCompany']);
		Route::get('/register-user', ['uses' => 'FrontController@showRegisterUser']);
		Route::post('/register-account', ['uses' => 'FrontController@registerAccount']);
		Route::post('/register-company-account', ['uses' => 'FrontController@registerCompanyAccount']);
		Route::post('/register-user', ['uses' => 'FrontController@registerUser'])->name('registerUsers');
		Route::get('/reload-captcha', ['uses' => 'FrontController@reloadCaptcha']);
		Route::get('/account-activated/{token?}', ['uses' => 'FrontController@showAccountActivated']);

		Route::get('/show_password_reset', array('uses' => 'FrontController@showPasswordReset'))->name('passReset');
		Route::post('/re_password_reset', array('uses' => 'FrontController@passwordReset'))->name('passResetzero');

		Route::get('/show_user_reset', array('uses' => 'FrontController@showUserReset'))->name('userReset');
		Route::post('/user_reset', array('uses' => 'FrontController@userReset'))->name('resetUser');

		Route::get('/show_resend_activation', array('uses' => 'FrontController@show_resend_activation'))->name('ShowresendActivation');
		Route::post('/resend_activation', array('uses' => 'FrontController@resendActivation'))->name('resendActivation');

		Route::get('/create_new_password/{token?}', array('uses' => 'FrontController@showCreateNewPassword'));
		Route::post('/create_new_password', array('uses' => 'FrontController@createNewPassword'));

		//faqs & pages

		Route::get('/site/{link_page}', ['uses' => 'FrontController@showPublicFaqs']);

		Route::get('/user/{user}/invetory', ['uses' => 'FrontController@showPublicUserInventory'])->name('user-inventory');
		Route::post('/user/{user}/invetory', ['uses' => 'FrontController@showPublicUserInventory']);
		Route::get('/user/{user}', ['uses' => 'FrontController@showPublicProfile'])->name('user-info');
		Route::get('/user/{user}/positive', ['uses' => 'FrontController@showPublicProfilePositive']);
		Route::get('/user/{user}/neutral', ['uses' => 'FrontController@showPublicProfileNeutral']);
		Route::get('/user/{user}/negative', ['uses' => 'FrontController@showPublicProfileNegative']);

		Route::get('/listing/users', ['uses' => 'FrontController@showPublicProfileList'])->name('listUsers');
		Route::post('/listing/users', ['uses' => 'FrontController@showPublicProfileList']);
		Route::post('/filtroUsuario', ['uses' => 'FrontController@filtroUsuario'])->name('filtroUsuario');

		Route::prefix('/notifications')->group(function () {
			Route::get('/', ['uses' => 'FrontController@showNotifications']);
		});

		Route::prefix('/alerts')->group(function () {
			Route::get('/', ['uses' => 'FrontController@showAlerts']);
		});

		Route::prefix('/cart')->group(function () {
			Route::get('/', ['uses' => 'FrontController@showCart']);
			Route::post('/add_item', ['uses' => 'FrontController@cartAddItem']);
			Route::post('/add_shipping', ['uses' => 'FrontController@cartAddShipping']);
		});

		Route::prefix('/blog')->group(function () {
			Route::get('/', ['uses' => 'FrontController@showBlogs'])->name('blogview');
			Route::get('/view', ['uses' => 'FrontController@SingleBlog'])->name('singleblogview');
		});


		Route::prefix('account')->group(function () {

			Route::post('/login', array('uses' => 'LoginController@doLogin'));
			Route::get('/logout', array('uses' => 'LoginController@doLogOut'));

			Route::get('/new', ['uses' => 'FrontController@getAllProducts']);

			Route::group(['middleware' => ['auth', 'frontend']], function () {



				Route::get('/get/user', ['uses' => 'FrontController@getUsersConversation']);
				Route::post('/ajax-autocomplete-search', ['uses' => 'Select2SearchController@selectSearch'])->name('newUserMessage');
				Route::post('/SendMessageNewChat', 'ConversationController@storeProfileChat')->name('newMessageChat');
				Route::post('/SendMessageNew', 'ConversationController@storeProfile')->name('newMessage');
				Route::get('/api/messages', 'MessageController@index');
				Route::post('/api/messages', 'MessageController@store');
				Route::post('/api/messages/del', 'MessageController@removeSMS');
				Route::post('/api/messages/all', 'MessageController@removeAll');
				//Route::get('/',['uses' => 'FrontController@showAccount']);
				Route::post('/', ['uses' => 'FrontController@CheckAmount'])->name('withdrawals_amount');
				Route::get('/payment', ['uses' => 'FrontController@showPaymentAccount']);
				Route::get('/messages', ['uses' => 'FrontController@showMessages'])->name('messages_index');
				Route::get('/messages/user/{id?}', ['uses' => 'FrontController@showMessagesUser'])->name("chat_index");

				Route::get('/order/{order_id}', ['uses' => 'FrontController@showOrderStatus']);

				Route::prefix('/transactions')->group(function () {
					Route::get('/', ['uses' => 'FrontController@showTransactions'])->name('transaction_index');
					Route::post('/', ['uses' => 'FrontController@getTransactions']);
					Route::get('/generate-pdf/{id}', 'FrontController@generatePDF');
				});


				Route::prefix('/profile')->group(function () {
					Route::get('/', ['uses' => 'FrontController@showProfile']);
					Route::get('/positive', ['uses' => 'FrontController@showAccountProfilePositive']);
					Route::get('/neutral', ['uses' => 'FrontController@showAccountProfileNeutral']);
					Route::get('/negative', ['uses' => 'FrontController@showAccountProfileNegative']);
					Route::get('/list-inv', ['uses' => 'FrontController@showListInventory']);
					//Route::get('/{user}',['uses' => 'FrontController@showAccountProfileNegative']);
				});

				Route::prefix('/inventory')->group(function () {
					Route::get('/', ['uses' => 'FrontController@showInventory']);
					Route::get('/list-dt', ['uses' => 'FrontController@dtAccountInventory']);
					Route::get('/edit/{id}', ['uses' => 'FrontController@editInventory'])->name('inventory_edit');
					Route::get('/edit_normal/{id}', ['uses' => 'FrontController@editNormalInventory'])->name('normal_inventory_edit');
					// GAMES
					Route::get('/add/{product_id?}', ['uses' => 'FrontController@showInventoryItem']);
					Route::post('/add/{product_id?}', ['uses' => 'FrontController@saveInventoryItem']);
					Route::get('/modify/{id?}', ['uses' => 'FrontController@showInventoryItem']);
					Route::put('/update/{id}', ['uses' => 'FrontController@updateInventoryItem'])->name("update_inventory");
					Route::get('/update/{id?}/{qty?}', ['uses' => 'FrontController@editInventory']);

					Route::get('/edit/{id}/{qty?}', ['uses' => 'FrontController@editInventory'])->name('inventory_update');

					// CONSOLES
					/*
					Route::get('/add/{product_id?}',['uses' => 'FrontController@showInventoryItem']);
					Route::post('/add/{product_id?}',['uses' => 'FrontController@saveInventoryItem']);
					Route::get('/modify/{id?}',['uses' => 'FrontController@showInventoryItem']);
					Route::get('/update/{id?}/{qty?}',['uses' => 'FrontController@showInventoryItem']);
					*/
					Route::post('/delete', ['uses' => 'FrontController@deleteInventoryItem']);

					Route::post('/upload_file', ['uses' => 'FrontController@uploadInventoryImage']);

					Route::post('/upload_file_request', ['uses' => 'FrontController@uploadInventoryImageRequest']);

					Route::post('/remove_file', ['uses' => 'FrontController@removeInventoryImage']);

					Route::post('/upload_file_ean', ['uses' => 'FrontController@uploadInventoryImageEan']);
				});

				Route::prefix('/sales')->group(function () {
					Route::get('/', ['uses' => 'FrontController@showSales']);

					Route::get('/list-dt', ['uses' => 'FrontController@dtAccountSales']);
					Route::get('/list', ['uses' => 'FrontController@listSales']);
					Route::get('/view/{id}', ['uses' => 'FrontController@showAccountSaleDetails']);

					Route::post('/update', ['uses' => 'FrontController@updateShippingSales']);
					Route::post('/status', ['uses' => 'FrontController@updateShippingStatus'])->name('statusShippingReceived');
					Route::post('/status-send', ['uses' => 'FrontController@SendUpdate'])->name('SendStatus');
					Route::post('/status-received', ['uses' => 'FrontController@updateShippingStatus'])->name('ReceivedStatus');
					Route::post('/view/{id}', ['uses' => 'FrontController@saveOrderCancel']);
				});




				Route::prefix('/purchases')->group(function () {
					Route::get('/', ['uses' => 'FrontController@showPurchases']);

					Route::get('/list-dt', ['uses' => 'FrontController@dtAccountPurchases']);
					Route::get('/view/{id}', ['uses' => 'FrontController@showAccountPurchaseDetails']);

					Route::post('/view/{id}', ['uses' => 'FrontController@saveOrderCancel']);

					Route::get('/view/{id}/score', ['uses' => 'FrontController@showOrderRating']);
					Route::post('/view/{id}/score', ['uses' => 'FrontController@saveOrderRating']);

					Route::get('/view/{id}/cancel', ['uses' => 'FrontController@showOrderCancel']);


					Route::post('/cancel', ['uses' => 'FrontController@cancelAccountPurchases']);
					Route::post('/complete', ['uses' => 'FrontController@completeAccountPurchases']);
				});

				Route::get('/banks', ['uses' => 'FrontController@showAccountBank']);
				Route::post('/banks', ['uses' => 'FrontController@updateAccountBank']);

				Route::get('/settings', ['uses' => 'FrontController@showAccountSettings']);
			});
		});

		Route::group(['middleware' => ['auth', 'frontend']], function () {

			Route::get('/product/ajax', ['uses' => 'FrontController@ajaxProduct'])->name('ajaxProduct');

			Route::get('/get/data', ['uses' => 'FrontController@getData'])->name('getData');


			Route::post('/change/email', ['uses' => 'FrontController@updateAccountEmail']);
			Route::post('/change/information', ['uses' => 'FrontController@updateAccountInformation']);
			Route::post('/change/pass', ['uses' => 'FrontController@updateAccountPass']);
			Route::post('/change/info', ['uses' => 'FrontController@updateAccountBank']);
			Route::get('/checkout', ['uses' => 'FrontController@showCheckout']);
			Route::post('/cartComplete', ['uses' => 'FrontController@storeCheckout']);
			Route::post('/cartCompleteCheck', ['uses' => 'FrontController@storeCompleteCheckout']);
			Route::post('/process_checkout', ['uses' => 'FrontController@processCheckoutNew']);
			Route::post('/process_checkout_new/{id}', ['uses' => 'FrontController@processCheckoutNew']);
			Route::get('/success_checkout', ['uses' => 'FrontController@showSuccessCheckoutNew']);
			Route::get('/success_checkout_new/{id}', ['uses' => 'FrontController@showSuccessCheckoutNew']);
			Route::get('/order_complete/{id}', ['uses' => 'FrontController@showSuccessOrder']);



			Route::post('/refund', ['uses' => 'FrontController@requestRefund'])->name('requestRefund');
			Route::post('/refund/response', ['uses' => 'FrontController@responseRefund'])->name('responseRefund');
		});

		Route::get('/activate_account/{token?}', array('uses' => 'UserController@activateAccount'));


		// Admin routes
		Route::prefix('11w5Cj9WDAjnzlg0')->group(function () {


			// Routes for Login Screen
			Route::get('/', array('uses' => 'LoginController@showLogin'));

			Route::post('/login', array('uses' => 'LoginController@doLogin'));

			Route::get('/password_reset', array('uses' => 'UserController@showPasswordReset'));

			Route::post('/password_reset', array('uses' => 'UserController@passwordReset'));

			Route::get('/create_new_password/{token?}', array('uses' => 'UserController@showCreateNewPassword'));

			Route::post('/create_new_password', array('uses' => 'UserController@createNewPassword'));

			Route::get('/register', array('uses' => 'UserController@showNewUserRegistration'));

			Route::post('/register', array('uses' => 'UserController@newUserRegistration'));

			Route::get('/show_resend_activation', array('uses' => 'UserController@showResendActivation'));

			Route::post('/resend_activation', array('uses' => 'UserController@resendActivation'));

			Route::post('/gestion', ['uses' => 'MarketController@requestGestion'])->name('requestGestion');
			Route::post('/solucion', ['uses' => 'MarketController@requestSolucion'])->name('requestSolucion');

			Route::group(['middleware' => ['auth', 'privilege']], function () {

				// Routes for application

				Route::get('/dashboard', 'HomeController@showDashboard');
				Route::get('/withdrawal', 'HomeController@showWithdrawal');

				Route::get('/showwiths', ['uses' => 'HomeController@showCredit']);
				Route::get('/showwithsped', ['uses' => 'HomeController@showPed']);
				Route::get('/showfees', ['uses' => 'HomeController@showFees']);
				Route::get('/showpur', ['uses' => 'HomeController@showPur']);

				Route::get('/list', 'HomeController@showList');

				Route::get('/configuration/lists', 'SystemController@showLists');
				Route::get('/configuration/lists-dt', 'SystemController@dtLists');
				Route::get('/configuration/list/{id?}', ['as' => '11w5Cj9WDAjnzlg0/configuration/list', 'uses' => 'SystemController@showList']);
				Route::post('/configuration/save_list', 'SystemController@saveList');
				Route::post('/configuration/delete_list', 'SystemController@deleteList');

				Route::get('/messages', 'MessageController@showMessages');
				Route::post('/messagesUs', 'MessageController@showUserMessages');
				// User management
				Route::prefix('users')->group(function () {

					Route::get('/', 'UserController@showUsers');
					Route::get('/info/{id}', ['as' => '11w5Cj9WDAjnzlg0/users/info', 'uses' => 'UserController@showInfoUsers']);
					Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/users', 'uses' => 'UserController@dtUsers']);
					Route::get('/add', 'UserController@showUser');
					Route::get('/modify/{id}', ['uses' => 'UserController@showUser']);
					Route::post('/delete', 'UserController@deleteUser');
					Route::get('/logout', array('uses' => 'LoginController@doLogOut'));
					Route::post('/AddCredit', 'UserController@storeCreditUser');
					Route::prefix('privileges')->group(function () {
						Route::get('/', 'UserController@showPrivileges');
						Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/users/privileges', 'uses' => 'UserController@dtPrivileges']);
						Route::get('/modify/{id?}', ['uses' => 'UserController@showPrivilege']);
						Route::post('/modify', 'UserController@savePrivilege');
					});

					Route::prefix('roles')->group(function () {
						Route::get('/', 'UserController@showRoles');
						Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/users/roles', 'uses' => 'UserController@dtRoles']);
						Route::get('/add', 'UserController@showRole');
						Route::post('/save', ['as' => '11w5Cj9WDAjnzlg0/users/roles/add', 'uses' => 'UserController@saveRole']);
						Route::get('/modify/{id}', ['uses' => 'UserController@showRole']);
						Route::post('/modify', 'UserController@saveRole');
						Route::post('/delete', 'UserController@deleteRole');
					});

					Route::prefix('profile')->group(function () {
						Route::get('/{id?}', 'UserController@showProfile');
						Route::post('/save_profile', 'UserController@saveProfile');
						Route::post('/upload_image', 'UserController@saveProfilePicture');
					});
				});

				Route::get('/utils/sidebar_collapse', 'UtilController@changeSideBar');

				Route::prefix('marketplace')->group(function () {

					//Route::get('/orders',['as' => '11w5Cj9WDAjnzlg0/marketplace/orders', 'uses' => 'MarketController@showOrders'] );

					Route::prefix('orders')->group(function () {
						Route::get('/', 'MarketController@showOrders');
						Route::get('/not', 'MarketController@showOrdersNot');
						Route::post('/', 'MarketController@showOrders');
					});

					// Market Place Products
					Route::prefix('products')->group(function () {
						Route::get('/', 'MarketController@showProducts');
						Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/products', 'uses' => 'MarketController@dtProducts']);

						Route::get('/list-search/{id}', ['as' => '11w5Cj9WDAjnzlg0/marketplace/products/search', 'uses' => 'MarketController@searchProducts']);

						Route::get('/add', 'MarketController@showProduct');
						Route::post('/save', ['as' => '11w5Cj9WDAjnzlg0/marketplace/products/add', 'uses' => 'MarketController@saveProduct']);
						Route::get('/modify/{id}', ['as' => '11w5Cj9WDAjnzlg0/marketplace/products/modify', 'uses' => 'MarketController@showProduct']);

						Route::post('/modify', 'MarketController@saveProduct');
						Route::post('/delete', 'MarketController@deleteProduct');

						Route::get('/images/{id}', ['uses' => 'MarketController@imageProduct']);
						Route::post('/images', ['uses' => 'MarketController@saveImagesProduct']);
						Route::post('/upload_file', ['uses' => 'MarketController@uploadProductImage']);
						Route::post('/remove_file', ['uses' => 'MarketController@removeProductImage']);
					});
					// Market Place Shipping
					Route::prefix('shipping')->group(function () {
						Route::get('/', 'MarketController@showShipping');
						Route::get('/list-dt', ['uses' => 'MarketController@listShipping']);
						Route::get('/add', ['uses' => 'MarketController@AddShowShipping']);
						Route::post('/add', 'MarketController@storeShipping');
						Route::get('/modify/{id}', ['as' => '11w5Cj9WDAjnzlg0/marketplace/shipping/modify', 'uses' => 'MarketController@updateShipping']);
						Route::post('/modify/{id}', ['uses' => 'MarketController@saveShipping']);
						Route::post('/delete', 'MarketController@deleteShipping');
					});
					// Market Place Inventory
					Route::prefix('inventory')->group(function () {
						Route::get('/', 'MarketController@showInventory');

						Route::get('/ean', 'MarketController@showInventoryEan');

						Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/inventory', 'uses' => 'MarketController@dtInventory']);
					});


					// Market Place Categories
					Route::prefix('categories')->group(function () {
						Route::get('/', 'MarketController@showCategories');
						Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/categories', 'uses' => 'MarketController@dtCategories']);
						Route::get('/add', 'MarketController@showCategory');
						Route::post('/save', ['as' => '11w5Cj9WDAjnzlg0/marketplace/categories/add', 'uses' => 'MarketController@saveCategory']);
						Route::get('/modify/{id}', ['as' => '11w5Cj9WDAjnzlg0/marketplace/categories/modify', 'uses' => 'MarketController@showCategory']);
						Route::post('/modify', 'MarketController@saveCategory');
						Route::post('/delete', 'MarketController@deleteCategory');
						Route::post('/upload_image', ['uses' => 'MarketController@saveCategoryImage']);
					});
					// Market Place Pages
					Route::prefix('pages')->group(function () {
						Route::get('/', 'MarketController@showPages');
						Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/pages', 'uses' => 'MarketController@dtPages']);
						Route::get('/add', ['uses' => 'MarketController@showPage']);
						Route::post('/save', ['as' => '11w5Cj9WDAjnzlg0/marketplace/pages/add', 'uses' => 'MarketController@savePage']);
						Route::get('/modify/{id}', ['as' => '11w5Cj9WDAjnzlg0/marketplace/pages/modify', 'uses' => 'MarketController@showPage']);
						Route::post('/modify', 'MarketController@savePage');
						Route::post('/delete', 'MarketController@deletePage');
					});
					// Market Place Footer Links
					Route::prefix('links')->group(function () {
						Route::get('/', 'MarketController@showLinks');
						Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/links', 'uses' => 'MarketController@dtLinks']);
						Route::get('/add', 'MarketController@showLink');
						Route::post('/save', ['uses' => 'MarketController@saveLink']);
						Route::get('/modify/{id}', ['uses' => 'MarketController@showLink']);
						Route::post('/modify', 'MarketController@saveLink');
						Route::post('/delete', 'MarketController@deleteLink');
					});
					// Market Place Footer Faqs
					Route::prefix('faqs')->group(function () {
						Route::get('/', 'MarketController@showFaqs');
						Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/faqs', 'uses' => 'MarketController@dtFaqs']);
						Route::get('/add', 'MarketController@showFaq');
						Route::post('/save', ['as' => '11w5Cj9WDAjnzlg0/marketplace/faqs/add', 'uses' => 'MarketController@saveFaq']);
						Route::get('/modify/{id}', ['as' => '11w5Cj9WDAjnzlg0/marketplace/faqs/modify', 'uses' => 'MarketController@showFaq']);
						Route::post('/modify', 'MarketController@saveFaq');
						Route::post('/delete', 'MarketController@deleteFaq');
					});
					// Market Place Carousel
					Route::prefix('carousels')->group(function () {
						Route::get('/', 'MarketController@showCarousels');
						Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/carousels', 'uses' => 'MarketController@dtCarousels']);
						Route::get('/add', 'MarketController@showCarousel');
						Route::post('/save', ['as' => '11w5Cj9WDAjnzlg0/marketplace/carousels/add', 'uses' => 'MarketController@saveCarousel']);
						Route::get('/modify/{id}', ['as' => '11w5Cj9WDAjnzlg0/marketplace/carousels/modify', 'uses' => 'MarketController@showCarousel']);
						Route::post('/modify', 'MarketController@saveCarousel');
						Route::post('/delete', 'MarketController@deleteCarousel');
						Route::post('/upload_image', ['uses' => 'MarketController@saveCarouselImage']);
					});
					// Market Place Banners
					Route::prefix('banners')->group(function () {
						Route::get('/', 'MarketController@showBanners');
						Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/banners', 'uses' => 'MarketController@dtBanners']);
						Route::get('/add', 'MarketController@showBanner');
						Route::post('/save', ['as' => '11w5Cj9WDAjnzlg0/marketplace/banners/add', 'uses' => 'MarketController@saveBanner']);
						Route::get('/modify/{id}', ['as' => '11w5Cj9WDAjnzlg0/marketplace/banners/modify', 'uses' => 'MarketController@showBanner']);
						Route::post('/modify', 'MarketController@saveBanner');
						Route::post('/delete', 'MarketController@deleteBanner');
						Route::post('/upload_image', ['uses' => 'MarketController@saveBannerImage']);
					});
					// Market Place News
					Route::prefix('news')->group(function () {
						Route::get('/', 'MarketController@showNews');
						Route::get('/list-dt', ['as' => '11w5Cj9WDAjnzlg0/marketplace/news', 'uses' => 'MarketController@dtNews']);
						Route::get('/add', 'MarketController@showNewsItem');
						Route::post('/save', ['as' => '11w5Cj9WDAjnzlg0/marketplace/news/add', 'uses' => 'MarketController@saveNewsItem']);
						Route::get('/modify/{id}', ['uses' => 'MarketController@showNewsItem']);
						Route::post('/modify', 'MarketController@saveNewsItem');
						Route::post('/delete', 'MarketController@deleteNewsItem');
						Route::post('/upload_image', ['uses' => 'MarketController@saveNewsItemImage']);
					});

					// Market Place News
					Route::prefix('blogs')->group(function () {
						Route::get('/', 'MarketController@showBlogs');
						Route::get('/list-dt', ['uses' => 'MarketController@dtBlogs']);
						Route::get('/add', 'MarketController@showBlogsItem');
						Route::post('/save', ['uses' => 'MarketController@saveBlogsItem']);
						Route::get('/modify/{id}', ['uses' => 'MarketController@showBlogsItem']);
						Route::post('/modify', 'MarketController@saveBlogsItem');
						Route::post('/delete', 'MarketController@deleteBlogsItem');
						Route::post('/upload_image', ['uses' => 'MarketController@saveBlogsItemImage']);
					});
				});

				// Organizations configuration

				// Organizations for individuals

				Route::prefix('configuration')->group(function () {
					Route::prefix('organizations')->group(function () {
						Route::get('/', 'SystemController@showOrganizations');
					});
				});

				Route::get('/organizations/{org_id}/info', ['as' => '11w5Cj9WDAjnzlg0/organizations/info', 'uses' => 'OrganizationController@showOrganizationInfo']);
				Route::post('/organizations/{org_id}/save_info', ['as' => '11w5Cj9WDAjnzlg0/organizations/save_info', 'uses' => 'OrganizationController@saveOrganizationInfo']);


				Route::get('/my_store/{user_id}/configuration', ['as' => '11w5Cj9WDAjnzlg0/my_store/configuration', 'uses' => 'MyStoreController@showConfiguration']);
				Route::post('/my_store/{user_id}/save_configuration', ['uses' => 'MyStoreController@saveConfiguration']);

				Route::get('/my_store/{user_id}/inventory', ['as' => '11w5Cj9WDAjnzlg0/my_store/inventory', 'uses' => 'MyStoreController@showInventories']);
				Route::get('/my_store/{user_id}/inventory/list-dt', ['uses' => 'MyStoreController@dtInventories']);
				Route::get('/my_store/{user_id}/add_inventory', ['uses' => 'MyStoreController@showInventory']);
				Route::post('/my_store/{user_id}/save_inventory', ['uses' => 'MyStoreController@saveInventory']);
				Route::get('/my_store/{user_id}/modify_inventory/{id}', ['uses' => 'MyStoreController@showInventory']);
				Route::post('/my_store/{user_id}/modify_inventory', ['uses' => 'MyStoreController@saveInventory']);
				Route::post('/my_store/{user_id}/delete_inventory', ['uses' => 'MyStoreController@deleteInventory']);
				Route::post('/my_store/inventory/upload_file', ['uses' => 'MyStoreController@uploadInventoryImages']);
				Route::post('/my_store/inventory/remove_file', ['uses' => 'MyStoreController@removeInventoryImages']);

				Route::get('/my_store/{user_id}/orders', ['as' => '11w5Cj9WDAjnzlg0/my_store/orders', 'uses' => 'MyStoreController@showOrders']);
				Route::get('/my_store/{user_id}/orders/list-dt', ['uses' => 'MyStoreController@dtOrders']);
				Route::get('/my_store/{user_id}/modify_order/{id}', ['uses' => 'MyStoreController@showOrder']);
				Route::post('/my_store/{user_id}/modify_order', ['uses' => 'MyStoreController@saveOrder']);
			});
		});
 
}
