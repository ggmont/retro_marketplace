<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\SysUserPasswordReset;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Mail;
use DB;
use App\Mail\ActivateAccount;
use App\SysUser;
use App\AppOrgUserInventory;
use App\AppOrgCartUser;
use App\SysUserAccountActivation;
use App\Events\ActionExecuted;
use App\Helpers\PtyCommons;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Http\Request;

class LoginController extends Controller
{
	function getUserIP()
	{
		if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') > 0) {
				$addr = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
				return trim($addr[0]);
			} else {
				return $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
		} else {
			return $_SERVER['REMOTE_ADDR'];
		}
	}


	public function doLogin(Request $request)
	{
		// Validar la entrada
		$this->validate($request, [
			'user_name' => 'required|min:3|max:255',
			'password' => 'required|min:8',
		]);
	
		$userdata = [
			'user_name' => $request->user_name,
			'password' => $request->password,
		];
	
		$remember = $request->has('remember_me');


		if (Auth::attempt($userdata, $remember)) {
			
			if (Auth::user()->is_activated === 'N') {
				$email = Auth::user()->email;
				$user = SysUser::where('email', $email)->first();
	
				if ($user) {
					$actRequest = SysUserAccountActivation::where('email', $email)->first();
	
					if (!$actRequest) {
						$actRequest = new SysUserPasswordReset();
					}
	
					$actRequest->email = $email;
					$actRequest->token = str_random(100);
					$actRequest->save();
	
					Mail::to($actRequest->email)->queue(new ActivateAccount($actRequest->token, $user->first_name . ' ' . $user->last_name));
	
					event(new ActionExecuted('events.user_activate_acc_sent', null, null, Request::ip(), array('user_name' => $user->email)));
				}
	
				Auth::logout();
	
				return redirect('/')->with([
					'flash_class' => 'alert-danger',
					'flash_message' => 'Tu cuenta aún no está activada. Se te ha enviado un correo para poder activarla.',
				]);
			}
	
			if (Auth::user()->is_enabled === 'N') {
				Auth::logout();
	
				return redirect('/')->with([
					'flash_class' => 'alert-danger',
					'flash_message' => 'Tu cuenta se encuentra desactivada. Contacte con el soporte.',
				]);
			}
	
			if ($remember) {
				// Si se seleccionó "Recordarme", se establece una cookie con los datos de inicio de sesión
				$cookie = cookie('remember_me', json_encode(['user_name' => $request->user_name, 'password' => $request->password]), 60 * 24 * 30);
				// La cookie dura 30 días
				return redirect('/')->withCookie($cookie);
			} else {
				// Si no se seleccionó "Recordarme", se establecen los datos de inicio de sesión en la sesión
				Session::put('user_name', $userdata['user_name']);
				Session::put('user_id', Auth::id());
				PtyCommons::setUserEvent(Auth::id(), 'Inicio sesión el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP()]);
			}
	
			return redirect('/');
		}
	
		return redirect('/')->with([
			'flash_class' => 'alert-danger',
			'flash_message' => 'Credenciales incorrectas. Por favor, inténtalo de nuevo.',
		]);
	}
	
	
 

	public function doLogout()
	{
		$details = Auth::user()->cart->details->groupBy('inventory_id');
	
		foreach ($details as $inventory_id => $items) {
			$inventory = AppOrgUserInventory::find($inventory_id);
			$inventory->quantity += $items->sum('quantity');
			$inventory->save();
		}
	
		AppOrgCartUser::where('id', Auth::user()->cart->id)
			->update(['status' => 'CS']);
	
		Auth::logout();
	
		return redirect('/');
	}
	
	
}