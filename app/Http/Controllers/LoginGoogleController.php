<?php namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use Redirect;
use Request;
use Response;
use Session;
use Validator;
use Mail;
use Socialite;

use App\Mail\ActivateAccount;
use App\SysUser;
use App\AppOrgUserInventory;
use App\AppOrgCartUser;
use App\SysUserAccountActivation;

use App\SysUserRoles;
use App\SysAccountBank;

use App\Events\ActionExecuted;
use App\Helpers\PtyCommons;

class LoginGoogleController extends Controller {

	/**
	 * Redirect the user to the Google authentication page.
	*
	* @return \Illuminate\Http\Response
	*/
	public function redirectToProvider()
	{
		return Socialite::driver('google')->redirect();
	}

	/**
	 * Obtain the user information from Google.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function handleProviderCallback()
	{
		try {
				$user = Socialite::driver('google')->user();
		} catch (\Exception $e) {
				return redirect('/');
		}
		// only allow people with @company.com to login
		if(explode("@", $user->email)[1] !== 'gmail.com'){
			return redirect()->to('/');
		}
		$existingUser = SysUser::where('email', $user->email)->first();

		if($existingUser){
			auth()->login($existingUser, true);
		} else {
		// create a new user

			$user_name = explode("@", $user->email)[0];
			$new_user_name = '';
			if(SysUser::where('user_name', $user_name)->first()){
				$stat = true; 
				do {
					$new_user_name = $user_name . '-' . rand(1000, 9999);
					//$user_name = '3593' . rand(100000000000, 999999999999);
					$tar = SysUser::where('user_name', $new_user_name)->first();
					if( ! $tar)
						$stat = false; 
				} while ($stat == true);
			}
			$username = $new_user_name != '' ? $new_user_name : $user_name;

			$random = (string) random_int(10000, 99999);

			$newUser                  = new SysUser();
			$newUser->first_name      = $user->user['given_name'];
			$newUser->last_name       = $user->user['family_name'];
			$newUser->display_name    = $user->name;
			$newUser->user_name       = $username;
			$newUser->email           = $user->email;
			$newUser->google_id       = $user->id;
			$newUser->is_activated    = 'Y';
			$newUser->concept         = $username . '-' . $random;;
			$newUser->avatar          = $user->avatar;
			$newUser->avatar_original = $user->avatar_original;
		
			if($newUser->save()){

				$newUserRol = new SysUserRoles;
				$newUserRol->user_id    = $newUser->id;
				$newUserRol->role_id    = 4;
				$newUserRol->save();

				$newAccountBank = new SysAccountBank;
				$newAccountBank->user_id = $newUser->id;
				$newAccountBank->beneficiary= $newUser->first_name . ' ' . $newUser->last_name ;
				$newAccountBank->save();

				PtyCommons::setUserEvent($newUser->id, 'Cuenta creada el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP()]);

				auth()->login($newUser, true);
			}
			
		}
		return redirect()->to('/account/profile');
		
        //return redirect()->to('/home');
	}
	
	function getUserIP() {
		if( array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
			if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')>0) {
				$addr = explode(",",$_SERVER['HTTP_X_FORWARDED_FOR']);
				return trim($addr[0]);
			} else {
				return $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
		}
		else {
			return $_SERVER['REMOTE_ADDR'];
		}
	}

}
