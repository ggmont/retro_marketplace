<?php namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use Config;
use Datatables;
use DB;
use Hash;
use Image;
use Input;
use Mail;
use Redirect;
use Request;
use Response;
use Session;
use Storage;
use Validator;
use View;

use Illuminate\Http\Request as FilterRequest;
use App\AppOrgUserInventory;
use App\AppOrgOrderPayment;
use App\AppOrgHistorialPromotionalCode;
use App\AppOrgOrder;
use App\SysOrganization;
use App\SysPrivileges;
use App\SysRolePrivileges;
use App\SysRoles;
use App\SysUser;
use App\SysUserRoles;
use App\SysUserReferred;
use App\SysUserPasswordReset;
use App\SysUserAccountActivation;
use App\Events\ActionExecuted;
use App\MyModel;
use App\Mail\ActivateAccount;
use App\Mail\PasswordReset;
use App\Helpers\PtyCommons;
use App\AppOrgUserEvent;
use App\AppOrgUserRating;

use App\Mail\OrderPaymentComplete;
use App\Mail\OrderPaymentBuyer;

class UserController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| User Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "User View" for the application and
	| is configured to only allow registered users. Like most of the other.
	|
	*/

	// Data to be used within the view
	private $viewData = [];

	// Column prefix for the form fields
	private $columnPrefix = '';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->columnPrefix =  Config::get('brcode.column_prefix');
		$this->viewData['column_prefix'] = $this->columnPrefix;
	}

	/**
	 * Set the initial values for the views
	 *
	 * @return void
	 */
	private function setInitialData() {
		$this->viewData['first_name'] = Auth::user()->first_name;
		$this->viewData['user_full_name'] = Auth::user()->first_name . ' ' . Auth::user()->last_name;
		$this->viewData['model_id'] = 0;
	}

	/*
	|--------------------------------------------------------------------------
	| Password recovery
	|--------------------------------------------------------------------------
	|
	*/

	/**
	 * Show the view that allows users to request a password reset
	 *
	 * @return view
	 */
	public function showPasswordReset() {

		if( ! Config::get('brcode.app_allow_pass_recover') ) {
			abort(404);
		}

		return view('brcode.users.password_reset');
	}

	/**
	 * Process the password reset request
	 *
	 * @return redirect(back if fails | login if success)
	 */
	public function passwordReset() {
		$input = Request::all();

		$rules = [
			'user_email' 	=> 'required|email',
		];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {
			$email = $input['user_email'];
			$user = SysUser::where('email',$email)->first();

			if($user) {

				if($user->is_activated == 'N') {
					event(new ActionExecuted('events.user_pwd_r',null,null, Request::ip(),array('user_name'=> $user->email )));

					return redirect('/11w5Cj9WDAjnzlg0')->with('login_message',['type' => 'warning', 'message' => str_replace('_URL_',url('/resend_activation'),trans('login.activate_acc_pending') ) ]);
				}

				$passReset = SysUserPasswordReset::where('email',$email)->first();

				if( ! $passReset) {
					$passReset = new SysUserPasswordReset();
				}

				$passReset->email = $email;
				$passReset->token = str_random(100);
				$passReset->save();

				Mail::to($user->email)->queue(new PasswordReset($passReset->token, $user->first_name . ' ' . $user->last_name));

				event(new ActionExecuted('events.user_password_reset_snt',null,null, Request::ip(),array('user_name'=> $email )));

				return redirect('/11w5Cj9WDAjnzlg0')->with('login_message',['type' => 'success', 'message' => trans('login.password_reset_sent')]);

			}
			else {

				event(new ActionExecuted('events.user_password_reset_unknown',null,null, Request::ip(),array('user_name'=> $email )));

				return Redirect::back()
					->withErrors($v->errors())
					->with('login_message',['type' => 'warning', 'message' => trans('login.email_not_registered')]);
			}

		}

	}

	/**
	 * Show the view that allows users to create a new password
	 *
	 * @return view
	 */
	public function showCreateNewPassword($token) {

		if( ! Config::get('brcode.app_allow_pass_recover')) {
			abort(404);
		}

		$passReset = SysUserPasswordReset::where('token',$token)->first();

		$this->viewData['token'] = '';

		if($passReset) {
			$this->viewData['token'] = $passReset->token;
			$userEmail = $passReset->email;
		}

		return view('brcode.users.create_new_password', compact('token', 'userEmail'))->with('viewData',$this->viewData);

	}

	/**
	 * Save the new password of the user
	 *
	 * @return redirect(back if fails | login if success)
	 */
	public function createNewPassword() {
		$input = Request::all();
	
		$rules = [
			'password' => 'required|min:5|max:15|confirmed',
		];
	
		$v = Validator::make($input, $rules);
	
		if ($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		} else {
			$token = $input['_resetToken'];
	
			$passReset = SysUserPasswordReset::where('token', $token)->first();
	
			if ($passReset) {
				// Si el campo 'user_email' está presente en el formulario, lo usamos, de lo contrario, utilizamos el correo del token
				$email = $input['user_email'] ?? $passReset->email;
	
				$user = SysUser::where('email', $email)->first();
	
				if ($user) {
					$user->password = Hash::make($input['password']);
					$user->save();
	
					$passReset->delete();
	
					event(new ActionExecuted('events.user_password_reset_snt', null, null, Request::ip(), ['user_name' => $passReset->email]));
	
					return redirect('/')
						->with([
							'flash_class' => 'alert-danger',
							'flash_message' => 'Contraseña cambiada satisfactoriamente. Inicia sesión para comprobar.',
						]);
				} else {
					return Redirect::back()
						->withErrors($v->errors())
						->with('login_message', ['type' => 'warning', 'message' => trans('app.error_unknown')]);
				}
			} else {
				return Redirect::back()
					->withErrors($v->errors())
					->with('login_message', ['type' => 'warning', 'message' => trans('login.email_pwd_reset_unrelated')]);
			}
		}
	}
	
	

	/*
	|--------------------------------------------------------------------------
	| New user registration
	|--------------------------------------------------------------------------
	|
	*/

	/**
	 * Show the view that allows users to register a new membership
	 *
	 * @return view
	 */
	public function showNewUserRegistration() {
		if( ! Config::get('brcode.app_allow_registration')) {
			abort(404);
		}

		return view('brcode.users.register');
	}

	/**
	 * Process the new registration and send email for activation
	 *
	 * @return redirect(back if fails | login if success)
	 */
	public function newUserRegistration() {
		$input = Request::all();

		$rules = [
			'user_first_name' 		=> 'required|min:2',
			'user_last_name' 			=> 'required|min:2',
			'user_name' 					=> 'required|min:5|max:15|unique:sysUsers,user_name',
			'user_email' 					=> 'required|email|unique:sysUsers,email',
			'password' 						=> 'required|min:5|max:15|confirmed',
			'terms' 							=> 'accepted',
		];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		} else {

			$newUser = new SysUser();

			$newUser->first_name 			= $input['user_first_name'];
			$newUser->last_name 			= $input['user_last_name'];
			$newUser->email 					= $input['user_email'];
			$newUser->user_name 			= $input['user_name'];
			$newUser->password 				= Hash::make($input['password']);

			$newUser->save();

			$actRequest = SysUserAccountActivation::where('email',$newUser->email)->first();

			if( ! $actRequest) {
				$actRequest = new SysUserAccountActivation();
			}

			$actRequest->email = $newUser->email;
			$actRequest->token = str_random(100);
			$actRequest->save();

			$organization = new SysOrganization();

			$organization->name 		= $newUser->user_name . '\'s organization';
			$organization->user_id 	= $newUser->id;

			$organization->save();

			Mail::to($newUser->email)->queue(new ActivateAccount($actRequest->token, $newUser->first_name . ' ' . $newUser->last_name));

			event(new ActionExecuted('events.user_activate_acc_sent',null,null, Request::ip(),array('user_name'=> $newUser->email )));

			return redirect('/11w5Cj9WDAjnzlg0')->with('login_message',['type' => 'success', 'message' => trans('login.activate_acc_sent')]);

		}
	}

	/**
	 * Process the received token and activate the related account
	 *
	 * @return redirect(login)
	 */
	public function activateAccount($token) {
		if( ! Config::get('brcode.app_allow_registration')) {
			abort(404);
		}

		//dd($token);

		$actRequest = SysUserAccountActivation::where('token', $token)->first();

		//dd($actRequest);

		if( ! $actRequest) {
			abort(404);
		}

		$user = SysUser::where('email',$actRequest->email)->first();

		//dd($user);

		if( ! $user) {
			abort(404);
		}

		$user->is_activated = 'Y';
		$user->save();

		$actRequest->delete();

		//session()->put('allow_account_activated', true);

		PtyCommons::setUserEvent($user->id, 'Cuenta activada el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP()]);

		return redirect("/")->with([
            'flash_message' => 'Tu cuenta ha sido activada satisfactoriamente , ahora puedes iniciar sesión sin problemas',
            'flash_class'   => 'alert-success',
        ]);
		
	}

	function getUserIP() {
		if( array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
			if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')>0) {
				$addr = explode(",",$_SERVER['HTTP_X_FORWARDED_FOR']);
				return trim($addr[0]);
			} else {
				return $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
		}
		else {
			return $_SERVER['REMOTE_ADDR'];
		}
	}

	/**
	 * Show the view that allows users to request a new activation email
	 *
	 * @return view
	 */
	public function showResendActivation() {
		if( ! Config::get('brcode.app_allow_registration')) {
			abort(404);
		}

		return view('brcode.users.resend_activation');
	}

	/**
	 * Process the request to receive a new activation email
	 *
	 * @return redirect(back if fails | login if success)
	 */
	public function resendActivation() {
		$input = Request::all();

		$rules = [
			'user_email' 	=> 'required|email',
		];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {
			$email = $input['user_email'];
			$user = SysUser::where('email',$email)->first();

			if($user) {

				$actRequest = SysUserAccountActivation::where('email',$email)->first();

				if( ! $actRequest) {
					$actRequest = new SysUserPasswordReset();
				}

				$actRequest->email = $email;
				$actRequest->token = str_random(100);
				$actRequest->save();

				Mail::to($actRequest->email)->queue(new ActivateAccount($actRequest->token, $user->first_name . ' ' . $user->last_name));

				event(new ActionExecuted('events.user_activate_acc_sent',null,null, Request::ip(),array('user_name'=> $user->email )));

				return redirect('/11w5Cj9WDAjnzlg0')->with('login_message',['type' => 'success', 'message' => trans('login.activate_acc_sent')]);

			}
			else {

				event(new ActionExecuted('events.new_acc_resend_act',null,null, Request::ip(),array('user_name'=> $email )));

				return Redirect::back()
					->withErrors($v->errors())
					->with('login_message',['type' => 'warning', 'message' => trans('login.email_not_registered')]);
			}

		}
	}

	/*
	|--------------------------------------------------------------------------
	| PRIVILEGES
	|--------------------------------------------------------------------------
	|
	*/

	/**
	 * Show the view that list privileges in: /users/privileges
	 *
	 * @return view
	 */
	public function showPrivileges() {
		$this->setInitialData();

		$this->viewData['form_title'] = trans('app.privileges');
		$this->viewData['form_url_dt'] = url('/11w5Cj9WDAjnzlg0/users/privileges/list-dt');
		$this->viewData['table_cols'] = ['col_id','col_name','col_description','col_code','col_is_enabled'];

		return view('brcode.users.privileges')->with('viewData',$this->viewData);
	}

	/**
	 * Create a DataTable output for privileges' table in: /users/privileges
	 *
	 * @return DataTable
	 */
	public function dtPrivileges() {
		$privileges = DB::table('sysPrivileges')->select(['id', 'name', 'description', 'code', 'is_enabled'])->whereNull('deleted_at');;
        return Datatables::of($privileges)
			->addColumn('action', function ($privileges) {
                return '<a href="' . url('/11w5Cj9WDAjnzlg0/users/privileges/modify/' .$privileges->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
            })
			->make();
	}

	/**
	 * Show the view for a privilege: /users/privilege/$id
	 *
	 * @return view
	 */
	public function showPrivilege($id = 0) {
		$this->setInitialData();
		$this->viewData['form_list_title'] = trans('app.privileges');
		$this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/users/privileges');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/users/privileges/modify');

		$this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('app.privilege');

		if($id > 0) {

			$this->viewData['model'] = SysPrivileges::find($id)->toArray();

			$this->viewData['model_id'] = $id;

		}

		$this->viewData['model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
													'name' =>  ['type' => 'text-readonly'],
													'description' =>  ['type' => 'text-readonly'],
													'code' =>  ['type' => 'text-readonly'],
													'is_enabled' => ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ] ],
													];

		$this->viewData['main_form'] = view('brcode.layout.app_form', $this->viewData);

		return view('brcode.users.privilege')->with('viewData',$this->viewData);
	}

	/**
	 * Save a privilege
	 *
	 * @return redirect('users/privileges')
	 */
	public function savePrivilege() {
		$colPrefix = $this->columnPrefix;
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		$rules = [
						$colPrefix.'is_enabled' 	=> 'required',
					];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {

			$new = true;

			if($id > 0) {
				$model = SysPrivileges::find($id);
				$new = false;
			}
			else {
				$model = new SysPrivileges();
			}

			$id = saveModel($id, $model, 'App\\SysPrivileges',$subModelExists, $input, $colPrefix, $new);

			$message = trans('app.privilege') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/11w5Cj9WDAjnzlg0/users/privileges')->with('message',$message)->with('messageType','success');

		}

	}

	/*
	|--------------------------------------------------------------------------
	| ROLES
	|--------------------------------------------------------------------------
	|
	*/

	/**
	 * Show the view that list roles in: /users/privileges
	 *
	 * @return view
	 */
	public function showRoles() {
		$this->setInitialData();
		$this->viewData['form_title'] = trans('app.roles');
		$this->viewData['form_url_add'] = url('/11w5Cj9WDAjnzlg0/users/roles/add');
		$this->viewData['form_url_dt'] = url('/11w5Cj9WDAjnzlg0/users/roles/list-dt');
		$this->viewData['table_cols'] = ['col_id','col_name','col_description', 'is_enabled'];

		return view('brcode.users.roles')->with('viewData',$this->viewData);
	}

	/**
	 * Create a DataTable output for roles' table in: /users/roles
	 *
	 * @return DataTable
	 */
	public function dtRoles() {
		$roles = DB::table('sysRoles')->select(['id', 'name', 'description', 'is_enabled'])->whereNull('deleted_at');

        return Datatables::of($roles)
			->addColumn('action', function ($roles) {
                return '<a href="' . url('/11w5Cj9WDAjnzlg0/users/roles/modify/' .$roles->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
            })
			->make();
	}

	/**
	 * Show the view for a role: /users/role/$id
	 *
	 * @return view
	 */
	public function showRole($id = 0) {
		$this->setInitialData();

		$this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('app.role');
		$this->viewData['form_list_title'] = trans('app.roles');
		$this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/users/roles');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/users/roles/save');
		$this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/users/roles/delete');
		$this->viewData['sub_model_view'] = 'table';
		$this->viewData['sub_model_title'] = trans('app.privileges');

		if($id > 0) {
			$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/users/roles/modify');

			$this->viewData['model'] = SysRoles::find($id);

			$this->viewData['sub_model'] = $this->viewData['model']->privileges->where('is_enabled','Y')->toArray();

			$this->viewData['model'] = $this->viewData['model']->toArray();

			$this->viewData['model_id'] = $id;
		}

		$this->viewData['model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
													'name' =>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
													'description' =>  ['type' => 'input-text', 'attr' => ' maxlength=150 '],
													'is_enabled' => ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ] ],
													];



		$privilegesModel = SysPrivileges::all();
		$privileges = [];

		foreach($privilegesModel as $key => $row) {
			$privileges[$key] = ['id' => $row->id, 'value' => $row->name];
		}

		// For submodel view = list
		$this->viewData['sub_model_list'] = [
			'privilege_id', // Column
			$privileges			// Array
		];

		// For submodel view = table
		$this->viewData['sub_model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
													'privilege_id' =>  ['type' => 'select','select_values' => $privileges,'class'=>'br-select2'],
													];

		return view('brcode.users.role')->with('viewData',$this->viewData);
	}

	/**
	 * Save a role
	 *
	 * @return redirect('users/roles')
	 */
	public function saveRole() {
		$colPrefix = $this->columnPrefix;

		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		$rules = [
						$colPrefix.'name' 			=> 'required|min:3|max:50|unique:sysRoles,name' . ($id > 0 ? ','.$id : ''),
						$colPrefix.'is_enabled' 	=> 'required',
					];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {

			$id = saveModel($id, $model, 'App\\SysRoles',$subModelExists, $input, $colPrefix, $new);

			$id = $model->id;

			saveSubModel($new, $id, 'role_id', 'App\\SysRoles', 'privileges', 'privilege_id', 'App\\SysRolePrivileges', $colPrefix, $input);

			$message = trans('app.role') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/11w5Cj9WDAjnzlg0/users/roles')->with('message',$message)->with('messageType','success');

		}


	}

	/**
	 * Delete a role
	 *
	 * @return redirect('users/roles')
	 */
	public function deleteRole() {
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		if($id > 0) {

			$role = SysRoles::find($id);

			$role->delete();

			$message = trans('app.role') . ' ' . trans('app.deleted_successfully');

			return redirect('/11w5Cj9WDAjnzlg0/users/roles')->with('message',$message)->with('messageType','success');
		}

	}

	/*
	|--------------------------------------------------------------------------
	| Users
	|--------------------------------------------------------------------------
	|
	*/

	/**
	 * Show the view that list users in: /users/list
	 *
	 * @return view
	 */
	public function showUsers() {
		$usuario = SysUser::orderBy('created_at', 'DESC')->get();
		$this->viewData['usuario'] = $usuario;
		$viewData = $this->viewData;

		return view('brcode.users.users', compact('viewData', 'usuario'));
	}

		public function filtroMes(FilterRequest $request,$id = 0)
    {
		//dd("prueba");
    	$this->setInitialData();

		$this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('app.user');
		          		$this->viewData['form_list_title'] = trans('app.roles');
		$this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/users');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/users/save');
		$this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/users/delete');
		$this->viewData['sub_model_title'] = trans('app.roles');
		$this->viewData['sub_model_view'] = 'list';

		if($id > 0) {
			$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/users/modify');
			$this->viewData['model'] = SysUser::find($id);
			$this->viewData['sub_model'] = $this->viewData['model']->roles->toArray();
			$this->viewData['model'] = $this->viewData['model']->toArray();
			$this->viewData['model_id'] = $id;

		}

		$this->viewData['model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
													'first_name' 						=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
													'last_name' 						=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
													'email' 								=>  ['type' => 'input-email', 'attr' => ' maxlength=50 '],
													'user_name' 						=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
													'company_id' 						=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
													'password' 							=>  ['type' => 'input-password', 'attr' => ' maxlength=50 '],
													'password_confirmation' =>  ['type' => 'input-password', 'attr' => ' maxlength=50 '],
													'company_id' 						=>  ['type' => 'text', 'attr' => ' maxlength=50 '],
													'is_enabled' 						=>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ] ],
													'is_activated' 					=>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ] ],
													];

		$rolesModel = SysRoles::all();
		$roles = [];

		foreach($rolesModel as $key => $row) {
			$roles[$key] = ['id' => $row->id, 'value' => $row->name];
		}

		$this->viewData['sub_model_list'] = [ 'role_id', $roles ];
        $from = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $request->desde)));
		//dd($from);
        $to   = date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $request->hasta)));

        $mes = $request->mes;
 
        

        if ($request->mes == null) {
            $usuario = SysUser::whereBetween("created_at", [$from, $to])->get();
            $this->viewData['usuario'] = $usuario;
            $viewData = $this->viewData;
            //$this->viewData['users'] = $users;
            //dd($users);
            return view('brcode.users.users', compact('viewData', 'usuario'))->with([
                'flash_class'   => 'alert-success',
                'flash_message' => 'Se encontraron registros.',
            ]);
        } else {

            $usuario = SysUser::whereMonth('created_at', $mes)->get();
            $this->viewData['usuario'] = $usuario;
            $viewData = $this->viewData;
             //$this->viewData['users'] = $users;
             //dd($users);
            return view('brcode.users.users', compact('viewData', 'usuario'))->with([
                'flash_class'   => 'alert-success',
                'flash_message' => 'Se encontraron registros.',
            ]);
        }

        if ($users->count() > 0) {
            set_time_limit(0);
            ini_set("memory_limit", -1);
            ini_set('max_execution_time', 0);

            return view('brcode.users.users', compact('viewData'))->with([
                'flash_class'   => 'alert-success',
                'flash_message' => 'Se encontraron registros.',
            ]);
        } else {
            return back()->with([
                'flash_class'     => 'alert-danger',
                'flash_message'   => 'No hay registros en el mes indicado.',
                'flash_important' => true,
            ]);
        }

    }

	public function showInfoUsers($id){
		error_log($id);
		$profile = SysUser::where('id',$id)->get();
		$ultra = $id;
		//dd($historial_inventory); 
		$rating = AppOrgUserRating::where('seller_user_id', $id)->where('processig', '>', 0)->orderBy('created_at','desc')->get();
		$this->viewData['ratingsCount'] = AppOrgUserRating::where('seller_user_id', $id)->where('processig', '>', 0)->count();
		$fp1 = $rating->where('processig', 1)->count();
		$fp2 = $rating->where('processig', 2)->count();
		$fp3 = $rating->where('processig', 3)->count();
	
		$fa1 = $rating->where('packaging', 1)->count();
		$fa2 = $rating->where('packaging', 2)->count();
		$fa3 = $rating->where('packaging', 3)->count();
		
		$fd1 = $rating->where('desc_prod', 1)->count();
		$fd2 = $rating->where('desc_prod', 2)->count();
		$fd3 = $rating->where('desc_prod', 3)->count();

		$this->viewData['ratings'] = $rating;

		$this->viewData['sc1'] = $rating->count() > 0 ? (($fp1 / $rating->count()) + ($fa1 / $rating->count()) + ($fd1 / $rating->count())) /3 : 0;
		$this->viewData['sc2'] = $rating->count() > 0 ? (($fp2 / $rating->count()) + ($fa2 / $rating->count()) + ($fd2 / $rating->count())) /3 : 0;
		$this->viewData['sc3'] = $rating->count() > 0 ? (($fp3 / $rating->count()) + ($fa3 / $rating->count()) + ($fd3 / $rating->count())) /3 : 0;

		$user = AppOrgUserEvent::where('user_id',$id)->orderBy('id', 'DESC')->get();
		//dd($user);
		$refered = SysUser::whereIn('id', SysUserReferred::where('master_id', $id)->pluck('son_id'))->get();

		$historial_inventory  = AppOrgUserInventory::where('user_id', $id)->get();

		$historial_promotional = AppOrgHistorialPromotionalCode::where('user_id',$id)->get();

		//dd($historial_promotional,$historial_inventory);

		//dd($historial_inventory);

		//dd($refered);
		return view('brcode.users.info', compact('user','refered','profile','historial_promotional','ultra'))->with('viewData',$this->viewData);
	}
	/**
	 * Create a DataTable output for users' table in: /users/roles
	 *
	 * @return DataTable
	 */
	public function dtUsers() {
		$users = SysUser::whereNull('deleted_at')->select(['id', 'first_name', 'last_name', 'email', 'user_name', 'is_enabled', 'concept'])->get();
			return Datatables::of($users)
		->addColumn('action', function ($users) {
							return '<a href="' . url('/11w5Cj9WDAjnzlg0/users/modify/' .$users->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>'.' <a href="#"  data-toggle="modal" data-id="' .$users->id.'" data-cre="' .$users->cash.'" data-option="add" class="btn btn-xs btn-success" onclick="openAdd(this);"><i class="fa fa-plus"></i> '.trans('app.addCre').'</a>'.' <a href="#" class="btn btn-xs btn-warning" data-option="remove" data-toggle="modal" data-id="' .$users->id.'" data-cre="' .$users->cash.'" onclick="openRemove(this);"><i class="fa fa-minus"></i> '.trans('app.minusCre').'</a>'.' <a href="/11w5Cj9WDAjnzlg0/users/info/' . $users->id . '" class="btn btn-xs btn-info"><i class="fa fa-info"></i> '.trans('Info').'</a>';
		})
		->make();
	}

	/**
	 * Show the view for a user: /users/user/$id
	 *
	 * @return view
	 */
	public function showUser($id = 0) {
		$this->setInitialData();

		$this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('app.user');
		$this->viewData['form_list_title'] = trans('app.roles');
		$this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/users');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/users/save');
		$this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/users/delete');
		$this->viewData['sub_model_title'] = trans('app.roles');
		$this->viewData['sub_model_view'] = 'list';

		if($id > 0) {
			$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/users/modify');
			$this->viewData['model'] = SysUser::find($id);
			$this->viewData['sub_model'] = $this->viewData['model']->roles->toArray();
			$this->viewData['model'] = $this->viewData['model']->toArray();
			$this->viewData['model_id'] = $id;

		}

		$this->viewData['model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
													'first_name' 						=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
													'last_name' 						=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
													'email' 								=>  ['type' => 'input-email', 'attr' => ' maxlength=50 '],
													'user_name' 						=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
													'company_id' 						=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
													'password' 							=>  ['type' => 'input-password', 'attr' => ' maxlength=50 '],
													'password_confirmation' =>  ['type' => 'input-password', 'attr' => ' maxlength=50 '],
													'company_id' 						=>  ['type' => 'text', 'attr' => ' maxlength=50 '],
													'is_enabled' 						=>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ] ],
													'is_activated' 					=>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ] ],
													];

		$rolesModel = SysRoles::all();
		$roles = [];

		foreach($rolesModel as $key => $row) {
			$roles[$key] = ['id' => $row->id, 'value' => $row->name];
		}

		$this->viewData['sub_model_list'] = [ 'role_id', $roles ];

		return view('brcode.users.user')->with('viewData',$this->viewData);
	}

	/**
	 * Save a user
	 *
	 * @return redirect('users/list')
	 */
	public function saveUser() {
		$colPrefix = $this->columnPrefix;
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		$rules = [
						$colPrefix.'first_name' 					=> 'required|min:3|max:50',
						$colPrefix.'last_name' 						=> 'required|min:3|max:50',
						$colPrefix.'email' 								=> 'required|min:3|max:50|unique:sysUsers,email' . ($id > 0 ? ','.$id : ''),
						$colPrefix.'user_name' 						=> 'required|min:3|max:50|unique:sysUsers,user_name' . ($id > 0 ? ','.$id : ''),
						$colPrefix.'password' 						=> 'confirmed' . ($id == 0 ? '|required' : ''),
					];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::except([$colPrefix.'password',$colPrefix.'password_confirmation']));
		}
		else {

			$new = true;

			if(strlen($input[$colPrefix.'password']) > 0) {
				$input[$colPrefix.'password'] = Hash::make($input[$colPrefix.'password']);
			}
			else {
				unset($input[$colPrefix.'password']);
			}

			unset($input[$colPrefix.'password_confirmation']);

			$id = saveModel($id, $model, 'App\\SysUser',$subModelExists, $input, $colPrefix, $new);

			if($subModelExists) {

				saveSubModel($new, $id, 'user_id', 'App\\SysUser', 'roles', 'role_id', 'App\\SysUserRoles', $colPrefix, $input);

			}

			$message = trans('app.user') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/11w5Cj9WDAjnzlg0/users')->with('message',$message)->with('messageType','success');

		}

	}

	/*
	|--------------------------------------------------------------------------
	| Store Credit
	|--------------------------------------------------------------------------
	|
	*/
	/**
	 * Save a user
	 *
	 * @return redirect('users/list')
	 */
	public function storeCreditUser(Request $request) {

		$input = Request::all();
		//d($input);

		
		#$id = $input['model_id'] > 0 ? $input['model_id'] : 0;
		$id = $input['orderDetails'];
		$credit = $input['credit'];
		$transaction = $input['transaction'];
		$pass = $input['pass'];
		$createdOn = date('Y-m-d H:i:s');
		if (Auth::user()->roles()->first()->role_id == 1 && Hash::check($pass, Auth::user()->password)){

			if($transaction == "1") {
				$user = SysUser::find($id);
				
				$user->cash = $user->cash + floatval($credit);
				//dd($user->cash);
				
				$message = trans('app.user') . ' ' . trans('app.addCredit');

				$order = new AppOrgOrder;
				$order->buyer_user_id = $user->id;
				$order->seller_user_id = 1;
				$order->created_on = $createdOn;
				$order->max_pay_out = $createdOn;
				$order->status = 'AC';
				$order->total = $credit;
				$order->tax = 0;
				$order->quantity = 1;
				$order->order_identification = Auth::id() . 'C' . getRandomToken(15);
				$order->paid_out = 'Y';
				$order->instructions = $order->order_identification;
				$order->save();

				foreach(AppOrgOrder::where('buyer_user_id', $user->id)->where('status', 'RP')->get() as $key){
					if($key->total <= $user->cash){
						$user->cash = (floatval($credit) * -1);
						//dd($user->cash);
						$key->status = 'CR';
						$key->paid_out = 'Y';
						$key->seller_read = 'N';
						$key->buyer_read = 'N';
						$key->max_pay_out = date('Y-m-d H:i:s', strtotime('+14 days'));
						$key->save();

						try {
							$payment = json_encode(array('date' => date('Y-m-d H:i:s'), 'order_id' => $key->order_identification, 'total' => $key->total, 'cantidad' => $key->quantity, 'type_payment' => 'Cash'));
							$orderPayment = new AppOrgOrderPayment([
								'payment_result' => $payment
							]);
							$key->payment()->save($orderPayment);
						} catch (Exception $ex) {
							$error = true;
							$this->loadCart();
						}
						
						PtyCommons::setUserEvent($user->id, 'Debito pedidos pendientes por pagar, al transferir saldo. Pago saldo personal en cuenta RGM el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP(), 'order_id' => $key->order_identification]);
						//OrderPaymentComplete
						Mail::to($key->seller->email)->queue(new OrderPaymentComplete(Auth::user(), $key));
						Mail::to($key->buyer->email)->queue(new OrderPaymentBuyer(Auth::user(), $key));

					}

				}

				return redirect('/11w5Cj9WDAjnzlg0/users')->with('message',$message)->with('messageType','success');
			}

			if($transaction == "0") {
				$user = SysUser::find($id);
				if($user->cash < (float) $credit){
					$message = trans('app.user') . ' ' . trans('app.outCredit');
					return redirect('/11w5Cj9WDAjnzlg0/users')->with('message',$message)->with('messageType','danger');
				}
				
				$user->cash = $user->cash - floatval($credit);
				//dd($user->cash);

				$order = new AppOrgOrder;
				$order->buyer_user_id = $user->id;
				$order->seller_user_id = 1;
				$order->created_on = $createdOn;
				$order->max_pay_out = $createdOn;
				$order->status = 'RC';
				$order->total = $credit;
				$order->tax = 0;
				$order->quantity = 1;
				$order->order_identification = Auth::id() . 'C' . getRandomToken(15);
				$order->paid_out = 'Y';
				$order->instructions = $order->order_identification;
				$order->save();
	
				$message = trans('app.user') . ' ' . trans('app.removeCredit');
	
				return redirect('/11w5Cj9WDAjnzlg0/users')->with('message',$message)->with('messageType','warning');
			}
		}
		
		$message = trans('app.errorPass');
	
		return redirect('/11w5Cj9WDAjnzlg0/users')->with('message',$message)->with('messageType','warning');


	}


	public function storeWithDrawal() {
		$input = Request::all();
		//dd($input);
		$id = $input['orderDetails'];
		$pass = $input['pass'];
		if (Auth::user()->roles()->first()->role_id == 1 && Hash::check($pass, Auth::user()->password)){
			$order = AppOrgOrder::where('order_identification', $id)->first();
			if($order->paid_out == 'N'){
				$order->paid_out = 'Y';
				//$order->updated_at = date('Y-m-d H:i:s');
				$order->status = 'CE';
				$order->save();
			}else{
				$order->paid_out = 'N';
				//$order->updated_at = date('Y-m-d H:i:s');
				$order->status = 'CP';
				$order->save();
			}
			$message = trans('app.user') . ' ' . trans('app.success_cash');
			return redirect('/11w5Cj9WDAjnzlg0/withdrawal')->with('message',$message)->with('messageType','success');
		}
		$message = trans('app.errorPass');
		return redirect('/11w5Cj9WDAjnzlg0/withdrawal')->with('message',$message)->with('messageType','warning');
	}

	/**
	 * Delete a user
	 *
	 * @return redirect('users/list')
	 */
	public function deleteUser() {
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		if($id > 0) {

			$role = SysUser::find($id);

			$role->delete();

			$message = trans('app.user') . ' ' . trans('app.deleted_successfully');

			return redirect('/11w5Cj9WDAjnzlg0/users')->with('message',$message)->with('messageType','success');
		}
	}

	/*
	|--------------------------------------------------------------------------
	| Profile
	|--------------------------------------------------------------------------
	|
	*/

	/**
	 * Show the view for the user's profile in:
	 *
	 * @return view
	 */
	public function showProfile() {
		$this->setInitialData();

		$this->viewData['form_title'] = trans('app.edit_profile');
		$this->viewData['form_list_title'] = trans('app.roles');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/users/profile/save_profile');

		$this->viewData['model_cols'] = [
			'id' 										=> 	['type' => 'hidden', 'attr' => ' '],
			'profile_picture'				=>	['type' => 'file-uploader', 'file-type' => 'image', 'is_array' => false,
																	'class' => 'image-uploader','attr' => 'data-url="'.url('11w5Cj9WDAjnzlg0/users/profile/upload_image').'"',
																   'img-class' => 'profile-user-image', 'img-path' => 'uploads/'],
			'first_name' 						=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
			'last_name' 						=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
			'email' 								=>  ['type' => 'input-email', 'attr' => ' maxlength=50 '],
			'user_name' 						=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
			'company_id' 						=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
			'password' 							=>  ['type' => 'input-password', 'attr' => ' maxlength=50 '],
			'password_confirmation' =>  ['type' => 'input-password', 'attr' => ' maxlength=50 '],
			'company_id' 						=>  ['type' => 'text', 'attr' => ' maxlength=50 '],
						];

		$this->viewData['model'] = SysUser::find(Auth::id());
		$this->viewData['model'] = $this->viewData['model']->toArray();
		$this->viewData['model_id'] = Auth::id();

		return view('brcode.users.profile')->with('viewData',$this->viewData);
	}

	/**
	 * Save the user's profile
	 *
	 * @return redirect('dashboard')
	 */
	public function saveProfile() {
		$colPrefix = $this->columnPrefix;
		$input = Request::all();

		$id = Auth::id();

		$rules = [
						$colPrefix.'first_name' 				=> 'required|min:3|max:50',
						$colPrefix.'last_name' 					=> 'required|min:3|max:50',
						$colPrefix.'password' 					=> ($id > 0 ? '' : 'required|') .'confirmed',
					];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::except([$colPrefix.'password',$colPrefix.'password_confirmation']));
		}
		else {

			$new = true;

			if(strlen($input[$colPrefix.'password']) > 0) {
				$input[$colPrefix.'password'] = Hash::make($input[$colPrefix.'password']);
			}
			else {
				unset($input[$colPrefix.'password']);
			}

			if(Auth::user()->profile_picture && strlen($input[$colPrefix . 'profile_picture']) == 0) {
				if(file_exists(public_path() . '/uploads/' . Auth::user()->profile_picture ) ) {
					unlink(public_path() . '/uploads/' . Auth::user()->profile_picture);
				}
			}

			unset($input[$colPrefix.'password_confirmation']);

			$id = saveModel($id, $model, 'App\\SysUser',$subModelExists, $input, $colPrefix, $new);

			$message = trans('app.profile_modified');

			return redirect('/11w5Cj9WDAjnzlg0/dashboard')->with('message',$message)->with('messageType','success');

		}
	}

	/**
	 * Upload the profile's image
	 *
	 * @return json response
	 */
	public function saveProfilePicture() {
		$inputName = 'file_' . $this->columnPrefix . 'profile_picture';
		$file = Request::file($inputName);

		if($file) {

        $destinationPath = public_path() . '/uploads/';
        //$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				//$filename = md5(microtime()) . '.' . $extension;
				$filename = 'profile_image_' . Auth::id() . '.' . $extension;
				$filename200 = 'profile_image_200x200_' . Auth::id() . '.' . $extension;
        $upload_success = Request::file($inputName)->move($destinationPath, $filename);

        if ($upload_success) {
            // resizing an uploaded file
            //Image::make($destinationPath . $filename)->resize(200, 200)->save($destinationPath . $filename200);

            return Response::json(array('message' => 'success', 'name' => $filename), 200);
        } else {
            return Response::json(array('mesage' => 'error'), 400);
        }
    }
	}
}
