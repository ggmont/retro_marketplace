<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\AppOrgBlog;


class BlogController extends Controller
{
    public function index() {

    	$blog       = AppOrgBlog::all();

        return view('admin.blog', [
            'blog'     => $blog,
        ]);
    }
}
