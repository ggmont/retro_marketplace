<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\AppOrgPromotionalCode;
use Illuminate\Http\Request;

class PromotionalController extends Controller
{
    public function index() {
    	
    	$promotional       = AppOrgPromotionalCode::all();

        return view('admin.promotional.index', [
            'promotional'     => $promotional,
        ]);
    }

    public function create()
    {
     
        return view('admin.promotional.create', [

        ]);
    }
}
