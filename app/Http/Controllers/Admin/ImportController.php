<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\AppOrgUserInventory;
use App\AppOrgUserInventoryImage;

class ImportController extends Controller
{

    public function index()
    {
        return view('admin.import');
    }

    public function import(Request $request)
    {
        if ($request->hasFile('csv_file')) {
            $path = $request->file('csv_file')->getRealPath();
            $data = array_map('str_getcsv', file($path));
            $header = array_shift($data);
    
            foreach ($data as $row) {
                $record = array_combine($header, $row);
    
                // Crear o actualizar AppOrgUserInventory
                $inventory = AppOrgUserInventory::updateOrCreate(
                    ['title' => $record['title']],
                    [
                        'user_id' => $record['user_id'],
                        'size' => $record['size'],
                        'kg' => $record['kg'],
                        'comments' => $record['comments'],
                        'category' => $record['category'],
                        'price' => $record['price']
                    ]
                );
    
                // Copiar y guardar la imagen en tu sistema
                if ($record['imagen']) {
                    $img = new AppOrgUserInventoryImage();
                    $imagePath = $record['imagen']; // Ruta de la imagen original
    
                    $extension = pathinfo($imagePath, PATHINFO_EXTENSION);
                    $allowedExtensions = ['jpg', 'jpeg', 'png', 'gif']; // Extensiones permitidas
    
                    // Verificar y ajustar la extensión del archivo
                    if (!in_array($extension, $allowedExtensions)) {
                        $extension = 'jpg'; // Cambiar la extensión a jpg por defecto
                    }
    
                    $image_name = 'request' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time() . '_' . $img->id  . '_' . md5(microtime()) . '.' . $extension;
    
                    $destinationPath = public_path('uploads/inventory-images/');
                    $dateFolder = date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $directory = $destinationPath . $dateFolder;
                    if (!file_exists($directory)) {
                        mkdir($directory, 0777, true);
                    }
                    $imageCopyPath = $directory . $image_name;
                    $imageUploadSuccess = copy($imagePath, $imageCopyPath);
    
                    if ($imageUploadSuccess) {
                        $img->inventory_id = $inventory->id;
                        $img->image_path = $dateFolder . $image_name;
                        $img->created_by = $record['user_id'];
                        $img->save();
                    }
                }
            }
    
            return redirect("11w5Cj9WDAjnzlg0/csv")->with([
                'flash_message' => 'CSV importado exitosamente.',
                'flash_class' => 'alert-success',
            ]);
        }
    
        return redirect()->back()->with('error', 'No se encontró ningún archivo CSV.');
    }
    
    
    
    
    
    
    
    
    
}
