<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
 
use App\AppOrgUserInventory;
 

class InventoryController extends Controller
{
    public function index() {

    	$inventory       = AppOrgUserInventory::all();
        //dd($inventory);

        return view('admin.inventory', [
            'inventory'     => $inventory,
        ]);
    }

}
