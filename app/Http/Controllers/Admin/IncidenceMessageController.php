<?php

namespace App\Http\Controllers\Admin;

use App\Incidence;
use App\AppConversation;
use App\AppMessage;
use App\SysUser;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IncidenceMessageController extends Controller
{
	public function message_incidence(Request $request)
	{
		//dd($request->all());
		$newContact = SysUser::where('user_name', $request->name)->select('id', 'user_name')->first();

        $incidence = Incidence::where('id', $request->incidence)->first();
        
        //dd($incidence);

        //dd($newContact);

		if (AppConversation::where('alias_user_id', auth()->id())->where('alias_contact_id', $newContact->id)->first()) {
			$data = new AppMessage;
			$data->content = 'Mensaje relacionado al id de su problema : '.  $incidence->id_identification . $request->content;
			$data->alias_from_id = auth()->id();
			$data->alias_to_id = $newContact->id;

			if ($data->save()) {
				AppConversation::where('alias_user_id', auth()->id())->where('alias_contact_id', $newContact->id)->first()
				->update(
					[
						'last_message' => 'Mensaje relacionado al id de su problema : '.  $incidence->id_identification . $request->content
					]
				);

                Incidence::where('id', $request->incidence)->first()
				->update(
					[
						'status' => 1
					]
				);

				return redirect('/account/messages');
			}
			
		} elseif (AppConversation::where('alias_contact_id', auth()->id())->where('alias_user_id', $newContact->id)->first()) {
			$data = new AppMessage;
			$data->content = 'Mensaje relacionado al id de su problema : '.  $incidence->id_identification . $request->content;
			$data->alias_from_id = auth()->id();
			$data->alias_to_id = $newContact->id;

			if ($data->save()) {
				AppConversation::where('alias_contact_id', auth()->id())->where('alias_user_id', $newContact->id)->first()
				->update(
					[
						'last_message' =>  'Mensaje relacionado al id de su problema : '.  $incidence->id_identification . $request->content
					]
				);

                Incidence::where('id', $request->incidence)->first()
				->update(
					[
						'status' => 1
					]
				);

				return redirect('/account/messages');
			}
		} else {
			$conversation = new AppConversation();

			$conversation->alias_user_id = auth()->id();

			$conversation->last_message = 'Nueva Conversacion Activa';

			$conversation->alias_contact_id = $newContact->id;

			if ($conversation->save()) {
                
                Incidence::where('id', $request->incidence)->first()
				->update(
					[
						'status' => 1
					]
				);

				$data = new AppMessage;
				$data->content = $request->content;
				//dd($data->content);
				$data->alias_from_id = auth()->id();
				$data->alias_to_id = $newContact->id;
				$data->save();

				return redirect('/account/messages');
			}
		}
	}

    public function confirm_incidence(Request $request)
    {
        $incidence = Incidence::where('id', $request->incidence)->first();

        Incidence::where('id', $request->incidence)->first()
        ->update(
            [
                'status' => 2
            ]
        );

		return redirect('/11w5Cj9WDAjnzlg0/management/incidences')->with([
			'flash_message' => 'Solventado correctamente.',
			'flash_class'   => 'alert-success',
		]);
        
    }
}
