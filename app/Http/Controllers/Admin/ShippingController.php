<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\AppOrgCategory;
use App\AppOrgOrder;
use App\AppOrgUserInventory;
use App\AppOrgOrderDetail;
use App\SysShipping;
use DB;

class ShippingController extends Controller
{
    public function index() {

    	$shipping       = SysShipping::all();

        return view('admin.shipping', [
            'shipping'     => $shipping,
        ]);
    }

    public function create()
    {
        $shipping  = SysShipping::all();

        return view('admin.shipping.create', ['shipping' => $shipping]);

    }

    public function store(Request $request)
    {
        $this->validate($request, SysShipping::$rules, SysShipping::$message);
        $shipping = SysShipping::create($request->all());
        if($request->input('type') == 'PA'){
          $shipping->price = $shipping->stamp_price + 1;
        }
        if($request->input('type') == 'CA'){
          $shipping->price = $shipping->stamp_price + 0.5;
        }

        $shipping->volume = ($shipping->hight * $shipping->large * $shipping->width)/6000;

        //dd($shipping->volume);

        if ($shipping->save()) {

            return redirect("11w5Cj9WDAjnzlg0/shipping")->with([
                'flash_message' => 'Envio agregado Con Exito ',
                'flash_class'   => 'alert-success',
            ]);

        } else {

            return back()->with([
                'flash_message'   => 'Ha ocurrido un error.',
                'flash_class'     => 'alert-danger',
                'flash_important' => true,
            ]);

        }

     }
}
