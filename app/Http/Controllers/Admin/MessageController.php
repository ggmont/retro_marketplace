<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
use Request;
use App\AppMessage;
use App\Models\SysRoles;
use App\Models\SysUser;
use DB;

class MessageController extends Controller
{
    public function index() {

    	$message       = AppMessage::all();

        return view('admin.messages', [
            'message'     => $message,
        ]);
    }

}
