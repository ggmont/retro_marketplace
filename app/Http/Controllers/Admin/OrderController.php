<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\AppOrgOrder;
use App\SysShipping;


class OrderController extends Controller
{
    public function index() {

    	$order       = AppOrgOrder::all();

        return view('admin.order', [
            'order'     => $order,
        ]);
    }

    public function view($id)
    {

        $order = AppOrgOrder::findOrFail($id);
        //dd($order);
        $order_shipping = SysShipping::find($order->shipping_id);
        $details = $order->details()->get();
        //dd($details);

        return view('admin.order.view', [
            'order'     => $order,
            'order_shipping' => $order_shipping,
            'details' => $details,
        ]);
    }
}
