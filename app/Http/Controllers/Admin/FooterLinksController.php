<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\AppOrgFooterLink;


class FooterLinksController extends Controller
{
    public function index() {

    	$footer_link       = AppOrgFooterLink::all();

        return view('admin.footer_link', [
            'footer_link'     => $footer_link,
        ]);
    }
}
