<?php

namespace App\Http\Controllers\Admin;
use App\SysUser;
use App\ChMessage as Message;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserMessageController extends Controller
{

    public function showMessagesUser($userId,$MsgId){

        $user_one = SysUser::findOrFail($userId);    

        $user_two = SysUser::findOrFail($MsgId);    

        //dd($user_one,$user_two);
    
        $this->viewData['message'] = Message::where('from_id', $user_one->id)->where('to_id', $user_two->id)->orWhere('from_id', $user_two->id)->where('to_id', $user_one->id)->orderBy('id', 'desc')->get();
        //dd($message);
    
    
        //dd($conversation);
        return view('admin.user.historial_message', compact('user_one','user_two'))->with('viewData', $this->viewData);
      }
}
