<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\AppOrgBanner;
use App\AppOrgPage;
use Illuminate\Http\Request;
use Auth;
use Intervention\Image\Facades\Image;

class BannerController extends Controller
{
    public function index()
    {

        $banner       = AppOrgBanner::all();

        return view('admin.banner', [
            'banner'     => $banner,
        ]);
    }




    public function create()
    {
        $banner  = AppOrgBanner::all();

        return view('admin.banner.create', ['banner' => $banner]);
    }

    public function store(Request $request)
    {
        $destinationPath = public_path('/uploads/banners-images/');

        $osi                = new AppOrgBanner;
        $secundary = $request->image_path;
        $osi->location  = 'BNR_MAIN_3';
        $osi->priority  = 1;
        $osi->is_enabled = 'Y';
        $nombre_imagen  = 'banner_image' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $secundary->getClientOriginalExtension();
        $osi->lang = 'fr';
        $osi->image_path    = $nombre_imagen;
        $osi->category    = 'faqs';
        $osi->created_by = \Auth::user()->id;

        $upload_success = $secundary->move($destinationPath, $nombre_imagen);
        $osi->save();

        return redirect("11w5Cj9WDAjnzlg0/banners")->with([
            'flash_message' => 'Imagen agregada Con Exito ',
            'flash_class'   => 'alert-success',
        ]);
    }

    public function edit($id)
    {
        $banner = AppOrgBanner::findOrFail($id);
        $pagina       = AppOrgPage::all();
        //dd($pagina);

        return view(
            "admin.banner.edit",
            compact(
                'banner',
                'pagina'
            )
        );
    }

    public function updateImage(Request $request, $id)
    {
        $banner = AppOrgBanner::find($id);

        //dd($banner);
        $banner_image = $request->image_path;


        if ($banner_image) {
            $filename  = 'banner_image' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $banner_image->getClientOriginalExtension();
            //dd($filename);
            $path = public_path('/uploads/banners-images/' . $filename);

            Image::make($banner_image->getRealPath())->resize(1280, 165)->save($path);
            $banner->image_path = $filename;
            $banner->save();
            $banner->update([
                'image_path' => $filename,
                'is_enabled' => $request->is_enabled,
                'link' => $request->link,
            ]);
            return redirect("11w5Cj9WDAjnzlg0/banners")->with([
                'flash_message' => 'Banner actualizado Con Exito ',
                'flash_class'   => 'alert-success',
            ]);
        } else {
            $banner->update([
                'is_enabled' => $request->is_enabled,
                'link' => $request->link,
            ]);
            return redirect("11w5Cj9WDAjnzlg0/banners")->with([
                'flash_message' => 'Banner actualizado Con Exito ',
                'flash_class'   => 'alert-success',
            ]);
        }
        //dd($carrousel_image_secundary_two); 


    }

    public function updateImageMagazine(Request $request, $id)
    {
        $banner = AppOrgBanner::find($id);


        $banner_image = $request->image_path;


        if ($banner_image) {
            $filename  = 'banner_image' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $banner_image->getClientOriginalExtension();
            //dd($filename);
            $path = public_path('/uploads/banners-images/' . $filename);

            Image::make($banner_image->getRealPath())->save($path);
            $banner->image_path = $filename;
            $banner->save();
            $banner->update([
                'image_path' => $filename,
                'is_enabled' => $request->is_enabled,
                'category' => $request->category,
            ]);
            return redirect("11w5Cj9WDAjnzlg0/banners")->with([
                'flash_message' => 'Banner actualizado Con Exito ',
                'flash_class'   => 'alert-success',
            ]);
        } else {
            $banner->update([
                'is_enabled' => $request->is_enabled,
                'category' => $request->category,
            ]);
            return redirect("11w5Cj9WDAjnzlg0/banners")->with([
                'flash_message' => 'Banner actualizado Con Exito ',
                'flash_class'   => 'alert-success',
            ]);
        }
        //dd($carrousel_image_secundary_two); 


    }

    public function updateImageCategory(Request $request, $id)
    {
        $banner = AppOrgBanner::find($id);


        $banner_image = $request->image_path;


        if ($banner_image) {
            $filename  = 'banner_image' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $banner_image->getClientOriginalExtension();
            //dd($filename);
            $path = public_path('/uploads/banners-images/' . $filename);

            Image::make($banner_image->getRealPath())->resize(270, 148)->save($path);
            $banner->image_path = $filename;
            $banner->save();
            $banner->update([
                'image_path' => $filename,
                'is_enabled' => $request->is_enabled,
                'category' => $request->category,
            ]);
            return redirect("11w5Cj9WDAjnzlg0/banners")->with([
                'flash_message' => 'Banner actualizado Con Exito ',
                'flash_class'   => 'alert-success',
            ]);
        } else {
            $banner->update([
                'is_enabled' => $request->is_enabled,
                'category' => $request->category,
            ]);
            return redirect("11w5Cj9WDAjnzlg0/banners")->with([
                'flash_message' => 'Banner actualizado Con Exito ',
                'flash_class'   => 'alert-success',
            ]);
        }
        //dd($carrousel_image_secundary_two); 


    }
}
