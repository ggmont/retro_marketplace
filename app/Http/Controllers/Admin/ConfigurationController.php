<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\SysDictionary;


class ConfigurationController extends Controller
{
    public function index() {
        $list = SysDictionary::groupBy('code')->get();
    
        return view('admin.configuration_list', [
            'list' => $list,
        ]);
    }
    
}
