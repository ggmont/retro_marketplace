<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ProductRequest;
use App\ProductRequestImage;
use App\SysDictionary;
use App\AppOrgCategory;
use App\AppOrgProduct;
use App\AppOrgProductImage;
use App\AppOrgUserInventory;
use App\AppOrgUserInventoryImage;
use App\AppOrgUserInventoryEan;
use App\ProductRequestImageEan;
use Illuminate\Http\Request;
use Mail;
use App\Mail\RequestProductInventory;
use App\Mail\ProductRequestMail;

class ProductRequestController extends Controller
{
    public function index()
    {

        $product_request       = ProductRequest::all();


        return view('admin.product.request.index', []);
    }

    public function edit($id)
    {
        $product_request = ProductRequest::findOrFail($id);
        $imagen_product = ProductRequestImage::where('inventory_id', $id)->get();
        $imagen_product_ean = ProductRequestImageEan::where('inventory_id', $id)->get();
        $categories = AppOrgCategory::all();
        $genero = SysDictionary::where("code", "GAME_CATEGORY")->get();
        $location = SysDictionary::where("code", "GAME_LOCATION")->get();
        $generation = SysDictionary::where("code", "GAME_GENERATION")->get();
        $language = SysDictionary::where("code", "GAME_LANGUAGE")->get();
        $media = SysDictionary::where("code", "GAME_SUPPORT")->get();
        $platform = SysDictionary::where("code", "GAME_PLATFORM")->get();
        $region = SysDictionary::where("code", "GAME_REGION")->get();
        //dd($prueba);
        return view(
            "admin.product.request.edit",
            compact(
                'categories',
                'product_request',
                'imagen_product',
                'imagen_product_ean',
                'genero',
                'location',
                'generation',
                'language',
                'media',
                'platform',
                'region'
            )
        );
    }

    public function updateAuction(Request $request, $id)
    {


        $data =  ProductRequest::findOrFail($id);
        $data->update([
            'status' => 'A',
            'aprobed_by' => \Auth::user()->user_name,
            'product_id' => $request->product
        ]);

        if ($request->hasFile('image_path')) {
            if ($request->has('box_language')) {
                $data   =   AppOrgProduct::create([
                    'name' => $request->name,
                    'name_en' => $request->name_en,
                    'release_date' => $request->release_date,
                    'prod_category_id' => $request->prod_category_id,
                    'game_category' => $request->game_category,
                    'game_location' => $request->game_location,
                    'game_generation' => $request->game_generation,
                    'language' => implode(",", $request->language),
                    'box_language' => implode(",", $request->box_language),
                    'large' => $request->large,
                    'width' => $request->width,
                    'high' => $request->high,
                    'price_solo' => $request->price_solo,
                    'price_used' => $request->price_used,
                    'price_new' => $request->price_new,
                    'media' => $request->media,
                    'platform' => $request->platform,
                    'region' => $request->region,
                    'ean_upc' => $request->ean_upc,
                    'volume' => $request->volume,
                    'weight' => $request->weight,
                    'comments' => $request->comments,
                    'image_path' => "",
                    'request_by' => $request->user_id,
                    'is_enabled' => 'Y',
                ]);
                if ($data->save()) {
                    $destinationPath = public_path('/images');

                    foreach ($request->file('image_path') as $e) {
                        $osi                = new AppOrgProductImage;
                        $osi->product_id = $data->id;

                        $nombre_imagen  = $data->game_generation . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $e->getClientOriginalName();
                        $osi->image_path    = $nombre_imagen;
                        $osi->created_by = \Auth::user()->id;
                        $upload_success = $e->move($destinationPath, $nombre_imagen);
                        $osi->save();
                    }
                    if ($request->image_inventory) {


                        $data_inventory   =   AppOrgUserInventory::create([
                            'user_id' => $request->user_id,
                            'product_id' => $data->id,
                            'box_condition' => $request->box,
                            'manual_condition' => $request->manual,
                            'cover_condition' => $request->cover,
                            'game_condition' => $request->game,
                            'extra_condition' => $request->extra,
                            'inside_condition' => $request->inside,
                            'location' => 1,
                            'offer_type' => 1,
                            'auction_type' => 1,
                            'countdown_hours' => 24,
                            'quantity' => $request->quantity,
                            'quantity_sold' => 0,
                            'max_bid' => $request->price,
                            'tax' => 0.0000,
                            'comments' => $request->description,
                            'status' => 'A',
                            'created_by' => \Auth::user()->id,
                            'image_path' => "",
                            'is_enabled' => 'Y',
                        ]);
                        if ($data_inventory->save()) {
                            for ($i = 0; $i < count($request->image_inventory); $i++) {
                                AppOrgUserInventoryImage::create([
                                    'inventory_id'          => $data_inventory->id,
                                    'image_path'    => $request->image_inventory[$i],
                                    'created_by' => \Auth::user()->id,
                                ]);
                            }

                            if ($request->imagen_product_ean) {
                                for ($i = 0; $i < count($request->image_inventory); $i++) {
                                    AppOrgUserInventoryEan::create([
                                        'inventory_id'          => $data_inventory->id,
                                        'image_path'    => $request->image_inventory[$i],
                                        'created_by' => \Auth::user()->id,
                                    ]);
                                }
                            }
                        }



                        return redirect("11w5Cj9WDAjnzlg0/product")->with([
                            'flash_message' => 'Producto solicitado agregado Con Exito',
                            'flash_class'   => 'alert-success',
                        ]);
                    } else {

                        $data_inventory   =   AppOrgUserInventory::create([
                            'user_id' => $request->user_id,
                            'product_id' => $data->id,
                            'box_condition' => $request->box,
                            'manual_condition' => $request->manual,
                            'cover_condition' => $request->cover,
                            'game_condition' => $request->game,
                            'extra_condition' => $request->extra,
                            'inside_condition' => $request->inside,
                            'location' => 1,
                            'offer_type' => 1,
                            'auction_type' => 1,
                            'countdown_hours' => 24,
                            'quantity' => $request->quantity,
                            'quantity_sold' => 0,
                            'max_bid' => $request->price,
                            'tax' => 0.0000,
                            'comments' => $request->description,
                            'status' => 'A',
                            'created_by' => \Auth::user()->id,
                            'image_path' => "",
                            'is_enabled' => 'Y',
                        ]);

                        return redirect("11w5Cj9WDAjnzlg0/product")->with([
                            'flash_message' => 'Producto solicitado agregado Con Exito',
                            'flash_class'   => 'alert-success',
                        ]);
                    }
                } else {
                    return back()->with([
                        'flash_message'   => 'Ha ocurrido un error.',
                        'flash_class'     => 'alert-danger',
                        'flash_important' => true,
                    ]);
                }
            } else {
                $data   =   AppOrgProduct::create([
                    'name' => $request->name,
                    'name_en' => $request->name_en,
                    'release_date' => $request->release_date,
                    'prod_category_id' => $request->prod_category_id,
                    'game_category' => $request->game_category,
                    'game_location' => $request->game_location,
                    'game_generation' => $request->game_generation,
                    'language' => implode(",", $request->language),
                    'media' => $request->media,
                    'large' => $request->large,
                    'width' => $request->width,
                    'high' => $request->high,
                    'price_solo' => $request->price_solo,
                    'price_used' => $request->price_used,
                    'price_new' => $request->price_new,
                    'platform' => $request->platform,
                    'region' => $request->region,
                    'ean_upc' => $request->ean_upc,
                    'volume' => $request->volume,
                    'weight' => $request->weight,
                    'comments' => $request->comments,
                    'image_path' => "",
                    'request_by' => $request->user_id,
                    'is_enabled' => 'Y',
                ]);

                if ($data->save()) {
                    $destinationPath = public_path('/images');

                    foreach ($request->file('image_path') as $e) {
                        $osi                = new AppOrgProductImage;
                        $osi->product_id = $data->id;

                        $nombre_imagen  = $data->game_generation . time() . $e->getClientOriginalName();
                        $osi->image_path    = $nombre_imagen;
                        $osi->created_by = \Auth::user()->id;
                        $upload_success = $e->move($destinationPath, $nombre_imagen);
                        $osi->save();
                    }

                    if ($request->image_inventory) {

                        $data_inventory   =   AppOrgUserInventory::create([
                            'user_id' => $request->user_id,
                            'product_id' => $data->id,
                            'box_condition' => $request->box,
                            'manual_condition' => $request->manual,
                            'cover_condition' => $request->cover,
                            'game_condition' => $request->game,
                            'extra_condition' => $request->extra,
                            'inside_condition' => $request->inside,
                            'location' => 1,
                            'offer_type' => 1,
                            'auction_type' => 1,
                            'countdown_hours' => 24,
                            'quantity' => $request->quantity,
                            'quantity_sold' => 0,
                            'max_bid' => $request->price,
                            'tax' => 0.0000,
                            'comments' => $request->description,
                            'status' => 'A',
                            'created_by' => \Auth::user()->id,
                            'image_path' => "",
                            'is_enabled' => 'Y',
                        ]);
                        if ($data_inventory->save()) {
                            for ($i = 0; $i < count($request->image_inventory); $i++) {
                                AppOrgUserInventoryImage::create([
                                    'inventory_id'          => $data_inventory->id,
                                    'image_path'    => $request->image_inventory[$i],
                                    'created_by' => \Auth::user()->id,
                                ]);
                            }

                            if ($request->imagen_product_ean) {
                                for ($i = 0; $i < count($request->image_inventory); $i++) {
                                    AppOrgUserInventoryEan::create([
                                        'inventory_id'          => $data_inventory->id,
                                        'image_path'    => $request->image_inventory[$i],
                                        'created_by' => \Auth::user()->id,
                                    ]);
                                }
                            }
                        }

                        return redirect("11w5Cj9WDAjnzlg0/product")->with([
                            'flash_message' => 'Producto solicitado agregado Con Exito',
                            'flash_class'   => 'alert-success',
                        ]);
                    } else {

                        $data_inventory   =   AppOrgUserInventory::create([
                            'user_id' => $request->user_id,
                            'product_id' => $data->id,
                            'box_condition' => $request->box,
                            'manual_condition' => $request->manual,
                            'cover_condition' => $request->cover,
                            'game_condition' => $request->game,
                            'extra_condition' => $request->extra,
                            'inside_condition' => $request->inside,
                            'location' => 1,
                            'offer_type' => 1,
                            'auction_type' => 1,
                            'countdown_hours' => 24,
                            'quantity' => $request->quantity,
                            'quantity_sold' => 0,
                            'max_bid' => $request->price,
                            'tax' => 0.0000,
                            'comments' => $request->description,
                            'status' => 'A',
                            'created_by' => \Auth::user()->id,
                            'image_path' => "",
                            'is_enabled' => 'Y',
                        ]);

                        return redirect("11w5Cj9WDAjnzlg0/product")->with([
                            'flash_message' => 'Producto solicitado agregado Con Exito',
                            'flash_class'   => 'alert-success',
                        ]);
                    }
                } else {

                    return back()->with([
                        'flash_message'   => 'Ha ocurrido un error.',
                        'flash_class'     => 'alert-danger',
                        'flash_important' => true,
                    ]);
                }
            }
        } elseif ($request->hasFile('image_path')) {
            $data   =   AppOrgProduct::create([
                'name' => $request->name,
                'name_en' => $request->name_en,
                'release_date' => $request->release_date,
                'prod_category_id' => $request->prod_category_id,
                'game_category' => $request->game_category,
                'game_location' => $request->game_location,
                'game_generation' => $request->game_generation,
                'language' => implode(",", $request->language),
                'box_language' => implode(",", $request->box_language),
                'media' => $request->media,
                'large' => $request->large,
                'width' => $request->width,
                'high' => $request->high,
                'price_solo' => $request->price_solo,
                'price_used' => $request->price_used,
                'price_new' => $request->price_new,
                'platform' => $request->platform,
                'region' => $request->region,
                'ean_upc' => $request->ean_upc,
                'volume' => $request->volume,
                'weight' => $request->weight,
                'comments' => $request->comments,
                'image_path' => "",
                'request_by' => $request->user_id,
                'is_enabled' => 'Y',
            ]);

            if ($data->save()) {
                $destinationPath = public_path('/images');

                foreach ($request->file('image_path') as $e) {
                    $osi                = new AppOrgProductImage;
                    $osi->product_id = $data->id;

                    $nombre_imagen  = $data->game_generation . time() . $e->getClientOriginalName();
                    $osi->image_path    = $nombre_imagen;
                    $osi->created_by = \Auth::user()->id;
                    $upload_success = $e->move($destinationPath, $nombre_imagen);
                    $osi->save();
                }

                if ($request->image_inventory) {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'offer_type' => 1,
                        'auction_type' => 1,
                        'countdown_hours' => 24,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'max_bid' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);
                    if ($data_inventory->save()) {
                        for ($i = 0; $i < count($request->image_inventory); $i++) {
                            AppOrgUserInventoryImage::create([
                                'inventory_id'          => $data_inventory->id,
                                'image_path'    => $request->image_inventory[$i],
                                'created_by' => \Auth::user()->id,
                            ]);
                        }

                        if ($request->imagen_product_ean) {
                            for ($i = 0; $i < count($request->image_inventory); $i++) {
                                AppOrgUserInventoryEan::create([
                                    'inventory_id'          => $data_inventory->id,
                                    'image_path'    => $request->image_inventory[$i],
                                    'created_by' => \Auth::user()->id,
                                ]);
                            }
                        }
                    }

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                } else {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'offer_type' => 1,
                        'auction_type' => 1,
                        'countdown_hours' => 24,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'max_bid' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                }
            } else {

                return back()->with([
                    'flash_message'   => 'Ha ocurrido un error.',
                    'flash_class'     => 'alert-danger',
                    'flash_important' => true,
                ]);
            }
        } elseif ($request->has('box_language')) {
            $data   =   AppOrgProduct::create([
                'name' => $request->name,
                'name_en' => $request->name_en,
                'release_date' => $request->release_date,
                'prod_category_id' => $request->prod_category_id,
                'game_category' => $request->game_category,
                'game_location' => $request->game_location,
                'game_generation' => $request->game_generation,
                'language' => implode(",", $request->language),
                'box_language' => implode(",", $request->box_language),
                'media' => $request->media,
                'large' => $request->large,
                'width' => $request->width,
                'high' => $request->high,
                'price_solo' => $request->price_solo,
                'price_used' => $request->price_used,
                'price_new' => $request->price_new,
                'platform' => $request->platform,
                'region' => $request->region,
                'ean_upc' => $request->ean_upc,
                'volume' => $request->volume,
                'weight' => $request->weight,
                'comments' => $request->comments,
                'image_path' => "",
                'request_by' => $request->user_id,
                'is_enabled' => 'Y',
            ]);
            if ($data->save()) {

                if ($request->image_inventory) {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'offer_type' => 1,
                        'auction_type' => 1,
                        'countdown_hours' => 24,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'max_bid' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);
                    if ($data_inventory->save()) {
                        for ($i = 0; $i < count($request->image_inventory); $i++) {
                            AppOrgUserInventoryImage::create([
                                'inventory_id'          => $data_inventory->id,
                                'image_path'    => $request->image_inventory[$i],
                                'created_by' => \Auth::user()->id,
                            ]);
                        }

                        if ($request->imagen_product_ean) {
                            for ($i = 0; $i < count($request->image_inventory); $i++) {
                                AppOrgUserInventoryEan::create([
                                    'inventory_id'          => $data_inventory->id,
                                    'image_path'    => $request->image_inventory[$i],
                                    'created_by' => \Auth::user()->id,
                                ]);
                            }
                        }
                    }

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                } else {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'offer_type' => 1,
                        'auction_type' => 1,
                        'countdown_hours' => 24,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'max_bid' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                }
            } else {

                return back()->with([
                    'flash_message'   => 'Ha ocurrido un error.',
                    'flash_class'     => 'alert-danger',
                    'flash_important' => true,
                ]);
            }
        } else {
            $data   =   AppOrgProduct::create([
                'name' => $request->name,
                'name_en' => $request->name_en,
                'release_date' => $request->release_date,
                'prod_category_id' => $request->prod_category_id,
                'game_category' => $request->game_category,
                'game_location' => $request->game_location,
                'game_generation' => $request->game_generation,
                'language' => implode(",", $request->language),
                'media' => $request->media,
                'large' => $request->large,
                'width' => $request->width,
                'high' => $request->high,
                'price_solo' => $request->price_solo,
                'price_used' => $request->price_used,
                'price_new' => $request->price_new,
                'platform' => $request->platform,
                'region' => $request->region,
                'ean_upc' => $request->ean_upc,
                'volume' => $request->volume,
                'weight' => $request->weight,
                'comments' => $request->comments,
                'image_path' => 'No',
                'is_enabled' => 'Y',
            ]);

            if ($data->save()) {

                if ($request->image_inventory) {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'offer_type' => 1,
                        'auction_type' => 1,
                        'countdown_hours' => 24,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'max_bid' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);
                    if ($data_inventory->save()) {
                        for ($i = 0; $i < count($request->image_inventory); $i++) {
                            AppOrgUserInventoryImage::create([
                                'inventory_id'          => $data_inventory->id,
                                'image_path'    => $request->image_inventory[$i],
                                'created_by' => \Auth::user()->id,
                            ]);
                        }

                        if ($request->imagen_product_ean) {
                            for ($i = 0; $i < count($request->image_inventory); $i++) {
                                AppOrgUserInventoryEan::create([
                                    'inventory_id'          => $data_inventory->id,
                                    'image_path'    => $request->image_inventory[$i],
                                    'created_by' => \Auth::user()->id,
                                ]);
                            }
                        }
                    }

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                } else {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'offer_type' => 1,
                        'auction_type' => 1,
                        'countdown_hours' => 24,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'max_bid' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                }
            } else {

                return back()->with([
                    'flash_message'   => 'Ha ocurrido un error.',
                    'flash_class'     => 'alert-danger',
                    'flash_important' => true,
                ]);
            }
        }

        if ($data->save()) {

            return redirect('11w5Cj9WDAjnzlg0/product')->with([
                'flash_class'   => 'alert-success',
                'flash_message' => 'Producto Actualizado con exito.'
            ]);
        } else {

            return redirect('11w5Cj9WDAjnzlg0/product')->with([
                'flash_class'     => 'alert-danger',
                'flash_message'   => 'Ha ocurrido un error.',
                'flash_important' => true
            ]);
        }
    }


    public function updateGame(Request $request, $id)
    {


        $data =  ProductRequest::findOrFail($id);
        $data->update([
            'status' => 'A',
            'aprobed_by' => \Auth::user()->user_name,
            'product_id' => $request->product
        ]);

        if ($request->hasFile('image_path')) {
            if ($request->has('box_language')) {
                $data   =   AppOrgProduct::create([
                    'name' => $request->name,
                    'name_en' => $request->name_en,
                    'release_date' => $request->release_date,
                    'prod_category_id' => $request->prod_category_id,
                    'game_category' => $request->game_category,
                    'game_location' => $request->game_location,
                    'game_generation' => $request->game_generation,
                    'language' => implode(",", $request->language),
                    'box_language' => implode(",", $request->box_language),
                    'large' => $request->large,
                    'width' => $request->width,
                    'high' => $request->high,
                    'price_solo' => $request->price_solo,
                    'price_used' => $request->price_used,
                    'price_new' => $request->price_new,
                    'media' => $request->media,
                    'platform' => $request->platform,
                    'region' => $request->region,
                    'ean_upc' => $request->ean_upc,
                    'volume' => $request->volume,
                    'weight' => $request->weight,
                    'comments' => $request->comments,
                    'image_path' => "",
                    'request_by' => $request->user_id,
                    'is_enabled' => 'Y',
                ]);
                if ($data->save()) {
                    $destinationPath = public_path('/images');

                    foreach ($request->file('image_path') as $e) {
                        $osi                = new AppOrgProductImage;
                        $osi->product_id = $data->id;

                        $nombre_imagen  = $data->game_generation . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $e->getClientOriginalName();
                        $osi->image_path    = $nombre_imagen;
                        $osi->created_by = \Auth::user()->id;
                        $upload_success = $e->move($destinationPath, $nombre_imagen);
                        $osi->save();
                    }
                    if ($request->image_inventory) {


                        $data_inventory   =   AppOrgUserInventory::create([
                            'user_id' => $request->user_id,
                            'product_id' => $data->id,
                            'box_condition' => $request->box,
                            'manual_condition' => $request->manual,
                            'cover_condition' => $request->cover,
                            'game_condition' => $request->game,
                            'extra_condition' => $request->extra,
                            'inside_condition' => $request->inside,
                            'location' => 1,
                            'quantity' => $request->quantity,
                            'quantity_sold' => 0,
                            'price' => $request->price,
                            'tax' => 0.0000,
                            'comments' => $request->description,
                            'status' => 'A',
                            'created_by' => \Auth::user()->id,
                            'image_path' => "",
                            'is_enabled' => 'Y',
                        ]);
                        if ($data_inventory->save()) {
                            for ($i = 0; $i < count($request->image_inventory); $i++) {
                                AppOrgUserInventoryImage::create([
                                    'inventory_id'          => $data_inventory->id,
                                    'image_path'    => $request->image_inventory[$i],
                                    'created_by' => \Auth::user()->id,
                                ]);
                            }

                            if ($request->imagen_product_ean) {
                                for ($i = 0; $i < count($request->image_inventory); $i++) {
                                    AppOrgUserInventoryEan::create([
                                        'inventory_id'          => $data_inventory->id,
                                        'image_path'    => $request->image_inventory[$i],
                                        'created_by' => \Auth::user()->id,
                                    ]);
                                }
                            }
                        }



                        return redirect("11w5Cj9WDAjnzlg0/product")->with([
                            'flash_message' => 'Producto solicitado agregado Con Exito',
                            'flash_class'   => 'alert-success',
                        ]);
                    } else {

                        $data_inventory   =   AppOrgUserInventory::create([
                            'user_id' => $request->user_id,
                            'product_id' => $data->id,
                            'box_condition' => $request->box,
                            'manual_condition' => $request->manual,
                            'cover_condition' => $request->cover,
                            'game_condition' => $request->game,
                            'extra_condition' => $request->extra,
                            'inside_condition' => $request->inside,
                            'location' => 1,
                            'quantity' => $request->quantity,
                            'quantity_sold' => 0,
                            'price' => $request->price,
                            'tax' => 0.0000,
                            'comments' => $request->description,
                            'status' => 'A',
                            'created_by' => \Auth::user()->id,
                            'image_path' => "",
                            'is_enabled' => 'Y',
                        ]);

                        return redirect("11w5Cj9WDAjnzlg0/product")->with([
                            'flash_message' => 'Producto solicitado agregado Con Exito',
                            'flash_class'   => 'alert-success',
                        ]);
                    }
                } else {
                    return back()->with([
                        'flash_message'   => 'Ha ocurrido un error.',
                        'flash_class'     => 'alert-danger',
                        'flash_important' => true,
                    ]);
                }
            } else {
                $data   =   AppOrgProduct::create([
                    'name' => $request->name,
                    'name_en' => $request->name_en,
                    'release_date' => $request->release_date,
                    'prod_category_id' => $request->prod_category_id,
                    'game_category' => $request->game_category,
                    'game_location' => $request->game_location,
                    'game_generation' => $request->game_generation,
                    'language' => implode(",", $request->language),
                    'media' => $request->media,
                    'large' => $request->large,
                    'width' => $request->width,
                    'high' => $request->high,
                    'price_solo' => $request->price_solo,
                    'price_used' => $request->price_used,
                    'price_new' => $request->price_new,
                    'platform' => $request->platform,
                    'region' => $request->region,
                    'ean_upc' => $request->ean_upc,
                    'volume' => $request->volume,
                    'weight' => $request->weight,
                    'comments' => $request->comments,
                    'image_path' => "",
                    'request_by' => $request->user_id,
                    'is_enabled' => 'Y',
                ]);

                if ($data->save()) {
                    $destinationPath = public_path('/images');

                    foreach ($request->file('image_path') as $e) {
                        $osi                = new AppOrgProductImage;
                        $osi->product_id = $data->id;

                        $nombre_imagen  = $data->game_generation . time() . $e->getClientOriginalName();
                        $osi->image_path    = $nombre_imagen;
                        $osi->created_by = \Auth::user()->id;
                        $upload_success = $e->move($destinationPath, $nombre_imagen);
                        $osi->save();
                    }

                    if ($request->image_inventory) {

                        $data_inventory   =   AppOrgUserInventory::create([
                            'user_id' => $request->user_id,
                            'product_id' => $data->id,
                            'box_condition' => $request->box,
                            'manual_condition' => $request->manual,
                            'cover_condition' => $request->cover,
                            'game_condition' => $request->game,
                            'extra_condition' => $request->extra,
                            'inside_condition' => $request->inside,
                            'location' => 1,
                            'quantity' => $request->quantity,
                            'quantity_sold' => 0,
                            'price' => $request->price,
                            'tax' => 0.0000,
                            'comments' => $request->description,
                            'status' => 'A',
                            'created_by' => \Auth::user()->id,
                            'image_path' => "",
                            'is_enabled' => 'Y',
                        ]);
                        if ($data_inventory->save()) {
                            for ($i = 0; $i < count($request->image_inventory); $i++) {
                                AppOrgUserInventoryImage::create([
                                    'inventory_id'          => $data_inventory->id,
                                    'image_path'    => $request->image_inventory[$i],
                                    'created_by' => \Auth::user()->id,
                                ]);
                            }

                            if ($request->imagen_product_ean) {
                                for ($i = 0; $i < count($request->image_inventory); $i++) {
                                    AppOrgUserInventoryEan::create([
                                        'inventory_id'          => $data_inventory->id,
                                        'image_path'    => $request->image_inventory[$i],
                                        'created_by' => \Auth::user()->id,
                                    ]);
                                }
                            }
                        }

                        return redirect("11w5Cj9WDAjnzlg0/product")->with([
                            'flash_message' => 'Producto solicitado agregado Con Exito',
                            'flash_class'   => 'alert-success',
                        ]);
                    } else {

                        $data_inventory   =   AppOrgUserInventory::create([
                            'user_id' => $request->user_id,
                            'product_id' => $data->id,
                            'box_condition' => $request->box,
                            'manual_condition' => $request->manual,
                            'cover_condition' => $request->cover,
                            'game_condition' => $request->game,
                            'extra_condition' => $request->extra,
                            'inside_condition' => $request->inside,
                            'location' => 1,
                            'quantity' => $request->quantity,
                            'quantity_sold' => 0,
                            'price' => $request->price,
                            'tax' => 0.0000,
                            'comments' => $request->description,
                            'status' => 'A',
                            'created_by' => \Auth::user()->id,
                            'image_path' => "",
                            'is_enabled' => 'Y',
                        ]);

                        return redirect("11w5Cj9WDAjnzlg0/product")->with([
                            'flash_message' => 'Producto solicitado agregado Con Exito',
                            'flash_class'   => 'alert-success',
                        ]);
                    }
                } else {

                    return back()->with([
                        'flash_message'   => 'Ha ocurrido un error.',
                        'flash_class'     => 'alert-danger',
                        'flash_important' => true,
                    ]);
                }
            }
        } elseif ($request->hasFile('image_path')) {
            $data   =   AppOrgProduct::create([
                'name' => $request->name,
                'name_en' => $request->name_en,
                'release_date' => $request->release_date,
                'prod_category_id' => $request->prod_category_id,
                'game_category' => $request->game_category,
                'game_location' => $request->game_location,
                'game_generation' => $request->game_generation,
                'language' => implode(",", $request->language),
                'box_language' => implode(",", $request->box_language),
                'media' => $request->media,
                'large' => $request->large,
                'width' => $request->width,
                'high' => $request->high,
                'price_solo' => $request->price_solo,
                'price_used' => $request->price_used,
                'price_new' => $request->price_new,
                'platform' => $request->platform,
                'region' => $request->region,
                'ean_upc' => $request->ean_upc,
                'volume' => $request->volume,
                'weight' => $request->weight,
                'comments' => $request->comments,
                'image_path' => "",
                'request_by' => $request->user_id,
                'is_enabled' => 'Y',
            ]);

            if ($data->save()) {
                $destinationPath = public_path('/images');

                foreach ($request->file('image_path') as $e) {
                    $osi                = new AppOrgProductImage;
                    $osi->product_id = $data->id;

                    $nombre_imagen  = $data->game_generation . time() . $e->getClientOriginalName();
                    $osi->image_path    = $nombre_imagen;
                    $osi->created_by = \Auth::user()->id;
                    $upload_success = $e->move($destinationPath, $nombre_imagen);
                    $osi->save();
                }

                if ($request->image_inventory) {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'price' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);
                    if ($data_inventory->save()) {
                        for ($i = 0; $i < count($request->image_inventory); $i++) {
                            AppOrgUserInventoryImage::create([
                                'inventory_id'          => $data_inventory->id,
                                'image_path'    => $request->image_inventory[$i],
                                'created_by' => \Auth::user()->id,
                            ]);
                        }

                        if ($request->imagen_product_ean) {
                            for ($i = 0; $i < count($request->image_inventory); $i++) {
                                AppOrgUserInventoryEan::create([
                                    'inventory_id'          => $data_inventory->id,
                                    'image_path'    => $request->image_inventory[$i],
                                    'created_by' => \Auth::user()->id,
                                ]);
                            }
                        }
                    }

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                } else {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'price' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                }
            } else {

                return back()->with([
                    'flash_message'   => 'Ha ocurrido un error.',
                    'flash_class'     => 'alert-danger',
                    'flash_important' => true,
                ]);
            }
        } elseif ($request->has('box_language')) {
            $data   =   AppOrgProduct::create([
                'name' => $request->name,
                'name_en' => $request->name_en,
                'release_date' => $request->release_date,
                'prod_category_id' => $request->prod_category_id,
                'game_category' => $request->game_category,
                'game_location' => $request->game_location,
                'game_generation' => $request->game_generation,
                'language' => implode(",", $request->language),
                'box_language' => implode(",", $request->box_language),
                'media' => $request->media,
                'large' => $request->large,
                'width' => $request->width,
                'high' => $request->high,
                'price_solo' => $request->price_solo,
                'price_used' => $request->price_used,
                'price_new' => $request->price_new,
                'platform' => $request->platform,
                'region' => $request->region,
                'ean_upc' => $request->ean_upc,
                'volume' => $request->volume,
                'weight' => $request->weight,
                'comments' => $request->comments,
                'image_path' => "",
                'request_by' => $request->user_id,
                'is_enabled' => 'Y',
            ]);
            if ($data->save()) {

                if ($request->image_inventory) {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'price' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);
                    if ($data_inventory->save()) {
                        for ($i = 0; $i < count($request->image_inventory); $i++) {
                            AppOrgUserInventoryImage::create([
                                'inventory_id'          => $data_inventory->id,
                                'image_path'    => $request->image_inventory[$i],
                                'created_by' => \Auth::user()->id,
                            ]);
                        }

                        if ($request->imagen_product_ean) {
                            for ($i = 0; $i < count($request->image_inventory); $i++) {
                                AppOrgUserInventoryEan::create([
                                    'inventory_id'          => $data_inventory->id,
                                    'image_path'    => $request->image_inventory[$i],
                                    'created_by' => \Auth::user()->id,
                                ]);
                            }
                        }
                    }

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                } else {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'price' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                }
            } else {

                return back()->with([
                    'flash_message'   => 'Ha ocurrido un error.',
                    'flash_class'     => 'alert-danger',
                    'flash_important' => true,
                ]);
            }
        } else {
            $data   =   AppOrgProduct::create([
                'name' => $request->name,
                'name_en' => $request->name_en,
                'release_date' => $request->release_date,
                'prod_category_id' => $request->prod_category_id,
                'game_category' => $request->game_category,
                'game_location' => $request->game_location,
                'game_generation' => $request->game_generation,
                'language' => implode(",", $request->language),
                'media' => $request->media,
                'large' => $request->large,
                'width' => $request->width,
                'high' => $request->high,
                'price_solo' => $request->price_solo,
                'price_used' => $request->price_used,
                'price_new' => $request->price_new,
                'platform' => $request->platform,
                'region' => $request->region,
                'ean_upc' => $request->ean_upc,
                'volume' => $request->volume,
                'weight' => $request->weight,
                'comments' => $request->comments,
                'image_path' => 'No',
                'is_enabled' => 'Y',
            ]);

            if ($data->save()) {

                if ($request->image_inventory) {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'price' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);
                    if ($data_inventory->save()) {
                        for ($i = 0; $i < count($request->image_inventory); $i++) {
                            AppOrgUserInventoryImage::create([
                                'inventory_id'          => $data_inventory->id,
                                'image_path'    => $request->image_inventory[$i],
                                'created_by' => \Auth::user()->id,
                            ]);
                        }

                        if ($request->imagen_product_ean) {
                            for ($i = 0; $i < count($request->image_inventory); $i++) {
                                AppOrgUserInventoryEan::create([
                                    'inventory_id'          => $data_inventory->id,
                                    'image_path'    => $request->image_inventory[$i],
                                    'created_by' => \Auth::user()->id,
                                ]);
                            }
                        }
                    }

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                } else {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'price' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                }
            } else {

                return back()->with([
                    'flash_message'   => 'Ha ocurrido un error.',
                    'flash_class'     => 'alert-danger',
                    'flash_important' => true,
                ]);
            }
        }

        if ($data->save()) {

            return redirect('11w5Cj9WDAjnzlg0/product')->with([
                'flash_class'   => 'alert-success',
                'flash_message' => 'Producto Actualizado con exito.'
            ]);
        } else {

            return redirect('11w5Cj9WDAjnzlg0/product')->with([
                'flash_class'     => 'alert-danger',
                'flash_message'   => 'Ha ocurrido un error.',
                'flash_important' => true
            ]);
        }
    }

    public function updateCollection(Request $request, $id)
    {


        $data =  ProductRequest::findOrFail($id);
        $data->update([
            'status' => 'A',
            'aprobed_by' => \Auth::user()->user_name,
            'product_id' => $request->product
        ]);

        if ($request->hasFile('image_path')) {
            if ($request->has('box_language')) {
                $data   =   AppOrgProduct::create([
                    'name' => $request->name,
                    'name_en' => $request->name_en,
                    'release_date' => $request->release_date,
                    'prod_category_id' => $request->prod_category_id,
                    'game_category' => $request->game_category,
                    'game_location' => $request->game_location,
                    'game_generation' => $request->game_generation,
                    'language' => implode(",", $request->language),
                    'box_language' => implode(",", $request->box_language),
                    'media' => $request->media,
                    'large' => $request->large,
                    'width' => $request->width,
                    'high' => $request->high,
                    'price_solo' => $request->price_solo,
                    'price_used' => $request->price_used,
                    'price_new' => $request->price_new,
                    'platform' => $request->platform,
                    'region' => $request->region,
                    'ean_upc' => $request->ean_upc,
                    'volume' => $request->volume,
                    'weight' => $request->weight,
                    'comments' => $request->comments,
                    'image_path' => "",
                    'request_by' => $request->user_id,
                    'is_enabled' => 'Y',
                ]);
                if ($data->save()) {
                    $destinationPath = public_path('/images');

                    foreach ($request->file('image_path') as $e) {
                        $osi                = new AppOrgProductImage;
                        $osi->product_id = $data->id;

                        $nombre_imagen  = $data->game_generation . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $e->getClientOriginalName();
                        $osi->image_path    = $nombre_imagen;
                        $osi->created_by = \Auth::user()->id;
                        $upload_success = $e->move($destinationPath, $nombre_imagen);
                        $osi->save();
                    }
                    if ($request->image_inventory) {


                        $data_inventory   =   AppOrgUserInventory::create([
                            'user_id' => $request->user_id,
                            'product_id' => $data->id,
                            'box_condition' => $request->box,
                            'manual_condition' => $request->manual,
                            'cover_condition' => $request->cover,
                            'game_condition' => $request->game,
                            'extra_condition' => $request->extra,
                            'inside_condition' => $request->inside,
                            'location' => 1,
                            'quantity' => $request->quantity,
                            'quantity_sold' => 0,
                            'price' => $request->price,
                            'tax' => 0.0000,
                            'comments' => $request->description,
                            'status' => 'A',
                            'in_collection' => 'Y',
                            'created_by' => \Auth::user()->id,
                            'image_path' => "",
                            'is_enabled' => 'Y',
                        ]);
                        if ($data_inventory->save()) {
                            for ($i = 0; $i < count($request->image_inventory); $i++) {
                                AppOrgUserInventoryImage::create([
                                    'inventory_id'          => $data_inventory->id,
                                    'image_path'    => $request->image_inventory[$i],
                                    'created_by' => \Auth::user()->id,
                                ]);
                            }

                            if ($request->imagen_product_ean) {
                                for ($i = 0; $i < count($request->image_inventory); $i++) {
                                    AppOrgUserInventoryEan::create([
                                        'inventory_id'          => $data_inventory->id,
                                        'image_path'    => $request->image_inventory[$i],
                                        'created_by' => \Auth::user()->id,
                                    ]);
                                }
                            }
                        }



                        return redirect("11w5Cj9WDAjnzlg0/product")->with([
                            'flash_message' => 'Producto solicitado agregado Con Exito',
                            'flash_class'   => 'alert-success',
                        ]);
                    } else {

                        $data_inventory   =   AppOrgUserInventory::create([
                            'user_id' => $request->user_id,
                            'product_id' => $data->id,
                            'box_condition' => $request->box,
                            'manual_condition' => $request->manual,
                            'cover_condition' => $request->cover,
                            'game_condition' => $request->game,
                            'extra_condition' => $request->extra,
                            'inside_condition' => $request->inside,
                            'location' => 1,
                            'quantity' => $request->quantity,
                            'quantity_sold' => 0,
                            'price' => $request->price,
                            'tax' => 0.0000,
                            'comments' => $request->description,
                            'status' => 'A',
                            'in_collection' => 'Y',
                            'created_by' => \Auth::user()->id,
                            'image_path' => "",
                            'is_enabled' => 'Y',
                        ]);

                        return redirect("11w5Cj9WDAjnzlg0/product")->with([
                            'flash_message' => 'Producto solicitado agregado Con Exito',
                            'flash_class'   => 'alert-success',
                        ]);
                    }
                } else {
                    return back()->with([
                        'flash_message'   => 'Ha ocurrido un error.',
                        'flash_class'     => 'alert-danger',
                        'flash_important' => true,
                    ]);
                }
            } else {
                $data   =   AppOrgProduct::create([
                    'name' => $request->name,
                    'name_en' => $request->name_en,
                    'release_date' => $request->release_date,
                    'prod_category_id' => $request->prod_category_id,
                    'game_category' => $request->game_category,
                    'game_location' => $request->game_location,
                    'game_generation' => $request->game_generation,
                    'language' => implode(",", $request->language),
                    'media' => $request->media,
                    'large' => $request->large,
                    'width' => $request->width,
                    'high' => $request->high,
                    'price_solo' => $request->price_solo,
                    'price_used' => $request->price_used,
                    'price_new' => $request->price_new,
                    'platform' => $request->platform,
                    'region' => $request->region,
                    'ean_upc' => $request->ean_upc,
                    'volume' => $request->volume,
                    'weight' => $request->weight,
                    'comments' => $request->comments,
                    'image_path' => "",
                    'request_by' => $request->user_id,
                    'is_enabled' => 'Y',
                ]);

                if ($data->save()) {
                    $destinationPath = public_path('/images');

                    foreach ($request->file('image_path') as $e) {
                        $osi                = new AppOrgProductImage;
                        $osi->product_id = $data->id;

                        $nombre_imagen  = $data->game_generation . time() . $e->getClientOriginalName();
                        $osi->image_path    = $nombre_imagen;
                        $osi->created_by = \Auth::user()->id;
                        $upload_success = $e->move($destinationPath, $nombre_imagen);
                        $osi->save();
                    }

                    if ($request->image_inventory) {

                        $data_inventory   =   AppOrgUserInventory::create([
                            'user_id' => $request->user_id,
                            'product_id' => $data->id,
                            'box_condition' => $request->box,
                            'manual_condition' => $request->manual,
                            'cover_condition' => $request->cover,
                            'game_condition' => $request->game,
                            'extra_condition' => $request->extra,
                            'inside_condition' => $request->inside,
                            'location' => 1,
                            'quantity' => $request->quantity,
                            'quantity_sold' => 0,
                            'price' => $request->price,
                            'tax' => 0.0000,
                            'comments' => $request->description,
                            'status' => 'A',
                            'in_collection' => 'Y',
                            'created_by' => \Auth::user()->id,
                            'image_path' => "",
                            'is_enabled' => 'Y',
                        ]);
                        if ($data_inventory->save()) {
                            for ($i = 0; $i < count($request->image_inventory); $i++) {
                                AppOrgUserInventoryImage::create([
                                    'inventory_id'          => $data_inventory->id,
                                    'image_path'    => $request->image_inventory[$i],
                                    'created_by' => \Auth::user()->id,
                                ]);
                            }

                            if ($request->imagen_product_ean) {
                                for ($i = 0; $i < count($request->image_inventory); $i++) {
                                    AppOrgUserInventoryEan::create([
                                        'inventory_id'          => $data_inventory->id,
                                        'image_path'    => $request->image_inventory[$i],
                                        'created_by' => \Auth::user()->id,
                                    ]);
                                }
                            }
                        }

                        return redirect("11w5Cj9WDAjnzlg0/product")->with([
                            'flash_message' => 'Producto solicitado agregado Con Exito',
                            'flash_class'   => 'alert-success',
                        ]);
                    } else {

                        $data_inventory   =   AppOrgUserInventory::create([
                            'user_id' => $request->user_id,
                            'product_id' => $data->id,
                            'box_condition' => $request->box,
                            'manual_condition' => $request->manual,
                            'cover_condition' => $request->cover,
                            'game_condition' => $request->game,
                            'extra_condition' => $request->extra,
                            'inside_condition' => $request->inside,
                            'location' => 1,
                            'quantity' => $request->quantity,
                            'quantity_sold' => 0,
                            'price' => $request->price,
                            'tax' => 0.0000,
                            'comments' => $request->description,
                            'status' => 'A',
                            'in_collection' => 'Y',
                            'created_by' => \Auth::user()->id,
                            'image_path' => "",
                            'is_enabled' => 'Y',
                        ]);

                        return redirect("11w5Cj9WDAjnzlg0/product")->with([
                            'flash_message' => 'Producto solicitado agregado Con Exito',
                            'flash_class'   => 'alert-success',
                        ]);
                    }
                } else {

                    return back()->with([
                        'flash_message'   => 'Ha ocurrido un error.',
                        'flash_class'     => 'alert-danger',
                        'flash_important' => true,
                    ]);
                }
            }
        } elseif ($request->hasFile('image_path')) {
            $data   =   AppOrgProduct::create([
                'name' => $request->name,
                'name_en' => $request->name_en,
                'release_date' => $request->release_date,
                'prod_category_id' => $request->prod_category_id,
                'game_category' => $request->game_category,
                'game_location' => $request->game_location,
                'game_generation' => $request->game_generation,
                'language' => implode(",", $request->language),
                'box_language' => implode(",", $request->box_language),
                'media' => $request->media,
                'large' => $request->large,
                'width' => $request->width,
                'high' => $request->high,
                'price_solo' => $request->price_solo,
                'price_used' => $request->price_used,
                'price_new' => $request->price_new,
                'platform' => $request->platform,
                'region' => $request->region,
                'ean_upc' => $request->ean_upc,
                'volume' => $request->volume,
                'weight' => $request->weight,
                'comments' => $request->comments,
                'image_path' => "",
                'request_by' => $request->user_id,
                'is_enabled' => 'Y',
            ]);

            if ($data->save()) {
                $destinationPath = public_path('/images');

                foreach ($request->file('image_path') as $e) {
                    $osi                = new AppOrgProductImage;
                    $osi->product_id = $data->id;

                    $nombre_imagen  = $data->game_generation . time() . $e->getClientOriginalName();
                    $osi->image_path    = $nombre_imagen;
                    $osi->created_by = \Auth::user()->id;
                    $upload_success = $e->move($destinationPath, $nombre_imagen);
                    $osi->save();
                }

                if ($request->image_inventory) {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'price' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'in_collection' => 'Y',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);
                    if ($data_inventory->save()) {
                        for ($i = 0; $i < count($request->image_inventory); $i++) {
                            AppOrgUserInventoryImage::create([
                                'inventory_id'          => $data_inventory->id,
                                'image_path'    => $request->image_inventory[$i],
                                'created_by' => \Auth::user()->id,
                            ]);
                        }

                        if ($request->imagen_product_ean) {
                            for ($i = 0; $i < count($request->image_inventory); $i++) {
                                AppOrgUserInventoryEan::create([
                                    'inventory_id'          => $data_inventory->id,
                                    'image_path'    => $request->image_inventory[$i],
                                    'created_by' => \Auth::user()->id,
                                ]);
                            }
                        }
                    }

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                } else {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'price' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'in_collection' => 'Y',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                }
            } else {

                return back()->with([
                    'flash_message'   => 'Ha ocurrido un error.',
                    'flash_class'     => 'alert-danger',
                    'flash_important' => true,
                ]);
            }
        } elseif ($request->has('box_language')) {
            $data   =   AppOrgProduct::create([
                'name' => $request->name,
                'name_en' => $request->name_en,
                'release_date' => $request->release_date,
                'prod_category_id' => $request->prod_category_id,
                'game_category' => $request->game_category,
                'game_location' => $request->game_location,
                'game_generation' => $request->game_generation,
                'language' => implode(",", $request->language),
                'box_language' => implode(",", $request->box_language),
                'media' => $request->media,
                'large' => $request->large,
                'width' => $request->width,
                'high' => $request->high,
                'price_solo' => $request->price_solo,
                'price_used' => $request->price_used,
                'price_new' => $request->price_new,
                'platform' => $request->platform,
                'region' => $request->region,
                'ean_upc' => $request->ean_upc,
                'volume' => $request->volume,
                'weight' => $request->weight,
                'comments' => $request->comments,
                'image_path' => "",
                'request_by' => $request->user_id,
                'is_enabled' => 'Y',
            ]);
            if ($data->save()) {

                if ($request->image_inventory) {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'price' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'in_collection' => 'Y',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);
                    if ($data_inventory->save()) {
                        for ($i = 0; $i < count($request->image_inventory); $i++) {
                            AppOrgUserInventoryImage::create([
                                'inventory_id'          => $data_inventory->id,
                                'image_path'    => $request->image_inventory[$i],
                                'created_by' => \Auth::user()->id,
                            ]);
                        }

                        if ($request->imagen_product_ean) {
                            for ($i = 0; $i < count($request->image_inventory); $i++) {
                                AppOrgUserInventoryEan::create([
                                    'inventory_id'          => $data_inventory->id,
                                    'image_path'    => $request->image_inventory[$i],
                                    'created_by' => \Auth::user()->id,
                                ]);
                            }
                        }
                    }

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                } else {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'price' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'in_collection' => 'Y',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                }
            } else {

                return back()->with([
                    'flash_message'   => 'Ha ocurrido un error.',
                    'flash_class'     => 'alert-danger',
                    'flash_important' => true,
                ]);
            }
        } else {
            $data   =   AppOrgProduct::create([
                'name' => $request->name,
                'name_en' => $request->name_en,
                'release_date' => $request->release_date,
                'prod_category_id' => $request->prod_category_id,
                'game_category' => $request->game_category,
                'game_location' => $request->game_location,
                'game_generation' => $request->game_generation,
                'language' => implode(",", $request->language),
                'media' => $request->media,
                'large' => $request->large,
                'width' => $request->width,
                'high' => $request->high,
                'price_solo' => $request->price_solo,
                'price_used' => $request->price_used,
                'price_new' => $request->price_new,
                'platform' => $request->platform,
                'region' => $request->region,
                'ean_upc' => $request->ean_upc,
                'volume' => $request->volume,
                'weight' => $request->weight,
                'comments' => $request->comments,
                'image_path' => 'No',
                'is_enabled' => 'Y',
            ]);

            if ($data->save()) {

                if ($request->image_inventory) {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'price' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'in_collection' => 'Y',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);
                    if ($data_inventory->save()) {
                        for ($i = 0; $i < count($request->image_inventory); $i++) {
                            AppOrgUserInventoryImage::create([
                                'inventory_id'          => $data_inventory->id,
                                'image_path'    => $request->image_inventory[$i],
                                'created_by' => \Auth::user()->id,
                            ]);
                        }

                        if ($request->imagen_product_ean) {
                            for ($i = 0; $i < count($request->image_inventory); $i++) {
                                AppOrgUserInventoryEan::create([
                                    'inventory_id'          => $data_inventory->id,
                                    'image_path'    => $request->image_inventory[$i],
                                    'created_by' => \Auth::user()->id,
                                ]);
                            }
                        }
                    }

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                } else {

                    $data_inventory   =   AppOrgUserInventory::create([
                        'user_id' => $request->user_id,
                        'product_id' => $data->id,
                        'box_condition' => $request->box,
                        'manual_condition' => $request->manual,
                        'cover_condition' => $request->cover,
                        'game_condition' => $request->game,
                        'extra_condition' => $request->extra,
                        'inside_condition' => $request->inside,
                        'location' => 1,
                        'quantity' => $request->quantity,
                        'quantity_sold' => 0,
                        'price' => $request->price,
                        'tax' => 0.0000,
                        'comments' => $request->description,
                        'status' => 'A',
                        'in_collection' => 'Y',
                        'created_by' => \Auth::user()->id,
                        'image_path' => "",
                        'is_enabled' => 'Y',
                    ]);

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto solicitado agregado Con Exito',
                        'flash_class'   => 'alert-success',
                    ]);
                }
            } else {

                return back()->with([
                    'flash_message'   => 'Ha ocurrido un error.',
                    'flash_class'     => 'alert-danger',
                    'flash_important' => true,
                ]);
            }
        }

        if ($data->save()) {

            return redirect('11w5Cj9WDAjnzlg0/product')->with([
                'flash_class'   => 'alert-success',
                'flash_message' => 'Producto Actualizado con exito.'
            ]);
        } else {

            return redirect('11w5Cj9WDAjnzlg0/product')->with([
                'flash_class'     => 'alert-danger',
                'flash_message'   => 'Ha ocurrido un error.',
                'flash_important' => true
            ]);
        }
    }


    public function updateProductExistAuction(Request $request, $id)
    {

        $data =  ProductRequest::findOrFail($id);
        //dd($data->user->user_name);
        $data->update([
            'status' => 'A',
            'aprobed_by' => \Auth::user()->user_name,
            'product_id' => $request->product
        ]);

        if ($request->image_inventory) {

            $data_inventory   =   AppOrgUserInventory::create([
                'user_id' => $request->user_id,
                'product_id' => $request->product,
                'box_condition' => $request->box,
                'manual_condition' => $request->manual,
                'cover_condition' => $request->cover,
                'game_condition' => $request->game,
                'extra_condition' => $request->extra,
                'inside_condition' => $request->inside,
                'location' => 1,
                'offer_type' => 1,
                'auction_type' => 1,
                'countdown_hours' => 24,
                'quantity' => $request->quantity,
                'quantity_sold' => 0,
                'max_bid' => $request->price,
                'tax' => 0.0000,
                'comments' => $request->description,
                'status' => 'A',
                'created_by' => \Auth::user()->id,
                'image_path' => "",
                'is_enabled' => 'Y',
            ]);
            if ($data_inventory->save()) {
                for ($i = 0; $i < count($request->image_inventory); $i++) {
                    AppOrgUserInventoryImage::create([
                        'inventory_id'          => $data_inventory->id,
                        'image_path'    => $request->image_inventory[$i],
                        'created_by' => \Auth::user()->id,
                    ]);
                }

                if ($request->imagen_product_ean) {
                    for ($i = 0; $i < count($request->image_inventory); $i++) {
                        AppOrgUserInventoryEan::create([
                            'inventory_id'          => $data_inventory->id,
                            'image_path'    => $request->image_inventory[$i],
                            'created_by' => \Auth::user()->id,
                        ]);
                    }
                }
            }



            return redirect("11w5Cj9WDAjnzlg0/product")->with([
                'flash_message' => 'Producto solicitado agregado Con Exito',
                'flash_class'   => 'alert-success',
            ]);
        } else {

            $data_inventory   =   AppOrgUserInventory::create([
                'user_id' => $request->user_id,
                'product_id' => $request->product,
                'box_condition' => $request->box,
                'manual_condition' => $request->manual,
                'cover_condition' => $request->cover,
                'game_condition' => $request->game,
                'extra_condition' => $request->extra,
                'inside_condition' => $request->inside,
                'location' => 1,
                'offer_type' => 1,
                'auction_type' => 1,
                'quantity' => $request->quantity,
                'quantity_sold' => 0,
                'max_bid' => $request->price,
                'tax' => 0.0000,
                'countdown_hours' => 24,
                'comments' => $request->description,
                'status' => 'A',
                'created_by' => \Auth::user()->id,
                'image_path' => "",
                'is_enabled' => 'Y',
            ]);



            return redirect("11w5Cj9WDAjnzlg0/product")->with([
                'flash_message' => 'Producto solicitado agregado Con Exito',
                'flash_class'   => 'alert-success',
            ]);
        }
    }

    public function updateProductExist(Request $request, $id)
    {

        $data =  ProductRequest::findOrFail($id);
        //dd($data->user->user_name);
        $data->update([
            'status' => 'A',
            'aprobed_by' => \Auth::user()->user_name,
            'product_id' => $request->product
        ]);

        if ($request->image_inventory) {

            $data_inventory   =   AppOrgUserInventory::create([
                'user_id' => $request->user_id,
                'product_id' => $request->product,
                'box_condition' => $request->box,
                'manual_condition' => $request->manual,
                'cover_condition' => $request->cover,
                'game_condition' => $request->game,
                'extra_condition' => $request->extra,
                'inside_condition' => $request->inside,
                'location' => 1,
                'quantity' => $request->quantity,
                'quantity_sold' => 0,
                'price' => $request->price,
                'tax' => 0.0000,
                'comments' => $request->description,
                'status' => 'A',
                'created_by' => \Auth::user()->id,
                'image_path' => "",
                'is_enabled' => 'Y',
            ]);
            if ($data_inventory->save()) {
                for ($i = 0; $i < count($request->image_inventory); $i++) {
                    AppOrgUserInventoryImage::create([
                        'inventory_id'          => $data_inventory->id,
                        'image_path'    => $request->image_inventory[$i],
                        'created_by' => \Auth::user()->id,
                    ]);
                }

                if ($request->imagen_product_ean) {
                    for ($i = 0; $i < count($request->image_inventory); $i++) {
                        AppOrgUserInventoryEan::create([
                            'inventory_id'          => $data_inventory->id,
                            'image_path'    => $request->image_inventory[$i],
                            'created_by' => \Auth::user()->id,
                        ]);
                    }
                }
            }
 

            return redirect("11w5Cj9WDAjnzlg0/product")->with([
                'flash_message' => 'Producto solicitado agregado Con Exito',
                'flash_class'   => 'alert-success',
            ]);
        } else {

            $data_inventory   =   AppOrgUserInventory::create([
                'user_id' => $request->user_id,
                'product_id' => $request->product,
                'box_condition' => $request->box,
                'manual_condition' => $request->manual,
                'cover_condition' => $request->cover,
                'game_condition' => $request->game,
                'extra_condition' => $request->extra,
                'inside_condition' => $request->inside,
                'location' => 1,
                'quantity' => $request->quantity,
                'quantity_sold' => 0,
                'price' => $request->price,
                'tax' => 0.0000,
                'comments' => $request->description,
                'status' => 'A',
                'created_by' => \Auth::user()->id,
                'image_path' => "",
                'is_enabled' => 'Y',
            ]);

            $mail = new RequestProductInventory($data->user, $data_inventory);

            // Envía el correo
            Mail::to($data->user->email)->send($mail);

            return redirect("11w5Cj9WDAjnzlg0/product")->with([
                'flash_message' => 'Producto solicitado agregado Con Exito',
                'flash_class'   => 'alert-success',
            ]);
        }
    }

    public function updateProductCollectionExist(Request $request, $id)
    {

        $data =  ProductRequest::findOrFail($id);
        //dd($data->user->user_name);
        $data->update([
            'status' => 'A',
            'aprobed_by' => \Auth::user()->user_name,
            'product_id' => $request->product
        ]);

        if ($request->image_inventory) {

            $data_inventory   =   AppOrgUserInventory::create([
                'user_id' => $request->user_id,
                'product_id' => $request->product,
                'box_condition' => $request->box,
                'manual_condition' => $request->manual,
                'cover_condition' => $request->cover,
                'game_condition' => $request->game,
                'extra_condition' => $request->extra,
                'inside_condition' => $request->inside,
                'location' => 1,
                'quantity' => $request->quantity,
                'quantity_sold' => 0,
                'price' => $request->price,
                'tax' => 0.0000,
                'comments' => $request->description,
                'status' => 'A',
                'in_collection' => 'Y',
                'created_by' => \Auth::user()->id,
                'image_path' => "",
                'is_enabled' => 'Y',
            ]);
            if ($data_inventory->save()) {
                for ($i = 0; $i < count($request->image_inventory); $i++) {
                    AppOrgUserInventoryImage::create([
                        'inventory_id'          => $data_inventory->id,
                        'image_path'    => $request->image_inventory[$i],
                        'created_by' => \Auth::user()->id,
                    ]);
                }

                if ($request->imagen_product_ean) {
                    for ($i = 0; $i < count($request->image_inventory); $i++) {
                        AppOrgUserInventoryEan::create([
                            'inventory_id'          => $data_inventory->id,
                            'image_path'    => $request->image_inventory[$i],
                            'created_by' => \Auth::user()->id,
                        ]);
                    }
                }
            }



            return redirect("11w5Cj9WDAjnzlg0/product-request")->with([
                'flash_message' => 'Producto solicitado agregado Con Exito',
                'flash_class'   => 'alert-success',
            ]);
        } else {

            $data_inventory   =   AppOrgUserInventory::create([
                'user_id' => $request->user_id,
                'product_id' => $request->product,
                'box_condition' => $request->box,
                'manual_condition' => $request->manual,
                'cover_condition' => $request->cover,
                'game_condition' => $request->game,
                'extra_condition' => $request->extra,
                'inside_condition' => $request->inside,
                'location' => 1,
                'quantity' => $request->quantity,
                'quantity_sold' => 0,
                'price' => $request->price,
                'tax' => 0.0000,
                'comments' => $request->description,
                'status' => 'A',
                'in_collection' => 'Y',
                'created_by' => \Auth::user()->id,
                'image_path' => "",
                'is_enabled' => 'Y',
            ]);



            return redirect("11w5Cj9WDAjnzlg0/product")->with([
                'flash_message' => 'Producto solicitado agregado Con Exito',
                'flash_class'   => 'alert-success',
            ]);
        }
    }

    public function ProductRequestDestroy($id)
    {
        $data = ProductRequest::findOrFail($id);

        if ($data->delete()) {
            return redirect('11w5Cj9WDAjnzlg0/product-request')->with([
                'flash_class'     => 'alert-warning',
                'flash_message'   => 'Solicitud de Producto Rechazada / Eliminada.',
                'flash_important' => true
            ]);
        } else {
            return redirect('11w5Cj9WDAjnzlg0/product')->with([
                'flash_class'     => 'alert-danger',
                'flash_message'   => 'Ha ocurrido un error.',
                'flash_important' => true
            ]);
        }
    }
}
