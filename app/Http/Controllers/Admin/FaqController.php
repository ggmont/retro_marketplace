<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\AppOrgFaq;
use Redirect;
use Request;
use Validator;
use Config;

class FaqController extends Controller
{
    public function __construct() {
		$this->columnPrefix =  Config::get('brcode.column_prefix');
		$this->viewData['column_prefix'] = $this->columnPrefix;
	}
	
    public function index()
    {

        $faq       = AppOrgFaq::all();

        return view('admin.faq', [
            'faq'     => $faq,
        ]);
    }

    public function edit($id = 0)
    {
        $prueba = AppOrgFaq::findOrFail($id);

        $this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('org.faq');
        $this->viewData['form_list_title'] = trans('app.faqs');
        $this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/marketplace/faqs');
        $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/faqs/save');
        $this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/marketplace/faqs/delete');
        $this->viewData['model_id'] = 0;

        if ($id > 0) {
            $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/faqs/modify');
            $this->viewData['model'] = AppOrgFaq::find($id);
            $this->viewData['model'] = $this->viewData['model']->toArray();
            $this->viewData['model_id'] = $id;
        }

        $this->viewData['model_cols'] = [
            'id' => ['type' => 'hidden', 'attr' => ' '],
            'question'                             =>  ['type' => 'input-text', 'attr' => ' maxlength=255 ', 'label' => trans('org.col_question')],
            'answer'                           =>  ['type' => 'html-editor', 'class' => 'br-html-editor', 'attr' => ' rows="10" ', 'label' => trans('org.col_answer'),],
            'is_enabled'              =>  ['type' => 'select', 'select_values' => [['id' => 'Y', 'value' => trans('app.yes')], ['id' => 'N', 'value' => trans('app.no')]], 'class' => 'br-select2'],
        ];

        return view('admin.faq.edit', [
            "prueba" => $prueba,
        ])->with('viewData', $this->viewData);

    }

    public function updateFaq($id = 0) {
        $colPrefix = $this->columnPrefix;
    
            $input = Request::all();
            //dd($input);
    
            $id = $input['model_id'] > 0 ? $input['model_id'] : 0;
    
            $rules = [
                $colPrefix.'question' 			  => 'required|min:3|max:50|unique:appOrgPages,title' . ($id > 0 ? ','.$id : ''),
                $colPrefix.'answer'           => 'required',
                $colPrefix.'is_enabled' 	    => 'required',
            ];
    
            $input = Request::all();
    
            $v = Validator::make($input, $rules);
    
            if($v->fails()) {
                return Redirect::back()
                    ->withErrors($v->errors()) // send back all errors to the login form
                    ->withInput(Request::all());
            }
            else {
    
                $id = saveModel($id, $model, 'App\\AppOrgFaq',$subModelExists, $input, $colPrefix, $new);
                return redirect('/11w5Cj9WDAjnzlg0/faqs')->with([
                    'flash_message' => 'Actualizado correctamente.',
                    'flash_class'   => 'alert-success',
                ]);
    
            }
      }
}
