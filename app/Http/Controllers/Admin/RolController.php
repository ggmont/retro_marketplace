<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\SysRoles;
use Illuminate\Http\Request;

class RolController extends Controller
{
    public function updateRol(Request $request, $id)
    {
        $rol = SysRoles::find($id);
		//dd($request->all());

		$rol->update([
			'name' => $request->name,
            'is_enabled' => $request->is_enabled,
            'description' => $request->description
		]);

		return redirect("11w5Cj9WDAjnzlg0/management/roles")->with([
			'flash_message' => 'Rol actualizado con exito',
			'flash_class'   => 'alert-success',
		]);

    }
}
