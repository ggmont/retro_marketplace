<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\AppOrgCategory;

class CategoryController extends Controller
{
    public function index() {
    	
    	$categories       = AppOrgCategory::all();

        return view('admin.categories', [
            'categories'     => $categories,
        ]);
    }
}
