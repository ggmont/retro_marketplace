<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
use Response;
use Illuminate\Http\Request;
use App\AppOrgCategory;
use App\AppOrgProduct;
use App\AppOrgUserInventory;
use App\AppOrgOrderDetail;
use App\SysUser;
use App\SysDictionary;
use App\AppOrgProductImage;
use DB;

class ProductController extends Controller
{
    public function index() {

        return view('admin.product', [
            
        ]);
    }

    public function create()
    {
     

        return view('admin.product.create', [

        ]);
    }

    public function edit($id)
    {
        $product = AppOrgProduct::findOrFail($id);

        $prueba = $product->images;
        //dd($prueba);

        $categories = AppOrgCategory::all();
        $genero = SysDictionary::where("code", "GAME_CATEGORY")->get();
        $location = SysDictionary::where("code", "GAME_LOCATION")->get();
        $generation = SysDictionary::where("code", "GAME_GENERATION")->get();
        $language = SysDictionary::where("code", "GAME_LANGUAGE")->get();
        $media = SysDictionary::where("code", "GAME_SUPPORT")->get();
        $platform = SysDictionary::where("code", "GAME_PLATFORM")->get();
        $region = SysDictionary::where("code", "GAME_REGION")->get();
        return view(
            "admin.product.edit",
            compact(
                'product',
                'prueba',
                'categories',
                'genero',
                'location',
                'generation',
                'language',
                'media',
                'platform',
                'region'
            )
        );
    }

    public function store(Request $request)
    {
        if ($request->hasFile('image_path')) {
            if ($request->has('box_language')) {
                $data   =   AppOrgProduct::create([
                    'name' => $request->name,
                    'name_en' => $request->name_en,
                    'release_date' => $request->release_date,
                    'prod_category_id' => $request->prod_category_id,
                    'game_category' => $request->game_category,
                    'game_location' => $request->game_location,
                    'game_generation' => $request->game_generation,
                    'language' => implode(",", $request->language),
                    'box_language' => implode(",", $request->box_language),
                    'media' => $request->media,
                    'platform' => $request->platform,
                    'region' => $request->region,
                    'ean_upc' => $request->ean_upc,
                    'volume' => $request->volume,
                    'large' => $request->large,
                    'width' => $request->width,
                    'high' => $request->high,
                    'weight' => $request->weight,
                    'price_used' => $request->price_used,
                    'price_new' => $request->price_new,
                    'price_solo' => $request->price_solo,
                    'comments' => $request->comments,
                    'image_path' => "",
                    'is_enabled' => 'Y',
                    ]);
                    if ($data->save()) {
                        $destinationPath = public_path('/images');

                        foreach ($request->file('image_path') as $e) {
                            $osi                = new AppOrgProductImage;
                            $osi->product_id = $data->id;

                            $nombre_imagen  = $data->game_generation . time() . $e->getClientOriginalName();
                            $osi->image_path    = $nombre_imagen;
                            $osi->created_by = \Auth::user()->id;
                            $upload_success = $e->move($destinationPath, $nombre_imagen);
                            $osi->save();

                        }

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto agregado Con Exito ',
                        'flash_class'   => 'alert-success',
                    ]);

                } else {

                    return back()->with([
                        'flash_message'   => 'Ha ocurrido un error.',
                        'flash_class'     => 'alert-danger',
                        'flash_important' => true,
                    ]);

                }
            } else {
                $data   =   AppOrgProduct::create([
                    'name' => $request->name,
                    'name_en' => $request->name_en,
                    'release_date' => $request->release_date,
                    'prod_category_id' => $request->prod_category_id,
                    'game_category' => $request->game_category,
                    'game_location' => $request->game_location,
                    'game_generation' => $request->game_generation,
                    'language' => implode(",", $request->language),
                    'media' => $request->media,
                    'platform' => $request->platform,
                    'region' => $request->region,
                    'ean_upc' => $request->ean_upc,
                    'volume' => $request->volume,
                    'weight' => $request->weight,
                    'large' => $request->large,
                    'width' => $request->width,
                    'high' => $request->high,
                    'price_used' => $request->price_used,
                    'price_new' => $request->price_new,
                    'price_solo' => $request->price_solo,
                    'comments' => $request->comments,
                    'image_path' => "",
                    'is_enabled' => 'Y',
                    ]);
    
                    if ($data->save()) {
                            $destinationPath = public_path('/images');
    
                            foreach ($request->file('image_path') as $e) {
                                $osi                = new AppOrgProductImage;
                                $osi->product_id = $data->id;
    
                                $nombre_imagen  = $data->game_generation . time() . $e->getClientOriginalName();
                                $osi->image_path    = $nombre_imagen;
                                $osi->created_by = \Auth::user()->id;
                                $upload_success = $e->move($destinationPath, $nombre_imagen);
                                $osi->save();
    
                            }
    
                        return redirect("11w5Cj9WDAjnzlg0/product")->with([
                            'flash_message' => 'Producto agregado Con Exito ',
                            'flash_class'   => 'alert-success',
                        ]);
    
                    } else {
    
                        return back()->with([
                            'flash_message'   => 'Ha ocurrido un error.',
                            'flash_class'     => 'alert-danger',
                            'flash_important' => true,
                        ]);
    
                    }
            }
        } elseif ($request->hasFile('image_path')) {
            $data   =   AppOrgProduct::create([
                'name' => $request->name,
                'name_en' => $request->name_en,
                'release_date' => $request->release_date,
                'prod_category_id' => $request->prod_category_id,
                'game_category' => $request->game_category,
                'game_location' => $request->game_location,
                'game_generation' => $request->game_generation,
                'language' => implode(",", $request->language),
                'media' => $request->media,
                'platform' => $request->platform,
                'region' => $request->region,
                'ean_upc' => $request->ean_upc,
                'volume' => $request->volume,
                'weight' => $request->weight,
                'large' => $request->large,
                'width' => $request->width,
                'high' => $request->high,
                'price_used' => $request->price_used,
                'price_new' => $request->price_new,
                'price_solo' => $request->price_solo,
                'comments' => $request->comments,
                'image_path' => "",
                'is_enabled' => 'Y',
                ]);

                if ($data->save()) {
                        $destinationPath = public_path('/images');

                        foreach ($request->file('image_path') as $e) {
                            $osi                = new AppOrgProductImage;
                            $osi->product_id = $data->id;

                            $nombre_imagen  = $data->game_generation . time() . $e->getClientOriginalName();
                            $osi->image_path    = $nombre_imagen;
                            $osi->created_by = \Auth::user()->id;
                            $upload_success = $e->move($destinationPath, $nombre_imagen);
                            $osi->save();

                        }

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto agregado Con Exito ',
                        'flash_class'   => 'alert-success',
                    ]);

                } else {

                    return back()->with([
                        'flash_message'   => 'Ha ocurrido un error.',
                        'flash_class'     => 'alert-danger',
                        'flash_important' => true,
                    ]);

                }
            }
        elseif ($request->has('box_language')) {
            $data   =   AppOrgProduct::create([
                'name' => $request->name,
                'name_en' => $request->name_en,
                'release_date' => $request->release_date,
                'prod_category_id' => $request->prod_category_id,
                'game_category' => $request->game_category,
                'game_location' => $request->game_location,
                'game_generation' => $request->game_generation,
                'language' => implode(",", $request->language),
                'box_language' => implode(",", $request->box_language),
                'media' => $request->media,
                'platform' => $request->platform,
                'region' => $request->region,
                'ean_upc' => $request->ean_upc,
                'volume' => $request->volume,
                'weight' => $request->weight,
                'large' => $request->large,
                'width' => $request->width,
                'high' => $request->high,
                'price_used' => $request->price_used,
                'price_new' => $request->price_new,
                'price_solo' => $request->price_solo,
                'comments' => $request->comments,
                'image_path' => "",
                'is_enabled' => 'Y',
                ]);
                if ($data->save()) {

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto agregado Con Exito ',
                        'flash_class'   => 'alert-success',
                    ]);

                } else {

                    return back()->with([
                        'flash_message'   => 'Ha ocurrido un error.',
                        'flash_class'     => 'alert-danger',
                        'flash_important' => true,
                    ]);

                }
        } else {
            $data   =   AppOrgProduct::create([
                'name' => $request->name,
                'name_en' => $request->name_en,
                'release_date' => $request->release_date,
                'prod_category_id' => $request->prod_category_id,
                'game_category' => $request->game_category,
                'game_location' => $request->game_location,
                'game_generation' => $request->game_generation,
                'language' => implode(",", $request->language),
                'media' => $request->media,
                'platform' => $request->platform,
                'region' => $request->region,
                'ean_upc' => $request->ean_upc,
                'volume' => $request->volume,
                'weight' => $request->weight,
                'large' => $request->large,
                'width' => $request->width,
                'high' => $request->high,
                'price_used' => $request->price_used,
                'price_new' => $request->price_new,
                'price_solo' => $request->price_solo,
                'comments' => $request->comments,
                'image_path' => 'No',
                'is_enabled' => 'Y',
                ]);

                if ($data->save()) {

                    return redirect("11w5Cj9WDAjnzlg0/product")->with([
                        'flash_message' => 'Producto agregado Con Exito ',
                        'flash_class'   => 'alert-success',
                    ]);

                } else {

                    return back()->with([
                        'flash_message'   => 'Ha ocurrido un error.',
                        'flash_class'     => 'alert-danger',
                        'flash_important' => true,
                    ]);

                }
        }


    }

    

    public function update(Request $request, $id)
    {

        $data = AppOrgProduct::findOrFail($id);
        $data->update($request->all());
        if ($request->has('box_language')) {
            $data->language = implode(",", $request->language);
            $data->box_language = implode(",", $request->box_language);
        } else {
            $data->language = implode(",", $request->language);
        }

        if ($data->save()) {
            
            return redirect('11w5Cj9WDAjnzlg0/product')->with([
                'flash_class'   => 'alert-success',
                'flash_message' => 'Producto Actualizado con exito.'
            ]);
        } else {

            return redirect('11w5Cj9WDAjnzlg0/product')->with([
                'flash_class'     => 'alert-danger',
                'flash_message'   => 'Ha ocurrido un error.',
                'flash_important' => true
            ]);
        }
    }

    
}
