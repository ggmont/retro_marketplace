<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Incidence;
use App\SysShipping;

class IncidenceController extends Controller
{

    public function index()
    {

        $incidence       = Incidence::all();

        return view('admin.incidence', [
            'incidence'     => $incidence,
        ]);
    }
    

    public function view($id)
    {

        $incidence = Incidence::findOrFail($id);
        $order_shipping = SysShipping::find($incidence->order->shipping_id);
        $details = $incidence->order->details()->get();
        //dd($incidence);

        return view('admin.incidence.show', [
            'incidence'     => $incidence,
            'order_shipping' => $order_shipping,
            'details' => $details,
        ]);
    }

}
