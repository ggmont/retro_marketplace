<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\AppOrgNewsItem;
use Redirect;
use Request;
use Validator;
use Config;
use File;


class NewsController extends Controller
{
    public function __construct()
    {
        $this->columnPrefix =  Config::get('brcode.column_prefix');
        $this->viewData['column_prefix'] = $this->columnPrefix;
    }

    public function index()
    {

        $news       = AppOrgNewsItem::all();

        return view('admin.news', [
            'news'     => $news,
        ]);
    }

    public function edit($id = 0)
    {
        $prueba = AppOrgNewsItem::findOrFail($id);

        $this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('org.news_item');
        $this->viewData['form_list_title'] = trans('app.news');
        $this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/marketplace/news');
        $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/news/save');
        $this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/marketplace/news/delete');
        $this->viewData['model_id'] = 0;

        if ($id > 0) {
            $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/news/modify');
            $this->viewData['model'] = AppOrgNewsItem::find($id);
            $this->viewData['model'] = $this->viewData['model']->toArray();
            $this->viewData['model_id'] = $id;
        }

        $this->viewData['model_cols'] = [
            'id' => ['type' => 'hidden', 'attr' => ' '],
            'title'                               =>  ['type' => 'input-text', 'attr' => ' maxlength=50 ', 'label' => trans('org.col_title')],
            'content'                         =>  ['type' => 'html-editor', 'class' => 'br-html-editor', 'attr' => ' rows="20" style="min-height: 150px;" ', 'label' => trans('org.col_content'),],
            'source_url'                           =>  ['type' => 'input-text', 'attr' => ' maxlength=50 ', 'label' => trans('org.col_source_url')],
            'image_path'                      =>    [
                'type' => 'file-uploader', 'file-type' => 'image', 'is_array' => false,
                'class' => 'image-uploader', 'attr' => 'data-url="' . url('11w5Cj9WDAjnzlg0/marketplace/news/upload_image') . '"',
                'img-class' => '', 'img-path' => 'uploads/news-images/'
            ],
            'is_enabled'              =>  ['type' => 'select', 'select_values' => [['id' => 'Y', 'value' => trans('app.yes')], ['id' => 'N', 'value' => trans('app.no')]], 'class' => 'br-select2'],
        ];

        return view('admin.news.edit', [
            "prueba" => $prueba,
        ])->with('viewData', $this->viewData);
    }

    public function updateNew($id = 0)
    {
        $colPrefix = $this->columnPrefix;

        $input = Request::all();
        //dd($input);

        $id = $input['model_id'] > 0 ? $input['model_id'] : 0;

        $id = saveModel($id, $model, 'App\\AppOrgNewsItem', $subModelExists, $input, $colPrefix, $new);

        $newsItemImage = isset($input[$colPrefix . 'image_path']) ? $input[$colPrefix . 'image_path'] : '';

        if (strlen($newsItemImage) > 0 && strpos($newsItemImage, 'news_item_image_') === false) {

            $ext = File::extension(public_path() . '/uploads/news-images/' . $newsItemImage);

            File::move(
                public_path() . '/uploads/news-images/' . $newsItemImage,
                public_path() . '/uploads/news-images/news_item_image_' . $model->id . '.' . $ext
            );

            $model->image_path = 'news_item_image_' . $model->id . '.' . $ext;
            $model->save();
        }

        return redirect('/11w5Cj9WDAjnzlg0/news')->with([
            'flash_message' => 'Actualizado correctamente.',
            'flash_class'   => 'alert-success',
        ]);
    }
}
