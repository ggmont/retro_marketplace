<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\SysUser;
use App\Services\FCMService;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class SendController extends Controller
{
    public function index()
    {

        $response = Http::post('https://beta.beseif.com/api/token', [
            'client_id' => 'RetroGamingMarket',
            'client_secret' => 'Bb4MCvBpjHSWxXJ4hktX4s3W2N4H9LeAX8vDdawjHNc07UIRy5vjBRglnLBZwFWBmw6j78UUnVEdAoXJ',
            'grant_type' => 'client_credentials',
        ]);
 
        
        $jsonData = $response->json();


        


        $response_two = Http::withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ])->withToken('a212435b89d1279235975e32b424c0755e60bca2')->post('https://beta.beseif.com/api/v3_1/transaccion', [
            "email_vendedor" => "vendedor@mail.com",
            "email_comprador" => "comprador@mail.com",
            "articulos" => "Cámara de fotos",
        ]);

        $jsonDatatwo = $response_two->json();

        //dd($jsonDatatwo);

        $body = [
            [
                "Url" => "StorageMgmt",
                "Method" => "PATCH",
                "Payload" => [
                    "Identities" => [
                        "userId" => "userid",
                        "companyId" => "company",
                        "ownerId" => "ownerId",
                    ],
                ],
            ],
            [
                "Url" => "Reports/List",
                "Method" => "GET",
            ],
        ];


        $miJsonEncode = json_encode($data);

        //dd($miJsonEncode);
       
          //dd($order->shipping_price);
       
          $response_two = Http::withHeaders([
           'Accept' => 'application/json',
           'Content-Type' => 'application/json'
         ])->withToken($jsonData['access_token'])->post('https://beta.beseif.com/api/v3_1/transaccion', [
                   "email_vendedor" => "vendedor@mail.com",
                   "email_comprador" => "comprador@mail.com",
                   'articulos' => [$miJsonEncode]
          ]);
       
           $jsonDatatwo = $response_two->json();
       
           dd($jsonDatatwo);
           
        
        

        $data = [
            "email_vendedor" => "vendedor@mail.com",
            "email_comprador" => "comprador@mail.com",
            'articulos' =>  [
                "nombre_articulo" => "Cámara de fotos",
                "precio_articulo" => 49.99,
                "url_anuncio" => "www.urlmarketplace.com/anuncio",
                "id_anuncio" => "1234A",
                "url_foto" => "www.marketplace.com/foto",
                "descripcion_articulo" => "Cámara réflex nueva",
                "categoria_articulo" => "Electrónica"
            ],
            "kg" => 3,
            "dimensiones" => "30x30x30",
            "url_response" => "www.marketplace.com/response",
            "url_tracking" => "www.marketplace.com/tracking",
            "direcciones_comprador" => [
                "direccion" => "Calle Prueba 1, 2ºC",
                "provincia" => "Madrid",
                "ciudad" => "Madrid",
                "cp" => 28005
            ],
            "contactos_comprador" => [
                "nombre" => "Juan",
                "apellidos" => "García",
                "telefono" => 612345678,
                "dni" => "12345678A"
            ]
        ];

        $response_three = Http::get('https://beta.beseif.com/marketplace/pre_checkout', [
            'codigo' => 'BRtzvHqSWOxteC',
            'webview' => 'true',
        ]);



        $jsonData = $response->json();

        //dd($response);

        //$jsonDatatwo = $response_two->json();

        //dd($response, $response_three);

        return view('admin.send', []);
    }

    public function pay(Request $request)
    {

        //dd($request->precio);


        //$response = Http::post('https://beta.beseif.com/api/v3_1/transaccion', $request->input());

        $response = Http::post('https://beta.beseif.com/api/token', [
            'client_id' => 'RetroGamingMarket',
            'client_secret' => 'Bb4MCvBpjHSWxXJ4hktX4s3W2N4H9LeAX8vDdawjHNc07UIRy5vjBRglnLBZwFWBmw6j78UUnVEdAoXJ',
            'grant_type' => 'client_credentials',
         ]);
      
      
         $jsonData = $response->json();
      
         //dd($jsonData);
      
         //dd($order->shipping_price);
      
         $response_two = Http::withHeaders([
          'Accept' => 'application/json',
          'Content-Type' => 'application/json'
        ])->withToken($jsonData['access_token'])->post('https://beta.beseif.com/api/v3_1/transaccion', [
            "email_vendedor" => "vendedor@mail.com",
            "email_comprador" => "comprador@mail.com",
            'articulos' =>  [
                "nombre_articulo" => "Cámara de fotos",
                "precio_articulo" => 0.00,
                "url_anuncio" => "www.urlmarketplace.com/anuncio",
                "id_anuncio" => "1234A",
                "url_foto" => "www.marketplace.com/foto",
                "descripcion_articulo" => "Cámara réflex nueva",
                "categoria_articulo" => "Electrónica"
            ],
            "kg" => 3,
            "dimensiones" => "30x30x30",
            "url_response" => "www.marketplace.com/response",
            "url_tracking" => "www.marketplace.com/tracking",
            "direcciones_comprador" => [
                "direccion" => "Calle Prueba 1, 2ºC",
                "provincia" => "Madrid",
                "ciudad" => "Madrid",
                "cp" => 28005
            ],
            "contactos_comprador" => [
                "nombre" => "Juan",
                "apellidos" => "García",
                "telefono" => 612345678,
                "dni" => "12345678A"
            ]
         ]);
         
         $jsonDatatwo = $response_two->json();

         dd($jsonDatatwo);
 

        //$response = Http::post('https://beta.beseif.com/api/v3_1/transaccion?token_type=Bearer&access_token=904b5f573235a2f9fafc80ff4767e161ae35320e', $request->input());

    }
}
