<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SysUser;
use App\Services\FCMService;
use Illuminate\Support\Facades\Auth;
use Kutia\Larafirebase\Facades\Larafirebase;
use App\Providers\RouteServiceProvider;
use App\Traits\Firebase;
use Kutia\Larafirebase\Messages\FirebaseMessage;
use Kawankoding\Fcm\Fcm;
use App\Notifications\SendPushNotification;


class WebNotificationController extends Controller
{

  use Firebase;

  public function index()
  {
    return view('admin.notification', []);
  }

  public function sendAll(Request $request) {
    $recipients = SysUser::where('user_name', 'adri')->whereNotNull('device_token')->pluck('device_token')->toArray();

    //dd($recipients);
    fcm()
    ->to($recipients) // $recipients must an array
    ->priority('high')
    ->timeToLive(0)
    ->notification([
        'title' => $request->input('title'),
        'body' => $request->input('body'),
    ])
    ->send();

    $notification = 'ESTO ES UNA PRUEBA';

    return back()->with(compact('notification'));
  }

  public function updateToken(Request $request){
    try{
        $request->user()->update(['fcm_token'=>$request->token]);
        return response()->json([
            'success'=>true
        ]);
    }catch(\Exception $e){
        report($e);
        return response()->json([
            'success'=>false
        ],500);
    }
 }

 public function notification(Request $request){
  $request->validate([
      'title'=>'required',
      'message'=>'required'
  ]);

  try{
      $fcmTokens = SysUser::whereNotNull('fcm_token')->pluck('fcm_token')->toArray();

      //dd($fcmTokens);

      //dd($fcmTokens);

      //Notification::send(null,new SendPushNotification($request->title,$request->message,$fcmTokens));

      /* or */

     // auth()->user()->notify(new SendPushNotification($request->title,$request->message,$fcmTokens));

      /* or */

      

      return (new FirebaseMessage)
      ->withTitle('prueba')
      ->withBody('probando')
      ->withImage('https://firebase.google.com/images/social.png')
      ->withIcon('https://seeklogo.com/images/F/firebase-logo-402F407EE0-seeklogo.com.png')
      ->withClickAction('https://www.google.com')
      ->withPriority('high')->asMessage($fcmTokens);


  }catch(\Exception $e){
      report($e);
      return redirect()->back()->with('error','Something goes wrong while sending notification.');
  }
}

  public function sendNotification()
  {
    $token = "";
    $notification = [
      'title' => 'title',
      'body' => 'body of message.',
      'icon' => 'myIcon',
      'sound' => 'mySound'
    ];
    $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];

    $fcmNotification = [
      //'registration_ids' => $tokenList, //multple token array
      'to'        => $token, //single token
      'notification' => $notification,
      'data' => $extraNotificationData
    ];

    return $this->firebaseNotification($fcmNotification);
  }

  public function updateDeviceToken(Request $request)
  {
    Auth::user()->fcm_token =  $request->token;

    Auth::user()->save();

    return response()->json(['Token successfully stored.']);
  }

  public function sendNotificationrToUser($id)
  {

    // get a user to get the fcm_token that already sent.               from mobile apps 
    $user = SysUser::findOrFail($id);

    //dd($user);

    FCMService::send(
      $user->fcm_token,
      [
        'title' => 'your title',
        'body' => 'your body',
      ]
    );

    return redirect("11w5Cj9WDAjnzlg0/notifications")->with([
      'flash_message' => 'Notificacion agregada Con Exito ',
      'flash_class'   => 'alert-success',
    ]);
  }

  public function test(Request $request)
  {
    $firebaseToken = SysUser::whereNotNull('fcm_token')->pluck('fcm_token')->all();
    $token = "ePNTh2-v1VE:APA91bEIZ-KGdC_SqUA3tSucKKTuK3u3jpGT7jpAjoT9t9PruxwRTsUVl3A3QQN1ectNZAkhSkCoo69WYYetaGWoA5uGRjkqXSiiwg1CV70TNQOp_bT4u-5gQXd90qF0n53L58bli3zW";
    $from = "AAAAkkZYRuY:APA91bHrRZhuu5XC1FzKfEJb_HMEx34bZVTz_oT6j4ZBBUTqYRm4xj6QR3PBAu7JqpBWQzSwNRr5vro84wGqefn0ni2kebFzauBtQpGQapHh3Y3H9JFw-XBeNMUiCpLsoSTL5ZdSmHsM";
    $msg = array(
      'body'  => "Testing Testing",
      'title' => "Hola, esto es una prueba",
      'receiver' => 'erw',
      'icon'  => "https://image.flaticon.com/icons/png/512/270/270014.png",/*Default Icon*/
      'sound' => 'mySound'/*Default sound*/
    );

    $fields = array(
      'registration_ids' =>$firebaseToken,
      'notification'  => $msg
    );

    //dd($fields);

    $headers = array(
      'Authorization: key=' . $from,
      'Content-Type: application/json'
    );

    //dd($headers);
    //#Send Reponse To FireBase Server 
    $ch = curl_init();

    //dd($ch);
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);

    dd($result);
  }



  function sendFCM()
  {

    $firebaseToken = SysUser::whereNotNull('fcm_token')->pluck('fcm_token')->all();
    // FCM API Url
    $url = 'https://fcm.googleapis.com/fcm/send';

    // Put your Server Key here
    $SERVER_API_KEY = env('FIREBASE_SERVER_KEY');

    // Compile headers in one variable
    $headers = array(
      'Authorization:key=' . $SERVER_API_KEY,
      'Content-Type:application/json'
    );

    // Add notification content to a variable for easy reference
    $notifData = [
      'title' => "Test Title",
      'body' => "Test notification body",
      //  "image": "url-to-image",//Optional
      'click_action' => "activities.NotifHandlerActivity" //Action/Activity - Optional
    ];

    $dataPayload = [
      'to' => 'My Name',
      'points' => 80,
      'other_data' => 'This is extra payload'
    ];

    // Create the api body
    $apiBody = [
      'notification' => $notifData,
      'data' => $dataPayload, //Optional
      'time_to_live' => 600, // optional - In Seconds
      //'to' => '/topics/mytargettopic'
      //'registration_ids' = ID ARRAY
      'to' => 'cc3y906oCS0:APA91bHhifJikCe-6q_5EXTdkAu57Oy1bqkSExZYkBvL6iKCq2hq3nrqKWymoxfTJRnzMSqiUkrWh4uuzzEt3yF5KZTV6tLQPOe9MCepimPDGTkrO8lyDy79O5sv046-etzqCGmKsKT4'
    ];

    // Initialize curl with the prepared headers and body
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($apiBody));

    // Execute call and save result
    $result = curl_exec($ch);
    print($result);
    // Close curl after call
    curl_close($ch);

    return $result;
  }
}
