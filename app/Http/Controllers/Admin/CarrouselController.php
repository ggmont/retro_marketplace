<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AppOrgCarouselItem;
use App\AppOrgCarousel;
use Auth;
use Intervention\Image\Facades\Image;


class CarrouselController extends Controller
{
    public function index()
    {

        $carrousel       = AppOrgCarouselItem::all();

        return view('admin.carrousel', [
            'carrousel'     => $carrousel,
        ]);
    }

    public function create()
    {
        $carrousel  = AppOrgCarouselItem::all();

        return view('admin.carrousel.create', ['carrousel' => $carrousel]);
    }

    public function store(Request $request)
    {
        $destinationPath = public_path('/uploads/carousels-images/');
        $osi                = new AppOrgCarouselItem;
        $secundary = $request->image_path;
        $osi->carousel_id  = 1;
        $osi->lang  = 'fr';
        $osi->content_title = $request->content_title;
        $osi->content_text = $request->content_text;
        $nombre_imagen  = 'carousel_image_english' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $secundary->getClientOriginalExtension();

        $osi->image_path    = $nombre_imagen;
        $osi->created_by = \Auth::user()->id;

        $upload_success = $secundary->move($destinationPath, $nombre_imagen);
        $osi->save();

        return redirect("11w5Cj9WDAjnzlg0/carrousel")->with([
            'flash_message' => 'Imagen agregada Con Exito ',
            'flash_class'   => 'alert-success',
        ]);
    }


    public function storeImageSecundaryOne(Request $request)
    {
        $destinationPath = public_path('/uploads/carousels-images/');
        $osi                = new AppOrgCarouselItem;
        $secundary = $request->image_path;
        $osi->carousel_id  = 3;
        $osi->lang  = 'fr';
        $osi->content_title = $request->content_title;
        $osi->content_text = $request->content_text;
        $nombre_imagen  = 'carousel_image_secundary_two' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $secundary->getClientOriginalExtension();

        $osi->image_path    = $nombre_imagen;
        $osi->created_by = \Auth::user()->id;

        $upload_success = $secundary->move($destinationPath, $nombre_imagen);
        $osi->save();

        return redirect("11w5Cj9WDAjnzlg0/product")->with([
            'flash_message' => 'Producto agregado Con Exito ',
            'flash_class'   => 'alert-success',
        ]);
    }

    public function edit($id)
    {
        $carrousel = AppOrgCarouselItem::findOrFail($id);

        return view(
            "admin.carrousel.edit",
            compact(
                'carrousel',
            )
        );
    }

    public function updateImage(Request $request, $id)
    {
        $carrousel = AppOrgCarouselItem::find($id);

        $carrousel_image_principal = $request->image_path;


        if ($carrousel_image_principal) {
            $filename  = 'carrousel_image_principal' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $carrousel_image_principal->getClientOriginalExtension();
            //dd($filename);
            $path = public_path('/uploads/carousels-images/' . $filename);

            Image::make($carrousel_image_principal->getRealPath())->resize(1098, 384)->save($path);
            $carrousel->image_path = $filename;
            $carrousel->save();
            $carrousel->update([
                'image_path' => $filename,
                'content_title' => $request->content_title,
                'content_text' => $request->content_text,
                'is_enabled' => $request->is_enabled,
                'link' => $request->link,
            ]);
            return redirect("11w5Cj9WDAjnzlg0/carrousel")->with([
                'flash_message' => 'Carrusel actualizado Con Exito ',
                'flash_class'   => 'alert-success',
            ]);
        } else {
            $carrousel->update([
                'content_title' => $request->content_title,
                'content_text' => $request->content_text,
                'is_enabled' => $request->is_enabled,
                'link' => $request->link,
            ]);
            return redirect("11w5Cj9WDAjnzlg0/carrousel")->with([
                'flash_message' => 'Carrusel actualizado Con Exito ',
                'flash_class'   => 'alert-success',
            ]);
        }
        //dd($carrousel_image_secundary_two); 


    }

    public function updateImageSecundaryOne(Request $request, $id)
    {
        $carrousel = AppOrgCarouselItem::find($id);
        $carrousel_image_secundary_one = $request->image_path;
        if ($carrousel_image_secundary_one) {
            //dd($carrousel_image_secundary_two);

            $filename  = 'carrousel_image_secundary_one' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $carrousel_image_secundary_one->getClientOriginalExtension();
            //dd($filename);
            $path = public_path('/uploads/carousels-images/' . $filename);

            Image::make($carrousel_image_secundary_one->getRealPath())->resize(540, 357)->save($path);
            $carrousel->image_path = $filename;
            $carrousel->save();
            $carrousel->update([
                'image_path' => $filename,
                'link' => $request->link,
            ]);
            return redirect("11w5Cj9WDAjnzlg0/carrousel")->with([
                'flash_message' => 'Imagen secundaria 1 actualizada Con Exito ',
                'flash_class'   => 'alert-success',
            ]);
        } else {
            $carrousel->update([
                'link' => $request->link,
            ]);
            return redirect("11w5Cj9WDAjnzlg0/carrousel")->with([
                'flash_message' => 'Carrusel actualizado Con Exito ',
                'flash_class'   => 'alert-success',
            ]);
        }
    }

    public function updateImageSecundaryTwo(Request $request, $id)
    {
        $carrousel = AppOrgCarouselItem::find($id);

        $carrousel_image_secundary_two = $request->image_path;

        if ($carrousel_image_secundary_two) {

            //dd($carrousel_image_secundary_two);

            $filename  = 'carousel_image_secundary_two' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $carrousel_image_secundary_two->getClientOriginalExtension();
            //dd($filename);
            $path = public_path('/uploads/carousels-images/' . $filename);

            Image::make($carrousel_image_secundary_two->getRealPath())->resize(540, 357)->save($path);
            $carrousel->image_path = $filename;
            $carrousel->save();
            $carrousel->update([
                'image_path' => $filename,
                'link' => $request->link,
            ]);
            return redirect("11w5Cj9WDAjnzlg0/carrousel")->with([
                'flash_message' => 'Imagen secundaria 2 actualizada Con Exito ',
                'flash_class'   => 'alert-success',
            ]);
        } else {
            $carrousel->update([
                'link' => $request->link,
            ]);
            return redirect("11w5Cj9WDAjnzlg0/carrousel")->with([
                'flash_message' => 'Carrusel actualizado Con Exito ',
                'flash_class'   => 'alert-success',
            ]);
        }
    }
}
