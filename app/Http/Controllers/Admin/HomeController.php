<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
use Request;
use App\AppOrgOrder;
use App\AppOrgUserInventory;
use App\AppOrgOrderDetail;
use App\ProductRequest;
use App\SysUser;
use App\SysUserComplaint;
use DB;

class HomeController extends Controller
{


	public function index()
	{

		$this->viewData['sys_complaint'] = SysUserComplaint::where('type', 1)->count();
		$this->viewData['sys_complaint_user'] = SysUserComplaint::where('type', 2)->count();
		$this->viewData['sys_complaint_inventory'] = SysUserComplaint::where('type', 3)->count();
		$this->viewData['user_inventory'] = AppOrgUserInventory::where('user_id', Auth::id())->count();
		$this->viewData['user_online'] = count(SysUser::where('is_online', 1)->get());
		$this->viewData['user_all'] = count(SysUser::all());
		$this->viewData['product_request'] = count(ProductRequest::all());
		$this->viewData['user_register'] = count(SysUser::whereMonth('created_at', '=', (date('m')))->get());
		$this->viewData['user_daily'] = count(SysUser::whereDate('created_at', '=', date('Y-m-d'))->get());
		$this->viewData['order_user'] = count(AppOrgOrder::whereNotIn('status', ['CP', 'CE', 'AC', 'RC', 'CS', 'TD', 'FD'])->whereMonth('created_at', '=', (date('m')))->get());
		$this->viewData['order_user_complete'] = count(AppOrgOrder::where('status', 'DD')->whereMonth('created_at', '=', (date('m')))->get());
		$this->viewData['product_user'] = count(AppOrgUserInventory::whereNull('deleted_at')->get());
		$this->viewData['product_user_qty'] = AppOrgUserInventory::whereNull('deleted_at')->sum('quantity');
		$this->viewData['purchase_all'] = count(AppOrgOrder::all());

		$this->viewData['resumen'] = AppOrgOrder::where('status', 'FD')
			->select(DB::raw('CONCAT(YEAR(created_at), MONTH(created_at)) AS dates'), DB::raw('MONTH(created_at) month'), DB::raw('YEAR(created_at) years'), "created_at")
			->groupBy('dates')
			->selectRaw('sum(total) as sum')
			->get();

		return view('admin.index')->with('viewData', $this->viewData);
	}



	public function showWithdrawal()
	{

		$withdrawal             = AppOrgOrder::where('status', 'CP')->orWhere('status', 'CE')->orWhere('status', 'AC')->orWhere('status', 'RC')->get();

		$withdrawal_pending     = AppOrgOrder::where('status', 'CP')->get();

		$fees                   = AppOrgOrder::where('status', 'FD')->orWhere('status', 'TD')->get();

		$purchases              = AppOrgOrder::where('status', 'CR')->orWhere('status', 'PD')->orWhere('status', 'DD')->orWhere('status', 'ST')->get();

		return view('admin.withdrawal', [
			'withdrawal'          => $withdrawal,
			'withdrawal_pending'  => $withdrawal_pending,
			'fees'                => $fees,
			'purchases'           => $purchases,
		]);
	}

	public function showCredit()
	{
		if (Request::ajax()) {
			$data = AppOrgOrder::where('status', 'CP')->orWhere('status', 'CE')->orWhere('status', 'AC')->orWhere('status', 'RC')->get();
			$datos = [];

			foreach ($data as $key) {
				$mapArray = (object) array(
					"d1" => $key->order_identification,
					"d0" => $key->status,
					"d2" => $key->buyer ? $key->buyer->concept : "",
					"d3" => date('d/m/Y  H:i', strtotime($key->created_at)),
					"d31" => $key->status == 'CE' ? date('d/m/Y  H:i', strtotime($key->updated_at)) :  '---',
					"d4" => number_format($key->total, 2),
					"d5" => $key->buyer ? $key->buyer->email : "",
					"d6" => $key->buyer ? $key->buyer->bank->beneficiary : "",
					"d7" => $key->buyer ? $key->buyer->bank->iban_code : "",
					"d8" => $key->buyer ? $key->buyer->bank->bic_code : "",
					"d9" => $key->paid_out,
				);
				array_push($datos, $mapArray);
			}
			return response()->json([
				'data' => $datos,
			]);
		}
	}

	public function showPed()
	{
		if (Request::ajax()) {
			$data = AppOrgOrder::where('status', 'CP')->get();
			$datos = [];
			foreach ($data as $key) {
				$mapArray = (object) array(
					"d1" => $key->order_identification,
					"d0" => $key->status,
					"d2" => $key->buyer ? $key->buyer->concept : "",
					"d3" => date('d/m/Y  H:i', strtotime($key->created_at)),
					"d31" => $key->status == 'CE' ? date('d/m/Y  H:i', strtotime($key->updated_at)) :  '---',
					"d4" => number_format($key->total, 2),
					"d5" => $key->buyer ? $key->buyer->email : "",
					"d6" => $key->buyer ? $key->buyer->bank->beneficiary : "",
					"d7" => $key->buyer ? $key->buyer->bank->iban_code : "",
					"d8" => $key->buyer ? $key->buyer->bank->bic_code : "",
					"d9" => $key->paid_out,
				);
				array_push($datos, $mapArray);
			}
			return response()->json([
				'data' => $datos,
			]);
		}
	}

	public function showFees()
	{
		if (Request::ajax()) {
			$data = AppOrgOrder::where('status', 'FD')->orWhere('status', 'TD')->get();
			$datos = [];
			foreach ($data as $key) {
				$mapArray = (object) array(
					"d1" => $key->order_identification,
					"d0" => $key->status,
					"d2" => $key->buyer ? $key->buyer->concept : "",
					"d3" => date('d/m/Y  H:i', strtotime($key->created_at)),
					"d31" => '---',
					"d4" => number_format($key->total, 2),
					"d5" => $key->buyer ? $key->buyer->email : "",
					"d6" => $key->buyer ? $key->buyer->bank->first()->beneficiary : "",
					"d7" => $key->buyer ? $key->buyer->bank->first()->iban_code : "",
					"d8" => $key->buyer ? $key->buyer->bank->first()->bic_code : "",
					"d9" => $key->paid_out,
				);
				array_push($datos, $mapArray);
			}
			return response()->json([
				'data' => $datos,
			]);
		}
	}

	public function showPur()
	{
		if (Request::ajax()) {
			$data = AppOrgOrder::where('status', 'CR')->orWhere('status', 'PD')->orWhere('status', 'DD')->orWhere('status', 'ST')->get();
			$datos = [];
			foreach ($data as $key) {
				$mapArray = (object) array(
					"d1" => $key->order_identification,
					"d0" => $key->status,
					"d2" => $key->buyer->concept,
					"d3" => date('d/m/Y  H:i', strtotime($key->created_at)),
					"d31" => '---',
					"d4" => number_format($key->total, 2),
					"d5" => $key->buyer->email,
					"d6" => $key->buyer->bank->first()->beneficiary,
					"d7" => $key->buyer->bank->first()->iban_code,
					"d8" => $key->buyer->bank->first()->bic_code,
					"d9" => $key->paid_out,
				);
				array_push($datos, $mapArray);
			}
			return response()->json([
				'data' => $datos,
			]);
		}
	}
}
