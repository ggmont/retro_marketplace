<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;

use Carbon\Carbon;
use Config;
use Datatables;
use DB;
use Hash;
use Image;
use Input;
use Mail;
use Redirect;
use Request;
use Response;
use Session;
use Storage;
use Validator;
use View;
use Awssat\Visits\Visits;

use Illuminate\Http\Request as FilterRequest;
use App\AppOrgOrderPayment;
use App\AppOrgOrder;
use App\Models\SysOrganization;
use App\SysPrivileges;
use App\Models\SysRolePrivileges;
use App\SysRoles;
use App\SysUser;
use App\SysUserRoles;
use App\SysUserComplaint;
use App\SysUserPasswordReset;
use App\SysUserAccountActivation;
use App\Events\ActionExecuted;
use App\MyModel;
use App\Mail\ActivateAccount;
use App\Mail\PasswordReset;
use App\Helpers\PtyCommons;
use App\AppOrgUserEvent;

use App\Mail\OrderPaymentComplete;
use App\Mail\OrderPaymentBuyer;
use App\Helpers;


class GestionController extends Controller
{
	public function __construct() {
		$this->columnPrefix =  Config::get('brcode.column_prefix');
		$this->viewData['column_prefix'] = $this->columnPrefix;
	}
	
	private function setInitialData() {
		$this->viewData['first_name'] = Auth::user()->first_name;
		$this->viewData['user_full_name'] = Auth::user()->first_name . ' ' . Auth::user()->last_name;
		$this->viewData['model_id'] = 0;
	}
	

    public function index() {

    	$privileges       = SysPrivileges::all();

        return view('admin.privileges', [
            'privileges'     => $privileges,
        ]);
    }

    public function rol_index() {

    	$roles       = SysRoles::all();

        return view('admin.roles', [
            'roles'     => $roles,
        ]);
    }

    public function user_index() {

    	$user       = SysUser::all();

        return view('admin.user', [
            'user'     => $user,
        ]);
    }


 

 

	public function visitor_index() {
		$users = SysUser::all();
		$totalVisitsDay = visits(SysUser::class)->period('day')->count(); // Contador general de visitas por día
		$totalVisitsYear = visits(SysUser::class)->period('year')->count(); // Contador general de visitas por año
		$totalVisitsMonth = visits(SysUser::class)->period('month')->count(); // Contador general de visitas por mes
	
		return view('admin.visitor', [
			'users' => $users,
			'totalVisitsDay' => $totalVisitsDay,
			'totalVisitsYear' => $totalVisitsYear,
			'totalVisitsMonth' => $totalVisitsMonth,
		]);
	}
	
	
	
	

	public function complaint_index() {

    	$complaint       = SysUserComplaint::all();

        return view('admin.complaint', [
            'complaint'     => $complaint,
        ]);
    }

    public function updateComplaint(Request $request, $id)
    {
        $banner = SysUserComplaint::find($id);

		$banner->update([
			'message' => 'aprobado'
		]);

		return redirect("11w5Cj9WDAjnzlg0/management/complaint")->with([
			'flash_message' => 'Denuncia aprobada con Exito ',
			'flash_class'   => 'alert-success',
		]);

    }

    public function edit($id = 0)
    {
        $user = SysUser::findOrFail($id);
		//dd($user);
        $rolesModel = SysRoles::all();
		$rol = SysUserRoles::where("user_id", $id)->get();

		$this->setInitialData();

		$this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('app.user');
		$this->viewData['form_list_title'] = trans('app.roles');
		$this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/users');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/users/save');
		$this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/users/delete');
		$this->viewData['sub_model_title'] = trans('app.roles');
		$this->viewData['sub_model_view'] = 'list';

		if($id > 0) {
			$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/management/modify');
			$this->viewData['model'] = SysUser::find($id);
			$this->viewData['sub_model'] = $this->viewData['model']->roles->toArray();
			$this->viewData['model'] = $this->viewData['model']->toArray();
			$this->viewData['model_id'] = $id;

		}

		$this->viewData['model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
													'first_name' 						=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
													'last_name' 						=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
													'email' 								=>  ['type' => 'input-email', 'attr' => ' maxlength=50 '],
													'user_name' 						=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
													'company_id' 						=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
													'password' 							=>  ['type' => 'input-password', 'attr' => ' maxlength=50 '],
													'password_confirmation' =>  ['type' => 'input-password', 'attr' => ' maxlength=50 '],
													'company_id' 						=>  ['type' => 'text', 'attr' => ' maxlength=50 '],
													'is_enabled' 						=>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ] ],
													'is_activated' 					=>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ] ],
													];

		$rolesModel = SysRoles::all();
		$roles = [];

		foreach($rolesModel as $key => $row) {
			$roles[$key] = ['id' => $row->id, 'value' => $row->name];
		}

		$this->viewData['sub_model_list'] = [ 'role_id', $roles ];
		//dd($rol);
        return view("admin.user.edit", [
            "user" => $user,
            "rolesModel" => $rolesModel,
			"rol" => $rol,
        ])->with('viewData',$this->viewData);
    }

	public function updateUser()
    {
		$colPrefix = $this->columnPrefix;
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		$rules = [
					];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::except([$colPrefix.'password',$colPrefix.'password_confirmation']));
		}
		else {

			$new = true;

			if(strlen($input[$colPrefix.'password']) > 0) {
				$input[$colPrefix.'password'] = Hash::make($input[$colPrefix.'password']);
			}
			else {
				unset($input[$colPrefix.'password']);
			}

			unset($input[$colPrefix.'password_confirmation']);

			$id = saveModel($id, $model, 'App\\SysUser',$subModelExists, $input, $colPrefix, $new);

			if($subModelExists) {

				saveSubModel($new, $id, 'user_id', 'App\\SysUser', 'roles', 'role_id', 'App\\SysUserRoles', $colPrefix, $input);

			}

			$message = trans('app.user') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/11w5Cj9WDAjnzlg0/management/users')->with([
                'flash_message' => 'Actualizado correctamente.',
                'flash_class'   => 'alert-success',
            ]);

		}
    }

	public function historial($id)
    {
        $user = SysUser::findOrFail($id);
		dd($user);
    }

    public function filtroMes(Request $request)
    {
		
		//dd(Request::all());
        $from = date('Y-m-d', strtotime(str_replace('/', '-', Request::input('desde'))));
 
        $to   = date('Y-m-d', strtotime(str_replace('/', '-', Request::input('hasta'))));
//dd($from,$to);
        $mes = Request::input('mes');

        if (Request::input('mes') == null) {
            $user = SysUser::whereBetween("created_at", [$from, $to])->get();

            //dd($user);
            return view('admin.user.date', compact('user','from','to'))->with([
                'flash_class'   => 'alert-success',
                'flash_message' => 'Se encontraron registros.',
            ]);
        } else {

            $user = SysUser::whereMonth('created_at', $mes)->get();
             //$this->viewData['users'] = $users;
             //dd($user);
            return view('admin.user.date', compact('user','mes'))->with([
                'flash_class'   => 'alert-success',
                'flash_message' => 'Se encontraron registros.',
            ]);
        }

        if ($user->count() > 0) {
            set_time_limit(0);
            ini_set("memory_limit", -1);
            ini_set('max_execution_time', 0);

            return view('admin.user', compact('user'))->with([
                'flash_class'   => 'alert-success',
                'flash_message' => 'Se encontraron registros.',
            ]);
        } else {
            return back()->with([
                'flash_class'     => 'alert-danger',
                'flash_message'   => 'No hay registros en el mes indicado.',
                'flash_important' => true,
            ]);
        }

    }

	function getUserIP() {
		if( array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
			if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')>0) {
				$addr = explode(",",$_SERVER['HTTP_X_FORWARDED_FOR']);
				return trim($addr[0]);
			} else {
				return $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
		}
		else {
			return $_SERVER['REMOTE_ADDR'];
		}
	}

	public function minusCreditUser(Request $request) {

		$input = Request::all();
		//dd($input);

		
		#$id = $input['model_id'] > 0 ? $input['model_id'] : 0;
		$id = $input['orderDetails'];
		
		$credit = $input['credit'];
		$transaction = $input['transaction'];
		$pass = $input['pass'];
		$createdOn = date('Y-m-d H:i:s');

			if($transaction == "1") {
				$user = SysUser::find($id);
				//dd($user);
				$user->cash = $user->cash + floatval($credit);
				//dd($user->cash);
				
				$message = trans('app.user') . ' ' . trans('app.addCredit');

				$order = new AppOrgOrder;
				$order->buyer_user_id = $user->id;
				$order->seller_user_id = 1;
				$order->created_on = $createdOn;
				$order->max_pay_out = $createdOn;
				$order->status = 'AC';
				$order->total = $credit;
				$order->tax = 0;
				$order->quantity = 1;
				$order->order_identification = Auth::id() . 'C' . getRandomToken(15);
				$order->paid_out = 'Y';
				$order->instructions = $order->order_identification;
				$order->save();

				foreach(AppOrgOrder::where('buyer_user_id', $user->id)->where('status', 'RP')->get() as $key){
					if($key->total <= $user->cash){
						$user->cash = $user->cash - floatval($key->total);
						$key->status = 'CR';
						//dd($key->status);
						$key->paid_out = 'Y';
						$key->seller_read = 'N';
						$key->buyer_read = 'N';
						$key->max_pay_out = date('Y-m-d H:i:s', strtotime('+14 days'));
						$key->save();

						try {
							$payment = json_encode(array('date' => date('Y-m-d H:i:s'), 'order_id' => $key->order_identification, 'total' => $key->total, 'cantidad' => $key->quantity, 'type_payment' => 'Cash'));
							$orderPayment = new AppOrgOrderPayment([
								'payment_result' => $payment
							]);
							$key->payment()->save($orderPayment);
						} catch (Exception $ex) {
							$error = true;
							$this->loadCart();
						}
						
						PtyCommons::setUserEvent($user->id, 'Debito pedidos pendientes por pagar, al transferir saldo. Pago saldo personal en cuenta RGM el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP(), 'order_id' => $key->order_identification]);
						//OrderPaymentComplete
						Mail::to($key->seller->email)->queue(new OrderPaymentComplete(Auth::user(), $key));
						Mail::to($key->buyer->email)->queue(new OrderPaymentBuyer(Auth::user(), $key));
						return redirect('11w5Cj9WDAjnzlg0/management/users')->with([
							'flash_class'   => 'alert-warning',
							'flash_message' => 'Saldo Agregado correctamente, el usuario tenia productos pendientes por pagar.',
						]);
					}

				}
				return redirect('11w5Cj9WDAjnzlg0/management/users')->with([
					'flash_class'   => 'alert-success',
					'flash_message' => 'Saldo Agregado correctamente.',
				]);
			}

			if($transaction == "0") {
				$user = SysUser::find($id);
				if($user->cash < (float) $credit){
					$message = trans('app.user') . ' ' . trans('app.outCredit');
					return redirect('11w5Cj9WDAjnzlg0/management/users')->with('message',$message)->with('messageType','success');
				}
				
				$user->cash = $user->cash - floatval($credit);
				//dd($user->cash);

				$order = new AppOrgOrder;
				$order->buyer_user_id = $user->id;
				$order->seller_user_id = 1;
				$order->created_on = $createdOn;
				$order->max_pay_out = $createdOn;
				$order->status = 'RC';
				$order->total = $credit;
				$order->tax = 0;
				$order->quantity = 1;
				$order->order_identification = Auth::id() . 'C' . getRandomToken(15);
				$order->paid_out = 'Y';
				$order->instructions = $order->order_identification;
				$order->save();
	
				$message = trans('app.user') . ' ' . trans('app.removeCredit');
	
				return redirect('11w5Cj9WDAjnzlg0/management/users')->with('message',$message)->with('messageType','success');
			}
 
		
		$message = trans('app.errorPass');
	
		return redirect('11w5Cj9WDAjnzlg0/management/users')->with('message',$message)->with('messageType','success');


	}


	public function storeCreditUser(Request $request) {

		$prueba = AppOrgOrder::where('status','RP')->get();
		//dd($prueba);

		$input = Request::all();


		
		#$id = $input['model_id'] > 0 ? $input['model_id'] : 0;
		$id = $input['orderDetails'];

		//dd($id);
		
		$credit = $input['credit'];
		$transaction = $input['transaction'];
		$pass = $input['pass'];
		$createdOn = date('Y-m-d H:i:s');
		$type = $input['type'];

		if($type == "Promocion") {
			
			$user = SysUser::find($id);
			//dd($user);
			$user->cash = $user->cash + floatval($credit);
			//dd($user->cash);
			
			$message = trans('app.user') . ' ' . trans('app.addCredit');

			$order = new AppOrgOrder;
			$order->buyer_user_id = $user->id;
			$order->seller_user_id = 1;
			$order->created_on = $createdOn;
			$order->max_pay_out = $createdOn;
			$order->status = 'PV';
			$order->total = $credit;
			$order->tax = 0;
			$order->quantity = 1;
			$order->order_identification = Auth::id() . 'C' . getRandomToken(15);
			$order->paid_out = 'Y';
			$order->instructions = $order->order_identification;
			$order->save();

			foreach(AppOrgOrder::where('buyer_user_id', $user->id)->where('status', 'RP')->get() as $key){
				if($key->total <= $user->cash){
					$user->cash = $user->cash - floatval($key->total);
					$key->status = 'CR';
					//dd($key->status);
					$key->paid_out = 'Y';
					$key->seller_read = 'N';
					$key->buyer_read = 'N';
					$key->max_pay_out = date('Y-m-d H:i:s', strtotime('+14 days'));
					$key->save();

					try {
						$payment = json_encode(array('date' => date('Y-m-d H:i:s'), 'order_id' => $key->order_identification, 'total' => $key->total, 'cantidad' => $key->quantity, 'type_payment' => 'Cash'));
						$orderPayment = new AppOrgOrderPayment([
							'payment_result' => $payment
						]);
						$key->payment()->save($orderPayment);
					} catch (Exception $ex) {
						$error = true;
						$this->loadCart();
					}
					
					PtyCommons::setUserEvent($user->id, 'Debito pedidos pendientes por pagar, al transferir saldo. Pago saldo personal en cuenta RGM el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP(), 'order_id' => $key->order_identification]);
					//OrderPaymentComplete
					Mail::to($key->seller->email)->queue(new OrderPaymentComplete(Auth::user(), $key));
					Mail::to($key->buyer->email)->queue(new OrderPaymentBuyer(Auth::user(), $key));
					return redirect('11w5Cj9WDAjnzlg0/management/users')->with([
						'flash_class'   => 'alert-warning',
						'flash_message' => 'Saldo De Promocion Agregado correctamente, el usuario tenia productos pendientes por pagar.',
					]);
				}

			}
			return redirect('11w5Cj9WDAjnzlg0/management/users')->with([
				'flash_class'   => 'alert-success',
				'flash_message' => 'Saldo de Promocion Agregado correctamente.',
			]);
		}
		 

			if($transaction == "1") {


				$user = SysUser::find($id);
				$nintendo = $user->id;
				//dd($nintendo);
				$month = count(AppOrgOrder::where('buyer_user_id', $nintendo)->whereIn('status', ['CR', 'PC'])->whereMonth('created_at', '=', (date('m')) )->get());
				//dd($month);



				//dd($user);
				$user->cash = $user->cash + floatval($credit);
				//dd($user->cash);
				
				$message = trans('app.user') . ' ' . trans('app.addCredit');

				$order = new AppOrgOrder;
				$order->buyer_user_id = $user->id;
				$order->seller_user_id = 1;
				$order->created_on = $createdOn;
				$order->max_pay_out = $createdOn;
				$order->status = 'AC';
				$order->total = $credit;
				$order->tax = 0;
				$order->quantity = 1;
				$order->order_identification = Auth::id() . 'C' . getRandomToken(15);
				$order->paid_out = 'Y';
				$order->instructions = $order->order_identification;
				$order->save();

				//$prueba = AppOrgOrder::where('buyer_user_id', $id)->where('status','RP')->get();
				//dd($prueba);

				$prueba = AppOrgOrder::where('buyer_user_id', $id)->where('status','RP')->get();
				//dd($prueba);
				$i = 0; //Initialize variable
				foreach($prueba as $item) {
					$i++;
					$item;
					if($item->total <= $user->cash){
						$item->status = 'CR';
						//dd($item->seller->email);
						$item->paid_out = 'Y';
						$item->seller_read = 'N';
						$item->buyer_read = 'N';
						$item->max_pay_out = date('Y-m-d H:i:s', strtotime('+14 days'));
						$user->cash = $user->cash - floatval($item->total);
						$item->save();

						try {
							$payment = json_encode(array('date' => date('Y-m-d H:i:s'), 'order_id' => $item->order_identification, 'total' => $item->total, 'cantidad' => $item->quantity, 'type_payment' => 'Cash'));
							$orderPayment = new AppOrgOrderPayment([
								'payment_result' => $payment
							]);
							$item->payment()->save($orderPayment);
						} catch (Exception $ex) {
							$error = true;
							$this->loadCart();
						}

						PtyCommons::setUserEvent($user->id, 'Debito pedidos pendientes por pagar, al transferir saldo. Pago saldo personal en cuenta RGM el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP(), 'order_id' => $item->order_identification]);
						//OrderPaymentComplete
						Mail::to($item->seller->email)->queue(new OrderPaymentComplete(Auth::user(), $item));
						Mail::to($item->buyer->email)->queue(new OrderPaymentBuyer(Auth::user(), $item));
						//dd('prueba');
					}

					//dd($special_promotion);
				  }

				  if($i > 0){
					return redirect('11w5Cj9WDAjnzlg0/management/users')->with([
						'flash_class'   => 'alert-warning',
						'flash_message' => 'Saldo Agregado correctamente, el usuario tenia productos pendientes por pagar.',
					]);
				  } else {
					return redirect('11w5Cj9WDAjnzlg0/management/users')->with([
						'flash_class'   => 'alert-success',
						'flash_message' => 'Saldo Agregado correctamente',
					]);
				  }
			}

			if($transaction == "0") {
				$user = SysUser::find($id);
				if($user->cash < (float) $credit){
					$message = trans('app.user') . ' ' . trans('app.outCredit');
					return redirect('11w5Cj9WDAjnzlg0/management/users')->with('message',$message)->with('messageType','success');
				}
				
				$user->cash = $user->cash - floatval($credit);
				//dd($user->cash);

				$order = new AppOrgOrder;
				$order->buyer_user_id = $user->id;
				$order->seller_user_id = 1;
				$order->created_on = $createdOn;
				$order->max_pay_out = $createdOn;
				$order->status = 'RC';
				$order->total = $credit;
				$order->tax = 0;
				$order->quantity = 1;
				$order->order_identification = Auth::id() . 'C' . getRandomToken(15);
				$order->paid_out = 'Y';
				$order->instructions = $order->order_identification;
				$order->save();
	
				$message = trans('app.user') . ' ' . trans('app.removeCredit');
	
				return redirect('11w5Cj9WDAjnzlg0/management/users')->with('message',$message)->with('messageType','success');
			}
 
		
		$message = trans('app.errorPass');
	
		return redirect('11w5Cj9WDAjnzlg0/management/users')->with('message',$message)->with('messageType','success');


	}

	public function storeCreditUserSpecial(Request $request) {

		$input = Request::all();
		//dd($input);

		
		#$id = $input['model_id'] > 0 ? $input['model_id'] : 0;
		$id = $input['orderDetails'];
		$credit = $input['credit'];
		$transaction = $input['transaction'];
		$pass = $input['pass'];
		$createdOn = date('Y-m-d H:i:s');

			if($transaction == "1") {
				$user = SysUser::find($id);
				//dd($user);
				$user->special_cash = $user->special_cash + floatval($credit);
				//dd($user->special_cash);
				
				$message = trans('app.user') . ' ' . trans('app.addCredit');

				$order = new AppOrgOrder;
				$order->buyer_user_id = $user->id;
				$order->seller_user_id = 1;
				$order->created_on = $createdOn;
				$order->max_pay_out = $createdOn;
				$order->status = 'SC';
				$order->total = $credit;
				$order->tax = 0;
				$order->quantity = 1;
				$order->order_identification = Auth::id() . 'C' . getRandomToken(15);
				$order->paid_out = 'Y';
				$order->instructions = $order->order_identification;
				$order->save();



				$prueba = AppOrgOrder::where('buyer_user_id', $id)->where('status','RP')->get();
				//dd($prueba);
				$i = 0; //Initialize variable
				foreach($prueba as $item) {
					$i++;
					$item;
					if($item->total <= $user->special_cash){
						$item->status = 'CR';
						//dd($item->seller->email);
						$item->paid_out = 'Y';
						$item->seller_read = 'N';
						$item->buyer_read = 'N';
						$item->max_pay_out = date('Y-m-d H:i:s', strtotime('+14 days'));
						$user->special_cash = $user->special_cash - floatval($item->total);
						$item->save();

						try {
							$payment = json_encode(array('date' => date('Y-m-d H:i:s'), 'order_id' => $item->order_identification, 'total' => $item->total, 'cantidad' => $item->quantity, 'type_payment' => 'Cash'));
							$orderPayment = new AppOrgOrderPayment([
								'payment_result' => $payment
							]);
							$item->payment()->save($orderPayment);
						} catch (Exception $ex) {
							$error = true;
							$this->loadCart();
						}

						PtyCommons::setUserEvent($user->id, 'Debito pedidos pendientes por pagar, al transferir saldo. Pago saldo personal en cuenta RGM el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP(), 'order_id' => $item->order_identification]);
						//OrderPaymentComplete
						Mail::to($item->seller->email)->queue(new OrderPaymentComplete(Auth::user(), $item));
						Mail::to($item->buyer->email)->queue(new OrderPaymentBuyer(Auth::user(), $item));
						//dd('prueba');
					}

					//dd($special_promotion);
				  }

				  if($i > 0){
					return redirect('11w5Cj9WDAjnzlg0/management/users')->with([
						'flash_class'   => 'alert-warning',
						'flash_message' => 'Saldo Especial agregado correctamente, el usuario tenia productos pendientes por pagar.',
					]);
				  } else {
					return redirect('11w5Cj9WDAjnzlg0/management/users')->with([
						'flash_class'   => 'alert-success',
						'flash_message' => 'Saldo Especial agregado correctamente',
					]);
				  }
			}

			if($transaction == "0") {
				$user = SysUser::find($id);
				//dd($user);
				if($user->special_cash < (float) $credit){
					$message = trans('app.user') . ' ' . trans('app.outCredit');
					return redirect('11w5Cj9WDAjnzlg0/management/users')->with('message',$message)->with('messageType','success');
				}
				
				$user->special_cash = $user->special_cash - floatval($credit);
				

				$order = new AppOrgOrder;
				$order->buyer_user_id = $user->id;
				$order->seller_user_id = 1;
				$order->created_on = $createdOn;
				$order->max_pay_out = $createdOn;
				$order->status = 'SC';
				$order->total = $credit;
				$order->tax = 0;
				$order->quantity = 1;
				$order->order_identification = Auth::id() . 'C' . getRandomToken(15);
				$order->paid_out = 'Y';
				$order->instructions = $order->order_identification;
				$order->save();
	
				$message = trans('app.user') . ' ' . trans('app.removeCredit');
	
				return redirect('11w5Cj9WDAjnzlg0/management/users')->with('message',$message)->with('messageType','success');
			}
 
		
		$message = trans('app.errorPass');
	
		return redirect('11w5Cj9WDAjnzlg0/management/users')->with('message',$message)->with('messageType','success');


	}


}
