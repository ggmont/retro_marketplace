<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\ProductRequest;
use Livewire\WithPagination;

class ProductRequestTable extends Component
{
    use WithPagination;

    public $accion = "store";
	public $perPage = '20';

    public $search_user = '';

    public function updatingSearchuser()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.admin.product-request-table', [
            'product_request'     => ProductRequest::orderBy('id', 'DESC')
            ->paginate($this->perPage)
        ]);
    }
}
