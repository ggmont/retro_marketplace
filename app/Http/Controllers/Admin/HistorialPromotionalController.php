<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\AppOrgPromotionalCode;
use App\AppOrgHistorialPromotionalCode;
use App\SysShipping;
use Illuminate\Http\Request;

class HistorialPromotionalController extends Controller
{
    public function index()
    {

        $historial_promotional       = AppOrgHistorialPromotionalCode::all();

        //dd($historial_promotional);

        return view('admin.historial_promotional.index', [
            'historial_promotional'     => $historial_promotional,
        ]);
    }

    public function show($id)
    {

        $historial_promotional = AppOrgHistorialPromotionalCode::findOrFail($id);
        $order_shipping = SysShipping::find($historial_promotional->order->shipping_id);
        $details = $historial_promotional->order->details()->get();
        //dd($details);

        return view('admin.historial_promotional.show', [
            'historial_promotional'     => $historial_promotional,
            'order_shipping' => $order_shipping,
            'details' => $details,
        ]);
    }
}
