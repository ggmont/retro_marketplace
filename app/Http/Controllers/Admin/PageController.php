<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\AppOrgPage;
use App\SysDictionary;
use Config;
use Redirect;
use Request;
use Validator;


class PageController extends Controller
{
    public function __construct()
    {
        $this->columnPrefix =  Config::get('brcode.column_prefix');
        $this->viewData['column_prefix'] = $this->columnPrefix;
    }



    public function index()
    {

        $pagina       = AppOrgPage::all();

        return view('admin.page', [
            'pagina'     => $pagina,
        ]);
    }

    public function create($id = 0)
    {

        $this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('org.page');
        $this->viewData['form_list_title'] = trans('org.pages');
        $this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/marketplace/pages');
        $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/pages/save');
        $this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/marketplace/pages/delete');
        $this->viewData['model_id'] = 0;

        if ($id > 0) {

            $this->viewData['model'] = AppOrgPage::find($id);
            $this->viewData['model'] = $this->viewData['model']->toArray();
            $this->viewData['model_id'] = $id;
            $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/pages/modify');
        }

        $listModel = SysDictionary::whereIn('code', ['PAGE_CATEGORY'])->orderBy('code')->orderBy('value')->get();
        $listCategory   = [];

        foreach ($listModel as $key => $row) {
            $listCategory[$key] = ['id' => $row->value_id, 'value' => $row->value];
        }

        $this->viewData['model_cols'] = [
            'id' => ['type' => 'hidden', 'attr' => ' '],
            'title'                               =>  ['type' => 'input-text', 'attr' => ' maxlength=50 ', 'label' => trans('org.col_title')],
            'url'                             =>  ['type' => 'input-slug', 'label' => trans('org.col_url'), 'attr' => ' data-url-from="' . $this->columnPrefix . 'title" '],
            'parent_id'                         =>  ['type' => 'select',  'select_values' => $listCategory, 'attr' => ' maxlength=50 ', 'class' => 'br-select2'],
            'content'                 =>  ['type' => 'html-editor', 'label' => trans('org.col_content'), 'class' => 'br-html-editor', 'attr' => ' rows="10" '],
            'show_title'                           =>  ['type' => 'select', 'label' => trans('org.col_show_title'), 'select_values' => [['id' => 'Y', 'value' => trans('app.yes')], ['id' => 'N', 'value' => trans('app.no')]], 'class' => 'br-select2'],
            'is_enabled'              =>  ['type' => 'select', 'select_values' => [['id' => 'Y', 'value' => trans('app.yes')], ['id' => 'N', 'value' => trans('app.no')]], 'class' => 'br-select2'],
        ];

        return view('admin.page.create', [])->with('viewData', $this->viewData);
    }

    public function savePage($id = 0)
    {
        $colPrefix = $this->columnPrefix;

        $input = Request::all();

        $id = $input['model_id'] > 0 ? $input['model_id'] : 0;


        $id = saveModel($id, $model, 'App\\AppOrgPage', $subModelExists, $input, $colPrefix, $new);

        return redirect('/11w5Cj9WDAjnzlg0/pages')->with([
            'flash_message' => 'Agregado correctamente.',
            'flash_class'   => 'alert-success',
        ]);
    }



    public function edit($id = 0)
    {
        $prueba = AppOrgPage::findOrFail($id);

        $this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('org.page');
        $this->viewData['form_list_title'] = trans('org.pages');
        $this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/marketplace/pages');
        $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/pages/save');
        $this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/marketplace/pages/delete');
        $this->viewData['model_id'] = 0;

        if ($id > 0) {

            $this->viewData['model'] = AppOrgPage::find($id);
            $this->viewData['model'] = $this->viewData['model']->toArray();
            $this->viewData['model_id'] = $id;
            $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/pages/modify');
        }

        $listModel = SysDictionary::whereIn('code', ['PAGE_CATEGORY'])->orderBy('code')->orderBy('value')->get();
        $listCategory   = [];

        foreach ($listModel as $key => $row) {
            $listCategory[$key] = ['id' => $row->value_id, 'value' => $row->value];
        }

        $this->viewData['model_cols'] = [
            'id' => ['type' => 'hidden', 'attr' => ' '],
            'title'                               =>  ['type' => 'input-text', 'attr' => ' maxlength=50 ', 'label' => trans('org.col_title')],
            'url'                             =>  ['type' => 'input-slug', 'label' => trans('org.col_url'), 'attr' => ' data-url-from="' . $this->columnPrefix . 'title" '],
            'parent_id'                         =>  ['type' => 'select',  'select_values' => $listCategory, 'attr' => ' maxlength=50 ', 'class' => 'br-select2'],
            'content'                 =>  ['type' => 'html-editor', 'label' => trans('org.col_content'), 'class' => 'br-html-editor', 'attr' => ' rows="10" '],
            'show_title'                           =>  ['type' => 'select', 'label' => trans('org.col_show_title'), 'select_values' => [['id' => 'Y', 'value' => trans('app.yes')], ['id' => 'N', 'value' => trans('app.no')]], 'class' => 'br-select2'],
            'is_enabled'              =>  ['type' => 'select', 'select_values' => [['id' => 'Y', 'value' => trans('app.yes')], ['id' => 'N', 'value' => trans('app.no')]], 'class' => 'br-select2'],
        ];

        return view('admin.page.edit', [
            "prueba" => $prueba,
        ])->with('viewData', $this->viewData);
    }

    public function updatePage($id = 0)
    {
        $colPrefix = $this->columnPrefix;

        $input = Request::all();

        $id = $input['model_id'] > 0 ? $input['model_id'] : 0;


        $id = saveModel($id, $model, 'App\\AppOrgPage', $subModelExists, $input, $colPrefix, $new);

        return redirect('/11w5Cj9WDAjnzlg0/pages')->with([
            'flash_message' => 'Actualizado correctamente.',
            'flash_class'   => 'alert-success',
        ]);
    }
}
