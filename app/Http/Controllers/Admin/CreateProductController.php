<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
 
use App\AppOrgProduct;

class CreateProductController extends Controller
{
    public function creacion()
    {
        $inventory  = AppOrgProduct::all();
        dd($inventory);

        return view('admin.product.create', ['inventory' => $inventory]);
    }
}
