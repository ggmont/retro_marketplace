<?php

namespace App\Http\Controllers;

use Log;
use Illuminate\Http\Request;
use App\Http\Requests;

class WebhookController extends Controller
{
    public function beseprueba(Request $request)
    {
        Log::info($request);

        return redirect('/');
    }

}
