<?php namespace App\Http\Controllers;

use Auth;
use Cookie;
use Request;

use App\Events\ActionExecuted;

class UtilController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {

	}

	public function changeSideBar() {
		$collapse = Request::get('sidebar_collapse');
		$cookie = Cookie::forever('sidebar_collapse', $collapse);
		Cookie::queue($cookie);
	}
	
	
}
