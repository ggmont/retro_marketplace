<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SendinBlue\Client\Configuration;
use SendinBlue\Client\Api\ContactsApi;
use SendinBlue\Client\Model\CreateContact;
use App\SysUser;

class SendinBlueController extends Controller
{
    protected $sendinblueConfig;
    protected $contactsApi;

    public function __construct()
    {
        $this->sendinblueConfig = Configuration::getDefaultConfiguration()->setApiKey('api-key', env('SENDINBLUE_API_KEY'));
        $this->contactsApi = new ContactsApi(null, $this->sendinblueConfig);
    }

    public function importContacts()
    {
        // Obtener todos los usuarios registrados en la tabla SysUser
        $users = SysUser::all();
    
        foreach ($users as $user) {
            // Crear un objeto CreateContact con la información del usuario
            $contact = new CreateContact([
                'email' => $user->email,
                'attributes' => [
                    'FIRSTNAME' => $user->first_name,
                    'LASTNAME' => $user->last_name,
                    'PHONE' => $user->phone,
                ],
            ]);
    
            // Agregar el contacto a la lista de SendinBlue
            $this->contactsApi->createContact($contact);
        }
    
        return "Importación de contactos completada.";
    }
    

    public function addContactToList($email, $listId)
    {
        $createContact = new CreateContact();
        $createContact['email'] = $email;

        $lists = [$listId];
        $createContact['listIds'] = $lists;

        try {
            $this->contactsApi->createContact($createContact);
        } catch (\Exception $e) {
            // Manejar cualquier error de SendinBlue
            return response()->json(['error' => $e->getMessage()], 500);
        }

        return response()->json(['message' => 'Contacto agregado correctamente a la lista de SendinBlue']);
    }

    public function sendEmail($recipientEmail, $subject, $content)
    {
        // Configurar los datos del correo
        $senderEmail = config('SENDINBLUE_SENDER_EMAIL');
        $senderName = config('SENDINBLUE_SENDER_NAME');

        // Implementar el código para enviar el correo utilizando SendinBlue
        // Puedes utilizar el método `sendTransactionalEmail` o `sendEmail` del SDK de SendinBlue

        return response()->json(['message' => 'Correo enviado correctamente']);
    }
}
