<?php

namespace App\Http\Controllers;
use App\SysCountry;
use App\SysUser;
use App\SysUserRoles;
use App\SysUserReferred;
use App\SysAccountBank;
use App\SysUserAccountActivation;
use App\Mail\ActivateAccount;
use Carbon\Carbon;
use Validator;
use Hash;
use Mail;
use Request;
use Illuminate\Http\Request as BaseRequest;
use App\Helpers\PtyCommons;

class LandingController extends Controller
{
    public function index()
    {
        return view('landing.index', [
        ]);

    }

    public function start()
    {
        return view('landing.start', [
        ]);

    }

    public function Registerstart()
    {
        $this->viewData['countries']       = SysCountry::all();

        return view('landing.register_start')->with('viewData', $this->viewData);

    }

    function getUserIP() {
        if( array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
            if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')>0) {
                $addr = explode(",",$_SERVER['HTTP_X_FORWARDED_FOR']);
                return trim($addr[0]);
            } else {
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
        else {
            return $_SERVER['REMOTE_ADDR'];
        }
      }

    public function registerUser(Request $request) {

        $referred = false;
        $referred_user = 0;
        $random = (string) random_int(10000, 99999);
        $input = Request::all();

        //dd($input);
        
        $rules = [
          'g-recaptcha-response'            => 'required',
          'register_email'                  => 'required|unique:sysUsers,email',
          'register_first_name'             => 'required|min:3',
          'register_last_name'              => 'required|min:3',
          'register_user_name'              => 'required|unique:sysUsers,user_name|alpha_num|min:4|max:10',
          'register_address'                => 'required',
          'register_zipcode'                => 'required',
          'register_city'                   => 'required',
          'register_country'                => 'required',
          'register_password'               => 'required',
        ];
    
        $messages = array(
          'register_email.required' => 'Email Obligatorio',
          'register_first_name.required' => 'Nombre Obligatorio',
          'register_last_name.required' => 'Apellido Obligatorio',
          'register_user_name.required' => 'Nombre de Usuario Obligatorio',
          'register_address.required' => 'Dirección Obligatoria',
          'register_zipcode.required' => 'Código postal  Obligatorio',
          'register_city.required' => 'Ciudad Obligatorio',
          'register_country.required' => 'País Obligatorio',
          'register_password.required' => 'Email Obligatorio',
    
          'register_email.unique' => 'El correo electrónico ya existe en nuestra base de datos',
          'register_user_name.unique' => 'El email ya existe en nuestra base de datos',
          'register_user_name.alpha_num' => 'El nombre de usuario solo puede contener letras y números',
          'register_user_name.min' => 'El nombre de usuario debe contener min. 4 caracteres',
          'register_user_name.max' => 'El nombre de usuario debe contener max. 10 caracteres',
          'register_password.min' => 'La contraseña debe contener min. 8 caracteres',
          'register_first_name.min' => 'El nombre debe contener min. 3 caracteres',
          'register_last_name.min' => 'El apellido debe contener min. 3 caracteres',
        );
    
        $v = Validator::make($input, $rules, $messages);
    
        //dd($v->errors(),$v->fails());
    
        if(isset($input["register_referred"])){
          // error_log($input["register_referred"]);
          if(SysUser::where('user_name', $input["register_referred"])->first()){
            $referred_user = SysUser::where('user_name', $input["register_referred"])->first()->id;
          } else {
            $referred = true;
          }
        }
        
        if($v->fails()) {
          
          $errors = $v->errors();
          //dd($errors);
          return redirect('/TobeContinued/11RgM8745777UPm/Register')->with([
            'flash_class'   => 'alert-danger',
            'flash_message'   => 'Se encotraron errores.',
            ])->withErrors($errors);
            
          } else {
    
          $country = SysCountry::find($input['register_country']);
          $userName = $input['register_user_name'];
          $newUser = new SysUser;
          $newUser->first_name    = $input['register_first_name'];
          $newUser->last_name     = $input['register_last_name'];
          $newUser->phone     = $input['register_phone'];
          $newUser->twitch_user     = $input['register_twitch_user'];
          $newUser->user_name     = $userName;
          $newUser->email         = $input['register_email'];
          $newUser->password      = Hash::make($input['register_password']);
          $newUser->country_id    = $input['register_country'];
          //dd($newUser->country_id);
          $newUser->country_code  = $country->alpha_2;
          $newUser->city          = $input['register_city'];
          $newUser->zipcode       = $input['register_zipcode'];
          $newUser->address       = $input['register_address'];
          $newUser->concept       = $userName . '-' . $random;
          $newUser->save();
    
          $newUserRol = new SysUserRoles;
          $newUserRol->user_id    = $newUser->id;
          $newUserRol->role_id    = 4;
          $newUserRol->save();
    
          if($referred_user > 0){
            $newUserReferred = new SysUserReferred;
            $newUserReferred->master_id    = $referred_user;
            $newUserReferred->son_id    = $newUser->id;
            $newUserReferred->save();
          }
    
          $newAccountBank = new SysAccountBank;
          $newAccountBank->user_id = $newUser->id;
          $newAccountBank->beneficiary= $newUser->first_name . ' ' . $newUser->last_name ;
          $newAccountBank->save();
          
          $actRequest = new SysUserAccountActivation();
    
          $actRequest->email = $newUser->email;
          $actRequest->token = str_random(100);
          $actRequest->save();
    
          PtyCommons::setUserEvent($newUser->id, 'Cuenta creada el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP()]);
          try {
            //code...
            Mail::to($newUser->email)->queue(new ActivateAccount($actRequest->token, $newUser->first_name . ' ' . $newUser->last_name));
          } catch (\Exception  $th) {
            Log::info($th);
            //throw $th;
          }
    
          return redirect('/TobeContinued/11RgM8745777UPm/Register')->with([
            'flash_class'     => 'alert-success',
            'flash_message'   => 'Cuenta registrada satisfactoriamente.Ahora es necesario que revises tu correo (El que registraste) para completar la activación de tu cuenta.',
            'flash_important' => true,
        ]);
    
        }
     
      }
}
