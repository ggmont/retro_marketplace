<?php

namespace App\Http\Controllers;

use Auth;
use Config;
use Datatables;
use DB;
use File;
use Request;
use Response;
use Validator;

use App\AppOrgUserInventory;
use App\AppOrgUserInventoryImage;
use App\AppOrgProduct;
use App\SysDictionary;

class MyStoreController extends Controller {

  private $viewData = [];

  public function __construct() {
    $this->viewData['column_prefix'] = Config::get('brcode.column_prefix');
  }

  /*
	|--------------------------------------------------------------------------
	| Configuration
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view displaying the store configuration in:
   * /my_store/$user_id/configuration
	 *
	 * @return view
	 */
  public function showConfiguration() {

  }

  /**
	 * Save the store configuration
	 *
	 * @return redirect('back if fails | /my_store/$user_id/configuration')
	 */
  public function saveConfiguration() {

  }

  /*
	|--------------------------------------------------------------------------
	| Inventory
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view that list inventory in: /my_store/$user_id/inventory
	 *
	 * @return view
	 */
  public function showInventories($user_id) {
    $this->viewData['form_title'] = trans('org.inventories');
		$this->viewData['form_url_add'] = url('/11w5Cj9WDAjnzlg0/my_store/'.$user_id.'/add_inventory');
		$this->viewData['form_url_dt'] =  url('/11w5Cj9WDAjnzlg0/my_store/'.$user_id.'/inventory/list-dt');
		$this->viewData['table_cols'] = ['app.col_id', 'org.col_product_name', 'org.col_box_condition',
      'org.col_cover_condition', 'org.col_game_condition', 'org.col_extra_condition','org.col_price',
      'org.col_status','app.col_is_enabled'];

    return view('brcode.mystore.inventories')->with('viewData',$this->viewData);
  }

  /**
	 * Create a DataTable output for inventory' table in: /my_store/$user_id/inventory
	 *
	 * @return DataTable
	 */
  public function dtInventories($user_id) {

    $inventories = DB::table(DB::raw('appOrgUserInventories AS i'))
      ->select(['i.id', 'p.name',
      DB::raw('d1.value AS box_condition'),DB::raw('d2.value AS cover_condition'),
      DB::raw('d3.value AS game_condition'),DB::raw('d4.value AS extra_condition'),
        'i.price', 'i.is_enabled',
        DB::raw('(CASE WHEN i.status = "A" THEN "Available" WHEN "H" THEN "Hidden" WHEN "S" THEN "Sold" END) as status')
      ])
      ->join('appOrgProducts AS p','p.id','=','i.product_id')
      ->join('sysDictionaries AS d1','d1.value_id','=','i.box_condition')
      ->join('sysDictionaries AS d2','d2.value_id','=','i.cover_condition')
      ->join('sysDictionaries AS d3','d3.value_id','=','i.game_condition')
      ->join('sysDictionaries AS d4','d4.value_id','=','i.extra_condition')
      ->where('user_id',$user_id)
      ->whereNull('i.deleted_at');

    $result =  Datatables::of($inventories)
		  ->addColumn('action', function ($inventories) use($user_id) {
        return '<a href="' . url('/11w5Cj9WDAjnzlg0/my_store/'.$user_id.'/modify_inventory/' .$inventories->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
      });

    //var_dump(DB::getQueryLog());
    return $result->make(true);
  }

  /**
	 * Show the view for an inventory: /my_store/$user_id/add_inventory
   * and /my_store/$user_id/modify_inventory
	 *
	 * @return view
	 */
  public function showInventory($user_id, $id = 0) {
    $this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('org.inventory_item');
		$this->viewData['form_list_title'] = trans('org.inventories');
		$this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/my_store/'.$user_id.'/inventory');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/my_store/'.$user_id.'/save_inventory');
		$this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/my_store/'.$user_id.'/delete_inventory');
    $this->viewData['sub_model_title'] = trans('org.images');
		$this->viewData['sub_model_view'] = 'file-uploader';
    $this->viewData['model_id'] = 0;

		if($id > 0) {

			$this->viewData['model']     = AppOrgUserInventory::find($id);
      $this->viewData['sub_model'] = $this->viewData['model']->images->toArray();
			$this->viewData['model']     = $this->viewData['model']->toArray();
			$this->viewData['model_id']  = $id;

		}

    $listModel = SysDictionary::whereIn('code',['GAME_STATE'])->get();
		$list = [];

		foreach($listModel as $key => $row) {
			$list[$key] = ['id' => $row->value_id, 'value' => $row->value];
		}

    $productModel = AppOrgProduct::where('is_enabled','Y')->get();
    $products = [];

    foreach($productModel as $key => $row) {
			$products[$key] = ['id' => $row->id, 'value' => $row->name];
		}

		$this->viewData['model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
  		'product_id' 						=>  ['type' => 'select', 'select_values' => $products, 'class' => 'br-select2', 'label' => trans('org.col_product_name')],
  		'box_condition' 				=>  ['type' => 'select', 'select_values' => $list, 'class' => 'br-select2', 'label' => trans('org.col_box_condition')],
  		'cover_condition' 			=>  ['type' => 'select', 'select_values' => $list, 'class' => 'br-select2', 'label' => trans('org.col_cover_condition')],
  		'game_condition' 			  =>  ['type' => 'select', 'select_values' => $list, 'class' => 'br-select2', 'label' => trans('org.col_game_condition')],
  		'extra_condition' 			=>  ['type' => 'select', 'select_values' => $list, 'class' => 'br-select2', 'label' => trans('org.col_extra_condition')],
  		'price' 			          =>  ['type' => 'input-text', 'attr' => ' data-number="4" ', 'label' => trans('org.col_price')],
  		'comments' 			        =>  ['type' => 'input-textarea'],
  		// 'image_path' 			      =>  ['type' => 'file-uploader','file-type' => 'image', 'is_array' => true,
      //                             'class' => 'image-uploader','attr' => 'data-url="'.url('users/profile/upload_image').'"',
      //                              'img-class' => 'profile-user-image', 'img-path' => 'uploads/product_images/'],
  		'is_enabled' 						=>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ], 'class' => 'br-select2'],
		];

    $this->viewData['sub_model_data'] = [
      'file_type'   => 'image',
      'column_name' => 'image_path',
      'upload_url'  => url('11w5Cj9WDAjnzlg0/my_store/inventory/upload_file'),
      'img_class'   => 'profile-user-image',
      'img_path'    => url('uploads/inventory-images'),
      'img_qty'     => 4
    ];

		return view('brcode.mystore.inventory')->with('viewData',$this->viewData);
  }

  /**
	 * Save an inventory
	 *
	 * @return redirect('back if fails | /my_store/$user_id/inventory')
	 */
  public function saveInventory($user_id) {
    $colPrefix = $this->viewData['column_prefix'];
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		$rules = [
						$colPrefix.'product_id' 					=> 'required',
						$colPrefix.'box_condition' 			  => 'required',
						$colPrefix.'cover_condition' 			=> 'required',
						$colPrefix.'game_condition' 			=> 'required',
						$colPrefix.'extra_condition' 			=> 'required',
						$colPrefix.'price' 			          => 'required',
					];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {
      $input['add_' . 'user_id'] = Auth::id();

      $id = saveModel($id, $model, 'App\\AppOrgUserInventory',$subModelExists, $input, $colPrefix, $new);

      saveSubModel($new, $id, 'inventory_id', 'App\\AppOrgUserInventory', 'images', 'image_path', 'App\\AppOrgUserInventoryImage'
        ,$colPrefix, $input,'0',[],[],'inv_img','inventory-images');

			$message = trans('org.inventory') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/11w5Cj9WDAjnzlg0/my_store/'.$user_id.'/inventory')->with('message',$message)->with('messageType','success');

		}
  }

  /**
	 * Delete an inventory
	 *
	 * @return redirect('/my_store/$user_id/inventory'')
	 */
  public function deleteInventory() {

  }

  /**
	 * Upload the profile's image
	 *
	 * @return json response
	 */
	public function uploadInventoryImages() {
		$inputName = 'files';

		$file = Request::file($inputName);

		if($file) {

        $destinationPath = public_path() . '/uploads/inventory-images/';

				$extension = $file[0]->getClientOriginalExtension();
				$filename = md5(microtime()) . '.' . $extension;

        // Prepare dir
        $dateFolder = date('Y') . '/' . date('m') . '/' . date('d') . '/';

        $directory = $destinationPath . $dateFolder;

				$filename200 = 'inv_img_' . Auth::id() . '_'.microtime(true).'.' . $extension;
        $upload_success = Request::file($inputName)[0]->move($directory, $filename);

        if ($upload_success) {
            // resizing an uploaded file
            //Image::make($destinationPath . $filename)->resize(200, 200)->save($destinationPath . $filename200);

            return Response::json(array('message' => 'success', 'name' => $dateFolder . $filename), 200);
        } else {
            return Response::json(array('mesage' => 'error'), 400);
        }
    }

	}

  public function removeInventoryImages() {
    $fileName = Request::input('img');

    if(file_exists(public_path() . '/uploads/inventory-images/' . $fileName)) {
      unlink(public_path() . '/uploads/inventory-images/' . $fileName);

      return Response::json(array('message' => 'success'), 200);
    }

    return Response::json(array('message' => 'error', 'reason' => 'Unknown file'), 200);
  }

  /*
	|--------------------------------------------------------------------------
	| Orders
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view that list orders in: /my_store/$user_id/orders
	 *
	 * @return view
	 */
  public function showOrders($user_id) {
		error_log('');
  }

  /**
	 * Create a DataTable output for orders' table in: /my_store/$user_id/orders
	 *
	 * @return DataTable
	 */
  public function dtOrders($user_id) {

  }

  /**
	 * Show the view for an order: /my_store/$user_id/modify_order
	 *
	 * @return view
	 */
  public function showOrder($user_id, $id = 0) {

  }

  /**
	 * Save an order
	 *
	 * @return redirect('back if fails | /my_store/$user_id/order')
	 */
  public function saveOrder($user_id) {

  }

}
