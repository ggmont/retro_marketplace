<?php

namespace App\Http\Controllers;

use Auth;
use Config;
use Datatables;
use DB;
use Redis;
use Illuminate\Http\Request;
use App\AppMessage;
use App\AppConversation;
use App\SysUser;
use Validator;

class MessageController extends Controller {

    public function index(Request $request)
    {    	
			
    	$userId = auth()->user()->user_name;
			$contactId = $request->contact_id;
			
			$messageNotRead = AppMessage::where('alias_from_id', $contactId)->where('alias_to_id', $userId)->where('read', false)->update(['read' => true]);   
			
			return AppMessage::select(
				'id', 
				DB::raw("IF(`alias_from_id`= '$userId' , TRUE, FALSE) as written_by_me"),
				'created_at', 
				'content',
				'read'
			)->where(function ($query) use ($userId, $contactId) {
							$query->where('alias_from_id', $userId)->where('alias_to_id', $contactId)->whereNull('alias_from_del');
			})->orWhere(function ($query) use ($userId, $contactId) {
							$query->where('alias_from_id', $contactId)->where('alias_to_id', $userId)->whereNull('alias_to_del');
			})->get();     
		   
    }


    public function store(Request $request)
    {
			$con = AppConversation::where('alias_user_id', auth()->user()->user_name)->where('alias_contact_id', $request->alias)->first();
			$conUs = AppConversation::where('alias_contact_id', auth()->user()->user_name)->where('alias_user_id', $request->alias)->first();

			if($con->del_messages){
				$con->del_messages = false;
				$con->save();
			}

			if($conUs->del_messages){
				$conUs->del_messages = false;
				$conUs->save();
			}

			$message = new AppMessage();
			$message->alias_from_id = auth()->user()->user_name;
			$message->alias_to_id = $request->alias;

			$message->content = $request->content;
			$saved = $message->save();

			$data = [];
			$data['success'] = $saved;
				$data['message'] = $message;
			return $data;
		}
	
    public function removeSMS(Request $request)
    {
			$msg = AppMessage::where('id', $request->msg)->first();
			$result = false;

			if($msg->alias_from_id == auth()->user()->user_name){
				$msg->alias_from_del = auth()->user()->user_name;
				$result = $msg->save();
			}

			if($msg->alias_to_id == auth()->user()->user_name){
				$msg->alias_to_del = auth()->user()->user_name;
				$result = $msg->save();
			}

			$data = [];
			$data['success'] = $result;
			return $data;
		}
	
    public function removeAll(Request $request)
		{

			AppMessage::where('alias_from_id',auth()->user()->user_name)->where('alias_to_id', $request->msg)->update(['alias_from_del' => auth()->user()->user_name]);
			AppMessage::where('alias_to_id', auth()->user()->user_name)->where('alias_from_id', $request->msg)->update(['alias_to_del' => auth()->user()->user_name]);

			$con = AppConversation::where('alias_user_id', auth()->user()->user_name)->where('alias_contact_id', $request->msg)->first();
			$con->del_messages = true;
			$res = $con->save();
			$data = [];
			$data['success'] = $res;
			return $data;
		}

		// Admin panel control
		public function showMessages(){
			return view('brcode.users.messages')->with('conversations', AppConversation::where('master_conversation', true)->get());
		}

		public function showUserMessages(Request $request){
			$userId = $request->u;
			$contactId = $request->c;

			$data = [];
			$data['success'] = true;
			$data['us'] = SysUser::select(
				'user_name',
				'first_name'
			)->where('user_name', $userId)->first();
			$data['con'] = SysUser::select(
				'user_name',
				'first_name'
			)->where('user_name', $contactId)->first();

			$data['messages'] = AppMessage::select(
				'id', 
				DB::raw("IF(`alias_from_id`='$userId', TRUE, FALSE) as written_by_me"),
				'created_at', 
				'content',
				'read'
			)->where(function ($query) use ($userId, $contactId) {
							$query->where('alias_from_id', $userId)->where('alias_to_id', $contactId);
			})->orWhere(function ($query) use ($userId, $contactId) {
					$query->where('alias_from_id', $contactId)->where('alias_to_id', $userId);
			})->get();  
		   dd($data);
		}

}
