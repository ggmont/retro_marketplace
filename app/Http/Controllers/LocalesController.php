<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\Cookie;

class LocalesController extends Controller
{
    public function setLocale($locale)
    {
        if (in_array($locale, \Config::get('app.locales'))) {
            session(['locale' => $locale]);
        }

        return redirect()->back();
    }
}