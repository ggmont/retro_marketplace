<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\AppOrgPromotionalCode;
use App\AppOrgHistorialPromotionalCode;
use Illuminate\Http\Request;

class HistorialPromotionalController extends Controller
{
    public function index() {
    	
    	$historial_promotional       = AppOrgHistorialPromotionalCode::all();

        //dd($historial_promotional);

        return view('admin.historial_promotional.index', [
            'historial_promotional'     => $historial_promotional,
        ]);
    }
}
