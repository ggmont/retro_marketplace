<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Mimey\MimeTypes;

class UserInventoryImageController extends Controller
{
    public function store(Request $request): array
    {
        $images = [];
        $directory = public_path() . '/uploads/inventory-images/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
        if(is_dir($directory) === false){
            mkdir($directory, 0755, true);
        }

        ini_set('memory_limit', '512M');

        foreach ($request->input('photos') as $photo){
            $mime_type = mime_content_type($photo);
            $mimes = new MimeTypes;
            $extension = $mimes->getExtension($mime_type);
            $filename = md5(microtime()) . '.' . $extension;
            Image::make(file_get_contents($photo))->save($directory.$filename);

            if(file_exists($directory.$filename)){
                $images[] = date('Y') . '/' . date('m') . '/' . date('d') . '/' . $filename;
            }
        }

        return $images;
    }
}
