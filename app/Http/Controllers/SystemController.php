<?php namespace App\Http\Controllers;

use Auth;
use DB;
use Datatables;
use Config;
use Cookie;
use Redirect;
use Request;
use Illuminate\Validation\Rule;
use Validator;

use App\Events\ActionExecuted;
use App\SysDictionary;

class SystemController extends Controller {

	// Data to be used within the view
	private $viewData = [];

	// Column prefix for the form fields
	private $columnPrefix = '';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->columnPrefix =  Config::get('brcode.column_prefix');
		$this->viewData['column_prefix'] = $this->columnPrefix;
	}

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	private function setInitialData() {
		$this->viewData['first_name'] = Auth::user()->first_name;
		$this->viewData['user_full_name'] = Auth::user()->first_name . ' ' . Auth::user()->last_name;
		$this->viewData['model_id'] = 0;
	}

	/*
	|--------------------------------------------------------------------------
	| Show List
	|--------------------------------------------------------------------------
	|
	*/

	public function showLists()  {
		$this->setInitialData();

		$this->viewData['form_title'] = trans('app.lists');
		$this->viewData['form_url_add'] = url('/11w5Cj9WDAjnzlg0/configuration/list');
		$this->viewData['form_url_dt'] = url('/11w5Cj9WDAjnzlg0/configuration/lists-dt');
		$this->viewData['table_cols'] = ['col_id','col_code','col_value_id','col_value'];
		$this->viewData['current_option'] = 'configuration/lists';

		return view('brcode.system.lists')->with('viewData',$this->viewData);
	}

	public function dtLists() {
		$dictionary = DB::table('sysDictionaries')
		->select(['id', 'code', 'value_id', 'value' ])
		->whereNull('deleted_at');

    return Datatables::of($dictionary)
			->addColumn('action', function ($dictionary)  {
              return '<a href="' . url('/11w5Cj9WDAjnzlg0/configuration/list/' .$dictionary->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
          })
			->make();
	}

	public function showList($id = 0) {

		$this->setInitialData();

		$this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('app.list_value');
		$this->viewData['form_list_title'] = trans('app.lists');
		$this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/configuration/lists');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/configuration/save_list');
		$this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/configuration/delete_list');

		if($id > 0) {
			$this->viewData['model'] = SysDictionary::find($id);
			$this->viewData['model'] = $this->viewData['model']->toArray();
			$this->viewData['model_id'] = $id;
		}

		$dictionaryCode  = SysDictionary::select(DB::raw('distinct(code) as code'))->whereNull('deleted_at')->get();

		$dictionaries = [];

		foreach($dictionaryCode as $key => $row) {
			array_push($dictionaries,$row->code);
		}

		$dictionaryItems = SysDictionary::whereNull('deleted_at')->orderBy('code')->orderBy('value')->get()->toArray();

		$dictionaries2 = [];

		foreach($dictionaryItems as $key => $row) {
			$parent = $this->_getDictionaryParent($row,'',$dictionaryItems);
			array_push($dictionaries2, ['id' => $row['id'],'value' => $row['code'] . ' - ' . $parent . $row['value']]);
		}

		$this->viewData['model_cols'] = [
			'id' 										=> 	['type' => 'hidden', 'attr' => ' '],
			'code' 									=>  ['type' => 'input-autocomplete', 'data-typeahead' => json_encode($dictionaries), 'attr' => ' maxlength=15 '],
			'value_id' 							=>  ['type' => 'input-text', 'attr' => ' maxlength=10 '],
			'value' 								=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
			'parent_id' 						=>  ['type' => 'select', 'select_values' => $dictionaries2,'class' => ' br-select2 '],
			'order_by' 							=>  ['type' => 'input-text', ' attr' => ' maxlength=4 '],
		];

		return view('brcode.system.list')->with('viewData', $this->viewData);

	}

	private function _getDictionaryParent($row, $str = '', &$arr) {
		if($row['parent_id'] > 0) {

			$parentIndex = array_search($row['parent_id'], array_column($arr,'id'));

			if($parentIndex >= 0) {
				$parentParentId = $arr[$parentIndex]['parent_id'];

				if($parentParentId > 0) {
					return $this->_getDictionaryParent($arr[$parentIndex], $str, $arr) . $arr[$parentIndex]['value'] . ' - ';
				}
				else {
					return $arr[$parentIndex]['value'] . ' - ';
				}

			}
			else {
				return $str;
			}
		}
		else {
			return $str;
		}
	}

	public function saveList() {
		$colPrefix = $this->columnPrefix;
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		$input = Request::all();

		$value = $input[$colPrefix.'value'];

		$rules = [
						$colPrefix.'code' 					=> 'required|min:3|max:15',
						$colPrefix.'value_id' 			=> [
							'required',
							'min:3',
							'max:10',
							Rule::unique('sysDictionaries', 'value_id')->ignore($id)->where(function($query) use($value) {
								$query->where('value',$value);
							})
						],
					];

		$messages = [
			$colPrefix.'value_id.unique' => str_replace(':field2',trans('app.col_value'),str_replace(':field1',trans('app.col_value_id'),trans('validation.unique_two_columns'))),
		];

		$v = Validator::make($input, $rules, $messages);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors())
				->withInput(Request::all());
		}
		else {

			$id = saveModel($id, $model, 'App\\SysDictionary',$subModelExists, $input, $colPrefix, $new);

			$message = trans('app.list_value') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/11w5Cj9WDAjnzlg0/configuration/lists')->with('message',$message)->with('messageType','success');

		}
	}

	public function deleteList() {
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		if($id > 0) {

			$listValue = SysDictionary::find($id);

			$listValue->delete();

			$message = trans('app.list_value') . ' ' . trans('app.deleted_successfully');

			return redirect('/11w5Cj9WDAjnzlg0/configuration/lists')->with('message',$message)->with('messageType','success');
		}
	}

	/*
	|--------------------------------------------------------------------------
	| Organizations
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view that list organizations in: /11w5Cj9WDAjnzlg0/configuration/organizations
	 *
	 * @return view
	 */
	public function showOrganizations() {
		$this->viewData['form_title'] = trans('app.organizations');
		$this->viewData['form_url_add'] = url('/11w5Cj9WDAjnzlg0/configuration/organizations/add');
		$this->viewData['form_url_dt'] = url('/11w5Cj9WDAjnzlg0/configuration/organizations/list-dt');
		$this->viewData['table_cols'] = ['app.col_id','app.col_name','app.col_administrator', 'app.col_is_enabled'];

    return view('brcode.config.organizations')->with('viewData', $this->viewData);
	}

	/**
	 * Create a DataTable output for organizations' table in: /11w5Cj9WDAjnzlg0/configuration/organizations
	 *
	 * @return DataTable
	 */
  public function dtCategories() {

    $cats = DB::table('appOrgCategories')->select(['id', 'name', 'category_text', 'image_path', 'is_enabled'])->whereNull('deleted_at');;
    return Datatables::of($cats)
      ->addColumn('image_path',function($cats) {
        if(strlen($cats->image_path) > 0 ) {
          return '<img class="table-image" src="'.url('uploads/category-images/'.$cats->image_path).'" />';
        }
        else {
          return '';
        }
      })
			->addColumn('action', function ($cats) {
                return '<a href="' . url('/11w5Cj9WDAjnzlg0/marketplace/categories/modify/' .$cats->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
            })
			->make();
  }

	// public function dtSchools() {
	// 	$rows = DB::table('schools')->select(['schools.id', 'schools.name','sysUsers.first_name', 'schools.is_enabled'])
	// 					->join('sysUsers','sysUsers.id','=','schools.user_id');
	//
  //       return Datatables::of($rows)
	// 		->addColumn('action', function ($rows) {
  //               return '<a href="' . url('/configuration/school/' .$rows->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
  //           })
	// 		->make();
	// }

}
