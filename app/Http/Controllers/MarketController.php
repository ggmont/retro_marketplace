<?php

namespace App\Http\Controllers;

use Auth;
use Config;

use Datatables;
use DB;
use Image;
use Redirect;
use Request;
use Response;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request as BaseRequest;

use File;
use Validator;

use App\AppOrgBanner;
use App\AppOrgCarousel;
use App\AppOrgCarouselItem;
use App\AppOrgCategory;
use App\AppOrgFaq;
use App\AppOrgFooterLink;
use App\AppOrgNewsItem;
use App\AppOrgPage;
use App\AppOrgProduct;
use App\AppOrgOrder;
use App\SysDictionary;
use App\SysShipping;
use App\SysCountry;
use App\SysUser;
use App\AppOrgOrderNotReceived;
use App\AppOrgProductImage;
use App\AppOrgBlog;

class MarketController extends Controller {

  private $viewData = [];
  private $columnPrefix = '';

  public function __construct() {
    $this->columnPrefix =  Config::get('brcode.column_prefix');
		$this->viewData['column_prefix'] = $this->columnPrefix;
  }
  

  /*
	|--------------------------------------------------------------------------
	| Categories
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view that list categories in: /marketplace/categories
	 *
	 * @return view
	 */
  public function showCategories() {
    $this->viewData['form_title'] = trans('app.categories');
		$this->viewData['form_url_add'] = url('/11w5Cj9WDAjnzlg0/marketplace/categories/add');
		$this->viewData['form_url_dt'] = url('/11w5Cj9WDAjnzlg0/marketplace/categories/list-dt');
		$this->viewData['table_cols'] = ['app.col_id', 'app.col_name', 'app.col_category_text', 'app.col_image', 'app.col_order_by', 'app.col_hierarchy','app.col_is_enabled'];

    return view('brcode.marketplace.categories')->with('viewData', $this->viewData);
  }

  /**
	 * Create a DataTable output for categories' table in: /marketplace/categories
	 *
	 * @return DataTable
	 */
  public function dtCategories() {

    $cats = DB::table('appOrgCategories')->select(['id', 'name', 'category_text', 'image_path', 'order_by','hierarchy','is_enabled'])->whereNull('deleted_at');;
    return Datatables::of($cats)
      ->addColumn('image_path',function($cats) {
        if(strlen($cats->image_path) > 0 ) {
          return '<img class="table-image" src="'.url('uploads/category-images/'.$cats->image_path).'" />';
        }
        else {
          return '';
        }
      })
			->addColumn('action', function ($cats) {
                return '<a href="' . url('/11w5Cj9WDAjnzlg0/marketplace/categories/modify/' .$cats->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
            })
			->make();
  }

  /**
	 * Show the view for a category: /marketplace/category
	 *
	 * @return view
	 */
  public function showCategory($id = 0) {
    $this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('app.category');
		$this->viewData['form_list_title'] = trans('org.categories');
		$this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/marketplace/categories');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/categories/save');
		$this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/marketplace/categories/delete');
    $this->viewData['model_id'] = 0;

		if($id > 0) {
      $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/categories/modify');
			$this->viewData['model'] = AppOrgCategory::find($id);
			$this->viewData['model'] = $this->viewData['model']->toArray();
			$this->viewData['model_id'] = $id;

		}

    $listModel = AppOrgCategory::all();
    $listCategory = [];
		foreach($listModel as $key => $row) {
      if($id == $row->id) continue;
      $listCategory[$key] = ['id' => $row->id, 'value' => strlen($row->category_text) > 0 ? $row->category_text : $row->name];
		}

		$this->viewData['model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
    'image_path'				      =>	['type' => 'file-uploader', 'file-type' => 'image', 'is_array' => false,
                                    'class' => 'image-uploader','attr' => 'data-url="'.url('11w5Cj9WDAjnzlg0/marketplace/categories/upload_image').'"',
                                    'img-class' => 'profile-user-image', 'img-path' => 'uploads/category-images/'],
      'name' 						      =>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
      'order_by' 						  =>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
      'parent_id' 						=>  ['type' => 'select',  'select_values' => $listCategory, 'attr' => ' maxlength=50 ', 'class' => 'br-select2'],

      'is_enabled' 						=>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ], 'class' => 'br-select2' ],
    ];

		return view('brcode.marketplace.category')->with('viewData',$this->viewData);
  }

  /**
	 * Save a category
	 *
	 * @return redirect(back if fails | /marketplace/categories)
	 */
  public function saveCategory() {
    $colPrefix = $this->columnPrefix;

		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		$rules = [
			$colPrefix.'name' 			  => 'required|min:3|max:50|unique:appOrgCategories,name' . ($id > 0 ? ','.$id : ''),
			$colPrefix.'is_enabled' 	=> 'required',
		];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {
      $parentId = isset($input[$colPrefix . 'parent_id']) ? intval($input[$colPrefix . 'parent_id']) : 0;
      if($parentId > 0 ) {
        $info = AppOrgCategory::find($parentId);
        $input['add_' . 'hierarchy'] = $info->hierarchy + 1;
      }

      $categoryImage = isset($input[$colPrefix . 'image_path']) ? $input[$colPrefix . 'image_path'] : '';
      $updatePrev = false;
      $updateNew = false;
      $prevParentId = 0;
      $newParentId = 0;

      if($id > 0) {
        $cat = AppOrgCategory::find($id);

        $prevParentId = $cat->parent_id;
        if($prevParentId > 0 && $parentId != $prevParentId) {
          $updatePrev = true;
          $updateNew = true;
          $newParentId = $parentId;
        }
        else if($prevParentId == 0 && $parentId > 0) {
          $updateNew = true;
          $newParentId = $parentId;
        }
      }
      else if($parentId > 0){
        $updateNew = true;
        $newParentId = $parentId;
      }

			saveModel($id, $model, 'App\\AppOrgCategory',$subModelExists, $input, $colPrefix, $new);

      if($updatePrev) {
        $parent = AppOrgCategory::find($prevParentId);
        $parent->children = $parent->children - 1;
        $parent->save();
      }

      if($updateNew) {
        $parent = AppOrgCategory::find($newParentId);
        $parent->children = $parent->children + 1;
        $parent->save();
      }

      if($id == 0 && strlen($categoryImage) > 0 && strpos($categoryImage,'category_image_') === false) {

        $ext = File::extension(public_path() . '/uploads/category-images/' . $categoryImage);

        File::move(public_path() . '/uploads/category-images/' . $categoryImage,
          public_path() . '/uploads/category-images/category_image_400x400_' . $model->id . '.' . $ext);

        $filename200 = 'category_image_200x200_' . $model->id . '.' . $ext;
        Image::make(public_path() . '/uploads/category-images/category_image_400x400_' . $model->id . '.' . $ext)
          ->resize(200, 200)->save(public_path() . '/uploads/category-images/category_image_200x200_' . $model->id . '.' . $ext);

        $model->image_path = 'category_image_400x400_' . $model->id . '.' . $ext;
        $model->save();

      }

      $this->_fixCategories();

			$message = trans('org.category') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/categories')->with('message',$message)->with('messageType','success');

		}
  }

  private function _fixCategories() {
    $cat = AppOrgCategory::orderBy('parent_id')->get();

    foreach($cat as $key => $row) {

      $curr = AppOrgCategory::find($row->id);

      $catData = $this->_getCatData($row);

      $curr->category_text = $catData['name'];
      $curr->order_text = $catData['order'];
      $curr->hierarchy = $catData['hierarchy'];
      $curr->save();
    }

  }

  private function _getCatData($row, $hierarchy = 0) {
    $result = [];
    $name = '';

    if($row->parent_id > 0) {
      $parent = AppOrgCategory::find($row->parent_id);
      if($parent){
        $parentData = $this->_getCatData($parent, $hierarchy);
        $name = $parentData['name'] . ' -> '. $row->name;
        $result['name'] = $name;
        $result['order'] = $parentData['order'] . '//'. $row->id;
        $result['hierarchy'] = $parentData['hierarchy'] + 1;
      } else{
        $result['name'] = $row->name;
        $result['order'] = $row->id;
        $result['hierarchy'] = 0;
      }

    }
    else {
      $result['name'] = $row->name;
      $result['order'] = $row->id;
      $result['hierarchy'] = 0;
    }

    return $result;
  }

  /**
	 * Delete a category
	 *
	 * @return redirect('marketplace/categories')
	 */
	public function deleteCategory() {
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		if($id > 0) {

			$product = AppOrgCategory::find($id);
      $product->modified_by = Auth::id();
			$product->deleted_at = date('Y-m-d H:i:s');
      $product->save();

      $parentId = $product->parent_id;
      if($parentId > 0) {
        $parent = AppOrgCategory::find($parentId);
        $parent->children = $parent->children - 1;
        $parent->save();
      }

			$message = trans('org.category') . ' ' . trans('app.deleted_successfully');

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/categories')->with('message',$message)->with('messageType','success');
		}

	}

  public function saveCategoryImage() {
    $inputName = 'file_' . $this->columnPrefix . 'image_path';

		$file = Request::file($inputName);
    $input = Request::all();
    $modelId = $input['model_id'];

		if($file) {

        $destinationPath = public_path() . '/uploads/category-images/';
        //$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				//$filename = md5(microtime()) . '.' . $extension;
        if($modelId > 0) {
          $filename = 'category_image_400x400_' . $modelId . '.' . $extension;
        }
        else {
          $filename = md5(microtime()) . '.' . $extension;
        }

        $upload_success = Request::file($inputName)->move($destinationPath, $filename);

        if ($upload_success) {
            // resizing an uploaded file
            if($modelId > 0) {
              $filename200 = 'category_image_200x200_' . $modelId . '.' . $extension;
              Image::make($destinationPath . $filename)->resize(200, 200)->save($destinationPath . $filename200);
            }

            return Response::json(array('message' => 'success', 'name' => $filename), 200);
        } else {
            return Response::json(array('mesage' => 'error'), 400);
        }
    }
	}



  /*
	|--------------------------------------------------------------------------
	| Products
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view that list products in: /marketplace/products
	 *
	 * @return view
	 */
  public function showProducts() {

    $this->viewData['platform_sys'] = SysDictionary::where('code', 'GAME_PLATFORM')->get();
    $this->viewData['form_title'] = trans('org.products');
		$this->viewData['form_url_add'] = url('/11w5Cj9WDAjnzlg0/marketplace/products/add');
		$this->viewData['form_url_dt'] = url('/11w5Cj9WDAjnzlg0/marketplace/products/list-dt');
		$this->viewData['table_cols'] = ['app.col_id', 'app.col_name', 'org.col_prod_category_id','org.col_release_date', 'org.col_game_language', 'org.col_platform', 'org.col_region', 'app.col_is_enabled'];

    return view('brcode.marketplace.products')->with('viewData', $this->viewData);
  }

  /**
	 * Create a DataTable output for product' table in: /marketplace/products
	 *
	 * @return DataTable
	 */
  public function dtProducts() {

    $products = DB::table(DB::raw('appOrgProducts AS p'))
    ->select(['p.id', 'p.name' ,'p.release_date', 'p.platform','p.region', 'p.is_enabled',
      DB::raw('c.category_text AS category'),DB::raw('l.value AS language')
    ])
    ->whereNull('p.deleted_at')
    ->join('appOrgCategories AS c','c.id','=','p.prod_category_id')
    ->leftJoin('sysDictionaries AS l', 'l.value_id','=','p.language');
    return Datatables::of($products)
			->addColumn('action', function ($products) {
                return '<a href="' . url('/11w5Cj9WDAjnzlg0/marketplace/products/modify/' .$products->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>'.' <button class="btn btn-xs btn-success btn-image-open" onclick="openModalImage(this)" data-product="' .$products->id.'" data-name="' .$products->name.'"><i class="fa fa-image"></i></button>';
            })
      ->filter(function ($query) {
        $searchValue = Request::input('search')['value'];
        if( strlen($searchValue) > 0 ) {
          $query->where('l.value', 'like', "%{$searchValue}%");
        }
      })
			->make(true);
  }

  /**
	 * Show the view for a product: /marketplace/add_product
   * and marketplace/modify_product
	 *
	 * @return view
	 */
  public function showProduct($id = 0) {
    $this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('org.product');
		$this->viewData['form_list_title'] = trans('org.products');
		$this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/marketplace/products');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/products/save');
		$this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/marketplace/products/delete');
    $this->viewData['sub_model_title'] = trans('org.images');
		$this->viewData['sub_model_view'] = 'file-uploader';
    $this->viewData['model_id'] = 0;

		if($id > 0) {
      $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/products/modify');
			$this->viewData['model'] = AppOrgProduct::find($id);
      $this->viewData['sub_model'] = $this->viewData['model']->images->toArray();
			$this->viewData['model']     = $this->viewData['model']->toArray();
      $this->viewData['model_id'] = $id;
      $pro = AppOrgProduct::find($id);
      $this->viewData['plat_dic'] = $pro->sysDictionaries->pluck('id');
      
    }

    $listModel        = SysDictionary::whereIn('code',['GAME_SUPPORT','GAME_REGION','GAME_PLATFORM', 'GAME_CATEGORY','GAME_GENERATION','GAME_LOCATION', 'GAME_LANGUAGE'])
      ->orderBy('code')->orderByRaw(' IFNULL(order_by,0) ')->get();
		$listPlatform     = [];
    $listRegion       = [];
    $listCategory     = [];
    $listGameCategory = [];
    $listGameSupport  = [];
    $listGameLanguage = [];

		foreach($listModel as $key => $row) {

      if($row->code == 'GAME_REGION') {
        $listRegion[$key] = ['id' => $row->value_id, 'value' => $row->value];
      }
      else if ($row->code == 'GAME_PLATFORM') {
        $listPlatform[$key] = ['id' => $row->value_id, 'value' => $row->value];
      }
      else if ($row->code == 'GAME_CATEGORY') {
        $listGameCategory[$key] = ['id' => $row->value_id, 'value' => $row->value];
      }
      else if ($row->code == 'GAME_GENERATION') {
        $listGameGeneration[$key] = ['id' => $row->value_id, 'value' => $row->value];
      }
      else if ($row->code == 'GAME_LANGUAGE') {
        $listGameLanguage[$key] = ['id' => $row->value_id, 'value' => $row->value];
      }
      else if ($row->code == 'GAME_LOCATION') {
        $listGameLocation[$key] = ['id' => $row->value_id, 'value' => $row->value];
      }
      else if ($row->code == 'GAME_SUPPORT') {
        $listGameSupport[$key] = ['id' => $row->value_id, 'value' => $row->value];
      }
		}

    $listModel = AppOrgCategory::where('is_enabled','Y')
      ->orderBy(DB::raw('(case when parent_id = 0 THEN concat(id,"-",parent_id) ELSE concat(parent_id,"-",id) END)'))
      ->get();

    foreach($listModel as $key => $row) {
      $listCategory[$key] = ['id' => $row->id, 'value' => strlen($row->category_text) > 0 ? $row->category_text : $row->name,'attr' => 'data-children="'.$row->children.'"'];
    }

		$this->viewData['model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
      'name' 						      =>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
      'name_en' 						  =>  ['type' => 'input-text', 'attr' => ' maxlength=50 ','label' => trans('org.col_name_en')],
      'release_date' 				  =>  ['type' => 'input-datetime', 'attr' => 'data-date-format="YYYY-MM-DD" maxlength=50 ', 'label' => trans('org.col_release_date')],
      'prod_category_id' 			=>  ['type' => 'select',  'select_values' => $listCategory,       'class' => 'br-select2','label' => trans('org.col_prod_category_id')],
      'game_category' 		    =>  ['type' => 'select',  'select_values' => $listGameCategory,   'class' => 'br-select2','label' => trans('org.col_game_category')],
      'game_location' 		    =>  ['type' => 'select',  'select_values' => $listGameLocation,   'class' => 'br-select2','label' => trans('org.col_game_location')],
      'game_generation' 		  =>  ['type' => 'select',  'select_values' => $listGameGeneration, 'class' => 'br-select2','label' => trans('org.col_game_generation')],
      'language' 		          =>  ['type' => 'select',  'select_values' => $listGameLanguage, 'class' => 'br-select2','label' => trans('org.col_game_language')],
      'box_language' 		          =>  ['type' => 'select',  'select_values' => $listGameLanguage, 'class' => 'br-select2','label' => trans('Box language')],
      'media' 		            =>  ['type' => 'select',  'select_values' => $listGameSupport, 'class' => 'br-select2','label' => trans('org.col_game_support')],
      'platform' 							=>  ['type' => 'select',  'select_values' => $listPlatform, 'class' => 'br-select2', 'label' => trans('org.col_platform')],
      'region' 							  =>  ['type' => 'select',  'select_values' => $listRegion, 'class' => 'br-select2', 'label' => trans('org.col_region')],
      'ean_upc' 						  =>  ['type' => 'input-text', 'attr' => ' maxlength=50  ', 'label' => trans('org.col_ean_upc')],
      
      //'measures' 						  =>  ['type' => 'input-text', 'attr' => ' maxlength=50 data-mask="9{1,3}x9{1,3}x9{1,3}"', 'label' => trans('org.col_measures')],
      'volume' 			          =>  ['type' => 'input-number', 'attr' => ' data-number="4" step=any', 'label' => trans('org.col_volume')],
      'weight' 			          =>  ['type' => 'input-number', 'attr' => ' data-number="4" step=any', 'label' => trans('org.col_weight')],
      
      'comments' 							=>  ['type' => 'textarea', 'attr' => ' rows="4" ' , 'label' => trans('org.col_comments')],
      'is_enabled' 						=>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ], 'class' => 'br-select2' ],
    ];

    $this->viewData['sub_model_data'] = [
      'file_type'   => 'image',
      'column_name' => 'image_path',
      'upload_url'  => url('11w5Cj9WDAjnzlg0/marketplace/products/upload_file'),
      'img_class'   => 'profile-user-image',
      'img_path'    => url('images'),
      'img_qty'     => 4
    ];

		return view('brcode.marketplace.product')->with('viewData',$this->viewData);
  }

  /**
	 * Save a product
	 *
	 * @return redirect('back if fails | marketplace/products')
	 */
  public function saveProduct($id = 0) {
    $colPrefix = $this->columnPrefix;

		$input = Request::all();
		//dd($input);

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		$rules = [
			$colPrefix.'name'                => 'required|min:3|max:50',
			$colPrefix.'release_date'        => 'required',
			$colPrefix.'prod_category_id'    => 'required',
			$colPrefix.'platform'            => 'required',
			$colPrefix.'region'              => 'required',
		];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {

			$id = saveModel($id, $model, 'App\\AppOrgProduct',$subModelExists, $input, $colPrefix, $new);

      saveSubModelNew($new, $id, 'product_id', 'App\\AppOrgProduct', 'images', 'image_path', 'App\\AppOrgProductImage'
        ,$colPrefix, $input,'0',[],[],'pro_img','product-images');

			$message = trans('org.product') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/products')->with('message',$message)->with('messageType','success');

		}
  }

  /**
	 * Delete a product
	 *
	 * @return redirect('marketplace/products')
	 */
	public function deleteProduct() {
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		if($id > 0) {

			$product = AppOrgProduct::find($id);
      $product->modified_by = Auth::id();
			$product->deleted_at = date('Y-m-d H:i:s');
      $product->save();
			$message = trans('org.product') . ' ' . trans('app.deleted_successfully');

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/products')->with('message',$message)->with('messageType','success');
		}

	}

  /**
	 * AJAX function to upload an image
	 *
	 * @return json
	 */
  public function imageProduct($id = 0) {
    //$inputName = 'files';

    //$file = Request::file($inputName);
    error_log($id);
    $product = AppOrgProduct::find($id);
    $images = $product->images;
    if ($id > 0) {
        // resizing an uploaded file
        //Image::make($destinationPath . $filename)->resize(200, 200)->save($destinationPath . $filename200);
        //return Response::json(array('error' => 1, 'message' => 'error'), 400);
        return  Response::json(array('message' => 'success', 'images' => $images, 'quant' => $images->count()), 200);
    } else {
        return Response::json(array('error' => 1, 'message' => 'error'), 400);
    }
    
  }

  /**
	 * AJAX function to upload an image
	 *
	 * @return json
	 */
  public function saveImagesProduct($id = 0) {
    $colPrefix = $this->columnPrefix;

    $input = Request::all();
    $id = $input['model_id'] > 0 ? $input['model_id'] : 0;
    
    //error_log($id);
    
    if($id > 0){
      saveSubModelNew(false, $id, 'product_id', 'App\\AppOrgProduct', 'images', 'image_path', 'App\\AppOrgProductImage'
        ,$colPrefix, $input,'0',[],[],'pro_img','product-images');
    }
    
    $message = trans('org.product') ;

    return redirect('/11w5Cj9WDAjnzlg0/marketplace/products')->with('message',$message)->with('messageType','success');
    /*
    $product = AppOrgProduct::find($id);
    $images = $product->images;
    if ($id > 0) {
        // resizing an uploaded file
        //Image::make($destinationPath . $filename)->resize(200, 200)->save($destinationPath . $filename200);
        //return Response::json(array('error' => 1, 'message' => 'error'), 400);
        return  Response::json(array('message' => 'success', 'images' => $images, 'quant' => $images->count()), 200);
    } else {
        return Response::json(array('error' => 1, 'message' => 'error'), 400);
    }
    */
    
  }

  /**
	 * Upload the product's image
	 *
	 * @return json response
	 */
	public function uploadProductImage() {
    
    $inputName = 'files';
    
    $file = Request::file($inputName);
    
		if($file) {

        $destinationPath = public_path() . '/images/';
        error_log('aqui');
				$extension = $file[0]->getClientOriginalExtension();
				$filename = md5(microtime()) . '.' . $extension;

        // Prepare dir
        //$dateFolder = date('Y') . '/' . date('m') . '/' . date('d') . '/';

        $directory = $destinationPath;

        $filename200 = 'pro_img_' . Auth::id() . '_'.microtime(true).'.' . $extension;
        
        $upload_success = Request::file($inputName)[0]->move($directory, $filename);

        if ($upload_success) {
            // resizing an uploaded file
            //Image::make($destinationPath . $filename)->resize(200, 200)->save($destinationPath . $filename200);

            return Response::json(array('message' => 'success', 'name' => $filename), 200);
        } else {
            return Response::json(array('mesage' => 'error'), 400);
        }
    }

	}

  public function removeProductImage() {
    $fileName = Request::input('img');

    AppOrgProductImage::where('image_path', $fileName)->delete();

    if(file_exists(public_path() . '/images/' . $fileName)) {
      unlink(public_path() . '/images/' . $fileName);
      
      return Response::json(array('message' => 'success'), 200);
    }

    return Response::json(array('message' => 'error', 'reason' => 'Unknown file'), 200);
  }

  /*
	|--------------------------------------------------------------------------
	| Shipping
	|--------------------------------------------------------------------------
	|
	*/

  public function showShipping() {

    $this->viewData['form_title'] = trans('org.shipping');
		$this->viewData['form_url_dt'] = url('/11w5Cj9WDAjnzlg0/marketplace/inventory/list-dt');
		$this->viewData['table_cols'] = ['app.col_name', 'app.col_certified', 'app.col_max_value','app.col_max_weight', 'app.col_volume', 'app.col_stamp_price', 'app.col_price', 'app.action'];
    return view('brcode.marketplace.shippings')->with('viewData', $this->viewData);
  }
  
  public function listShipping() {
    if(Request::ajax()) {
			$data = SysShipping::whereNull('deleted_at')->get();
			$datos = [];
			foreach($data as $key){
				$mapArray = (object) array(
					"d0" => $key->name  . ( $key->type == 'Carta' ? ' (' . ($key->certified == 'Yes'? 'Certificada)': 'Ordinaria)') : ''),
					"d1" => $key->certified,
					"d2" => number_format($key->max_value,2,'.','') . ' €',
					"d3" => $key->max_weight,
					"d4" => 'L ' . $key->large . ' x W ' . $key->width . ' x H ' . $key->hight .'mm',
					"d5" => number_format($key->stamp_price,2,'.','') . ' €',
					"d6" => number_format($key->price,2,'.','') . ' €',
					"d7" => $key->id,
				);
				array_push($datos, $mapArray);
			}
			return response()->json([
				'data' => $datos, 
			]);
		}
  }

  public function AddShowShipping() {
    $countries = SysCountry::all();
    return view('brcode.marketplace.shipping')->with(compact('countries'));
  }

  public function storeShipping(BaseRequest $request) {

    //$request = Request::all();
    $this->validate($request, SysShipping::$rules, SysShipping::$message);

    $shipping = SysShipping::create($request->all());
    if($request->input('type') == 'PA'){
      $shipping->price = $shipping->stamp_price + 1;
    }
    if($request->input('type') == 'CA'){
      $shipping->price = $shipping->stamp_price + 0.5;
    }

    $shipping->volume = ($shipping->hight * $shipping->large * $shipping->width)/6000;
    
    $shipping->save();
    $message = 'Shipping' . ' ' . trans('app.created_successfully');
    return redirect('/11w5Cj9WDAjnzlg0/marketplace/shipping')->with('message',$message)->with('messageType','success');

 }

  public function updateShipping($id) {
    $shipping = SysShipping::find($id);
    $countries = SysCountry::all();
    return view('brcode.marketplace.shippinge')->with(compact('shipping','countries'));
  }

  public function saveShipping(BaseRequest $request, $id) {

    //$request = Request::all();
    $this->validate($request, SysShipping::$rules, SysShipping::$message);

    $shipping = SysShipping::find($id);

    $shipping->name = $request->input('name');
    $shipping->certified = $request->input('certified');
    $shipping->type = $request->input('type');
    $shipping->large = $request->input('large');
    $shipping->width = $request->input('width');
    $shipping->hight = $request->input('hight');
    $shipping->max_weight = $request->input('max_weight');
    $shipping->max_value = $request->input('max_value');
    $shipping->stamp_price = $request->input('stamp_price');
    $shipping->save();
    if($request->input('type') == 'PA'){
      $shipping->price = $shipping->stamp_price + 1;
    }
    if($request->input('type') == 'CA'){
      $shipping->price = $shipping->stamp_price + 0.5;
    }
    $shipping->volume = ($shipping->hight * $shipping->large * $shipping->width)/6000;
    $shipping->save();
    $message = 'Shipping' . ' ' . trans('app.modified_successfully');
    return redirect('/11w5Cj9WDAjnzlg0/marketplace/shipping')->with('message',$message)->with('messageType','success');

 }

  public function deleteShipping() {
    $input = Request::all();
    $shipping = SysShipping::find($input['id']);
    $shipping->modified_by = Auth::id();
    $shipping->deleted_at = date('Y-m-d H:i:s');
    $shipping->save();
    $message = 'Shipping' . ' ' . trans('app.deleted_successfully');
    return redirect('/11w5Cj9WDAjnzlg0/marketplace/shipping')->with('message',$message)->with('messageType','success');

  }
 

  /*
	|--------------------------------------------------------------------------
	| Inventory
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view that list the users' inventory in: /marketplace/inventory
	 *
	 * @return view
	 */
  public function showInventory() {
    $this->viewData['form_title'] = trans('org.inventory');
		$this->viewData['form_url_dt'] = url('/11w5Cj9WDAjnzlg0/marketplace/inventory/list-dt');
		$this->viewData['table_cols'] = ['app.col_id', 'app.col_user_name', 'org.col_product_name','org.col_quantity', 'org.col_items_sold'];

    return view('brcode.marketplace.inventory')->with('viewData', $this->viewData);
  }

  public function showInventoryEan() {

    if(Request::ajax()) {
      $status = false;
      // error_log(Request::get('ean'));
      // error_log(Request::get('id'));
      if(Request::get('eancheck')){

        if(\App\AppOrgUserInventory::where('id', Request::get('eancheck'))->first()){
          $status = true;
          error_log('asdasd');
          \App\AppOrgUserInventory::where('id', Request::get('eancheck'))->update(['is_ean_enabled' => 'Y']);
        }
        return ['status' => $status];
      }

      if(\App\AppOrgUserInventory::where('id', Request::get('id'))->first()){
        $status = true;
        \App\AppOrgUserInventory::where('id', Request::get('id'))->update(['ean' => Request::get('ean')]);
      }
      return ['status' => $status];
    }

    $this->viewData['form_title'] = trans('org.inventory');
		$this->viewData['form_url_dt'] = url('/11w5Cj9WDAjnzlg0/marketplace/inventory/list-dt');
		$this->viewData['table_cols'] = ['app.col_id', 'app.col_user_name', 'org.col_product_name','org.col_quantity', 'org.col_items_sold'];

    $inventoriesImg = \App\AppOrgUserInventoryEan::pluck('inventory_id');
    
    $inventories = \App\AppOrgUserInventory::whereIn('id', $inventoriesImg)->where('is_ean_enabled', 'N')->whereNull('deleted_at')->get();
    
    return view('brcode.marketplace.inventory_ean')->with('viewData', $this->viewData)->with('inventories', $inventories);
  }

  /**
	 * Create a DataTable output for inventory' table in: /marketplace/inventory
	 *
	 * @return DataTable
	 */
  public function dtInventory() {
    $inventory = DB::table('appOrgUserInventories AS i')
    ->select(['i.id', 'u.user_name', 'p.name',
      'i.quantity','i.quantity_sold'])
    ->join('sysUsers AS u','u.id','=','i.user_id')
    ->join('appOrgProducts AS p','p.id','=','i.product_id')
    ->whereNull('i.deleted_at');
    
    return Datatables::of($inventory)
			// ->addColumn('action', function ($inventory) {
      //           return '<a href="' . url('/marketplace/modify_inventory/' .$inventory->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
      //       })
			->make();
  }



  /*
	|--------------------------------------------------------------------------
	| Orders
	|--------------------------------------------------------------------------
	|
	*/
  public function showOrders() {
    $input = Request::all();

    $this->viewData['form_title'] = trans('app.orders');
    $this->viewData['form_url_add'] = url('/11w5Cj9WDAjnzlg0/marketplace/orders/add');
    
    if(isset($input['d1'])){
      if($input['d1'] != '')
        $orders = AppOrgOrder::where('order_identification', $input['d1'])->whereIn('status', ['CR', 'PD', 'ST', 'DD', 'RT', 'RP', 'CN', 'CW', 'ON'])->orderBy('updated_at', 'desc')->paginate(25);
      
      if($input['d2'] != ''){
        $user_id = SysUser::withTrashed()->where('user_name', $input['d2'])->first();
        $orders = AppOrgOrder::where('buyer_user_id', $user_id->id)->whereIn('status', ['CR', 'PD', 'ST', 'DD', 'RT', 'RP', 'CN', 'CW', 'ON'])->orderBy('updated_at', 'desc')->paginate(25);
      }
      if($input['d3'] != ''){
        $user_id = SysUser::withTrashed()->where('user_name', $input['d3'])->first();
        $orders = AppOrgOrder::where('seller_user_id', $user_id->id)->whereIn('status', ['CR', 'PD', 'ST', 'DD', 'RT', 'RP', 'CN', 'CW', 'ON'])->orderBy('updated_at', 'desc')->paginate(25);
      }
      if($input['d4'] != ''){
        $orders = AppOrgOrder::where('status', $input['d4'])->orderBy('updated_at', 'desc')->paginate(25);
      }
      $id = AppOrgOrder::select('order_identification')->whereIn('status', ['CR', 'PD', 'ST', 'DD', 'RT', 'RP', 'CN', 'CW', 'ON'])->orderBy('updated_at', 'desc')->get();
      $buyer = AppOrgOrder::whereIn('status', ['CR', 'PD', 'ST', 'DD', 'RT', 'RP', 'CN', 'CW', 'ON'])->groupBy('buyer_user_id')->get();
      $seller = AppOrgOrder::whereIn('status', ['CR', 'PD', 'ST', 'DD', 'RT', 'RP', 'CN', 'CW', 'ON'])->groupBy('seller_user_id')->get();
      $status = AppOrgOrder::whereIn('status', ['CR', 'PD', 'ST', 'DD', 'RT', 'RP', 'CN', 'CW', 'ON'])->groupBy('status')->get();

      return view('brcode.marketplace.orders')->with('viewData', $this->viewData)->with('orders',$orders)->with('ids',$id)->with('buyers',$buyer)->with('sellers',$seller)->with('status', $status)->with('d1',$input['d1'])->with('d2',$input['d2'])->with('d3',$input['d3'])->with('d4',$input['d4']);
    }
    
		$orders = AppOrgOrder::whereIn('status', ['CR', 'PD', 'ST', 'DD', 'RT', 'RP', 'CN', 'CW', 'ON'])->orderBy('updated_at', 'desc')->paginate(25);
		$id = AppOrgOrder::select('order_identification')->whereIn('status', ['CR', 'PD', 'ST', 'DD', 'RT', 'RP', 'CN', 'CW', 'ON'])->orderBy('updated_at', 'desc')->get();
    $buyer = AppOrgOrder::whereIn('status', ['CR', 'PD', 'ST', 'DD', 'RT', 'RP', 'CN', 'CW', 'ON'])->groupBy('buyer_user_id')->get();
    $seller = AppOrgOrder::whereIn('status', ['CR', 'PD', 'ST', 'DD', 'RT', 'RP', 'CN', 'CW', 'ON'])->groupBy('seller_user_id')->get();
    $status = AppOrgOrder::whereIn('status', ['CR', 'PD', 'ST', 'DD', 'RT', 'RP', 'CN', 'CW', 'ON'])->groupBy('status')->get();
    return view('brcode.marketplace.orders')->with('viewData', $this->viewData)->with('orders',$orders)->with('ids',$id)->with('buyers',$buyer)->with('sellers',$seller)->with('status', $status);
  }

  public function showOrdersNot() {
      
    $this->viewData['form_title'] = trans('app.orders');
    $this->viewData['form_url_add'] = url('/11w5Cj9WDAjnzlg0/marketplace/orders/add');
    $orders = AppOrgOrder::whereIn('shipping_status', ['Yes', 'No'])->paginate(25);
    return view('brcode.marketplace.ordersNot')->with('viewData', $this->viewData)->with('orders',$orders);
  }

  public function requestGestion(BaseRequest $request){
    $tab = '05'; 

    $order = AppOrgOrder::where('order_identification', $request->order)->first();
    if($order){
      $date = date('Y-m-d H:i:s');
      $order->management_date = date('Y-m-d H:i:s'); 
      $order->expiration_date = date('Y-m-d H:i:s', strtotime('+14 days'));
      $order->save();

      return back()->with('tab', $tab);
    }else{
      return back();
    }
  }

  public function requestSolucion(BaseRequest $request){
    $tab = '05'; 

    $order = AppOrgOrder::where('order_identification', $request->order)->first();
    
    if($order){

      $date = date('Y-m-d H:i:s');
      $order->solution_date = date('Y-m-d H:i:s'); 
      $order->save();

      return back()->with('tab', $tab);
    }else{
      return back();
    }
  }
  /*
	|--------------------------------------------------------------------------
	| Messages
	|--------------------------------------------------------------------------
	|
	*/

  /*
	|--------------------------------------------------------------------------
	| Front End - Pages
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view that list products in: /marketplace/pages
	 *
	 * @return view
	 */
  public function showPages() {

    $this->viewData['form_title'] = trans('app.pages');
		$this->viewData['form_url_add'] = url('/11w5Cj9WDAjnzlg0/marketplace/pages/add');
		$this->viewData['form_url_dt'] = url('/11w5Cj9WDAjnzlg0/marketplace/pages/list-dt');
		$this->viewData['table_cols'] = ['app.col_id', 'org.col_title','org.col_url', 'org.col_parent_id', 'org.col_show_title', 'app.col_is_enabled'];

    return view('brcode.marketplace.pages')->with('viewData', $this->viewData);
  }

  /**
	 * Create a DataTable output for product' table in: /marketplace/products
	 *
	 * @return DataTable
	 */
  public function dtPages() {
    $pages = DB::table(DB::raw('appOrgPages AS p'))
    ->select(['p.id', 'p.title', 'p.url',  DB::raw('IFNULL(p2.title,"") as parent'), 'p.show_title', 'p.is_enabled'
    ])
    ->leftJoin('appOrgPages AS p2', 'p2.id', '=', 'p.parent_id')
    ->whereNull('p.deleted_at');
    return Datatables::of($pages)
			->addColumn('action', function ($pages) {
                return '<a href="' . url('/11w5Cj9WDAjnzlg0/marketplace/pages/modify/' .$pages->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
            })
			->make(true);
  }

  /**
	 * Show the view for a product: /marketplace/add_product
   * and marketplace/modify_product
	 *
	 * @return view
	 */
  public function showPage($id = 0) {
    $this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('org.page');
		$this->viewData['form_list_title'] = trans('org.pages');
		$this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/marketplace/pages');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/pages/save');
		$this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/marketplace/pages/delete');
    $this->viewData['model_id'] = 0;

		if($id > 0) {

			$this->viewData['model'] = AppOrgPage::find($id);
			$this->viewData['model'] = $this->viewData['model']->toArray();
			$this->viewData['model_id'] = $id;
      $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/pages/modify');
		}

    $listModel = SysDictionary::whereIn('code',['PAGE_CATEGORY'])->orderBy('code')->orderBy('value')->get();
    $listCategory   = [];

		foreach($listModel as $key => $row) {
      $listCategory[$key] = ['id' => $row->value_id, 'value' => $row->value];
		}

		$this->viewData['model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
      'title' 						      =>  ['type' => 'input-text', 'attr' => ' maxlength=50 ', 'label' => trans('org.col_title')],
      'url' 				            =>  ['type' => 'input-slug', 'label' => trans('org.col_url'), 'attr' => ' data-url-from="'.$this->columnPrefix.'title" '],
      'parent_id' 					    =>  ['type' => 'select',  'select_values' => $listCategory, 'attr' => ' maxlength=50 ', 'class' => 'br-select2'],
      'content'                 =>  ['type' => 'html-editor', 'label' => trans('org.col_content'), 'class' => 'br-html-editor', 'attr' => ' rows="10" '],
      'show_title' 					  	=>  ['type' => 'select', 'label' => trans('org.col_show_title'), 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ], 'class' => 'br-select2' ],
      'is_enabled'              =>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ], 'class' => 'br-select2' ],
    ];

		return view('brcode.marketplace.page')->with('viewData',$this->viewData);
  }

  /**
	 * Save a product
	 *
	 * @return redirect('back if fails | marketplace/pages')
	 */
  public function savePage($id = 0) {
    $colPrefix = $this->columnPrefix;

		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		$rules = [
			$colPrefix.'title' 			  => 'required|min:3|max:50|unique:appOrgPages,title' . ($id > 0 ? ','.$id : ''),
			$colPrefix.'url'          => 'required',
			$colPrefix.'content' 	    => 'required',
			$colPrefix.'show_title' 	=> 'required',
			$colPrefix.'is_enabled' 	=> 'required',
		];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {

			$id = saveModel($id, $model, 'App\\AppOrgPage',$subModelExists, $input, $colPrefix, $new);

			$message = trans('org.product') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/pages')->with('message',$message)->with('messageType','success');

		}
  }

  /**
	 * Delete a product
	 *
	 * @return redirect('marketplace/products')
	 */
	public function deletePage() {
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		if($id > 0) {

			$product = AppOrgPage::find($id);
      $product->modified_by = Auth::id();
			$product->deleted_at = date('Y-m-d H:i:s');
      $product->save();
			$message = trans('org.page') . ' ' . trans('app.deleted_successfully');

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/pages')->with('message',$message)->with('messageType','success');
		}

	}

  /*
	|--------------------------------------------------------------------------
	| Front End - Footer Links
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view that list footer links in: /marketplace/links
	 *
	 * @return view
	 */
  public function showLinks() {

    $this->viewData['form_title'] = trans('app.footer_links');
		$this->viewData['form_url_add'] = url('/11w5Cj9WDAjnzlg0/marketplace/links/add');
		$this->viewData['form_url_dt'] = url('/11w5Cj9WDAjnzlg0/marketplace/links/list-dt');
		$this->viewData['table_cols'] = ['app.col_id', 'app.col_name','org.col_column', 'org.col_url','app.col_is_enabled'];

    return view('brcode.marketplace.flinks')->with('viewData', $this->viewData);
  }

  /**
	 * Create a DataTable output for footer links' table in: /marketplace/links
	 *
	 * @return DataTable
	 */
  public function dtLinks() {
    $links = DB::table(DB::raw('appOrgFooterLinks AS l'))
    ->select(['l.id', 'l.name', 'c.value AS column','l.url',
    DB::raw('(CASE WHEN l.is_enabled = "Y" THEN "Yes" ELSE "No" END) as is_enabled')
    ])
    ->leftJoin('sysDictionaries AS c', 'c.value_id', '=', 'l.column')
    ->whereNull('l.deleted_at');
    return Datatables::of($links)
			->addColumn('action', function ($links) {
                return '<a href="' . url('/11w5Cj9WDAjnzlg0/marketplace/links/modify/' .$links->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
            })
			->make(true);
  }

  /**
	 * Show the view for a footer link: /marketplace/add_link
   * and marketplace/modify_link
	 *
	 * @return view
	 */
  public function showLink($id = 0) {
    $this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('org.link');
		$this->viewData['form_list_title'] = trans('app.links');
		$this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/marketplace/links');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/links/save');
		$this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/marketplace/links/delete');
    $this->viewData['model_id'] = 0;

		if($id > 0) {
      $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/links/modify');
			$this->viewData['model'] = AppOrgFooterLink::find($id);
			$this->viewData['model'] = $this->viewData['model']->toArray();
			$this->viewData['model_id'] = $id;

		}

    $listModel = SysDictionary::whereIn('code',['FOOTER_COL'])->orderBy('code')->orderBy('value')->get();
    $listColumns   = [];

		foreach($listModel as $key => $row) {
      $listColumns[$key] = ['id' => $row->value_id, 'value' => $row->value];
		}

		$this->viewData['model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
      'name' 						        =>  ['type' => 'input-text', 'attr' => ' maxlength=50 ', 'label' => trans('org.col_title')],
      'url' 				            =>  ['type' => 'input-url', 'label' => trans('org.col_url'), ],
      'column' 					        =>  ['type' => 'select',  'select_values' => $listColumns, 'attr' => ' maxlength=50 ', 'class' => 'br-select2', 'label' => trans('org.col_column')],
      'is_enabled'              =>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ], 'class' => 'br-select2' ],
    ];

		return view('brcode.marketplace.flink')->with('viewData',$this->viewData);
  }

  /**
	 * Save a footer link
	 *
	 * @return redirect('back if fails | marketplace/links')
	 */
  public function saveLink($id = 0) {
    $colPrefix = $this->columnPrefix;

		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		$rules = [
			$colPrefix.'name' 			  => 'required|min:3|max:50|unique:appOrgPages,title' . ($id > 0 ? ','.$id : ''),
			$colPrefix.'url'          => 'required',
			$colPrefix.'column' 	    => 'required',
			$colPrefix.'is_enabled' 	=> 'required',
		];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {

			$id = saveModel($id, $model, 'App\\AppOrgFooterLink',$subModelExists, $input, $colPrefix, $new);

			$message = trans('org.link') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/links')->with('message',$message)->with('messageType','success');

		}
  }

  /**
	 * Delete a footer link
	 *
	 * @return redirect('marketplace/links')
	 */
	public function deleteLink() {
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		if($id > 0) {

			$product = AppOrgFooterLink::find($id);
      $product->modified_by = Auth::id();
			$product->deleted_at = date('Y-m-d H:i:s');
      $product->save();
			$message = trans('org.link') . ' ' . trans('app.deleted_successfully');

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/links')->with('message',$message)->with('messageType','success');
		}

	}

  /*
	|--------------------------------------------------------------------------
	| Front End - FAQs
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view that list faq in: /marketplace/faqs
	 *
	 * @return view
	 */
  public function showFaqs() {

    $this->viewData['form_title'] = trans('app.faqs');
		$this->viewData['form_url_add'] = url('/11w5Cj9WDAjnzlg0/marketplace/faqs/add');
		$this->viewData['form_url_dt'] = url('/11w5Cj9WDAjnzlg0/marketplace/faqs/list-dt');
		$this->viewData['table_cols'] = ['app.col_id', 'org.col_question','org.col_answer','app.col_is_enabled'];

    return view('brcode.marketplace.faqs')->with('viewData', $this->viewData);
  }

  /**
	 * Create a DataTable output for faq table in: /marketplace/faqs
	 *
	 * @return DataTable
	 */
  public function dtFaqs() {
    $faqs = DB::table(DB::raw('appOrgFaqs AS f'))
    ->select(['f.id', 'f.question', 'f.answer',
    DB::raw('(CASE WHEN f.is_enabled = "Y" THEN "Yes" ELSE "No" END) as is_enabled')
    ])
    ->whereNull('f.deleted_at');
    return Datatables::of($faqs)
			->addColumn('action', function ($faqs) {
                return '<a href="' . url('/11w5Cj9WDAjnzlg0/marketplace/faqs/modify/' .$faqs->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
            })
			->make(true);
  }

  /**
	 * Show the view for a footer link: /marketplace/add_link
   * and marketplace/modify_link
	 *
	 * @return view
	 */
  public function showFaq($id = 0) {
    $this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('org.faq');
		$this->viewData['form_list_title'] = trans('app.faqs');
		$this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/marketplace/faqs');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/faqs/save');
		$this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/marketplace/faqs/delete');
    $this->viewData['model_id'] = 0;

		if($id > 0) {
      $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/faqs/modify');
			$this->viewData['model'] = AppOrgFaq::find($id);
			$this->viewData['model'] = $this->viewData['model']->toArray();
			$this->viewData['model_id'] = $id;

		}

		$this->viewData['model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
      'question' 						    =>  ['type' => 'input-text', 'attr' => ' maxlength=255 ', 'label' => trans('org.col_question')],
      'answer' 				          =>  ['type' => 'html-editor', 'class' => 'br-html-editor', 'attr' => ' rows="10" ', 'label' => trans('org.col_answer'), ],
      'is_enabled'              =>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ], 'class' => 'br-select2' ],
    ];

		return view('brcode.marketplace.faq')->with('viewData',$this->viewData);
  }

  /**
	 * Save a footer link
	 *
	 * @return redirect('back if fails | marketplace/links')
	 */
  public function saveFaq($id = 0) {
    $colPrefix = $this->columnPrefix;

		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		$rules = [
			$colPrefix.'question' 			  => 'required|min:3|max:50|unique:appOrgPages,title' . ($id > 0 ? ','.$id : ''),
			$colPrefix.'answer'           => 'required',
			$colPrefix.'is_enabled' 	    => 'required',
		];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {

			$id = saveModel($id, $model, 'App\\AppOrgFaq',$subModelExists, $input, $colPrefix, $new);

			$message = trans('org.faq') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/faqs')->with('message',$message)->with('messageType','success');

		}
  }

  /**
	 * Delete a footer link
	 *
	 * @return redirect('marketplace/links')
	 */
	public function deleteFaq() {
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		if($id > 0) {

			$product = AppOrgFaq::find($id);
      $product->modified_by = Auth::id();
			$product->deleted_at = date('Y-m-d H:i:s');
      $product->save();
			$message = trans('org.faq') . ' ' . trans('app.deleted_successfully');

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/faqs')->with('message',$message)->with('messageType','success');
		}

	}

  /*
	|--------------------------------------------------------------------------
	| Front End - Carousels
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view that list carousels in: /marketplace/carousels
	 *
	 * @return view
	 */
  public function showCarousels() {

    $this->viewData['form_title'] = trans('app.carousels');
		$this->viewData['form_url_add'] = url('/11w5Cj9WDAjnzlg0/marketplace/carousels/add');
		$this->viewData['form_url_dt'] = url('/11w5Cj9WDAjnzlg0/marketplace/carousels/list-dt');
		$this->viewData['table_cols'] = ['app.col_id', 'app.col_name','org.col_page_location','org.col_in_use','org.col_images','app.col_is_enabled'];

    return view('brcode.marketplace.carousels')->with('viewData', $this->viewData);
  }

  /**
	 * Create a DataTable output for carousel table in: /marketplace/carousels
	 *
	 * @return DataTable
	 */
  public function dtCarousels() {
    $carousels = DB::table(DB::raw('appOrgCarousels AS c'))
    ->select(['c.id', 'c.name', 'c.location','c.in_use',
    DB::raw('(SELECT COUNT(1) FROM appOrgCarouselItems i WHERE i.carousel_id = c.id) AS images'),
    DB::raw('(CASE WHEN c.is_enabled = "Y" THEN "Yes" ELSE "No" END) as is_enabled')
    ])
    ->whereNull('c.deleted_at');
    return Datatables::of($carousels)
			->addColumn('action', function ($carousels) {
                return '<a href="' . url('/11w5Cj9WDAjnzlg0/marketplace/carousels/modify/' .$carousels->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
            })
			->make(true);
  }

  /**
	 * Show the view for a carousel: /marketplace/carousels/add
   * and marketplace/carousels/modify
	 *
	 * @return view
	 */
  public function showCarousel($id = 0) {
    $this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('org.carousel');
		$this->viewData['form_list_title'] = trans('app.carousels');
		$this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/marketplace/carousels');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/carousels/save');
		$this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/marketplace/carousels/delete');
    $this->viewData['model_id'] = 0;

    $subModel1 = [
			'sub_model_title' => trans('org.carousel_images'),
			'sub_model_view' => 'table',
		];

		if($id > 0) {
      $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/carousels/modify');
			$this->viewData['model'] = AppOrgCarousel::find($id);

			$this->viewData['model_id'] = $id;

      // Load subModels
      $subModel1['sub_model'] = $this->viewData['model']->images->toArray();

      $this->viewData['model'] = $this->viewData['model']->toArray();
		}

    $listModel = SysDictionary::whereIn('code',['CAROUSEL_LOC','CAROUSEL_WIDTH','CAROUSEL_HEIGHT'])->orderBy('code')->orderBy('value')->get();
		$listLocation   = [];
    $listWidth      = [];
    $listHeight     = [];

		foreach($listModel as $key => $row) {

      if($row->code == 'CAROUSEL_LOC') {
        $listLocation[$key] = ['id' => $row->value_id, 'value' => $row->value];
      }
      else if ($row->code == 'CAROUSEL_WIDTH') {
        $listWidth[$key] = ['id' => $row->value_id, 'value' => $row->value];
      }
      else if ($row->code == 'CAROUSEL_HEIGHT') {
        $listHeight[$key] = ['id' => $row->value_id, 'value' => $row->value];
      }
		}

		$this->viewData['model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
      'name' 				=>  ['type' => 'input-text', 'attr' => ' maxlength=255 '],
      'location'    =>  ['type' => 'select', 'select_values' => $listLocation, 'class' => 'br-select2','label' => trans('org.col_page_location') ],
      'width_type'  =>  ['type' => 'select', 'select_values' => $listWidth, 'class' => 'br-select2','label' => trans('org.col_car_width') ],
      'height_type'      =>  ['type' => 'select', 'select_values' => $listHeight, 'class' => 'br-select2','label' => trans('org.col_car_height') ],
      'is_enabled'  =>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ], 'class' => 'br-select2' ],
    ];

    // For submodel view = table
		$subModel1['sub_model_cols'] = [
			'id' 								=> 	['type' => 'hidden', 'attr' => ' '],
      'image_path'				=>	['type' => 'file-uploader', 'file-type' => 'image', 'is_array' => false,
                                'class' => 'image-uploader','attr' => 'data-url="'.url('11w5Cj9WDAjnzlg0/marketplace/carousels/upload_image').'"',
                                'img-class' => 'table-image', 'img-path' => 'uploads/carousels-images/', 'label' => trans('app.col_image')
                              ],
      'content_title' 		=> 	['type' => 'input-text', 'attr' => ' maxlength="50" ', 'label' => trans('org.col_content_title')],
      'content_text' 		  => 	['type' => 'input-text', 'attr' => ' maxlength="100" ', 'label' => trans('org.col_content_text')],
      'slide_order' 		  => 	['type' => 'input-number', 'attr' => ' maxlength="10" ', 'label' => trans('org.col_slide_order')],
      'slide_duration' 		=> 	['type' => 'input-number', 'attr' => ' maxlength="10" ', 'label' => trans('org.col_slide_duration')],
    ];

    $this->viewData['sub_models'] = array($subModel1);
    $this->viewData['sub_model_width'] = 'col-md-12';
		return view('brcode.marketplace.carousel')->with('viewData',$this->viewData);
  }

  /**
	 * Save a carousel
	 *
	 * @return redirect('back if fails | marketplace/carousels')
	 */
  public function saveCarousel($id = 0) {
    $colPrefix = $this->columnPrefix;

		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;
    $name = $input[$colPrefix.'name'];

		$rules = [
			$colPrefix.'name' 			      => 'required|min:3|max:50',
      $colPrefix.'location' 			=> [
        'required',
        'min:3',
        'max:10',
        Rule::unique('appOrgCarousels', 'location')->ignore($id)->where(function($query) use($name) {
          $query->where('name',$name);
        })
      ],
      $colPrefix.'width_type' 			=> 'required|min:3|max:50',
      $colPrefix.'height_type' 			=> 'required|min:3|max:50',
			$colPrefix.'is_enabled' 	    => 'required',
		];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {
      $new = $id == 0 ? true : false;

			$id = saveModel($id, $model, 'App\\AppOrgCarousel',$subModelExists, $input, $colPrefix, $new);

      $otherInputs = [
        'content_title',
        'content_text',
        'slide_order',
        'slide_duration',
      ];
      saveSubModel($new, $id, 'carousel_id', 'App\\AppOrgCarousel', 'images', 'image_path', 'App\\AppOrgCarouselItem', $colPrefix, $input, 0, [], $otherInputs,'carousel_image_','carousels-images');

			$message = trans('org.carousel') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/carousels')->with('message',$message)->with('messageType','success');

		}
  }

  /**
	 * Delete a carousel
	 *
	 * @return redirect('marketplace/carousels')
	 */
	public function deleteCarousel() {
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		if($id > 0) {

			$product = AppOrgCarousel::find($id);
      $product->modified_by = Auth::id();
			$product->deleted_at = date('Y-m-d H:i:s');
      $product->save();
			$message = trans('org.carousel') . ' ' . trans('app.deleted_successfully');

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/carousels')->with('message',$message)->with('messageType','success');
		}

	}

  /**
	 * Upload the image related to the carousel
	 *
	 * @return JSON response
	 */
  public function saveCarouselImage() {
    $inputName    = 'file_' . $this->columnPrefix . 'image_path';

		$file         = Request::file($inputName);
    $input        = Request::all();
    $modelId      = $input['model_id'];
    $subModelId   = $input['sub_model_id'];
    $imageIndex   = $input['image_index'];

		if($file) {
        $file = $file[0];
        $destinationPath = public_path() . '/uploads/carousels-images/';
        //$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				//$filename = md5(microtime()) . '.' . $extension;
        if($modelId > 0 && $subModelId > 0) {
          $filename = 'carousel_image_' . $modelId . '_' . $subModelId . '.' . $extension;
        }
        else {
          $filename = md5(microtime()) . '.' . $extension;
        }

        $upload_success = $file->move($destinationPath, $filename);

        if ($upload_success) {

            return Response::json(array('message' => 'success', 'name' => $filename), 200);
        } else {
            return Response::json(array('mesage' => 'error'), 400);
        }
    }
	}

  /*
	|--------------------------------------------------------------------------
	| Front End - News
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view that list banners in: /marketplace/banners
	 *
	 * @return view
	 */
  public function showBanners() {

    $this->viewData['form_title'] = trans('app.banners');
		$this->viewData['form_url_add'] = url('/11w5Cj9WDAjnzlg0/marketplace/banners/add');
		$this->viewData['form_url_dt'] = url('/11w5Cj9WDAjnzlg0/marketplace/banners/list-dt');
		$this->viewData['table_cols'] = ['app.col_id', 'app.col_name','org.col_page_location','app.col_image','org.col_priority','org.col_counter', 'app.col_is_enabled'];

    return view('brcode.marketplace.banners')->with('viewData', $this->viewData);

  }

  /**
	 * Create a DataTable output for banners table in: /marketplace/banners
	 *
	 * @return DataTable
	 */
  public function dtBanners() {
    $banners = DB::table(DB::raw('appOrgBanners AS b'))
    ->select(['b.id', 'b.name', 'b.image_path','b.counter','b.priority','b.location',
    DB::raw('(CASE WHEN b.is_enabled = "Y" THEN "Yes" ELSE "No" END) as is_enabled')
    ])
    ->whereNull('b.deleted_at');
    return Datatables::of($banners)
			->addColumn('action', function ($banners) {
                return '<a href="' . url('/11w5Cj9WDAjnzlg0/marketplace/banners/modify/' .$banners->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
            })
      ->addColumn('image_path',function($banners) {
        if(strlen($banners->image_path) > 0 ) {
          return '<img class="table-image" src="'.url('uploads/banners-images/'.$banners->image_path).'" />';
        }
        else {
          return '';
        }
      })
			->make(true);
  }

  /**
	 * Show the view for a news item: /marketplace/news/add
   * and marketplace/news/modify
	 *
	 * @return view
	 */
  public function showBanner($id = 0) {
    $this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('org.banner');
		$this->viewData['form_list_title'] = trans('app.banners');
		$this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/marketplace/banners');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/banners/save');
		$this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/marketplace/banners/delete');
    $this->viewData['model_id'] = 0;

		if($id > 0) {
      $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/banners/modify');
			$this->viewData['model'] = AppOrgBanner::find($id);
			$this->viewData['model'] = $this->viewData['model']->toArray();
			$this->viewData['model_id'] = $id;

		}

    $listPriorities = [
      ['id' => 1, 'value' => 1],
      ['id' => 2, 'value' => 2],
      ['id' => 3, 'value' => 3],
    ];

    $listModel = SysDictionary::whereIn('code',['BANNER_LOC'])->orderBy('code')->orderBy('order_by')->orderBy('value')->get();
		$listLocation   = [];

		foreach($listModel as $key => $row) {
        $listLocation[$key] = ['id' => $row->value_id, 'value' => $row->value];
		}

		$this->viewData['model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
      'name' 						        =>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
      'image_path'				      =>	['type' => 'file-uploader', 'file-type' => 'image', 'is_array' => false,
                                      'class' => 'image-uploader','attr' => 'data-url="'.url('11w5Cj9WDAjnzlg0/marketplace/banners/upload_image').'"',
                                      'img-class' => '', 'img-path' => 'uploads/banners-images/'],
      'priority'                =>  ['type' => 'select', 'select_values' => $listPriorities, 'class' => 'br-select2', 'label' => trans('org.col_priority')],
      'location'                =>  ['type' => 'select', 'select_values' => $listLocation, 'class' => 'br-select2','label' => trans('org.col_page_location') ],
      'is_enabled'              =>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ], 'class' => 'br-select2' ],
    ];

		return view('brcode.marketplace.banner')->with('viewData',$this->viewData);
  }

  /**
	 * Save a news item
	 *
	 * @return redirect('back if fails | marketplace/news')
	 */
  public function saveBanner($id = 0) {
    $colPrefix = $this->columnPrefix;

		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		$rules = [
			$colPrefix.'name' 			  => 'required|min:3|max:50|unique:appOrgBanners,name' . ($id > 0 ? ','.$id : ''),
			$colPrefix.'location'     => 'required',
			$colPrefix.'priority'     => 'required',
			$colPrefix.'is_enabled' 	=> 'required',
		];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {

      if($id > 0) {
        $banner = AppOrgBanner::find($id);
        if($banner->image_path && strlen($input[$colPrefix . 'image_path']) == 0) {
  				if(file_exists(public_path() . '/uploads/banners-images/' . $banner->image_path ) ) {
  					unlink(public_path() . '/uploads/banners-images/' . $banner->image_path);
  				}
  			}
      }

			$id = saveModel($id, $model, 'App\\AppOrgBanner',$subModelExists, $input, $colPrefix, $new);
      $saveModel = false;
      $bannerImage = isset($input[$colPrefix . 'image_path']) ? $input[$colPrefix . 'image_path'] : '';

      if(strlen($bannerImage) > 0 && strpos($bannerImage,'banner_image_') === false) {

        $ext = File::extension(public_path() . '/uploads/banners-images/' . $bannerImage);

        File::move(public_path() . '/uploads/banners-images/' . $bannerImage,
          public_path() . '/uploads/banners-images/banner_image_' . $model->id . '.' . $ext);

        $model->image_path = 'banner_image_' . $model->id . '.' . $ext;
        $model->save();
        $saveModel = true;
      }

      if($saveModel)
        $model->save();

			$message = trans('org.banner') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/banners')->with('message',$message)->with('messageType','success');

		}
  }

  /**
	 * Delete a news item
	 *
	 * @return redirect('marketplace/news')
	 */
	public function deleteBanner() {
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		if($id > 0) {

			$product = AppOrgBanner::find($id);
      $product->modified_by = Auth::id();
			$product->deleted_at = date('Y-m-d H:i:s');
      $product->save();
			$message = trans('org.banner') . ' ' . trans('app.deleted_successfully');

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/banners')->with('message',$message)->with('messageType','success');
		}

	}

  /**
	 * Upload the image related to the banner
	 *
	 * @return JSON response
	 */
  public function saveBannerImage() {
    $inputName = 'file_' . $this->columnPrefix . 'image_path';

		$file = Request::file($inputName);
    $input = Request::all();
    $modelId = $input['model_id'];

		if($file) {

        $destinationPath = public_path() . '/uploads/banners-images/';
        //$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				//$filename = md5(microtime()) . '.' . $extension;
        if($modelId > 0) {
          $filename = 'banner_image_' . $modelId . '.' . $extension;
        }
        else {
          $filename = md5(microtime()) . '.' . $extension;
        }

        $upload_success = Request::file($inputName)->move($destinationPath, $filename);

        if ($upload_success) {

            return Response::json(array('message' => 'success', 'name' => $filename), 200);
        } else {
            return Response::json(array('mesage' => 'error'), 400);
        }
    }
	}

  /*
	|--------------------------------------------------------------------------
	| Front End - News
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view that list news in: /marketplace/news
	 *
	 * @return view
	 */
  public function showNews() {

    $this->viewData['form_title'] = trans('app.news');
		$this->viewData['form_url_add'] = url('/11w5Cj9WDAjnzlg0/marketplace/news/add');
		$this->viewData['form_url_dt'] = url('/11w5Cj9WDAjnzlg0/marketplace/news/list-dt');
		$this->viewData['table_cols'] = ['app.col_id', 'org.col_title','org.col_source_url','app.col_image', 'app.col_is_enabled'];
    return view('brcode.marketplace.news')->with('viewData', $this->viewData);

  }

  /**
	 * Create a DataTable output for news table in: /marketplace/news
	 *
	 * @return DataTable
	 */
  public function dtNews() {
    $news = DB::table(DB::raw('appOrgNewsItems AS n'))
    ->select(['n.id', 'n.title', 'n.source_url','n.image_path',
    DB::raw('(CASE WHEN n.is_enabled = "Y" THEN "Yes" ELSE "No" END) as is_enabled')
    ])
    ->whereNull('n.deleted_at');
    return Datatables::of($news)
			->addColumn('action', function ($news) {
                return '<a href="' . url('/11w5Cj9WDAjnzlg0/marketplace/news/modify/' .$news->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
            })
      ->addColumn('image_path',function($news) {
        if(strlen($news->image_path) > 0 ) {
          return '<img class="table-image" src="'.url('uploads/news-images/'.$news->image_path).'" />';
        }
        else {
          return '';
        }
      })
			->make(true);
  }

  /**
	 * Show the view for a news item: /marketplace/news/add
   * and marketplace/news/modify
	 *
	 * @return view
	 */
  public function showNewsItem($id = 0) {
    $this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('org.news_item');
		$this->viewData['form_list_title'] = trans('app.news');
		$this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/marketplace/news');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/news/save');
		$this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/marketplace/news/delete');
    $this->viewData['model_id'] = 0;

		if($id > 0) {
      $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/news/modify');
			$this->viewData['model'] = AppOrgNewsItem::find($id);
			$this->viewData['model'] = $this->viewData['model']->toArray();
			$this->viewData['model_id'] = $id;

		}

		$this->viewData['model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
      'title' 						      =>  ['type' => 'input-text', 'attr' => ' maxlength=50 ', 'label' => trans('org.col_title')],
      'content' 				        =>  ['type' => 'html-editor', 'class' => 'br-html-editor', 'attr' => ' rows="20" style="min-height: 150px;" ', 'label' => trans('org.col_content'), ],
      'source_url' 						  =>  ['type' => 'input-text', 'attr' => ' maxlength=50 ', 'label' => trans('org.col_source_url')],
      'image_path'				      =>	['type' => 'file-uploader', 'file-type' => 'image', 'is_array' => false,
                                      'class' => 'image-uploader','attr' => 'data-url="'.url('11w5Cj9WDAjnzlg0/marketplace/news/upload_image').'"',
                                      'img-class' => '', 'img-path' => 'uploads/news-images/'],
      'is_enabled'              =>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ], 'class' => 'br-select2' ],
    ];

		return view('brcode.marketplace.new')->with('viewData',$this->viewData);
  }

  /**
	 * Save a news item
	 *
	 * @return redirect('back if fails | marketplace/news')
	 */
  public function saveNewsItem($id = 0) {
    $colPrefix = $this->columnPrefix;

		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		$rules = [
			$colPrefix.'title' 			  => 'required|min:3|max:50|unique:appOrgNewsItems,title' . ($id > 0 ? ','.$id : ''),
			$colPrefix.'content'      => 'required',
			
			$colPrefix.'is_enabled' 	=> 'required',
		];
    //$colPrefix.'image_path'   => 'required',
		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {

			$id = saveModel($id, $model, 'App\\AppOrgNewsItem',$subModelExists, $input, $colPrefix, $new);

      $newsItemImage = isset($input[$colPrefix . 'image_path']) ? $input[$colPrefix . 'image_path'] : '';

      if(strlen($newsItemImage) > 0 && strpos($newsItemImage,'news_item_image_') === false) {

        $ext = File::extension(public_path() . '/uploads/news-images/' . $newsItemImage);

        File::move(public_path() . '/uploads/news-images/' . $newsItemImage,
          public_path() . '/uploads/news-images/news_item_image_' . $model->id . '.' . $ext);

        $model->image_path = 'news_item_image_' . $model->id . '.' . $ext;
        $model->save();

      }

			$message = trans('org.news_item') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/news')->with('message',$message)->with('messageType','success');

		}
  }

  /**
	 * Delete a news item
	 *
	 * @return redirect('marketplace/news')
	 */
	public function deleteNewsItem() {
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		if($id > 0) {

			$product = AppOrgNewsItem::find($id);
      $product->modified_by = Auth::id();
			$product->deleted_at = date('Y-m-d H:i:s');
      $product->save();
			$message = trans('org.faq') . ' ' . trans('app.deleted_successfully');

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/news')->with('message',$message)->with('messageType','success');
		}

	}

  /**
	 * Upload the image related to the News Item
	 *
	 * @return JSON response
	 */
  public function saveNewsItemImage() {
    $inputName = 'file_' . $this->columnPrefix . 'image_path';

		$file = Request::file($inputName);
    $input = Request::all();
    $modelId = $input['model_id'];

		if($file) {

        $destinationPath = public_path() . '/uploads/news-images/';
        //$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				//$filename = md5(microtime()) . '.' . $extension;
        if($modelId > 0) {
          $filename = 'news_item_image_' . $modelId . '.' . $extension;
        }
        else {
          $filename = md5(microtime()) . '.' . $extension;
        }

        $upload_success = Request::file($inputName)->move($destinationPath, $filename);

        if ($upload_success) {

            return Response::json(array('message' => 'success', 'name' => $filename), 200);
        } else {
            return Response::json(array('mesage' => 'error'), 400);
        }
    }
  }

  /*
	|--------------------------------------------------------------------------
	| Front End - Blogs
	|--------------------------------------------------------------------------
	|
	*/

  /**
	 * Show the view that list blogs in: /marketplace/blogs
	 *
	 * @return view
	 */
  public function showBlogs() {

    $this->viewData['form_title'] = trans('app.blogs');
		$this->viewData['form_url_add'] = url('/11w5Cj9WDAjnzlg0/marketplace/blogs/add');
		$this->viewData['form_url_dt'] = url('/11w5Cj9WDAjnzlg0/marketplace/blogs/list-dt');
		$this->viewData['table_cols'] = ['app.col_id', 'org.col_title','app.col_image', 'app.col_is_enabled'];
    return view('brcode.marketplace.blogs')->with('viewData', $this->viewData);

  }

  /**
	 * Create a DataTable output for blogs table in: /marketplace/blogs
	 *
	 * @return DataTable
	 */
  public function dtBlogs() {
    $blogs = DB::table(DB::raw('appOrgBlogs AS n'))
    ->select(['n.id', 'n.title','n.image_path',
    DB::raw('(CASE WHEN n.is_enabled = "Y" THEN "Yes" ELSE "No" END) as is_enabled')
    ])
    ->whereNull('n.deleted_at');
    return Datatables::of($blogs)
			->addColumn('action', function ($blogs) {
                return '<a href="' . url('/11w5Cj9WDAjnzlg0/marketplace/blogs/modify/' .$blogs->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
            })
      ->addColumn('image_path',function($blogs) {
        if(strlen($blogs->image_path) > 0 ) {
          return '<img class="table-image" src="'.url('uploads/blogs-images/'.$blogs->image_path).'" />';
        }
        else {
          return '';
        }
      })
			->make(true);
  }

  /**
	 * Show the view for a blogs item: /marketplace/blogs/add
   * and marketplace/blogs/modify
	 *
	 * @return view
	 */
  public function showBlogsItem($id = 0) {
    $this->viewData['form_title'] = ($id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('org.blogs_item');
		$this->viewData['form_list_title'] = trans('app.blogs');
		$this->viewData['form_url_list'] = url('/11w5Cj9WDAjnzlg0/marketplace/blogs');
		$this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/blogs/save');
		$this->viewData['form_url_post_delete'] = url('/11w5Cj9WDAjnzlg0/marketplace/blogs/delete');
    $this->viewData['model_id'] = 0;

		if($id > 0) {
      $this->viewData['form_url_post'] = url('/11w5Cj9WDAjnzlg0/marketplace/blogs/modify');
			$this->viewData['model'] = AppOrgBlog::find($id);
			$this->viewData['model'] = $this->viewData['model']->toArray();
			$this->viewData['model_id'] = $id;

		}

		$this->viewData['model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
      'title' 						      =>  ['type' => 'input-text', 'attr' => ' maxlength=50 ', 'label' => trans('org.col_title')],
      'description' 						  =>  ['type' => 'input-text', 'attr' => ' maxlength=200 ', 'label' => trans('org.col_description')],
      'content' 				        =>  ['type' => 'html-editor', 'class' => 'br-html-editor', 'attr' => ' rows="20" style="min-height: 150px;" ', 'label' => trans('org.col_content'), ],
      'image_path'				      =>	['type' => 'file-uploader', 'file-type' => 'image', 'is_array' => false,
                                      'class' => 'image-uploader','attr' => 'data-url="'.url('11w5Cj9WDAjnzlg0/marketplace/blogs/upload_image').'"',
                                      'img-class' => '', 'img-path' => 'uploads/blogs-images/'],
      'is_enabled'              =>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ], 'class' => 'br-select2' ],
      'is_top'              =>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ], 'class' => 'br-select2' ],
    ];

		return view('brcode.marketplace.blog')->with('viewData',$this->viewData);
  }

  /**
	 * Save a blogs item
	 *
	 * @return redirect('back if fails | marketplace/blogs')
	 */
  public function saveBlogsItem($id = 0) {
    $colPrefix = $this->columnPrefix;

		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		$rules = [
			$colPrefix.'title' 			  => 'required|min:3|max:50|unique:appOrgBlogs,title' . ($id > 0 ? ','.$id : ''),
			$colPrefix.'content'      => 'required',
			
			$colPrefix.'is_enabled' 	=> 'required',
		];
    //$colPrefix.'image_path'   => 'required',
		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {

			$id = saveModel($id, $model, 'App\\AppOrgBlog',$subModelExists, $input, $colPrefix, $new);

      $blogsItemImage = isset($input[$colPrefix . 'image_path']) ? $input[$colPrefix . 'image_path'] : '';

      if(strlen($blogsItemImage) > 0 && strpos($blogsItemImage,'blogs_item_image_') === false) {

        $ext = File::extension(public_path() . '/uploads/blogs-images/' . $blogsItemImage);

        File::move(public_path() . '/uploads/blogs-images/' . $blogsItemImage,
          public_path() . '/uploads/blogs-images/blogs_item_image_' . $model->id . '.' . $ext);

        $model->image_path = 'blogs_item_image_' . $model->id . '.' . $ext;
        $model->save();

      }

			$message = trans('org.blogs_item') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/blogs')->with('message',$message)->with('messageType','success');

		}
  }

  /**
	 * Delete a blogs item
	 *
	 * @return redirect('marketplace/blogs')
	 */
	public function deleteBlogsItem() {
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		if($id > 0) {

			$product = AppOrgBlog::find($id);
      $product->modified_by = Auth::id();
			$product->deleted_at = date('Y-m-d H:i:s');
      $product->save();
			$message = trans('org.faq') . ' ' . trans('app.deleted_successfully');

			return redirect('/11w5Cj9WDAjnzlg0/marketplace/blogs')->with('message',$message)->with('messageType','success');
		}

	}

  /**
	 * Upload the image related to the News Item
	 *
	 * @return JSON response
	 */
  public function saveBlogsItemImage() {
    $inputName = 'file_' . $this->columnPrefix . 'image_path';

		$file = Request::file($inputName);
    $input = Request::all();
    $modelId = $input['model_id'];

		if($file) {

        $destinationPath = public_path() . '/uploads/blogs-images/';
        //$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				//$filename = md5(microtime()) . '.' . $extension;
        if($modelId > 0) {
          $filename = 'blogs_item_image_' . $modelId . '.' . $extension;
        }
        else {
          $filename = md5(microtime()) . '.' . $extension;
        }

        $upload_success = Request::file($inputName)->move($destinationPath, $filename);

        if ($upload_success) {

            return Response::json(array('message' => 'success', 'name' => $filename), 200);
        } else {
            return Response::json(array('mesage' => 'error'), 400);
        }
    }
  }
  



  public function searchProducts($id=''){
    
    $imageVal = str_replace("_img_","", substr($id, strpos($id, '_img_')));
    $plat = substr($id, 0, strpos($id, '_img_'));
    $productImg = $imageVal == 'true' ? AppOrgProduct::has('images')->pluck('id') : [];
    
      $products = DB::table(DB::raw('appOrgProducts AS p'))
      ->select(['p.id', 'p.name' ,'p.release_date', 'p.platform','p.region', 'p.is_enabled',
        DB::raw('c.category_text AS category'),DB::raw('l.value AS language')
      ])
      ->where(function ($query) use ($plat) {
        if('all' != $plat)
          $query->where('p.platform',$plat);
      })
      ->whereNotIn('p.id', $productImg)
      ->whereNull('p.deleted_at')
      ->join('appOrgCategories AS c','c.id','=','p.prod_category_id')
      ->leftJoin('sysDictionaries AS l', 'l.value_id','=','p.language');
    
    return Datatables::of($products)
			->addColumn('action', function ($products) {
          return '<a href="' . route('product.edit',[$products->id]).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>'.' <button class="btn btn-xs btn-success btn-image-open" onclick="openModalImage(this)" data-product="' .$products->id.'" data-name="' .$products->name.'"><i class="fa fa-image"></i></button>';
      })
			->make(true);
  }
  
  

}
