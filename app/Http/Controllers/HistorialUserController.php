<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\AppOrgPromotionalCode;
use App\AppOrgUserInventory;
use App\SysShipping;
use Illuminate\Http\Request;

class HistorialUserController extends Controller
{
    public function index()
    {

        $historial_user       = AppOrgUserInventory::where('quantity', '>', 0)->get();

        //dd($historial_product);

        return view('admin.historial_product.index', [
            'historial_user'     => $historial_user,
        ]);
    }
}