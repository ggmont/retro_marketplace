<?php

namespace App\Http\Controllers\Special;

use App\Http\Controllers\Controller;
use Auth;
use Request;
use App\AppOrgOrder;
use App\AppOrgUserInventory;
use App\AppOrgOrderDetail;
use App\SysUser;
use App\SysCountry;
use App\AppMessage;
use App\SysUserRoles;
use DB;
use File;
use Cookie;
use App;

class HomeController extends Controller
{
	private function loadCart() {

		$cart   = [];
		$ship   = [];
	
		$hasta = date('Y-m-d 23:59:59');
		$desde = date('Y-m-d 00:00:00', strtotime($hasta."- 15 days"));
	
		//// error_log($user_ip);
	
		$userId = Auth::id();
		
		$this->viewData['profesional'] = SysUserRoles::where('user_id', Auth::user()->id)->whereIn('role_id', [2])->get();
		$this->viewData['imagen'] = SysUserRoles::where('user_id', Auth::user()->id)->whereIn('role_id', [5])->get();
		 
		$items_total = 0;
		$locale = App::getLocale();
		$k = 0;
	
		if(! Cookie::get('LG')){
		  Cookie::queue('LG', 'es', 2628000);
		  session(['locale' => Cookie::get('LG')]);
		}else{
		  if(! session('locale')){
			session(['locale' => Cookie::get('LG')]);
		  }else if(session('locale') != Cookie::get('LG')){
			session(['locale' => Cookie::get('LG')]);
		  }
		}
	
		if(Auth::user()){
		  foreach(Auth::user()->cart->details as $detail){
			$items_total += $detail->quantity;
		  }
		}
		
		$this->viewData['translations'] = File::exists(base_path() . '/resources/lang/' . $locale . '_ang.json') ? File::get(base_path() . '/resources/lang/' . $locale . '_ang.json') : '{}';
		$this->viewData['header_pay_pending'] = AppOrgOrder::where('buyer_user_id', Auth::id())->where('status', 'RP')->whereBetween('updated_at',[$desde, $hasta])->get();
		$this->viewData['header_send_pending'] = AppOrgOrder::where('seller_user_id', Auth::id())->where('status', 'CR')->whereBetween('updated_at',[$desde, $hasta])->get();
	  
		$this->viewData['header_notifications'] = AppOrgOrder::
		where(function ($query) use ($userId) {
		  $query->where('buyer_user_id', $userId)->where(function ($query2){
			$query2->where('status', 'ST')->orWhere('status', 'RP');
		  });
		})->orWhere(function ($query) use ($userId) {
		  $query->where('seller_user_id', $userId)->where(function ($query2){
			$query2->where('status', 'DD')->orWhere('status', 'RP')->orWhere('status', 'CR')->orWhere('status', 'CW')->orWhere('status', 'PC');
		  });
		})
		->whereBetween('updated_at',[$desde, $hasta])
		->orderBy('updated_at', 'desc')->get();
		
		foreach($this->viewData['header_notifications'] as $key){
		  $k += $key->seller_user_id == Auth::id() ? ( $key->seller_read == 'N' ? 1 : 0 ) : ( $key->buyer_read == 'N' ? 1 : 0 );
		}
		
		if(Auth::user()){
		  $this->viewData['header_messages'] = AppMessage::where('alias_to_id', Auth::user()->id)->where('read', false)->get()->unique('alias_from_id');
		}
		
		$this->viewData['c_h_n']           = $k;
		$this->viewData['cart']            = $cart;
		$this->viewData['ship']            = $ship;
		$this->viewData['products_count']  = count($cart);
		$this->viewData['items_count']     = $items_total;
		$this->viewData['countries']       = SysCountry::all();
	
	  }

	public function index()
	{
		$this->loadCart();
		$this->viewData['user_inventory'] = AppOrgUserInventory::where('user_id', Auth::id())->count();
		$this->viewData['user_online'] = count(SysUser::where('is_online', 1)->get());
		$this->viewData['user_all'] = count(SysUser::all());
		$this->viewData['user_register'] = count(SysUser::whereMonth('created_at', '=', (date('m')))->get());
		$this->viewData['order_user'] = count(AppOrgOrder::whereNotIn('status', ['CP', 'CE', 'AC', 'RC', 'CS', 'TD', 'FD'])->whereMonth('created_at', '=', (date('m')))->get());
		$this->viewData['order_user_complete'] = count(AppOrgOrder::where('status', 'DD')->whereMonth('created_at', '=', (date('m')))->get());
		$this->viewData['product_user'] = count(AppOrgUserInventory::whereNull('deleted_at')->get());
		$this->viewData['product_user_qty'] = AppOrgUserInventory::whereNull('deleted_at')->sum('quantity');

		$this->viewData['resumen'] = AppOrgOrder::where('status', 'FD')
			->select(DB::raw('CONCAT(YEAR(created_at), MONTH(created_at)) AS dates'), DB::raw('MONTH(created_at) month'), DB::raw('YEAR(created_at) years'), "created_at")
			->groupBy('dates')
			->selectRaw('sum(total) as sum')
			->get();

		return view('special.index')->with('viewData', $this->viewData);
	}
}
