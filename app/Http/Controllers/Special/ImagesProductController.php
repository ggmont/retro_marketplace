<?php

namespace App\Http\Controllers\Special;

use App\Http\Controllers\Controller;
use Auth;
use Response;
use Illuminate\Http\Request;
use App\AppOrgCategory;
use App\AppOrgProduct;
use App\AppOrgOrder;
use App\AppMessage;
use App\SysCountry;
use App\AppOrgUserInventory;
use App\AppOrgOrderDetail;
use App\SysUser;
use App\SysDictionary;
use App\AppOrgProductImage;
use DB;
use App;
use Cookie;
use File;


class ImagesProductController extends Controller
{
    private function loadCart() {

		$cart   = [];
		$ship   = [];
	
		$hasta = date('Y-m-d 23:59:59');
		$desde = date('Y-m-d 00:00:00', strtotime($hasta."- 15 days"));
	
		//// error_log($user_ip);
	
		$userId = Auth::id();
		$items_total = 0;
		$locale = App::getLocale();
		$k = 0;
	
		if(! Cookie::get('LG')){
		  Cookie::queue('LG', 'es', 2628000);
		  session(['locale' => Cookie::get('LG')]);
		}else{
		  if(! session('locale')){
			session(['locale' => Cookie::get('LG')]);
		  }else if(session('locale') != Cookie::get('LG')){
			session(['locale' => Cookie::get('LG')]);
		  }
		}
	
		if(Auth::user()){
		  foreach(Auth::user()->cart->details as $detail){
			$items_total += $detail->quantity;
		  }
		}
		
		$this->viewData['translations'] = File::exists(base_path() . '/resources/lang/' . $locale . '_ang.json') ? File::get(base_path() . '/resources/lang/' . $locale . '_ang.json') : '{}';
		$this->viewData['header_pay_pending'] = AppOrgOrder::where('buyer_user_id', Auth::id())->where('status', 'RP')->whereBetween('updated_at',[$desde, $hasta])->get();
		$this->viewData['header_send_pending'] = AppOrgOrder::where('seller_user_id', Auth::id())->where('status', 'CR')->whereBetween('updated_at',[$desde, $hasta])->get();
	  
		$this->viewData['header_notifications'] = AppOrgOrder::
		where(function ($query) use ($userId) {
		  $query->where('buyer_user_id', $userId)->where(function ($query2){
			$query2->where('status', 'ST')->orWhere('status', 'RP');
		  });
		})->orWhere(function ($query) use ($userId) {
		  $query->where('seller_user_id', $userId)->where(function ($query2){
			$query2->where('status', 'DD')->orWhere('status', 'RP')->orWhere('status', 'CR')->orWhere('status', 'CW')->orWhere('status', 'PC');
		  });
		})
		->whereBetween('updated_at',[$desde, $hasta])
		->orderBy('updated_at', 'desc')->get();
		
		foreach($this->viewData['header_notifications'] as $key){
		  $k += $key->seller_user_id == Auth::id() ? ( $key->seller_read == 'N' ? 1 : 0 ) : ( $key->buyer_read == 'N' ? 1 : 0 );
		}
		
		if(Auth::user()){
		  $this->viewData['header_messages'] = AppMessage::where('alias_to_id', Auth::user()->id)->where('read', false)->get()->unique('alias_from_id');
		}
		
		$this->viewData['c_h_n']           = $k;
		$this->viewData['cart']            = $cart;
		$this->viewData['ship']            = $ship;
		$this->viewData['products_count']  = count($cart);
		$this->viewData['items_count']     = $items_total;
		$this->viewData['countries']       = SysCountry::all();
	
	  }

    public function index() {
        $this->loadCart();
        return view('special.images.index', [
            
        ])->with('viewData', $this->viewData);
    }

    public function edit($id)
    {
        $this->loadCart();
        $product = AppOrgProduct::findOrFail($id);

        $prueba = $product->images;
        //dd($prueba);

        $categories = AppOrgCategory::all();
        $genero = SysDictionary::where("code", "GAME_CATEGORY")->get();
        $location = SysDictionary::where("code", "GAME_LOCATION")->get();
        $generation = SysDictionary::where("code", "GAME_GENERATION")->get();
        $language = SysDictionary::where("code", "GAME_LANGUAGE")->get();
        $media = SysDictionary::where("code", "GAME_SUPPORT")->get();
        $platform = SysDictionary::where("code", "GAME_PLATFORM")->get();
        $region = SysDictionary::where("code", "GAME_REGION")->get();
        return view(
            "special.images.edit",
            compact(
                'product',
                'prueba',
                'categories',
                'genero',
                'location',
                'generation',
                'language',
                'media',
                'platform',
                'region'
            )
        )->with('viewData', $this->viewData);
    }
}
