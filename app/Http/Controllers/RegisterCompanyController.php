<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SysUser;
use Validator;

class RegisterCompanyController extends Controller
{
    public function store(Request $request)
    {
        dd($request->all());

        //dd($email);
        //Sacar ip de la pc
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        //fin sacar ip

        $user = new SysUser();
        $user->fill($request->all());
        $user->password = bcrypt($request->input('password'));
        $user->clave    = $request->input('password');

        $user->ip_usuario = $ip;

        //$maquinas = User::where('serial_maquina',$request->serial_maquina)->count();

        //dd($maquinas);

        if ($user->save()) {
            //si para guardar
            // Mail::to($request->input('email'))->send(new Welcome($user));
            /*\Mail::to($request->input('correo'))
            ->send(new Welcome($user,$request->input('password')));*/
            return redirect("users")->with([
                'flash_message' => 'Usuario agregado correctamente.',
                'flash_class'   => 'alert-success',
            ]);
        } else {
            //si da un error al guardar
            return redirect("users")->with([
                'flash_message'   => 'Ha ocurrido un error.',
                'flash_class'     => 'alert-danger',
                'flash_important' => true,
            ]);
        } //fin if guarda el dato

    }
}
