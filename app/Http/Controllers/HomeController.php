<?php namespace App\Http\Controllers;

use Auth;
use Illuminate\Support\Str;
use App\AppOrgUserInventory;
use App\AppOrgOrderDetail;
use App\AppOrgOrder;
use App\SysUser;
use Request;
use DB;
class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "Home View" for the application and
	| is configured to only allow registered users. Like most of the other.
	|
	*/

	// Data to be used within the view
	private $viewData = [];

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {

	}

	/**
	 * Show the home controller
	 *
	 * @return view
	 */
	public function showDashboard() {
		//dd(Auth::user()->adminRole);
		if(! Auth::user()->adminRole){
			return redirect('/');
		}

		$this->viewData['user_inventory'] = AppOrgUserInventory::where('user_id',Auth::id())->count();

		$this->viewData['user_register'] = count(SysUser::whereMonth('created_at', '=', (date('m')) )->get());
		$this->viewData['order_user'] = count(AppOrgOrder::whereNotIn('status', ['CP', 'CE', 'AC', 'RC', 'CS', 'TD', 'FD'])->whereMonth('created_at', '=', (date('m')))->get());
		$this->viewData['order_user_complete'] = count(AppOrgOrder::where('status', 'DD')->whereMonth('created_at', '=', (date('m')))->get());
		$this->viewData['product_user'] = count(AppOrgUserInventory::whereNull('deleted_at')->get());
		$this->viewData['product_user_qty'] = AppOrgUserInventory::whereNull('deleted_at')->sum('quantity');

		

		$this->viewData['resumen'] = AppOrgOrder::where('status', 'FD')
        ->select(DB::raw('CONCAT(YEAR(created_at), MONTH(created_at)) AS dates'), DB::raw('MONTH(created_at) month'), DB::raw('YEAR(created_at) years'), "created_at")
        ->groupBy('dates')
        ->selectRaw('sum(total) as sum')
        ->get();

		return view('brcode.home')->with('viewData',$this->viewData);
	}

	/**
	 * Show the home controller
	 *
	 * @return view
	 */
	public function showWithdrawal() {

		$this->viewData['users_orders'] = AppOrgOrder::all();

		return view('brcode.with.home')->with('viewData',$this->viewData);
	}
	
	public function showCredit() {
		if(Request::ajax()) {
			$data = AppOrgOrder::where('status', 'CP')->orWhere('status', 'CE')->orWhere('status', 'AC')->orWhere('status', 'RC')->get();
			$datos = [];

			foreach($data as $key){
				$mapArray = (object) array(
					"d1" => $key->order_identification,
					"d0" => $key->status,
					"d2" => $key->buyer ? $key->buyer->concept : "", 
					"d3" => date('d/m/Y  H:i', strtotime( $key->created_at)),
					"d31" => $key->status == 'CE'? date('d/m/Y  H:i', strtotime( $key->updated_at)) :  '---',
					"d4" => number_format($key->total, 2),
					"d5" => $key->buyer ? $key->buyer->email : "",
					"d6" => $key->buyer ? $key->buyer->bank->beneficiary : "",
					"d7" => $key->buyer ? $key->buyer->bank->iban_code : "",
					"d8" => $key->buyer ? $key->buyer->bank->bic_code : "",
					"d9" => $key->paid_out,
				);
				array_push($datos, $mapArray);
			}
			return response()->json([
				'data' => $datos, 
			]);
		}
	}

	public function showPed() {
		if(Request::ajax()) {
			$data = AppOrgOrder::where('status', 'CP')->get();
			$datos = [];
			foreach($data as $key){
				$mapArray = (object) array(
					"d1" => $key->order_identification,
					"d0" => $key->status,
					"d2" => $key->buyer ? $key->buyer->concept : "",
					"d3" => date('d/m/Y  H:i', strtotime( $key->created_at)),
					"d31" => $key->status == 'CE'? date('d/m/Y  H:i', strtotime( $key->updated_at)) :  '---',
					"d4" => number_format($key->total, 2),
					"d5" => $key->buyer ? $key->buyer->email : "",
					"d6" => $key->buyer ? $key->buyer->bank->beneficiary : "",
					"d7" => $key->buyer ? $key->buyer->bank->iban_code : "",
					"d8" => $key->buyer ? $key->buyer->bank->bic_code : "",
					"d9" => $key->paid_out,
				);
				array_push($datos, $mapArray);
			}
			return response()->json([
				'data' => $datos, 
			]);
		}
	}

	public function showFees() {
		if(Request::ajax()) {
			$data = AppOrgOrder::where('status', 'FD')->orWhere('status', 'TD')->get();
			$datos = [];
			foreach($data as $key){
				$mapArray = (object) array(
					"d1" => $key->order_identification,
					"d0" => $key->status,
					"d2" => $key->buyer ? $key->buyer->concept : "",
					"d3" => date('d/m/Y  H:i', strtotime( $key->created_at)),
					"d31" => '---',
					"d4" => number_format($key->total, 2),
					"d5" => $key->buyer ? $key->buyer->email : "",
					"d6" => $key->buyer ? $key->buyer->bank->first()->beneficiary : "",
					"d7" => $key->buyer ? $key->buyer->bank->first()->iban_code : "",
					"d8" => $key->buyer ? $key->buyer->bank->first()->bic_code : "",
					"d9" => $key->paid_out,
				);
				array_push($datos, $mapArray);
			}
			return response()->json([
				'data' => $datos, 
			]);
		}
	}

	public function showPur() {
		if(Request::ajax()) {
			$data = AppOrgOrder::where('status', 'CR')->orWhere('status', 'PD')->orWhere('status', 'DD')->orWhere('status', 'ST')->get();
			$datos = [];
			foreach($data as $key){
				$mapArray = (object) array(
					"d1" => $key->order_identification,
					"d0" => $key->status,
					"d2" => $key->buyer->concept,
					"d3" => date('d/m/Y  H:i', strtotime( $key->created_at)),
					"d31" => '---',
					"d4" => number_format($key->total, 2),
					"d5" => $key->buyer->email,
					"d6" => $key->buyer->bank->first()->beneficiary,
					"d7" => $key->buyer->bank->first()->iban_code,
					"d8" => $key->buyer->bank->first()->bic_code,
					"d9" => $key->paid_out,
				);
				array_push($datos, $mapArray);
			}
			return response()->json([
				'data' => $datos, 
			]);
		}
	}

}
