<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Mimey\MimeTypes;

class ProductRequestImageController extends Controller
{
    public function store(Request $request): array
    {
        $images = [];
        $directory = public_path() . '/uploads/inventory-images/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
        if(is_dir($directory) === false){
            mkdir($directory, 0755, true);
        }

        ini_set('memory_limit', '512M');
        foreach ($request->input('photos') as $photo){
            $mime_type = mime_content_type($photo);
            $mimes = new MimeTypes;
            $extension = $mimes->getExtension($mime_type);
            $filename = 'request' . '_' . auth('sanctum')->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $extension;
            Image::make(file_get_contents($photo))->save($directory.$filename);
            /* Si es necesario volver a cambiar el tamaño el frontend esta usando este codigo para tal fin
                $imgx = Image::make($image->getRealPath());
                $imgx->resize(600, 750, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($directory.'/'.$filename);
            */

            if(file_exists($directory.$filename)){
                $images[] = date('Y') . '/' . date('m') . '/' . date('d') . '/' . $filename;
            }
        }

        return $images;
    }
}
