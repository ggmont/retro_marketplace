<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SysUser;
use App\SysUserRoles;
use App\AppOrgProduct;

class Select2SearchController extends Controller
{
    public function selectSearch(Request $request)
    {
        $search = $request->search;

        $filter_user = SysUserRoles::where('role_id', '!=', 1)->pluck('user_id');

        if($search == ''){
           $employees = SysUser::wherein('id', $filter_user)->orderby('user_name','asc')->select('id','user_name')->limit(5)->get();
        }else{
           $employees = SysUser::wherein('id', $filter_user)->orderby('user_name','asc')->select('id','user_name')->where('user_name', 'like', '%' .$search . '%')->limit(5)->get();
        }
  
        $response = array();
        foreach($employees as $employee){
           $response[] = array(
                "id"=>$employee->id,
                "text"=>$employee->user_name
           );
        }
  
        return response()->json($response);
    }

    public function selectSearchRefered(Request $request)
    {
        $search = $request->search;

        $filter_user = SysUserRoles::where('role_id', '!=', 1)->pluck('user_id');

        if($search == ''){
           $employees = SysUser::wherein('id', $filter_user)->orderby('user_name','asc')->select('id','user_name')->limit(5)->get();
        }else{
           $employees = SysUser::wherein('id', $filter_user)->orderby('user_name','asc')->select('id','user_name')->where('user_name', 'like', '%' .$search . '%')->limit(5)->get();
        }
  
        $response = array();
        foreach($employees as $employee){
           $response[] = array(
                "id"=>$employee->id,
                "text"=>$employee->user_name
           );
        }
  
        return response()->json($response);
    }

    public function selectSearchProduct(Request $request)
    {
        $search_product = $request->search_product;

        if($search_product == ''){
           $products = AppOrgProduct::orderby('name','asc')->select('id','name','platform','region')->limit(20)->get();

           
        }else{
           $products = AppOrgProduct::orderby('name','asc')->select('id','name','platform','region')->where('name', 'like', '%' .$search_product . '%')->limit(70)->get();
        }
  
        $response = array();
        foreach($products as $p){
           $response[] = array(
                "id"=>$p->id,
                "text"=>$p->name. ' - ' .$p->platform. ' - ' .$p->region. '-'
           );
        }
  
        return response()->json($response);
    }

    public function selectSearchGame(Request $request)
    {
        $search_product = $request->search_product;

        if($search_product == ''){
          $products = AppOrgProduct::whereIn('prod_category_id', [198,5,6,7,8,202,203,207,199])->orderby('name','asc')->where('name', 'like', '%' .$search_product . '%')->limit(7)->get();
        }else{
           $products = AppOrgProduct::whereIn('prod_category_id', [198,5,6,7,8,202,203,207,199])->orderby('name','asc')->where('name', 'like', '%' .$search_product . '%')->limit(7)->get();
        }
  
        $response = array();
        foreach($products as $p){
           $response[] = array(
                "id"=>$p->id,
                "text"=>$p->name. ' - ' .$p->platform. ' - ' .$p->region
           );
        }
  
        return response()->json($response);
    }

    public function selectSearchConsole(Request $request)
    {
        $search_product = $request->search_product;

        if($search_product == ''){
         $products = AppOrgProduct::whereIn('prod_category_id', 
         [9,208,204,205,201,200,197,196,195,194,193,192,191,190,
         189,188,187,186,185,184,183,182,181,180,171,170,169,168,167,166,165,164,163,162,161,160,
         159,158,157,156,155,154,153,152,151,150,149,148,147,146,145,144,143,142,141,140,139,138,
         137,136,135,134,133,132,131,130,129,128,127,126,125,124,123,122,121,120,119,118,117,116,
         115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,92,
         90,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,69,68,67,66,65,64,63,53,52,51,50,
         49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,26,25,24,23,22])->orderby('name','asc')->where('name', 'like', '%' .$search_product . '%')->limit(7)->get();
        }else{
           $products = AppOrgProduct::whereIn('prod_category_id', 
           [9,208,204,205,201,200,197,196,195,194,193,192,191,190,
           189,188,187,186,185,184,183,182,181,180,171,170,169,168,167,166,165,164,163,162,161,160,
           159,158,157,156,155,154,153,152,151,150,149,148,147,146,145,144,143,142,141,140,139,138,
           137,136,135,134,133,132,131,130,129,128,127,126,125,124,123,122,121,120,119,118,117,116,
           115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,92,
           90,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,69,68,67,66,65,64,63,53,52,51,50,
           49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,26,25,24,23,22])->orderby('name','asc')->where('name', 'like', '%' .$search_product . '%')->limit(7)->get();
        }
  
        $response = array();
        foreach($products as $p){
           $response[] = array(
                "id"=>$p->id,
                "text"=>$p->name. ' - ' .$p->platform. ' - ' .$p->region
           );
        }
  
        return response()->json($response);
    }

    public function selectSearchPeriferico(Request $request)
    {
        $search_product = $request->search_product;

        if($search_product == ''){
          $products = AppOrgProduct::whereIn('prod_category_id', [10,11,12,13,14,15,16,177,178,179])->orderby('name','asc')->where('name', 'like', '%' .$search_product . '%')->limit(7)->get();
        }else{
           $products = AppOrgProduct::whereIn('prod_category_id', [10,11,12,13,14,15,16,177,178,179])->orderby('name','asc')->where('name', 'like', '%' .$search_product . '%')->limit(7)->get();
        }
  
        $response = array();
        foreach($products as $p){
           $response[] = array(
                "id"=>$p->id,
                "text"=>$p->name. ' - ' .$p->platform. ' - ' .$p->region
           );
        }
  
        return response()->json($response);
    }

    public function selectSearchAccesorio(Request $request)
    {
        $search_product = $request->search_product;

        if($search_product == ''){
          $products = AppOrgProduct::whereIn('prod_category_id', [17,18,19,20,21])->orderby('name','asc')->where('name', 'like', '%' .$search_product . '%')->limit(7)->get();
        }else{
           $products = AppOrgProduct::whereIn('prod_category_id', [17,18,19,20,21])->orderby('name','asc')->where('name', 'like', '%' .$search_product . '%')->limit(7)->get();
        }
  
        $response = array();
        foreach($products as $p){
           $response[] = array(
                "id"=>$p->id,
                "text"=>$p->name. ' - ' .$p->platform. ' - ' .$p->region
           );
        }
  
        return response()->json($response);
    }

    public function selectSearchMerchandising(Request $request)
    {
        $search_product = $request->search_product;

        if($search_product == ''){
          $products = AppOrgProduct::whereIn('prod_category_id', [210,172,173,174,175,176])->orderby('name','asc')->where('name', 'like', '%' .$search_product . '%')->limit(7)->get();
        }else{
           $products = AppOrgProduct::whereIn('prod_category_id', [210,172,173,174,175,176])->orderby('name','asc')->where('name', 'like', '%' .$search_product . '%')->limit(7)->get();
        }
  
        $response = array();
        foreach($products as $p){
           $response[] = array(
                "id"=>$p->id,
                "text"=>$p->name. ' - ' .$p->platform. ' - ' .$p->region
           );
        }
  
        return response()->json($response);
    }
}
