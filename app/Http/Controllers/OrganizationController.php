<?php namespace App\Http\Controllers;

use Auth;
use Config;
use Datatables;
use DB;
use Hash;
use Redirect;
use Request;
use Session;
use Storage;
use Validator;
use View;

use App\AppOrgAirfield;
use App\AppOrgAirfieldRole;
use App\AppOrganization;
use App\AppOrgUser;
use App\AppOrgRole;
use App\AppOrgUserRole;
use App\SysCountry;
use App\SysDictionary;
use App\SysOrganization;

use App\Events\ActionExecuted;

class OrganizationController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Organization Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "Organizations View" for the application and
	| is configured to only allow registered users. Like most of the other.
	|
	*/
	private $viewData = [];

	private $columnPrefix = '';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->columnPrefix =  Config::get('brcode.column_prefix');
		$this->viewData['column_prefix'] 	= $this->columnPrefix;
		$this->viewData['lang'] 					= 'org';
	}

	/**
	 * Set the initial values for the views
	 *
	 * @return void
	 */
	public function setInitialData() {
		$this->viewData['first_name'] = Auth::user()->first_name;
		$this->viewData['user_full_name'] = Auth::user()->first_name . ' ' . Auth::user()->last_name;
		$this->viewData['model_id'] = 0;
	}

	/**
	 * Show the view that allows clients to modify their organizations
	 *
	 * @return view
	 */
	public function showConfig($org_id) {
		$this->setInitialData();

		$this->viewData['form_title'] = trans('org.config');
		$this->viewData['form_url_add'] = url('/users/user');
		$this->viewData['form_url_dt'] = url('/users/list-dt');
		$this->viewData['table_cols'] = ['col_id', 'col_first_name', 'col_last_name', 'col_email', 'col_user_name', 'col_is_enabled'];

		return view('brcode.organizations.config')->with('viewData', $this->viewData);
	}

	/*
	|--------------------------------------------------------------------------
	| AIRFIELDS
	|--------------------------------------------------------------------------
	|
	*/

	/**
	 * Show the view that list airfields in: /organizations/#/airfields
	 *
	 * @return view
	 */
	public function showAirfields($org_id) {
		$this->setInitialData();

		$this->viewData['form_title'] = trans('org.airfields');
		$this->viewData['form_url_add'] = url('/organizations/'.$org_id.'/airfield');
		$this->viewData['form_url_dt'] = url('/organizations/'.$org_id.'/airfields/list-dt');

		$this->viewData['table_cols'] = ['app.col_id', 'app.col_name', 'app.col_code', 'org.col_alt_name', 'org.col_latitude_dms', 'org.col_longitude_dms','org.col_is_private'];

		return view('brcode.organizations.airfields')->with('viewData', $this->viewData);
	}

	/**
	 * Create a DataTable output for airfields' table in:
	 * /organizations/#/airfields
	 *
	 * @return DataTable
	 */
	public function dtAirfields($org_id) {
		$orgs = DB::table('appOrgAirfields')
		->select(['id', 'name', 'code', 'alt_name', 'latitude_dms','longitude_dms', DB::raw('(CASE WHEN is_private = "N" THEN "No" ELSE "Yes" END) as is_private') ] ) //
		->where('sys_org_id',$org_id)
		->whereNull('deleted_at');

		$result = Datatables::of($orgs)->addColumn('action', function ($orgs) use($org_id) {
			return '<a href="' . url('/organizations/'.$org_id.'/airfield/' .$orgs->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
		});

		return $result->make(true);
	}

	/**
	 * Show the view for a airfield: /organizations/#/airfield
	 *
	 * @return view
	 */
	public function showAirfield($org_id, $airfield_id = 0) {
		$this->setInitialData();

		$this->viewData['form_title'] = ($airfield_id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('org.airfield');
		$this->viewData['form_list_title'] = trans('org.airfields');
		$this->viewData['form_url_list'] = url('/organizations/'.$org_id.'/airfields');
		$this->viewData['form_url_post'] = url('/organizations/'.$org_id.'/save_airfield');
		$this->viewData['form_url_post_delete'] = url('/organizations/'.$org_id.'/delete_airfield');

		if($airfield_id > 0) {
			$this->viewData['model'] = AppOrgAirfield::find($airfield_id);
			$this->viewData['model'] = $this->viewData['model']->toArray();
			$this->viewData['model_id'] = $airfield_id;
		}

		$categoryRows = SysDictionary::ofCode('AIRFIELD_CAT')->get();

		$categories = [];

		foreach($categoryRows as $key => $row) {
			$categories[$key] = ['id' => $row->value_id, 'value' => $row->value];
		}

		$this->viewData['model_cols'] = [
			'id' 										=> 	['type' => 'hidden', 'attr' => ' '],
			'name' 									=>  ['type' => 'input-text', 'attr' => ' maxlength=50 ', 'label' => trans('app.col_name')],
			'code' 									=>  ['type' => 'input-text', 'attr' => ' maxlength=50 ', 'label' => trans('app.col_code')],
			'alt_name' 							=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
			'latitude_dms' 					=>  ['type' => 'input-text', 'attr' => ' maxlength=50 data-mask="DMS" ', 'class' => 'br-dms-lat-s'],
			'longitude_dms' 				=>  ['type' => 'input-text', 'attr' => ' maxlength=50 data-mask="DMS" ', 'class' => 'br-dms-lng-s'],
			'latitude_dec' 					=>  ['type' => 'input-text', 'attr' => ' maxlength=50 data-number="8" ', 'class' => 'br-dms-lat-d'],
			'longitude_dec' 				=>  ['type' => 'input-text', 'attr' => ' maxlength=50 data-number="8" ', 'class' => 'br-dms-lng-d'],
			'notices' 							=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
			'category' 							=>  ['type' => 'select', 'label' => trans('app.col_category'), 'select_values' => $categories ],
			'is_private' 						=>  ['type' => 'select', 'select_values' => [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ] ],
		];

		return view('brcode.organizations.airfield')->with('viewData', $this->viewData);
	}

	/**
	 * Save a airfield
	 *
	 * @return redirect(back if fails | /organizations/#/airfields if success)
	 */
	public function saveAirfield($org_id) {

		$colPrefix = $this->columnPrefix;

		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		$rules = [
						$colPrefix.'name' 					=> 'required|min:3|max:50|unique:appOrgAirfields,name' . ($id > 0 ? ','.$id : ''),
						$colPrefix.'code' 					=> 'required',
						$colPrefix.'alt_name' 			=> 'required',
						$colPrefix.'latitude_dms' 	=> 'required',
						$colPrefix.'latitude_dec' 	=> 'required',
						$colPrefix.'longitude_dms' 	=> 'required',
						$colPrefix.'longitude_dec' 	=> 'required',
						$colPrefix.'notices' 				=> 'required',
						$colPrefix.'category' 			=> 'required',
						$colPrefix.'is_private' 		=> 'required',
					];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {

			$input['add_' . 'sys_org_id'] = $org_id;

			$id = saveModel($id, $model, 'App\\AppOrgAirfield',$subModelExists, $input, $colPrefix, $new);

			$id = $model->id;

			$message = trans('org.airfield') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/organizations/'.$org_id.'/airfields')->with('message',$message)->with('messageType','success');

		}

	}

	/**
	 * Delete a airfield
	 *
	 * @return redirect(back if fails | /organizations/#/airfields if success)
	 */
	public function deleteAirfield($org_id) {
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		if($id > 0) {

			$airfield = AppOrgAirfield::find($id);
			$airfield->modified_by = Auth::id();
			$airfield->save();
			$airfield->delete();

			$message = trans('org.airfield') . ' ' . trans('app.deleted_successfully');

			return redirect('/organizations/'.$org_id.'/airfields')->with('message',$message)->with('messageType','success');
		}
		else {
			return Redirect::back();
		}
	}

	/*
	|--------------------------------------------------------------------------
	| COLOR CODING
	|--------------------------------------------------------------------------
	|
	*/

	/*
	|--------------------------------------------------------------------------
	| REPORTS
	|--------------------------------------------------------------------------
	|
	*/

	/*
	|--------------------------------------------------------------------------
	| ORGANIZATION INFO
	|--------------------------------------------------------------------------
	|
	*/

	/**
	 * Show the view that shows the Org Info in: /organizations/#/info
	 *
	 * @return view
	 */
	public function showOrganizationInfo($org_id) {
		$this->setInitialData();

		$this->viewData['form_title'] = trans('org.edit_organization');
		$this->viewData['form_list_title'] = trans('org.organization_info');
		$this->viewData['form_url_post'] = url('/organizations/' . $org_id . '/save_info');

		$countryRows = SysCountry::all();
		$countries = [];
		foreach($countryRows as $key => $row) {
			$countries[$key] = ['id' => $row->id, 'value' => $row->name];
		}

		$this->viewData['model_cols'] = [
			'id' 										=> 	['type' => 'hidden', 'attr' => ' '],
			'company_name' 					=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
			'address_line_1' 				=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
			'address_line_2' 				=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '],
			'country_id' 						=>  ['type' => 'select', 'select_values' => $countries, 'class' => 'br-select2', 'label' => trans('app.country_id')],
			'country_state' 				=>  ['type' => 'input-text', 'attr' => ' maxlength=50 ', 'label' => trans('app.country_state')],
			'zip_code' 							=>  ['type' => 'input-text', 'attr' => ' maxlength=10 ', 'label' => trans('app.zip_code')],
		];

		$this->viewData['model'] = AppOrganization::where('sys_org_id',$org_id)->get();

		if(count($this->viewData['model']) > 0) {
			$this->viewData['model'] = $this->viewData['model']->toArray()[0];
			$this->viewData['model_id'] = $this->viewData['model']['id'];
		}
		else {
			$this->viewData['model'] = [] ;
			$this->viewData['model_id'] = 0;
		}

		return view('brcode.organizations.info')->with('viewData', $this->viewData);
	}

	/**
	 * Save the organization's info
	 *
	 * @return redirect('organizations/#/info')
	 */
	public function saveOrganizationInfo($org_id) {
		$colPrefix = $this->columnPrefix;
		$input = Request::all();

		$rules = [
						$colPrefix.'company_name' 			=> 'required|min:3|max:50',
						$colPrefix.'address_line_1' 		=> 'required|min:3|max:50',
						$colPrefix.'address_line_2' 		=> 'max:50',
						$colPrefix.'country_id' 				=> 'required',
						$colPrefix.'country_state' 			=> 'max:50',
						$colPrefix.'zip_code' 					=> 'max:10',
					];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors());
		}
		else {

			$input['add_' . 'sys_org_id'] = $org_id;

			$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

			$id = saveModel($id, $model, 'App\\AppOrganization',$subModelExists, $input, $colPrefix, $new);

			SysOrganization::where('id', $org_id)
				->update(['name' => $model->company_name]);

			$message = trans('org.organization_info_modified');

			return redirect('/organizations/' . $org_id . '/info')->with('message',$message)->with('messageType','success');

		}
	}
	/*
	|--------------------------------------------------------------------------
	| MY USERS
	|--------------------------------------------------------------------------
	|
	*/
	/**
	 * Show the view that list org users in: /organizations/#/users
	 *
	 * @return view
	 */
	public function showUsers($org_id) {
		$this->setInitialData();

		$this->viewData['form_title'] = trans('org.users');
		$this->viewData['form_url_add'] = url('/organizations/'.$org_id.'/user');
		$this->viewData['form_url_dt'] = url('/organizations/'.$org_id.'/users/list-dt');

		$this->viewData['table_cols'] = ['app.col_id', 'app.col_first_name', 'app.col_last_name',
			'app.col_email', 'app.col_user_name', 'app.col_is_enabled'];

		return view('brcode.organizations.users')->with('viewData', $this->viewData);
	}

	/**
	 * Create a DataTable output for org users' table in:
	 * /organizations/#/users
	 *
	 * @return DataTable
	 */
	public function dtUsers($org_id) {
		$orgUsers = DB::table('appOrgUsers')
		->select(['appOrgUsers.id', 'sysUsers.first_name', 'sysUsers.last_name', 'sysUsers.email',
		'sysUsers.user_name', 'sysUsers.is_enabled'])
		->join('sysUsers','sysUsers.id','=','appOrgUsers.sys_user_id')
		->where('sys_org_id',$org_id)
		->whereNull('appOrgUsers.deleted_at');

		return Datatables::of($orgUsers)
			->addColumn('action', function ($orgUsers) use($org_id) {
              return '<a href="' . url('/organizations/'.$org_id.'/user/' .$orgUsers->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
          })
		->make();
	}

	/**
	 * Show the view for a org's user: /organizations/#/user
	 *
	 * @return view
	 */
	public function showUser($org_id, $user_id = 0) {
		$this->setInitialData();

		$this->viewData['form_title'] = ($user_id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('org.user');
		$this->viewData['form_list_title'] = trans('org.users');
		$this->viewData['form_url_list'] = url('/organizations/'.$org_id.'/users');
		$this->viewData['form_url_post'] = url('/organizations/'.$org_id.'/save_user');
		$this->viewData['form_url_post_delete'] = url('/organizations/'.$org_id.'/delete_user');
		$this->viewData['sub_model_title'] = trans('app.roles');
		$this->viewData['sub_model_view'] = 'list';

		if($user_id > 0) {
			$this->viewData['model'] = AppOrgUser::ofUser($org_id, $user_id)->get();

			// Load sub models
			$this->viewData['sub_model'] = AppOrgUserRole::where('sys_org_id',$org_id)
				->where('org_user_id',$user_id)->get()->toArray();

			$this->viewData['model'] = $this->viewData['model']->toArray()[0];
			$this->viewData['model_id'] = $user_id;
		}

		$this->viewData['model_cols'] = [
			'sys_user_id' 				=> 	['type' => 'hidden', 'attr' => ' value="" '],
			'rel_first_name' 			=>  ['type' => 'input-text', 'attr' => ' maxlength=50 ', 'label' => trans('app.col_first_name')],
			'rel_last_name' 			=>  ['type' => 'input-text', 'attr' => ' maxlength=50 ', 'label' => trans('app.col_last_name')],
			'rel_email' 					=>  ['type' => 'input-email', 'attr' => ' maxlength=50 '.($user_id>0?'DISABLED="DISABLED"':'').' ', 'label' => trans('app.col_email')],
			'rel_user_name' 			=>  ['type' => 'input-text', 'attr' => ' maxlength=50 '.($user_id>0?'DISABLED="DISABLED"':'').' ', 'label' => trans('app.col_user_name')],
			'rel_password' 				=>  ['type' => 'input-password', 'attr' => ' maxlength=50 ', 'label' => trans('app.col_password')],
			'rel_password_confirmation' =>  ['type' => 'input-password', 'attr' => ' maxlength=50 ', 'label' => trans('app.col_password_confirmation')],
		];

		$rolesModel = AppOrgRole::where('sys_org_id',$org_id)->get();
		$roles = [];

		foreach($rolesModel as $key => $row) {
			$roles[$key] = ['id' => $row->id, 'value' => $row->name];
		}

		// For submodel view = list
		$this->viewData['sub_model_list'] = [
			'org_role_id', // Column
			$roles,			// Array
			trans('org.col_org_role_id'),
		];

		return view('brcode.organizations.user')->with('viewData', $this->viewData);
	}

	/**
	 * Save a role
	 *
	 * @return redirect(back if fails | /organizations/#/roles if success)
	 */
	public function saveUser($org_id) {
		$colPrefix = $this->columnPrefix;

		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;
		$user_id = $input[$colPrefix.'sys_user_id'] > 0 ? $input[$colPrefix.'sys_user_id'] : 0;

		$rules = [
						$colPrefix.'rel_first_name' 				=> 'required|min:3|max:50',
						$colPrefix.'rel_last_name' 					=> 'required|min:3|max:50',
						$colPrefix.'rel_password' 					=>  ($id > 0 ? '' : 'required|') .'confirmed',
					];

		if($id == 0) {
			$rules[$colPrefix.'rel_email'] = 'required|min:3|max:50|unique:sysUsers,email' . ($id > 0 ? ','.$user_id : '');
			$rules[$colPrefix.'rel_user_name'] = 'required|min:3|max:50|unique:sysUsers,user_name' . ($id > 0 ? ','.$user_id : '');
		}

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {

			$new = true;

			if(strlen($input[$colPrefix.'rel_password']) > 0) {
				$input[$colPrefix.'rel_password'] = Hash::make($input[$colPrefix.'rel_password']);
			}
			else {
				unset($input[$colPrefix.'rel_password']);
			}

			unset($input[$colPrefix.'rel_password_confirmation']);

			$userData = [];
			$data = [];

			foreach($input as $key => $value) {
				if(substr(str_replace($colPrefix,'',$key),0,4) == 'rel_') {
					$userData[str_replace('rel_','',$key)] = $value;
				}
				else {
					$data[$key] = $value;
				}
			}

			// Store System User in System Tables
			// Save user in main user's table
			$user_id = saveModel($user_id, $model, 'App\\SysUser',$subModelExists, $userData, $colPrefix, $new);

			// Save user role to be #3 in main user roles table
			if($new) {
				$roleData['sub0_'.$colPrefix.'role_id'] = ['0'=>3];
				saveSubModel($new, $user_id, 'user_id', 'App\\SysUser', 'roles', 'role_id', 'App\\SysUserRoles', $colPrefix, $roleData);
			}

			// Store user relation in Organization Tables
			// Save the user information
			$data[$colPrefix.'sys_user_id'] = $user_id;
			$data[$colPrefix.'sys_org_id'] 	= $org_id;
			$model = null;
			$org_user_id = saveModel($id, $model, 'App\\AppOrgUser', $subModelExists, $data, $colPrefix, $new);

			$roleData = [];
			$roleData['sub0_'.$colPrefix.'org_role_id'] = isset($input['sub0_'.$colPrefix.'org_role_id']) ? $input['sub0_'.$colPrefix.'org_role_id'] : [] ;
			$otherVals = [];
			$otherVals['sys_org_id'] = $org_id;
			$otherVals['sys_user_id'] = $user_id;
			saveSubModel($new, $org_user_id, 'org_user_id', 'App\\AppOrgUser', 'roles', 'org_role_id', 'App\\AppOrgUserRole', $colPrefix, $roleData, 0, $otherVals);

			$message = trans('org.user') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/organizations/'.$org_id.'/users')->with('message',$message)->with('messageType','success');

		}
	}

	/**
 	 * Delete a role
 	 *
 	 * @return redirect(back if fails | /organizations/#/roles if success)
 	 */

	/*
	|--------------------------------------------------------------------------
	| MY ROLES
	|--------------------------------------------------------------------------
	|
	*/
	/**
	 * Show the view that list org roles in: /organizations/#/roles
	 *
	 * @return view
	 */
	public function showRoles($org_id) {
		$this->setInitialData();

		$this->viewData['form_title'] = trans('org.roles');
		$this->viewData['form_url_add'] = url('/organizations/'.$org_id.'/role');
		$this->viewData['form_url_dt'] = url('/organizations/'.$org_id.'/roles/list-dt');

		$this->viewData['table_cols'] = ['app.col_id', 'app.col_name', 'app.users',
			'org.airfields'];

		return view('brcode.organizations.roles')->with('viewData', $this->viewData);
	}

	/**
	 * Create a DataTable output for org roles' table in:
	 * /organizations/#/roles
	 *
	 * @return DataTable
	 */
	public function dtRoles($org_id) {
		$orgRoles = DB::table('appOrgRoles')
		->select(['appOrgRoles.id', 'appOrgRoles.name',
			DB::raw('(SELECT count(1) FROM appOrgUserRoles u WHERE u.sys_org_id = '.$org_id.' AND u.org_role_id = appOrgRoles.id) users'),
			DB::raw('(SELECT count(1) FROM appOrgAirfieldRoles a WHERE a.sys_org_id = '.$org_id.' AND a.org_role_id = appOrgRoles.id) airfields'),
		])
		->where('appOrgRoles.sys_org_id',$org_id)
		->whereNull('appOrgRoles.deleted_at');

    return Datatables::of($orgRoles)
			->addColumn('action', function ($orgRoles) use($org_id) {
              return '<a href="' . url('/organizations/'.$org_id.'/role/' .$orgRoles->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> '.trans('app.edit').'</a>';
          })
			->make();
	}

	/**
	 * Show the view for a org's role: /organizations/#/role
	 *
	 * @return view
	 */
	public function showRole($org_id, $role_id = 0) {
		$this->setInitialData();

		$this->viewData['form_title'] = ($role_id > 0 ? trans('app.edit_existing') : trans('app.add_new')) . ' ' . trans('org.role');
		$this->viewData['form_list_title'] = trans('org.roles');
		$this->viewData['form_url_list'] = url('/organizations/'.$org_id.'/roles');
		$this->viewData['form_url_post'] = url('/organizations/'.$org_id.'/save_role');
		$this->viewData['form_url_post_delete'] = url('/organizations/'.$org_id.'/delete_role');


		$subModel1 = [
			'sub_model_title' => trans('app.users'),
			'sub_model_view' => 'table',
		];

		$subModel2 = [
			'sub_model_title' => trans('org.airfields'),
			'sub_model_view' => 'table',
		];

		if($role_id > 0) {
			$this->viewData['model'] 		= AppOrgRole::find($role_id);
			$this->viewData['model_id'] = $role_id;

			// Load sub models
			$subModel1['sub_model'] = $this->viewData['model']->users->toArray();
			$subModel2['sub_model'] = $this->viewData['model']->airfields->toArray();

			$this->viewData['model'] 		= $this->viewData['model']->toArray();
		}

		$this->viewData['model_cols'] = ['id' => ['type' => 'hidden', 'attr' => ' '],
			'name' 									=>  ['type' => 'input-text', 'attr' => ' maxlength=50 ', 'label' => trans('app.col_name')],
			'is_enabled' 						=>  ['type' => 'select',
				'select_values' 	=> [ ['id' => 'Y', 'value' => trans('app.yes')],['id' => 'N', 'value' => trans('app.no')] ],
				'label' 					=> trans('app.col_is_enabled'),
				],
		];

		$userModel = AppOrgUser::ofUser($org_id)->get();
		$users = [];

		foreach($userModel as $key => $row) {
			$users[$key] = ['id' => $row->id, 'value' => $row->rel_first_name . ' ' . $row->rel_last_name];
		}

		$airfieldModel = AppOrgAirfield::where('sys_org_id', $org_id)->get();
		$airfields = [];

		foreach($airfieldModel as $key => $row) {
			$airfields[$key] = ['id' => $row->id, 'value' => $row->name];
		}

		// For submodel view = table
		$subModel1['sub_model_cols'] = [
			'id' 								=> 	['type' => 'hidden', 'attr' => ' '],
			'org_user_id' 			=>	['type' => 'select','select_values' => $users,'class'=>'br-select2','label' => trans('org.col_org_user_id')],
		];

		$subModel2['sub_model_cols'] = [
			'id' 								=> 	['type' => 'hidden', 'attr' => ' '],
			'org_airfield_id' 	=>  ['type' => 'select','select_values' => $airfields,'class'=>'br-select2','label' => trans('org.col_org_airfield_id')],
		];

		$this->viewData['sub_models'] = array($subModel1, $subModel2);

		return view('brcode.organizations.role')->with('viewData', $this->viewData);
	}

	/**
	 * Save a role
	 *
	 * @return redirect(back if fails | /organizations/#/roles if success)
	 */
	public function saveRole($org_id) {
		$colPrefix = $this->columnPrefix;

		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		$rules = [
						$colPrefix.'name' 					=> 'required|min:3|max:50',
						$colPrefix.'is_enabled' 		=> 'required',
					];

		$input = Request::all();

		$v = Validator::make($input, $rules);

		if($v->fails()) {
			return Redirect::back()
				->withErrors($v->errors()) // send back all errors to the login form
				->withInput(Request::all());
		}
		else {

			$input['add_' . 'sys_org_id'] = $org_id;

			$id = saveModel($id, $model, 'App\\AppOrgRole',$subModelExists, $input, $colPrefix, $new);

			saveSubModel($new, $id, 'org_role_id', 'App\\AppOrgRole', 'users', 'org_user_id', 'App\\AppOrgUserRole', $colPrefix, $input, 0, ['sys_org_id' => $org_id]);
			saveSubModel($new, $id, 'org_role_id', 'App\\AppOrgRole', 'airfields', 'org_airfield_id', 'App\\AppOrgAirfieldRole', $colPrefix, $input, 1, ['sys_org_id' => $org_id]);

			$message = trans('org.role') . ' ' . ($new ? trans('app.created_successfully') : trans('app.modified_successfully'));

			return redirect('/organizations/'.$org_id.'/roles')->with('message',$message)->with('messageType','success');

		}
	}

	/**
 	 * Delete a role
 	 *
 	 * @return redirect(back if fails | /organizations/#/roles if success)
 	 */
	public function deleteRole($org_id) {
		$input = Request::all();

		$id = $input['model_id'] > 0 ? $input['model_id'] : 0;

		if($id > 0) {

			$role = AppOrgRole::find($id);
			$role->modified_by 	= Auth::id();
			$role->deleted_at 	= date("Y-m-d H:i:s");
			$role->save();
			//$role->delete();

			$deletedRows = AppOrgUserRole::where('sys_org_id',$org_id)->delete();
			$deletedRows = AppOrgAirfieldRole::where('sys_org_id',$org_id)->delete();

			$message = trans('org.role') . ' ' . trans('app.deleted_successfully');

			return redirect('/organizations/'.$org_id.'/roles')->with('message',$message)->with('messageType','success');
		}
		else {
			return Redirect::back();
		}
	}

}
