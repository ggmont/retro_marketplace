<?php

namespace App\Http\Controllers;

use Request;
use App\SysUser;
use App\SysUserSocial;
use Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request as BaseRequest;

class ProfileController extends Controller
{
    public function UpdateProfileImage(Request $request, $id)
    {
        $usuario = SysUser::find($id);


        if ($usuario) {
            $profile_image = Request::file('profile_picture');

            $filename  = 'profile_picture' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $profile_image->getClientOriginalExtension();
            $path = public_path('/uploads/profile-images/' . $filename);
            $dateFolder = date('Y') . '/' . date('m') . '/' . date('d') . '/';

            $directory = $path . $dateFolder;
            Image::make($profile_image->getRealPath())->resize(500, 500)->save($path);
            $usuario->profile_picture = '/uploads/profile-images/' . $filename;
            $usuario->save();
            $usuario->update(['profile_picture' => $filename]);
            return back()
                ->with('success', 'You have successfully upload image.');
        }

        return back()
            ->with('success', 'You have successfully upload image.');
    }

    public function UpdateCoverImage(Request $request, $id)
    {
        $user = SysUser::find($id);


        if ($user) {
            $cover_image = Request::file('cover_picture');

            $filename  = 'cover_picture' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $cover_image->getClientOriginalExtension();

            $path = public_path('/uploads/cover-images/' . $filename);
            Image::make($cover_image->getRealPath())->resize(1098, 500)->save($path);
            $user->cover_picture = '/uploads/cover-images/' . $filename;
            $user->save();
            $user->update(['cover_picture' => $filename]);
            return back()
                ->with('success', 'You have successfully upload image.');
        }

        return back()
            ->with('success', 'You have successfully upload image.');
    }

    public function SocialUpdate(BaseRequest $request)
    {

        $user = Auth::user();
        $user->terms_and_conditions = $request->input('terms_and_conditions');
        //dd($user->terms_and_conditions);
        if ($user->save()) {


            return redirect('/account/profile')->with('error', 'hola');
        } else {
            return redirect('/account/profile')->with('error', 'hola');
        }
    }

    public function SocialNetworkStore(BaseRequest $request)
    {
        $prueba               = new SysUserSocial;
        $prueba->user_id = auth()->user()->id;
        $prueba->youtube = $request->youtube;
        $prueba->discord = $request->discord;
        $prueba->instagram = $request->instagram;
        if (strpos($prueba->youtube, 'https://www.youtube.com/channel/') !== false || strpos($prueba->discord, 'https://discord.com/channels/@me') !== false || strpos($prueba->instagram, 'https://www.instagram.com/') !== false) {
            $prueba->save();
            return redirect('/account/profile')->with([
                'flash_class'     => 'alert-success',
                'flash_message'   => 'Redes Sociales Actualizadas.',
                'flash_important' => true,
            ]);
        } else {
            return redirect('/account/profile')->with([
                'flash_class'     => 'alert-danger',
                'flash_message'   => 'La Red Social no Coincide con los campos.',
                'flash_important' => true,
            ]);
        }
    }

    public function SocialNetworkUpdate(BaseRequest $request, $id)
    {

        $prueba = SysUserSocial::where('user_id', $id);
        $prueba->youtube = $request->youtube;
        $prueba->discord = $request->discord;
        $prueba->instagram = $request->instagram;
        if (strpos($prueba->youtube, 'https://www.youtube.com/channel/') !== false || strpos($prueba->discord, 'https://discord.com/channels/@me') !== false || strpos($prueba->instagram, 'https://www.instagram.com/') !== false) {

            SysUserSocial::where('user_id', $id)
                ->update(
                    [
                        'youtube' => $request->input('youtube'),
                        'discord' => $request->input('discord'),
                        'instagram' => $request->input('instagram')
                    ]
                );
            return redirect('/account/profile')->with([
                'flash_class'     => 'alert-success',
                'flash_message'   => 'Redes Sociales Actualizadas.',
                'flash_important' => true,
            ]);
        } else {
            return redirect('/account/profile')->with([
                'flash_class'     => 'alert-danger',
                'flash_message'   => 'La Red Social no Coincide con los campos.',
                'flash_important' => true,
            ]);
        }
    }

    public function SocialNetworkUpdatePrueba(BaseRequest $request, $id)
    {
        $prueba = SysUserSocial::where('user_id', $id);


        $prueba->youtube = $request->youtube;
        dd($prueba->youtube);
        $prueba->discord = $request->youtube;
        $prueba->instagram = $request->youtube;

        $prueba->save();

        return redirect('/account/profile')->with('error', 'hola');
    }

    public function SocialNetwork(BaseRequest $request)
    {

        if ($request->has('link')) {
            for ($i = 0; $i < count($request->link); $i++) {
                $prueba               = new SysUserSocial;
                $prueba->user_id = auth()->user()->id;
                $prueba->link = $request->link[$i];
                if (strpos($prueba->link, 'https://www.instagram.com/') !== false) {
                    $prueba->tipo = 'Instagram';
                    //dd($prueba->tipo);
                    $prueba->save();
                    return redirect('/account/profile')->with('error', 'hola');
                } else if (strpos($prueba->link, 'https://www.youtube.com/channel/') !== false) {
                    $prueba->tipo = 'Youtube';
                    //dd('aja');
                    $prueba->save();
                    return redirect('/account/profile')->with('error', 'hola');
                } else if (strpos($prueba->link, 'https://discord.com/channel/') !== false) {
                    $prueba->tipo = 'Discord';
                    $prueba->save();
                    return redirect('/account/profile')->with('error', 'hola');
                }
            }
            return redirect('/account/profile')->with('error', 'hola');
        } else {
            return redirect('/account/profile')->with('error', 'hola');
        }
    }
}
