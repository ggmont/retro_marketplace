<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AppConversation;
use App\AppMessage;
use App\SysUser;
use App\ChMessage as Message;
use App\ChFavorite as Favorite;
use Chatify\Facades\ChatifyMessenger as Chatify;
use Auth;

class ConversationController extends Controller
{



	public function storeProfile(Request $request)
	{
		//dd($request->all());
		$newContact = SysUser::where('user_name', $request->name)->select('id', 'user_name')->first();

		if (AppConversation::where('alias_user_id', auth()->id())->where('alias_contact_id', $newContact->id)->first()) {
 
			$data = new Message;

			$data->from_id = auth()->id();
			$data->to_id = $newContact->id;
			$data->body = $request->content;
			$data->seen = 0;

			if ($data->save()) {
				AppConversation::where('alias_user_id', auth()->id())->where('alias_contact_id', $newContact->id)->first()
					->update(
						[
							'last_message' => $request->content,
							'seen_user_id' => 'N',
							'seen_contact_id' => 'Y'
						]
					);
				return redirect('/chatrgm/' . $newContact->id);
			}
		} elseif (AppConversation::where('alias_contact_id', auth()->id())->where('alias_user_id', $newContact->id)->first()) {

			$data = new Message;
			$data->from_id = auth()->id();
			$data->to_id = $newContact->id;
			$data->body = $request->content;
			$data->seen = 0;

			if ($data->save()) {
				AppConversation::where('alias_contact_id', auth()->id())->where('alias_user_id', $newContact->id)->first()
					->update(
						[
							'last_message' =>  $request->content,
							'seen_user_id' => 'Y',
							'seen_contact_id' => 'N'
						]
					);
				return redirect('/chatrgm/' . $newContact->id);
			}
		} else {
			$conversation = new AppConversation();

			$conversation->alias_user_id = auth()->id();

			$conversation->last_message = 'Nueva Conversacion Activa';

			$conversation->alias_contact_id = $newContact->id;

			$conversation->seen_user_id = 'Y';

			$conversation->seen_contact_id = 'N';

			//dd('aviso , estamos en mantenimiento 2');

			if ($conversation->save()) {
				$data = new Message;
				$data->from_id = auth()->id();
				$data->to_id = $newContact->id;
				$data->body = $request->content;
				$data->seen = 0;
				$data->save();

				return redirect('/chatrgm/' . $newContact->id);
			}
		}
	}

	public function storeProfileChat(Request $request)
	{

		$newContact = SysUser::where('id', $request->name)->select('id', 'user_name')->first();

		if (AppConversation::where('alias_user_id', auth()->id())->where('alias_contact_id', $newContact->id)->first()) {
 
			$data = new Message;

			$data->from_id = auth()->id();
			$data->to_id = $newContact->id;
			$data->body = $request->content;
			$data->seen = 0;

			if ($data->save()) {
				AppConversation::where('alias_user_id', auth()->id())->where('alias_contact_id', $newContact->id)->first()
					->update(
						[
							'last_message' => $request->content,
							'seen_user_id' => 'N',
							'seen_contact_id' => 'Y'
						]
					);
				return redirect('/chatrgm/' . $newContact->id);
			}
		} elseif (AppConversation::where('alias_contact_id', auth()->id())->where('alias_user_id', $newContact->id)->first()) {

			$data = new Message;
			$data->from_id = auth()->id();
			$data->to_id = $newContact->id;
			$data->body = $request->content;
			$data->seen = 0;

			if ($data->save()) {
				AppConversation::where('alias_contact_id', auth()->id())->where('alias_user_id', $newContact->id)->first()
					->update(
						[
							'last_message' =>  $request->content,
							'seen_user_id' => 'Y',
							'seen_contact_id' => 'N'
						]
					);
				return redirect('/chatrgm/' . $newContact->id);
			}
		} else {
			$conversation = new AppConversation();

			$conversation->alias_user_id = auth()->id();

			$conversation->last_message = 'Nuevo chat activo';

			$conversation->alias_contact_id = $newContact->id;

			$conversation->seen_user_id = 'Y';

			$conversation->seen_contact_id = 'N';

			//dd('aviso , estamos en mantenimiento');

			if ($conversation->save()) {
				$data = new Message;
				$data->from_id = auth()->id();
				$data->to_id = $newContact->id;
				$data->body = $request->content;
				$data->seen = 0;
				$data->save();

				return redirect('/chatrgm/' . $newContact->id);
			}
		}
	}
}
