<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;

class LocaleController extends Controller
{
    public function setLocale(Request $request, $locale)
    {
        if (in_array($locale, Config::get('app.locales'))) {
            session(['locale' => $locale]);
            Cookie::queue('LG', $locale, 2628000);
        }
        return redirect()->back();
    }
}
