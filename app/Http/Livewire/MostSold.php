<?php

namespace App\Http\Livewire;
use App\AppOrgOrderDetail;
use Livewire\Component;

class MostSold extends Component
{
    public function render()
    {
        return view('livewire.most-sold', [
            'inventory_most_sold'     => AppOrgOrderDetail::ofMostSoldPro(10)->get()
        ]);
    }
  
}
