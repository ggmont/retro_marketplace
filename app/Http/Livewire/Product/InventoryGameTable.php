<?php

namespace App\Http\Livewire\Product;

use App\AppOrgProduct;
use App\SysDictionary;
use App\AppOrgUserInventory;
use App\SysCountry;
use Livewire\Component;
use Livewire\WithPagination;

class InventoryGameTable extends Component
{
    use WithPagination;

    public $selCity = '';
    public $cities = [
        'Rajkot',
        'Surat',
        'Baroda',
    ];

    protected $queryString = [
        'search' => ['except' => ''],
        'search_country' => ['except' => ''],
    ];



    public $deleteId = '';
    public $perPage = '50';
    public $prueba;
    public $articulo;
    public $search = '';
    public $search_box = '';
    public $search_manual = '';
    public $search_game = '';
    public $search_extra = '';
    public $search_cover = '';
    public $search_inside = '';
    public $search_country = '';
    public $viewData;
    public $update;


    public function updatingSearchbox()
    {
        $this->resetPage();
        $this->emit('game');
    }

    public function updatingPage()
    {
        $this->resetPage();
        $this->emit('game');
    }

    public function updatingSearch()
    {
        $this->resetPage();
        $this->emit('game');
    }

    public function updatingSearchmanual()
    {
        $this->resetPage();
        $this->emit('game');
    }

    public function updatingSearchgame()
    {
        $this->resetPage();
        $this->emit('game');
    }

    public function updatingSearchextra()
    {
        $this->resetPage();
        $this->emit('game');
    }

    public function updatingSearchcover()
    {
        $this->resetPage();
        $this->emit('game');
    }

    public function updatingSearchinside()
    {
        $this->resetPage();
        $this->emit('game');
    }

    public function updatingSearchcountry()
    {
        $this->resetPage();
        $this->emit('game');
    }

    public function mount($producto)
    {
        //$this->prueba = SysUser::findOrFail($user);
        $this->prueba = $producto->id;
        $this->articulo = $producto;
        $this->emit('game');
    }

    public function render()
    {
        $product = $this->articulo;
        $condition = SysDictionary::where('code', 'GAME_STATE')->get(['value_id', 'value', 'parent_id']);
        $country = SysCountry::all();
        //dd($product);
        $seller = AppOrgUserInventory::ofSeller()->where('product_id', $this->prueba)
            ->where('quantity', '>', 0)
            ->where('in_collection', 'N')
            ->where('price', '>', 0)
            ->where('user_name', 'LIKE', "%{$this->search}%")
            ->where('box_condition', 'LIKE', "%{$this->search_box}%")
            ->where('manual_condition', 'LIKE', "%{$this->search_manual}%")
            ->where('cover_condition', 'LIKE', "%{$this->search_cover}%")
            ->where('game_condition', 'LIKE', "%{$this->search_game}%")
            ->where('extra_condition', 'LIKE', "%{$this->search_extra}%")
            ->whereHas('user', function ($q) {
                $q->whereHas('country', function ($q) {
                    $q->where('name', 'LIKE', "%{$this->search_country}%");
                });
            })
            ->orderBy('price', 'ASC')
            ->paginate($this->perPage);
        //dd($seller);
        return view('livewire.product.inventory-game-table', [
            'country' => $country,
            'seller' => $seller,
            'product' => $product,
            'condition' => $condition

        ]);
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function delete()
    {
        AppOrgUserInventory::find($this->deleteId)->delete();
        $this->emit('alert');
    }

    public function clear_condition()
    {
        $this->search_box = '';
        $this->search_manual = '';
        $this->search_game = '';
        $this->search_extra = '';
        $this->search_cover = '';
        $this->search_inside = '';
        $this->emit('game');
    }
}
