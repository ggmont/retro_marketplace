<?php

namespace App\Http\Livewire\Product;

use App\AppOrgProduct;
use App\SysDictionary;
use App\AppOrgUserInventory;
use App\SysCountry;
use Livewire\Component;
use Livewire\WithPagination;

class InventoryPerifericoTable extends Component
{
    use WithPagination;
    public $selCity = '';
    public $cities = [
        'Rajkot',
        'Surat',
        'Baroda',
    ]; 

    protected $queryString = [
        'search' => ['except' => ''],
    ];

    public $deleteId = '';
    public $perPage = '50';
    public $prueba;
    public $articulo;
    public $search = '';
    public $search_box = '';
    public $search_status = '';
    public $search_extra = '';
    public $search_country = '';
    public $viewData;

    public function updatingSearch()
    {
        $this->resetPage();
        $this->emit('periferico');
    }

    public function updatingPage()
    {
        $this->resetPage();
        $this->emit('periferico');
    }

    public function updatingSearchbox()
    {
        $this->resetPage();
        $this->emit('periferico');
    }
    
    public function updatingSearchstatus()
    {
        $this->resetPage();
        $this->emit('periferico');
    }

    public function updatingSearchextra()
    {
        $this->resetPage();
        $this->emit('periferico');
    }

    public function updatingSearchcountry()
    {
        $this->resetPage();
        $this->emit('periferico');
    }

    public function mount($producto)
    {
        //$this->prueba = SysUser::findOrFail($user);
        $this->prueba = $producto->id;
        $this->articulo = $producto;
        $this->emit('periferico');
    }

    public function render()
    {
        $product = $this->articulo;
        $condition = SysDictionary::where('code', 'GAME_STATE')->get(['value_id', 'value', 'parent_id']);
        $country = SysCountry::all();
        //dd($product);
        $seller = AppOrgUserInventory::ofSeller()->where('product_id', $this->prueba)
            ->where('quantity', '>', 0)
            ->where('user_name','LIKE',"%{$this->search}%")
            ->where('box_condition','LIKE',"%{$this->search_box}%")
            ->where('game_condition','LIKE',"%{$this->search_status}%")
            ->where('extra_condition','LIKE',"%{$this->search_extra}%")
            ->whereHas('user', function ($q) {
                $q->whereHas('country', function ($q) {
                    $q->where('name','LIKE',"%{$this->search_country}%");
                });
            })
            ->orderBy('price', 'ASC')
            ->paginate($this->perPage);
        //dd($seller);
        return view('livewire.product.inventory-periferico-table', [

            'country' => $country,
            'seller' => $seller,
            'product' => $product,
            'condition' => $condition  

        ]);
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function delete()
    {
        AppOrgUserInventory::find($this->deleteId)->delete();
        $this->emit('alert');
    }

    public function clear_condition()
    {
        
        $this->search = '';
        $this->search_box = '';
        $this->search_status = ''; 
        $this->search_extra = ''; 
        $this->search_country = '';
        $this->emit('periferico');
    }
 
}
