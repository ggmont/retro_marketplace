<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\AppOrgProduct;
use App\AppOrgUserInventory;
use App\SysDictionary;
use App\SysCountry;

class HomeProduct extends Component
{
    public $totalRecords;
    public $loadAmount = 20;
    public $query = '';
    public $probando = '';
    public $search_sales = '';
    public $search_box = '';
    public $search_manual = '';
    public $search_game = '';
    public $search_extra = '';
    public $search_cover = '';
    public $search_inside = '';
    public $search_country = '';
    public $search_platform = '';
    public $search_category = '';
    public $search_region = '';
    public $search_language = '';
    public $search_media = '';
    public $search_prueba = '';

    protected $listeners = ['postAdded' => 'incrementPostCount'];

    protected $queryString = [
        'query' => ['except' => ''],
        'search_category' => ['except' => ''],
    ];

    public function loadMore()
    {
        if (auth()->guest()) {
            $this->dispatchBrowserEvent('show-session');
        } else {
            $this->loadAmount += 10;
        }
    }

    public function incrementPostCount()
    {
        $this->probando = 'NOT-PRES';
        $this->search_prueba = AppOrgUserInventory::where('box_condition', 'LIKE', "%{$this->probando}%")->get();
        //dd($this->search_prueba);
    }


    public function mount()
    {
        $this->totalRecords = AppOrgUserInventory::where('quantity', '>', 0)->count();
    }

    public function updateQuery()
    {
        $this->query = trim($this->query);
    }

    public function clear()
    {
        $this->query = '';
        $this->search_country = '';
        $this->search_platform = '';
        $this->search_category = '';
        $this->search_region = '';

        $this->emit('prueba');
        $this->emit('clearFilters');
    }

    public function render()
    {
        $condition = SysDictionary::where('code', 'GAME_STATE')->get(['value_id', 'value', 'parent_id']);
        $country = SysCountry::all();
        $inventories_game = AppOrgUserInventory::where('quantity', '>', 0)
            ->whereHas('product', function ($q) {
                $q->whereHas('categoryProd', function ($q) {
                    $q->where('parent_id', 1);
                });
            })->count();
        $inventories_consoles = AppOrgUserInventory::where('quantity', '>', 0)
            ->whereHas('product', function ($q) {
                $q->whereHas('categoryProd', function ($q) {
                    $q->where('parent_id', 2);
                });
            })->count();
        $inventories_perifericos = AppOrgUserInventory::where('quantity', '>', 0)
            ->whereHas('product', function ($q) {
                $q->whereHas('categoryProd', function ($q) {
                    $q->where('parent_id', 3);
                });
            })->count();
        $inventories_accesorios = AppOrgUserInventory::where('quantity', '>', 0)
            ->whereHas('product', function ($q) {
                $q->whereHas('categoryProd', function ($q) {
                    $q->where('parent_id', 4);
                });
            })->count();
        $inventories_merch = AppOrgUserInventory::where('quantity', '>', 0)
            ->whereHas('product', function ($q) {
                $q->whereHas('categoryProd', function ($q) {
                    $q->where('parent_id', 173);
                });
            })->count();
        $platform = SysDictionary::where("code", "GAME_PLATFORM")->get();
        $region = SysDictionary::where("code", "GAME_REGION")->get();
        $media = SysDictionary::where("code", "GAME_SUPPORT")->get();
        $language = SysDictionary::where("code", "GAME_LANGUAGE")->get();

        return view('livewire.home-product', [
            'country' => $country,
            'platform' => $platform,
            'region' => $region,
            'media' => $media,
            'language' => $language,
            'inventories_game' => $inventories_game,
            'inventories_consoles' => $inventories_consoles,
            'inventories_perifericos' => $inventories_perifericos,
            'inventories_accesorios' => $inventories_accesorios,
            'inventories_merch' => $inventories_merch,
            'condition' => $condition
        ])->with(
            'seller',
            AppOrgUserInventory::where('quantity', '>', 0)
            ->where('in_collection', 'N')
            ->whereIn('auction_type', ["0", "1"]) // Solo productos con auction_type 0 o 1
            ->where('auction_type', 'LIKE', "%{$this->search_sales}%")
            ->where('box_condition', 'LIKE', "%{$this->search_box}%")
            ->where('manual_condition', 'LIKE', "%{$this->search_manual}%")
            ->where('cover_condition', 'LIKE', "%{$this->search_cover}%")
            ->where('game_condition', 'LIKE', "%{$this->search_game}%")
            ->where('extra_condition', 'LIKE', "%{$this->search_extra}%")
            ->when($this->query, function ($query) {
                $query->where(function ($query) {
                    $query->where(function ($q) {
                        $q->where('title', 'LIKE', "%{$this->query}%");
                    })->orWhereHas('product', function ($q) {
                        $q->where('name', 'LIKE', "%{$this->query}%");
                    });
                });
            })
            ->when($this->search_platform, function ($query) {
                $query->whereHas('product', function ($q) {
                    $q->where('platform', 'like', '%' . $this->search_platform);
                })->orWhere('platform', 'like', '%' . $this->search_platform);
            })
            ->when($this->search_region, function ($query) {
                $query->whereHas('product', function ($q) {
                    $q->where('region', 'LIKE', "%{$this->search_region}%");
                })->orWhere('region', 'LIKE', "%{$this->search_region}%");
            })            
            ->when($this->search_language, function ($query) {
                $query->whereHas('product', function ($q) {
                    $q->where('language', 'LIKE', "%{$this->search_language}%");
                });
            })
            ->when($this->search_category, function ($query) {
                $query->whereHas('product.categoryProd', function ($q) {
                    $q->where('category_text', 'LIKE', "%{$this->search_category}%");
                });
            })
            ->when($this->search_country, function ($query) {
                return $query->whereHas('user', function ($q) {
                    $q->whereHas('country', function ($q) {
                        $q->where('name', 'LIKE', "%{$this->search_country}%");
                    });
                });
            })
            ->orderBy('id', 'desc')
            ->limit($this->loadAmount)
            ->get()                     
        );
    }
}
