<?php

namespace App\Http\Livewire\Mobile;

use App\AppMessage;
use App\AppConversation;
use Livewire\Component;
use App\SysUser;
use Livewire\WithFileUploads;
use Carbon\Carbon;

class ConversationMobile extends Component
{
    use WithFileUploads;
    public $photo;
    public $iteration;
    public $deleteId = '';
    public $message;
    public $allmessages;
    public $sender;
    public $search = '';

    protected $rules = [
        'photo' => 'nullable|image',
    ];
    
    public function render()
    {
        $beta  =  AppConversation::where('alias_user_id', auth()->user()->id)
            ->whereHas('prueba', function ($q) {
                $q->where('user_name', 'LIKE', "%{$this->search}%");
            })
            ->orWhere('alias_contact_id', auth()->user()->id)
            ->whereHas('user', function ($q) {
                $q->where('user_name', 'LIKE', "%{$this->search}%");
            })
            ->where(
                'del_messages',
                false
            )
            ->where('status_deleted')
            ->orderBy('updated_at', 'desc')
            ->get();


        $messages = AppMessage::with('user')
            ->latest()
            ->take(10)
            ->get()
            ->sortBy('id');


        //dd($beta);

        $users = SysUser::all();
        $sender = $this->sender;
        $prueba = AppMessage::where('alias_from_id', \Auth::user()->id)->where('alias_to_id')->get();
        //dd($prueba);
        $this->allmessages;
        return view('livewire.mobile.conversation-mobile', compact('users', 'sender', 'prueba', 'beta', 'messages'));
    }

    public function mountdata()
    {
        if (isset($this->sender->id)) {
            $this->allmessages = AppMessage::where('alias_from_id', auth()->id())->where('alias_to_id', $this->sender->id)->orWhere('alias_from_id', $this->sender->id)->where('alias_to_id', auth()->id())->orderBy('id', 'desc')->get();

            $not_seen = AppMessage::where('alias_from_id', $this->sender->id)->where('alias_to_id', auth()->id());
            $not_seen->update(['read' => true]);
        }
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function update()
    {
        $conversation = AppConversation::find($this->deleteId);
        $conversation->update([
           'status_deleted' =>  \Auth::user()->id,
        ]);
        
    }

    public function delete()
    {
        AppConversation::find($this->deleteId)->delete();
   
    }
}
