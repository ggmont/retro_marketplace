<?php

namespace App\Http\Livewire;

use App\AppOrgProduct;
use App\SysDictionary;
use App\AppOrgUserInventory;
use Livewire\Component;
use Livewire\WithPagination;

class InventoryProductTable extends Component
{
    use WithPagination;

    protected $queryString = [
        'search' => ['except' => ''],
    ];

    public $perPage = '75';
    public $prueba;
    public $articulo;
    public $search = '';
    public $viewData;



    public function mount($producto)
    {
        //$this->prueba = SysUser::findOrFail($user);
        $this->prueba = $producto->id;
        $this->articulo = $producto;
    }


    public function render()
    {
        $product = $this->articulo;
        //dd($product);
        $seller = AppOrgUserInventory::ofSeller()->where('product_id', $this->prueba)
            ->where('quantity', '>', 0)
            ->where('box_condition','LIKE',"%{$this->search}%")
            ->orderBy('price', 'ASC')
            ->paginate($this->perPage);
        //dd($seller);
        return view('livewire.inventory-product-table', [

            'seller' => $seller,
            'product' => $product

        ]);
    }
}
