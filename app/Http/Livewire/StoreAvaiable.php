<?php

namespace App\Http\Livewire;

use App\SysUser;
use App\SysDictionary;
use App\AppOrgUserInventory;
use App\AppOrgProduct;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Http\Request;

class StoreAvaiable extends Component
{
    use WithPagination;

    public $selected = [
        'quantity' => []
    ];

    protected $queryString = [
		'search_platform' => ['except' => '' ],
        'search_category' => ['except' => '' ],
        'search' => ['except' => '' ],
        'search_region' => ['except' => '' ],
     ];

 

     public $search = '';
     public $search_platform = '';
     public $search_category = '';
     public $search_region = '';
     public $search_language = '';
     public $search_media = '';
     public $search_cash = '';

     public $perPage = '15';

     protected $listeners = ['refreshChildren' => 'refreshMe'];

     public function updatingSearch()
     {
         $this->resetPage();
     }

     public function updatingSearchplatform()
     {
         $this->resetPage();
     }

     public function updatingSearchcategory()
     {
         $this->resetPage();
     }

     public function updatingSearchlanguage()
     {
         $this->resetPage();
     }

     public function updatingSearchmedia()
     {
         $this->resetPage();
     }

     public function updatingSearchregion()
     {
         $this->resetPage();
     }

     public function updatingSearchcash()
     {
         $this->resetPage();
     }

     public function mount(Request $request)
     {
        $this->from = $request->desde;
        $this->to = $request->hasta;
     }

     public function render()
     {
        $region = SysDictionary::where("code", "GAME_REGION")->get();
        $language = SysDictionary::where("code", "GAME_LANGUAGE")->get();
        $platform = SysDictionary::where("code", "GAME_PLATFORM")->get();
        $media = SysDictionary::where("code", "GAME_SUPPORT")->get();

        return view('livewire.store-avaiable', [
            'product'     => AppOrgProduct::where('name','LIKE',"%{$this->search}%")
            ->where('platform','LIKE',"%{$this->search_platform}%")
            ->where('region','LIKE',"%{$this->search_region}%")
            ->where('language','LIKE',"%{$this->search_language}%")
            ->where('media','LIKE',"%{$this->search_media}%")
            ->whereHas('categoryProd', function ($q) {
                $q->where('category_text','LIKE',"%{$this->search_category}%");
            })
            ->whereHas('inventory', function ($a){
                $a->where('quantity','>',0);
            })
            ->orderBy("id", "DESC")
            ->paginate($this->perPage),
            'platform' => $platform,
            'region' => $region,
            'language' => $language,
            'media' => $media
        ]);
     }
 
     public function updatedSelected()
     {
         $this->emit('updatedSidebar');
     }
}
