<?php

namespace App\Http\Livewire;

use Auth;
use DB;
use App\AppOrgOrder;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Collection;

class TransactionsTable extends Component
{
    use WithPagination;

    public $perPage = '75';
    public $search = '';

    public function render()
    {
        $userId = Auth::id();
        $dates = AppOrgOrder::where('buyer_user_id', Auth::id())->orWhere('seller_user_id', Auth::id())->orderBy('updated_at')
        ->get([
            DB::raw('DATE_FORMAT(updated_at, "%m / %Y" ) AS `Dates`'),
        ]);
        $fecha = $dates->unique('Dates');
        //dd($fecha);
    
        $tipos = AppOrgOrder::where('status', '!=', 'CS')->select(DB::raw("IF(`buyer_user_id`='$userId', CONCAT('C', `status`), CONCAT('V', `status`)) as status_type"), "status")->where('buyer_user_id', Auth::id())->orWhere('seller_user_id', Auth::id())->get()->unique('status_type');
        
        $trans = AppOrgOrder::where('paid_out', 'Y')->where(function ($query) {
            $query->where('buyer_user_id', Auth::id())
                  ->orWhere('seller_user_id', Auth::id());
        })->whereIn('status', ['DD','RE','RF','FD','CE','AC','RC','CR','ST','EC','SC','AS','YC','YV'])
        ->select("*", DB::raw("IF(`buyer_user_id`='$userId', CONCAT('C', `status`), CONCAT('V', `status`)) as status_type"))
        ->having('status_type', '!=', "VEC")
        ->orderBy('updated_at', 'desc')->paginate($this->perPage);
        
        
        return view('livewire.transactions-table', [

            'userId' => $userId,
            'dates' => $dates,
            'fecha' => $fecha,
            'tipos' => $tipos,
            'trans' => $trans,

        ]);
    }
}
