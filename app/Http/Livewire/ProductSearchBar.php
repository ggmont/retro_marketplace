<?php

namespace App\Http\Livewire;
use App\AppOrgProduct;
use Livewire\Component;

class ProductSearchBar extends Component
{
    public $query;
    public $products;
    public $highlightIndex;

    public function mount()
    {
        $this->reset();
    }

    public function restart()
    {
        $this->query = '';
        $this->products = [];
        $this->highlightIndex = 0;
    }

    public function incrementHighlight()
    {
        if($this->highlightIndex === count($this->products) - 1) {
            $this->highlightIndex = 0;
            return;
        }
        $this->highlightIndex++;
    }

    public function decrementHighlight()
    {
        if($this->highlightIndex === 0) {
            $this->highlightIndex = count($this->products) - 1;
            return;
        }
        $this->highlightIndex--;
    }

    public function selectProduct()
    {
        $product = $this->products[$this->highlightIndex] ?? null;

        if ($product) {
            $this->redirect(route('product-show', $product['id']));
        }
    }

    public function updatedQuery()
    {
        $this->products = AppOrgProduct::where('name','like','%' . $this->query . '%')
        ->take(10)
        ->with('images')
        ->get();

        //dd($this->products);
    }

    public function render()
    {
        return view('livewire.product-search-bar');
    }
}
