<?php

namespace App\Http\Livewire;
use App\SysUser;
use App\SysUserRoles;
use Livewire\Component;
use Livewire\WithPagination;

class ListUser extends Component
{
	use WithPagination;

    protected $listeners = ['postAdded' => 'incrementPostCount'];

	protected $queryString = [
		'search' => ['except' => '' ],
     ];

    public $accion = "store";
	public $search = '';
	public $perPage = '10';

 
    

    public function updatingSearch()
    {
        $this->resetPage();
        $this->emit('alert');
        //dd($this->emit('alert'));
    }
    
    public function incrementPostCount()
    {
        $this->search_prueba = SysUser::get();
        //dd($this->search_prueba);
    }


 

    public function render()
    {
        $filter_user = SysUserRoles::where('role_id', '!=', 1)->pluck('user_id');
        return view('livewire.list-user', [
            'user'     => SysUser::wherein('id', $filter_user)->groupBy('id')
            ->orderBy("id", "DESC")
            ->where('user_name','LIKE',"%{$this->search}%")
            ->paginate($this->perPage)
        ]);
    }

    public function clear()
    {
    	$this->search = '';
    	$this->page = 1;
    	$this->perPage = '50';
        $this->emit('alert');
    }
}
