<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CollectionPersonall extends Component
{
    public function render()
    {
        return view('livewire.collection-personall');
    }
}
