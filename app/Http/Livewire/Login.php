<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use App\Mail\ActivateAccount;
use App\SysUser;
use App\AppOrgUserInventory;
use App\AppOrgCartUser;
use App\SysUserAccountActivation;
use App\Events\ActionExecuted;
use App\Helpers\PtyCommons;
use App\SysUserPasswordReset;
use Carbon\Carbon;
use Mail;
use Request;
use DB;


class Login extends Component
{
    public $user_name;
    public $password;
    public $remember_me;
    public $rememberMe = true;
    public $passwordFieldType = 'password';
    

    function getUserIP()
	{
		if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') > 0) {
				$addr = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
				return trim($addr[0]);
			} else {
				return $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
		} else {
			return $_SERVER['REMOTE_ADDR'];
		}
	}




    public function doLogin()
    {
        $this->validate([
            'user_name' => 'required',
            'password' => 'required',
        ]);

        $this->validateCredentials();

        $credentials = [
            'user_name' => $this->user_name,
            'password' => $this->password,
        ];

        if (Auth::attempt($credentials)) {
            // Las credenciales son correctas
            if (Auth::user()->is_activated === 'N') {
                // La cuenta aún no está activada
                $email = Auth::user()->email;
                $user = SysUser::where('email', $email)->first();

                if ($user) {
                    $actRequest = SysUserAccountActivation::where('email', $email)->first();

                    if (!$actRequest) {
                        $actRequest = new SysUserPasswordReset();
                    }

                    $actRequest->email = $email;
                    $actRequest->token = str_random(100);
                    $actRequest->save();

                    Mail::to($actRequest->email)->queue(new ActivateAccount($actRequest->token, $user->first_name . ' ' . $user->last_name));

                    event(new ActionExecuted('events.user_activate_acc_sent', null, null, Request::ip(), array('user_name' => $user->email)));
                }

                Auth::logout();

                return redirect('/')->with([
                    'flash_class' => 'alert-danger',
                    'flash_message' => 'Tu cuenta aún no está activada. Se te ha enviado un correo para poder activarla.',
                ]);
            }

            if (Auth::user()->is_enabled === 'N') {
                // La cuenta está desactivada
                Auth::logout();

                return redirect('/')->with([
                    'flash_class' => 'alert-danger',
                    'flash_message' => 'Tu cuenta se encuentra desactivada. Contacte con el soporte.',
                ]);
            }

            // Las credenciales son correctas y la cuenta está activa y habilitada
            Session::put('user_name', $credentials['user_name']);
            Session::put('user_id', Auth::id());
            PtyCommons::setUserEvent(Auth::id(), 'Inicio sesión el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP()]);

            if ($this->remember_me) {
                // Si se seleccionó "Recordarme", se establece una cookie con los datos de inicio de sesión
                $cookie = cookie('remember_me', json_encode(['user_name' => $credentials['user_name'], 'password' => $credentials['password']]), 60 * 24 * 30);

                // La cookie dura 30 días
                return redirect('/')->withCookie($cookie);
            } else {
                // Si no se seleccionó "Recordarme", se establecen los datos de inicio de sesión en la sesión
                Session::put('user_name', $credentials['user_name']);
                Session::put('user_id', Auth::id());
                PtyCommons::setUserEvent(Auth::id(), 'Inicio sesión el :date', ['date' => Carbon::now()->toDateTimeString(), 'ip_user' => $this->getUserIP()]);
            }

            return redirect('/');
        }

    }

    
    protected function validateCredentials()
    {
        $credentials = [
            'user_name' => $this->user_name,
            'password' => $this->password,
        ];

        if (!Auth::attempt($credentials)) {
            $this->addError('user_name', 'Credenciales incorrectas<br>Por favor, inténtalo de nuevo.');
        }
    }

    public function render()
{
    return view('livewire.login')->with([
        'passwordFieldType' => $this->passwordFieldType,
    ]);
}


}
