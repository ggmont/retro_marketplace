<?php

namespace App\Http\Livewire;

use App\ChMessage;
use App\AppMessage;
use App\AppConversation;
use Livewire\Component;
use Auth;
use App\SysUser;
use Livewire\WithFileUploads;
use Carbon\Carbon;

class MenuHome extends Component
{
    protected $listeners = ['refreshComponent' => '$refresh'];

    public function render()
    {
        if(Auth::user()){
            $messages = ChMessage::where('to_id', Auth::user()->id)->where('seen', 0)->get()->unique('from_id');
            $this->emitself('refreshComponent');
            return view('livewire.menu-home', compact('messages'));
          } else {
            return view('livewire.menu-home');
          }
        
    }
}
