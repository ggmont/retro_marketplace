<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AdvanceSearch extends Component
{
    public function render()
    {
        return view('livewire.advance-search');
    }
}
