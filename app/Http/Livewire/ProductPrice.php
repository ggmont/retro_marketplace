<?php

namespace App\Http\Livewire;

use App\AppOrgProduct;
use App\SysDictionary;
use App\AppOrgUserInventory;
use Livewire\Component;
use DB;

class ProductPrice extends Component
{
    public $prueba;
    public $articulo;
    public $viewData;

    public function mount($producto)
    {
        //$this->prueba = SysUser::findOrFail($user);
        $this->prueba = $producto->id;
        $this->articulo = $producto;
    }

    public function render()
    {
        $product = $this->articulo;
        $actones = AppOrgUserInventory::where('product_id', $this->prueba)->where('quantity_sold', '>', 0)->latest()->paginate(5)
        ->groupBy(function ($acetone) {
            return $acetone->updated_at->toDateString();
        });
         
        
        $sell = AppOrgUserInventory::where('product_id', $this->prueba)->where('quantity_sold', '>', 0)
        ->where(function ($query) {
            $query->whereIn('game_condition', ['USED', 'USED-VERY','USED-NEW'])
                  ->orWhereIn('cover_condition', ['NEW'])
                  ->orWhereIn('cover_condition', ['NOT-PRES']);
        })->select(
            "id" ,
            DB::raw("(sum(quantity_sold)) as total_click"),
            DB::raw("(sum(price)) as total_click"),
            DB::raw("(DATE_FORMAT(created_at, '%d-%m-%Y')) as my_date")
            )
            ->orderBy('created_at', 'ASC')
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y')"))
            ->take(15)
            ->get();
        $date_new = AppOrgUserInventory::where('product_id', $this->prueba)->where('quantity_sold', '>', 0)
        ->whereIn('game_condition', ['NEW'])->select(
            "id" ,
            DB::raw("(sum(price)) / COUNT(product_id) as price"),
            DB::raw("(DATE_FORMAT(created_at, '%d-%m-%Y')) as my_date"),
            DB::raw('game_condition as game_condition')
            )
            ->orderBy('created_at', 'ASC')
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y')"))
            ->take(15)
            ->get();
        //dd($visitors);
        $date_used = AppOrgUserInventory::where('product_id', $this->prueba)->where('quantity_sold', '>', 0)
        ->whereIn('game_condition', ['USED', 'USED-VERY','USED-NEW'])->select(
            "id" ,
            DB::raw("(sum(price)) / COUNT(product_id) as price"),
            DB::raw("(DATE_FORMAT(created_at, '%d-%m-%Y')) as my_date"),
            DB::raw('game_condition as game_condition')
            )
            ->orderBy('created_at', 'ASC')
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y')"))
            ->take(15)
            ->get();
        //dd($date_used);
        $date_not_pres = AppOrgUserInventory::where('product_id', $this->prueba)->where('quantity_sold', '>', 0)
        ->whereIn('cover_condition', ['NOT-PRES'])->select(
            "id" ,
            DB::raw("(sum(price)) / COUNT(product_id) as price"),
            DB::raw("(DATE_FORMAT(created_at, '%d-%m-%Y')) as my_date"),
            DB::raw('game_condition as game_condition')
            )
            ->orderBy('created_at', 'ASC')
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y')"))
            ->take(15)
            ->get();
        $fecha_general = AppOrgUserInventory::where('product_id', $this->prueba)->where('quantity_sold', '>', 0)
        ->select(
            "id" ,
            DB::raw("(sum(price)) / COUNT(product_id) as price"),
            DB::raw("(DATE_FORMAT(created_at, '%d-%m-%Y')) as my_date"),
            DB::raw('game_condition as game_condition')
            )
            ->orderBy('created_at', 'ASC')
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y')"))
            ->take(15)
            ->get();
        //dd($fecha_general);
        $seller_sold_general = AppOrgUserInventory::where('product_id', $this->prueba)
        ->where('quantity_sold', '>', 0)
        ->orderBy('id', 'ASC')->get();
        //dd($prueba);
        $seller_sold = AppOrgUserInventory::where('product_id', $this->prueba)
        ->where('quantity_sold', '>', 0)
        ->orderBy('id', 'ASC')->take(15)->get();
        //dd($seller_sold);
        $seller_sold_used = AppOrgUserInventory::where('product_id', $this->prueba)
        ->whereIn('game_condition', ['USED', 'USED-VERY','USED-NEW'])
        ->where('quantity_sold', '>', 0)
        ->orderBy('created_at', 'ASC')->take(15)->get();
        //dd($seller_sold_used);
        $seller_sold_used_for = AppOrgUserInventory::where('product_id', $this->prueba)
        ->whereIn('cover_condition', ['USED', 'USED-VERY','USED-NEW'])
        ->where('quantity_sold', '>', 0)->select(
           "id" ,
           DB::raw("(sum(price)) as price"),
           DB::raw("(DATE_FORMAT(created_at, '%d-%m-%Y')) as my_date")
           )
           ->orderBy('created_at')
           ->groupBy(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y')"))
           ->get();
        //dd($seller_sold_used);
        $seller_sold_new = AppOrgUserInventory::where('product_id', $this->prueba)
        ->whereIn('game_condition', ['NEW'])
        ->where('quantity_sold', '>', 0)
        ->groupBy('created_at')
        ->orderBy('created_at', 'ASC')->take(15)->get();
        //dd($seller_sold_new);
       $seller_sold_new_for = AppOrgUserInventory::where('product_id', $this->prueba)
        ->whereIn('cover_condition', ['NEW'])
        ->where('quantity_sold', '>', 0)->select(
           "id" ,
           DB::raw("(sum(price)) as price"),
           DB::raw("(DATE_FORMAT(created_at, '%d-%m-%Y')) as my_date")
           )
           ->orderBy('created_at')
           ->groupBy(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y')"))
           ->get();
         //dd($seller_sold_new);
         $seller_sold_not_pres = AppOrgUserInventory::where('product_id', $this->prueba)
         ->whereIn('cover_condition', ['NOT-PRES'])
         ->where('quantity_sold', '>', 0)->select(
            "id" ,
            DB::raw("(sum(price)) as price"),
            DB::raw("(DATE_FORMAT(created_at, '%d-%m-%Y')) as my_date")
            )
            ->orderBy('created_at', 'ASC')
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y')"))
            ->get();
        //dd($seller_sold_not_pres);
        $seller_sold_not_pres_for = AppOrgUserInventory::where('product_id', $this->prueba)
        ->whereIn('cover_condition', ['NOT-PRES'])
        ->where('quantity_sold', '>', 0)->select(
           "id" ,
           DB::raw("(sum(price)) as price"),
           DB::raw("(DATE_FORMAT(created_at, '%d-%m-%Y')) as my_date")
           )
           ->orderBy('created_at', 'ASC')
           ->groupBy(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y')"))
           ->get();
        //dd($seller_sold_not_pres);
        return view('livewire.product-price', [
            'actones' => $actones,
            'product' => $product,
            'date_new' => $date_new,
            'date_used' => $date_used,
            'date_not_pres' => $date_not_pres,
            'fecha_general' => $fecha_general,
            'seller_sold_general' => $seller_sold_general,
            'seller_sold_not_pres' => $seller_sold_not_pres,
            'seller_sold_used' => $seller_sold_used,
            'seller_sold_new' => $seller_sold_new,
            'seller_sold_new_for' => $seller_sold_new_for,
            'seller_sold_used_for' => $seller_sold_used_for,
            'seller_sold_not_pres_for' => $seller_sold_not_pres_for,
        ]);
    }
}
