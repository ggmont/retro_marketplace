<?php

namespace App\Http\Livewire\Mobile\Profile;

use App\SysUser;
use App\AppOrgUserRating;
use App\SysUserSocial;
use App\AppOrgProductImage;
use App\SysUserRoles;
use App\AppOrgProduct;
use App\SysDictionary;
use App\SysCountry;
use App\AppOrgUserInventory;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Livewire\Component;

class RatingProfilePc extends Component
{
    public $viewData,$description,$prueba;
    public $student_id, $name, $email, $phone, $student_edit_id, $student_delete_id;


    public $view_rating_id, $view_rating_name, $view_rating_description, $view_rating_processig,$view_rating_packaging,
    $view_rating_desc_prod,$view_rating_date;


    public $searchTerm;

    public function mount($user)
    {
        //$this->prueba = SysUser::findOrFail($user);
        $this->prueba = $user;
        //$usuario = SysUser::where('user_name', $this->prueba)->first();
        //$ultra = AppOrgUserInventory::where('user_id', $usuario->id);
        //dd($ultra);
        $this->emit('pop');
    }

    public function viewDetails($id)
    {
        $rating = AppOrgUserRating::where('id', $id)->first();
        //dd($student);

        $this->view_rating_id = $rating->id;
        $this->view_rating_name = $rating->buyer->user_name;
        $this->view_rating_date = $rating->created_at;
        $this->view_rating_description = $rating->description;
        $this->view_rating_processig = $rating->processig;
        $this->view_rating_packaging = $rating->packaging;
        $this->view_rating_desc_prod = $rating->desc_prod;


        $this->dispatchBrowserEvent('show-form');
    }

    public function render()
    {
        $usuario = SysUser::where('user_name', $this->prueba)->first();
        $rating = AppOrgUserRating::where('seller_user_id', $usuario->id)->where('processig', '>', 0)->orderBy('created_at','desc')->get();
        $fp1 = $rating->where('processig', 1)->count();
        $fp2 = $rating->where('processig', 2)->count();
        $fp3 = $rating->where('processig', 3)->count();
    
        $fa1 = $rating->where('packaging', 1)->count();
        $fa2 = $rating->where('packaging', 2)->count();
        $fa3 = $rating->where('packaging', 3)->count();
        
        $fd1 = $rating->where('desc_prod', 1)->count();
        $fd2 = $rating->where('desc_prod', 2)->count();
        $fd3 = $rating->where('desc_prod', 3)->count();

        $sc1 = $rating->count() > 0 ? (($fp1 / $rating->count()) + ($fa1 / $rating->count()) + ($fd1 / $rating->count())) /3 : 0;
        //dd($sc1);
        $sc2 = $rating->count() > 0 ? (($fp2 / $rating->count()) + ($fa2 / $rating->count()) + ($fd2 / $rating->count())) /3 : 0;
        $sc3  = $rating->count() > 0 ? (($fp3 / $rating->count()) + ($fa3 / $rating->count()) + ($fd3 / $rating->count())) /3 : 0;

        return view('livewire.mobile.profile.rating-profile-pc', [
            'sc1' => $sc1,
            'sc2' => $sc2,
            'sc3' => $sc3,
            'rating' => AppOrgUserRating::where('seller_user_id',$usuario->id)
            ->where('processig', '>', 0)
            ->orderBy('created_at','desc')
            ->get()
        ]);
    }
}