<?php

namespace App\Http\Livewire;

use App\AppMessage;
use App\AppConversation;
use Livewire\Component;
use App\SysUser;
use Livewire\WithFileUploads;
use Carbon\Carbon;

class Messages extends Component
{
    use WithFileUploads;
    public $photo;
    public $iteration;
    public $deleteId = '';
    public $message;
    public $allmessages;
    public $sender;
    public $search = '';

    protected $rules = [
        'photo' => 'nullable|image',
    ];


    public function render()
    {
        $beta  =  AppConversation::where('alias_user_id', auth()->user()->id)
            ->whereHas('prueba', function ($q) {
                $q->where('user_name', 'LIKE', "%{$this->search}%");
            })
            ->orWhere('alias_contact_id', auth()->user()->id)
            ->whereHas('user', function ($q) {
                $q->where('user_name', 'LIKE', "%{$this->search}%");
            })
            ->where(
                'del_messages',
                false
            )
            ->orderBy('updated_at', 'desc')
            ->get();


        $messages = AppMessage::with('user')
            ->latest()
            ->take(10)
            ->get()
            ->sortBy('id');


        //dd($beta);

        $users = SysUser::all();
        $sender = $this->sender;
        $prueba = AppMessage::where('alias_from_id', \Auth::user()->id)->where('alias_to_id')->get();
        //dd($prueba);
        $this->allmessages;
        return view('livewire.messages', compact('users', 'sender', 'prueba', 'beta', 'messages'));
    }
    public function mountdata()
    {
        if (isset($this->sender->id)) {
            $this->allmessages = AppMessage::where('alias_from_id', auth()->id())->where('alias_to_id', $this->sender->id)->orWhere('alias_from_id', $this->sender->id)->where('alias_to_id', auth()->id())->orderBy('id', 'desc')->get();

            $not_seen = AppMessage::where('alias_from_id', $this->sender->id)->where('alias_to_id', auth()->id());
            $not_seen->update(['read' => true]);
        }
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function delete()
    {
        AppMessage::find($this->deleteId)->delete();
        $this->emit('alert');
    }


    public function resetForm()
    {
        $this->message = '';
    }

    public function SendMessage()
    {
        $this->validate();
        if (AppConversation::where('alias_user_id', auth()->id())->where('alias_contact_id', $this->sender->id)->first()) {
            $data = new AppMessage;
            $prueba_imagen = $this->photo;
            if ($prueba_imagen) {
                $image = $this->photo->store('img', 'public');
                $data->image = $image;
            }
            $data->content = $this->message;
            $data->alias_from_id = auth()->id();
            $data->alias_to_id = $this->sender->id;
            $data->save();

            

            if ($data->save()) {
                AppConversation::where('alias_user_id', auth()->id())->where('alias_contact_id', $this->sender->id)->first()
                    ->update(
                        [
                            'last_message' => $data->content
                        ]
                    );
                $this->resetForm();
                if ($prueba_imagen) {
                    $this->photo=null;
                    $this->iteration++;
                }
            }
        } elseif (AppConversation::where('alias_contact_id', auth()->id())->where('alias_user_id', $this->sender->id)->first()) {

            $data = new AppMessage;
            $prueba_imagen = $this->photo;
            if ($prueba_imagen) {
                $image = $this->photo->store('img', 'public');
                $data->image = $image;
            }
            $data->content = $this->message;
            $data->alias_from_id = auth()->id();
            $data->alias_to_id = $this->sender->id;
            if ($data->save()) {
                AppConversation::where('alias_contact_id', auth()->id())->where('alias_user_id', $this->sender->id)->first()
                    ->update(
                        [
                            'last_message' => $data->content
                        ]
                    );

                $this->resetForm();
                if ($prueba_imagen) {
                    $this->photo=null;
                    $this->iteration++;
                }
            }
        } else {
            $conversation = new AppConversation();

            $conversation->alias_user_id = auth()->id();

            $conversation->alias_contact_id = $this->sender->id;

            $conversation->save();
            $data = new AppMessage;
            $data->content = $this->message;
            $prueba_imagen = $this->photo;
            if ($prueba_imagen) {
                $image = $this->photo->store('img', 'public');
                $data->image = $image;
            }
            $data->alias_from_id = auth()->id();
            $data->alias_to_id = $this->sender->id;
            $data->save();

            $this->resetForm();
            if ($prueba_imagen) {
                $this->photo=null;
                $this->iteration++;
            }
        }
    }

    public function update($userId)
    {
        $user = SysUser::find($userId);

        AppMessage::where('alias_from_id', auth()->id())->where('alias_to_id', $userId)->orWhere('alias_from_id', $userId)->where('alias_to_id', auth()->id())
            ->update(
                [
                    'status' => 0
                ]
            );
    }

    public function getUser($userId)
    {
        $user = SysUser::find($userId);
        //dd($user);
        $this->sender = $user;
        $this->allmessages = AppMessage::where('alias_from_id', auth()->id())->where('alias_to_id', $userId)->orWhere('alias_from_id', $userId)->where('alias_to_id', auth()->id())->orderBy('id', 'desc')->get();
    }
}
