<?php

namespace App\Http\Livewire\PublicTable\Inventory;
use App\SysUser;
use App\SysDictionary;
use App\AppOrgUserInventory;
use Livewire\Component;
use Livewire\WithPagination;

class InventoryPublicConsolaUser extends Component
{
    use WithPagination;

    protected $queryString = [
		'search_platform' => ['except' => '' ],
     ];

     public $deleteId = '';
     public $accion = "store";
     public $search = '';
     public $search_platform = '';
     public $search_box = '';
     public $search_manual = '';
     public $search_game = '';
     public $search_extra = '';
     public $search_cover = '';
     public $search_inside = '';
     public $perPage = '15';
     public $prueba;
     public $nose;
     public $viewData;

     public function updatingSearchplatform()
     {
         $this->resetPage();
         $this->emit('console');
     }

     public function updatingSearchbox()
     {
         $this->resetPage();
         $this->emit('console');
     }

     public function updatingPage()
     {
         $this->resetPage();
         $this->emit('console');
     }
 
     public function updatingSearch()
     {
         $this->resetPage();
         $this->emit('console');
     }
     
     public function updatingSearchmanual()
     {
         $this->resetPage();
         $this->emit('console');
     }
 
     public function updatingSearchgame()
     {
         $this->resetPage();
         $this->emit('console');
     }
 
     public function updatingSearchextra()
     {
         $this->resetPage();
         $this->emit('console');
     }
 
     public function updatingSearchcover()
     {
         $this->resetPage();
         $this->emit('console');
     }
 
     public function updatingSearchinside()
     {
         $this->resetPage();
         $this->emit('console');
     }
 
     public function updatingSearchcountry()
     {
         $this->resetPage();
         $this->emit('console');
     }
    

    public function mount($user)
    {
        //$this->prueba = SysUser::findOrFail($user);
        $this->prueba = $user;
        $this->emit('console');
    }
    

    public function render()
    {
       $prueba = SysDictionary::where('code', 'GAME_PLATFORM')->get(['value_id', 'value', 'parent_id']);
       $condition = SysDictionary::where('code', 'GAME_STATE')->get(['value_id', 'value', 'parent_id']);
        $usuario = SysUser::where('user_name', $this->prueba)->first();
        $this->viewData['platform'] = SysDictionary::where('code', 'GAME_PLATFORM')->get(['value_id', 'value', 'parent_id']);
        $this->viewData['categoria_id'] = 1;
        $viewData = $this->viewData;
        //dd($viewData);
        
        $ultra = AppOrgUserInventory::where('user_id', $usuario->id)
        ->where('in_collection', 'N')
        ->where('box_condition','LIKE',"%{$this->search_box}%")
        ->where('cover_condition','LIKE',"%{$this->search_cover}%")
        ->where('manual_condition','LIKE',"%{$this->search_manual}%")
        ->where('game_condition','LIKE',"%{$this->search_game}%")
        ->where('extra_condition','LIKE',"%{$this->search_extra}%")
        ->where('inside_condition','LIKE',"%{$this->search_inside}%")
        ->whereHas('product', function ($q) {
            $q->where('name','LIKE',"%{$this->search}%");
            $q->where('platform', $this->search_platform);
            $q->whereHas('categoryProd', function ($q) {
                $q->where('parent_id', 2);
            });
        })->paginate($this->perPage);
        //dd($ultra);
        return view('livewire.public-table.inventory.inventory-public-consola-user', [
          'user' => $usuario,
          'ari' => $ultra, 
          'viewData' => $viewData,
          'platform' => $prueba,
          'condition' => $condition  
            
        ]);
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function delete()
    {
        AppOrgUserInventory::find($this->deleteId)->delete();
        $this->emit('alert');
    }

    public function clear()
    {
        $this->search = '';
        $this->page = 1;
        $this->perPage = '15';
        $this->emit('console');
    }

    public function clear_condition()
    {
        $this->search_box = '';
        $this->search_manual = '';
        $this->search_game = '';
        $this->search_extra = '';
        $this->search_cover = '';
        $this->search_inside = '';
        $this->emit('console');
    }
}