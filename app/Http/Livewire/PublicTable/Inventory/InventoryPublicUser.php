<?php

namespace App\Http\Livewire\PublicTable\Inventory;
use App\SysUser;
use App\SysDictionary;
use App\AppOrgUserInventory;
use Livewire\Component;
use Livewire\WithPagination;

class InventoryPublicUser extends Component
{
    use WithPagination;

    protected $queryString = [
		'search_platform' => ['except' => '' ],
     ];
    
    public $deleteId = '';
    public $accion = "store";
    public $search = '';
    public $search_platform = '';
    public $search_box = '';
    public $search_manual = '';
    public $search_game = '';
    public $search_extra = '';
    public $search_cover = '';
    public $search_inside = '';
    public $perPage = '15';
    public $prueba;
    public $nose;
    public $viewData;

    public function updatingSearchplatform()
    {
        $this->resetPage();
        $this->emit('pop');
    }

    public function updatingSearchbox()
    {
        $this->resetPage();
        $this->emit('pop');
    }

    public function updatingSearch()
    {
        $this->resetPage();
        $this->emit('pop');
    }
    
    public function updatingSearchmanual()
    {
        $this->resetPage();
        $this->emit('pop');
    }

    public function updatingSearchgame()
    {
        $this->resetPage();
        $this->emit('pop');
    }

    public function updatingSearchextra()
    {
        $this->resetPage();
        $this->emit('pop');
    }

    public function updatingSearchcover()
    {
        $this->resetPage();
        $this->emit('pop');
    }

    public function updatingSearchinside()
    {
        $this->resetPage();
        $this->emit('pop');
    }

    public function updatingSearchcountry()
    {
        $this->resetPage();
        $this->emit('pop');
    }
    

    public function mount($user)
    {
        //$this->prueba = SysUser::findOrFail($user);
        $this->prueba = $user;
        //$usuario = SysUser::where('user_name', $this->prueba)->first();
        //$ultra = AppOrgUserInventory::where('user_id', $usuario->id);
        //dd($ultra);
        $this->emit('pop');
        
         
    }
    

    public function render()
    {
        $platform = SysDictionary::where('code', 'GAME_PLATFORM')->get(['value_id', 'value', 'parent_id']);
        $condition = SysDictionary::where('code', 'GAME_STATE')->get(['value_id', 'value', 'parent_id']);
        $usuario = SysUser::where('user_name', $this->prueba)->first();
        $this->viewData['categoria_id'] = 1;
        $viewData = $this->viewData;
        //dd($platform);
        
        $ultra = AppOrgUserInventory::where('user_id', $usuario->id)
        ->where('box_condition','LIKE',"%{$this->search_box}%")
        ->where('manual_condition','LIKE',"%{$this->search_manual}%")
        ->where('cover_condition','LIKE',"%{$this->search_cover}%")
        ->where('game_condition','LIKE',"%{$this->search_game}%")
        ->where('extra_condition','LIKE',"%{$this->search_extra}%")
        ->whereHas('product', function ($q) {
            $q->where('name','LIKE',"%{$this->search}%");
            $q->where('platform', $this->search_platform);
            $q->whereHas('categoryProd', function ($q) {
                $q->where('parent_id', 1);
            });
        })
        ->where('in_collection', 'N')
        ->where('quantity','>',0)
        ->whereNull('deleted_at')
        ->orderBy("id", "DESC")
        ->paginate($this->perPage);
        //dd($ultra);
        return view('livewire.public-table.inventory.inventory-public-user', [
          'user' => $usuario,
          'ari' => $ultra, 
          'viewData' => $viewData,
          'platform' => $platform,
          'condition' => $condition 
            
        ]);
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function delete()
    {
        AppOrgUserInventory::find($this->deleteId)->delete();
        $this->emit('alert');
    }

    public function clear()
    {
        $this->search = '';
        $this->page = 1;
        $this->perPage = '5';
        $this->emit('pop');
    }

    public function clear_condition()
    {
        $this->search_box = '';
        $this->search_manual = '';
        $this->search_game = '';
        $this->search_extra = '';
        $this->search_cover = '';
        $this->search_inside = '';
        $this->emit('pop');
    }
}
