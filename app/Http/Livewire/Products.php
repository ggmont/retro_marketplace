<?php

namespace App\Http\Livewire;

use App\AppOrgUserInventory;
use Livewire\Component;

class Products extends Component
{
    protected $selected = [
        'price' => [],
        'categories' => []
    ];

    protected $listeners = ['updatedSidebar' => 'setSelected'];

    public function render()
    {
        $products = AppOrgUserInventory::withFilters(
            $this->selected['price'],
            $this->selected['categories']
        )->get();

        return view('livewire.products', compact('products'));
    }

    public function setSelected($selected)
    {
        $this->selected = $selected;
    }
}
