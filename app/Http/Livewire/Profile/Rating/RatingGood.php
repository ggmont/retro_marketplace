<?php

namespace App\Http\Livewire\Profile\Rating;

use Livewire\Component;
use App\AppOrgUserInventory;
use Auth;
use Livewire\WithPagination;
use App\AppOrgUserRating;

class RatingGood extends Component
{
    use WithPagination;
    public $perPage = '15';
    public $search = '';

    public function mount($usuario)
    {
        //$this->prueba = SysUser::findOrFail($user);
        $this->prueba = $usuario->id; 
        //dd($this->prueba);
    }

    public function render()
    {
        return view('livewire.profile.rating.rating-good', [
            'rating_good' => AppOrgUserRating::where('seller_user_id',$this->prueba)
                ->where('processig', 1)
                ->where('packaging', 1)
                ->where('desc_prod', 1)
                ->orderBy('created_at', 'desc')
                ->paginate($this->perPage)
        ]);
    }
}
