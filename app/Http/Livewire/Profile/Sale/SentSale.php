<?php

namespace App\Http\Livewire\Profile\Sale;

use App\AppOrgOrder;
use Livewire\Component;
use Auth;
use Livewire\WithPagination;

class SentSale extends Component
{
    use WithPagination;


    public $search = '';
    public $perPage = '30';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $sent = AppOrgOrder::where('seller_user_id', Auth::id())
        ->where('status', 'ST')
        ->whereHas('buyer', function ($q) {
            $q->where('user_name', 'LIKE', "%{$this->search}%");
        })
        ->orderBy('id','desc')
        ->paginate($this->perPage); // sent - enviado
        return view('livewire.profile.sale.sent-sale', compact(
            'sent'
            )
        );
    }

        public function clear()
    {
        $this->search = '';
        $this->page = 1;
        $this->perPage = '30';
    }
}
