<?php

namespace App\Http\Livewire\Profile\Sale;

use App\AppOrgOrder;
use Livewire\Component;
use Auth;
use Livewire\WithPagination;

class ArrivedSale extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = '30';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $arrived = AppOrgOrder::where('seller_user_id', Auth::id())
        ->where('status', 'DD')
        ->whereHas('buyer', function ($q) {
            $q->where('user_name', 'LIKE', "%{$this->search}%");
        })
        ->orderBy('id','desc')
        ->paginate($this->perPage); // arrived - entregados
        return view('livewire.profile.sale.arrived-sale', compact(
            'arrived'
            )
        );
    }

    public function clear()
    {
        $this->search = '';
        $this->page = 1;
        $this->perPage = '30';
    }
}
