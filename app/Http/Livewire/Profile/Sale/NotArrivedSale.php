<?php

namespace App\Http\Livewire\Profile\Sale;

use App\AppOrgOrder;
use Livewire\Component;
use Auth;
use Livewire\WithPagination;

class NotArrivedSale extends Component
{
    use WithPagination;


    public $search = '';
    public $perPage = '30';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $notArrived = AppOrgOrder::where('seller_user_id', Auth::id())
        ->where('status', 'ON')
        ->whereHas('seller', function ($q) {
            $q->where('user_name', 'LIKE', "%{$this->search}%");
        })
        ->orderBy('id','desc')
        ->paginate($this->perPage);  // Not arrived - No entregado
        return view('livewire.profile.sale.not-arrived-sale', compact(
            'notArrived'
            )
        );
    }

        public function clear()
    {
        $this->search = '';
        $this->page = 1;
        $this->perPage = '30';
    }
}
