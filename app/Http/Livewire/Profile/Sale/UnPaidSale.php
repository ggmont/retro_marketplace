<?php

namespace App\Http\Livewire\Profile\Sale;

use App\AppOrgOrder;
use Livewire\Component;
use Auth;
use Livewire\WithPagination;

class UnPaidSale extends Component
{
    use WithPagination;
    

    public $search = '';
    public $perPage = '30';

    public function updatingSearch()
    {
        $this->resetPage();
        $this->emit('alert');
    }

    public function mount()
    {
        $this->emit('alert');
    }

    public function render()
    {
        $unpaid = AppOrgOrder::where('seller_user_id', Auth::id())
        ->whereIn('status', ['CW','RP'])
        ->whereHas('buyer', function ($q) {
            $q->where('user_name', 'LIKE', "%{$this->search}%");
        })
        ->orderBy('id','desc')
        ->paginate($this->perPage); //Unpaid - No Pagado
        
 
        return view('livewire.profile.sale.un-paid-sale', compact(
            'unpaid'
            )
        );
    }

    public function clear()
    {
        $this->search = '';
        $this->page = 1;
        $this->perPage = '30';
        $this->emit('alert');
    }
}
