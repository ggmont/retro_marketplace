<?php

namespace App\Http\Livewire\Profile\Profesional;

use Livewire\Component;
use App\AppOrgUserInventory;
use App\SysUser;
use Auth;
use Livewire\WithPagination;
use App\SysDictionary;


class InventoryStarHome extends Component
{
    use WithPagination;

    public $page = 1;
    public $favoriteId = '';
    public $desfavoriteId = '';
    public $deleteId = '';
    public $accion = "store";
    public $search = '';
    public $search_platform = '';
    public $search_box = '';
    public $search_manual = '';
    public $search_game = '';
    public $search_extra = '';
    public $search_cover = '';
    public $perPage = '12';
    public $prueba;
    public $nose;
    public $viewData;

    public function previousPage()
    {
        $this->setPage(max($this->page - 1, 1));
        $this->emit('star');
    }

    public function nextPage()
    {
        $this->setPage($this->page + 1);
        $this->emit('star');
    }

    public function gotoPage($page)
    {
        $this->setPage($page);
        $this->emit('star');
    }

    public function setPage($page)
    {
        $this->page = $page;
        $this->emit('star');
    }

    public function updatingSearchbox()
    {
        $this->resetPage();
        $this->emit('star');
    }

    public function updatingSearch()
    {
        $this->resetPage();
        $this->emit('star');
    }

    public function updatingSearchmanual()
    {
        $this->resetPage();
        $this->emit('star');
    }

    public function updatingSearchgame()
    {
        $this->resetPage();
        $this->emit('star');
    }

    public function updatingSearchextra()
    {
        $this->resetPage();
        $this->emit('star');
    }

    public function updatingSearchcover()
    {
        $this->resetPage();
        $this->emit('star');
    }

    public function updatingSearchinside()
    {
        $this->resetPage();
        $this->emit('star');
    }

    public function updatingSearchplatform()
    {
        $this->resetPage();
        $this->emit('star');
    }

    public function mount($user)
    {
        //$this->prueba = SysUser::findOrFail($user);
        $this->prueba = $user;
        //$usuario = SysUser::where('user_name', $this->prueba)->first();
        //$ultra = AppOrgUserInventory::where('user_id', $usuario->id);
        //dd($ultra);
        $this->emit('pop');
        $this->emit('star');
    }

    public function render()
    {
        $platform = SysDictionary::where('code', 'GAME_PLATFORM')->get(['value_id', 'value', 'parent_id']);
        $condition = SysDictionary::where('code', 'GAME_STATE')->get(['value_id', 'value', 'parent_id']);
        $usuario = SysUser::where('user_name', $this->prueba)->first();
        $viewData = $this->viewData;
        return view('livewire.profile.profesional.inventory-star-home', [
            'platform' => $platform,
            'condition' => $condition,
            'viewData' => $viewData,
            'ari' => AppOrgUserInventory::where('user_id', $usuario->id)
                ->where('quantity', '>', 0)
                ->where('in_collection', 'N')
                ->where('favorite', 1)
                ->where('box_condition', 'LIKE', "%{$this->search_box}%")
                ->where('cover_condition', 'LIKE', "%{$this->search_cover}%")
                ->where('manual_condition', 'LIKE', "%{$this->search_manual}%")
                ->where('game_condition', 'LIKE', "%{$this->search_game}%")
                ->where('extra_condition', 'LIKE', "%{$this->search_extra}%")
                ->whereHas('product', function ($q) {
                    $q->where('name', 'LIKE', "%{$this->search}%");
                    $q->where('platform', 'LIKE', "%{$this->search_platform}%");
                })
                ->whereNull('deleted_at')
                ->orderBy("id", "DESC")
                ->paginate($this->perPage),
        ]);
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function delete()
    {
        AppOrgUserInventory::find($this->deleteId)->delete();
        $this->emit('alert');
        $this->emit('render');
        $this->emit('star');
    }

    public function favoriteId($id)
    {
        $this->favoriteId = $id;
    }

    public function update()
    {
        AppOrgUserInventory::find($this->favoriteId)->update(['favorite' => 1]);
        $this->emit('favorite');
        $this->emit('render');
        $this->emit('star');
    }

    public function desfavoriteId($id)
    {
        $this->desfavoriteId = $id;
    }

    public function update_desfavorite()
    {
        AppOrgUserInventory::find($this->desfavoriteId)->update(['favorite' => 0]);
        $this->emit('desfavorite');
        $this->emit('render');
        $this->emit('star');
    }

    public function clear()
    {
        $this->search = '';
        $this->page = 1;
        $this->perPage = '15';
        $this->emit('star');
    }

    public function clear_condition()
    {
        $this->search_box = '';
        $this->search_manual = '';
        $this->search_game = '';
        $this->search_extra = '';
        $this->search_cover = '';
        $this->emit('star');
    }
}
