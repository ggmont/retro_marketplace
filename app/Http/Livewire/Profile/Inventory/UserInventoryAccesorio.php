<?php

namespace App\Http\Livewire\Profile\Inventory;

use Livewire\Component;
use App\AppOrgUserInventory;
use Auth;
use Livewire\WithPagination;
use App\SysDictionary;

class UserInventoryAccesorio extends Component
{
    use WithPagination;

    protected $queryString = [
        'search' => ['except' => ''],
    ];

    public $favoriteId = '';
    public $desfavoriteId = '';
    public $deleteId = '';
    public $accion = "store";
    public $search = '';
    public $search_platform = '';
    public $search_box = '';
    public $search_status = '';
    public $search_extra = '';
    public $perPage = '30';

    public function updatingSearch()
    {
        $this->resetPage();
        $this->emit('accesorio');
    }

    public function updatingSearchbox()
    {
        $this->resetPage();
        $this->emit('accesorio');
    }
    
    public function updatingSearchstatus()
    {
        $this->resetPage();
        $this->emit('accesorio');
    }

    public function updatingSearchextra()
    {
        $this->resetPage();
        $this->emit('accesorio');
    }

    public function updatingSearchplatform()
    {
        $this->resetPage();
        $this->emit('accesorio');
    }

    public function mount()
    {
        $this->emit('accesorio');    
    }

    public function render()
    {
        $platform = SysDictionary::where('code', 'GAME_PLATFORM')->get(['value_id', 'value', 'parent_id']);
        $condition = SysDictionary::where('code', 'GAME_STATE')->get(['value_id', 'value', 'parent_id']);

        return view('livewire.profile.inventory.user-inventory-accesorio', [
            'ari'     => AppOrgUserInventory::where('user_id', Auth::id())
                ->where('quantity', '>', 0)
                ->where('in_collection', 'N')
                ->where('box_condition','LIKE',"%{$this->search_box}%")
                ->where('game_condition','LIKE',"%{$this->search_status}%")
                ->where('extra_condition','LIKE',"%{$this->search_extra}%")
                ->whereHas('product', function ($q) {
                    $q->where('platform', 'LIKE', "%{$this->search_platform}%");
                    $q->where('name','LIKE',"%{$this->search}%");
                    $q->whereHas('categoryProd', function ($q) {
                        $q->where('parent_id', 4);
                    });
                })
                ->whereNull('deleted_at')
                ->orderBy("id", "DESC")
                ->paginate($this->perPage),
                'platform' => $platform,
                'condition' => $condition
        ]);
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function delete()
    {
        AppOrgUserInventory::find($this->deleteId)->delete();
        $this->emit('alert');
        $this->emit('render');
    }

    public function favoriteId($id)
    {
        $this->favoriteId = $id;
    }

    public function update()
    {
        AppOrgUserInventory::find($this->favoriteId)->update(['favorite' => 1]);
        $this->emit('favorite');
        $this->emit('render');
    }

    public function desfavoriteId($id)
    {
        $this->desfavoriteId = $id;
    }

    public function update_desfavorite()
    {
        AppOrgUserInventory::find($this->desfavoriteId)->update(['favorite' => 0]);
        $this->emit('desfavorite');
        $this->emit('render');
    }

    public function clear()
    {
        $this->search = '';
        $this->search_platform = '';
        $this->page = 1;
        $this->perPage = '15';
        $this->emit('accesorio');
    }

    public function clear_condition()
    {
        $this->search_box = '';
        $this->search_status = '';
        $this->search_extra = '';
        $this->emit('accesorio');
    }
}
