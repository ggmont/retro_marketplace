<?php

namespace App\Http\Livewire\Profile\Inventory;

use Livewire\Component;
use App\AppOrgUserInventory;
use Auth;
use Livewire\WithPagination;
use App\SysDictionary;

class UserInventoryStar extends Component
{
    use WithPagination;

    public $favoriteId = '';
    public $desfavoriteId = '';
    public $deleteId = '';
    public $accion = "store";
    public $search = '';
    public $search_platform = '';
    public $search_box = '';
    public $search_manual = '';
    public $search_game = '';
    public $search_extra = '';
    public $search_cover = '';
    public $perPage = '30';
    public $prueba;
    public $nose;
    public $viewData;
    
    protected $listeners = ['render' => 'render'];

    public function render()
    {
        $platform = SysDictionary::where('code', 'GAME_PLATFORM')->get(['value_id', 'value', 'parent_id']);
        $condition = SysDictionary::where('code', 'GAME_STATE')->get(['value_id', 'value', 'parent_id']);
        return view('livewire.profile.inventory.user-inventory-star', [
            'platform' => $platform,
            'condition' => $condition,
            'ari' => AppOrgUserInventory::where('user_id', Auth::id())
                ->where('quantity', '>', 0)
                ->where('favorite', 1)
                ->where('in_collection', 'N')
                ->where('box_condition', 'LIKE', "%{$this->search_box}%")
                ->where('cover_condition', 'LIKE', "%{$this->search_cover}%")
                ->where('manual_condition', 'LIKE', "%{$this->search_manual}%")
                ->where('game_condition', 'LIKE', "%{$this->search_game}%")
                ->where('extra_condition', 'LIKE', "%{$this->search_extra}%")
                ->whereHas('product', function ($q) {
                    $q->where('name', 'LIKE', "%{$this->search}%");
                    $q->where('platform', 'LIKE', "%{$this->search_platform}%");
                })
                ->whereNull('deleted_at')
                ->orderBy("id", "DESC")
                ->paginate($this->perPage),
        ]);
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function delete()
    {
        AppOrgUserInventory::find($this->deleteId)->delete();
        $this->emit('alert');
    }

    public function favoriteId($id)
    {
        $this->favoriteId = $id;
    }

    public function update()
    {
        AppOrgUserInventory::find($this->favoriteId)->update(['favorite' => 1]);
        $this->emit('favorite');
    }

    public function desfavoriteId($id)
    {
        $this->desfavoriteId = $id;
    }

    public function update_desfavorite()
    {
        AppOrgUserInventory::find($this->desfavoriteId)->update(['favorite' => 0]);
        $this->emit('desfavorite');
    }

    public function clear()
    {
        $this->search = '';
        $this->page = 1;
        $this->perPage = '15';
    }

    public function clear_condition()
    {
        $this->search_box = '';
        $this->search_manual = '';
        $this->search_game = '';
        $this->search_extra = '';
        $this->search_cover = '';
    }
}
