<?php

namespace App\Http\Livewire\Profile\Purchase;

use App\AppOrgOrder;
use Livewire\Component;
use Auth;
use Livewire\WithPagination;

class CancelledPurchase extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = '30';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $cancelled = AppOrgOrder::where('buyer_user_id', Auth::id())
        ->where('status', 'CN')
        ->whereHas('seller', function ($q) {
            $q->where('user_name', 'LIKE', "%{$this->search}%");
        })
        ->orderBy('id','desc')
        ->paginate($this->perPage); //cancelled - cancelados

    
        return view('livewire.profile.purchase.cancelled-purchase', compact(
            'cancelled'
            )
        );
    }

    public function clear()
    {
        $this->search = '';
        $this->page = 1;
        $this->perPage = '5';
    }
}
