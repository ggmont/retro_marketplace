<?php

namespace App\Http\Livewire\Profile\Purchase;

use App\AppOrgOrder;
use Livewire\Component;
use Auth;
use Carbon\Carbon;
use Livewire\WithPagination;

class UnpaidPurchase extends Component
{
    use WithPagination;


    public $search_seller = '';
    public $perPage = '30';

    public function updatingSearchseller()
    {
        $this->resetPage();
        $this->emit('alert');
    }

    public function mount()
    {
        $this->emit('alert');
    }

    public function render()
    {
        $unpaid = AppOrgOrder::where('buyer_user_id', Auth::id())
        ->whereIn('status', ['CW','RP'])
        ->whereHas('seller', function ($q) {
            $q->where('user_name', 'LIKE', "%{$this->search_seller}%");
        })
        ->orderBy('id','desc')
        ->paginate($this->perPage); // Unpaid - No Pagado

        return view('livewire.profile.purchase.unpaid-purchase', compact(
            'unpaid'
            )
        );
    }

        public function clear()
    {
        $this->emit('alert');
        $this->search_seller = '';
        $this->page = 1;
        $this->perPage = '30';
    }
}
