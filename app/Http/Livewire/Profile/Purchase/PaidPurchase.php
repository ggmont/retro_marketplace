<?php

namespace App\Http\Livewire\Profile\Purchase;

use App\AppOrgOrder;
use Livewire\Component;
use Auth;
use Livewire\WithPagination;

class PaidPurchase extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = '30';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $paid = AppOrgOrder::where('buyer_user_id', Auth::id())
        ->whereIn('status', ['CR', 'PC'])
        ->whereHas('seller', function ($q) {
                $q->where('user_name', 'LIKE', "%{$this->search}%");
        })
        ->orderBy('id','desc')
        ->paginate($this->perPage);  //Paid - Pagado
        return view('livewire.profile.purchase.paid-purchase', compact(
            'paid'
            )
        );
    }

        public function clear()
    {
        $this->search = '';
        $this->page = 1;
        $this->perPage = '30';
    }
}
