<?php

namespace App\Http\Livewire\Profile\Purchase;

use App\AppOrgOrder;
use Livewire\Component;
use Auth;
use Livewire\WithPagination;

class ArrivedPurchase extends Component
{
    use WithPagination;


    public $search = '';
    public $perPage = '30';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $arrived = AppOrgOrder::where('buyer_user_id', Auth::id())
        ->where('status', 'DD')
        ->whereHas('seller', function ($q) {
            $q->where('user_name', 'LIKE', "%{$this->search}%");
        })
        ->orderBy('id','desc')
        ->paginate($this->perPage); // arrived - entregados
        return view('livewire.profile.purchase.arrived-purchase', compact(
            'arrived'
            )
        );
    }

        public function clear()
    {
        $this->search = '';
        $this->page = 1;
        $this->perPage = '30';
    }
}
