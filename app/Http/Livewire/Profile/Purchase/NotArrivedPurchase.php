<?php

namespace App\Http\Livewire\Profile\Purchase;

use App\AppOrgOrder;
use Livewire\Component;
use Auth;
use Livewire\WithPagination;

class NotArrivedPurchase extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = '30';

    public function updatingSearch()
    {
        $this->resetPage();
    }
    
    public function render()
    {
        $notArrived = AppOrgOrder::where('seller_user_id', Auth::id())
        ->where('status', 'ON')
        ->whereHas('buyer', function ($q) {
            $q->where('user_name', 'LIKE', "%{$this->search}%");
        })
        ->orderBy('id','desc')
        ->paginate($this->perPage);// Not arrived - No Entregados
        return view('livewire.profile.purchase.not-arrived-purchase', compact(
            'notArrived'
            )
        );
        
    }

    public function clear()
    {
        $this->search = '';
        $this->page = 1;
        $this->perPage = '30';
    }
}
