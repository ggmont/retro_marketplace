<?php

namespace App\Http\Livewire\Special;
use App\AppOrgProduct;
use App\AppOrgUserInventory;
use App\SysDictionary;
use Livewire\Component;
use Livewire\WithPagination;

class InventoryEditionTable extends Component
{
    use WithPagination;
    public $perPage = '20';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $platform = SysDictionary::where('code', 'GAME_PLATFORM')->get(['value_id', 'value', 'parent_id']);
        $condition = SysDictionary::where('code', 'GAME_STATE')->get(['value_id', 'value', 'parent_id']);
        return view('livewire.special.inventory-edition-table', [
            'platform' => $platform,
            'condition' => $condition,
            'ari' => AppOrgUserInventory::where('user_id', Auth::id())
                ->where('quantity', '>', 0)
                ->where('in_collection', 'N')
                ->where('box_condition', 'LIKE', "%{$this->search_box}%")
                ->where('cover_condition', 'LIKE', "%{$this->search_cover}%")
                ->where('manual_condition', 'LIKE', "%{$this->search_manual}%")
                ->where('game_condition', 'LIKE', "%{$this->search_game}%")
                ->where('extra_condition', 'LIKE', "%{$this->search_extra}%")
                ->whereHas('product', function ($q) {
                    $q->where('name', 'LIKE', "%{$this->search}%");
                    $q->where('platform', 'LIKE', "%{$this->search_platform}%");
                    $q->whereHas('categoryProd', function ($q) {
                        $q->where('parent_id', 1);
                    });
                })
                ->whereNull('deleted_at')
                ->orderBy("id", "DESC")
                ->paginate($this->perPage),
        ]);

    }
}
