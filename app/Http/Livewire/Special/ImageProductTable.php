<?php

namespace App\Http\Livewire\Special;
use App\AppOrgProduct;
use App\AppOrgCategory;
use App\SysDictionary;
use Livewire\Component;
use Livewire\WithPagination;

class ImageProductTable extends Component
{
    use WithPagination;

    protected $queryString = [
		'search' => ['except' => '' ],
     ];

	public $search = '';
	public $perPage = '20';

    public $search_platform = '';
    public $search_category = '';
    public $search_region = '';
    public $search_language = '';
    public $search_box_language = '';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingSearchplatform()
    {
        $this->resetPage();
    }

    public function updatingSearchcategory()
    {
        $this->resetPage();
    }

    public function updatingSearchregion()
    {
        $this->resetPage();
    }

    public function updatingSearchlanguage()
    {
        $this->resetPage();
    }

    public function updatingSearchboxlanguage()
    {
        $this->resetPage();
    }

    public function render()
    {
        $region = SysDictionary::where("code", "GAME_REGION")->get();
        $language = SysDictionary::where("code", "GAME_LANGUAGE")->get();
        $platform = SysDictionary::where("code", "GAME_PLATFORM")->get();
        $media = SysDictionary::where("code", "GAME_SUPPORT")->get();
        $category = AppOrgCategory::all();

        return view('livewire.special.image-product-table', [
            'product'     => AppOrgProduct::where('name','LIKE',"%{$this->search}%")
            ->where('platform','LIKE',"%{$this->search_platform}%")
            ->where('region','LIKE',"%{$this->search_region}%")
            ->where('language','LIKE',"%{$this->search_language}%")
            ->where('language','LIKE',"%{$this->search_box_language}%")
            ->whereHas('categoryProd', function ($q) {
                $q->where('category_text','LIKE',"%{$this->search_category}%");
            })
            ->paginate($this->perPage),
            'platform' => $platform,
            'region' => $region,
            'language' => $language,
            'category' => $category,
            'media' => $media
        ]);
    }

    public function clear()
    {
    	$this->search = '';
        $this->search_box = '';
        $this->search_platform = '';
        $this->search_region = '';
        $this->search_language = '';
        $this->search_box_language = '';
        $this->search_category = '';

    	$this->page = 1;
    	$this->perPage = '5';
    }


}
