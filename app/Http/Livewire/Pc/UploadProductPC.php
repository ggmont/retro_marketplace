<?php

namespace App\Http\Livewire\Pc;

use Livewire\Component;
use App\AppOrgProduct;
use App\SysDictionary;

class UploadProductPC extends Component
{
    public $totalRecords;
    public $loadAmount = 20;
    public $query = '';
    public $search_platform = '';
    public $search_category = '';
    public $search_region = '';

    protected $listeners = ['postAdded' => 'incrementPostCount'];

    public function loadMore()
    {
        $this->loadAmount += 10;
    }

    public function mount()
    {
        $this->totalRecords = AppOrgProduct::count();
    }

    public function updateQuery()
    {
        $this->query = trim($this->query);
    }


    public function render()
    {
        $platform = SysDictionary::where("code", "GAME_PLATFORM")->get();
        $region = SysDictionary::where("code", "GAME_REGION")->get();
        
        $randomProducts = AppOrgProduct::whereHas('images') // Filtrar solo productos con imágenes asociadas
        ->inRandomOrder()
        ->limit($this->loadAmount)
        ->get();
        
        return view('livewire.pc.upload-product-p-c', [
            'platform' => $platform,
            'region' => $region,
            'random' => $randomProducts,
        ])->with(
            'products',
            AppOrgProduct::where('name', 'LIKE', "%{$this->query}%")
                ->where('platform', 'LIKE', "%{$this->search_platform}%")
                ->where('region', 'LIKE', "%{$this->search_region}%")
                ->whereHas('categoryProd', function ($q) {
                    $q->where('category_text', 'LIKE', "%{$this->search_category}%");
                })
                ->orderBy('id', 'desc')
                ->limit($this->loadAmount)
                ->get()
        );
    }
    
}
