<?php

namespace App\Http\Livewire;
use App\SysUser;
use App\SysUserRoles;
use Livewire\Component;
use Livewire\WithPagination;

class ProductUserPublicTable extends Component
{
    
	use WithPagination;

	protected $queryString = [
		'search' => ['except' => '' ],
     ];

    public $accion = "store";
	public $search = '';
	public $perPage = '15';
     
    
    public function mount($user)
    {
        $this->prueba = $user;
       
    }


    public function render()
    {      
         
        //dd($usuario);
        return view('livewire.product-user-public-table', [
            'usuario'     => SysUser::where('user_name',$this->prueba)->first()
             
        ]);
    }

    public function clear()
    {
    	$this->search = '';
    	$this->page = 1;
    	$this->perPage = '5';
    }
  
}
