<?php

namespace App\Http\Livewire\Admin;

use App\SysDictionary;
use Livewire\Component;
use Livewire\WithPagination;

class ConfigurationTable extends Component
{
    use WithPagination;

    protected $queryString = [
        'search' => ['except' => ''],
    ];

    public $accion = "store";
    public $search = '';
    public $perPage = '20';

    public $value, $value_id, $code,$selectedItem;

    public $selectedItemId; // Propiedad para almacenar el ID del elemento seleccionado

    protected $listeners = ['render' => 'render'];

    public function updatingSearch()
    {
        $this->emit('twoAxisConfig');
    }

    public function render()
    {
        return view('livewire.admin.configuration-table', [
            'list'     => SysDictionary::orderBy('id', 'DESC')->paginate($this->perPage)
        ]);
    }


    public function showEditModal($id)
    {
        $this->selectedItem = SysDictionary::find($id);
        $this->code = $this->selectedItem->code;
        $this->value = $this->selectedItem->value;
        $this->value_id = $this->selectedItem->value_id;

        $this->dispatchBrowserEvent('showEditModal');
    }

    public function clear()
    {
        $this->search = '';
        $this->page = 1;
        $this->perPage = '5';
    }

    public function rules()
    {
        return [
            'code' => 'required', // Regla de validación para la propiedad 'code'
            'value' => 'required', // Regla de validación para la propiedad 'value'
            'value_id' => 'required', // Regla de validación para la propiedad 'value_id'
        ];
    }


    public function update($id)
    {

        $promotion = SysDictionary::find($id);
        $promotion->code = $this->code;
        $promotion->value = $this->value;
        $promotion->value_id = $this->value_id;
        $promotion->save();

        $this->reset();
        $this->emit('list_alert');
    }

    public function store()
    {
        $this->validate();

        SysDictionary::create([
            'code' => $this->code,
            'value' => $this->value,
            'value_id' => $this->value_id,
        ]);

        $this->emitself('refreshComponent');
        $this->reset(['code', 'value', 'value_id']);
        $this->dispatchBrowserEvent('hide-form-list');
        $this->emit('alert');
    }
}
