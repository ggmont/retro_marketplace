<?php

namespace App\Http\Livewire\Admin;

use App\AppConversation;
use App\SysUser;
use Livewire\Component;
use Livewire\WithPagination;

class HistorialMessage extends Component
{
    use WithPagination;

    public $page = 1;
    public $favoriteId = '';
    public $desfavoriteId = '';
    public $deleteId = '';
    public $accion = "store";
    public $search = '';
    public $perPage = '12';
    public $prueba;
    public $nose;
    public $viewData;

    protected $queryString = [
        'search' => ['except' => ''],
    ];

    public function mount($ultra)
    {

        $this->prueba = $ultra;
        //dd($this->prueba);
        $usuario = SysUser::where('id', $ultra)->first();
        //$ultra = AppOrgUserInventory::where('user_id', $usuario->id);
        //dd($usuario);
        $this->emit('omega');
        //dd($this->prueba);
    }

    public function clear_inventory()
    {
        $this->search = '';
    }

    public function render()
    {
        //dd('hola');
        $usuario = SysUser::where('id', $this->prueba)->first();

        return view('livewire.admin.historial-message', [
            'beta' => AppConversation::where('alias_user_id', $usuario->id)
            ->whereHas('prueba', function ($q) {
                $q->where('user_name', 'LIKE', "%{$this->search}%");
            })
            ->orWhere(function ($query) {
                $query->where('alias_contact_id',  $this->prueba)
                ->whereHas('user', function ($q) {
                    $q->where('user_name', 'LIKE', "%{$this->search}%");
                });
            })
            ->orderBy('updated_at', 'desc')
            ->paginate($this->perPage),
        ]);
    }
}
