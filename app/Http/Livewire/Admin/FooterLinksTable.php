<?php

namespace App\Http\Livewire\Admin;
use App\AppOrgFooterLink;
use Livewire\Component;
use Livewire\WithPagination;

class FooterLinksTable extends Component
{
	use WithPagination;

	protected $queryString = [
		'search' => ['except' => '' ],
     ];

    public $accion = "store";
	public $search = '';
	public $perPage = '20';

    public function render()
    {
        return view('livewire.admin.footer-links-table', [
            'footer_link'     => AppOrgFooterLink::paginate($this->perPage)
        ]);
    }

    public function clear()
    {
    	$this->search = '';
    	$this->page = 1;
    	$this->perPage = '5';
    }
}
