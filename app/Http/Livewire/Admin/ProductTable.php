<?php

namespace App\Http\Livewire\Admin;
use App\AppOrgProduct;
use App\AppOrgCategory;
use App\SysDictionary;
use Livewire\Component;
use Livewire\WithPagination;

class ProductTable extends Component
{
   use WithPagination;


    public $accion = "store";
	public $search = '';
	public $perPage = '20';

    public $search_platform = '';
    public $search_category = '';
    public $search_region = '';
    public $search_language = '';
    public $search_box_language = '';
    public $search_enabled = '';

    public function updatingSearch()
    {
        $this->emit('twoAxis');
    }

    public function updatingSearchplatform()
    {
        $this->emit('twoAxis');
    }

    public function updatingSearchcategory()
    {
        $this->emit('twoAxis');
    }

    public function updatingSearchregion()
    {
        $this->emit('twoAxis');
    }

    public function updatingSearchlanguage()
    {
        $this->emit('twoAxis');
    }

    public function updatingSearchboxlanguage()
    {
        $this->emit('twoAxis');
    }

    public function updatingSearchenabled()
    {
        $this->emit('twoAxis');
    }
    


    public function render()
    {
        $region = SysDictionary::where("code", "GAME_REGION")->get();
        $language = SysDictionary::where("code", "GAME_LANGUAGE")->get();
        $platform = SysDictionary::where("code", "GAME_PLATFORM")->get();
        $media = SysDictionary::where("code", "GAME_SUPPORT")->get();
        $category = AppOrgCategory::all();

        return view('livewire.admin.product-table', [
            'product'     => AppOrgProduct::where('name','LIKE',"%{$this->search}%")
            ->where('platform', 'like', '%' .$this->search_platform)
            ->where('region','LIKE',"%{$this->search_region}%")
            ->where('language','LIKE',"%{$this->search_language}%")
            ->where('language','LIKE',"%{$this->search_box_language}%")
            ->where('is_enabled','LIKE',"%{$this->search_enabled}%")
            ->whereHas('categoryProd', function ($q) {
                $q->where('category_text','LIKE',"%{$this->search_category}%");
            })
            ->with(['inventory' => function($query){
            }])
            ->orderBy('id', 'DESC')
            ->paginate($this->perPage),
            'platform' => $platform,
            'region' => $region,
            'language' => $language,
            'category' => $category,
            'media' => $media
        ]);
    }

    public function clear()
    {
    	$this->search = '';
        $this->search_box = '';
        $this->search_platform = '';
        $this->search_region = '';
        $this->search_language = '';
        $this->search_box_language = '';
        $this->search_enabled = '';
        $this->search_category = '';

    	$this->page = 1;
    	$this->perPage = '5';
    }
}
