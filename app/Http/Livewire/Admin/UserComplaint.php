<?php

namespace App\Http\Livewire\Admin;

use App\SysUserComplaint;
use Livewire\Component;
use Livewire\WithPagination;

class UserComplaint extends Component
{
    use WithPagination;

    protected $queryString = [
        'search' => ['except' => ''],
    ];

    public $accion = "store";
    public $search = '';
    public $perPage = '20';

    public function render()
    {
        return view('livewire.admin.user-complaint', [
            'complaint'     => SysUserComplaint::where('category', 'LIKE', "%{$this->search}%")
                ->paginate($this->perPage)
        ]);
    }

}
