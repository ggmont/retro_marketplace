<?php

namespace App\Http\Livewire\Admin;
use App\AppOrgFaq;
use Livewire\Component;
use Livewire\WithPagination;

class FaqTable extends Component
{
	use WithPagination;

    protected $queryString = [
        'search_question' => ['except' => ''],
    ];

    public $accion = "store";
	public $search_question = '';
    public $search_response = '';
    public $search_enabled = '';
	public $perPage = '20';

    protected $listeners = ['render' => 'render'];

    public function render()
    {
        return view('livewire.admin.faq-table', [
            'faq'     => AppOrgFaq::where('question','LIKE',"%{$this->search_question}%")
            ->orWhere('answer','LIKE',"%{$this->search_response}%")
            ->orWhere('is_enabled','LIKE',"%{$this->search_enabled}%")
            ->whereNull('deleted_at')
            ->orderBy('id', 'DESC')
            ->paginate($this->perPage)
        ]);
    }

    public function clear()
    {
    	$this->search_question = '';
        $this->search_response = '';
        $this->search_enabled = '';
    	$this->page = 1;
    	$this->perPage = '5';
    }
}