<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use App\SysUser;
use App\SysUserReferred;

class UserReferedTable extends Component
{
    use WithPagination;

    public $prueba;

    public function mount($ultra)
    {

        $this->prueba = $ultra;
    }

    public function render()
    {
        $refered = SysUser::whereIn('id', SysUserReferred::where('master_id', $this->prueba)->pluck('son_id'))
            ->paginate(20);

        return view('livewire.admin.user-refered-table', [
            'refered' => $refered
        ]);
    }
}
