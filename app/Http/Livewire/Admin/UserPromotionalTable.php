<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use App\AppOrgHistorialPromotionalCode;

class UserPromotionalTable extends Component
{
    use WithPagination;

    public $prueba;

    public function mount($ultra)
    {
        $this->prueba = $ultra;
    }

    public function render()
    {
        $promotionalHistory = AppOrgHistorialPromotionalCode::where('user_id', $this->prueba)
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('livewire.admin.user-promotional-table', [
            'promotionalHistory' => $promotionalHistory
        ]);
    }
}

