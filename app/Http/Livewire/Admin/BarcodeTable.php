<?php

namespace App\Http\Livewire\Admin;

use App\AppOrgUserInventory;
use App\AppOrgCategory;
use App\SysDictionary;
use Livewire\Component;
use Livewire\WithPagination;

class BarcodeTable extends Component
{
    use WithPagination;

    public $search = '';
    public $search_platform = '';
    public $perPage = '20';


    

    public function render()
    {
        return view('livewire.admin.barcode-table', [
            'inventories_ean'     => AppOrgUserInventory::whereHas('imagesean', function ($q) {
                $q->where('image_path', '>', 0);
            })
                ->orderBy('id', 'DESC')
                ->groupBy('product_id')
                ->paginate($this->perPage),
        ]);
    }
}
