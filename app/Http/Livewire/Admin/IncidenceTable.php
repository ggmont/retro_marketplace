<?php

namespace App\Http\Livewire\Admin;
use App\Incidence;
use Livewire\Component;
use Livewire\WithPagination;

class IncidenceTable extends Component
{
    use WithPagination;

	protected $queryString = [
		'search' => ['except' => '' ],
     ];

    public $accion = "store";
	public $search = '';
	public $perPage = '20';

    public function render()
    {
        return view('livewire.admin.incidence-table', [
            'incidence'     => Incidence::paginate($this->perPage)
        ]);
    }
}
