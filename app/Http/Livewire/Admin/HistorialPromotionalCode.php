<?php

namespace App\Http\Livewire\Admin;
use App\AppOrgHistorialPromotionalCode;
use Livewire\Component;
use Livewire\WithPagination;

class HistorialPromotionalCode extends Component
{
    use WithPagination;
	public $perPage = '20';
    public $search = '';

    protected $listeners = ['render' => 'render'];

    public function updatingSearch()
    {
        $this->emit('twoAxisHistorialPromo');
    }

    public function render()
    {
        return view('livewire.admin.historial-promotional-code', [
            'historial_promotional'     => AppOrgHistorialPromotionalCode::orderBy('id', 'DESC')->paginate($this->perPage)
        ]);
    }

}
