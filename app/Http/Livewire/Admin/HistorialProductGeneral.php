<?php

namespace App\Http\Livewire\Admin;
use App\AppOrgUserInventory;
use Livewire\Component;
use Livewire\WithPagination;

class HistorialProductGeneral extends Component
{
    use WithPagination;
	public $perPage = '20';
    
    public $accion = "store";
    public $search = '';
    public $search_platform = '';
    public $search_box = '';
    public $search_manual = '';
    public $search_game = '';
    public $search_extra = '';
    public $search_cover = '';

    protected $listeners = ['render' => 'render'];

    public function updatingSearch()
    {
        $this->emit('twoAxisHistorialPromo');
    }

    public function previousPage()
    {
        $this->setPage(max($this->page - 1, 1));
        $this->emit('omega');
    }

    public function nextPage()
    {
        $this->setPage($this->page + 1);
        $this->emit('omega');
    }

    public function gotoPage($page)
    {
        $this->setPage($page);
        $this->emit('omega');
    }

    public function setPage($page)
    {
        $this->page = $page;
        $this->emit('omega');
    }

    public function mount()
    {
        $this->emit('omega');
    }

    public function render()
    {
        //dd('hola');
        return view('livewire.admin.historial-product-general', [
            'historial_inventory' => AppOrgUserInventory::where('quantity', '>', 0)
                ->where('box_condition', 'LIKE', "%{$this->search_box}%")
                ->where('cover_condition', 'LIKE', "%{$this->search_cover}%")
                ->where('manual_condition', 'LIKE', "%{$this->search_manual}%")
                ->where('game_condition', 'LIKE', "%{$this->search_game}%")
                ->where('extra_condition', 'LIKE', "%{$this->search_extra}%")
                ->whereHas('product', function ($q) {
                    $q->where('name', 'LIKE', "%{$this->search}%");
                    $q->where('platform', 'LIKE', "%{$this->search_platform}%");
                })
                ->whereNull('deleted_at')
                ->orderBy("id", "DESC")
                ->paginate($this->perPage),
        ]);
    }

}
