<?php

namespace App\Http\Livewire\Admin;
use App\SysUser;
use App\SysUserRoles;
use Livewire\Component;
use Livewire\WithPagination;

class Visitor extends Component
{
    use WithPagination;

    protected $queryString = [
        'search_user_name' => ['except' => ''],
    ];

    public $accion = "store";
    public $search_user_name = '';
    public $search_complete = '';
    public $search_email = '';
    public $perPage = '50';

    public function updatingSearchusername()
    {
        $this->resetPage();
    }

    public function updatingSearchcomplete()
    {
        $this->resetPage();
    }

    public function updatingSearchemail()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.admin.visitor', [
            'visitor' => SysUser::where('user_name', 'LIKE', "%{$this->search_user_name}%")
                ->where(function ($query) {
                    $query->where('first_name', 'LIKE', "%{$this->search_complete}%")
                        ->orWhereNull('first_name'); // Incluye registros con first_name nulo
                })
                ->where(function ($query) {
                    $query->where('last_name', 'LIKE', "%{$this->search_complete}%")
                        ->orWhereNull('last_name'); // Incluye registros con last_name nulo
                })
                ->where('email', 'LIKE', "%{$this->search_email}%")
                ->whereHas('roles', function ($a) {
                    $a->whereIn('role_id', [2,5,4,1]);
                })
                ->withCount('visits') // Obtener la cuenta de visitas generales
                ->orderBy('visits_count', 'DESC') 
                ->paginate($this->perPage)
        ]);
        
        
    }

    public function clear()
    {
        $this->search_user_name = '';
        $this->search_complete = '';
        $this->search_email = '';
        $this->page = 1;
        $this->perPage = '20';
    }
}
