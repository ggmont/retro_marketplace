<?php

namespace App\Http\Livewire\Admin;

use App\AppOrgCategory;
use Livewire\Component;
use Livewire\WithPagination;

class CategoryTable extends Component
{
    use WithPagination;

    protected $queryString = [
        'search' => ['except' => ''],
    ];

    public $accion = "store";
    public $search = '';
    public $perPage = '20';
    public $name, $order_by, $category_text, $is_enabled, $hierarchy, $parent_id;

    protected $rules = [
        'name' => 'required',
        'category_text' => 'required',
        'is_enabled' => 'required',
    ];

    protected $listeners = ['render' => 'render'];

    public function updatingSearch()
    {
        $this->emit('twoAxisCategory');
    }

    public function render()
    {
        return view('livewire.admin.category-table', [
            'categories'     => AppOrgCategory::where('name', 'LIKE', "%{$this->search}%")
                ->orWhere('category_text', 'LIKE', "%{$this->search}%")
                ->orWhere('order_by', 'LIKE', "%{$this->search}%")
                ->orderBy('id', 'DESC')
                ->paginate($this->perPage)
        ]);
    }

    public function clear()
    {
        $this->search = '';
        $this->page = 1;
        $this->perPage = '20';
    }

    public function store()
    {
        $this->validate();

        AppOrgCategory::create([
            'category_text' => $this->category_text,
            'name' => $this->name,
            'order_by' => 0,
            'is_enabled' => $this->is_enabled,
            'hierarchy' => 0,
            'parent_id' => 172,
        ]);

        $this->emitself('refreshComponent');
        $this->reset(['category_text', 'name', 'order_by', 'is_enabled']);
        $this->dispatchBrowserEvent('hide-form-category');
        $this->emit('alert');

    }
}
