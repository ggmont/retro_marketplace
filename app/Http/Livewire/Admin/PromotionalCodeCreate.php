<?php

namespace App\Http\Livewire\Admin;
use App\AppOrgPromotionalCode;
use Livewire\Component;

class PromotionalCodeCreate extends Component
{
    public $accion = "store";
    public $code,$type,$special,$is_enabled,$description;

    protected $rules = [
        'code' => 'required',
        'type' => 'required',
        'special' => 'required',
        'description' => 'required',
        'is_enabled' => 'required',
    ];

    public function render()
    {
        return view('livewire.admin.promotional-code-create');
    }

    public function store(){
        $this->validate();
        
    	AppOrgPromotionalCode::create([
          'code' => $this->code,
          'type' => $this->type,
          'special' => $this->special,
          'description' => $this->description,
          'is_enabled' => $this->is_enabled,
    	]);

        $this->emit('alert');
        $this->emit('render');

    	$this->reset(['code','type','special','description','is_enabled']);
    }
}
