<?php

namespace App\Http\Livewire\Admin;

use App\AppOrgPromotionalCode;
use Livewire\Component;
use Livewire\WithPagination;

class PromotionalCodeTable extends Component
{
    use WithPagination;
    public $perPage = '20';
    public $search = '';
    public $accion = "store";
    public $selectedItem, $code, $type, $special, $is_enabled, $description;

    protected $rules = [
        'code' => 'required',
        'type' => 'required',
        'special' => 'required',
        'description' => 'required',
        'is_enabled' => 'required',
    ];

    protected $listeners = ['update' => 'promotionUpdated'];

    public function updatingSearch()
    {
        $this->emit('twoAxisPromotion');
    }

    public function render()
    {
        return view('livewire.admin.promotional-code-table', [
            'promotion'     => AppOrgPromotionalCode::where('code', 'LIKE', "%{$this->search}%")->orderBy('id', 'DESC')->paginate($this->perPage)
        ]);
    }

    public function showEditModal($id)
    {
        $this->selectedItem = AppOrgPromotionalCode::find($id);
        $this->code = $this->selectedItem->code;
        $this->type = $this->selectedItem->type;
        $this->special = $this->selectedItem->special;
        $this->description = $this->selectedItem->description;
        $this->is_enabled = $this->selectedItem->is_enabled;

        $this->dispatchBrowserEvent('showEditModal');
    }

    public function store()
    {
        $this->validate();

        AppOrgPromotionalCode::create([
            'code' => $this->code,
            'type' => $this->type,
            'special' => $this->special,
            'description' => $this->description,
            'is_enabled' => $this->is_enabled,
        ]);

        $this->emitself('refreshComponent');
        $this->reset(['code', 'type', 'special', 'description', 'is_enabled']);
        $this->dispatchBrowserEvent('hide-form-code');
        $this->emit('alert');
    }

    public function update($id)
    {

        $promotion = AppOrgPromotionalCode::find($id);
        $promotion->code = $this->code;
        $promotion->type = $this->type;
        $promotion->special = $this->special;
        $promotion->description = $this->description;
        $promotion->is_enabled = $this->is_enabled;
        $promotion->save();

        $this->reset();
        $this->emit('promotionUpdated');
    }
}
