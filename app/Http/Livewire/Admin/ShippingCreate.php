<?php

namespace App\Http\Livewire\Admin;
use App\SysCountry;
use App\SysShipping;
use Livewire\Component;

class ShippingCreate extends Component
{

    public function render()
    {

        $country = SysCountry::all();

        return view('livewire.admin.shipping-create', compact('country'));
    }

}
