<?php

namespace App\Http\Livewire\Admin;
use App\AppOrgOrder;
use Livewire\Component;
use Livewire\WithPagination;
use Carbon\Carbon;

class OrderTable extends Component
{
	use WithPagination;

 

    public $accion = "store";
	public $search_buyer = '';
    public $search_buyer_name = '';
    public $search_seller = '';
    public $search_seller_name = '';
    public $search_status = '';
	public $perPage = '20';

    public function updatingSearchBuyer()
    {
        $this->emit('twoAxisOrder');
    }


    public function updatingSearchSeller()
    {
        $this->emit('twoAxisOrder');
    }

    public function updatingSearchStatus()
    {
        $this->emit('twoAxisOrder');
    }


    public function render()
    {

        $order_48 = AppOrgOrder::where('status', 'RP')
        ->where(function ($query) {
            $query->where('created_at', '>=', Carbon::now()->subHours(24))
                ->orWhere('created_at', '>=', Carbon::now()->subHours(48));
        })
        ->get();   
 

        return view('livewire.admin.order-table', [
            'orders'     => AppOrgOrder::whereHas('buyer', function ($q) {
                $q->where('user_name','LIKE',"%{$this->search_buyer_name}%");
            })
            ->where('status','LIKE',"%{$this->search_status}%")
            ->whereHas('seller', function ($q) {
                $q->where('user_name','LIKE',"%{$this->search_seller_name}%");
            })
            ->orderBy('id', 'DESC')
            ->paginate($this->perPage),
        ]);
    }

    public function clear()
    {
    	$this->search_buyer = '';
        $this->search_seller = '';
    	$this->page = 1;
    	$this->perPage = '20';
    }
}
