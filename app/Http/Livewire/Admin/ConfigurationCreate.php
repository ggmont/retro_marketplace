<?php

namespace App\Http\Livewire\Admin;
use App\SysDictionary;
use Livewire\Component;

class ConfigurationCreate extends Component
{
	public $accion = "store";
    public $code,$value_id,$value,$parent_id,$order_by;

    public function render()
    {

        $list = SysDictionary::whereNull('deleted_at')->orderBy('code')->orderBy('value')->get();

        return view('livewire.admin.configuration-create', compact(
            'list'
        ));
    }

    public function store(){
    	SysDictionary::create([
          'code' => $this->code,
          'value_id' => $this->value_id,
          'value' => $this->value,
          'parent_id' => $this->parent_id,
          'order_by' => $this->order_by,
    	]);

        $this->emit('alert');
        $this->emit('render');

    	$this->reset(['code','value_id','value','parent_id','order_by']);
    }
}
