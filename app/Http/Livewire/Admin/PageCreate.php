<?php

namespace App\Http\Livewire\Admin;
use Illuminate\Support\Str;
use App\AppOrgPage;
use Livewire\Component;

class PageCreate extends Component
{
	public $accion = "store";
    public $title,$url,$content,$show_title,$is_enabled;

    protected $rules = [
        'title' => 'required',
        'url' => 'required',
        'content' => 'required',
        'is_enabled' => 'required',
    ];

    public function render()
    {
    	$page = AppOrgPage::all();

        return view('livewire.admin.page-create', compact(
            'page'
        ));
    }

    public function store(){
        $this->validate();
        
    	AppOrgPage::create([
          'title' => $this->title,
          'url' => Str::slug($this->url),
          'content' => $this->content,
          'show_title' => 'Y',
          'is_enabled' => $this->is_enabled,
    	]);

        $this->emit('alert');
        $this->emit('render');
        $this->reset();

        
    }
}
