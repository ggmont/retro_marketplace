<?php

namespace App\Http\Livewire\Admin;
use App\AppOrgCategory;
use App\SysDictionary;
use Livewire\Component;

class ProductCreate extends Component
{
    public function render()
    {
        $categories = AppOrgCategory::all();
        $genero = SysDictionary::where("code", "GAME_CATEGORY")->get();
        $location = SysDictionary::where("code", "GAME_LOCATION")->get();
        $generation = SysDictionary::where("code", "GAME_GENERATION")->get();
        $language = SysDictionary::where("code", "GAME_LANGUAGE")->get();
        $media = SysDictionary::where("code", "GAME_SUPPORT")->get();
        $platform = SysDictionary::where("code", "GAME_PLATFORM")->get();
        $region = SysDictionary::where("code", "GAME_REGION")->get();

        //dd($genero);

        return view('livewire.admin.product-create', compact(
            'categories',
            'genero',
            'location',
            'generation',
            'language',
            'media',
            'platform',
            'region')
        );
    }
}
