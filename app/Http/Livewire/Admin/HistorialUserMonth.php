<?php

namespace App\Http\Livewire;

use Livewire\Component;

class HistorialUserMonth extends Component
{
    public function render()
    {
        return view('livewire.historial-user-month');
    }
}
