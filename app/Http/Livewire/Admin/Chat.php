<?php

namespace App\Http\Livewire\Admin;
use App\AppMessage;
use Livewire\Component;
use Livewire\WithPagination;

class Chat extends Component
{
	use WithPagination;

	protected $queryString = [
		'search' => ['except' => '' ],
     ];
    public $accion = "store";
	public $search = '';
	public $perPage = '5';

    public function render()
    {
        return view('livewire.admin.chat', [
            'chat'     => AppMessage::paginate($this->perPage)
        ]);
    }

    public function clear()
    {
    	$this->search = '';
    	$this->page = 1;
    	$this->perPage = '5';
    }
}
