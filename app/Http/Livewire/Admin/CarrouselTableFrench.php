<?php

namespace App\Http\Livewire\Admin;
use App\AppOrgCarouselItem;
use App\AppOrgCarousel;
use Livewire\Component;
use Livewire\WithPagination;

class CarrouselTableFrench extends Component
{
    use WithPagination;

	protected $queryString = [
		'search_location' => ['except' => '' ],
     ];

    public $accion = "store";
	public $search_location = '';
	public $perPage = '10';

    public function render()
    {
        
        return view('livewire.admin.carrousel-table-french', [
            'carrousel'     => AppOrgCarouselItem::whereHas('carrusel', function ($q) {
                $q->where('name','LIKE',"%{$this->search_location}%");
            })->where('lang','fr')->paginate($this->perPage)
        ]);
    }

    public function clear()
    {
    	$this->search = '';
    	$this->page = 1;
    	$this->perPage = '10';
    }
}
