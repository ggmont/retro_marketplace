<?php

namespace App\Http\Livewire\Admin;
use App\AppOrgFooterLink;
use App\SysDictionary;
use Livewire\Component;

class FooterLinksCreate extends Component
{
	public $accion = "store";
    public $column,$name,$url,$new_page,$is_enabled;

    public function render()
    {
    	$footer_link = AppOrgFooterLink::all();
        $footer_column = SysDictionary::where("code", "FOOTER_COL")->get();
        return view('livewire.admin.footer-links-create', compact(
            'footer_link',
            'footer_column'
        ));
    }

    public function store(){
    	AppOrgFooterLink::create([
          'name' => $this->name,
          'url' => $this->url,
          'column' => $this->column,
          'is_enabled' => $this->is_enabled,
    	]);

    	$this->reset(['name','url','column','is_enabled']);
    }
}

