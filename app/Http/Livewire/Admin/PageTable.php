<?php

namespace App\Http\Livewire\Admin;
use App\AppOrgPage;
use Livewire\Component;
use Livewire\WithPagination;

class PageTable extends Component
{
	use WithPagination;

	protected $queryString = [
		'search' => ['except' => '' ],
     ];

    public $accion = "store";
	public $search = '';
	public $perPage = '20';

    protected $listeners = ['render' => 'render'];

    public function render()
    {
        return view('livewire.admin.page-table', [
            'pagina'     => AppOrgPage::where('lang','es')->where('title','LIKE',"%{$this->search}%")->orderBy('id', 'DESC')->paginate($this->perPage)
        ]);
    }

    public function clear()
    {
    	$this->search = '';
    	$this->page = 1;
    	$this->perPage = '5';
    }
}
