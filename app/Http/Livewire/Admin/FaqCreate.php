<?php

namespace App\Http\Livewire\Admin;
use App\AppOrgFaq;
use Livewire\Component;

class FaqCreate extends Component
{
	public $accion = "store";
    public $question,$answer,$is_enabled;

    protected $rules = [
        'question' => 'required',
        'answer' => 'required',
        'is_enabled' => 'required',
    ];

    public function render()
    {
    	$faqs = AppOrgFaq::all();
        return view('livewire.admin.faq-create', compact('faqs'));
    }

    public function store(){
        $this->validate();
        
    	AppOrgFaq::create([
          'question' => $this->question,
          'answer' => $this->answer,
          'is_enabled' => $this->is_enabled,
    	]);

        $this->emit('alert');
        $this->emit('render');
        $this->reset();
    }
}
