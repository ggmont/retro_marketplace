<?php

namespace App\Http\Livewire\Admin;
use App\SysShipping;
use Livewire\Component;
use Livewire\WithPagination;

class ShippingTable extends Component
{
	use WithPagination;

	protected $queryString = [
		'search' => ['except' => '' ],
     ];

    public $accion = "store";
	public $search = '';
	public $perPage = '20';

    public function updatingSearch()
    {
        $this->emit('twoAxisShipping');
    }

    public function render()
    {
        return view('livewire.admin.shipping-table', [
            'shipping'     => SysShipping::where('name','LIKE',"%{$this->search}%")
            ->orWhere('certified','LIKE',"%{$this->search}%")
            ->orWhere('max_value','LIKE',"%{$this->search}%")
            ->orWhere('volume','LIKE',"%{$this->search}%")
            ->orWhere('stamp_price','LIKE',"%{$this->search}%")
            ->orWhere('price','LIKE',"%{$this->search}%")
            ->whereNull('deleted_at')
            ->paginate($this->perPage)
        ]);
    }

    public function clear()
    {
    	$this->search = '';
    	$this->page = 1;
    	$this->perPage = '20';
    }
}
