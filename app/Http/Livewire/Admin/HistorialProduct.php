<?php

namespace App\Http\Livewire\Admin;

use App\AppOrgUserInventory;
use App\SysUser;
use Livewire\Component;
use Livewire\WithPagination;

class HistorialProduct extends Component
{
    use WithPagination;

    public $page = 1;
    public $favoriteId = '';
    public $desfavoriteId = '';
    public $deleteId = '';
    public $accion = "store";
    public $search = '';
    public $search_platform = '';
    public $search_box = '';
    public $search_manual = '';
    public $search_game = '';
    public $search_extra = '';
    public $search_cover = '';
    public $perPage = '12';
    public $prueba;
    public $nose;
    public $viewData;

    protected $queryString = [
        'search' => ['except' => ''],
    ];

    public function previousPage()
    {
        $this->setPage(max($this->page - 1, 1));
        $this->emit('omega');
    }

    public function nextPage()
    {
        $this->setPage($this->page + 1);
        $this->emit('omega');
    }

    public function gotoPage($page)
    {
        $this->setPage($page);
        $this->emit('omega');
    }

    public function setPage($page)
    {
        $this->page = $page;
        $this->emit('omega');
    }


    public function mount($ultra)
    {

        $this->prueba = $ultra;
        $usuario = SysUser::where('id', $ultra)->first();
        //$ultra = AppOrgUserInventory::where('user_id', $usuario->id);
        //dd($usuario);
        $this->emit('omega');
        //dd($this->prueba);
    }

    public function clear_inventory()
    {
        $this->search = '';
        $this->search_box = '';
        $this->search_manual = '';
        $this->search_game = '';
        $this->search_extra = '';
        $this->search_cover = '';
        $this->search_inside = '';
        $this->search_platform = '';
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function delete()
    {
        AppOrgUserInventory::find($this->deleteId)->delete();
        $this->emit('alert');
    }

    public function render()
    {
        //dd('hola');
        $usuario = SysUser::where('id', $this->prueba)->first();
        $pruebas = AppOrgUserInventory::where('user_id', $usuario->id)->get();
        //dd($pruebas);
        return view('livewire.admin.historial-product', [
            'historial_inventory' => AppOrgUserInventory::where('user_id', $usuario->id)
                ->where('quantity', '>', 0)
                ->where('box_condition', 'LIKE', "%{$this->search_box}%")
                ->where('cover_condition', 'LIKE', "%{$this->search_cover}%")
                ->where('manual_condition', 'LIKE', "%{$this->search_manual}%")
                ->where('game_condition', 'LIKE', "%{$this->search_game}%")
                ->where('extra_condition', 'LIKE', "%{$this->search_extra}%")
                ->whereHas('product', function ($q) {
                    $q->where('name', 'LIKE', "%{$this->search}%");
                    $q->where('platform', 'LIKE', "%{$this->search_platform}%");
                })
                ->whereNull('deleted_at')
                ->orderBy("id", "DESC")
                ->paginate($this->perPage),
        ]);
    }
}
