<?php

namespace App\Http\Livewire\Admin;
use App\AppOrgCategory;
use Livewire\Component;

class CategoryCreate extends Component
{
	public $accion = "store";
    public $name,$order_by,$category_text,$is_enabled,$hierarchy,$parent_id;

    protected $rules = [
        'name' => 'required',
        'order_by' => 'required',
        'category_text' => 'required',
        'is_enabled' => 'required',
    ];

    public function render()
    {
    	$categories = AppOrgCategory::all();
        return view('livewire.admin.category-create', compact('categories'));
    }

    public function store(){
        $this->validate();
        
    	AppOrgCategory::create([
          'category_text' => $this->category_text,
          'name' => $this->name,
          'order_by' => $this->order_by,
          'is_enabled' => $this->is_enabled,
          'hierarchy' => 0,
          'parent_id' => 172,
    	]);

        $this->emit('alert');
        $this->emit('render');

    	$this->reset(['category_text','name','order_by','is_enabled']);
    }
}
