<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\ProductRequest;
use Livewire\WithPagination;

class ProductRequestTable extends Component
{
    use WithPagination;

    public $accion = "store";
	public $perPage = '20';

    public $search_user = '';
    public $search_product = '';
    public $search_status = '';

    public function updatingSearchuser()
    {
        $this->emit('twoAxisRequest');
    }
    
    public function updatingSearchproduct()
    {
        $this->emit('twoAxisRequest');
    }

    public function updatingSearchstatus()
    {
        $this->emit('twoAxisRequest');
    }

    public function render()
    {
        return view('livewire.admin.product-request-table', [
            'product_request'     => ProductRequest::orderBy('id', 'DESC')
            ->whereHas('user', function ($q) {
                $q->where('user_name','LIKE',"%{$this->search_user}%");
            })
            ->where('product','LIKE',"%{$this->search_product}%")
            ->where('status','LIKE',"%{$this->search_status}%")
            ->paginate($this->perPage)
        ]);
    }

    public function clear()
    {
    	$this->search_user = '';
        $this->search_product = '';
        $this->search_status = '';
    	$this->page = 1;
    	$this->perPage = '20';
    }
}
