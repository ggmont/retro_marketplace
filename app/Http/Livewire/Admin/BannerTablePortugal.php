<?php

namespace App\Http\Livewire\Admin;
use App\AppOrgBanner;
use Livewire\Component;
use Livewire\WithPagination;

class BannerTablePortugal extends Component
{
    use WithPagination;

	protected $queryString = [
		'search' => ['except' => '' ],
     ];

    public $accion = "store";
	public $search = '';
	public $perPage = '20';

    public function render()
    {
        return view('livewire.admin.banner-table-portugal', [
            'banner'     => AppOrgBanner::where('lang','en')->paginate($this->perPage)
        ]);
    }

    public function clear()
    {
    	$this->search = '';
    	$this->page = 1;
    	$this->perPage = '5';
    }
}
