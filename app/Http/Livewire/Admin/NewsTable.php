<?php

namespace App\Http\Livewire\Admin;
use App\AppOrgNewsItem;
use Livewire\Component;
use Livewire\WithPagination;

class NewsTable extends Component
{
	use WithPagination;

	protected $queryString = [
		'search' => ['except' => '' ],
     ];

    public $accion = "store";
	public $search = '';
    public $perPage = '20';

    protected $listeners = ['render' => 'render'];

    public function render()
    {
        return view('livewire.admin.news-table', [
            'news'     => AppOrgNewsItem::paginate($this->perPage)
        ]);
    }

    public function clear()
    {
    	$this->search = '';
    	$this->page = 1;
    	$this->perPage = '5';
    }
}
