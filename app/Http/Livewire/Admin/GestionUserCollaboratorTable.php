<?php

namespace App\Http\Livewire\Admin;

use App\SysUser;
use Livewire\Component;
use Livewire\WithPagination;

class GestionUserCollaboratorTable extends Component
{
    use WithPagination;

    protected $queryString = [
        'search_user_name' => ['except' => ''],
    ];

    public $accion = "store";
    public $search_user_name = '';
    public $search_name = '';
    public $search_last = '';
    public $search_email = '';
    public $perPage = '20';

    public function updatingSearchusername()
    {
        $this->resetPage();
    }

    public function updatingSearchcomplete()
    {
        $this->emit('twoAxisCollaborator');
    }

    public function updatingSearchemail()
    {
        $this->resetPage();
    }

    public function render()
    {

        return view('livewire.admin.gestion-user-collaborator-table', [
            'user' => SysUser::where('user_name', 'LIKE', "%{$this->search_user_name}%")
            ->where(function ($query) {
                $query->where('first_name', 'LIKE', "%{$this->search_name}%")
                    ->orWhereNull('first_name'); // Incluye registros con first_name nulo
            })
            ->where(function ($query) {
                $query->where('last_name', 'LIKE', "%{$this->search_last}%")
                    ->orWhereNull('last_name'); // Incluye registros con last_name nulo
            })
            ->where('email', 'LIKE', "%{$this->search_email}%")
            ->whereHas('roles', function ($a) {
                $a->whereIn('role_id', [6]);
            })
            ->orderBy('created_at', 'DESC')
            ->paginate($this->perPage)
        ]);
    }

    public function clear()
    {
        $this->search_user_name = '';
        $this->search_name = '';
        $this->search_last = '';
        $this->search_email = '';
        $this->page = 1;
        $this->perPage = '20';
    }
}
