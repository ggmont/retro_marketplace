<?php

namespace App\Http\Livewire\Admin;

use App\SysUser;
use Livewire\Component;
use Livewire\WithPagination;
use App\AppOrgUserInventory;

class InventoryTable extends Component
{
    use WithPagination;

    protected $queryString = [
        'search' => ['except' => ''],
    ];

	public $perPage = '20';
    public $search = '';   
    public $search_product = '';    

    public function updatingSearch()
    {
        $this->emit('twoAxisInventory');
    }

    public function updatingSearchproduct()
    {
        $this->emit('twoAxisInventory');
    }


    public function render()
    {
        return view('livewire.admin.inventory-table', [
            'inventory'     => AppOrgUserInventory::whereHas('user', function ($q) {
                $q->where('user_name','LIKE',"%{$this->search}%");
            })
            ->whereHas('product', function ($q) {
                $q->where('name','LIKE',"%{$this->search_product}%");
            })
            ->orderBy('id', 'DESC')
           ->paginate($this->perPage)
        ]);
    }

    public function clear()
    {
    	$this->search = '';
    	$this->page = 1;
    	$this->perPage = '20';
    }
}
