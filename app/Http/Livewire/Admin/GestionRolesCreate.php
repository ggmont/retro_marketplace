<?php

namespace App\Http\Livewire\Admin;
use App\SysRoles;
use Livewire\Component;

class GestionRolesCreate extends Component
{
    public $accion = "store";
    public $name,$description,$is_enabled;

    public function render()
    {
    	$rol = SysRoles::all();
        return view('livewire.admin.gestion-roles-create', compact(
            'rol'
        ));
    }

    public function store(){
    	SysRoles::create([
          'name' => $this->name,
          'description' => $this->description,
          'is_enabled' => $this->is_enabled,
    	]);

        $this->emit('alert');
        $this->emit('render');

    	$this->reset(['name','description','is_enabled']);
    }

    
}
