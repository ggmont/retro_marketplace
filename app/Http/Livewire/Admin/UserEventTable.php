<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use App\AppOrgUserEvent;

class UserEventTable extends Component
{
    use WithPagination;

    public $prueba;

    public function mount($ultra)
    {

        $this->prueba = $ultra;
    }

    public function render()
    {
        $userEvents = AppOrgUserEvent::where('user_id', $this->prueba)
            ->orderBy('id', 'DESC')
            ->paginate(20);

        return view('livewire.admin.user-event-table', [
            'userEvents' => $userEvents
        ]);
    }
}
