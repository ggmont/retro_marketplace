<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class BannerCreate extends Component
{
    public function render()
    {
        return view('livewire.admin.banner-create');
    }
}
