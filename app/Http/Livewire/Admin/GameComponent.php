<?php

namespace App\Http\Livewire;

use Livewire\Component;

class GameComponent extends Component
{
    public function render()
    {
        return view('livewire.game-component');
    }
}
