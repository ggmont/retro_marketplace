<?php

namespace App\Http\Livewire\Admin;
use App\AppOrgNewsItem;
use Livewire\Component;

class NewsCreate extends Component
{
	public $accion = "store";
    public $title,$content,$is_enabled;

 
    protected $rules = [
        'title' => 'required',
        'content' => 'required',
        'is_enabled' => 'required',
    ];

    public function render()
    {
    	$news = AppOrgNewsItem::all();
        return view('livewire.admin.news-create', compact('news'));
    }

    public function store(){
        $this->validate();
        
    	AppOrgNewsItem::create([
          'title' => $this->title,
          'content' => $this->content,
          'is_enabled' => $this->is_enabled,
    	]);

        $this->emit('alert');
        $this->emit('render');
        $this->reset();

        
    }

}
