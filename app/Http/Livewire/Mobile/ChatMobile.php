<?php

namespace App\Http\Livewire\Mobile;

use App\AppMessage;
use App\AppConversation;
use Livewire\Component;
use App\SysUser;
use Auth;
use Livewire\WithFileUploads;
use Carbon\Carbon;

class ChatMobile extends Component
{
    use WithFileUploads;
    public $refresh, $photo, $iteration, $deleteId = '', $message, $allmessages, $sender, $search = '', $prueba, $articulo;

    protected $rules = [
        'photo' => 'nullable|image',
    ];

    public function mount($usuario)
    {

        $this->prueba = $usuario->id;
        $this->sender = $usuario;

        if (isset($this->prueba)) {
            $this->allmessages = AppMessage::where('alias_from_id', auth()->id())
                ->where('alias_to_id', $this->prueba)
                ->where('delete_message', 'N')
                ->orWhere(function ($query) {
                    $query->where('alias_from_id', $this->prueba)
                        ->where('alias_to_id', auth()->id())
                        ->where('delete_message', 'N');
                })
                ->get();

            $prueba = AppMessage::where('alias_from_id', \Auth::user()->id)
                ->where('alias_to_id')
                ->get();

            $not_seen = AppMessage::where('alias_from_id', $this->prueba)
                ->where('alias_to_id', auth()->id());
            $not_seen->update(['read' => true]);
        }
    }



    public function mountdata()
    {
        if (isset($this->prueba)) {
            $this->allmessages  =  AppMessage::where('alias_from_id', auth()->id())->where('alias_to_id', $this->prueba)->where('delete_message', 'N')
                ->orWhere(function ($query) {
                    $query->where('alias_from_id', $this->prueba)->where('alias_to_id', auth()->id())->where('delete_message', 'N');
                })

                ->get();

            //dd($this->allmessages,$this->prueba);

            $not_seen = AppMessage::where('alias_from_id', $this->prueba)->where('alias_to_id', auth()->id());
            $not_seen->update(['read' => true]);
        }
    }

    public function openModal()
    {
        $this->emitself('refreshComponent');
        $this->emit('show');
    }

    public function render()
    {


 
        $prueba = AppMessage::where('alias_from_id', auth()->id())->where('alias_to_id', $this->prueba)->orWhere('alias_from_id', $this->prueba)->where('alias_to_id', auth()->id())->orderBy('id', 'desc')->get();
       
        $this->dispatchBrowserEvent('scrollToBottom');
        return view('livewire.mobile.chat-mobile');
    }

    public function SubmitPhoto()
    {


        $this->dispatchBrowserEvent('show-form-chat');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }



    public function deleteId($id)
    {
        $this->emitself('refreshComponent');
        $this->deleteId = $id;
    }

    public function delete()
    {
        $this->emitself('refreshComponent');
        $message = AppMessage::find($this->deleteId);


        $message->update([
            'delete_message' => 'Y',
        ]);

        if (AppConversation::where('alias_user_id', auth()->id())->where('alias_contact_id', $this->prueba)->first()) {

            AppConversation::where('alias_user_id', auth()->id())->where('alias_contact_id', $this->prueba)->first()
                ->update(
                    [
                        'last_message' => '-'
                    ]
                );
        } elseif (AppConversation::where('alias_contact_id', auth()->id())->where('alias_user_id', $this->prueba)->first()) {

            AppConversation::where('alias_contact_id', auth()->id())->where('alias_user_id', $this->prueba)->first()
                ->update(
                    [
                        'last_message' => '-'
                    ]
                );
        }


        $this->emit('alert');
    }


    public function stopImage()
    {
        $this->emitself('refreshComponent');
        $this->photo = '';
        $this->dispatchBrowserEvent('hide-form-chat');
    }


    public function resetForm()
    {
        $this->emitself('refreshComponent');
        $this->message = '';
    }

    public function SendMessage()
    {
 
  
        if (AppConversation::where('alias_user_id', auth()->id())->where('alias_contact_id', $this->prueba)->first()) {
            $data = new AppMessage;
            $prueba_imagen = $this->photo;
            if ($prueba_imagen) {
                $image = $this->photo->store('img', 'public');
                $data->image = $image;
            }
            $data->content = $this->message;

            $data->alias_from_id = auth()->id();
            $data->alias_to_id = $this->sender->id;
            $data->save();



            if ($data->save()) {
                AppConversation::where('alias_user_id', auth()->id())->where('alias_contact_id', $this->prueba)->first()
                    ->update(
                        [
                            'last_message' => $data->content
                        ]
                    );
                $this->resetForm();
                if ($prueba_imagen) {
                    $this->photo = null;
                    $this->iteration++;
                }

                $this->dispatchBrowserEvent('scrollToBottom'); // Emitir el evento para desplazar la página hacia abajo
            }
        } elseif (AppConversation::where('alias_contact_id', auth()->id())->where('alias_user_id', $this->prueba)->first()) {

            $data = new AppMessage;
            $prueba_imagen = $this->photo;
            if ($prueba_imagen) {
                $image = $this->photo->store('img', 'public');
                $data->image = $image;
            }
            $data->content = $this->message;
            $data->alias_from_id = auth()->id();
            $data->alias_to_id = $this->sender->id;
            if ($data->save()) {
                AppConversation::where('alias_contact_id', auth()->id())->where('alias_user_id', $this->prueba)->first()
                    ->update(
                        [
                            'last_message' => $data->content,
                        ]
                    );

                $this->resetForm();
                if ($prueba_imagen) {
                    $this->photo = null;
                    $this->iteration++;
                }
                $this->dispatchBrowserEvent('scrollToBottom');
            }
        } else {
            $conversation = new AppConversation();

            $conversation->alias_user_id = auth()->id();

            $conversation->alias_contact_id = $this->prueba;

            $conversation->save();
            $data = new AppMessage;
            $data->content = $this->message;
            $prueba_imagen = $this->photo;
            if ($prueba_imagen) {
                $image = $this->photo->store('img', 'public');
                $data->image = $image;
            }
            $data->alias_from_id = auth()->id();
            $data->alias_to_id = $this->prueba;
            $data->save();

            $this->resetForm();
            if ($prueba_imagen) {
                $this->photo = null;
                $this->iteration++;
            }
            $this->dispatchBrowserEvent('scrollToBottom');
        }

       
        $this->render();
      
    }

    public function SendFile()
    {
        //dd('aaa');

       
        if (AppConversation::where('alias_user_id', auth()->id())->where('alias_contact_id', $this->prueba)->first()) {
            $data = new AppMessage;
            $prueba_imagen = $this->photo;
            if ($prueba_imagen) {
                $image = $this->photo->store('img', 'public');
                $data->image = $image;
            }
            $data->content = $this->message;
            //dd($data->content);
            $data->alias_from_id = auth()->id();
            $data->alias_to_id = $this->sender->id;
            $data->save();



            if ($data->save()) {
                if (AppConversation::where('status_deleted_one', auth()->id())
                    ->orWhereNull('status_deleted_one')
                    ->where('alias_user_id', auth()->id())
                    ->where('alias_contact_id', $this->prueba)->first()
                ) {
                    AppConversation::where('alias_user_id', auth()->id())->where('alias_contact_id', $this->prueba)->first()
                        ->update(
                            [
                                'last_message' => $data->content,
                                'status_deleted_one' => NULL
                            ]
                        );
                } elseif (AppConversation::where('status_deleted_two', auth()->id())
                    ->orWhereNull('status_deleted_two')
                    ->where('alias_user_id', auth()->id())
                    ->where('alias_contact_id', $this->prueba)->first()
                ) {
                    AppConversation::where('alias_user_id', auth()->id())->where('alias_contact_id', $this->prueba)->first()
                        ->update(
                            [
                                'last_message' => $data->content,
                                'status_deleted_two' => NULL
                            ]
                        );
                } else {
                    AppConversation::where('alias_user_id', auth()->id())->where('alias_contact_id', $this->prueba)->first()
                        ->update(
                            [
                                'last_message' => $data->content
                            ]
                        );
                }
                $this->resetForm();
                if ($prueba_imagen) {
                    $this->photo = null;
                    $this->iteration++;
                }
            }
        } elseif (AppConversation::where('alias_contact_id', auth()->id())->where('alias_user_id', $this->prueba)->first()) {

            $data = new AppMessage;
            $prueba_imagen = $this->photo;
            if ($prueba_imagen) {
                $image = $this->photo->store('img', 'public');
                $data->image = $image;
            }
            $data->content = $this->message;
            //dd($data->content);
            $data->alias_from_id = auth()->id();
            $data->alias_to_id = $this->sender->id;
            if ($data->save()) {
                //dd('retrospecter');
                if (AppConversation::where('status_deleted_one', auth()->id())
                    ->orWhereNull('status_deleted_one')
                    ->where('alias_contact_id', auth()->id())
                    ->where('alias_user_id', $this->prueba)->first()
                ) {
                    //dd('retrospecter');
                    AppConversation::where('alias_contact_id', auth()->id())->where('alias_user_id', $this->prueba)->first()
                        ->update(
                            [
                                'last_message' => $data->content,
                                'status_deleted_one' => NULL,
                                'status_deleted_two' => NULL
                            ]
                        );
                } elseif (AppConversation::where('status_deleted_two', auth()->id())
                    ->orWhereNull('status_deleted_two')
                    ->where('alias_contact_id', auth()->id())
                    ->where('alias_user_id', $this->prueba)->first()
                ) {
                    //dd('retrospecter2');
                    AppConversation::where('alias_contact_id', auth()->id())->where('alias_user_id', $this->prueba)->first()
                        ->update(
                            [
                                'last_message' => $data->content,
                                'status_deleted_two' => NULL,
                                'status_deleted_one' => NULL
                            ]
                        );
                } else {
                    //dd('retrospecter');
                    AppConversation::where('alias_contact_id', auth()->id())->where('alias_user_id', $this->prueba)->first()
                        ->update(
                            [
                                'last_message' => $data->content,
                                'status_deleted_one' => NULL,
                                'status_deleted_two' => NULL
                            ]
                        );
                }


                $this->resetForm();
                if ($prueba_imagen) {
                    $this->photo = null;
                    $this->iteration++;
                }
            }
        } else {
            $conversation = new AppConversation();

            $conversation->alias_user_id = auth()->id();

            $conversation->alias_contact_id = $this->prueba;

            $conversation->save();
            $data = new AppMessage;
            $data->content = $this->message;
            $prueba_imagen = $this->photo;
            if ($prueba_imagen) {
                $image = $this->photo->store('img', 'public');
                $data->image = $image;
            }
            $data->alias_from_id = auth()->id();
            $data->alias_to_id = $this->prueba;
            $data->save();

            $this->resetForm();
            if ($prueba_imagen) {
                $this->photo = null;
                $this->iteration++;
            }
        }

        
        $this->render();
        $this->dispatchBrowserEvent('scrollToBottom');
      
    }

    public function update($userId)
    {
        $this->emitself('refreshComponent');
        $user = SysUser::find($userId);
        //dd($user);

        AppMessage::where('alias_from_id', auth()->id())->where('alias_to_id', $userId)->orWhere('alias_from_id', $userId)->where('alias_to_id', auth()->id())
            ->update(
                [
                    'status' => 0
                ]
            );
    }
}
