<?php

namespace App\Http\Livewire\Mobile;

use Livewire\Component;
use App\AppOrgProduct;
use App\AppOrgUserInventory;
use App\SysDictionary;
use App\SysCountry;

class RegisterUserMobile extends Component
{

    public $first_name,$last_name,$address,$zipcode,$city,$country_id,
    $email,$user_name,$password,$brRegisterCoYearsOld,$brRegisterCoTerms;

    protected $rules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'address' => 'required',
        'zipcode' => 'required',
        'country_id' => 'required',
        'email' => 'required',
        'user_name' => 'required',
        'password' => 'required',
    ];

    public function render()
    {
        $condition = SysDictionary::where('code', 'GAME_STATE')->get(['value_id', 'value', 'parent_id']);
        $country = SysCountry::all();
        return view('livewire.mobile.register-user-mobile', [
            'country' => $country
        ]);
    }

    public function store(){
        $this->validate();
        
    	AppOrgNewsItem::create([
          'title' => $this->title,
          'content' => $this->content,
          'is_enabled' => $this->is_enabled,
    	]);

        $this->emit('alert');
        $this->emit('render');
        $this->reset();

        
    }

 
}
