<?php

namespace App\Http\Livewire\Mobile;

use Livewire\Component;
use App\AppOrgProduct;
use App\AppOrgUserInventory;
use App\SysDictionary;
use App\SysCountry;

class UploadProductMobile extends Component
{
    public $totalRecords;
    public $loadAmount = 10;
    public $query = '';
    public $search_platform = '';
    public $search_category = '';
    public $search_region = '';

    protected $listeners = ['postAdded' => 'incrementPostCount'];

    public function loadMore()
    {
        $this->loadAmount += 10;
    }

    public function mount()
    {
        $this->totalRecords = AppOrgProduct::count();
    }

    public function updateQuery()
    {
        $this->query = trim($this->query);
    }


    public function render()
    {
        $platform = SysDictionary::where("code", "GAME_PLATFORM")->get();
        $region = SysDictionary::where("code", "GAME_REGION")->get();
        return view('livewire.mobile.upload-product-mobile', [
            'platform' => $platform,
            'region' => $region,
        ])->with(
            'products',
            AppOrgProduct::where('name', 'LIKE', "%{$this->query}%")
                ->where('platform', 'LIKE', "%{$this->search_platform}%")
                ->where('region', 'LIKE', "%{$this->search_region}%")
                ->whereHas('categoryProd', function ($q) {
                    $q->where('category_text', 'LIKE', "%{$this->search_category}%");
                })
                ->orderBy('id', 'desc')
                ->limit($this->loadAmount)
                ->get()
        );
    }
}
