<?php

namespace App\Http\Livewire\Mobile;

use App\AppMessage;
use App\AppConversation;
use Livewire\Component;
use App\SysUser;
use Livewire\WithFileUploads;
use Carbon\Carbon;

class ConversationMobile extends Component
{
    use WithFileUploads;
    public $photo;
    public $iteration;
    public $deleteId = '';
    public $message;
    public $allmessages;
    public $sender;
    public $search = '';

    protected $rules = [
        'photo' => 'nullable|image',
    ];

    public function render()
    {


        $beta  =  AppConversation::where('alias_user_id', auth()->id())
            ->whereHas('prueba', function ($q) {
                $q->where('user_name', 'LIKE', "%{$this->search}%");
            })
            ->orWhere(function ($query) {
                $query->where('alias_contact_id',  auth()->id())
                    ->whereHas('user', function ($q) {
                        $q->where('user_name', 'LIKE', "%{$this->search}%");
                    });
            })
            ->orderBy('updated_at', 'desc')
            ->get();


        $messages = AppMessage::with('user')
            ->latest()
            ->take(10)
            ->get()
            ->sortBy('id');


        //dd($beta);

        $users = SysUser::all();
        $sender = $this->sender;
        $prueba = AppMessage::where('alias_from_id', \Auth::user()->id)->where('alias_to_id')->get();
        //dd($prueba);
        $this->allmessages;
        return view('livewire.mobile.conversation-mobile', compact('users', 'sender', 'prueba', 'beta', 'messages'));
    }




    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function updateSeen($conversationId)
    {
        $conversation = AppConversation::find($conversationId);
        
        if ($conversation) {
            $isCurrentUserAliasUser = $conversation->prueba->user_name == auth()->user()->user_name;
    
            if ($isCurrentUserAliasUser) {
                $conversation->seen_user_id = 'Y';
            } else {
                $conversation->seen_contact_id = 'Y';
            }
    
            $conversation->save();
        }
    }
    


    public function delete()
    {
        AppConversation::find($this->deleteId)->delete();
    }
}
