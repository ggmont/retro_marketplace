<?php

namespace App\Http\Livewire\Mobile;

use App\SysUser;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Livewire\WithFileUploads;
use Intervention\Image\Facades\Image;

use Livewire\Component;

class PersonalData extends Component
{

    use WithFileUploads;
    public $email, $first_name, $last_name, $phone, $address, $zipcode,
        $city, $password, $profile_picture, $iteration, $province, $dni;
    public $prueba = '';
    public $ultra = '';
    public $state = [];
    public $user;
    public $showEditModal = false;


    protected $listeners = ['refreshComponent' => '$refresh'];

    protected $rules = [
        'email' => 'required|email|unique:sysUsers,email',
    ];

    public function mount()
    {
        //dd('aa');
        $this->dispatchBrowserEvent('show-form');
    }

    public function render()
    {
        $user =  SysUser::where('id', Auth::id())->get();

        //dd($user);
        return view('livewire.mobile.personal-data', [
            'user' => $user,
        ]);
    }

    public function edit(SysUser $id)
    {

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $user->id;

        //dd($this->user);

        $this->email = $user->email;

        //dd($this->email);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function editProfilePhoto(SysUser $id)
    {

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $user->id;

        //dd($this->user);

        $this->profile_picture = $user->profile_picture;

        //dd($this->email);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-profile-picture');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function editNam(SysUser $id)
    {

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $user->id;

        //dd($this->user);
        $this->first_name = $user->first_name;
        $this->last_name = $user->last_name;
        //dd($this->email);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-name');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function editPass(SysUser $id)
    {

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        //$this->reset();

        $this->showEditModal = true;

        $this->user = $user->id;

        //dd($this->user);
        $this->password = "";
        //dd($this->password);
        //$encrypted = Crypt::encryptString($this->password);
        //dd($encrypted);
        //$decrypted = Crypt::decryptString($this->password);
        //dd($decrypted);

        //$this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-pass');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function editCity(SysUser $id)
    {

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $user->id;

        //dd($this->user);
        $this->city = $user->city;
        //dd($this->email);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-city');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function editPhone(SysUser $id)
    {

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $user->id;

        //dd($this->user);
        $this->phone = $user->phone;
        //dd($this->email);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-phone');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function editAddress(SysUser $id)
    {

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $user->id;

        //dd($this->user);
        $this->address = $user->address;
        //dd($this->email);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-address');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function editZipcode(SysUser $id)
    {

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $user->id;

        //dd($this->user);
        $this->zipcode = $user->zipcode;
        //dd($this->email);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-zipcode');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function editProvince(SysUser $id)
    {

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $user->id;

        //dd($this->user);
        $this->province = $user->province;
        //dd($this->email);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-province');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function editDni(SysUser $id)
    {

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $user->id;

        //dd($this->user);
        $this->dni = $user->dni;
        //dd($this->email);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-dni');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function updateProvince()
    {
        //dd('hhh');
        $probando = SysUser::find($this->user);
        //dd($probando);
        $probando->update([
            'province' => $this->province
        ]);
        $this->emitself('refreshComponent');
        $this->dispatchBrowserEvent('hide-form-province');
        $this->dispatchBrowserEvent('show-form-ultimate');
        //dd($probando);
    }

    public function updateDni()
    {
        //dd('hhh');
        $probando = SysUser::find($this->user);
        //dd($probando);
        $probando->update([
            'dni' => $this->dni
        ]);
        $this->emitself('refreshComponent');
        $this->dispatchBrowserEvent('hide-form-dni');
        $this->dispatchBrowserEvent('show-form-ultimate');
        //dd($probando);
    }

    public function updatePass()
    {
        //dd('hhh');
        $probando = SysUser::find($this->user);

        $probando->update([
            'password' =>  bcrypt($this->password)
        ]);
        $this->emitself('refreshComponent');
        $this->dispatchBrowserEvent('hide-form-pass');
        $this->dispatchBrowserEvent('show-form-ultimate');
        //dd($probando);
    }



    public function updateZipcode()
    {
        //dd('hhh');
        $probando = SysUser::find($this->user);
        //dd($probando);
        $probando->update([
            'zipcode' => $this->zipcode
        ]);
        $this->emitself('refreshComponent');
        $this->dispatchBrowserEvent('hide-form-zipcode');
        $this->dispatchBrowserEvent('show-form-ultimate');
        //dd($probando);
    }

    public function updateCity()
    {
        //dd('hhh');
        $probando = SysUser::find($this->user);
        //dd($probando);
        $probando->update([
            'city' => $this->city
        ]);
        $this->emitself('refreshComponent');
        $this->dispatchBrowserEvent('hide-form-city');
        $this->dispatchBrowserEvent('show-form-ultimate');
        //dd($probando);
    }

    public function updateAddress()
    {
        //dd('hhh');
        $probando = SysUser::find($this->user);
        //dd($probando);
        $probando->update([
            'address' => $this->address
        ]);
        $this->emitself('refreshComponent');
        $this->dispatchBrowserEvent('hide-form-address');
        $this->dispatchBrowserEvent('show-form-ultimate');
        //dd($probando);
    }

    public function updatePhone()
    {
        //dd('hhh');
        $probando = SysUser::find($this->user);
        //dd($probando);
        $probando->update([
            'phone' => $this->phone
        ]);
        $this->emitself('refreshComponent');
        $this->dispatchBrowserEvent('hide-form-phone');
        $this->dispatchBrowserEvent('show-form-ultimate');
        //dd($probando);
    }

    public function updateName()
    {
        //dd('hhh');
        $probando = SysUser::find($this->user);
        //dd($probando);
        $probando->update([
            'first_name' => $this->first_name,
            'last_name' => $this->last_name
        ]);
        $this->emitself('refreshComponent');
        $this->dispatchBrowserEvent('hide-form-name');
        $this->dispatchBrowserEvent('show-form-ultimate');
        //dd($probando);
    }


    public function update()
    {
        //dd('hhh');
        $this->validate();
        $probando = SysUser::find($this->user);
        //dd($probando);
        $probando->update([
            'email' => $this->email,
        ]);
        $this->emitself('refreshComponent');
        $this->dispatchBrowserEvent('hide-form');
        $this->dispatchBrowserEvent('show-form-ultimate');
        //dd($probando);
    }

    public function createUser()
    {
        $validatedData = Validator::make($this->state, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ])->validate();

        $validatedData['password'] = bcrypt($validatedData['password']);

        if ($this->photo) {
            $validatedData['avatar'] = $this->photo->store('/', 'avatars');
        }

        User::create($validatedData);

        // session()->flash('message', 'User added successfully!');

        $this->dispatchBrowserEvent('hide-form', ['message' => 'User added successfully!']);
    }

    public function openModal()
    {
        $this->emit('show');
    }



    public function SendFile()
    {

        //dd('prueba');

        $data = SysUser::where('id', Auth::id())->first();
        $filename  = 'profile_picture' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $this->profile_picture->getClientOriginalExtension();
        $path = public_path('/uploads/profile-images/' . $filename);
        $dateFolder = date('Y') . '/' . date('m') . '/' . date('d') . '/';
        $prueba_imagen = $this->profile_picture;
        $directory = $path . $dateFolder;

        // Obtenemos las dimensiones originales de la imagen
        list($width, $height) = getimagesize($prueba_imagen->getRealPath());

        // Redimensionamos la imagen manteniendo la proporción
        $newWidth = $width;
        $newHeight = $height;
        if ($width > $height) {
            // Si la imagen es horizontal
            if ($width > 500) {
                $newWidth = 500;
                $newHeight = intval($height * 500 / $width);
            }
        } else {
            // Si la imagen es vertical
            if ($height > 500) {
                $newHeight = 500;
                $newWidth = intval($width * 500 / $height);
            }
        }

        // Guardamos la imagen redimensionada
        Image::make($prueba_imagen->getRealPath())->resize($newWidth, $newHeight)->encode('jpg')->save($path);

        $data->profile_picture = '/uploads/profile-images/' . $filename;

        $data->update([
            'profile_picture' => '/uploads/profile-images/' . $filename
        ]);

        $this->emitself('refreshComponent');
        $this->reset();
        $this->dispatchBrowserEvent('hide-form-profile-picture');
        $this->dispatchBrowserEvent('show-form-ultimate');
    }
}
