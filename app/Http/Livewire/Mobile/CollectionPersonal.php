<?php

namespace App\Http\Livewire\Mobile;

use Livewire\Component;
use App\AppOrgProduct;
use Auth;
use App\AppOrgUserInventory;
use App\SysDictionary;
use App\SysCountry;

class CollectionPersonal extends Component
{
    public $favoriteId = '';
    public $desfavoriteId = '';
    public $deleteId = '';
    public $totalRecords;
    public $loadAmount = 20;
    public $search = '';
    public $probando = '';
    public $search_box = '';
    public $search_manual = '';
    public $search_game = '';
    public $search_extra = '';
    public $search_cover = '';
    public $search_inside = '';
    public $search_country = '';
    public $search_platform = '';
    public $search_category = '';
    public $search_region = '';
    public $search_language = '';
    public $search_media = '';
    public $search_prueba = '';
    public $showEditModal = false;
    public $state = [];
    public $inventory;
    public $price;

    protected $listeners = ['postAdded' => 'incrementPostCount'];

    protected $queryString = [
		'search' => ['except' => '' ],
        'search_category' => ['except' => '' ],
     ];

    public function loadMore()
    {
        $this->loadAmount += 10;
        $this->emit('star');
    }

    public function incrementPostCount()
    {
        $this->probando = 'NOT-PRES';
        $this->search_prueba = AppOrgUserInventory::where('box_condition', 'LIKE', "%{$this->probando}%")->get();
        $this->emit('star');
        //dd($this->search_prueba);
    }

    public function mount()
    {
        $this->totalRecords = AppOrgUserInventory::where('user_id', Auth::id())->where('quantity', '>', 0)->count();
        $this->emit('star');
    }

    public function clear_all()
    {
        $this->search = '';
        $this->search_box = '';
        $this->search_manual = '';
        $this->search_game = '';
        $this->search_extra = '';
        $this->search_cover = '';
        $this->search_inside = '';
        $this->search_platform ='';
        $this->search_region ='';
        $this->search_category ='';
    }

    public function render()
    {
        $condition = SysDictionary::where('code', 'GAME_STATE')->get(['value_id', 'value', 'parent_id']);
        //dd($condition);
        $country = SysCountry::all();
        $inventories_game = AppOrgUserInventory::where('quantity', '>', 0)
        ->whereHas('product', function ($q) {
            $q->whereHas('categoryProd', function ($q) {
                $q->where('parent_id', 1);
            });
        })->count();
        $inventories_consoles = AppOrgUserInventory::where('quantity', '>', 0)
        ->whereHas('product', function ($q) {
            $q->whereHas('categoryProd', function ($q) {
                $q->where('parent_id', 2);
            });
        })->count();
        $inventories_perifericos = AppOrgUserInventory::where('quantity', '>', 0)
        ->whereHas('product', function ($q) {
            $q->whereHas('categoryProd', function ($q) {
                $q->where('parent_id', 3);
            });
        })->count();
        $inventories_accesorios = AppOrgUserInventory::where('quantity', '>', 0)
        ->whereHas('product', function ($q) {
            $q->whereHas('categoryProd', function ($q) {
                $q->where('parent_id', 4);
            });
        })->count();
        $inventories_merch = AppOrgUserInventory::where('quantity', '>', 0)
        ->whereHas('product', function ($q) {
            $q->whereHas('categoryProd', function ($q) {
                $q->where('parent_id', 173);
            });
        })->count();
        $platform = SysDictionary::where("code", "GAME_PLATFORM")->get();
        $region = SysDictionary::where("code", "GAME_REGION")->get();
        $media = SysDictionary::where("code", "GAME_SUPPORT")->get();
        $language = SysDictionary::where("code", "GAME_LANGUAGE")->get();
        return view('livewire.mobile.collection-personal', [
            'country' => $country,
            'platform' => $platform,
            'region' => $region,
            'media' => $media,
            'language' => $language,
            'inventories_game' => $inventories_game,
            'inventories_consoles' => $inventories_consoles,
            'inventories_perifericos' => $inventories_perifericos,
            'inventories_accesorios' => $inventories_accesorios,
            'inventories_merch' => $inventories_merch,
            'condition' => $condition
        ])->with(
            'seller',
            AppOrgUserInventory::where('user_id', Auth::id())
                ->where('in_collection', 'Y')
                ->where('quantity', '>', 0)
                ->where('box_condition', 'LIKE', "%{$this->search_box}%")
                ->where('manual_condition', 'LIKE', "%{$this->search_manual}%")
                ->where('cover_condition', 'LIKE', "%{$this->search_cover}%")
                ->where('game_condition', 'LIKE', "%{$this->search_game}%")
                ->where('extra_condition', 'LIKE', "%{$this->search_extra}%")
                ->whereHas('product', function ($q) {
                        $q->where('name', 'LIKE', "%{$this->search}%");
                        $q->where('platform', 'like', '%' .$this->search_platform);
                        $q->where('region','LIKE',"%{$this->search_region}%");
                        $q->where('language','LIKE',"%{$this->search_language}%");
                        $q->whereHas('categoryProd', function ($q) {
                            $q->where('category_text','LIKE',"%{$this->search_category}%");
                        });
                })
                ->whereHas('user', function ($q) {
                    $q->whereHas('country', function ($q) {
                        $q->where('name', 'LIKE', "%{$this->search_country}%");
                    });
                })
                ->orderBy('id', 'desc')
                ->limit($this->loadAmount)
                ->get()
        );
    }

    public function PutOnSale(AppOrgUserInventory $id)
    {

        //dd($id);
        $inventory =  AppOrgUserInventory::find($id)->first();
        //dd($inventory);
        $this->reset();

        $this->showEditModal = true;

        $this->inventory = $inventory->id;

        //dd($this->user);
        $this->price = $inventory->price;
        //dd($this->email);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-price');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function updatePutOnSale()
    {
        AppOrgUserInventory::find($this->inventory)
        ->update([
            'price' => $this->price,
            'in_collection' => 'N'
        ]);
        $this->emitself('refreshComponent');
        $this->dispatchBrowserEvent('hide-form-price');
        $this->dispatchBrowserEvent('show-form-ultimate');
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function delete()
    {
        AppOrgUserInventory::find($this->deleteId)->delete();
        $this->emit('alert');
        $this->emit('render');
    }

    public function favoriteId($id)
    {
        $this->favoriteId = $id;
    }

    public function update()
    {
        AppOrgUserInventory::find($this->favoriteId)->update(['favorite' => 1]);
        $this->emit('favorite');
        $this->emit('render');
    }

    public function desfavoriteId($id)
    {
        $this->desfavoriteId = $id;
    }

    public function update_desfavorite()
    {
        AppOrgUserInventory::find($this->desfavoriteId)->update(['favorite' => 0]);
        $this->emit('desfavorite');
        $this->emit('render');
    }
}
