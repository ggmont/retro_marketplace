<?php

namespace App\Http\Livewire\Mobile\Purchase;

use App\AppOrgOrder;
use Livewire\Component;
use Auth;
use Carbon\Carbon;
use Livewire\WithPagination;

class ArrivedPurchase extends Component
{
    public $totalRecords;
    public $loadAmount = 100;
    public $search_seller = '';

    public function loadMore()
    {
        $this->loadAmount += 10;
    }

    public function mount()
    {
        $this->totalRecords = AppOrgOrder::where('buyer_user_id', Auth::id())->where('status', 'DD')->count();
    }

    public function render()
    {
        return view('livewire.mobile.purchase.arrived-purchase', [
            'arrived' => AppOrgOrder::where('buyer_user_id', Auth::id())
            ->where('status', 'DD')
            ->whereHas('seller', function ($q) {
                $q->where('user_name', 'LIKE', "%{$this->search_seller}%");
            })
            ->orderBy('id','desc')
            ->limit($this->loadAmount)
            ->get()
        ]); 
    }
}
