<?php

namespace App\Http\Livewire\Mobile\Purchase;

use App\AppOrgOrder;
use Livewire\Component;
use Auth;
use Carbon\Carbon;
use Livewire\WithPagination;

class CancelInternationalPurchase extends Component
{
    public $totalRecords;
    public $loadAmount = 100;
    public $search_seller = '';

    public function loadMore()
    {
        $this->loadAmount += 10;
    }

    public function mount()
    {
        $this->totalRecords = AppOrgOrder::where(function ($query) {
            $query->where(function ($query) {
                $query->whereHas('buyer', function ($query) {
                    $query->where('country_id', '!=', 46); // ID del país de España
                });
            })
            ->orWhere(function ($query) {
                $query->whereHas('seller', function ($query) {
                    $query->where('country_id', '!=', 46); // ID del país de España
                });
            });
        })
        ->where('buyer_user_id', Auth::id())
        ->where('status', 'CN')
        ->count();
        
    }

    public function render()
    {
        return view('livewire.mobile.purchase.cancel-international-purchase', [
            'cancel' => AppOrgOrder::where('buyer_user_id', Auth::id())
                ->where('status', 'CN')
                ->where(function ($query) {
                    $query->where(function ($query) {
                        $query->whereHas('seller', function ($query) {
                            $query->where('country_id', '!=', 46); // ID del país de España
                        });
                    })
                    ->orWhere(function ($query) {
                        $query->whereHas('buyer', function ($query) {
                            $query->where('country_id', '!=', 46); // ID del país de España
                        });
                    });
                })
                ->whereHas('seller', function ($q) {
                    $q->where('user_name', 'LIKE', "%{$this->search_seller}%");
                })
                ->orderBy('id', 'desc')
                ->limit($this->loadAmount)
                ->get()
        ]);        
    }
}
