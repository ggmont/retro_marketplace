<?php

namespace App\Http\Livewire\Mobile\Purchase;

use App\AppOrgOrder;
use Livewire\Component;
use Auth;
use Carbon\Carbon;
use Livewire\WithPagination;

class UnpaidPurchase extends Component
{
    public $totalRecords;
    public $loadAmount = 100;
    public $search_seller = '';

    public function loadMore()
    {
        $this->loadAmount += 10;
    }

    public function mount()
    {
        $this->totalRecords = AppOrgOrder::where('buyer_user_id', Auth::id())
        ->whereIn('status', ['CW', 'RP'])
        ->whereHas('seller', function ($q) {
            $q->where('country_id', 46); // ID del país de España
        })
        ->whereHas('buyer', function ($q) {
            $q->where('country_id', 46); // ID del país de España
        })
        ->count();
    
    }

    public function render()
    {
        return view('livewire.mobile.purchase.unpaid-purchase', [
            'unpaid' => AppOrgOrder::where('buyer_user_id', Auth::id())
                ->whereIn('status', ['CW', 'RP'])
                ->whereHas('seller', function ($q) {
                    $q->where('user_name', 'LIKE', "%{$this->search_seller}%")
                        ->where('country_id', 46); // ID del país de España
                })
                ->whereHas('buyer', function ($q) {
                    $q->where('country_id', 46); // ID del país de España
                })
                ->orderBy('id', 'desc')
                ->get()
        ]);
        
    }
}
