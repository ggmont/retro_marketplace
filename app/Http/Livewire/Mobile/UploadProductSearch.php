<?php

namespace App\Http\Livewire\Mobile;
use App\AppOrgProduct;
use App\SysDictionary;
use Livewire\Component;

class UploadProductSearch extends Component
{
    public $query;
    public $products;
    public $highlightIndex;
    public $search_platform = '';
    public $search_category = '';
    public $search_region = '';

    protected $queryString = [
        'query' => ['except' => ''],
    ];

    public function mount()
    {
        $this->reset();
    }

    
    public function restart()
    {
        $this->query = '';
        $this->products = [];
        $this->highlightIndex = 0;
    }

    public function incrementHighlight()
    {
        if($this->highlightIndex === count($this->products) - 1) {
            $this->highlightIndex = 0;
            return;
        }
        $this->highlightIndex++;
    }

    public function decrementHighlight()
    {
        if($this->highlightIndex === 0) {
            $this->highlightIndex = count($this->products) - 1;
            return;
        }
        $this->highlightIndex--;
    }

    public function selectProduct()
    {
        $product = $this->products[$this->highlightIndex] ?? null;

        if ($product) {
            $this->redirect(route('product-show', $product['id']));
        }
    }

    public function updatedQuery()
    {
        $this->products = AppOrgProduct::where('name','like','%' . $this->query . '%')
        ->where('platform','LIKE',"%{$this->search_platform}%")
        ->where('region','LIKE',"%{$this->search_region}%")
        ->whereHas('categoryProd', function ($q) {
            $q->where('category_text','LIKE',"%{$this->search_category}%");
        })
        ->take(20)
        ->with('images')
        ->get();

        //dd($this->products);
    }

    public function render()
    {
        $platform = SysDictionary::where("code", "GAME_PLATFORM")->get();
        $region = SysDictionary::where("code", "GAME_REGION")->get();
        $media = SysDictionary::where("code", "GAME_SUPPORT")->get();
        $language = SysDictionary::where("code", "GAME_LANGUAGE")->get();

        return view('livewire.mobile.upload-product-search', [
            'platform' => $platform,
            'region' => $region,
            'media' => $media,
            'language' => $language,
        ]);
    }
}
