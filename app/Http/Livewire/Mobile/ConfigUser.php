<?php

namespace App\Http\Livewire\Mobile;

use App\SysUser;
use App\SysUserSocial;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Livewire\WithFileUploads;
use Intervention\Image\Facades\Image;
use Illuminate\Validation\Rule;

use Livewire\Component;

class ConfigUser extends Component
{

    use WithFileUploads;
    public $profile_picture, $iteration;
    public $prueba = '';
    public $ultra = '';
    public $state = [];
    public $user;
    public $showEditModal = false;

    protected $listeners = ['refreshComponent' => '$refresh'];

    protected $rules = [
        'profile_picture' => 'nullable|image',
    ];

    public function render()
    {
        $user =  SysUser::where('id', Auth::id())->get();
        $social = count(SysUserSocial::where('user_id',  Auth::id())->get());
        $social_user =  SysUserSocial::where('user_id',  Auth::id())->get();
        //dd($social_user);
        return view('livewire.mobile.config-user', [
            'user' => $user,
            'social' => $social,
            'social_user' => $social_user,
        ]);
    }
}
