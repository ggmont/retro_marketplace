<?php

namespace App\Http\Livewire\Mobile;
use App\AppOrgOrder;
use Auth;
use Livewire\Component;

class OrderBeseif extends Component
{

    public $order;

    public function mount($orderIdentification)
    {
        //$this->prueba = SysUser::findOrFail($user);
        $this->order = $orderIdentification;
        
        //$usuario = SysUser::where('user_name', $this->prueba)->first();
        //$ultra = AppOrgUserInventory::where('user_id', $usuario->id);
        $order = AppOrgOrder::where('order_identification',$this->order)->where('buyer_user_id', Auth::id())->first();

        if($order->status == 'CR') {
            $this->dispatchBrowserEvent('show-form-ultimate');
        }
       
    }

    public function render()
    {
        return view('livewire.mobile.order-beseif');
    }

    public function prueba()
    {
        $this->dispatchBrowserEvent('show-form-ultimate');
        //dd($probando);
    }
}
