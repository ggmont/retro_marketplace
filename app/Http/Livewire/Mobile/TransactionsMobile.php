<?php

namespace App\Http\Livewire\Mobile;

use Auth;
use DB;
use App\AppOrgOrder;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Collection;

class TransactionsMobile extends Component
{

    use WithPagination;

    public $perPage = '75';
    public $totalRecords;
    public $loadAmount = 75;
    public $search = '';

    protected $listeners = ['postAdded' => 'incrementPostCount'];

    public function loadMore()
    {
        $this->loadAmount += 20;
    }

    public function mount()
    {
        $userId = Auth::id();
        $this->totalRecords = AppOrgOrder::where('paid_out', 'Y')->where(function ($query) {
            $query->where('buyer_user_id', Auth::id())
                  ->orWhere('seller_user_id', Auth::id());
        })->whereIn('status', ['DD','RE','RF','FD','CE','AC','RC','CR','ST','EC','SC','AS','YC','YV'])->count();

        //dd($this->totalRecords);
    }

    public function render()
    {
        $userId = Auth::id();
        $dates = AppOrgOrder::where('buyer_user_id', Auth::id())->orWhere('seller_user_id', Auth::id())->orderBy('updated_at')
        ->get([
            DB::raw('DATE_FORMAT(updated_at, "%m / %Y" ) AS `Dates`'),
        ]);
        $fecha = $dates->unique('Dates');
        //dd($fecha);
    
        $tipos = AppOrgOrder::where('status', '!=', 'CS')->select(DB::raw("IF(`buyer_user_id`='$userId', CONCAT('C', `status`), CONCAT('V', `status`)) as status_type"), "status")->where('buyer_user_id', Auth::id())->orWhere('seller_user_id', Auth::id())->get()->unique('status_type');
        
        $trans = AppOrgOrder::where('paid_out', 'Y')->where(function ($query) {
            $query->where('buyer_user_id', Auth::id())
                  ->orWhere('seller_user_id', Auth::id());
        })->whereIn('status', ['DD','RE','RF','FD','CE','AC','RC','CR','ST','EC','SC','AS','YC','YV'])
        ->select("*", DB::raw("IF(`buyer_user_id`='$userId', CONCAT('C', `status`), CONCAT('V', `status`)) as status_type"))
        ->having('status_type', '!=', "VEC")
        ->orderBy('updated_at', 'desc')->limit($this->loadAmount)
        ->get();
        
        
        return view('livewire.mobile.transactions-mobile', [

            'userId' => $userId,
            'dates' => $dates,
            'fecha' => $fecha,
            'tipos' => $tipos,
            'trans' => $trans,

        ]);
    }

}
