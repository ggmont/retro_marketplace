<?php

namespace App\Http\Livewire\Mobile;

use Livewire\Component;
use App\AppOrgProduct;
use Auth;
use App\AppOrgUserHistorialBidInventory;
use App\SysDictionary;
use App\SysCountry;

class OfferSentTable extends Component
{
    public function render()
    {
        $offersSent = AppOrgUserHistorialBidInventory::where('user_bid', Auth::id())
            ->orderBy('created_at', 'desc') // Ordenar de forma ascendente por la columna 'created_at'
            ->get();

        return view('livewire.mobile.offer-sent-table', [
            'offersSent' => $offersSent
        ]);
    }
}
