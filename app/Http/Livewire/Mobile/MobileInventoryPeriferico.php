<?php

namespace App\Http\Livewire;

use Livewire\Component;

class MobileInventoryPeriferico extends Component
{
    public function render()
    {
        return view('livewire.mobile-inventory-periferico');
    }
}
