<?php

namespace App\Http\Livewire\Mobile;

use App\AppOrgProduct;
use App\SysDictionary;
use App\AppOrgUserInventory;
use App\SysCountry;
use Auth;
use Livewire\Component;
use Livewire\WithPagination;

class MobileInventoryGameTable extends Component
{
    use WithPagination;

    protected $queryString = [
        'search' => ['except' => ''],
        'search_country' => ['except' => ''],
    ];

    public $deleteId = '';
    public $perPage = '50';
    public $loadAmount = 20;
    public $prueba;
    public $articulo;
    public $search = '';
    public $search_box = '';
    public $search_manual = '';
    public $search_game = '';
    public $search_extra = '';
    public $search_cover = '';
    public $search_inside = '';
    public $search_country = '';
    public $search_category = '';
    public $search_region = '';
    public $viewData;
    public $update;

    public function loadMore()
    {
        $this->loadAmount += 10;
    }

    public function updatingSearchbox()
    {
        $this->resetPage();
        $this->emit('game');
    }

    public function updatingSearchcountry()
    {
        $this->resetPage();
        $this->emit('game');
    }

    public function mount($producto)
    {
        //$this->prueba = SysUser::findOrFail($user);
        $this->prueba = $producto->id;
        $this->articulo = $producto;
        $this->emit('game');
    }

    public function clear()
    {
        $this->search_region = '';
        $this->search_country = '';
        $this->search_box = '';
        $this->search_manual = '';
        $this->search_cover = '';
        $this->search_game = '';
        $this->search_extra = '';
        $this->emit('game');
        $this->emit('clearFilters');
    }

    public function render()
    {
        $product = $this->articulo;
        $condition = SysDictionary::where('code', 'GAME_STATE')->get(['value_id', 'value', 'parent_id']);
        $country = SysCountry::all();
        $region = SysDictionary::where("code", "GAME_REGION")->get();

        $seller = AppOrgUserInventory::ofSeller()->where('product_id', $this->prueba)
            ->where('quantity', '>', 0)
            ->where('user_name', 'LIKE', "%{$this->search}%")
            ->where('box_condition', 'LIKE', "%{$this->search_box}%")
            ->where('manual_condition', 'LIKE', "%{$this->search_manual}%")
            ->where('cover_condition', 'LIKE', "%{$this->search_cover}%")
            ->where('game_condition', 'LIKE', "%{$this->search_game}%")
            ->where('extra_condition', 'LIKE', "%{$this->search_extra}%")
            ->whereHas('product', function ($q) {;
                $q->where('region', 'LIKE', "%{$this->search_region}%");
                $q->whereHas('categoryProd', function ($q) {
                    $q->where('category_text', 'LIKE', "%{$this->search_category}%");
                });
            })
            ->whereHas('user', function ($q) {
                $q->whereHas('country', function ($q) {
                    $q->where('name', 'LIKE', "%{$this->search_country}%");
                });
            })
            ->orderBy('price', 'ASC')
            ->paginate($this->perPage);
        //dd($seller);
        return view('livewire.mobile.mobile-inventory-game-table', [
            'country' => $country,
            'seller' => $seller,
            'product' => $product,
            'condition' => $condition,
            'region' => $region,

        ]);
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function delete()
    {
        AppOrgUserInventory::find($this->deleteId)->delete();
        $this->emit('alert');
    }
}
