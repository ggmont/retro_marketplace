<?php

namespace App\Http\Livewire\Mobile;

use App\SysUser;
use App\SysUserSocial;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Livewire\WithFileUploads;
use Intervention\Image\Facades\Image;
use Illuminate\Validation\Rule;

use Livewire\Component;

class ConfigPublic extends Component
{
    use WithFileUploads;
    public $terms_and_conditions, $youtube, $tiktok, $instagram,$instagram_user, $profile_picture, $iteration,$twitch_user,$twitch,$facebook,$cover_picture;
    public $prueba = '';
    public $ultra = '';
    public $state = [];
    public $user;
    public $showEditModal = false;

    protected $listeners = ['refreshComponent' => '$refresh'];

    protected $rules = [
        'profile_picture' => 'nullable|image',
        'cover_picture' => 'nullable|image',
    ];

    public function render()
    {
        $social = count(SysUserSocial::where('user_id',  Auth::id())->get());
        $social_user =  SysUserSocial::where('user_id',  Auth::id())->get();
        //dd($social_user);
        return view('livewire.mobile.config-public', [
            'user' =>  SysUser::where('id', Auth::id())->first(),
            'social' => $social,
            'social_user' => $social_user,
        ]);
    }

    public function openModal()
    {
        $this->emit('show');
    }

 
    public function editProfile(SysUser $id)
    {

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $user->id;

        //dd($this->user);
        $this->profile_picture = $user->profile_picture;
        //dd($this->cover_picture);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-profile_picture');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function editCover(SysUser $id)
    {

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $user->id;

        //dd($this->user);
        $this->cover_picture = $user->cover_picture;
        //dd($this->cover_picture);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-cover_picture');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function SendFile()
    {
        $this->validate();
        $data = SysUser::where('id', Auth::id())->first();
        
        $filename  = 'profile_picture' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $this->profile_picture->getClientOriginalExtension();
        $path = public_path('/uploads/profile-images/' . $filename);
        $dateFolder = date('Y') . '/' . date('m') . '/' . date('d') . '/';
        $prueba_imagen = $this->profile_picture;
        $directory = $path . $dateFolder;
        Image::make($prueba_imagen->getRealPath())->resize(500, 500)->encode('jpg')->save($path);
        $data->profile_picture = '/uploads/profile-images/' . $filename;
      
        $image = $this->profile_picture->store('images', 'public');

        $data->update([
            'profile_picture' =>'/uploads/profile-images/' . $filename
        ]);

        $this->emitself('refreshComponent');
        $this->reset();
       
        $this->dispatchBrowserEvent('show-form-ultimate');

    }

    public function SendFileCover()
    {
        $this->validate();
        $data = SysUser::where('id', Auth::id())->first();
        //dd($this->cover_picture);
        $filename  = 'cover_picture' . '_' . auth()->user()->user_name . '_' . date("d-m-Y") . '_' . time()  . '.' . $this->cover_picture->getClientOriginalExtension();
        //dd($filename);
        $path = public_path('/uploads/cover-images/' . $filename);
        $dateFolder = date('Y') . '/' . date('m') . '/' . date('d') . '/';
        $prueba_imagen = $this->cover_picture;
        $directory = $path . $dateFolder;

        Image::make($prueba_imagen->getRealPath())->orientate()
        ->fit(1098, 500, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->encode('jpg')->save($path)->destroy();

        $data->cover_picture = '/uploads/cover-images/' . $filename;
      
        $image = $this->cover_picture->store('images', 'public');

        $data->update([
            'cover_picture' =>'/uploads/cover-images/' . $filename
        ]);

        $this->emitself('refreshComponent');
        $this->reset();
        $this->dispatchBrowserEvent('hide-form-cover_picture');
        $this->dispatchBrowserEvent('show-form-ultimate');

    }


    public function editTerms(SysUser $id)
    {

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $user->id;

        //dd($this->user);
        $this->terms_and_conditions = $user->terms_and_conditions;
        //dd($this->email);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-terms');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function updateTerms()
    {
        //dd('hhh');
        $probando = SysUser::find($this->user);
        //dd($probando);
        $probando->update([
            'terms_and_conditions' => $this->terms_and_conditions
        ]);
        $this->emitself('refreshComponent');
        //$this->emit('eventName');
        $this->dispatchBrowserEvent('hide-form-terms', ['message' => 'User added successfully!']);
        $this->dispatchBrowserEvent('show-form-ultimate');
        //dd($probando);
    }

    public function editTwitchNew(SysUserSocial $id)
    {

        //dd($id);
        $social_user =  SysUserSocial::where('user_id',  Auth::id())->first();
        //dd($social_user);
        //$user = SysUser::find(Auth::id());

        $this->reset();

        $this->showEditModal = true;

        $this->dispatchBrowserEvent('show-form-new-twitch');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function NewTwitch()
    {
        //dd('hhh');
        $social_edit =  SysUserSocial::where('user_id',  Auth::id())->first();
        $usuario = SysUser::find(Auth::id());
        //dd($this->youtube);

            $usuario->update(
                [
                'twitch_user' => $this->twitch
                ]
            );

                    
            SysUserSocial::create([
            'user_id' => $usuario->id,
            'twitch' => $this->twitch,
          ]);

            $this->emitself('refreshComponent');
            $this->dispatchBrowserEvent('hide-form-new-twitch');
            $this->dispatchBrowserEvent('show-form-ultimate');

        //dd($probando);
    }

    public function editYoutubeNew(SysUserSocial $id)
    {

        //dd($id);
        $social_user =  SysUserSocial::where('user_id',  Auth::id())->first();
        //dd($social_user);
        //$user = SysUser::find(Auth::id());

        $this->reset();

        $this->showEditModal = true;

        $this->dispatchBrowserEvent('show-form-new-youtube');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function NewYoutube()
    {
        //dd('hhh');
        $social_edit =  SysUserSocial::where('user_id',  Auth::id())->first();
        $usuario = SysUser::find(Auth::id());
        //dd($this->youtube);
                    
            SysUserSocial::create([
            'user_id' => $usuario->id,
            'youtube' => $this->youtube,
          ]);

            $this->emitself('refreshComponent');
            $this->dispatchBrowserEvent('hide-form-new-youtube');
            $this->dispatchBrowserEvent('show-form-ultimate');

        //dd($probando);
    }

    public function editTiktokNew(SysUserSocial $id)
    {

        //dd($id);
        $social_user =  SysUserSocial::where('user_id',  Auth::id())->first();
        //dd($social_user);
        //$user = SysUser::find(Auth::id());

        $this->reset();

        $this->showEditModal = true;

        $this->dispatchBrowserEvent('show-form-new-tiktok');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function NewTiktok()
    {
        //dd('hhh');
        $social_edit =  SysUserSocial::where('user_id',  Auth::id())->first();
        $usuario = SysUser::find(Auth::id());
        //dd($this->youtube);
                    
            SysUserSocial::create([
            'user_id' => $usuario->id,
            'tiktok' => $this->tiktok,
          ]);

            $this->emitself('refreshComponent');
            $this->dispatchBrowserEvent('hide-form-new-tiktok');
            $this->dispatchBrowserEvent('show-form-ultimate');

        //dd($probando);
    }

    public function editInstagramNew(SysUserSocial $id)
    {

        //dd($id);
        $social_user =  SysUserSocial::where('user_id',  Auth::id())->first();
        //dd($social_user);
        //$user = SysUser::find(Auth::id());

        $this->reset();

        $this->showEditModal = true;

        $this->dispatchBrowserEvent('show-form-new-instagram');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function NewInstagram()
    {
        //dd('hhh');
        $social_edit =  SysUserSocial::where('user_id',  Auth::id())->first();
        $usuario = SysUser::find(Auth::id());
        //dd($this->youtube);
                    
            SysUserSocial::create([
            'user_id' => $usuario->id,
            'instagram' => $this->instagram,
          ]);

            $this->emitself('refreshComponent');
            $this->dispatchBrowserEvent('hide-form-new-instagram');
            $this->dispatchBrowserEvent('show-form-ultimate');

        //dd($probando);
    }

    public function editTwitch(SysUserSocial $id)
    {

        //dd($id);
        $social_user =  SysUserSocial::where('user_id',  Auth::id())->first();
        //$user = SysUser::find(Auth::id());
        
        $this->reset();

        $this->showEditModal = true;

        $this->user = $social_user->id;

        //dd($this->user);
        $this->twitch = $social_user->twitch;
        //dd($this->beneficiary);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-twitch');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }



    public function updateTwitch()
    {
        //dd('hhh');
        $social_edit =  SysUserSocial::where('user_id',  Auth::id())->first();
        $usuario = SysUser::find(Auth::id());
        //dd($this->youtube);

            $usuario->update(
                [
                'twitch_user' => $this->twitch
                ]
            );

            $social_edit->update(
                [
                    'twitch' => $this->twitch
                ]
            );
            $this->emitself('refreshComponent');
            $this->dispatchBrowserEvent('hide-form-twitch');
            $this->dispatchBrowserEvent('show-form-ultimate');

        //dd($probando);
    }

    public function resetForm()
    {
        $this->profile_picture = '';
    }

    public function editFacebook(SysUserSocial $id)
    {

        //dd($id);
        $social_user =  SysUserSocial::where('user_id',  Auth::id())->first();
        //dd($social_user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $social_user->id;

        //dd($this->user);
        $this->facebook = $social_user->facebook;
        //dd($this->beneficiary);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-facebook');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function updateFacebook()
    {
        //dd('hhh');
        $social_edit =  SysUserSocial::where('user_id',  Auth::id())->first();
        //dd($this->youtube);
        if (strpos($this->facebook, 'https://www.facebook.com/') !== false) {
            $social_edit->update(
                [
                    'facebook' => $this->facebook
                ]
            );
            $this->emitself('refreshComponent');
            $this->dispatchBrowserEvent('hide-form-facebook');
            $this->dispatchBrowserEvent('show-form-ultimate');
        } else {
            
            $this->emitself('refreshComponent');
            $this->dispatchBrowserEvent('hide-form-facebook');
            $this->dispatchBrowserEvent('show-form-errorgm');
        }
        //dd($probando);
    }


    public function editYoutube(SysUserSocial $id)
    {

        //dd($id);
        $social_user =  SysUserSocial::where('user_id',  Auth::id())->first();
        //dd($social_user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $social_user->id;

        //dd($this->user);
        $this->youtube = $social_user->youtube;
        //dd($this->beneficiary);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-youtube');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function updateYoutube()
    {
        //dd('hhh');
        $social_edit =  SysUserSocial::where('user_id',  Auth::id())->first();
        //dd($this->youtube);
            $social_edit->update(
                [
                    'youtube' => $this->youtube
                ]
            );
            $this->emitself('refreshComponent');
            $this->dispatchBrowserEvent('hide-form-youtube');
            $this->dispatchBrowserEvent('show-form-ultimate');

        //dd($probando);
    }

    public function editTikTok(SysUserSocial $id)
    {

        //dd($id);
        $social_user =  SysUserSocial::where('user_id',  Auth::id())->first();
        //dd($social_user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $social_user->id;

        //dd($this->user);
        $this->tiktok = $social_user->tiktok;
        //dd($this->discord);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-tiktok');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function updateTikTok()
    {
        //dd('hhh');
        $social_edit =  SysUserSocial::where('user_id',  Auth::id())->first();
        //dd($social_edit);
       
            $social_edit->update(
                [
                    'tiktok' => $this->tiktok
                ]
            );
            $this->emitself('refreshComponent');
            $this->dispatchBrowserEvent('hide-form-tiktok');
            $this->dispatchBrowserEvent('show-form-ultimate');

        //dd($probando);
    }


    public function editInstagram(SysUserSocial $id)
    {

        //dd($id);
        $social_user =  SysUserSocial::where('user_id',  Auth::id())->first();
        //dd($social_user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $social_user->id;

        //dd($this->user);
        $this->instagram = $social_user->instagram;
        //dd($this->beneficiary);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-instagram');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function updateInstagram()
    {
        //dd('hhh');
        $social_edit =  SysUserSocial::where('user_id',  Auth::id())->first();
        //dd($probando);
            $social_edit->update(
                [
                    'instagram' => $this->instagram
                ]
            );
            $this->emitself('refreshComponent');
            $this->dispatchBrowserEvent('hide-form-instagram');
            $this->dispatchBrowserEvent('show-form-ultimate');
    }
}
