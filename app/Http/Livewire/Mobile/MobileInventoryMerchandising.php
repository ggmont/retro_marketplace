<?php

namespace App\Http\Livewire;

use Livewire\Component;

class MobileInventoryMerchandising extends Component
{
    public function render()
    {
        return view('livewire.mobile-inventory-merchandising');
    }
}
