<?php

namespace App\Http\Livewire\Mobile;

use Livewire\Component;
use App\AppOrgProduct;
use Auth;
use App\AppOrgUserHistorialBidInventory;
use App\SysDictionary;
use App\SysCountry;

class OfferReceivedTable extends Component
{
    public function render()
    {
        $offersReceived = AppOrgUserHistorialBidInventory::where('user_id', Auth::id())
            ->orderBy('created_at', 'desc') // Ordenar de forma ascendente por la columna 'created_at'
            ->get();
    
        return view('livewire.mobile.offer-received-table', [
            'offersReceived' => $offersReceived
        ]);
    }
    
}
