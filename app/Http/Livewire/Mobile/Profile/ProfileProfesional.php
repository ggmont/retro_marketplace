<?php

namespace App\Http\Livewire\Mobile\Profile;

use App\SysUser;
use App\AppOrgUserRating;
use App\SysUserSocial;
use App\AppOrgProductImage;
use App\SysUserRoles;
use App\AppOrgProduct;
use App\SysDictionary;
use App\SysCountry;
use App\AppOrgUserInventory;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Livewire\Component;

class ProfileProfesional extends Component
{
    public $viewData;
    public $totalRecords;
    public $loadAmount = 30;
    public $search = '';
    public $probando = '';
    public $search_box = '';
    public $search_manual = '';
    public $search_game = '';
    public $search_extra = '';
    public $search_cover = '';
    public $search_inside = '';
    public $search_country = '';
    public $search_platform = '';
    public $search_category = '';
    public $search_sales = '';
    public $search_region = '';
    public $search_language = '';
    public $search_media = '';
    public $search_prueba = '';
    public $page = 1;
    public $favoriteId = '';
    public $desfavoriteId = '';
    public $deleteId = '';
    public $accion = "store";
    public $perPage = '12';
    public $prueba;
    public $nose;

    protected $listeners = ['postAdded' => 'incrementPostCount'];

    protected $queryString = [
        'search' => ['except' => ''],
        'search_category' => ['except' => ''],
    ];

    public function loadMore()
    {
        $this->loadAmount += 10;
    }

    public function mount($user)
    {
        //$this->prueba = SysUser::findOrFail($user);
        $this->prueba = $user;
        //$usuario = SysUser::where('user_name', $this->prueba)->first();
        //$ultra = AppOrgUserInventory::where('user_id', $usuario->id);
        //dd($ultra);
        $this->emit('pop');
        $this->totalRecords = AppOrgUserInventory::where('quantity', '>', 0)->count();
    }

    public function clear()
    {
        $this->search = '';
        $this->search_box = '';
        $this->search_manual = '';
        $this->search_game = '';
        $this->search_extra = '';
        $this->search_cover = '';
        $this->search_inside = '';
        $this->search_platform ='';
        $this->search_sales ='';
    }

    public function render()
    {
        $usuario = SysUser::where('user_name',  $this->prueba)->first();
        //dd($usuario);
        $condition = SysDictionary::where('code', 'GAME_STATE')->get(['value_id', 'value', 'parent_id']);
        $country = SysCountry::all();
        $inventories_game = AppOrgUserInventory::where('quantity', '>', 0)
        ->whereHas('product', function ($q) {
            $q->whereHas('categoryProd', function ($q) {
                $q->where('parent_id', 1);
            });
        })->count();
        $inventories_consoles = AppOrgUserInventory::where('quantity', '>', 0)
        ->whereHas('product', function ($q) {
            $q->whereHas('categoryProd', function ($q) {
                $q->where('parent_id', 2);
            });
        })->count();
        $inventories_perifericos = AppOrgUserInventory::where('quantity', '>', 0)
        ->whereHas('product', function ($q) {
            $q->whereHas('categoryProd', function ($q) {
                $q->where('parent_id', 3);
            });
        })->count();
        $inventories_accesorios = AppOrgUserInventory::where('quantity', '>', 0)
        ->whereHas('product', function ($q) {
            $q->whereHas('categoryProd', function ($q) {
                $q->where('parent_id', 4);
            });
        })->count();
        $inventories_merch = AppOrgUserInventory::where('quantity', '>', 0)
        ->whereHas('product', function ($q) {
            $q->whereHas('categoryProd', function ($q) {
                $q->where('parent_id', 173);
            });
        })->count();
        $platform = SysDictionary::where("code", "GAME_PLATFORM")->get();
        $region = SysDictionary::where("code", "GAME_REGION")->get();
        $media = SysDictionary::where("code", "GAME_SUPPORT")->get();
        $language = SysDictionary::where("code", "GAME_LANGUAGE")->get();

        return view('livewire.mobile.profile.profile-profesional', [
            'country' => $country,
            'platform' => $platform,
            'region' => $region,
            'media' => $media,
            'language' => $language,
            'inventories_game' => $inventories_game,
            'inventories_consoles' => $inventories_consoles,
            'inventories_perifericos' => $inventories_perifericos,
            'inventories_accesorios' => $inventories_accesorios,
            'inventories_merch' => $inventories_merch,
            'condition' => $condition
        ])->with(
            'profesional',
            AppOrgUserInventory::where('user_id', $usuario->id)
            ->where('quantity', '>', 0)
            ->where('in_collection', 'N')
            ->where('favorite', 1)
            ->where('auction_type', 'LIKE', "%{$this->search_sales}%")
            ->where('box_condition', 'LIKE', "%{$this->search_box}%")
            ->where('manual_condition', 'LIKE', "%{$this->search_manual}%")
            ->where('cover_condition', 'LIKE', "%{$this->search_cover}%")
            ->where('game_condition', 'LIKE', "%{$this->search_game}%")
            ->where('extra_condition', 'LIKE', "%{$this->search_extra}%")
            ->when($this->search, function ($query) {
                return $query->whereHas('product', function ($q) {
                    $q->where('name', 'LIKE', "%{$this->search}%");
                    $q->where('platform', 'like', '%' . $this->search_platform);
                    $q->where('region', 'LIKE', "%{$this->search_region}%");
                    $q->where('language', 'LIKE', "%{$this->search_language}%");
                    $q->when($this->search_category, function ($q) {
                        $q->whereHas('categoryProd', function ($q) {
                            $q->where('category_text', 'LIKE', "%{$this->search_category}%");
                        });
                    });
                });
            })
            ->when($this->search_country, function ($query) {
                return $query->whereHas('user', function ($q) {
                    $q->whereHas('country', function ($q) {
                        $q->where('name', 'LIKE', "%{$this->search_country}%");
                    });
                });
            })
            ->orderBy('id', 'desc')
            ->limit($this->loadAmount)
            ->get()
        );
 
    }

}
