<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ReferralsMobile extends Component
{
    public function render()
    {
        return view('livewire.referrals-mobile');
    }
}
