<?php

namespace App\Http\Livewire\Mobile;

use App\AppOrgProduct;
use App\SysDictionary;
use App\AppOrgUserInventory;
use App\SysCountry;
use Auth;
use Livewire\Component;
use Livewire\WithPagination;

class MobileInventoryConsole extends Component
{
    use WithPagination;

    protected $queryString = [
        'search' => ['except' => ''],
        'search_country' => ['except' => ''],
    ];

    public $deleteId = '';
    public $perPage = '50';
    public $prueba;
    public $articulo;
    public $search = '';
    public $search_box = '';
    public $search_manual = '';
    public $search_game = '';
    public $search_region = '';
    public $search_extra = '';
    public $search_cover = '';
    public $search_inside = '';
    public $search_country = '';
    public $viewData;
    public $update;


    public function mount($producto)
    {
        //$this->prueba = SysUser::findOrFail($user);
        $this->prueba = $producto->id;
        $this->articulo = $producto;
        $this->emit('game');
    }

    public function clear()
    {
        $this->search_box = '';
        $this->search_manual = '';
        $this->search_game = '';
        $this->search_region = '';
        $this->search_extra = '';
        $this->search_cover = '';
        $this->search_inside = '';
        $this->search_country ='';
        $this->emit('game');
        $this->emit('clearFiltersConsole');
    }

    public function render()
    {
        $product = $this->articulo;
        $condition = SysDictionary::where('code', 'GAME_STATE')->get(['value_id', 'value', 'parent_id']);
        $country = SysCountry::all();
        $region = SysDictionary::where("code", "GAME_REGION")->get();

        $productsInCart = [];
        $productInventory = 0;
        if (Auth::user()) {
            foreach (Auth::user()->cart->details as $key) {
                $inventory = AppOrgUserInventory::find($key->inventory_id);
                $productInventory += 1;
                $inventory->qty = $key->quantity;
                $inventory->inventory_images = $inventory->images;
                $inventory->product_info = AppOrgProduct::find($inventory->product_id);
                $inventory->product_info->product_images = $inventory->product_info->images;
                array_push($productsInCart, $inventory);
            }
        }

        //dd($product);
        $seller = AppOrgUserInventory::ofSeller()->where('product_id', $this->prueba)
            ->where('quantity', '>', 0)
            ->where('user_name', 'LIKE', "%{$this->search}%")
            ->where('box_condition', 'LIKE', "%{$this->search_box}%")
            ->where('manual_condition', 'LIKE', "%{$this->search_manual}%")
            ->where('cover_condition', 'LIKE', "%{$this->search_cover}%")
            ->where('game_condition', 'LIKE', "%{$this->search_game}%")
            ->where('extra_condition', 'LIKE', "%{$this->search_extra}%")
            ->where('inside_condition', 'LIKE', "%{$this->search_inside}%")
            ->whereHas('user', function ($q) {
                $q->whereHas('country', function ($q) {
                    $q->where('name', 'LIKE', "%{$this->search_country}%");
                });
            })
            ->orderBy('price', 'ASC')
            ->paginate($this->perPage);
        //dd($seller);
        return view('livewire.mobile.mobile-inventory-console', [
            'country' => $country,
            'seller' => $seller,
            'product' => $product,
            'condition' => $condition,
            'region' => $region,
            'productInventory' => $productInventory

        ]);
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function delete()
    {
        AppOrgUserInventory::find($this->deleteId)->delete();
        $this->emit('alert');
    }
    
}



