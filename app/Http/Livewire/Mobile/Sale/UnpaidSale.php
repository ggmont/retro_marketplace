<?php

namespace App\Http\Livewire\Mobile\Sale;

use App\AppOrgOrder;
use Livewire\Component;
use Auth;
use Carbon\Carbon;
use Livewire\WithPagination;

class UnpaidSale extends Component
{
    public $totalRecords;
    public $loadAmount = 100;
    public $search_seller = '';

    public function loadMore()
    {
        $this->loadAmount += 10;
    }

    public function mount()
    {
        $this->totalRecords = AppOrgOrder::where('seller_user_id', Auth::id())->whereIn('status', ['CW','RP'])->count();
    }

    public function render()
    {
        return view('livewire.mobile.sale.unpaid-sale', [
            'unpaid' => AppOrgOrder::where('seller_user_id', Auth::id())
            ->whereIn('status', ['CW','RP'])
            ->whereHas('seller', function ($q) {
                $q->where('user_name', 'LIKE', "%{$this->search_seller}%");
            })
            ->orderBy('id','desc')
            ->get()
        ]); 
    }
}
