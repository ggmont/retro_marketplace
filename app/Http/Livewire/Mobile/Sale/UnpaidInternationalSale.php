<?php

namespace App\Http\Livewire\Mobile\Sale;

use App\AppOrgOrder;
use Livewire\Component;
use Auth;
use Carbon\Carbon;
use Livewire\WithPagination;

class UnpaidInternationalSale extends Component
{

    public $totalRecords;
    public $loadAmount = 100;
    public $search_seller = '';

    public function loadMore()
    {
        $this->loadAmount += 10;
    }

    public function mount()
    {
        $this->totalRecords = AppOrgOrder::where(function ($query) {
            $query->where(function ($query) {
                $query->whereHas('buyer', function ($query) {
                    $query->where('country_id', '!=', 46); // ID del país de España
                });
            })
            ->orWhere(function ($query) {
                $query->whereHas('seller', function ($query) {
                    $query->where('country_id', '!=', 46); // ID del país de España
                });
            });
        })
        ->where('seller_user_id', Auth::id())
        ->whereIn('status', ['CW', 'RP'])
        ->count();
        
    }

    public function render()
    {
        return view('livewire.mobile.sale.unpaid-international-sale', [
            'unpaid' => AppOrgOrder::where(function ($query) {
                $query->where(function ($query) {
                    $query->whereHas('seller', function ($query) {
                        $query->where('country_id', '!=', 46); // ID del país de España
                    });
                })->orWhere(function ($query) {
                    $query->whereHas('buyer', function ($query) {
                        $query->where('country_id', '!=', 46); // ID del país de España
                    });
                });
            })
            ->whereIn('status', ['CW', 'RP'])
            ->whereHas('seller', function ($q) {
                $q->where('user_name', 'LIKE', "%{$this->search_seller}%");
            })
            ->orderBy('id', 'desc')
            ->get()
        ]); 
    }

}
