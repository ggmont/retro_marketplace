<?php

namespace App\Http\Livewire\Mobile\Sale;

use App\AppOrgOrder;
use Livewire\Component;
use Auth;
use Carbon\Carbon;
use Livewire\WithPagination;

class CancelSale extends Component
{
    public $totalRecords;
    public $loadAmount = 100;
    public $search_seller = '';

    public function loadMore()
    {
        $this->loadAmount += 10;
    }

    public function mount()
    {
        $this->totalRecords = AppOrgOrder::where('seller_user_id', Auth::id())->where('status', 'CN')->count();
    }

    public function render()
    {
        return view('livewire.mobile.sale.cancel-sale', [
            'cancel' => AppOrgOrder::where('seller_user_id', Auth::id())
            ->where('status', 'CN')
            ->whereHas('seller', function ($q) {
                $q->where('user_name', 'LIKE', "%{$this->search_seller}%");
            })
            ->orderBy('id','desc')
            ->limit($this->loadAmount)
            ->get()
        ]); 
    }
}
