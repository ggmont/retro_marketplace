<?php

namespace App\Http\Livewire\Mobile;
use App\SysUser;
use App\SysAccountBank;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Livewire\WithFileUploads;
use Illuminate\Validation\Rule;

use Livewire\Component;

class BalancePersonal extends Component
{
    public $beneficiary,$iban_code,$bic_code,$cash;
    public $prueba = '';
    public $ultra = '';
    public $state = [];
    public $user;
    public $showEditModal = false;

    protected $listeners = ['refreshComponent' => '$refresh'];

    public function render()
    {
        $user =  SysUser::where('id', Auth::id())->get();
        //dd($user);
        return view('livewire.mobile.balance-personal', [
            'user' => $user,
        ]);
    }

    public function editCash(SysUser $id){

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        $this->reset();

		$this->showEditModal = true;

		$this->user = $user->id;

        //dd($this->user);
        $this->cash = $user->cash;
        //dd($this->beneficiary);

		$this->state = $id->toArray();

		$this->dispatchBrowserEvent('show-form-cash');
        
        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }


    public function editBank(SysUser $id){

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        $this->reset();

		$this->showEditModal = true;

		$this->user = $user->id;

        //dd($this->user);
        $this->beneficiary = $user->bank->beneficiary;
        //dd($this->beneficiary);

		$this->state = $id->toArray();

		$this->dispatchBrowserEvent('show-form-bank');
        
        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function updateBank()
    {
        //dd('hhh');
        //$probando = SysUser::find($this->user);
        $bankAccount = SysAccountBank::where('user_id', $this->user)->first();
        //dd($bankAccount);
        $bankAccount->update([
            'beneficiary' => $this->beneficiary
        ]);
        $this->emitself('refreshComponent');
        $this->dispatchBrowserEvent('hide-form-bank');
        $this->dispatchBrowserEvent('show-form-ultimate');
        //dd($probando);
    }

    public function editIban(SysUser $id){

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        $this->reset();

		$this->showEditModal = true;

		$this->user = $user->id;

        //dd($this->user);
        $this->iban_code = $user->bank->iban_code;
        //dd($this->beneficiary);

		$this->state = $id->toArray();

		$this->dispatchBrowserEvent('show-form-iban');
        
        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function updateIban()
    {
        //dd('hhh');
        //$probando = SysUser::find($this->user);
        $bankAccount = SysAccountBank::where('user_id', $this->user)->first();
        //dd($bankAccount);
        $bankAccount->update([
            'iban_code' => $this->iban_code
        ]);
        $this->emitself('refreshComponent');
        $this->dispatchBrowserEvent('hide-form-iban');
        $this->dispatchBrowserEvent('show-form-ultimate');
    }

    public function editBicCode(SysUser $id){

        //dd($id);
        $user =  SysUser::where('id', Auth::id())->first();
        //dd($user);
        $this->reset();

		$this->showEditModal = true;

		$this->user = $user->id;

        //dd($this->user);
        $this->bic_code = $user->bank->bic_code;
        //dd($this->beneficiary);

		$this->state = $id->toArray();

		$this->dispatchBrowserEvent('show-form-bic_code');
        
        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }

    public function updateBicCode()
    {
        //dd('hhh');
        //$probando = SysUser::find($this->user);
        $bankAccount = SysAccountBank::where('user_id', $this->user)->first();
        //dd($bankAccount);
        $bankAccount->update([
            'bic_code' => $this->bic_code
        ]);
        $this->emitself('refreshComponent');
        $this->dispatchBrowserEvent('hide-form-bic_code');
        $this->dispatchBrowserEvent('show-form-ultimate');
    }

}
