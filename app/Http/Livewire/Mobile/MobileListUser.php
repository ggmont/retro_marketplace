<?php

namespace App\Http\Livewire\Mobile;

use App\SysUser;
use App\SysCountry;
use App\SysUserRoles;
use App\SysRoles;
use Livewire\Component;

class MobileListUser extends Component
{
    public $userId = '';
    public $search = '';
    public $search_country = '';
    public $search_rol = '';
    public $totalRecords;
    public $loadAmount = 50;
    public $perPage = '50';
    public $filter_user;

    protected $listeners = ['postAdded' => 'incrementPostCount'];

    public function loadMore()
    {
        $this->loadAmount += 10;
    }

    public function mount()
    {
        $this->totalRecords = SysUser::count();

        //dd($this->totalRecords);
    }

    public function editMsg(SysUser $id)
    {

        //dd($id);
        $user =  $id;
        //dd($user);
        $this->reset();

        $this->showEditModal = true;

        $this->user = $user->id;

        //dd($this->user);
        $this->user_name = $user->user_name;
        $this->first_name = $user->first_name;
        $this->last_name = $user->last_name;
        //dd($this->email);

        $this->state = $id->toArray();

        $this->dispatchBrowserEvent('show-form-message');

        //$user =  SysUser::where('id', Auth::id())->first();

        //$this->emit('openContactModal');  //this is where I open my modal form component
        // dd($contacts);
    }


    public function render()
    {
        $rol = SysRoles::all();
        $country = SysCountry::all();
        $filter_user = SysUserRoles::where('role_id', '!=', 1)->pluck('user_id');
        return view('livewire.mobile.mobile-list-user', [
            'rol' => $rol,
            'country' => $country,
            'user'     => SysUser::wherein('id', $filter_user)->groupBy('id')
                ->orderBy("id", "DESC")
                ->where('user_name', 'LIKE', "%{$this->search}%")
                ->whereHas('country', function ($q) {
                    $q->where('name', 'LIKE', "%{$this->search_country}%");
                })
                ->whereHas('roles', function ($q) {
                    $q->whereHas('rol', function ($q) {
                        $q->where('name', 'LIKE', "%{$this->search_rol}%");
                    });
                })
                ->limit($this->loadAmount)
                ->get()
        ]);
    }

    public function userId($id)
    {
        $this->userId = $id;
    }
}
