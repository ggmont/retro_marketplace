<?php

namespace App\Http\Livewire\Mobile;

use Livewire\Component;
use App\AppOrgProduct;
use Illuminate\Support\Facades\Mail;
use App\Mail\BidDeniedNotification;
use Auth;
use App\AppOrgUserInventory;
use App\SysDictionary;
use App\SysCountry;

class MyInventoryMobile extends Component
{

    public $favoriteId = '';
    public $desfavoriteId = '';
    public $refreshId = '';
    public $deleteId = '';
    public $totalRecords;
    public $loadAmount = 20;
    public $search = '';
    public $probando = '';
    public $search_box = '';
    public $search_manual = '';
    public $search_game = '';
    public $search_extra = '';
    public $search_cover = '';
    public $search_inside = '';
    public $search_country = '';
    public $search_platform = '';
    public $search_category = '';
    public $search_region = '';
    public $search_language = '';
    public $search_media = '';
    public $search_prueba = '';
    public $search_sales = '';

    public $filterProduct = true;
    public $filterCondition = true;

    protected $listeners = ['postAdded' => 'incrementPostCount'];

    protected $queryString = [
        'search' => ['except' => ''],
        'search_category' => ['except' => ''],
    ];

    public function loadMore()
    {
        $this->loadAmount += 10;
        $this->emit('star');
    }

    public function incrementPostCount()
    {
        $this->probando = 'NOT-PRES';
        $this->search_prueba = AppOrgUserInventory::where('box_condition', 'LIKE', "%{$this->probando}%")->get();
        $this->emit('star');
        //dd($this->search_prueba);
    }


    public function mount()
    {
        $this->totalRecords = AppOrgUserInventory::where('user_id', Auth::id())->where('quantity', '>', 0)->count();
        $this->emit('star');
    }

    public function clear_all()
    {
        $this->search = '';
        $this->search_box = '';
        $this->search_manual = '';
        $this->search_game = '';
        $this->search_extra = '';
        $this->search_cover = '';
        $this->search_inside = '';
        $this->search_platform = '';
        $this->search_region = '';
        $this->search_category = '';
    }

    public function render()
    {
        $condition = SysDictionary::where('code', 'GAME_STATE')->get(['value_id', 'value', 'parent_id']);
        //dd($condition);
        $country = SysCountry::all();
        $inventories_game = AppOrgUserInventory::where('quantity', '>', 0)
            ->whereHas('product', function ($q) {
                $q->whereHas('categoryProd', function ($q) {
                    $q->where('parent_id', 1);
                });
            })->count();
        $inventories_consoles = AppOrgUserInventory::where('quantity', '>', 0)
            ->whereHas('product', function ($q) {
                $q->whereHas('categoryProd', function ($q) {
                    $q->where('parent_id', 2);
                });
            })->count();
        $inventories_perifericos = AppOrgUserInventory::where('quantity', '>', 0)
            ->whereHas('product', function ($q) {
                $q->whereHas('categoryProd', function ($q) {
                    $q->where('parent_id', 3);
                });
            })->count();
        $inventories_accesorios = AppOrgUserInventory::where('quantity', '>', 0)
            ->whereHas('product', function ($q) {
                $q->whereHas('categoryProd', function ($q) {
                    $q->where('parent_id', 4);
                });
            })->count();
        $inventories_merch = AppOrgUserInventory::where('quantity', '>', 0)
            ->whereHas('product', function ($q) {
                $q->whereHas('categoryProd', function ($q) {
                    $q->where('parent_id', 173);
                });
            })->count();
        $platform = SysDictionary::where("code", "GAME_PLATFORM")->get();
        $region = SysDictionary::where("code", "GAME_REGION")->get();
        $media = SysDictionary::where("code", "GAME_SUPPORT")->get();
        $language = SysDictionary::where("code", "GAME_LANGUAGE")->get();

        $query = AppOrgUserInventory::where('user_id', Auth::id())
            ->where('in_collection', 'N')
            ->where('quantity', '>', 0);

        if ($this->filterCondition) {
            $query->where('box_condition', 'LIKE', "%{$this->search_box}%")
                ->where('manual_condition', 'LIKE', "%{$this->search_manual}%")
                ->where('cover_condition', 'LIKE', "%{$this->search_cover}%")
                ->where('auction_type', 'LIKE', "%{$this->search_sales}%")
                ->where('game_condition', 'LIKE', "%{$this->search_game}%")
                ->where('extra_condition', 'LIKE', "%{$this->search_extra}%");
        }

        if ($this->filterProduct) {

            $query->where(function ($query) {
                $query->where(function ($q) {
                    $q->where('title', 'LIKE', "%{$this->search}%");
                })->orWhereHas('product', function ($q) {
                    $q->where('name', 'LIKE', "%{$this->search}%");
                });
            });
        }

        $query->whereHas('user', function ($q) {
            $q->whereHas('country', function ($q) {
                $q->where('name', 'LIKE', "%{$this->search_country}%");
            });
        })
            ->orderBy('id', 'desc')
            ->limit($this->loadAmount);

        return view('livewire.mobile.my-inventory-mobile', [
            'country' => $country,
            'platform' => $platform,
            'region' => $region,
            'media' => $media,
            'language' => $language,
            'inventories_game' => $inventories_game,
            'inventories_consoles' => $inventories_consoles,
            'inventories_perifericos' => $inventories_perifericos,
            'inventories_accesorios' => $inventories_accesorios,
            'inventories_merch' => $inventories_merch,
            'condition' => $condition,
            'seller' => $query->get()
        ]);
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function delete()
    {
        $inventory = AppOrgUserInventory::find($this->deleteId);
    
        // Verificar si hay una puja activa en este inventario
        if (!empty($inventory->user_bid)) {
            Mail::to($inventory->userbid->email)->send(new BidDeniedNotification($inventory));
        }
    
        // Elimina el inventario
        $inventory->delete();
    
        $this->emit('alert');
        $this->emit('render');
    }
    

    public function favoriteId($id)
    {
        $this->favoriteId = $id;
    }

    public function update()
    {
        AppOrgUserInventory::find($this->favoriteId)->update(['favorite' => 1]);
        $this->emit('favorite');
        $this->emit('render');
    }

    public function desfavoriteId($id)
    {
        $this->desfavoriteId = $id;
    }

    public function update_desfavorite()
    {
        AppOrgUserInventory::find($this->desfavoriteId)->update(['favorite' => 0]);
        $this->emit('desfavorite');
        $this->emit('render');
    }

    public function RefreshId($id)
    {
        $this->refreshId = $id;
    }

    public function update_refresh()
    {
        AppOrgUserInventory::find($this->refreshId)->update([
            'countdown_hours' => 24,
            'offer_type' => 1, 
            'auction_type' => 1, 
            'max_bid' => 1, 
        ]);
        $this->emit('refresh');
        $this->emit('render');
    }    
}
