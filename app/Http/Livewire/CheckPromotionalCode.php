<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CheckPromotionalCode extends Component
{
    public $enabled;
    public $email;
    public $accion = "store";
    public $code,$is_enabled;
 
    protected $rules = [
        'code' => 'required',
        'is_enabled' => 'Y',
    ];

    public function render()
    {
        return view('livewire.check-promotional-code');
    }

    public function store()
    {
        $this->validate();
    }
}
