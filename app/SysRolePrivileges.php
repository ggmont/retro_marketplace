<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SysRolePrivileges extends Model
{
    use SoftDeletes;
    
	protected $dates = ['deleted_at'];
	
	protected $table = 'sysRolePrivileges';
	
	protected $fillable = [
        'role_id', 'privilege_id',
	];

	public function scopeOfRole($query, $role_id, $privilege_id) {
		return $query->where('role_id','=',$role_id)->where('privilege_id','=',$privilege_id);
	}
	
	public static function markOtherAsDeleted($role_id, $ids) {
		
		DB::table('sysRolePrivileges')
            ->where('role_id', $role_id)
            ->whereNotIn('id', $ids)
            ->delete();
		
	}
}
