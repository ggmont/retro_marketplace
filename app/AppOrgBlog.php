<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppOrgBlog extends Model
{
    use SoftDeletes;
    protected $table = 'appOrgBlogs';

    public function getTextHtmlAttribute()
    {
        return nl2br(e($this->content), true);
    }

}
