<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppOrgOrderCancel extends Model
{

    protected $table = 'appOrgOrderCancels';

    public function order() {
        return $this->hasOne('App\AppOrgOrder', 'id', 'order_id');
    }
    //appOrgOrderCancels
}
