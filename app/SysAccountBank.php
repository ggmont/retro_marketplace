<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\SysUser;

class SysAccountBank extends Model
{
    use SoftDeletes;
    
	protected $dates = ['deleted_at'];
	
    protected $table = 'sysAccountBanks';

    protected $fillable = [
        'beneficiary', 'iban_code', 'bic_code', 'bank_address'
    ];

    public function user() {
        return $this->hasMany('App\SysUser', 'id', 'user_id');
      }
    
    public function userBank(){
        return $this->belongsTo(SysUser::class);
    }
}
