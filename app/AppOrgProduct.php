<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\SysDictionary;
use App\AppOrgCategory;

use DB;

class AppOrgProduct extends Model
{
  use SoftDeletes;

  protected $table = 'appOrgProducts';

  protected $fillable = [
    'id', 'name', 'name_en', 'release_date', 'prod_category_id', 'price_new', 'price_solo', 'price_used',
    'game_category', 'game_producer', 'game_generation', 'game_location', 'language', 'box_language', 'volume',
    'weight', 'ean_upc', 'media', 'platform', 'platform_id', 'region', 'region_id', 'comments', 'is_featured',
    'is_enabled', 'request_by','large','width','high'
  ];

  protected $appends = ['category_val', 'average_price'];

  public function images()
  {
    return $this->hasMany('App\AppOrgProductImage', 'product_id', 'id');
  }

  public function inventory()
  {
    return $this->hasMany('App\AppOrgUserInventory', 'product_id', 'id')->orderBy('quantity', 'desc');
  }

  public function sysDictionaries()
  {
    return $this->belongsToMany(SysDictionary::class, 'appOrgProduct_sysDictionary', 'product_id', 'dictionary_id');
  }

  public function lang()
  {
    return $this->belongsTo('App\SysDictionary', 'value_id', 'language');
  }

  public function imageSearchBar()
  {
    if ($this->validateImagesExists()) {
      return $this->images[0]['image_path'];
    } else {
      return 'art-not-found.jpg';
    }
  }

  public function validateImagesExists()
  {
    return $this->images()->exists();
  }

  public function scopeWithAll($query)
  {
    $query
      ->select([
        DB::raw('appOrgProducts.*'),
        DB::raw(' (SELECT SUM(price) / count(1) average_price FROM appOrgUserInventories WHERE appOrgUserInventories.product_id = appOrgProducts.id) average_price')
      ])
      ->where('is_enabled', 'Y')
      ->with('images')
      ->with('inventory');
  }

  public function category()
  {
    return $this->belongsTo('App\AppOrgCategory', 'prod_category_id');
  }

  public function prueba()
  {
    return $this->belongsTo('App\AppOrgUserInventory', 'product_id');
  }

  public function categoryProd()
  {
    return $this->hasOne('App\AppOrgCategory', 'id', 'prod_category_id');;
  }

  public function getCategoryValAttribute()
  {
    return 'hola';
  }

  public function getAveragePriceAttribute()
  {
    return 0;
  }

  public function scopeOfCategory($query)
  {
    return $query;
  }

  public function getCatidAttribute()
  {
    $cat = substr(AppOrgCategory::find($this->prod_category_id)->category_text, 0, strpos(AppOrgCategory::find($this->prod_category_id)->category_text, ' -> '));
    $cat = AppOrgCategory::where('parent_id', 0)->where('name', 'like', $cat)->first();

    return $cat->id;
  }
}
