<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemLog extends Model
{
	protected $table = 'sysLogs';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['action','ip_address','table_name','affected_object_id','user_id','created_at','modified_at'];
}
