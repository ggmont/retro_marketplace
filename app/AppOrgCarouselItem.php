<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class AppOrgCarouselItem extends Model {

  protected $table = 'appOrgCarouselItems';

  protected $fillable = ['content_title','content_text','image_path','is_enabled','link','lang'];

  public static function markOtherAsDeleted($carousel_id, $ids) {

		DB::table('appOrgCarouselItems')
            ->where('carousel_id', $carousel_id)
            ->whereNotIn('id', $ids)
            ->delete();

	}

  public function carrusel() {
    return $this->belongsTo('App\AppOrgCarousel','carousel_id','id');
  }
}
