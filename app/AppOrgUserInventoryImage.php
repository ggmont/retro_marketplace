<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class AppOrgUserInventoryImage extends Model {
  
  protected $table = 'appOrgUserInventoryImages';

  protected $fillable = ['inventory_id', 'image_path', 'created_by', 'modified_by'];

  public static function markOtherAsDeleted($inventory_id, $ids) {

		DB::table('appOrgUserInventoryImages')
            ->where('inventory_id', $inventory_id)
            ->whereNotIn('id', $ids)
            ->delete();

	}
}
