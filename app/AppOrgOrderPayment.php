<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppOrgOrderPayment extends Model {

  protected $fillable = ['payment_result'];
  protected $table = 'appOrgOrderPayments';


}
