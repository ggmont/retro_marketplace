<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppOrgPromotionalCode extends Model
{
    protected $table = 'code_promotional';
	
    protected $fillable = [
        'user_id','code', 'is_enabled','special','type','description'
    ];

    public function user()
    {
        return $this->hasMany('App\SysUser', 'user_id');
    }

    public function cart()
    {
        return $this->belongsTo(AppOrgCartUser::class);
    }
}
