<?php

return [

  'dashboard' => ['type' => 'URL', 'url' => '/11w5Cj9WDAjnzlg0/dashboard', 'icon' => 'home', 'access' => ['CONFPRIV_VLT']],
  'withdrawals' => ['type' => 'URL', 'url' => '/11w5Cj9WDAjnzlg0/withdrawal', 'icon' => 'euro', 'access' => ['CONFPRIV_VLT']],
  'marketplace'   => ['type' => 'ROOT', 'icon' => 'cogs', 'label' => 'org.marketplace','children' =>
    [
      'categories' => ['type' => 'URL', 'url' => '/11w5Cj9WDAjnzlg0/marketplace/categories', 'icon' => 'circle-o', 'label' => 'org.products','access'=> ['MPCATG_VLT']],
      'products' => ['type' => 'URL', 'url' => '/11w5Cj9WDAjnzlg0/marketplace/products', 'icon' => 'circle-o', 'label' => 'org.products','access'=> ['MPPROD_VLT']],
      'inventory' => ['type' => 'URL', 'url' => '/11w5Cj9WDAjnzlg0/marketplace/inventory', 'icon' => 'circle-o', 'label' => 'org.inventory','access'=> ['MPINVE_VLT']],
      'inventory ean' => ['type' => 'URL', 'url' => '/11w5Cj9WDAjnzlg0/marketplace/inventory/ean', 'icon' => 'circle-o', 'label' => 'org.inventory_ean','access'=> ['MPINVE_VLT']],
      'shipping' => ['type' => 'URL', 'url' => '/11w5Cj9WDAjnzlg0/marketplace/shipping', 'icon' => 'circle-o', 'label' => 'org.shipping','access'=> ['CONFPRIV_VLT']],
      
      'orders' => ['type' => 'ROOT', 'icon' => 'circle-o', 'label' => 'org.orders', 'children' =>
        [
          'all' => ['type' => 'URL', 'url' => '/11w5Cj9WDAjnzlg0/marketplace/orders', 'icon' => 'circle-o', 'label' => 'org.orders','access'=> ['MPCATG_VLT']],
          'not_rec' => ['type' => 'URL', 'url' => '/11w5Cj9WDAjnzlg0/marketplace/orders/not', 'icon' => 'circle-o', 'label' => 'org.orders','access'=> ['MPCATG_VLT']],
        ]],
      'messages' => ['type' => 'URL', 'url' => '/11w5Cj9WDAjnzlg0/marketplace/messages', 'icon' => 'circle-o', 'label' => 'org.messages', 'access'=> ['MPMESG_VLT']],
      
    ],
  ],


  'front_end' => [ 'type' => 'ROOT', 'icon'=>'industry', 'children' =>
    [
      'banners'   => ['type'=>'URL','url' => '/11w5Cj9WDAjnzlg0/marketplace/banners','icon'=>'circle-o','access' => ['MPBANN_VLT']],
      'blogs'   => ['type'=>'URL','url' => '/11w5Cj9WDAjnzlg0/marketplace/blogs','icon'=>'circle-o','access' => ['MPBANN_VLT']],
      'carousels'   => ['type'=>'URL','url' => '/11w5Cj9WDAjnzlg0/marketplace/carousels','icon'=>'circle-o','access' => ['MPCARO_VLT']],
      'faqs'   => ['type'=>'URL','url' => '/11w5Cj9WDAjnzlg0/marketplace/faqs','icon'=>'circle-o','access' => ['MPFAQS_VLT']],
      'footer_links'   => ['type'=>'URL','url' => '/11w5Cj9WDAjnzlg0/marketplace/links','icon'=>'circle-o','access' => ['MPFLNK_VLT']],
      'news'   => ['type'=>'URL','url' => '/11w5Cj9WDAjnzlg0/marketplace/news','icon'=>'circle-o','access' => ['MPNEWS_VLT']],
      'pages'   => ['type'=>'URL','url' => '/11w5Cj9WDAjnzlg0/marketplace/pages','icon'=>'circle-o','access' => ['MPPAGE_VLT']],
    ]
  ],
  'configuration' => ['type' => 'ROOT', 'icon' => 'cogs', 'children' =>
    [
      'general' => ['type' => 'URL','url' => '/11w5Cj9WDAjnzlg0/configuration/general','icon' => 'cog','access'=> ['CONFGENE_VLT'] ],
      'lists'   => ['type'=>'URL','url' => '/11w5Cj9WDAjnzlg0/configuration/lists','icon'=>'circle-o','access' => ['CONFLIST_VLT']],
      'stripe'  => ['type' => 'URL','url' => '/11w5Cj9WDAjnzlg0/configuration/stripe','icon' => 'circle-o','access'=> ['CONFSTRI_MOD'] ],
      'google_maps'   => ['type'=>'URL','url' => '/11w5Cj9WDAjnzlg0/configuration/google_map','icon'=>'circle-o','access' => ['CONFGOMP_MOD']],
      'organizations'    => ['type'=>'URL','url' => '/11w5Cj9WDAjnzlg0/configuration/organizations','icon'=>'circle-o','access'   => ['CONFORG_VLT']],
      
    ],
  ],
  'user_management' => [ 'type' => 'ROOT', 'icon'=>'users', 'children' =>
    [
      'privileges'   => ['type'=>'URL','url' => '/11w5Cj9WDAjnzlg0/users/privileges','icon'=>'circle-o','access' => ['CONFPRIV_VLT']],
      'roles'   => ['type'=>'URL','url' => '/11w5Cj9WDAjnzlg0/users/roles','icon'=>'circle-o','access' => ['CONFROLES_VLT']],
      'users'   => ['type'=>'URL','url' => '/11w5Cj9WDAjnzlg0/users','icon'=>'circle-o','access' => ['CONFUSERS_VLT']],
      'messages'   => ['type'=>'URL','url' => '/11w5Cj9WDAjnzlg0/messages','icon'=>'circle-o','access' => ['CONFUSERS_VLT']],
    ],
  ],
];
