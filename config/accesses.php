<?php

return [

	'11w5Cj9WDAjnzlg0/dashboard'								=> 'ALL',
	'11w5Cj9WDAjnzlg0/messages'								=> 'ALL',
	'11w5Cj9WDAjnzlg0/messagesUs'								=> 'ALL',
	'11w5Cj9WDAjnzlg0/withdrawal'								=> 'CONFPRIV_VLT',
	'11w5Cj9WDAjnzlg0/showwiths'								=> 'ALL',
	'11w5Cj9WDAjnzlg0/showfees'								=> 'ALL',
	'11w5Cj9WDAjnzlg0/showpur'									=> 'ALL',
	'11w5Cj9WDAjnzlg0/showwithsped'							=> 'ALL',
	'11w5Cj9WDAjnzlg0/users/confirm_withdrawal'						=>	'CONFUSERS_ADD',
	// USER MANAGEMENT
	// Users
	'11w5Cj9WDAjnzlg0/users'													=>	'CONFUSERS_VLT',
	'11w5Cj9WDAjnzlg0/users/info'													=>	'CONFUSERS_VLT',
	'11w5Cj9WDAjnzlg0/users/view'										=>	'CONFUSERS_VDT',
	'11w5Cj9WDAjnzlg0/users/add'											=>	'CONFUSERS_ADD',
	'11w5Cj9WDAjnzlg0/users/modify'									=>	'CONFUSERS_MOD',
	'11w5Cj9WDAjnzlg0/users/delete'									=>	'CONFUSERS_DEL',
	'11w5Cj9WDAjnzlg0/users/audit'										=>	'CONFUSERS_AUD',
	'11w5Cj9WDAjnzlg0/users/logout'									=>	'ALL',
	'11w5Cj9WDAjnzlg0/users/AddCredit'									=>	'CONFUSERS_ADD',
	// Roles
	'11w5Cj9WDAjnzlg0/users/roles'										=>	'CONFROLES_VLT',
	'11w5Cj9WDAjnzlg0/users/roles/view'							=>	'CONFROLES_VDT',
	'11w5Cj9WDAjnzlg0/users/roles/add'								=>	'CONFROLES_ADD',
	'11w5Cj9WDAjnzlg0/users/roles/modify'						=>	'CONFROLES_MOD',
	'11w5Cj9WDAjnzlg0/users/roles/delete'						=>	'CONFROLES_DEL',
	'11w5Cj9WDAjnzlg0/users/roles/audit'							=>	'CONFROLES_AUD',

	// Privileges
	'11w5Cj9WDAjnzlg0/users/privileges'							=>	'CONFPRIV_VLT',
	'11w5Cj9WDAjnzlg0/users/privileges/view'					=>	'CONFPRIV_VDT',
	'11w5Cj9WDAjnzlg0/users/privileges/modify'					=>	'CONFPRIV_MOD',
	'11w5Cj9WDAjnzlg0/users/privileges/audit'					=>	'CONFPRIV_AUD',

	// Profiles
	'11w5Cj9WDAjnzlg0/users/profile'									=>	'ALL',
	'11w5Cj9WDAjnzlg0/users/profile/upload_image'		=>	'ALL',
	'11w5Cj9WDAjnzlg0/users/profile/save_profile'		=>	'ALL',
	'11w5Cj9WDAjnzlg0/utils/sidebar_collapse'				=>	'ALL',

	// CONFIGURATION
	// Organizations
	'11w5Cj9WDAjnzlg0/configuration/organizations'					=>	'CONFORG_VLT',
	'11w5Cj9WDAjnzlg0/configuration/organizations/view'		=>	'CONFORG_VDT',
	'11w5Cj9WDAjnzlg0/configuration/organizations/add'			=>	'CONFORG_ADD',
	'11w5Cj9WDAjnzlg0/configuration/organizations/modify'	=>	'CONFORG_MOD',
	'11w5Cj9WDAjnzlg0/configuration/organizations/delete'	=>	'CONFORG_DEL',
	'11w5Cj9WDAjnzlg0/configuration/organizations/audit'		=>	'CONFORG_AUD',

	// Lists
	'11w5Cj9WDAjnzlg0/configuration/lists'						=>	'CONFLIST_VLT',
	'11w5Cj9WDAjnzlg0/configuration/lists-dt'				=>	'CONFLIST_VLT',
	'11w5Cj9WDAjnzlg0/configuration/list'						=>	'CONFLIST_ADD',
	'11w5Cj9WDAjnzlg0/configuration/save_list'				=>	'CONFLIST_ADD',
	'11w5Cj9WDAjnzlg0/configuration/delete_list'			=>	'CONFLIST_DEL',
	'11w5Cj9WDAjnzlg0/configuration/audit_list'			=>	'CONFLIST_AUD',

	// Stripe
	'11w5Cj9WDAjnzlg0/configuration/stripe'					=>	'CONFSTRI_MOD',
	'11w5Cj9WDAjnzlg0/configuration/save_stripe'			=>	'CONFSTRI_MOD',
	'11w5Cj9WDAjnzlg0/configuration/audit_stripe'		=>	'CONFSTRI_AUD',

	// Stripe
	'11w5Cj9WDAjnzlg0/configuration/google_map'			=>	'CONFGOMP_MOD',
	'11w5Cj9WDAjnzlg0/configuration/save_google_map'	=>	'CONFGOMP_MOD',
	'11w5Cj9WDAjnzlg0/configuration/audit_google_map'=>	'CONFGOMP_AUD',

	// Market Place - Categories
	'11w5Cj9WDAjnzlg0/marketplace/categories'					=>	'MPCATG_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/categories/view'			=>	'MPCATG_VDT',
	'11w5Cj9WDAjnzlg0/marketplace/categories/add'			=>	'MPCATG_ADD',
	'11w5Cj9WDAjnzlg0/marketplace/categories/modify'		=>	'MPCATG_MOD',
	'11w5Cj9WDAjnzlg0/marketplace/categories/delete'		=>	'MPCATG_DEL',
	'11w5Cj9WDAjnzlg0/marketplace/categories/audit'		=>	'MPCATG_AUD',

	// Market Place - Products
	'11w5Cj9WDAjnzlg0/marketplace/products'					=>	'MPPROD_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/products/search'					=>	'MPPROD_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/products/view'			=>	'MPPROD_VDT',
	'11w5Cj9WDAjnzlg0/marketplace/products/add'			=>	'MPPROD_ADD',
	'11w5Cj9WDAjnzlg0/marketplace/products/modify'		=>	'MPPROD_MOD',
	'11w5Cj9WDAjnzlg0/marketplace/products/images'		=>	'CONFUSERS_ADD',
	'11w5Cj9WDAjnzlg0/marketplace/products/delete'		=>	'MPPROD_DEL',
	'11w5Cj9WDAjnzlg0/marketplace/products/audit'		=>	'MPPROD_AUD',

	// Market Place - Inventory
	'11w5Cj9WDAjnzlg0/marketplace/shipping'					=>	'CONFPRIV_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/shipping/list-dt'			=>	'CONFPRIV_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/shipping/add'          	=>	'CONFPRIV_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/shipping/modify'			=>	'CONFPRIV_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/shipping/delete'			=>	'CONFPRIV_VLT',

	// Market Place - Inventory
	'11w5Cj9WDAjnzlg0/marketplace/inventory'					=>	'MPINVE_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/inventory/ean'					=>	'MPINVE_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/view_inventory'		=>	'MPINVE_VDT',
	'11w5Cj9WDAjnzlg0/marketplace/add_inventory'			=>	'MPINVE_ADD',
	'11w5Cj9WDAjnzlg0/marketplace/modify_inventory'	=>	'MPINVE_MOD',
	'11w5Cj9WDAjnzlg0/marketplace/delete_inventory'	=>	'MPINVE_DEL',
	'11w5Cj9WDAjnzlg0/marketplace/audit_inventory'		=>	'MPINVE_AUD',

	// Market Place - Orders
	'11w5Cj9WDAjnzlg0/marketplace/orders'						=>	'MPPROD_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/orders/not'						=>	'MPPROD_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/view_order'				=>	'MPORDR_VDT',
	'11w5Cj9WDAjnzlg0/marketplace/add_order'					=>	'MPORDR_ADD',
	'11w5Cj9WDAjnzlg0/marketplace/modify_order'			=>	'MPORDR_MOD',
	'11w5Cj9WDAjnzlg0/marketplace/delete_order'			=>	'MPORDR_DEL',
	'11w5Cj9WDAjnzlg0/marketplace/audit_order'				=>	'MPORDR_AUD',

	// Market Place - Messages
	'11w5Cj9WDAjnzlg0/marketplace/messages'					=>	'MPMESG_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/view_message'			=>	'MPMESG_VDT',
	'11w5Cj9WDAjnzlg0/marketplace/add_message'				=>	'MPMESG_ADD',
	'11w5Cj9WDAjnzlg0/marketplace/modify_message'		=>	'MPMESG_MOD',
	'11w5Cj9WDAjnzlg0/marketplace/delete_message'		=>	'MPMESG_DEL',
	'11w5Cj9WDAjnzlg0/marketplace/audit_message'			=>	'MPMESG_AUD',

	// Market Place - Pages
	'11w5Cj9WDAjnzlg0/marketplace/pages'							=>	'MPPAGE_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/pages/view'				=>	'MPPAGE_VDT',
	'11w5Cj9WDAjnzlg0/marketplace/pages/add'					=>	'MPPAGE_ADD',
	'11w5Cj9WDAjnzlg0/marketplace/pages/modify'			=>	'MPPAGE_MOD',
	'11w5Cj9WDAjnzlg0/marketplace/pages/delete'			=>	'MPPAGE_DEL',
	'11w5Cj9WDAjnzlg0/marketplace/pages/audit'				=>	'MPPAGE_AUD',

	// Market Place - Links
	'11w5Cj9WDAjnzlg0/marketplace/links'							=>	'MPFLNK_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/links/view'				=>	'MPFLNK_VDT',
	'11w5Cj9WDAjnzlg0/marketplace/links/add'					=>	'MPFLNK_ADD',
	'11w5Cj9WDAjnzlg0/marketplace/links/modify'			=>	'MPFLNK_MOD',
	'11w5Cj9WDAjnzlg0/marketplace/links/delete'			=>	'MPFLNK_DEL',
	'11w5Cj9WDAjnzlg0/marketplace/links/audit'				=>	'MPFLNK_AUD',

	// Market Place - Faqs
	'11w5Cj9WDAjnzlg0/marketplace/faqs'							=>	'MPFAQS_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/faqs/view'					=>	'MPFAQS_VDT',
	'11w5Cj9WDAjnzlg0/marketplace/faqs/add'					=>	'MPFAQS_ADD',
	'11w5Cj9WDAjnzlg0/marketplace/faqs/modify'				=>	'MPFAQS_MOD',
	'11w5Cj9WDAjnzlg0/marketplace/faqs/delete'				=>	'MPFAQS_DEL',
	'11w5Cj9WDAjnzlg0/marketplace/faqs/audit'				=>	'MPFAQS_AUD',

	// Market Place - Carousel
	'11w5Cj9WDAjnzlg0/marketplace/carousels'					=>	'MPCARO_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/carousels/view'		=>	'MPCARO_VDT',
	'11w5Cj9WDAjnzlg0/marketplace/carousels/add'			=>	'MPCARO_ADD',
	'11w5Cj9WDAjnzlg0/marketplace/carousels/modify'	=>	'MPCARO_MOD',
	'11w5Cj9WDAjnzlg0/marketplace/carousels/delete'	=>	'MPCARO_DEL',
	'11w5Cj9WDAjnzlg0/marketplace/carousels/audit'		=>	'MPCARO_AUD',

	// Market Place - News
	'11w5Cj9WDAjnzlg0/marketplace/banners'						=>	'MPBANN_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/banners/view'			=>	'MPBANN_VDT',
	'11w5Cj9WDAjnzlg0/marketplace/banners/add'				=>	'MPBANN_ADD',
	'11w5Cj9WDAjnzlg0/marketplace/banners/modify'		=>	'MPBANN_MOD',
	'11w5Cj9WDAjnzlg0/marketplace/banners/delete'		=>	'MPBANN_DEL',
	'11w5Cj9WDAjnzlg0/marketplace/banners/audit'			=>	'MPBANN_AUD',

	// Market Place - News
	'11w5Cj9WDAjnzlg0/marketplace/news'						=>	'MPNEWS_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/news/view'				=>	'MPNEWS_VDT',
	'11w5Cj9WDAjnzlg0/marketplace/news/add'				=>	'MPNEWS_ADD',
	'11w5Cj9WDAjnzlg0/marketplace/news/modify'			=>	'MPNEWS_MOD',
	'11w5Cj9WDAjnzlg0/marketplace/news/delete'			=>	'MPNEWS_DEL',
	'11w5Cj9WDAjnzlg0/marketplace/news/audit'			=>	'MPNEWS_AUD',

	// Market Place - Blogs
	'11w5Cj9WDAjnzlg0/marketplace/blogs'						=>	'MPBLOGS_VLT',
	'11w5Cj9WDAjnzlg0/marketplace/blogs/view'				=>	'MPBLOGS_VDT',
	'11w5Cj9WDAjnzlg0/marketplace/blogs/add'				=>	'MPBLOGS_ADD',
	'11w5Cj9WDAjnzlg0/marketplace/blogs/modify'			=>	'MPBLOGS_MOD',
	'11w5Cj9WDAjnzlg0/marketplace/blogs/delete'			=>	'MPBLOGS_DEL',
	'11w5Cj9WDAjnzlg0/marketplace/blogs/audit'			=>	'MPBLOGS_AUD',


	// Organizations for individuals
	'11w5Cj9WDAjnzlg0/organizations/info'														=> 'ALL',
	'11w5Cj9WDAjnzlg0/organizations/save_info'												=> 'ALL',
	// Airfields
	'11w5Cj9WDAjnzlg0/my_store/configuration'												=> 'UMYCONF',
	'11w5Cj9WDAjnzlg0/my_store/messages'															=> 'CONFMESSAGE',
	'11w5Cj9WDAjnzlg0/my_store/inventory'														=> 'UMYINVE',
	'11w5Cj9WDAjnzlg0/my_store/invoices'															=> 'UMYINVO',
	'11w5Cj9WDAjnzlg0/my_store/orders'																=> 'UMYORDR',

];
