<?php

return [
  // General configuration
  'column_prefix' => 'br_c_',
  'app_name' => 'RetroMarketGaming Admin',
  'app_name_public' => 'Retro Gaming Market',
  'app_name_html' => '<b>Retro Gaming Market</b>',
  'app_name_html_mini' => '<b>RGM</b>ADM',
  'app_footer' => '<div class="pull-right hidden-xs"><b>Version</b> 1.0.0</div><strong>Copyright &copy; 2016 .</strong> All rights reserved.',

  // Permissions
  'app_allow_registration'  => true,
  'app_allow_pass_recover'  => true,


  // Stripe configuration
  'stripe_on_test'  =>  true,
  'stripe_test_pub_key' => '',
  'stripe_test_priv_key' => '',
  'stripe_prod_pub_key' => '',
  'stripe_prod_priv_key' => '',

  // Paypal configuration
  //'paypal_on_test'        =>  true,
  'paypal_sb_acc'         => 'sb-7prkg74430@business.example.com',
  'paypal_sb_acc_live'         => 'marketplace790@gmail.com',

  //'paypal_sb_token'       => 'access_token$sandbox$qmrysdf4hnwrnsn6$b877fc8d24b6fa2dfb9905a1ad89b947',

  'paypal_sb_client_id'   => 'AQnYXRUF-e-PQ42IGPXTqsO6VZqcL5jic9oqIvLg9KxhYmw1kxNU3aPhtKieYk83FCuTEW6nXM5H4Wey',
  'paypal_sb_client_id_live'   => 'AWbTqBarDb2KH6tJbxOoa_HebyQxCCEDJMBYSTLXoy8GKQg5gGu4yZCzDYOxkr_2h8mTApFZBtp8JeNs',

  'paypal_sb_secret'      => 'ED5Y_fFvWnS3d1AyZBSe3YltpI9IACqesGwfxZn-6pKlDmzx2ex5u_4FNhAFTPVrCXS9wlWNUoododGO',
  'paypal_sb_secret_live'      => 'EBfresY4_5A4MVWk0A1fPskR5cKJBRzbW0SiV90yq55IemqcZoQJQM2vqI3XJ4_74GomxGO9aKf32hfB',

  'paypal_currency'       => 'EUR',

];
/*
'paypal_sb_acc'         => 'ptykamikaze-facilitator@hotmail.com',
'paypal_sb_token'       => 'access_token$sandbox$qmrysdf4hnwrnsn6$b877fc8d24b6fa2dfb9905a1ad89b947',
'paypal_sb_client_id'   => 'Adpd80X4-uf44usDWT_0h0qiiJqol4I2M1uGefIItNTEG8KRIGDS7nI_8ZRZ0qF7hzP-4DBfAooREzo8',
'paypal_sb_secret'      => 'EOYafKlOhsmZ32vsUS8ue2Awsti77w3Y9mmn5PZLe569Zz78XXJQF1Q_LqCR9Epd48gHbTsa_TMDvZzy',
*/
